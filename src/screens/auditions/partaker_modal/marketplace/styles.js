import { StyleSheet } from 'react-native';
import { resize } from '../../../../assets/styles';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgba(0, 0, 0, 0.70)",
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        width: resize(500),
        height: resize(400),
        backgroundColor: 'white',
        borderRadius: resize(10),
        paddingVertical: resize(15, 'height'),
        paddingHorizontal: resize(15)
    },
    closeIcon: {
        alignSelf: 'flex-end',
        width: resize(60),
        height: '10%',
        alignItems: 'flex-end'
    },
    items: {
        width: '100%',
        height: '90%',
        justifyContent: 'flex-start'
    },
    inputSeach: {
        height: resize(50, 'height'),
        width: '100%',
        borderBottomColor: '#5f2543',
        borderBottomWidth: 1
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    places: {
        width: '100%',
        alignSelf: 'center',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: resize(60),
        backgroundColor: 'rgba(0, 0, 0, 0.05)'
    }
});
export default styles;
