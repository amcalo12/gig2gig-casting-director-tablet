import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  ScrollView,
  Image,
  TouchableOpacity,
  Keyboard
} from "react-native";
import { Input, Form } from "native-base";

import { searchMarketplace } from "../../../../api/auditions";
import Tags from "../../../../components/auditions/tags";
import styles from "./styles";

//Images
import Close from "../../../../assets/icons/commons/icon_close.png";

var timer;

class ModalMarket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      data: []
    };
  }

  onChange = (value, state) => {
    let isClear = value.length >= this.state[state].length;
    this.setState({ [state]: value });
    this.callFunction(value, isClear);
  };

  callFunction = (value, isClear) => {
    clearTimeout(timer);
    timer = setTimeout(async () => {
      searchMarketplace(value)
        .then(response => {
          let data = response.data.data;
          this.setState({ data });
        })
        .catch(e => {
          console.log(e);
          isClear ? Keyboard.dismiss() : "";
          this.setState({ data: [] });
        });
    }, 400);
  };

  clearData = (state, value) => {
    let { addTags, user_id, appointment_id } = this.props;
    this.setState({ search: "", data: [] });
    let custom = {
      id: null,
      markeplace: {
        title: value.title
      },
      user_id,
      marketplace_id: value.id,
      appointment_id: appointment_id
    };
    console.log(" appointment id 1111", appointment_id);
    addTags(state, custom);
  };

  render() {
    let { show, dismiss } = this.props;
    let { search, data } = this.state;
    return (
      <Modal
        animationType="fade"
        animated={true}
        visible={show}
        onRequestClose={dismiss}
        transparent={true}
      >
        <View style={styles.container}>
          <View>
            <View style={styles.content}>
              <TouchableOpacity onPress={dismiss} style={styles.closeIcon}>
                <Image resizeMode="contain" source={Close} />
              </TouchableOpacity>

              <View style={styles.items}>
                <View style={styles.inputSeach}>
                  <Input
                    ref={input => {
                      this.secondTextInput = input;
                    }}
                    value={search}
                    onChangeText={value => this.onChange(value, "search")}
                    style={{ color: "#5f2543" }}
                    placeholderTextColor="#5f2543"
                    placeholder="Search Marketplace"
                  />
                </View>
                {data.length !== 0 ? (
                  <ScrollView
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                  >
                    {data.map((snap, index) => {
                      return (
                        <TouchableOpacity
                          key={`options${index}`}
                          onPress={() => this.clearData("markets", snap)}
                        >
                          <Tags
                            style={styles.places}
                            title={snap.title}
                            showDelete={false}
                          />
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>
                ) : (
                  <View style={styles.center}>
                    <Text style={{ color: "#5f2543" }}>
                      No match with the marketplaces.
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

export default ModalMarket;
