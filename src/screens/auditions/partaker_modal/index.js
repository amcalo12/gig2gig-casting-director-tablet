import React, { Component, Fragment } from 'react';
import { View, TouchableOpacity, Text, Image, Modal, ScrollView } from 'react-native';
import { Container, Content } from 'native-base';

//custom
import styles from './styles';
import icon_close from '../../../assets/icons/commons/icon_close.png';

export default class PartakerModal extends Component {
  getMeasure = (y) => {
    this.container.scrollTo({ x: 0, y, animated: true })
  }

  render() {
    return (
      <Modal
        animationType='fade'
        animated={true}
        visible={this.props.show}
        transparent={true}
        onRequestClose={this.props.dismiss}
      >
        <Container style={styles.nBContainer}>
          <TouchableOpacity
            activeOpacity={1}
            style={styles.dismissButton}
            onPress={this.props.dismiss}
          >
            <View style={styles.blurView} />
          </TouchableOpacity>
          <Content bounces={false}>
            <View style={styles.cardContainer}>
              <View style={styles.card}>
                <TouchableOpacity
                  style={styles.closeButton}
                  onPress={this.props.dismiss}
                >
                  <Image source={icon_close} />
                </TouchableOpacity>
                <ScrollView
                  ref={scroll => this.container = scroll}
                >
                  {
                    this.props.data.map((section, index) => {
                      return (
                        <Fragment key={"title" + index}>
                          <View
                            onLayout={event => {
                              if (this.props.scrollTo === section.title) {
                                this.getMeasure(event.nativeEvent.layout.y)
                              }
                            }}
                          >
                            <Text style={styles.title} >
                              {section.title}
                            </Text>
                          </View>
                          <View style={styles.itemsContainers}>
                            {
                              section.items.map((item, indexItem) => (
                                <View
                                  key={section.title + indexItem}
                                  style={styles.item}
                                >
                                  <Text style={styles.itemText}>
                                    {item.label}
                                  </Text>
                                  <Text style={styles.itemSubText}>
                                    {item.value}
                                  </Text>
                                </View>
                              ))
                            }
                          </View>
                        </Fragment>
                      )
                    })
                  }
                </ScrollView>
              </View>
            </View>
          </Content>
        </Container>
      </Modal>
    )
  }
}

PartakerModal.defaultProps = {
  dismiss: () => null,
  show: true,
  scrollTo: "Appearance",
  data: [
    {
      title: "Info",
      items: [
        {
          label: "Stage Name",
          value: ""
        },
        {
          label: "Professional/ Working Title",
          value: ""
        },
        {
          label: "Location",
          value: ""
        }
      ]
    },
    {
      title: "Credits",
      items: [
        {
          label: "Production Type",
          value: ""
        },
        {
          label: "Project Name",
          value: ""
        },
        {
          label: "Role",
          value: ""
        },
        {
          label: "Director/Production Company",
          value: ""
        }
      ]
    }
    ,
    {
      title: "Education & Training",
      items: [
        {
          label: "School",
          value: ""
        },
        {
          label: "Degree/Course",
          value: ""
        },
        {
          label: "Instructor",
          value: ""
        },
        {
          label: "Location",
          value: ""
        },
        {
          label: "Year",
          value: ""
        }
      ]
    },
    {
      title: "Appearance",
      items: [
        {
          label: "Height",
          value: ""
        },
        {
          label: "Weight",
          value: ""
        },
        {
          label: "Hair Color",
          value: ""
        },
        {
          label: "Eye Color",
          value: ""
        },
        {
          label: "Race",
          value: ""
        }
      ]
    }
  ]
}