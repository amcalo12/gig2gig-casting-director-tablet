import { StyleSheet } from 'react-native';

//custom
import { width, height, NEXABOLD, NEXALIGHT, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  wrapperContent: {
    width,
    flexDirection: 'row',
    height
  },
  firstSectionAnimated: {
    width: resize(674),
  },
  firstSection: {
    paddingTop: resize(22, 'height'),
  },
  partakerContainer: {
    flexDirection: 'row',
    marginLeft: resize(31),
    flexWrap: 'wrap',
    marginBottom: resize(125, 'height'),
  },
  spiltContainer: {
    width: resize(22),
    height: resize(752, 'height'),
    backgroundColor: '#bfbfbf',
    alignItems: 'center',
    justifyContent: 'center'
  },
  spiltIcon: {
    width: resize(1),
    height: resize(40, "height"),
    borderColor: '#707070',
    borderWidth: resize(2.5),
    borderRadius: resize(30)
  },
  secondSection: {
    width: resize(416),
    backgroundColor: '#f0f0f0',
  },
  infoContainer: {
    height: resize(55, 'height'),
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
  },
  infoTitle: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
  },
  contentRight: {
    marginLeft: resize(42),
  },
  auditionInfoContainer: {
    paddingTop: resize(31, 'height'),
    paddingBottom: resize(26, 'height'),
    paddingRight: resize(91),
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
  },
  auditionName: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(28),
    fontWeight: '400',
    paddingVertical: resize(5, 'height')
  },
  auditionDateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  auditionDate: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(17),
    fontWeight: '400',
    paddingVertical: resize(5, 'height')
  },
  auditionCompany: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(17),
    fontWeight: '400',
    paddingVertical: resize(5, 'height')
  },
  appointmentsContainer: {
    paddingRight: resize(91),
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
    paddingVertical: resize(16, 'height'),
  },
  appointments: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
  },
  tagsContainer: {
    paddingRight: resize(96),
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
    paddingVertical: resize(16, 'height'),
    flexDirection: 'row',
  },
  tagPurple: {
    borderRadius: 19,
    borderColor: '#4d2545',
    borderWidth: 1,
    backgroundColor: '#4d2545',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
  },
  tagYellow: {
    borderRadius: 19,
    borderColor: '#d8893a',
    borderWidth: 1,
    backgroundColor: '#d8893a',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
  },
  tagRed: {
    borderRadius: 19,
    borderColor: '#93183e',
    borderWidth: 1,
    backgroundColor: '#93183e',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
  },
  tagsText: {
    color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: 13,
    fontWeight: '400',
  },
  auditionDescriptionContainer: {
    paddingRight: resize(42),
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
    paddingVertical: resize(27, 'height'),
  },
  auditionDescription: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(13),
    fontWeight: '400',
    textAlign: 'justify',
    paddingVertical: resize(5, 'height')
  },
  auditionUrlContainer: {
    paddingRight: resize(42),
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
    paddingVertical: resize(22, 'height'),
  },
  auditionUrl: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
  },
  rolesContainer: {
    paddingRight: resize(20)
  },
  resourcesContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
    borderTopWidth: 1,
    borderTopColor: '#d6d6d6',
    paddingVertical: resize(37, 'height'),
    paddingRight: resize(40),
    marginBottom: resize(125, 'height'),
  },
  noAuditions: {
    marginTop: resize(375, 'height'),
    paddingHorizontal: resize(171)
  },
  noAuditionsText: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: 28,
    fontWeight: '400',
    textAlign: 'center'
  },
  emptyText: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(17),
    fontWeight: '400',
  },
});

export default styles;