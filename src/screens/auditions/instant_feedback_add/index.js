import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  Image,
  TextInput,
  Platform,
  Keyboard
} from "react-native";
import { resize, NEXABOLD, NEXALIGHT } from "../../../assets/styles";
import { styles } from "./styles";
import en from "../../../locale/en";
import colors from "../../../utils/colors";
import { Assets } from "../../../assets";
import Autocomplete from "react-native-autocomplete-input";
import {
  addInstantFeedback,
  getSuggestedAudition
} from "../../../api/instant_feedback";
import showMessage from "../../../utils/show_message/index";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import parseError from "../../../utils/parse_error";

export default class InstantFeedbackAdd extends Component {
  constructor(props) {
    super(props);
    this.auditionData = [];
    this.state = {
      searchText: "",
      feedbackMessage: "",
      suggestedAudition: "",
      hideResults: false
    };
  }

  onMessageChange = text => {
    this.setState({ feedbackMessage: text });
  };

  onSearchText = text => {
    this.setState({ searchText: text });
  };

  checkValidation = () => {
    if (this.state.feedbackMessage == "") {
      showMessage("danger", "Please enter feedback message");
      return false;
    }

    return true;
  };

  onSubmitEditing = async () => {
    Keyboard.dismiss();

    try {
      const auditions = await getSuggestedAudition(this.state.searchText);
      console.log(" suggested audition ", auditions);
      this.auditionData = auditions.data.data;
      this.setState({ hideResults: false });
    } catch (error) {
      console.log(" suggested audition error", error);
    }
  };

  onItemSelect = (item, index) => {
    this.setState({
      searchText: item.title,
      suggestedAudition: item,
      hideResults: true,
      auditionData: []
    });
  };
  onFeedbackSubmit = async () => {
    if (!this.checkValidation()) {
      return;
    }

    let params = {
      appointment_id: this.props.appointment_id,
      user: this.props.currentUser.user_id,
      evaluator: this.props.evaluator,
      comment: this.state.feedbackMessage,
      suggested_appointment_id:
        this.state.suggestedAudition != ""
          ? this.state.suggestedAudition.id
          : "",
      accepted: 1
    };
    console.log("ACCEPT FEEDBACK PARAMS========>", params);

    try {
      const response = await addInstantFeedback(params);
      console.log(" ACCEPT FEEDBACK SUCCESS ", response);
      this.props.onCloseClick();
      showMessage("success", response.data.data);
      this.setState({
        isAddFeedback: false,
        auditionData: [],
        feedbackMessage: "",
        searchText: "",
        suggestedAudition: ""
      });
      this.props.feedbackSuccess();
    } catch (error) {
      console.log(" ACCEPT FEEDBACK ERROR ", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  render() {
    return (
      <Modal
        // animationType="slide"
        animationType="none"
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={this.props.onCloseClick}
      >
        {/* <TouchableOpacity
          style={styles.modalContainer}
          onPress={this.props.onCloseClick}
          activeOpacity={1}
        > */}
        <KeyboardAwareScrollView
          contentContainerStyle={styles.modalContainer}
          keyboardShouldPersistTaps="never"
          showsVerticalScrollIndicator={false}
        >
          <TouchableOpacity style={styles.modalSubContainer} activeOpacity={1}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ position: "absolute", left: 5 }}
                onPress={this.props.onCloseClick}
              >
                <Image source={Assets.close} />
              </TouchableOpacity>
              <Text style={{ color: colors.PRIMARY, fontSize: resize(18) }}>
                {en.instant_feedback}
              </Text>
            </View>

            <Text
              style={{
                alignSelf: "center",
                marginTop: 20,
                color: colors.PRIMARY
              }}
            >
              {en.send_a_message}
            </Text>

            <TextInput
              multiline={true}
              value={this.state.feedbackMessage}
              onChangeText={this.onMessageChange}
              style={{
                height: 150,
                borderColor: colors.PRIMARY,
                paddingTop: 10,
                lineHeight: 20,
                paddingHorizontal: 10,
                fontFamily: NEXALIGHT,
                borderWidth: 2,
                borderRadius: 16,
                marginTop: 10,
                textAlignVertical: "top"
              }}
              placeholder={en.add_brief_message}
              placeholderTextColor={colors.PRIMARY}
              maxLength={150}
            />

            <Text
              style={{
                alignSelf: "center",
                marginTop: 50,
                color: colors.PRIMARY
              }}
            >
              {en.recommend_audition}
            </Text>

            {/* <View style={{ marginTop: 10, borderColor: "red", borderWidth: 0 }}> */}
            <Autocomplete
              data={this.auditionData}
              containerStyle={{
                alignSelf: "center",
                position: "absolute",
                zIndex: 999,
                width: "100%",
                top: Platform.OS == "ios" ? 310 : 320
              }}
              defaultValue={this.state.searchText}
              hideResults={this.state.hideResults}
              onChangeText={this.onSearchText}
              renderItem={({ item, i }) => (
                <TouchableOpacity
                  // style={{
                  //   borderBottomColor: "#a9a9a9",
                  //   borderBottomWidth: 1
                  // }}
                  onPress={() => this.onItemSelect(item, i)}
                >
                  <Text
                    style={{
                      padding: 5,
                      fontSize: 16,
                      fontFamily: NEXALIGHT,
                      color: "#5f2543"
                      // marginLeft: 10
                    }}
                  >
                    {item.title}
                  </Text>
                </TouchableOpacity>
              )}
              inputContainerStyle={[{ borderWidth: 0 }]}
              listStyle={{ height: 100, marginHorizontal: 8 }}
              renderTextInput={() => {
                return (
                  <View
                    style={{
                      borderRadius: 30,
                      borderColor: colors.PRIMARY,
                      borderWidth: 2,
                      padding: 8,
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <Image
                      resizeMode="contain"
                      source={Assets.search}
                      style={{ width: 15, height: 15 }}
                    />
                    <TextInput
                      value={this.state.searchText}
                      onChangeText={this.onSearchText}
                      style={[
                        {
                          fontFamily: NEXALIGHT,
                          color: "#5f2543",
                          marginLeft: 10,
                          padding: 0
                        }
                      ]}
                      placeholderTextColor="#5f2543"
                      returnKeyType={"search"}
                      onSubmitEditing={this.onSubmitEditing}
                      placeholder={en.search_audition}
                    />
                  </View>
                );
              }}
            />

            <TouchableOpacity
              style={styles.buttonContainer}
              onPress={this.onFeedbackSubmit}
            >
              <Text style={{ fontFamily: NEXABOLD, color: colors.WHITE }}>
                {"Submit"}
              </Text>
            </TouchableOpacity>
          </TouchableOpacity>
          {/* </TouchableOpacity> */}
        </KeyboardAwareScrollView>
      </Modal>
    );
  }
}
