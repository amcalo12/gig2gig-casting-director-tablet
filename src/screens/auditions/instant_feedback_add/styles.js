import { StyleSheet, Platform ,Dimensions} from "react-native";
import colors from "../../../utils/colors";
import { resize } from "../../../assets/styles";
const DEVICE_HEIGHT = Math.round(Dimensions.get("window").height);
console.log("DEVICE HEIGHT=========>", DEVICE_HEIGHT);
 
export const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.SHADOW_COLOR
  },
  modalSubContainer: {
    borderRadius: 8,
    backgroundColor: colors.WHITE,
    shadowOpacity: 0.2,
    elevation: 5,
    width: "35%",
    //height: Platform.OS == "ios" ? "50%" : "90%",

    padding: 20
  },
  comentInput1: {
    width: "100%",
    height: resize(50, "height"),
    // marginTop: resize(30, "height"),
    flexDirection: "row",
    // paddingHorizontal: resize(10),
    borderWidth: 4,
    borderColor: "#5f2543",
    borderRadius: resize(30),
    alignItems: "center",
    alignSelf: "center",
    marginBottom: resize(10, "height")
  },
  searchIcon: {
    width: resize(30),
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonContainer: {
    backgroundColor: colors.PRIMARY,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 100,
    paddingVertical: 15,
    borderRadius: 30,
    marginHorizontal: 50
  }
});
