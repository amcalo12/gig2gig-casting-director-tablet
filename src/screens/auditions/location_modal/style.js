
import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXABOLD, height, STATUS_BAR_HEIGHT } from '../../../assets/styles';

const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT;


const styles = StyleSheet.create({
  nBContainer: {
    backgroundColor: 'transparent'
  },
  dismissButton: {
    width: '100%',
    height: containersHeight,
    position: 'absolute',
    justifyContent: 'flex-end'
  },
  blurView: {
    width: '100%',
    height: resize(Platform.OS === 'ios' ? 732 : 729, 'height'),
    backgroundColor: '#f0f0f0',
    opacity: 0.84,
  },
  cardContainer: {
    width: '100%',
    height: containersHeight,
    justifyContent: 'flex-end'
  },
  card: {
    alignSelf: 'center',
    width: resize(1012),
    justifyContent: 'space-between',
    height: resize(664, 'height'),
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: { width: 3, height: 0 },
    shadowRadius: resize(6),
    borderRadius: resize(13),
    backgroundColor: '#ffffff',
    marginBottom: resize(33, 'height'),
    flexDirection: 'row'
  },
  closeButton: {
    position: 'absolute',
    zIndex: 2,
    paddingHorizontal: resize(18),
    paddingVertical: resize(32, 'height')
  },
  mapContainer: {
    flex: 1
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%',
  },
  marker: {
    height: resize(48, 'height'),
    width: resize(48),
    resizeMode: 'contain'
  },
  buttonSaveContainer: {
    position: 'absolute',
    right: resize(10),
    bottom: resize(20, 'height'),
    width: resize(100),
    height: resize(45, 'height'),
  },
  buttonSaveGradient: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.18)',
    shadowOffset: { width: 3, height: 0 },
    shadowRadius: 8,
    borderRadius: 50,
  },
  inputSearchContainer: {
    position: 'absolute',
    zIndex: 10,
    width: '100%',
    paddingHorizontal: resize(100)
  },
  inputSearchTextInputContainer: {
    width: '100%',
    height: resize(55, 'height'),
    backgroundColor: 'transparent',
    borderBottomWidth: 0,
    borderTopWidth: 0,
    marginTop: resize(5, 'height')
  },
  inpurtSearch: {
    height: '100%',
    borderRadius: 28,
    borderColor: '#4d2545',
    borderWidth: 1,
    width: '100%',
    fontSize: resize(20),
    fontWeight: '400',
    color: '#4d2545',
    paddingLeft: resize(20),
  },
  searchListView: {
    backgroundColor: 'rgba(255,255,255, 0.9)',
    borderBottomLeftRadius: 28,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 28,
    marginTop: resize(8, 'height'),
    marginHorizontal: resize(25)
  }
});

export default styles;