import React, { Component } from 'react';
import { Modal, TouchableOpacity, View, Image } from 'react-native';
import { Container } from 'native-base';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

//custom
import styles from './style';
import icon_close from '../../../assets/icons/commons/icon_close.png';
import marker from '../../../assets/icons/auditions/marker.png';
import GradientButton from '../../../components/commons/gradient_button';

// locale
import en from '../../../locale/en';

export default class LocationModal extends Component {

  state = {
    region: {
      latitude: 37.779223846030874,
      longitude: -122.4208035402102,
      latitudeDelta: 0.09221126069288488,
      longitudeDelta: 0.17781238344312555,
    },
  }

  onChangeRegion = (region) => {
    this.setState({ region });
  }

  moveToRegion = (data, details) => {
    console.log(" DETAILS ", details)
    console.log(" DETAILS ", details)
    const latitudeDelta = Number(details.geometry.viewport.northeast.lat) - Number(details.geometry.viewport.southwest.lat)
    const longitudeDelta = Number(details.geometry.viewport.northeast.lng) - Number(details.geometry.viewport.southwest.lng)

    const region = {
      latitude: details.geometry.location.lat,
      longitude: details.geometry.location.lng,
      latitudeDelta,
      longitudeDelta
    }

    this.map.animateToRegion(region);

    return this.setState({ region });
  }

  render() {

    const {
      region
    } = this.state;
    const {
      location
    } = this.props;

    return (
      <Modal
        animationType='fade'
        animated={true}
        visible={this.props.show}
        transparent={true}
        onRequestClose={this.props.dismiss}
      >
        <Container style={styles.nBContainer}>
          <TouchableOpacity
            activeOpacity={1}
            style={styles.dismissButton}
            onPress={this.props.dismiss}
          >
            <View style={styles.blurView} />
          </TouchableOpacity>
          <View style={styles.cardContainer}>
            <View style={styles.card}>
              <TouchableOpacity
                style={styles.closeButton}
                onPress={this.props.dismiss}
              >
                <Image source={icon_close} />
              </TouchableOpacity>
              <View style={styles.mapContainer}>
                <GooglePlacesAutocomplete
                  autoFocus={false}
                  placeholder='Search'
                  minLength={2}
                  returnKeyType='default'
                  keyboardAppearance='light'
                  listViewDisplayed='false'
                  fetchDetails={true}
                  renderDescription={row => row.description}
                  onPress={(data, details = null) => this.moveToRegion(data, details)}
                  getDefaultValue={() => ''}
                  query={{
                    key: 'AIzaSyCBwvwOsPR82AjeUx5o3FUvr4syuoNFrLI',
                    language: 'en'
                  }}
                  styles={{
                    container: styles.inputSearchContainer,
                    textInputContainer: styles.inputSearchTextInputContainer,
                    textInput: styles.inpurtSearch,
                    listView: styles.searchListView
                  }}
                  nearbyPlacesAPI='GooglePlacesSearch'
                  GooglePlacesSearchQuery={{
                    rankby: 'distance',
                    type: 'cafe'
                  }}
                  GooglePlacesDetailsQuery={{
                    fields: 'formatted_address',
                  }}
                  filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                  debounce={200}
                />
                <MapView
                  style={styles.mapContainer}
                  provider={PROVIDER_GOOGLE}
                  initialRegion={location != null ? location : region}
                  onRegionChangeComplete={(region) => this.onChangeRegion(region)}
                  zoomControlEnabled
                  zoomEnabled
                  loadingEnabled
                  loadingIndicatorColor='#4d2545'
                  ref={(map) => this.map = map}
                />
                <View style={styles.markerFixed}>
                  <Image
                    style={styles.marker}
                    source={marker}
                  />
                </View>
              </View>
              <GradientButton
                buttonText={en.save_button_text}
                onPress={() => this.props.agree(region)}
                containerStyle={styles.buttonSaveContainer}
                gradientStyle={styles.buttonSaveGradient}
              />
            </View>
          </View>
        </Container>
      </Modal>
    );
  }
}

