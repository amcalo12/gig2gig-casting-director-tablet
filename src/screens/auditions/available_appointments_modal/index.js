import React from 'react';
import { View, TouchableOpacity, Text, Image, Modal, ScrollView } from 'react-native';
import { Container, Content } from 'native-base';
import moment from 'moment';

//custom
import styles from './style';
import icon_close from '../../../assets/icons/commons/icon_close.png';
import GradientButton from '../../../components/commons/gradient_button';
import AppointmentRow from '../../../components/auditions/appointment_management_modal/appointment_row';
import clockIcon from '../../../assets/icons/auditions/clock-icon.png';

//locale
import en from '../../../locale/en';

export default function AvailableAppointmentsModal(props) {
  return (
    <Modal
      animationType='fade'
      animated={true}
      visible={props.show}
      transparent={true}
      onRequestClose={props.dismiss}
    >
      <Container style={styles.nBContainer}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.dismissButton}
          onPress={props.dismiss}
        >
          <View style={styles.blurView} />
        </TouchableOpacity>
        <Content bounces={false}>
          <View style={styles.cardContainer}>
            <View style={styles.card}>
              <TouchableOpacity
                style={styles.closeButton}
                onPress={props.dismiss}
              >
                <Image source={icon_close} />
              </TouchableOpacity>
              <View style={styles.leftContainer}>
                <Text style={styles.titleLeftContainer}>
                  {en.mark_as_walk}
                </Text>
                <ScrollView style={styles.appointmentScroll}>
                  {
                    props.appointments.length > 0 && (
                      props.appointments.map((item, index) => {
                        return (
                          <AppointmentRow
                            numberRow={item.number}
                            time={item.time}
                            walkInAppointment={item.is_walk === 1 ? true : false}
                            disabled
                            key={index}
                          />
                        )
                      })
                    )
                  }
                </ScrollView>
              </View>
              <View style={styles.rightContainer}>
                <Text style={styles.titleSection}>
                  {en.appointment_slots}
                </Text>
                <View
                  style={styles.gradientContainer}
                >
                  <View style={styles.appointmentNumberContainer}>
                    {console.log('props.details.slots :', props.details)}
                    <Text style={styles.appointmentNumberText}>
                      {props.details.slots}
                    </Text>
                  </View>
                  <GradientButton
                    activeOpacity={1}
                    containerStyle={styles.gradientLabelContainer}
                    gradientStyle={styles.gradientLabelContainer}
                    buttonText={en.appointments}
                    disabled
                  />
                </View>
                <Text style={styles.titleSection}>
                  {en.appointments_type}
                </Text>
                <View style={styles.inputsContainer}>
                  <View style={styles.infoContainer}>
                    <Text style={styles.text}>
                      {props.details.type}
                    </Text>
                  </View>
                </View>
                <Text style={styles.titleSection}>
                  {en.appointments_length}
                </Text>
                <View style={styles.inputsContainer}>
                  <View style={styles.infoContainer}>
                    <Text style={styles.text}>
                      {`${props.details.length} ${en.minutes}`}
                    </Text>
                  </View>
                </View>
                <Text style={styles.titleSection}>
                  {en.start_time}
                </Text>
                <View style={styles.inputsContainer}>
                  <View style={styles.infoContainer}>
                    <Text style={styles.text}>
                      {moment(props.details.start, 'hh:mm').format('LT')}
                    </Text>
                    <View style={styles.iconContainer}>
                      <Image
                        source={clockIcon}
                        style={styles.icon}
                      />
                    </View>
                  </View>
                </View>
                <Text style={styles.titleSection}>
                  {en.end_time}
                </Text>
                <View style={styles.inputsContainer}>
                  <View style={styles.infoContainer}>
                    <Text style={styles.text}>
                      {moment(props.details.end, 'hh:mm').format('LT')}
                    </Text>
                    <View style={styles.iconContainer}>
                      <Image
                        source={clockIcon}
                        style={styles.icon}
                      />
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Content>
      </Container>
    </Modal>
  );
}

AvailableAppointmentsModal.defaultProps = {
  show: false,
  dismiss: () => { },
  details: {},
  appointments: []
}