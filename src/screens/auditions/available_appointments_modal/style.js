import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXABOLD, height, STATUS_BAR_HEIGHT } from '../../../assets/styles';

const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT;

const styles = StyleSheet.create({
  nBContainer: { backgroundColor: 'transparent' },
  dismissButton: {
    width: '100%',
    height: containersHeight,
    position: 'absolute',
    justifyContent: 'flex-end'
  },
  blurView: {
    width: '100%',
    height: resize(Platform.OS === 'ios' ? 732 : 729, 'height'),
    backgroundColor: '#f0f0f0',
    opacity: 0.84,
  },
  cardContainer: {
    width: '100%',
    height: containersHeight,
    justifyContent: 'flex-end'
  },
  card: {
    alignSelf: 'center',
    width: resize(1012),
    justifyContent: 'space-between',
    height: resize(664, 'height'),
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: { width: 3, height: 0 },
    shadowRadius: resize(6),
    borderRadius: resize(13),
    backgroundColor: '#ffffff',
    marginBottom: resize(33, 'height'),
    flexDirection: 'row'
  },
  closeButton: {
    position: 'absolute',
    zIndex: 2,
    paddingHorizontal: resize(18),
    paddingVertical: resize(21, 'height')
  },
  leftContainer: {
    width: resize(623)
  },
  appointmentScroll: {
    paddingLeft: resize(161),
    paddingRight: resize(57)
  },
  titleLeftContainer: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(10),
    fontWeight: '400',
    letterSpacing: resize(0.5),
    marginTop: resize(27, 'height'),
    marginLeft: resize(500),
    textAlign: 'center',
    marginBottom: resize(9, 'height')
  },
  rightContainer: {
    paddingTop: resize(19, 'height'),
    borderLeftWidth: 1,
    borderColor: '#707070',
    width: resize(388),
    alignItems: 'center'
  },
  titleSection: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(17),
    fontWeight: '400',
    letterSpacing: resize(0.85),
    marginTop: resize(Platform.OS === 'ios' ? 23 : 17, 'height')
  },
  gradientContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: resize(14, 'height'),
    width: resize(256),
    height: resize(51, 'height'),
    borderColor: '#4d2545',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: resize(28),
  },
  inputError: {
    borderColor: 'red'
  },
  inputsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: resize(14, 'height'),
    width: resize(256),
    height: resize(51, 'height'),
  },
  appointmentNumberContainer: {
    textAlignVertical: 'center',
    flex: 1,
    padding: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  appointmentNumberText: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(29),
    fontWeight: '400',
    letterSpacing: resize(1.45),
    textAlign: 'center',
  },
  gradientLabelContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: resize(164),
    height: resize(50, 'height'),
    borderTopRightRadius: resize(26),
    borderBottomRightRadius: resize(25),
  },
  gradientButton: {
    width: resize(257),
    height: resize(51, 'height'),
    shadowColor: 'rgba(0, 0, 0, 0.4)',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: resize(3),
    borderRadius: resize(28),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: resize(13, 'height')
  },
  disabledInput: {
    backgroundColor: '#FFF'
  },
  infoContainer: {
    width: '100%',
    height: '100%',
    borderRadius: 28,
    borderColor: '#4d2545',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center'
  },
  text: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
    paddingLeft: resize(21),
  },
  inputTime: {
    width: '100%',
    height: resize(55, 'height'),
    borderRadius: 28,
    borderColor: '#4d2545',
    borderWidth: 1,
  },
  inputTimeError: {
    width: '100%',
    height: resize(55, 'height'),
    borderRadius: 28,
    borderColor: 'red',
    borderWidth: 1,
  },
  iconContainer: {
    position: 'absolute',
    right: resize(18),
    top: resize(16, 'height'),
    zIndex: 2
  },
  icon: {
    width: resize(18),
    height: resize(19, 'height'),
    resizeMode: 'contain'
  }
});

export default styles;