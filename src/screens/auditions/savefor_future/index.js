import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity, Image } from 'react-native';
import en from '../../../locale/en';
import { styles } from './styles';
import { NEXABOLD, resize } from '../../../assets/styles';
import colors from '../../../utils/colors'
import { Assets } from '../../../assets';

export default class SaveForFutureModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Modal
                // animationType="slide"
                animationType="none"
                transparent={true}
                visible={this.props.isVisible}
                onRequestClose={this.props.onCloseClick}>
                <TouchableOpacity style={styles.modalContainer} onPress={this.props.onCloseClick} activeOpacity={1}>
                    <View style={styles.modalSubContainer} activeOpacity={1}>
                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                            <Image source={Assets.close} style={{ position: "absolute", left: 5 }} />
                            <Text style={{ color: colors.PRIMARY, fontSize: resize(18), }}>{en.instant_feeback_title}</Text>
                        </View>

                        <Text style={styles.textInfo}>{"Performers who have been “saved for future” will receive the message:"}</Text>
                        <View style={styles.messageContainer}>
                            <Text style={styles.messageText}>{this.props.message}</Text>
                        </View>
                        <Text style={styles.textInfo}>{en.change_feedback_text}</Text>

                        <TouchableOpacity style={styles.buttonContainer} onPress={this.props.doneClick}>
                            <Text style={{ fontFamily: NEXABOLD, color: colors.WHITE }}>{"Done"}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.props.dontShowClick}>
                            <Text style={[styles.textInfo, { fontSize: resize(12), marginTop: 10 }]}>{en.dont_show_text}</Text>
                        </TouchableOpacity>
                    </View>
                </TouchableOpacity>
            </Modal>
        );
    }
}
