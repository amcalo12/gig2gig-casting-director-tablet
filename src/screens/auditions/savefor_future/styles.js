import { StyleSheet, Platform } from "react-native";
import colors from "../../../utils/colors";
import { NEXALIGHT, NEXABOLD } from "../../../assets/styles";

export const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: colors.SHADOW_COLOR
    },
    modalSubContainer: {
        borderRadius: 8,
        backgroundColor: colors.WHITE,
        shadowOpacity: 0.2,
        elevation: 5,
        width: "35%",
        //height: Platform.OS == "ios" ? "50%" : "90%",
        padding: 10,
        justifyContent: "center"
    },
    textInfo: {
        marginTop: 30,
        fontFamily: NEXALIGHT,
        color: colors.PRIMARY,
        textAlign: "center",
        alignSelf: "center",
        paddingHorizontal: 30,
        lineHeight: 18
    },
    messageContainer: {
        borderRadius: 10,
        backgroundColor: "#f5f5f5",
        marginTop: 30,
        marginHorizontal: 30
    },
    messageText: {
        color: colors.PRIMARY,
        textAlign: "center",
        alignSelf: "center",
        lineHeight: 18,
        backgroundColor: "#f5f5f5",
        padding: 10,
        fontFamily: NEXABOLD
    },
    buttonContainer: {
        backgroundColor: colors.PRIMARY,
        marginHorizontal: 50,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 10,
        borderRadius: 20,
        marginTop: 20
    }
});
