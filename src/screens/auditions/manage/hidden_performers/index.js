import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native';
import { Container } from 'native-base';
import Search from '../../../../components/commons/search';
import HeaderButton from '../../../../components/commons/header_button';
import BackButton from '../../../../components/commons/back_button';
import HeaderText from '../../../../components/commons/header_text';
import Header from '../../../../components/commons/header';
import Partaker from '../../../../components/auditions/partaker';
import en from '../../../../locale/en';
import showMessage from '../../../../utils/show_message';
import { getHiddenPerformer, restoreHiddenPerformer } from '../../../../api/auditions';
import { withNavigationFocus } from "react-navigation";
import { NEXABOLD, resize, NEXALIGHT } from '../../../../assets/styles';
import { connect } from "react-redux";
import styles from './style';
import { Assets } from '../../../../assets';
import colors from '../../../../utils/colors';
import parseError from '../../../../utils/parse_error';
import ProgressLoader from '../../../../components/commons/progress_loading';
import { restorePerformer } from '../../../../utils/constant';

class HiddenPerformers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            performerList: undefined,
            actualRound: this.props.navigation.state.params.round,
            audition_id: this.props.navigation.state.params.audition_id,
            status: this.props.navigation.state.params.status,
            refreshing: false,
            isLoading: false
        };
    }

    async componentDidMount() {
        this.getHiddenPerformer()
    }

    refreshPartakers = async () => {
        this.setState({ refreshing: true });
        this.getHiddenPerformer();
    };

    getHiddenPerformer = async () => {
        let { actualRound } = this.state;
        this.setState({ isLoading: true })
        try {
            const hiddenPerformer = await getHiddenPerformer(actualRound.id);
            // let temp = [...hiddenPerformer.data.data, ...hiddenPerformer.data.data, ...hiddenPerformer.data.data, ...hiddenPerformer.data.data, ...hiddenPerformer.data.data]
            this.setState({ performerList: hiddenPerformer.data.data, isLoading: false })
        } catch (error) {
            console.log(" ERROR hiddenPerformer ", error);
            this.setState({ isLoading: false })
            const parsedError = parseError(error);
            showMessage("danger", parsedError);
        }
    }

    restorePerformer = async (item, index) => {
        this.setState({ isLoading: true })
        try {
            let params = {
                user: item.user_id,
                appointment_id: this.state.actualRound.id
            }
            const hiddenPerformer = await restoreHiddenPerformer(params);
            this.getHiddenPerformer();
            this.setState({ isLoading: false })
        } catch (error) {
            console.log(" ERROR hiddenPerformer ", error);
            this.setState({ isLoading: false })
            const parsedError = parseError(error);
            showMessage("danger", parsedError);
        }
    }

    restoreConfirmation = (item, index) => {
        restorePerformer(() => {
            this.restorePerformer(item, index)
        })
    }

    goBack = () => {
        this.props.navigation.goBack()
    }
    render() {
        return (
            <Container>
                <Header
                    left={<BackButton onPress={() => this.goBack()} />}
                    center={<HeaderText title={""} />}
                />
                {this.state.isLoading ? <ProgressLoader isLoading={true} /> : null}
                {this.state.performerList != undefined && this.state.performerList.length > 0 ? 
                <FlatList
                    contentContainerStyle={{ padding: 10 }}
                    numColumns={5}
                    data={this.state.performerList}
                    extraData={this.state}
                    keyExtractor={(item, index) => item + index}
                    renderItem={({ item, index }) => (
                        <View style={styles.container}>
                            <View style={styles.imageContainer}>
                                <Image source={{ uri: item.image }} style={styles.image} />
                            </View>

                            <Text style={styles.name} numberOfLines={1}>
                                {item.name}
                            </Text>

                            <TouchableOpacity style={styles.buttonContainer} onPress={() =>
                                this.restoreConfirmation(item, index)
                            }>
                                <Image source={Assets.restore} resizeMode={"contain"}
                                    style={{ paddingLeft: 10, height: 15, width: 15 }} />

                                <Text style={styles.buttonText} numberOfLines={1}>
                                    {en.restore}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    )}
                    refreshing={this.state.refreshing}
                    onRefresh={this.refreshPartakers}
                    showsVerticalScrollIndicator={false}
                /> :
                    this.state.performerList != undefined && this.state.performerList.length == 0 ?
                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontFamily: NEXABOLD, color: colors.PRIMARY }}>
                                {en.no_performances}
                            </Text>
                        </View>
                        : null}
            </Container>
        );
    }
}

const mapStateToProps = state => {
    console.log(" REDUX DATA CREATE GROUP=========>", state);

    return {
        user: state.user.data,
        feedback: state.user.instantFeedback
    };
};

export default withNavigationFocus(connect(mapStateToProps)(HiddenPerformers));