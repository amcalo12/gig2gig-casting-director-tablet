import { StyleSheet, Platform } from 'react-native';
import { resize, NEXABOLD, height, STATUS_BAR_HEIGHT, NEXALIGHT } from '../../../../assets/styles';
import colors from '../../../../utils/colors';

const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT;

const styles = StyleSheet.create({
    container: {
        width: resize(200),
        flexGrow: 0,
        flexShrink: 1,
        flexBasis: "auto",
        marginBottom: resize(30, "height"),
        margin: 10,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: "#d6d6d6"
    },
    imageContainer: {
        width: "100%",
        height: resize(154, "height")
    },
    image: {
        resizeMode: "cover",
        width: "100%",
        height: "100%",
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
    },
    name: {
        color: "#4d2545",
        fontFamily: NEXABOLD,
        fontSize: resize(16),
        fontWeight: "400",
        marginTop: 10,
        marginLeft: 5,
        paddingBottom: resize(1, "height")
    },
    buttonContainer: {
        flexDirection: "row", backgroundColor: colors.LIGHT_GREY,
        alignItems: "center", padding: 10, justifyContent: "flex-end", alignSelf: "center",
        width: "70%", borderRadius: 8, marginTop: 10,
        marginBottom: 10
    },
    buttonText: {
        color: "#4d2545",
        fontFamily: NEXALIGHT,
        letterSpacing: 2,
        fontSize: resize(14), marginLeft: 8
    }
})
export default styles;