import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
} from "react-native";
import { Container } from "native-base";
import Header from "../../../../components/commons/header";
import BackButton from "../../../../components/commons/back_button";
import HeaderText from "../../../../components/commons/header_text";
import colors from "../../../../utils/colors";
import { NEXABOLD, NEXALIGHT } from "../../../../assets/styles";
import GradientButtom from "../../../../components/commons/gradient_button";
import { gradient, gradientLight, resize } from "../../../../assets/styles";
import styles from "./style";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import showMessage from "../../../../utils/show_message";
import ProgressLoader from "../../../../components/commons/progress_loading";
import { sendPushNotification } from "../../../../api/auditions";
import parseError from "../../../../utils/parse_error";

export default class PushNotification extends Component {
  constructor(props) {
    super(props);
    this.auditionId = this.props.navigation.state.params.data.auditions_id;
    this.state = {
      title: "",
      message: "",
      loadingButton: false,
      isLoading: false,
    };
  }

  onTitleChange = (title) => {
    this.setState({ title });
  };

  onMessageChange = (message) => {
    this.setState({ message });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  onSend = async () => {
    Keyboard.dismiss();
    if (this.state.title == "") {
      showMessage("danger", "Please enter title");
      return;
    } else if (this.state.message == "") {
      showMessage("danger", "Please enter message");
      return;
    }
    this.setState({ isLoading: true });
    let params = {
      title: this.state.title,
      message: this.state.message,
      audition_id: this.auditionId,
    };
    try {
      const response = await sendPushNotification(params);
      showMessage("success", response.data.data);
      this.setState({ isLoading: false, title: "", message: "" });
    } catch (error) {
      this.setState({ isLoading: false, title: "", message: "" });
      const hasError = parseError(error);
      showMessage("danger", hasError);
    }
  };

  render() {
    return (
      <Container>
        <Header
          left={<BackButton onPress={() => this.goBack()} />}
          center={<HeaderText title={"Push Notifications"} />}
        />
        {this.state.isLoading ? <ProgressLoader isLoading={true} /> : null}
        <KeyboardAwareScrollView
          contentContainerStyle={{ flexGrow: 1, margin: 40 }}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={"always"}
          enableOnAndroid
          scrollEnabled
        >
          {/* <View style={{ margin: 40 }}> */}
          <Text
            style={{
              marginLeft: 10,
              marginTop: 20,
              fontFamily: NEXALIGHT,
              color: colors.PRIMARY,
              fontSize: 16,
            }}
          >
            {/* {"Notification will be sent to all users who have acquire an appointment for this audition"} */}
            {
              "Notifications will be sent all users who have appointment for this audition"
            }
          </Text>

          <TextInput
            placeholder={"Message Title"}
            style={{
              marginTop: 40,
              borderRadius: 28,
              borderWidth: 1,
              borderColor: colors.PRIMARY,
              padding: 20,
              fontFamily: NEXABOLD,
              fontSize: 16,
            }}
            value={this.state.title}
            onChangeText={this.onTitleChange}
            placeholderTextColor={colors.PRIMARY}
          />

          <TextInput
            placeholder={"Message"}
            style={{
              marginTop: 20,
              borderRadius: 24,
              borderWidth: 1,
              borderColor: colors.PRIMARY,
              padding: 20,
              paddingTop: 20,
              fontFamily: NEXABOLD,
              fontSize: 16,
              height: 150,
              textAlignVertical: "top",
            }}
            value={this.state.message}
            multiline={true}
            onChangeText={this.onMessageChange}
            placeholderTextColor={colors.PRIMARY}
          />

          <GradientButtom
            buttonText={"Send"}
            colors={gradient}
            containerStyle={styles.gradientButtonContainer}
            gradientStyle={styles.gradientButtonSave}
            textStyle={styles.gradientButtonTextSave}
            onPress={this.onSend}
            loading={this.state.loadingButton}
          />
          {/* </View> */}
        </KeyboardAwareScrollView>
        {/* </KeyboardAvoidingView> */}
      </Container>
    );
  }
}
