import { StyleSheet } from 'react-native';

//custom
import { width, NEXABOLD, NEXALIGHT, resize } from '../../../../assets/styles';

const styles = StyleSheet.create({
    gradientButtonContainer: {
        width: resize(225),
        height: resize(49, 'height'),
        alignSelf: 'flex-end',
        marginTop: 20
    },
    gradientButtonSave: {
        height: resize(49, 'height'),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
    },
    gradientButtonTextSave: {
        color: '#ffffff',
        fontFamily: NEXABOLD,
        fontSize: resize(18),
        fontWeight: '400',
    }
});

export default styles;