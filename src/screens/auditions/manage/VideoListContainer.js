//import liraries
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Modal,
  Share,
  Platform
} from "react-native";
import styles, { headerHeight } from "./style";
import { Button, Content } from "native-base";
import File from "../../../components/auditions/file";
import { resize } from "../../../assets/styles";
import { deleteVideo, renameAuditionVideo } from "../../../api/auditions";
import parseError from "../../../utils/parse_error";
import showMessage from "../../../utils/parse_error";
import { Assets } from "../../../assets";
import RenameModal from "../../../components/auditions/rename_file";

const DEVICE_HEIGHT = Math.round(Dimensions.get("window").height);

// create a component
class VideoListContainer extends Component {
  constructor(props) {
    super(props);
    this.currentItem = null
    this.positionList = [];
    this.feedPost = [];
    this.positionX = 0;
    this.positionY = 0;
    this.state = {
      ModalVisibleStatus: false,
      message: "Default Message...",
      currObj: null,
      isShowPopup: false,
      isVisible: false,
      fileName: ""
    };
    console.log("NAVIGATION =====>", this.props);
  }

  componentWillReceiveProps(props) {
    console.log(" check props 111", props);
    this.props.videoList = props.videoList;
    this.setState({ isShowPopup: props.showPopup });

    // this.setState({ ModalVisibleStatus: props.isShwModal })
  }

  shareTextMessage(message, item) {
    Share.share(
      {
        message: "Video shared from Gig2Gig Casting " + this.state.currObj.url,
        url: "Video shared from Gig2Gig Casting " + this.state.currObj.url, //'http://bam.tech',
        title: "Video shared from Gig2Gig Casting"
      },
      {
        // Android only:
        dialogTitle: "Share BAM goodness",
        // iOS only:
        excludedActivityTypes: ["com.apple.UIKit.activity.PostToTwitter"]
      }
    );
  }

  // call delete video api
  deleteVideo = async () => {
    console.log("this.state.currObj", this.state.currObj);
    console.log("this.state.currObj", this.props.auditionID);

    this.setState({ loading: true });

    try {
      let resquestItems = await deleteVideo(
        this.state.currObj.id,
        this.props.auditionID
      );

      console.log("DELETED VIDEO RES ==> " + JSON.stringify(resquestItems));

      this.setState({ loading: false });

      this.props.onVideoDeleteSuccess();
    } catch (error) {
      console.log(error.response);
      this.setState({ loading: false });
      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }
    this.ShowModalFunction(false);
  };

  ShowModalFunction(visible) {
    // this.setState({ ModalVisibleStatus: visible });
    this.setState({ isShowPopup: false });
  }

  onCloseClick = () => {
    this.setState({ isVisible: false })
  }

  onSubmitClick = async () => {
    this.setState({ loading: true });
    try {
      let params = {
        id: this.state.currObj.id,
        name: this.state.fileName,
        audition_id: this.props.auditionID
      }
      const response = await renameAuditionVideo(params)
      console.log('response :', response);
      this.props.onVideoDeleteSuccess();
      this.setState({ loading: false, isVisible: false });
    } catch (error) {
      this.setState({ loading: false, isVisible: false });
      console.log(error.response);
      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }
    // this.setState({ isVisible: false })
  }

  onChangeText = (fileName) => {
    this.setState({ fileName })
  }

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    )
  }

  showPopup() {
    // console.log(" CHECK Y POSITION ", this.positionY);
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "flex-end",
          position: "absolute",
          left: resize(150),
          //top: 40,
          width: resize(140),
          height: resize(70),
          top: (this.positionY),
        }}
      >
        <View
          style={{
            //height:150,
            height: resize(170),
            // right: "10%",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "white",
            // width: 190,
            width: resize(140),
            borderRadius: 10,
            borderWidth: 1,
            position: "absolute",
            borderColor: "white"
          }}
        >
          <View>
            {/* {SHARE} */}
            <TouchableOpacity
              style={styles.ShareStyle}
              activeOpacity={0.5}
              onPress={() => {
                this.shareTextMessage(
                  this,
                  this.state.message,
                  this.state.currObj
                );
                //alert(JSON.stringify(this.state.currObj))
                setTimeout(() => {
                  this.ShowModalFunction(false);
                }, 5000);
              }}
            >
              <Image
                source={require("../../../assets/icons/auditions/share-icon.png")}
                style={styles.ImageIconStyle}
              />
              <Text style={styles.TextStyle}> Share </Text>
            </TouchableOpacity>

            {/* {OPEN IN} */}
            <TouchableOpacity
              style={styles.OpenIn}
              activeOpacity={0.5}
              onPress={() => {
                console.log(" CHECK LIST PROPS ", this.props);
                const { navigation } = this.props;
                //Pass dynamic video URL
                navigation.navigate("VideoPlayerOpen", {
                  video_url: this.state.currObj.url
                    ? this.state.currObj.url
                    : ""
                });
                setTimeout(() => {
                  this.ShowModalFunction(false);
                }, 5000);
              }}
            >
              <Image
                source={require("../../../assets/icons/auditions/open-in-icon.png")}
                style={styles.ImageIconStyle}
              />
              <Text style={styles.TextStyle}> Open in </Text>

              {/* {DELETE} */}
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.Delete}
              activeOpacity={0.5}
              onPress={() => {
                this.deleteVideo();
              }}
            >
              <Image
                source={require("../../../assets/icons/auditions/trash-icon.png")}
                style={styles.ImageIconStyle}
              />
              <Text style={styles.TextStyle}> Delete </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.Delete}
              activeOpacity={0.5}
              onPress={() => {
                console.log('this.state.current :', this.state.currObj);
                this.setState({ isVisible: true, fileName: this.state.currObj.name })
              }}
            >
              <Image
                source={Assets.rename}
                style={[styles.ImageIconStyle, { width: 15, height: 15 }]}
              />
              <Text style={styles.TextStyle}> Rename </Text>
            </TouchableOpacity>
          </View>
          <Button
            title="Click Here To Hide Modal"
            onPress={() => {
              this.ShowModalFunction(!this.state.ModalVisibleStatus);
            }}
          />
        </View>
      </View>
    );
  }
  // handlePressBackButton = () => {
  // 	const { navigation } = this.props;
  // 	navigation.goBack();
  // }
  onPressBackToTabs() { }

  render() {
    var array = [...this.props.videoList, ...this.props.videoList];
    console.log(" check props ===============", this.props);
    // let showPopUp = this.props.isShowPopup
    return (
      <View style={[styles.secondSection, { flex: 1 }]}>
        {this.renderRenameModal()}
        {this.props.showAuditionVideos && (
          <View style={styles.auditionVideosContainer}>
            <View style={styles.auditionVideosHeader}>
              <TouchableOpacity
                onPress={() => this.onPressBackToTabs()}
                style={styles.iconArrowLeftContainer}
              >
                <Image style={styles.iconArrowLeft} source={ArrowLeft} />
              </TouchableOpacity>
              <View style={styles.auditionVideosTitleContainer}>
                <Text style={styles.auditionVideosTitle}>
                  {this.props.title.audition_videos_title}
                </Text>
              </View>
            </View>

            <Content style={styles.auditionVideosContent}>
              <View style={styles.videosContainer}>
                {this.props.auditionVideos.length > 0 &&
                  this.props.auditionVideos.map((file, index) => {
                    return (
                      <File
                        key={index}
                        refs={fileRef => this.handleSetRef(fileRef, file)}
                        collapsable={false}
                        filename={file.name}
                        fileType={file.type}
                        file={file}
                        onPressMore={() => this.onPressFileMore(file, true)}
                      />
                    );
                  })}
                {this.props.auditionVideos.length === 0 && (
                  <Text style={styles.emptyText}>
                    {this.props.title.no_videos}
                  </Text>
                )}
              </View>
            </Content>
          </View>
        )}
        {/* <View style={{
            position: "absolute", justifyContent: "center",
            alignItems: "center", width: resize(416),
            borderColor: "red", borderWidth: 2,
          }}> */}
        {/* {this.openModal1()} */}
        {/* </View> */}
        <View style={{ height: 65, width: "100%", flexDirection: "row" }}>
          <TouchableOpacity
            style={[styles.backButtonStyle, { zIndex: 9999999 }]}
            onPress={() => {
              this.props.handleBackPress();
            }}
          >
            <Image
              style={{ resizeMode: "contain", height: 20, width: 20 }}
              source={require("../../../assets/icons/auditions/arrow-left-icon.png")}
            />
          </TouchableOpacity>
          <View style={styles.viewVideo}>
            <Text style={styles.manageVideoText}>Audition Videos</Text>
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.props.videoList}
            contentContainerStyle={{ paddingBottom: 50 }}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => item + index}
            renderItem={({ item, index }) => (
              <View
                collapsable={false}
                ref={view => {
                  this.feedPost[index] = view;
                }}
                style={{
                  paddingHorizontal: 15,
                  paddingVertical: 10
                }}
              >
                <View
                  style={{
                    height: 50,
                    width: "90%",
                    flexDirection: "row",
                    marginTop: 25,

                    alignSelf: "center",
                    marginLeft: 0
                  }}
                >
                  <TouchableOpacity
                    style={{
                      height: 70,
                      backgroundColor: "white",
                      flexDirection: "row",
                      width: "100%",
                      // borderColor: "gray",
                      // borderWidth: 1,
                      overflow: "hidden",
                      borderRadius: 10
                    }}
                    onPress={event => {
                      // console.log(" CHECK EVENT POSITION ", event.nativeEvent)
                      // { this.openModal1() }
                      console.log("item=====>" + JSON.stringify(item)),
                        console.log("index=====>" + JSON.stringify(index));

                      this.feedPost[index].measure(
                        (fx, fy, width, height, px, py) => {
                          console.log("Component width is:===> " + width);
                          console.log("Component height is:===> " + height);
                          console.log("X offset to page:===> " + px);
                          console.log("Y offset to page:===> " + py);
                          console.log(" Page index:===> " + index);

                          console.log(" DEVICE HEIGHT: " + DEVICE_HEIGHT);
                          console.log(" HEADER HEIGHT: " + headerHeight);

                          // this.positionX = px;
                          if (DEVICE_HEIGHT < py + headerHeight + 55) {
                            this.positionY = py - (100 + 55);
                          } else {
                            this.positionY = py - 40;
                          }

                          this.setState({ currObj: item, isShowPopup: true });
                        }
                      );

                      //alert(JSON.stringify(item))
                      // alert(JSON.stringify(index))
                    }}
                  // onPress = {() =>{
                  // 	let temp = this.state.videoList;
                  // 	temp[index].isVisible = true;
                  // 	this.setState({videoList:temp})
                  // 	}}
                  >
                    {console.log('item :', item)}
                    {item.thumbnail == null ? <View
                      style={{
                        height: "100%",
                        width: 65,
                        backgroundColor: "#4d2545",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center"
                      }}
                    >
                      <Image
                        style={{
                          resizeMode: "center",
                          height: 40,
                          width: 40
                          // backgroundColor: "#4d2545"
                        }}
                        source={require("../../../assets/icons/auditions/mp4Icon.png")}
                      />
                    </View> :
                      <Image
                        style={{
                          resizeMode: "center",
                          height: "100%",
                          width: 65,
                          // backgroundColor: "#4d2545"
                        }}
                        source={{ uri: item.thumbnail }}
                      />
                    }
                    <View
                      style={{
                        alignItems: "center",
                        justifyContent: "center",
                        alignSelf: "center"
                      }}
                    >
                      <Text style={{ color: "#4d2545", marginStart: 30 }}>
                        {item.name ? item.name : ""}
                      </Text>
                    </View>
                    <Image
                      style={{
                        resizeMode: "center",
                        height: 20,
                        width: 45,
                        marginBottom: 15,
                        alignSelf: "flex-end",
                        right: 0,
                        position: "absolute"
                      }}
                      source={require("../../../assets/icons/auditions/more-icon.png")}
                    />
                  </TouchableOpacity>
                  {/* {this.openModal1()} */}

                  {/* {(item.isVisible) && this.showModal(item)}   */}
                </View>
              </View>
            )}
          />
          {this.state.isShowPopup ? this.showPopup() : null}
        </View>
      </View>
    );
  }
}

//make this component available to the app
export default VideoListContainer;
