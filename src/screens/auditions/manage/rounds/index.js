import React, { Component } from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import { Container, Content } from 'native-base';
import moment from 'moment';
import LightBar from '../../../../components/commons/light_bar';
import Header from '../../../../components/commons/header';
import HeaderText from '../../../../components/commons/header_text';
import HeaderBorderedButton from '../../../../components/commons/header_bordered_button';
import en from '../../../../locale/en';
import BackButton from '../../../../components/commons/back_button';

import GradientButton from '../../../../components/commons/gradient_button';
import AppointmentRow from '../../../../components/auditions/appointment_management_modal/appointment_row';
import BorderedSelect from '../../../../components/auditions/bordered_select';
import BorderedTimepicker from '../../../../components/auditions/bordered_timepicker';
import BorderedDatepicker from '../../../../components/auditions/bordered_datepicker';
import LocationButton from '../../../../components/auditions/location_button';
import LocationModal from '../../location_modal';
import { createNewRound } from '../../../../api/rounds';
import parseError from '../../../../utils/parse_error';
import showMessage from '../../../../utils/show_message';

import uncheck from '../../../../assets/icons/commons/unCheck.png';
import check from '../../../../assets/icons/commons/check.png';

class Round extends Component {
	constructor(props) {
		super(props);
		let { navigation } = this.props;
		console.log(" this.props.timeInterval ", this.props.timeInterval)
		this.appointmentArray = [{ label: "3 minutes", value: 3 }, { label: "5 minutes", value: 5 },]
		this.appointmentArray = [...this.appointmentArray, ...this.props.timeInterval]
		console.log(" this.appointmentArray ", this.appointmentArray)
		this.state = {
			audition_id: navigation.getParam('audition_id', null),
			round: navigation.getParam('round', 0),
			startTime: null,
			endTime: null,
			type: null,
			length: null,
			appointmentNumber: null,
			appointmentNumberError: false,
			typeError: false,
			lengthError: false,
			startTimeError: false,
			date: null,
			time: null,
			location: null,
			loading: false,
			appointment: [],
			isSelectAll: false,
			showLocationModal: false
		}
	}

	handlePressBackButton = () => {
		const { navigation } = this.props;
		navigation.goBack();
	}

	markRow = (value, index) => {
		let { appointment } = this.state;
		appointment[index].is_walk = value;
		this.setState({ appointment });
	}

	onSelectAll = () => {
		let { appointment } = this.state;
		this.state.isSelectAll = !this.state.isSelectAll
		appointment.map(data => data.is_walk = this.state.isSelectAll)

		this.setState({ appointment: appointment })
	}


	handleChangeStartTime = (time) => {
		var coeff = 1000 * 60 * 10;
		var date = new Date('02/02/2019 ' + time);
		var rounded = new Date(Math.round(date.getTime() / coeff) * coeff);
		this.setState(
			{
				startTime: rounded
			},
			() => this.handleCreateAppointments()
		);
	}

	handleChangeEndTime = (time = null, update = true) => {
		var coeff = 1000 * 60 * 10;
		var date = new Date('02/02/2019 ' + time);
		var rounded = new Date(Math.round(date.getTime() / coeff) * coeff);
		this.setState({ endTime: rounded }, () => { if (update) { this.handleCreateAppointments(true) } })
	}

	changeAppointmentNumber = (appointmentNumber) => {
		this.setState({ appointmentNumber }, () => {
			if (appointmentNumber) {
				this.handleCreateAppointments();
			} else {
				this.setState({ appointment: [] });
			}
		}
		)
	}

	changeType = (value) => {
		this.setState({ type: value }, () => this.handleCreateAppointments());
	}

	changeLength = (value) => {
		this.setState({ length: value }, () => { this.handleCreateAppointments() }
		);
	}

	handleCreateAppointments = (forceTime = false) => {
		const { appointmentNumber, length, type, startTime, endTime } = this.state;
		if (Number(appointmentNumber === null ? 0 : appointmentNumber) === 0 || forceTime) {
			if (length && startTime && endTime && !appointmentNumber && type || forceTime && length && startTime && endTime && type) {
				var sTime = moment(startTime, "HH:mm:ss a");
				var eTime = moment(endTime, "HH:mm:ss a");
				var duration = moment.duration(eTime.diff(sTime));
				var hours = duration.asMinutes();
				let newAppointmentNumber = String(Math.floor((hours / length)));
				const newAppointments = Array.from({ length: Number(newAppointmentNumber) }, (v, i) => ({
					numberRow: type === 2 ? i + 1 : null,
					time: moment(startTime, 'HH:mm A').add(length * i, 'minutes').format('LT'),
					namePartaker: null,
					is_walk: false
				}));
				this.setState({
					appointmentNumber: newAppointmentNumber,
					// appointment: newAppointments
					appointment: this.setLastAppointmentsMark(newAppointments)
				});
			} else {
				this.setState({ appointment: [] });
			}
		}
		else if (length && type && startTime) {
			const newAppointments = Array.from({ length: Number(appointmentNumber) }, (v, i) => ({
				numberRow: type === 2 ? i + 1 : null,
				time: moment(startTime, 'HH:mm A').add(length * i, 'minutes').format('LT'),
				namePartaker: null,
				is_walk: false
			}));
			const lastTime = newAppointments[newAppointments.length - 1].time;
			this.handleChangeEndTime(moment(lastTime, 'HH:mm A').add(length, 'minutes').format('LT'), false);
			this.setState({ appointment: this.setLastAppointmentsMark(newAppointments) });
			// this.setState({ appointment: newAppointments });
		}
		else {
			this.setState({ appointment: [] });
		}
	}

	setLastAppointmentsMark = (newAppointments) => {
		// let lastItems = 3
		let lastItems = parseInt(newAppointments.length / 3)
		if (newAppointments.length >= 4) {
			for (i = newAppointments.length - 1; i >= 0 && lastItems > 0; i--) {
				newAppointments[i].is_walk = true
				lastItems--;
			}
			return newAppointments
		} else {
			return newAppointments
		}
	}

	parseDataToSend = async () => {
		let { round, audition_id, location, appointmentNumber, type, length, startTime,
			endTime, appointment, date } = this.state;

		let data = {
			number_slots: appointmentNumber,
			type,
			length,
			start: moment(startTime, 'HH:mm A').format('HH:mm'),
			end: moment(endTime, 'HH:mm A').format('HH:mm'),
			status: true,
			round,
			time: moment(startTime, 'HH:mm A').format('HH:mm'),
			location,
			date
		};

		data['slots'] = [];

		appointment.map((item) => {
			let _row = {};
			_row['time'] = item.time;
			_row['status'] = true;
			_row['is_walk'] = item.is_walk;
			return data['slots'].push(_row);
		});

		if (this.validateFields()) {
			this.setState({
				appointmentNumberError: false,
				typeError: false,
				lengthError: false,
				startTimeError: false
			});
			try {
				this.setState({ loading: true });
				await createNewRound(data, audition_id);
				this.setState({ loading: false });
				showMessage('success', 'Round created successfully');
				this.handlePressBackButton();
			} catch (error) {
				console.log('eroor', error);
				const parsedError = parseError(error);
				this.setState({ loading: false });
				showMessage('danger', parsedError);
			}
		}
	}

	onChangeDate = date => { this.setState({ date }); };

	onChangeTime = time => { this.setState({ time }); };

	handlePressLocation = () => {
		const { showLocationModal } = this.state;
		this.setState({ showLocationModal: !showLocationModal });
	};

	handlePressSetLocation = location => {
		const { showLocationModal } = this.state;
		this.setState({ location, showLocationModal: !showLocationModal });
	};

	validateFields = () => {
		const { appointmentNumber, type, length, startTime } = this.state;

		if (appointmentNumber === '' || appointmentNumber === null) {
			this.setState({ appointmentNumberError: true });
			return false;
		} else {
			this.setState({ appointmentNumberError: false });
		}

		if (type === null || type === '') {
			this.setState({ typeError: true });
			return false;
		} else {
			this.setState({ typeError: false });
		}

		if (length === null || length === '') {
			this.setState({ lengthError: true });

			return false;
		} else {
			this.setState({ lengthError: false });
		}

		if (startTime === null) {
			this.setState({ startTimeError: true });

			return false;
		} else {
			this.setState({ startTimeError: false });
		}

		return true;
	}

	render() {
		const {
			appointment, startTime, endTime, type, length, appointmentNumber, date, time,
			appointmentNumberError, typeError, lengthError, startTimeError, location, showLocationModal,
			loading
		} = this.state;
		return (
			<Container >
				<LightBar />
				<Header
					left={<BackButton onPress={() => this.handlePressBackButton()} />}
					center={<HeaderText title={en.manage_audition_title} />}
					right={
						<HeaderBorderedButton
							loading={loading}
							title={en.save_button_text}
							withIcon={false}
							onPress={() => this.parseDataToSend()}
							textStyle={styles.headerButtonText}
						/>
					}
				/>
				<Content bounces={false} >
					<View style={styles.cardContainer}>
						<View style={styles.optionsContainer}>
							<BorderedDatepicker
								placeholder={en.date_placeholder}
								value={date}
								onChange={this.onChangeDate}
								containerStyle={styles.dateInputContainer}
							/>
							<BorderedTimepicker
								placeholder={en.time_placeholder}
								dateA={date}
								value={startTime}
								onChange={this.handleChangeStartTime}
								containerStyle={styles.timeInputContainer}
							/>
							<LocationButton
								title={
									location !== null
										? en.location_saved_placeholder
										: en.location_placeholder
								}
								withIcon
								onPress={() => this.handlePressLocation()}
							/>
						</View>
						<View style={styles.card}>
							<View style={styles.leftContainer}>
								<Text style={styles.titleLeftContainer}>
									{en.mark_as_walk}
								</Text>
								{appointment.length !== 0 ? <View style={styles.selectAllContainer}>
									<Text style={styles.selectAllText}>
										{en.select_all}
									</Text>
									<TouchableOpacity onPress={this.onSelectAll}>
										<Image source={this.state.isSelectAll ? check : uncheck} />
									</TouchableOpacity>
								</View> : null}
								<ScrollView style={styles.appointmentScroll}>
									{
										appointment.length !== 0 && (
											appointment.map((item, index) => {
												return (
													<AppointmentRow
														{...item}
														mark={
															(value) => this.markRow(value, index)
														}
														key={'AppointmentRow' + index}
													/>
												)
											})
										)
									}
								</ScrollView>
							</View>
							<View style={styles.rightContainer}>
								<Text style={styles.titleSection}>
									{en.appointment_slots}
								</Text>
								<View
									style={
										[
											styles.gradientContainer,
											appointmentNumberError ? styles.inputError : {}
										]
									}
								>
									<TextInput
										placeholder='0'
										value={appointmentNumber}
										keyboardType='number-pad'
										placeholderTextColor={this.props.inputPlaceholder}
										onChangeText={this.changeAppointmentNumber}
										style={styles.gradientInput}
									/>
									<GradientButton
										activeOpacity={1}
										containerStyle={styles.gradientLabelContainer}
										gradientStyle={styles.gradientLabelContainer}
										buttonText={en.appointments}
									/>
								</View>
								<Text style={styles.titleSection}>
									{en.appointments_type}
								</Text>
								<View style={styles.inputsContainer}>
									<BorderedSelect
										onChange={this.changeType}
										data={this.props.appointmentType}
										placeholder={en.select_type_placeholder}
										value={type}
										style={
											[
												styles.input,
												typeError ? styles.inputError : {}
											]
										}
									/>
								</View>
								<Text style={styles.titleSection}>
									{en.appointments_length}
								</Text>
								<View style={styles.inputsContainer}>
									<BorderedSelect
										onChange={this.changeLength}
										data={this.appointmentArray}
										// data={this.props.timeInterval}
										placeholder={en.select_length_placeholder}
										value={length}
										style={
											[
												styles.input,
												lengthError ? styles.inputError : {}
											]
										}
									/>
								</View>
								<Text style={styles.titleSection}>
									{en.start_time}
								</Text>
								<View style={styles.inputsContainer}>
									<BorderedTimepicker
										value={startTime}
										placeholder={en.select_start_time_placeholder}
										onChange={this.handleChangeStartTime}
										inputStyle={startTimeError ? styles.inputTimeError : styles.inputTime}
									/>
								</View>
								<Text style={styles.titleSection}>
									{en.end_time}
								</Text>
								<View style={styles.inputsContainer}>
									<BorderedTimepicker
										value={endTime}
										placeholder={en.select_end_time_placeholder}
										onChange={this.handleChangeEndTime}
										// disabled={false}
										disabledInput={styles.disabledInput}
									/>
								</View>
							</View>
						</View>
					</View>
				</Content>
				{/* location modal */}
				<LocationModal
					show={showLocationModal}
					dismiss={this.handlePressLocation}
					agree={this.handlePressSetLocation}
					location={location}
				/>
			</Container>
		)
	}
}

Round.defaultProps = {
	dismiss: () => { },
	show: true,
	appointment: [],
	inputPlaceholder: '#4d2545',
	appointmentType: [{ label: 'Time', value: 1 }, { label: 'Numeric', value: 2 }],
	timeInterval: Array.from({ length: 6 }, (v, i) => ({ label: `${10 * (i + 1)} minutes`, value: 10 * (i + 1) })),
	done: () => null
}

export default Round;
