import { StyleSheet, Platform } from 'react-native';
import { resize, NEXABOLD, height, STATUS_BAR_HEIGHT } from '../../../../assets/styles';

const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT;

const styles = StyleSheet.create({
	container: {
		// flex: 1,
	},
	headerButtonText: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(20)
	},
	nBContainer: { backgroundColor: 'transparent' },
	dismissButton: {
		width: '100%',
		height: containersHeight,
		position: 'absolute',
		justifyContent: 'flex-end'
	},
	blurView: {
		width: '100%',
		height: resize(Platform.OS === 'ios' ? 732 : 729, 'height'),
		backgroundColor: '#f0f0f0',
		opacity: 0.84,
	},
	cardContainer: {
		width: '100%',
		paddingVertical: resize(30, 'height'),
		paddingHorizontal: resize(40)
	},
	card: {
		alignSelf: 'center',
		width: '100%',
		justifyContent: 'space-between',
		height: resize(585, 'height'),
		shadowColor: 'rgba(0, 0, 0, 0.16)',
		shadowOffset: { width: 3, height: 0 },
		shadowRadius: resize(6),
		borderRadius: resize(13),
		backgroundColor: 'transparent',
		borderColor: '#4D2545',
		borderWidth: 1,
		marginBottom: resize(33, 'height'),
		flexDirection: 'row'
	},
	closeButton: {
		position: 'absolute',
		zIndex: 2,
		paddingHorizontal: resize(18),
		paddingVertical: resize(21, 'height')
	},
	leftContainer: {
		width: resize(623)
	},
	appointmentScroll: {
		paddingLeft: resize(161),
		paddingRight: resize(57)
	},
	titleLeftContainer: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(10),
		fontWeight: '400',
		letterSpacing: resize(0.5),
		marginTop: resize(27, 'height'),
		marginLeft: resize(500),
		textAlign: 'center',
		marginBottom: resize(9, 'height')
	},
	rightContainer: {
		paddingTop: resize(19, 'height'),
		borderLeftWidth: 1,
		borderColor: '#707070',
		width: resize(388),
		alignItems: 'center'
	},
	titleSection: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(17),
		fontWeight: '400',
		letterSpacing: resize(0.85),
		marginTop: resize(Platform.OS === 'ios' ? 23 : 17, 'height')
	},
	gradientContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: resize(14, 'height'),
		width: resize(256),
		height: resize(51, 'height'),
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 1,
		borderRadius: resize(28),
	},
	inputError: {
		borderColor: 'red'
	},
	inputsContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: resize(14, 'height'),
		width: resize(256),
		height: resize(51, 'height'),
	},
	gradientInput: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(29),
		fontWeight: '400',
		letterSpacing: resize(1.45),
		textAlign: 'center',
		textAlignVertical: 'center',
		flex: 1,
		padding: 0
	},
	gradientLabelContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		width: resize(164),
		height: resize(50, 'height'),
		borderTopRightRadius: resize(26),
		borderBottomRightRadius: resize(25),
	},
	gradientButton: {
		width: resize(257),
		height: resize(51, 'height'),
		shadowColor: 'rgba(0, 0, 0, 0.4)',
		shadowOffset: { width: 0, height: 0 },
		shadowRadius: resize(3),
		borderRadius: resize(28),
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: resize(13, 'height')
	},
	disabledInput: {
		backgroundColor: '#FFF'
	},
	input: {
		margin: 0,
		padding: 0,
		width: '100%',
		height: '100%',
		borderRadius: 28,
		borderColor: '#4d2545',
		borderWidth: 1,
		backgroundColor: '#ffffff',
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
		paddingLeft: resize(21),
	},
	inputTime: {
		width: '100%',
		height: resize(55, 'height'),
		borderRadius: 28,
		borderColor: '#4d2545',
		borderWidth: 1,
	},
	inputTimeError: {
		width: '100%',
		height: resize(55, 'height'),
		borderRadius: 28,
		borderColor: 'red',
		borderWidth: 1,
	},
	optionsContainer: {
		width: '100%',
		height: resize(100, 'height'),
		marginBottom: resize(20, 'height'),
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row'
	},
	dateInputContainer: {
		width: resize(328),
		height: resize(55, 'height'),
	},
	timeInputContainer: {
		width: resize(322),
		height: resize(55, 'height'),
	},
	locationInputContainer: {
		width: resize(328),
		height: resize(55, 'height'),
		flexDirection: 'row',
	},
	selectAllContainer: {
		flexDirection: "row", paddingLeft: resize(170), marginVertical: 10,
		paddingRight: resize(57), justifyContent: "space-between", alignItems: "center"
	},
	selectAllText: {
		fontSize: resize(16),
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontWeight: '400',
		letterSpacing: resize(0.5),
	}
})
export default styles;
