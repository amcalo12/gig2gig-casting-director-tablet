//import liraries
import React, { Component } from "react";
import { View, Text, StyleSheet, Alert } from "react-native";
import Video from "react-native-video";
import { withNavigationFocus } from "react-navigation";
import { connect } from "react-redux";
import Header from "../../../components/commons/header";
import HeaderButton from "../../../components/commons/header_button";
import HeaderText from "../../../components/commons/header_text";
// import styles, { headerHeight } from "./style";
import BackButton from "../../../components/commons/back_button";
import { Container, Content, Tab, Tabs } from "native-base";

class VideoPlayerOpen extends Component {
  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack(null);
  };

  videoError = () => {
    Alert.alert(
      "",
      "Video format unsupported.",
      [
        {
          text: "OK",
          onPress: () => {
            const { navigation } = this.props;
            navigation.goBack(null);
          }
        }
        //   { text: "No", onPress: () => {} }
      ],
      {
        cancelable: false
      }
    );
  };

  render() {
    const video_Url = this.props.navigation.getParam("video_url");
    console.log("this.props.navigation ==> " + JSON.stringify(video_Url));
    console.log("VIDEO URL=====>", video_Url);
    return (
      <Container>
        <Header
          left={<BackButton onPress={() => this.goBack()} />}
          center={<HeaderText title={"Video"} />}
        />
        <View style={styles.container}>
          <Video
            //    source={require('../../../assets/videos/broadchurch.mp4')}
            source={{ uri: video_Url ? video_Url : "" }}
            onError={this.videoError}
            ref={ref => {
              this.player = ref;
            }} // Store reference
            onBuffer={this.onBuffer} // Callback when remote video is buffering
            onError={this.videoError} // Callback when video cannot be loaded
            style={styles.backgroundVideo}
          />
        </View>
      </Container>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#2c3e50"
  },
  backgroundVideo: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});
// const mapStateToProps = state => ({
//     //user: state.user.data
//   });
// export default withNavigationFocus(
//     connect(VideoPlayerOpen)
//   );
export default VideoPlayerOpen;
