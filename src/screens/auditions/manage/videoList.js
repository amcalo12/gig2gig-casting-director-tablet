import { Container, Content, Tab, Tabs } from "native-base";
import React, { Component } from "react";
import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Animated,
  Linking,
  Keyboard,
  FlatList,
  Modal,
  TouchableHighlight,
  RefreshControl,
  Platform,
  Button
} from "react-native";
import moment from "moment";
import { connect } from "react-redux";
import { Icon } from "native-base";
import Menu, { MenuItem, MenuDivider } from "react-native-material-menu";

//custom
import styles, { headerHeight } from "./style";
import AlertIOS from "../../../components/commons/alert_ios";
import FileMenu from "../../../components/auditions/file_menu";
import ArrowLeft from "../../../assets/icons/auditions/arrow-left-icon.png";
import iconSearch from "../../../assets/icons/search/search.png";
import Contributor from "../../../components/auditions/contributor";
import File from "../../../components/auditions/file";
import ManageButton from "../../../components/auditions/manage_button";
import Partaker from "../../../components/auditions/partaker";
import PartakerDrag from "../../../components/auditions/partaker/draggable";
import Role from "../../../components/auditions/role";
import BackButton from "../../../components/commons/back_button";
import GradientButton from "../../../components/commons/gradient_button";
import Header from "../../../components/commons/header";
import HeaderButton from "../../../components/commons/header_button";
import HeaderText from "../../../components/commons/header_text";
import Search from "../../../components/commons/search";
import PinKeyboard from "../../../components/commons/pin_keyboard";
import Loading from "../../../components/commons/loading";
import UnionTag from "../../../components/auditions/union_tag";
import ContractTag from "../../../components/auditions/contract_tag";
import ProductionTag from "../../../components/auditions/production_tag";
import RoleIcon from "../../../assets/icons/auditions/role-icon.png";
import parseError from "../../../utils/parse_error";
import showMessage from "../../../utils/show_message";
import shareContent from "../../../utils/share_content";
import { withNavigationFocus } from "react-navigation";
import RNFetchBlob from "react-native-fetch-blob";

import {
  DragContainer,
  Draggable,
  DropZone
} from "react-native-drag-drop-and-swap";

//api
import {
  getAudition,
  openAudition,
  closeAudition,
  getAuditionAppointments,
  getAuditionVideos,
  deleteMedia,
  addPersonToRol,
  getFinalList,
  removePersonToRol,
  closeRoound,
  getVideoList
} from "../../../api/auditions";

//locale
import en from "../../../locale/en";

// actions
import {
  SET_CHECKIN_MODE_PIN,
  SET_MONITOR_UPDATES_PIN,
  SET_AUDITION_ID_TO_CHECKIN
} from "../../../actions/manage";
import { resize } from "../../../assets/styles";
import FakeDropDown from "../../../components/commons/fakeSelect";
import { getRoundsByAudition, createNewRound } from "../../../api/rounds";
import { SET_AUDITION_INFO } from "../../../actions/checkin";
import { hidden } from "../../../../node_modules/ansi-colors";
import VideoListContainer from "./VideoListContainer";

const AnimatedView = Animated.View;
var scrollOffset = 0;
var refDropZones = [];

class VideoListComponent extends Component {
  state = {
    audition_id: this.props.navigation.getParam("audition_id", ""),
    round: this.props.navigation.getParam("round", ""),
    title: "",
    date: "",
    time: "",
    description: "",
    url: "",
    union: "",
    contract: "",
    production: [],
    companyName: "",
    appointmentsNumber: 0,
    roles: [],
    files: [],
    contributors: [],
    loading: true,
    search: "",
    showSearch: false,
    partakers: [],
    contributors: [],
    currentTab: 0,
    fileSelected: {},
    openAuditions: false,
    showAuditionVideos: false,
    showOptions: false,
    optionsPosition: 0,
    optionsPosition2: 0,
    enterMonitorModeModal: false,
    enterCheckinModeModal: false,
    pinModalCheckin: false,
    pinModalMonitor: false,
    checkinPin: "",
    updatesPin: "",
    viewSplit: false,
    viewSplitAnimatedValueA: new Animated.Value(0),
    viewSplitAnimatedValue: new Animated.Value(255),
    status: this.props.navigation.getParam("status", 0),
    loadingButton: false,
    loadingButtonRound: false,
    auditionVideos: [],
    isAuditionVideo: false,
    isOpen: true,
    scrollOffset: 0,
    isFinalList: false,
    position: [],
    rolesAvailable: [],
    indexItem: [],
    zIndex: 2,
    finalList: [],
    listData: [],
    slots: [],
    showAppointmentsModal: false,
    audition_rounds: [],
    actualRound: {},
    modalCloseAudition: false,
    modalOpenNewRound: false,
    entreConfirm: false,
    hasFinal: false,
    modalAudition: false,
    loadingButtonC: false,
    showFinal: false,
    loadingRoundStart: false,
    refreshing: false,
    videoList1: [
      { key: "Video 1", ext: ".mp4" },
      { key: "Video 2", ext: ".mp4" },
      { key: "Video 3", ext: ".mp4" },
      { key: "Video 4", ext: ".mp4" },
      { key: "Video 5", ext: ".mp4" },
      { key: "Video 6", ext: ".mp4" },
      { key: "Video 7", ext: ".mp4" }
    ],
    videoList: [],
    isShowPopup: false,
    modalVisible: false,
    ModalVisibleStatus: false,
    // isVisible: false,
    performerArray: []
  };

  animatedValue = new Animated.Value(0);

  componentDidMount() {
    //this.getRounds();
    this.getVideoList();
  }

  //Call get video list
  getVideoList = async () => {
    // const { navigation } = this.props;

    this.setState({ loading: true });

    try {
      let resquestItems = await getVideoList(
        this.state.audition_id,
        this.state.round.round
      );
      // let resquestItems = await getVideoList(1,1);
      //let newArr = Object.keys(resquestItems.data.data.performer);

      // console.log("New Arry ===> " + newArr);
      //  console.log(
      //   "New Arry ===> " + JSON.stringify(resquestItems.data.data.performer)
      // );
      // this.state.partakers[0]=resquestItems.data.data[0].performer;
      resquestItems.data.data.map((data, index) => {
        this.state.partakers[index] = data.performer;
      });

      this.state.partakers = this.state.partakers.filter(
        (Value, index, array) =>
          array.findIndex(item => item.user_id === Value.user_id) === index
      );
      console.log("VIDEO LIST FETCH FILTER ==> 111", this.state.partakers);

      this.setState(
        {
          videoList: resquestItems.data.data
        },
        () => {
          console.log("VideoPerformerData===>", this.state.partakers);
        }
      );

      console.log(
        "VIDEO LIST FETCH ==> " + JSON.stringify(this.state.videoList)
      );

      //   var tempArray = [];
      //  var tempArray2 = tempArray.push(resquestItems.data.data.performer);

      //   console.log("VIDEO LIST FETCH2 ==> " + JSON.stringify(tempArray2));

      //   this.setState({
      //     partakers: temprray,
      //     isFinalList: true
      //   });

      this.setState({ loading: false });

      // showMessage('success', 'Please check your email inbox');

      // navigation.navigate('Login');
    } catch (error) {
      console.log(error.response);
      this.setState({ loading: false, videoList: [] });
      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }
  };

  ShowModalFunction(visible) {
    this.setState({ ModalVisibleStatus: visible });
  }
  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack(null);
  };

  openModal1() {
    return (
      <Modal
        transparent={true}
        animationType={"fade"}
        visible={this.state.ModalVisibleStatus}
        onRequestClose={() => {
          this.ShowModalFunction(!this.state.ModalVisibleStatus);
        }}
      >
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginLeft: 50
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#fff",
              height: 50,
              width: "30%",
              borderRadius: 10,
              borderWidth: 1,
              borderColor: "#fff"
            }}
          >
            {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}

            <Text
              style={{
                fontSize: 20,
                marginBottom: 20,
                color: "#fff",
                padding: 20,
                textAlign: "center"
              }}
            >
              Text Component With Some Sample Text In Modal.{" "}
            </Text>

            <Button
              title="Click Here To Hide Modal"
              onPress={() => {
                this.ShowModalFunction(!this.state.ModalVisibleStatus);
              }}
            />

            {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}
          </View>
        </View>
      </Modal>
    );
  }
  showModal() {
    return (
      <View
        style={{
          position: "absolute",
          marginTop: -50,
          top: 100,
          left: 100,
          width: 100,
          height: 100,
          backgroundColor: "blue",
          zIndex: 999
        }}
      >
        {/* //style={{height:"30%",width:"40%",backgroundColor:'white', justifyContent:'center',alignSelf:'center'}}> */}
        <View>
          <TouchableOpacity
            style={styles.FacebookStyle}
            activeOpacity={0.5}
            //    onPress={() => {this.setState({ isVisible: false})}}
            onPress={() => {
              currentItem.isVisible = false;
            }}
          >
            <Image
              source={require("../../../assets/icons/auditions/share-icon.png")}
              style={styles.ImageIconStyle}
            />
            <Text style={styles.TextStyle}> Share </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.GooglePlusStyle}
            activeOpacity={0.5}
            //    onPress={() => {this.setState({ isVisible: false})}}
            onPress={() => {
              currentItem.isVisible = false;
            }}
          >
            <Image
              source={require("../../../assets/icons/auditions/open-in-icon.png")}
              style={styles.ImageIconStyle}
            />

            <Text style={styles.TextStyle}> Open in </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.GooglePlusStyle}
            activeOpacity={0.5}
            //    onPress={() => {this.setState({ isVisible: false})}}
            onPress={() => {
              this.currentItem.isVisible = false;
            }}
          >
            <Image
              //We are showing the Image from online
              source={require("../../../assets/icons/auditions/trash-icon.png")}
              //You can also show the image from you project directory like below
              //source={require('./Images/google-plus.png')}
              //Image Style
              style={styles.ImageIconStyle}
            />

            <Text style={styles.TextStyle}> Delete </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  handlePressShowSearch = () => {
    this.setState({
      showSearch: true
    });
  };
  onChangeSearch = (search) => {
    this.setState({ search });
  }
  showMenu = () => {
    this._menu.show();
  };
  onFinalListPress = index => {
    const { navigation } = this.props;
    const { partakers, audition_id, actualRound } = this.state;
    navigation.navigate("ManagerTalentDatabase", {
      personId: partakers[index].user_id,
      image: partakers[index].image,
      audition_id: actualRound.id,
      audition: audition_id
    });
  };
  render() {
    const {
      showSearch,
      search,
      partakers,
      showAuditionVideos,
      currentTab,
      showOptions,
      fileSelected,
      optionsPosition,
      enterMonitorModeModal,
      pinModalCheckin,
      pinModalMonitor,
      enterCheckinModeModal,
      loading,
      title,
      date,
      time,
      appointmentsNumber,
      union,
      contract,
      production,
      description,
      url,
      contributors,
      roles,
      files,
      status,
      loadingButton,
      companyName,
      auditionVideos,
      checkinPin,
      updatesPin,
      viewSplitAnimatedValue,
      viewSplitAnimatedValueA,
      position,
      zIndex,
      rolesAvailable,
      indexItem,
      isFinalList,
      chagingType,
      optionsPosition2,
      actualRound,
      audition_rounds,
      loadingButtonRound,
      modalCloseAudition,
      modalOpenNewRound,
      entreConfirm,
      modalAudition,
      loadingButtonC,
      showFinal,
      online,
      loadingRoundStart,
      refreshing
    } = this.state;
    console.log("status========>" + status);

    const partakersFilter = partakers.filter(partaker => {
      let name = partaker.name.toUpperCase();
      return name.match(new RegExp(search.toUpperCase(), "g"));
    });

    const widthFirstContainerA = viewSplitAnimatedValueA.interpolate({
      inputRange: [-resize(405), 0],
      outputRange: [-resize(405), 0]
    });

    const widthFirstContainer = viewSplitAnimatedValue.interpolate({
      inputRange: [0, resize(255)],
      outputRange: [0, resize(255)]
    });

    // if (loading) {
    // 	return <Loading />
    // }

    return (
      <Container>
        <Header
          left={<BackButton onPress={() => this.goBack()} />}
          center={<HeaderText title={title} />}
          right={
            showSearch ? (
              <Search
                placeholder={en.search_audition_text}
                value={search}
                onChange={this.onChangeSearch}
              />
            ) : (
                <HeaderButton
                  icon={iconSearch}
                  onPress={() => this.handlePressShowSearch()}
                  containerStyle={styles.searchIcon}
                />
              )
          }

        />
        <View style={styles.wrapperContent}>
          <FileMenu
            show={showOptions}
            dismiss={() => this.onPressFileMore({})}
            posy={optionsPosition}
            showOptionShare={fileSelected.shareable === "yes" ? true : false}
            share={() => this.shareAsset(fileSelected)}
            showOptionOpen
            openIn={() => this.openAsset(fileSelected)}
            showOptionDelete
            delete={() => this.handleDeleteFile(fileSelected)}
          >
            {"id" in fileSelected && (
              <File
                filename={fileSelected.name}
                fileType={fileSelected.type}
                onPressMore={() => this.onPressFileMore({})}
              />
            )}
          </FileMenu>
          {status === 0 && (
            <AnimatedView
              style={[
                styles.firstSectionAnimated,
                {
                  paddingRight: widthFirstContainer
                }
              ]}
            >
              <View style={styles.noAuditions}>
                <Text style={styles.noAuditionsText}>
                  {!isFinalList ? en.checkin_no_open : ""}
                </Text>
              </View>
            </AnimatedView>
          )}
          {(status === 1 || status === 2) && (
            <AnimatedView
              style={[
                styles.firstSectionAnimated,
                {
                  paddingRight: widthFirstContainer
                }
              ]}
            >
              <Content bounces={false} style={styles.firstSection}>
                <View style={styles.partakerContainer}>
                  {console.log(
                    "partakersFilter======>" + JSON.stringify(partakersFilter)
                  )}
                  {!isFinalList ? (
                    <FlatList
                      contentContainerStyle={styles.partakerContainer}
                      // style={styles.content}
                      data={partakersFilter}
                      extraData={this.state}
                      keyExtractor={item =>
                        item.slot_id !== null ? item.slot_id.toString() : null
                      }
                      renderItem={({ item, index }) => {
                        console.log(
                          "Performer item=====>" + JSON.stringify(item)
                        );
                        //DISPLAY PERFORMER LIST
                        return (
                          <Partaker
                            online={online}
                            name={item.name}
                            image={item.image}
                            time={item.time !== null ? item.time : null}
                            favorite={item.favorite === 1 ? true : false}
                            onPress={() => {
                              // actualRound.status === 1
                              //   ? this.onPressPartaker(index)
                              //   :
                              this.props.navigation.state.params
                                .onFinalListPressCallBack
                                ? this.props.navigation.state.params.onFinalListPressCallBack(
                                  item.user_id,
                                  item.image
                                )
                                : null;
                            }}
                          />
                        );
                      }}
                      ListEmptyComponent={
                        <Text style={styles.emptyText}>
                          {en.no_performances}
                        </Text>
                      }
                      refreshing={refreshing}
                      onRefresh={this.refreshPartakers}
                      showsVerticalScrollIndicator={false}
                      keyExtractor={item => item.toString()}
                    />
                  ) : null}
                </View>
              </Content>
            </AnimatedView>
          )}

          <Animated.View
            style={[styles.lateralLayout, { right: widthFirstContainerA }]}
          >
            <TouchableOpacity
              style={styles.spiltContainer}
            //onPress={this.handleSideMenu}
            >
              <View style={styles.spiltIcon} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => {
                this.setState({ isShowPopup: false });
              }}
            >
              {this.state.videoList.length > 0 ? (
                <VideoListContainer
                  {...this.props}
                  showAuditionVideos={this.state.showAuditionVideos}
                  title={en}
                  auditionVideos={{ auditionVideos }}
                  videoList={this.state.videoList}
                  onVideoDeleteSuccess={() => {
                    this.getVideoList();
                  }}
                  handleBackPress={() => {
                    this.goBack();
                  }}
                  auditionID={this.state.audition_id}
                  isShowPopup={this.state.isShowPopup}
                />
              ) : null}
            </TouchableOpacity>
          </Animated.View>
        </View>
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user.data
});

export default withNavigationFocus(
  connect(mapStateToProps)(VideoListComponent)
);
