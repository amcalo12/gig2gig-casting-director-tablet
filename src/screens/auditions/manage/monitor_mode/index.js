import React, { Component } from "react";
import {
  View,
  Text,
  Keyboard,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { Content, Input } from "native-base";
import { StackActions, NavigationActions } from "react-navigation";
import moment from "moment";
import { connect } from "react-redux";
import DraggableFlatList from "react-native-draggable-flatlist";

//custom
import styles from "./style";
import LightBar from "../../../../components/commons/light_bar";
import Header from "../../../../components/commons/header";
import HeaderText from "../../../../components/commons/header_text";
import CheckButton from "../../../../components/auditions/check_button";
import AppointmentsItem from "../../../../components/auditions/appointment_item";
import UpdatesButton from "../../../../components/auditions/updates_button";
import GradientButton from "../../../../components/commons/gradient_button";
import TimelineItem from "../../../../components/auditions/timeline_item";
import AlertIOS from "../../../../components/commons/alert_ios";
import PinKeyboard from "../../../../components/commons/pin_keyboard";
import Loading from "../../../../components/commons/loading";
import showMessage from "../../../../utils/show_message";
import parseError from "../../../../utils/parse_error";

//locale
import en from "../../../../locale/en";

//actions
import { DESTROY_MONITOR_UPDATES_PIN } from "../../../../actions/manage";

//api
import {
  getAuditionAppointments,
  getAuditionNotifications,
  createAuditionUpdate,
  getAuditionPreNotifications,
  reorderAppointments,
  getRoundSlots
} from "../../../../api/auditions";

class MonitorMode extends Component {
  state = {
    audition_id: this.props.navigation.getParam("audition_id", ""),
    slots: {},
    actualRound: this.props.navigation.getParam("actualRound", ""),
    audition: this.props.navigation.getParam("audition", ""),
    appointments: [],
    loading: true,
    loadingButton: false,
    updates: [],
    preUpdates: [],
    updateSelected: {},
    dismissOutMonitor: false,
    pinModalMonitor: false,
    pin: "",
    newNotification: "",
    showNewNotificationInput: false
  };

  componentDidMount() {
    this.fetchNotification();
    this.fetchPreNotification();
    this.getSlotsByRound();
  }

  getSlotsByRound = async () => {
    let { actualRound } = this.state;
    try {
      let responseSlots = await getRoundSlots(actualRound.id);
      let newSlots = [];
      let slots = responseSlots.data.data;
      if (slots.length > 0) {
        this.setState({ slots: slots }, () => {
          this.fetchSelectedAppointments();
        });
      } else {
        for (const key in slots) {
          newSlots.push(slots[key]);
        }
        this.setState({ slots: newSlots }, () => {
          this.fetchSelectedAppointments();
        });
      }
    } catch (error) {
      console.log("getSlotsByRound =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  fetchSelectedAppointments = async () => {
    const { actualRound, slots } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getAuditionAppointments(actualRound.id);
      let search = response.data.data;
      console.log("fetchSelectedAppointments SEARCH =====>", search);
      let appointments = [];
      for (const iterator of slots) {
        let findTime = search.find(snap => {
          return snap.time === iterator.time;
        });
        if (findTime) {
          appointments.push(findTime);
        } else {
          appointments.push({ name: "Free" });
        }
      }

      this.setState({
        appointments
      });
      console.log(
        "fetchSelectedAppointments appointments =====>",
        this.state.appointments
      );
    } catch (error) {
      console.log("fetchSelectedAppointments ERROR =====>", error);
    }

    this.setState({ loading: false });
  };

  fetchNotification = async () => {
    const { audition_id } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getAuditionNotifications(audition_id);

      this.setState({
        updates: response.data.data
      });
    } catch (error) {
      console.log("fetchNotification=====>", error);
    }

    this.setState({ loading: false });
  };

  fetchPreNotification = async () => {
    const { audition_id } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getAuditionPreNotifications(audition_id);

      this.setState({
        preUpdates: response.data.data
      });
    } catch (error) {
      console.log("fetchNotification=====>", error);
    }

    this.setState({ loading: false });
  };

  handlePressCheckButton = () => {
    this.setState({
      dismissOutMonitor: true,
      pinModalMonitor: false,
      pin: ""
    });
  };

  handlePressUpdate = updateSelected => {
    if (this.state.updateSelected === updateSelected) {
      return this.setState({ updateSelected: {} });
    }

    return this.setState({ updateSelected });
  };

  handlePressAddUpdate = () => {
    this.setState({
      showNewNotificationInput: true,
      updateSelected: {}
    });
  };

  handlePressSendUpdate = async () => {
    const {
      updates,
      newNotification,
      updateSelected,
      audition_id,
      preUpdates
    } = this.state;

    const hasError = this.validateSendNotification();

    if (hasError !== "") {
      return showMessage("danger", hasError);
    }

    this.setState({ loadingButton: true });

    try {
      let notification = {
        time: moment().format("HH:mm:ss"),
        appointment: audition_id
      };
      let preUpdate = "";
      let newPreUpdates = [...preUpdates];

      if (updateSelected.hasOwnProperty("title")) {
        notification["title"] = updateSelected.title;
      } else {
        notification["title"] = newNotification;
        preUpdate = newNotification;
      }
      await createAuditionUpdate(notification);

      if (preUpdate !== "") {
        newPreUpdates = [
          {
            title: preUpdate
          },
          ...newPreUpdates
        ];
      }

      this.setState({
        showNewNotificationInput: false,
        newNotification: "",
        updates: [notification, ...updates],
        updateSelected: {},
        preUpdates: newPreUpdates
      });

      showMessage("success", en.update_was_sent);
    } catch (error) {
      console.log("handlePressSendUpdate=====>", error);

      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }

    this.setState({ loadingButton: false });
  };

  validateSendNotification = () => {
    const {
      showNewNotificationInput,
      updateSelected,
      newNotification
    } = this.state;
    let errorMessage = "";

    Keyboard.dismiss();

    if (showNewNotificationInput) {
      if (newNotification === "") {
        errorMessage = en.require_new_notification;
      }
    } else {
      if (Object.entries(updateSelected).length === 0) {
        errorMessage = en.select_an_update;
      }
    }

    return errorMessage;
  };

  agreeOutMonitor = () => {
    this.setState({
      dismissOutMonitor: false,
      pinModalMonitor: true,
      pin: ""
    });
  };

  dismissOutMonitor = () => {
    this.setState({
      dismissOutMonitor: false,
      pinModalMonitor: false,
      pin: ""
    });
  };

  onPressSetPinMonitor = () => {
    const { audition, pin } = this.state;
    const { navigation, dispatch, updatesPin } = this.props;

    if (pin !== "") {
      if (pin === updatesPin) {
        this.setState({
          dismissOutMonitor: false,
          pinModalMonitor: false,
          pin: ""
        });

        dispatch({
          type: DESTROY_MONITOR_UPDATES_PIN,
          payload: ""
        });

        const resetManagerAction = StackActions.reset({
          index: 1,
          key: null,
          actions: [
            NavigationActions.navigate({
              routeName: "Home"
            }),
            NavigationActions.navigate({
              routeName: "ManagerAudition",
              params: { audition_id: audition }
            })
          ]
        });

        return navigation.dispatch(resetManagerAction);
      }

      return this.setState({
        pin: ""
      });
    }
  };

  handleChangePin = digit => {
    const { pin } = this.state;

    this.setState({ pin: `${pin}${digit}` });
  };

  onChangeNewNotification = newNotification => {
    this.setState({ newNotification });
  };

  onMoveEnd = async ({ data, from, to }) => {

    console.log(" DATA ", data);
    console.log(" from ", from);
    console.log(" to ", to);
    // return
    // comment replace item of list logic
    // let oldList = this.state.appointments;
    // let newList = [];
    // newList = oldList;
    // let fromObj = newList[from];
    // let toObj = newList[to];
    // newList[to] = fromObj;
    // newList[from] = toObj;
    console.log(
      "====================== ON MOVE END CALL ====================== "
    );
    let { slots, audition_id } = this.state;
    console.log("ON MOVE END slots data=====");
    console.log("ON MOVE END slots data=====>", JSON.stringify(slots));
    console.log("ON MOVE END Appointments==========");
    console.log("ON MOVE END DATA", JSON.stringify(data));
    let finalData = [];
    // for (let index = 0; index < data.length; index++) {
    //   const element = data[index];
    //   if (element.user_id) {
    //     finalData.push({
    //       slot_id: slots[index].id,
    //       user_id: element.user_id
    //     });
    //   }
    // }

    if (this.state.appointments[from].name != undefined && this.state.appointments[from].name != "Free") {
      finalData[finalData.length] = {
        slot_id: slots[to].id,
        user_id: this.state.appointments[from].user_id
      }
    }

    if (this.state.appointments[to].name != undefined && this.state.appointments[to].name != "Free") {
      finalData[finalData.length] = {
        slot_id: slots[from].id,
        user_id: this.state.appointments[to].user_id
      }
    }

    console.log(" CHECK NEW DATA ", finalData)
    this.setState({ appointments: data, loading: true });
    try {
      console.log("ON MOVE END FINAL DATA=====>", finalData);
      let test = await reorderAppointments(audition_id, finalData);
      console.log("ON MOVE END TEST=====>", test);
      this.getSlotsByRound();
    } catch (error) {
      this.setState({ loading: false })
      console.log("ON MOVE END ERROR=====>", error);
    }
  };

  // onMoveEnd_Old = async ({ data, from, to }) => {

  //   console.log(" DATA ", data);
  //   console.log(" from ", from);
  //   console.log(" to ", to);
  //   // return
  //   // comment replace item of list logic
  //   // let oldList = this.state.appointments;
  //   // let newList = [];
  //   // newList = oldList;
  //   // let fromObj = newList[from];
  //   // let toObj = newList[to];
  //   // newList[to] = fromObj;
  //   // newList[from] = toObj;
  //   console.log(
  //     "====================== ON MOVE END CALL ====================== "
  //   );
  //   this.setState({ appointments: data });
  //   let { slots, audition_id } = this.state;
  //   console.log("ON MOVE END slots data=====");
  //   console.log("ON MOVE END slots data=====>", JSON.stringify(slots));
  //   console.log("ON MOVE END Appointments==========");
  //   console.log("ON MOVE END DATA", JSON.stringify(data));
  //   let finalData = [];
  //   for (let index = 0; index < data.length; index++) {
  //     const element = data[index];
  //     if (element.user_id) {
  //       finalData.push({
  //         slot_id: slots[index].id,
  //         user_id: element.user_id
  //       });
  //     }
  //   }
  //   try {
  //     console.log("ON MOVE END FINAL DATA=====>", finalData);
  //     let test = await reorderAppointments(audition_id, finalData);
  //     console.log("ON MOVE END TEST=====>", test);
  //   } catch (error) {
  //     console.log("ON MOVE END ERROR=====>", error);
  //   }
  // };

  onSlotsMoveEnd = async ({ data, from, to }) => {
    console.log("====================== ON SLOTS MOVE END CALL ====================== ");
    console.log(" DATA ", data);
    console.log(" from ", from);
    console.log(" to ", to);


    let { appointments, audition_id } = this.state;
    console.log("ON SLOTS MOVE END DATA onSlotsMoveEnd", data);
    let finalData = [];

    if (this.state.appointments[from].name != undefined && this.state.appointments[from].name != "Free") {
      finalData[finalData.length] = {
        slot_id: this.state.slots[to].id,
        user_id: this.state.appointments[from].user_id
      }
    }

    if (this.state.appointments[to].name != undefined && this.state.appointments[to].name != "Free") {
      finalData[finalData.length] = {
        slot_id: this.state.slots[from].id,
        user_id: this.state.appointments[to].user_id
      }
    }

    console.log(" CHECK NEW DATA ", finalData)
    this.setState({ slots: data });
    // for (let index = 0; index < appointments.length; index++) {
    //   const element = appointments[index];
    //   if (element.user_id) {
    //     finalData.push({
    //       slot_id: data[index].id,
    //       user_id: element.user_id
    //     });
    //   }
    // }
    this.setState({ loading: true });
    try {
      console.log(
        "ON SLOTS MOVE END FINAL DATA onSlotsMoveEnd=====>",
        finalData
      );
      let test = await reorderAppointments(audition_id, finalData);
      console.log("ON SLOTS MOVE END TEST onSlotsMoveEnd=====>", test);
      this.getSlotsByRound();
    } catch (error) {
      this.setState({ loading: false });
      console.log("ON SLOTS MOVE END ERROR=====>", error);
    }
  };

  render() {
    const {
      appointments,
      updates,
      updateSelected,
      dismissOutMonitor,
      pinModalMonitor,
      loading,
      showNewNotificationInput,
      newNotification,
      loadingButton,
      preUpdates,
      pin,
      slots
    } = this.state;

    if (loading || Object.entries(slots).length === 0) {
      return <Loading />;
    }

    return (
      <View style={{ flex: 1 }}>
        <AlertIOS
          // title={"hello"}
          title={en.close_monitor_mode_title}
          show={dismissOutMonitor}
          dismiss={this.dismissOutMonitor}
          agree={this.agreeOutMonitor}
        >
          <Text style={styles.monitorModeText}>
            {en.close_monitor_mode_description}
          </Text>
        </AlertIOS>
        <AlertIOS
          title={en.enter_passcode}
          show={pinModalMonitor}
          dismiss={this.dismissOutMonitor}
          cancelTitle={en.cancel_button_text}
          acceptTitle={en.set_button_text}
          agree={this.onPressSetPinMonitor}
        >
          <View style={styles.pinLabelContainer}>
            <Text style={styles.pin}>{pin}</Text>
          </View>
          <PinKeyboard onPressDigit={this.handleChangePin} />
        </AlertIOS>
        <LightBar />
        <Header
          center={<HeaderText title={en.monitor_mode} />}
          right={<CheckButton onPress={() => this.handlePressCheckButton()} />}
        />
        <Content bounces={false} disableKBDismissScroll style={styles.content}>
          <View style={styles.wrapperContent}>
            <View style={styles.firstSection}>
              <View style={styles.updatesListContainer}>
                {updates.length > 0 &&
                  preUpdates.map((update, index) => {
                    return (
                      <UpdatesButton
                        key={index}
                        title={update.title}
                        onPress={() => this.handlePressUpdate(update)}
                        selected={updateSelected === update}
                      />
                    );
                  })}
                {showNewNotificationInput && (
                  <View style={styles.inputNotificationContainer}>
                    <Input
                      style={styles.inputNotification}
                      value={newNotification}
                      onChangeText={value =>
                        this.onChangeNewNotification(value)
                      }
                      autoFocus
                    />
                  </View>
                )}
                <UpdatesButton
                  title={en.add_new_notification}
                  onPress={() => this.handlePressAddUpdate()}
                  showIcon
                />
              </View>
              <View style={styles.sendUpdateContainer}>
                <GradientButton
                  onPress={() => !loadingButton && this.handlePressSendUpdate()}
                  buttonText={en.send_update}
                  containerStyle={styles.sendButtonContainer}
                  gradientStyle={styles.sendButtonGradient}
                  loading={loadingButton}
                />
              </View>
              <View style={styles.timelineContainer}>
                <Text style={styles.timelineTitle}>{en.updates_title}</Text>
                <View style={styles.timelineContent}>
                  {updates.length > 0 &&
                    updates.map((update, index) => {
                      return (
                        <TimelineItem
                          key={index}
                          title={update.title}
                          time={moment(
                            moment(update.time, "HH:mm:ss").format()
                          ).fromNow()}
                        />
                      );
                    })}
                  {updates.length === 0 && (
                    <Text style={styles.emptyText}>{en.no_updates}</Text>
                  )}
                </View>
              </View>
            </View>
            <View style={styles.secondSection}>
              <View style={styles.appointmentsTitleContainer}>
                <Text style={styles.appointmentsTitle}>
                  {en.audition_appointments_title}
                </Text>
              </View>
              <ScrollView>
                <View style={styles.appointmentsContent}>
                  <DraggableFlatList
                    showsVerticalScrollIndicator={false}
                    // style={styles.customFlat}
                    data={appointments}
                    renderItem={({ item, move, moveEnd, isActive }) => {
                      return (
                        <TouchableOpacity
                          onLongPress={move}
                          onPressOut={moveEnd}
                        >
                          <AppointmentsItem showTime={false} {...item} />
                        </TouchableOpacity>
                      );
                    }}
                    keyExtractor={(item, index) => `draggable-item-${index}`}
                    scrollEnabled={false}
                    scrollPercent={5}
                    onMoveEnd={this.onMoveEnd}
                  />
                  <DraggableFlatList
                    showsVerticalScrollIndicator={false}
                    // style={styles.customFlat}
                    data={slots}
                    renderItem={({ item, move, moveEnd, isActive }) => {
                      return (
                        <TouchableOpacity
                          onLongPress={move}
                          onPressOut={moveEnd}
                        >
                          <View style={styles.timeContainer}>
                            <Text style={styles.schedule}>{item.time}</Text>
                          </View>
                        </TouchableOpacity>
                      );
                    }}
                    keyExtractor={(item, index) => `draggable-item-${index}`}
                    scrollEnabled={false}
                    scrollPercent={5}
                    onMoveEnd={this.onSlotsMoveEnd}
                  />
                  {/* <View style={styles.timeFix}>
                  {slots.map(snap => {
                    return (
                      <View style={styles.timeContainer}>
                        <Text style={styles.schedule}>{snap.time}</Text>
                      </View>
                    );
                  })}
                </View>  */}
                </View>
              </ScrollView>
            </View>
          </View>
        </Content>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.data,
  updatesPin: state.manage.updatesPin
});

export default connect(mapStateToProps)(MonitorMode);
