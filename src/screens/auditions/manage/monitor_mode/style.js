import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, width, height, NEXABOLD, NEXALIGHT } from '../../../../assets/styles';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    flexDirection: 'row'
  },
  wrapperContent: {
    flex: 1,
    flexDirection: 'row',
  },
  firstSection: {
    width: resize(741),
    height: '100%',
    backgroundColor: '#f0f0f0',
  },
  updatesListContainer: {
    marginHorizontal: resize(66),
    marginTop: resize(60, 'height'),
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  timelineContainer: {
    paddingTop: resize(87, 'height'),
    flex: 1
  },
  timelineTitle: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(24),
    fontWeight: '400',
    paddingLeft: resize(161),
    paddingRight: resize(155),
  },
  timelineContent: {
    marginTop: resize(15, 'height'),
    paddingTop: resize(20, 'height'),
    paddingLeft: resize(161),
    paddingRight: resize(155),
  },
  timelineList: {
    marginBottom: resize(125, 'height')
  },
  secondSection: {
    width: resize(371),
    height: '100%',
    backgroundColor: '#fff',
  },
  appointmentsTitleContainer: {
    flexDirection: 'row',
    height: '20%',
    paddingTop: resize(45, 'height'),
    paddingBottom: resize(20, 'height'),
    paddingHorizontal: resize(90),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: resize(10, 'height'),
  },
  appointmentsTitle: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(24),
    fontWeight: '400',
    textAlign: 'center'
  },
  appointmentsContainer: {
    paddingTop: resize(38, 'height')
  },
  appointmentsContent: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '80%',
    marginBottom: resize(175, 'height'),
    paddingHorizontal: resize(15),
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  sendUpdateContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  sendButtonContainer: {
    width: resize(234),
    height: resize(48, 'height'),
  },
  sendButtonGradient: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 28,
  },
  monitorModeText: {
    paddingHorizontal: resize(50),
    marginTop: resize(10, 'height'),
    color: '#000000',
    fontSize: resize(13),
    fontWeight: '400',
    lineHeight: resize(18),
    textAlign: 'center',
    ...Platform.select({
      android: {
        paddingBottom: resize(40, 'height')
      }
    })
  },
  inputNotificationContainer: {
    borderRadius: 24,
    borderColor: '#4d2545',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    width: resize(275),
    height: resize(49, 'height'),
    marginRight: resize(25),
    marginBottom: resize(22, 'height'),
    flexDirection: 'row',
  },
  inputNotification: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(16),
    fontWeight: '400',
    paddingLeft: resize(20)
  },
  emptyText: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(17),
    fontWeight: '400',
  },
  pinLabelContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: resize(15, 'height')
  },
  pin: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(18),
    fontWeight: '400',
  },
  timeFix: {
    width: resize(110),
    height: '100%',
    alignItems: 'flex-end'
    // paddingTop: 20
  },
  timeContainer: {
    width: '100%',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginBottom: resize(20, 'height'),
    borderBottomColor: '#F0F0F0',
    borderBottomWidth: 1
  },
  schedule: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
    textAlign: 'right',
    paddingBottom: resize(5, 'height')
  }
});

export default styles;