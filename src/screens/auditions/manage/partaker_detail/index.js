import React, { Component } from "react";
import {
  View,
  Text,
  Platform,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  Animated,
  Linking,
  RefreshControl,
  TextInput,
  FlatList,
  Keyboard,
} from "react-native";
import { Container, Content, Input } from "native-base";
import {
  StackActions,
  NavigationActions,
  NavigationEvents,
} from "react-navigation";
import { connect } from "react-redux";
import Video from "react-native-video";
import {
  resize,
  NEXALIGHT,
  NEXABOLD,
  STATUS_BAR_HEIGHT,
} from "../../../../assets/styles";

//custom
import styles from "./style";
import LightBar from "../../../../components/commons/light_bar";
import Header from "../../../../components/commons/header";
import BackButton from "../../../../components/commons/back_button";
import HeaderText from "../../../../components/commons/header_text";
import HeaderBorderedButton from "../../../../components/commons/header_bordered_button";
import GradientButton from "../../../../components/commons/gradient_button";
import PartakerCard from "../../../../components/auditions/partaker_card";
import EmojiButton from "../../../../components/auditions/emoji_button";
import StarEmoji from "../../../../assets/icons/auditions/star-emoji.png";
import SmileEmoji from "../../../../assets/icons/auditions/smile-emoji.png";
import GoodEmoji from "../../../../assets/icons/auditions/good-emoji.png";
import DoubtEmoji from "../../../../assets/icons/auditions/doubt-emoji.png";
import BadEmoji from "../../../../assets/icons/auditions/bad-emoji.png";
import Plus from "../../../../assets/icons/auditions/icon-plus.png";
import Search from "../../../../assets/icons/search/icon-search.png";
import FeedbackButton from "../../../../components/auditions/feedback_button";
import RoleSelected from "../../../../components/auditions/role_selected";
import FeedbackItem from "../../../../components/auditions/feedback_item";
import Calendar from "../../../../components/commons/calendar_list";
import PartakerModal from "../../partaker_modal";
import ModalMarket from "../../partaker_modal/marketplace";
import recordVideo from "../../../../utils/record_video";
import parseDataModalPartaker from "../../../../utils/auditions/parse_data_modal_partaker";
import Loading from "../../../../components/commons/loading";
import showMessage from "../../../../utils/show_message";
import parseError from "../../../../utils/parse_error";
import uploadAsset from "../../../../utils/upload_asset";
import Base64 from "../../../../../Base64";

//locale
import en from "../../../../locale/en";

//api
import {
  getPerformanceProfile,
  getTeamFeedbacks,
  createFeedback,
  createAuditionVideo,
  getPerformanceCalendar,
  addTagfeedback,
  addRecommendationfeedback,
  getUserInstantFeedback,
  getUserTags,
  updateInstantFeedback,
  updateTags,
  updateMarkets,
  getUserMarket,
  removePastTag,
  removePastTagMarket,
  getOnlineDocs,
  assignNumber,
  getAllAuditionUsers,
} from "../../../../api/auditions";
import Tags from "../../../../components/auditions/tags";
import VideoOnline from "../../../../components/commons/videoOnline";
import File from "../../../../components/auditions/file";
import FileMenu from "../../../../components/auditions/file_menu";
import shareContent from "../../../../utils/share_content";
import { Assets } from "../../../../assets";
import { ProcessingManager } from "react-native-video-processing";
import RenameModal from "../../../../components/auditions/rename_file";
import RNThumbnail from "react-native-thumbnail";
import RangeSlider from "rn-range-slider";
import Slider from "react-native-slider";
import colors from "../../../../utils/colors";
import Pdf from "react-native-pdf";
import { DEVICE, setUrl } from "../../../../utils/constant";
import { general_styles } from "../../../../utils/general_style";
import firebase from "react-native-firebase";
import settings, { webUrl } from "../../../../../settings";
import moment from "moment";
import user from "../../../../reducers/user";
import Share from "../../../../components/commons/share";
import { sharePerformance } from "../../../../api/talents";
import validateEmail from "../../../../utils/validate_email";
import CardItem from "../../../../components/auditions/card_item";
import AppearanceIcon from "../../../../assets/icons/auditions/appearance-icon.png";
import VideoIcon from "../../../../assets/icons/auditions/video-icon.png";
import MusicIcon from "../../../../assets/icons/auditions/music-icon.png";
import PhotoIcon from "../../../../assets/icons/auditions/photo-icon.png";
import SheetIcon from "../../../../assets/icons/auditions/sheet-icon.png";
import Music from "../../../talent_database/allMediaModal/Music";

export const HeaderIcons = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <Image
        source={props.image}
      // style={{ width: 25, height: 25 }} resizeMode={'contain'}
      />
    </TouchableOpacity>
  );
};

const headerHeight =
  Platform.OS === "ios"
    ? resize(82 + 20, "height")
    : resize(82 + STATUS_BAR_HEIGHT, "height");

class PartakerDetail extends Component {
  constructor(props) {
    super(props);
    this.intervalID = undefined;
    // console.log('this.props.user.user_id :>> ', this.props.user.id);
    // // console.log('this.props.user.user_id parse:>> ', parseInt(this.props.user.user_id));
    this.refFireStore = firebase.firestore().collection("audition_chats");
    this.scrollOffset = 0;
    this.keyboardHeight = 0;
    console.log("USER ID DETAILS=========>", this.props);
    let partakers = this.props.navigation.getParam("partakers", []);
    // let partakers = this.props.navigation.getParam("user", []);
    let indexPartaker = this.props.navigation.getParam("indexPartaker", 0);

    this.state = {
      refresh: false,
      partakers,
      indexPartaker: this.props.navigation.getParam("indexPartaker", 0),
      audition: this.props.navigation.getParam("audition", ""),
      online: this.props.navigation.getParam("online", ""),
      audition_id: this.props.navigation.getParam("audition_id", ""),
      actualRound: this.props.navigation.getParam("actualRound", ""),
      contract: this.props.navigation.getParam("contract", ""),
      // user_id: partakers.user_id,
      // slot_id: partakers.slot_id,
      // user_image: partakers.image,
      // role_id: partakers.rol,
      user_id: partakers[indexPartaker].user_id,
      slot_id: partakers[indexPartaker].slot_id,
      user_image: partakers[indexPartaker].image,
      role_id: partakers[indexPartaker].rol,
      auditionName: this.props.navigation.getParam("auditionName", ""),
      feedbackEmoji: null,
      callback: null,
      workOn: "",
      favorited: false,
      roles: this.props.navigation.getParam("roles", []),
      teamFeedback: [],
      partakerDetailModal: false,
      partakerDetailModalSectionSelected: "Info",
      video: null,
      modalData: [],
      name: "",
      stageName: "",
      city: "",
      loading: true,
      loadingButtonSend: false,
      loadingButtonVideo: false,
      dates: [],
      tags: [],
      actualTags: "",
      viewSplitAnimatedValue: new Animated.Value(-resize(405)),
      isOpen: false,
      comment: "",
      actualComment: "",
      isSearching: false,
      markets: [],
      isEdit: false,
      videos: [],
      documents: [],
      showFileMenu: false,
      optionsPosition: { py: 0, px: 0 },
      fileSelected: {},
      showVideo: [],
      assignNumber: "",
      isAssignDisable: false,
      details: {},
      fileName: "",
      isVisible: false,
      value: 0,
      isShowResume: false,
      isShowChat: false,
      message: "",
      messageList: [],
      auditionUser: [],
      showShare: false,
      shareEmail: "",
      isSending: false,
      resume: "",
      shareCode: "",
      email: "",
      isMusicModal: false,
      selectedMediaType: "",
      modalTitle: "",
      recommendation: "",
    };
  }

  handleNextData = () => {
    let { partakers, indexPartaker } = this.state;
    this.resetData();
    let acum = indexPartaker + 1;
    if (acum < partakers.length) {
      this.setState(
        {
          user_id: partakers[acum].user_id,
          slot_id: partakers[acum].slot_id,
          user_image: partakers[acum].image,
          role_id: partakers[acum].rol,
          indexPartaker: acum,
        },
        () => {
          this.handleDataUser();
        }
      );
    } else {
      this.setState(
        {
          user_id: partakers[0].user_id,
          slot_id: partakers[0].slot_id,
          user_image: partakers[0].image,
          role_id: partakers[0].rol,
          indexPartaker: 0,
        },
        () => {
          this.handleDataUser();
        }
      );
    }
  };

  async componentDidMount() {
    const currentChatPath = `${settings.chatPrefix}${this.state.audition}`;
    console.log("currentChatPath :>> ", currentChatPath);
    this.auditionChatRef = this.refFireStore.doc(currentChatPath);
    await this.auditionChatRef
      .get()
      .then((doc) => {
        if (!doc.exists) {
          console.log("initializeChat -> create doc");
          this.auditionChatRef.set({});
        } else {
          console.log("initializeChat -> already exist doc");
        }
      })
      .catch((err) => {
        console.log("Error getting document", err);
      });
    this.chatManage();
    this.handleDataUser();
    this.keyboardWillShowListener = Keyboard.addListener(
      "keyboardWillShow",
      this._keyboardWillShow
    );
    this.keyboardWillHideListener = Keyboard.addListener(
      "keyboardWillHide",
      this._keyboardWillHide
    );
  }

  getAuditionUserData = (sender_id) => {
    return this.props.navigation.state.params.auditionUser.length > 0
      ? this.props.navigation.state.params.auditionUser.filter(
        (data) => data.id == sender_id
      )
      : [];
  };

  chatManage = async () => {
    this.auditionChatRef
      .collection(`${this.state.audition_id}`)
      .orderBy("createDate", "asc")
      .onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          // console.log("chatManage -> doc id", doc.id)
          // console.log("chatManage -> doc data", doc.data())
          let data = doc.data();
          // data.sender = this.getChatUserDetails(data.sender_id);
          console.log("chatManage -> data", data);
          let userData = this.getAuditionUserData(data.sender_id);
          console.log("userData getAuditionUserData:>> ", userData);
          // data[name] = ""

          if (userData.length > 0) {
            data["name"] =
              userData[0].details.first_name +
              " " +
              userData[0].details.last_name;
            data["image"] = userData[0].image;

            console.log("data 111111 :>> ", data);
          }

          const index =
            this.state.messageList && this.state.messageList.length > 0
              ? this.state.messageList.findIndex((e) => e.id === doc.id)
              : -1;
          console.log("data after image add :>> ", data);
          if (index === -1) {
            this.state.messageList.push({
              id: doc.id,
              ...data,
            });
          } else {
            this.state.messageList[index] = {
              id: doc.id,
              ...data,
            };
          }
        });
        this.setState({ messageList: this.state.messageList, message: "" });
      });
  };

  componentWillUnmount() {
    // console.log('componentWillUnmount :>> ');
    // if (this.intervalID != undefined) {
    //   clearInterval(this.intervalID);
    // }
    this.keyboardWillShowListener.remove();
    this.keyboardWillHideListener.remove();
  }

  onWillBlur = () => {
    console.log(
      "onWillBlur ===========================================================:>> ",
      this.intervalID
    );
    // if (this.intervalID != undefined) {
    //   clearInterval(this.intervalID);
    // }
    this.removeFeedbackInterval();
  };

  startFeedbackInterval = () => {
    if (this.intervalID != undefined) {
      clearInterval(this.intervalID);
    }
    this.intervalID = setInterval(async () => {
      this.fetchTeamFeedback();
    }, 5000);
  };

  removeFeedbackInterval = () => {
    if (this.intervalID != undefined) {
      clearInterval(this.intervalID);
    }
  };

  _keyboardWillShow = (e) => {
    console.log("_keyboardWillShow :>> ");
    this.removeFeedbackInterval();
    this.keyboardHeight = e.endCoordinates
      ? e.endCoordinates.height
      : e.end.height;

    const newOffset = this.scrollOffset + this.keyboardHeight;
    this.refs.flatList.scrollToOffset({ offset: newOffset, animated: true });
  };

  _keyboardWillHide = (e) => {
    console.log("_keyboardWillHide :>> ");
    this.startFeedbackInterval();
    const newOffset = this.scrollOffset - this.keyboardHeight;
    this.refs.flatList.scrollToOffset({ offset: newOffset, animated: true });
  };

  handleScroll = (e) => {
    this.scrollOffset = e.nativeEvent.contentOffset.y;
  };

  handleDataUser = () => {
    let { online } = this.state;
    this.fetchUser();

    // if (this.intervalID != undefined) {
    //   clearInterval(this.intervalID)
    // }
    // this.intervalID = setInterval(async () => {
    //   this.fetchTeamFeedback()
    // }, 5000);
    this.startFeedbackInterval();
    this.fetchUserCalendar();
    this.validateUserDataExist();
    if (online === 1) {
      this.fetchUserVideos();
    }
  };

  fetchUserVideos = async () => {
    let { user_id, audition_id } = this.state;
    console.log("Media audition_id===> " + audition_id + "user_id " + user_id);
    try {
      let videos = [];
      let documents = [];
      let resquestItems = await getOnlineDocs({
        performer_id: user_id,
        appointment_id: audition_id,
      });

      if (resquestItems.data.data.length) {
        for (const iterator of resquestItems.data.data) {
          if (iterator.type === "video") {
            iterator.thumbnail =
              "https://images.pexels.com/photos/66134/pexels-photo-66134.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";
            videos.push(iterator);
          } else {
            iterator.showOptions = true;
            documents.push(iterator);
          }
        }
      } else {
        for (const key in resquestItems.data.data) {
          let iterator = resquestItems.data.data[key];
          if (iterator.type === "video") {
            if (!iterator.thumbnail) {
              iterator.thumbnail =
                "https://images.pexels.com/photos/66134/pexels-photo-66134.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";
            }
            videos.push(iterator);
          } else {
            iterator.showOptions = true;
            documents.push(iterator);
          }
        }
      }
      console.log("videos", videos);
      console.log("documents", documents);
      this.setState({ videos, documents });
    } catch (error) {
      console.log({ videos: [], documents: [] });
    }
  };

  resetData = () => {
    this.setState({
      feedbackEmoji: null,
      callback: null,
      workOn: "",
      favorited: false,
      actualComment: "",
    });
  };

  transformBadResponseFromBackEnd = (data) => {
    let { user_id } = this.state;
    let dataGoog = [];
    for (const iterator of data) {
      let temp = {
        marketplace_id: iterator.markeplace.id,
        id: iterator.id,
        user_id,
        appointment_id: this.state.audition_id,
        markeplace: {
          title: iterator.markeplace.title,
        },
      };
      dataGoog.push(temp);
    }
    return dataGoog;
  };

  validateUserDataExist = async () => {
    let { audition_id, user_id, audition } = this.state;
    try {
      let data = { audition_id, user_id };
      let data2 = { audition_id: audition, user_id };
      let responseIns = await getUserInstantFeedback(data);
      let responseTag = await getUserTags(data);
      let responseMark = await getUserMarket(data2);
      let {
        evaluation,
        callback,
        work,
        favorite,
        comment,
        rating,
        recommendation,
        simple_feedback,
      } = responseIns.data.data;
      console.log("FeedBAckData=====>" + JSON.stringify(responseIns));
      console.log("FeedBAckData2=====>" + evaluation);
      this.setState({
        feedbackEmoji: evaluation,
        callback: simple_feedback
          ? simple_feedback === en.called_back_title
            ? 0
            : simple_feedback === en.on_file_title
              ? 1
              : simple_feedback === en.not_today_title
                ? 2
                : callback === 1
                  ? true
                  : false
          : null,
        workOn: this.capitalize(work),
        favorited: favorite,
        actualComment: comment,
        loading: false,
        isEdit: true,
        tags: responseTag.data.data,
        markets: this.transformBadResponseFromBackEnd(responseMark.data.data),
        value: rating,
        recommendation: recommendation,
      });
      console.log("FeedBAckData3=====>" + this.state.feedbackEmoji);
    } catch (error) {
      this.setState({
        loading: false,
        isEdit: false,
        tags: [],
        markets: [],
        value: 0,
        recommendation: ""
      });
    }
  };

  capitalize = (s) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  fetchUser = async () => {
    const { user_id, audition_id } = this.state;
    try {
      this.setState({ loading: true });
      const response = await getPerformanceProfile(user_id, audition_id);
      console.log(" USER INFO ======>", response.data.data.assign_number);
      // if (response.data.data.assign_number != null) {

      this.state.assignNumber =
        response.data.data.assign_number != null
          ? response.data.data.assign_number
          : "";
      this.state.isAssignDisable = response.data.data.assign_number != null;
      this.state.details = response.data.data.details;
      this.state.resume = response.data.data.resume;
      this.state.shareCode = response.data.data.share_code;
      this.state.email = response.data.data.email;
      const details = response.data.data.details;
      const name = `${details.first_name} ${details.last_name}`;
      const stageName = details.stage_name;
      const city = details.city;
      const modalData = parseDataModalPartaker(response.data.data);
      modalData.splice(0, 3);
      console.log(
        "modalData =========================================:>> ",
        modalData
      );
      this.setState({ modalData, name, stageName, city });
      console.log("FetchUser SUCCESS MANAGE=====>", response);
    } catch (error) {
      console.log("FetchUser ERROR MANAGE=====>", error);
    }
  };

  fetchTeamFeedback = async () => {
    console.log("fetchTeamFeedback :>> ");
    const { user_id, audition_id } = this.state;
    try {
      const params = { appointment_id: audition_id, performer: user_id };
      const response = await getTeamFeedbacks(params);
      this.setState({ teamFeedback: response.data.data, refresh: false });
    } catch (error) {
      console.log(error);
    }
  };

  onTeamFeedbackRefresh = () => {
    this.state.teamFeedback = [];
    this.fetchTeamFeedback();
    // this.setState({ refresh: true }, () => {
    //   this.fetchTeamFeedback()
    // })
  };

  fetchUserCalendar = async () => {
    const { user_id } = this.state;
    try {
      const response = await getPerformanceCalendar(user_id);
      this.setState({
        dates: response.data.data,
      });
      console.log("CalenderData" + JSON.stringify(response.data.data));
    } catch (error) {
      console.log("CalenderData Error ===>" + error);
      console.log(error);
    }
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  onPressRecordAudition = async () => {
    try {
      const response = await recordVideo();
      let uri = Platform.OS === "android" ? response.path : response.uri;
      let result = await RNThumbnail.get(uri);
      response["thumbnail"] = result.path;
      console.log(" record response ", response);
      this.setState({ video: response, isVisible: true });
      // if (Platform.OS === "android") {
      //   path = response.path;
      // } else {
      //   path = response.uri;
      // }
      // // this.setState({ loadingButtonVideo: true });
      // // this.setState({ video: response }, () => this.uploadAuditionVideo());
      // this.setState({ video: response }, () => this.compressVideo(path));
      // this.setState({ video: response }, () => this.uploadAuditionVideo());
    } catch (error) {
      console.log(error);
    }
  };

  compressVideo(source) {
    const options = {
      // width: 720,
      // height: 1280,
      bitrateMultiplier: 3,
      saveToCameraRoll: true, // default is false, iOS only
      saveWithCurrentDate: true, // default is false, iOS only
      minimumBitrate: 300000,
      removeAudio: false, // default is false
    };
    ProcessingManager.compress(source, options) // like VideoPlayer compress options
      .then((data) => {
        this.setState({ loadingButtonVideo: false });
        console.log("Compress result", data);
        let path = data;
        if (Platform.OS === "android") {
          path = path.source;
        }
        this.uploadAuditionVideo(path);
      })
      .catch((error) => {
        console.log("Compress error", error);
        this.setState({ loadingButtonVideo: false });
      });
  }

  uploadAuditionVideo = async (uri) => {
    const { video, name, audition_id, user_id, slot_id } = this.state;
    // let path = video.uri;
    let path = uri;
    let metadata = {
      contentType: "video/quicktime",
    };
    if (Platform.OS === "android") {
      // path = video.path;
      metadata["contentType"] = "video/mp4";
    }
    this.setState({ loadingButtonVideo: true });
    try {
      const url = await uploadAsset(
        `tablet/auditions/${audition_id}/videos`,
        path,
        name,
        metadata
      );
      const thumbnail = await uploadAsset(
        `tablet/auditions/${audition_id}/videos`,
        video.thumbnail,
        name,
        metadata
      );
      const data = {
        url,
        appointment_id: audition_id,
        performer: user_id,
        slot_id,
        name: this.state.fileName,
        thumbnail,
      };
      const response = await createAuditionVideo(data);
      this.setState({ loadingButtonVideo: false });
      showMessage("success", "Record audition saved");
    } catch (error) {
      this.setState({ loadingButtonVideo: false });
      console.log(error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
    this.setState({ loadingButtonVideo: false });
  };

  onPressStar = () => {
    this.setState({ favorited: !this.state.favorited });
  };

  handlePressEdit = async () => {
    const { user, navigation } = this.props;
    const {
      feedbackEmoji,
      callback,
      workOn,
      audition_id,
      user_id,
      favorited,
      tags,
      actualComment,
      markets,
      audition,
      value,
      slot_id,
      recommendation,
    } = this.state;
    const currentUserId = user.id;
    try {
      const data = {
        user_id,
        evaluation: feedbackEmoji,
        callback,
        work: workOn,
        favorite: favorited,
        comment: actualComment,
        appointment_id: this.state.audition_id,
        rating: parseFloat(value),
        user: user_id,
        slot_id: slot_id,
        evaluator: currentUserId,
        recommendation: recommendation,
        simple_feedback:
          callback == 0
            ? en.called_back_title
            : callback === 1
              ? en.on_file_title
              : callback === 2
                ? en.not_today_title
                : "",
      };

      console.log("save feedback data :>> ", data);
      // return
      this.setState({ loadingButtonSend: true });
      await updateTags(audition_id, tags);
      await updateMarkets(audition, markets);
      console.log("updateInstantFeedback======>" + JSON.stringify(data));
      await updateInstantFeedback(audition_id, data);
      console.log("FAVOURITE EDIT=====>", this.state.favorited);
      this.setState({ loadingButtonSend: false });
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "ManagerAuditionDetail",
            params: {
              audition_id: audition,
            },
          }),
        ],
      });
      // navigation.dispatch(resetAction);

      return showMessage("success", en.feedback_edited);
    } catch (error) {
      console.log(error);
      this.setState({ loadingButtonSend: false });
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  handlePressSave = async () => {
    console.log(" PROPS ", this.props);
    console.log(" STATE ", this.state);

    // const hasError = this.validateFeedback();
    // if (hasError !== "") {
    //   return showMessage("danger", hasError);
    // }
    const {
      feedbackEmoji,
      callback,
      workOn,
      audition_id,
      user_id,
      favorited,
      slot_id,
      tags,
      actualComment,
      markets,
      audition,
      value,
      recommendation,
    } = this.state;
    const { user, navigation } = this.props;
    const currentUserId = user.id;
    this.setState({ loadingButtonSend: true });
    try {
      const data = {
        appointment_id: audition_id,
        user: user_id,
        evaluator: currentUserId,
        evaluation: feedbackEmoji,
        callback,
        work: workOn,
        favorite: favorited,
        slot_id,
        comment: actualComment,
        rating: parseFloat(value),
        recommendation: recommendation,
        simple_feedback:
          callback == 0
            ? en.called_back_title
            : callback === 1
              ? en.on_file_title
              : callback === 2
                ? en.not_today_title
                : "",
      };

      for (const iterator of tags) {
        let valor = await addTagfeedback({
          appointment_id: audition_id,
          user_id,
          title: iterator.title,
        });
        console.log("tags ==>", valor);
      }

      //FOR LIVE SERVER
      for (const iterator of markets) {
        let mar = await addRecommendationfeedback({
          audition_id: audition,
          marketplace_id: iterator.marketplace_id,
          user_id,
          appointment_id: this.state.audition_id,
        });
        console.log("MArketplace ==>", mar);
      }
      console.log("Feedback Comment========>" + JSON.stringify(data));

      //FOR LOCAL SERVER
      // for (const iterator of markets) {
      //   let mar = await addRecommendationfeedback({
      //     audition_id: audition,
      //     marketplace_id: iterator.marketplace_id,
      //     user_id,
      //     appointment_id: this.state.audition_id
      //   });
      //   console.log("MArketplace ==>", mar);
      // }
      //console.log("Feedback Comment========>" + JSON.stringify(data));

      await createFeedback(data);
      this.setState({ isEdit: true });
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "ManagerAuditionDetail",
            params: {
              audition_id: audition,
            },
          }),
        ],
      });
      // navigation.dispatch(resetAction);
      this.setState({ loadingButtonSend: false });
      return showMessage("success", en.feedback_created);
    } catch (error) {
      this.setState({ loadingButtonSend: false });
      console.log(error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
    this.setState({ loadingButtonSend: false });
  };

  validateFeedback = () => {
    const { feedbackEmoji, callback, workOn, actualComment } = this.state;
    let errorMessage = "";
    if (feedbackEmoji === null) {
      errorMessage = en.require_feedback_emoji;
    } else if (callback === null) {
      errorMessage = en.require_callback;
    } else if (workOn === "") {
      errorMessage = en.require_workon;
    }
    return errorMessage;
  };

  handlePressPartakerSection = (section = null) => {
    const { partakerDetailModal } = this.state;
    this.setState({
      partakerDetailModal: !partakerDetailModal,
      partakerDetailModalSectionSelected: section,
    });
  };

  handlePressEmoji = (feedbackEmoji) => {
    this.setState({ feedbackEmoji });
  };

  handlePressCallbackButton = (callback) => {
    let comment = "";
    if (callback === 0) {
      comment =
        "Thank you for auditioning today. We would like to see more. Please be on the lookout for Callback information.";
    } else if (callback === 1) {
      comment =
        "Thank you for attending today's audition. We will keep your information on file for possible future projects. ";
    } else if (callback == 2) {
      comment =
        "Thank you for attending today's audition. That is all we need from you for today. Please feel free to attend our future auditions.";
    }
    this.setState({ callback, actualComment: comment });
  };

  handlePressWorkOnButton = (workOn) => {
    this.setState({ workOn });
  };

  handleSideMenu = () => {
    const { viewSplitAnimatedValue, isOpen } = this.state;
    const toValue = !isOpen ? 0 : -resize(405);
    Animated.timing(viewSplitAnimatedValue, {
      toValue: toValue,
      duration: 300,
    }).start(() => {
      this.setState((prevState) => ({
        isOpen: !prevState.isOpen,
      }));
    });
  };

  openSideMenu = () => {
    const { viewSplitAnimatedValue, isOpen } = this.state;
    // const toValue = !isOpen ? 0 : -resize(405);
    const toValue = 0;
    Animated.timing(viewSplitAnimatedValue, {
      toValue: toValue,
      duration: 300,
    }).start(() => {
      this.setState({ isOpen: true });
    });
  };

  addTags = (state, value) => {
    console.log(" STATE 111 ", state);
    console.log(" VALUE 111 ", value);

    if (value.title !== "") {
      let actualTag = [...this.state[state]];
      actualTag.push(value);
      this.setState({ actualTags: "", [state]: actualTag, isSearching: false });
    }
  };

  removeTags = (index, state) => {
    let actualTag = [...this.state[state]];
    if (
      actualTag[index].id &&
      actualTag[index].appointment_id &&
      state === "tags"
    ) {
      removePastTag(actualTag[index].id);
    }
    if (
      actualTag[index].id &&
      actualTag[index].markeplace &&
      state === "markets"
    ) {
      removePastTagMarket(actualTag[index].id);
    }
    actualTag.splice(index, 1);
    this.setState({ [state]: actualTag });
  };

  onChange = (value, state) => {
    this.setState({ [state]: value });
  };

  addComment = () => {
    let { comment } = this.state;
    if (comment) {
      this.setState({ actualComment: comment, comment: "" });
    }
  };

  removeComment = () => {
    this.setState({ actualComment: "" });
  };

  handleSearchModal = () => {
    this.setState((prevState) => ({
      isSearching: !prevState.isSearching,
    }));
  };

  handleSetRef = (file, index) => {
    this[`file_${index}`] = file;
  };

  onPressFileMore = async (file, index) => {
    try {
      const { showFileMenu } = this.state;
      console.log("file", file);
      await this.handleGetMeasure(file, index);
      this.setState({
        showFileMenu: !showFileMenu,
      });
    } catch (error) {
      console.log(error);
    }
  };

  handleGetMeasure = async (file, index) => {
    await new Promise((resolve, reject) => {
      const refName = `file_${index}`;
      if (refName in this) {
        this[refName].measure((a, b, width, height, px, py) => {
          this.setState(
            {
              optionsPosition: { py, px },
              fileSelected: file,
            },
            () => resolve(true)
          );
        });
      } else {
        this.setState(
          {
            optionsPosition: { py: 0, px: 0 },
            fileSelected: file,
          },
          () => resolve(false)
        );
      }
    });
  };

  handleFileShare = () => { };

  shareAsset = async (file) => {
    try {
      await shareContent(`Open this link: \n ${file.url}`);
    } catch (error) {
      console.log(error);
    }
  };

  onFullscreenPlayerDidDismiss = (index) => {
    let { showVideo } = this.state;
    let copyArray = [...showVideo];
    copyArray[index] = false;
    this.setState({ showVideo: copyArray });
  };

  playVideo = (index, video) => {
    let { showVideo } = this.state;
    let copyArray = [...showVideo];
    if (copyArray[index]) return "";
    copyArray[index] = true;
    if (Platform.OS === "android") {
      VideoPlayer.showVideoPlayer(video.url);
    } else {
      this.setState({ showVideo: copyArray }, () => {
        setTimeout(() => {
          this.videoRef.presentFullscreenPlayer();
        }, 1000);
      });
    }
  };

  openAsset = (file) => {
    return Linking.openURL(file.url);
  };

  getIconText(fileText) {
    let type = fileText.split(".");
    type = type[type.length - 1];
    return type.toUpperCase();
  }

  handleAssignNumber = (text) => {
    this.setState({ assignNumber: text });
  };

  //Assign Number Api
  callAssignNumberApi = async () => {
    const { user_id, audition_id } = this.state;

    if (this.state.assignNumber == "") {
      showMessage("Please enter assign number");
      return;
    }
    let params = {
      appointment_id: audition_id,
      user_id: user_id,
      assign_no: this.state.assignNumber,
    };
    console.log("PARAMS Assign Number Api=====> ", JSON.stringify(params));
    this.setState({ loading: true });

    try {
      const response = await assignNumber(params);
      console.log("SUCCESS Assign Number Api======>", JSON.stringify(response));
      showMessage("success", "Assign number added.");
      this.setState({ loading: false, isAssignDisable: true });
    } catch (error) {
      this.setState({ loading: false });
      console.log("ERROR GROUP LIST API =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  onCloseClick = () => {
    this.setState({ isVisible: false });
  };

  onSubmitClick = () => {
    if (Platform.OS === "android") {
      path = this.state.video.path;
    } else {
      path = this.state.video.uri;
    }
    // this.setState({ loading: true });
    // this.setState({ video: response }, () => this.uploadAuditionVideo());
    this.setState({ loadingButtonVideo: true }, () => this.compressVideo(path));

    this.setState({ isVisible: false });
  };

  onChangeText = (fileName) => {
    this.setState({ fileName });
  };

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    );
  };

  renderItems = ({ item, index }) => {
    return (
      <View style={{ marginVertical: 10, marginHorizontal: 20 }}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Image
            source={{
              uri:
                item.image.thumbnail != null
                  ? item.image.thumbnail
                  : item.image.url,
            }}
            style={{ width: 30, height: 30, borderRadius: 15 }}
          />
          <Text
            style={{
              fontFamily: NEXABOLD,
              fontSize: 10,
              marginHorizontal: 10,
              textTransform: "capitalize",
            }}
          >
            {item.name}
          </Text>
          <Text style={{ fontFamily: NEXALIGHT, fontSize: 10 }}>
            {moment(String(this.chatTimeFormat(item.createDate))).format(
              "MMMM DD, YYYY hh:mm A"
            )}
          </Text>
        </View>
        <Text
          style={{
            fontFamily: NEXALIGHT,
            fontSize: 10,
            marginLeft: 40,
            lineHeight: 15,
          }}
        >
          {item.message}
          {/* {'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam.'} */}
        </Text>
      </View>
    );
  };

  chatTimeFormat(timestamp) {
    // const date = timestamp ?  timestamp.toMillis() : null;
    const date = timestamp ? timestamp.toDate() : null;
    return date;
  }

  onMessageSend = async () => {
    // Keyboard.dismiss();
    if (this.state.message == "") {
      showMessage("danger", "Please enter message to send");
      return;
    }

    let chatMessageDoc = this.auditionChatRef.collection(
      String(this.state.audition_id)
    );
    console.log("chatMessageDoc :>> ", chatMessageDoc);
    await chatMessageDoc.add({
      message: this.state.message,
      sender_id: parseInt(this.props.user.id),
      createDate: new Date(),
      read: false,
    });

    setTimeout(() => {
      this.refs.flatList.scrollToEnd();
    }, 300);
    // this.state.messageList[this.state.messageList.length] = "1"
    // this.setState({ messageList: this.state.messageList, message: '' }, () => {
    // })
  };

  handleShare = () => {
    this.setState((prevState) => {
      return { showShare: !prevState.showShare };
    });
  };

  handleEmail = (shareEmail) => {
    this.setState({ shareEmail });
  };

  sendInvitation = async () => {
    let { shareEmail, isSending, shareCode, details } = this.state;
    console.log("params :>> ", {
      code: shareCode,
      email: shareEmail,
      link: webUrl + "/talent-shared/" + Base64.btoa(String(details.user_id)),
    });

    if (isSending) {
      return;
    }
    this.setState({ isSending: true });
    if (!validateEmail(shareEmail)) {
      this.setState({ isSending: false });
      return showMessage("danger", en.valid_email);
    }
    try {
      await sharePerformance({
        code: shareCode,
        email: shareEmail,
        link: webUrl + "/talent-shared/" + Base64.btoa(String(details.user_id)),
      });
      this.setState({ isSending: false, shareEmail: "" });
      this.handleShare();
      showMessage("success", en.performer_shared);
    } catch (error) {
      console.log("error :>> ", error);
      this.setState({ isSending: false });
      const parsedError = parseError(error);

      showMessage("danger", parsedError);
      // showMessage("danger", "There was an error sending the code");
    }
  };

  renderMediaItemModal = () => {
    const { user_id } = this.state;
    return (
      <Music
        show={this.state.isMusicModal}
        userId={user_id}
        title={this.state.modalTitle}
        mediaType={this.state.selectedMediaType}
        onCloseClick={() => {
          this.setState({ isMusicModal: false });
        }}
      />
    );
  };

  render() {
    const {
      feedbackEmoji,
      callback,
      workOn,
      favorited,
      roles,
      role_id,
      teamFeedback,
      partakerDetailModal,
      partakerDetailModalSectionSelected,
      loading,
      modalData,
      name,
      city,
      user_image,
      stageName,
      auditionName,
      loadingButtonSend,
      loadingButtonVideo,
      dates,
      viewSplitAnimatedValue,
      actualTags,
      tags,
      actualComment,
      comment,
      isSearching,
      markets,
      isEdit,
      user_id,
      online,
      documents,
      videos,
      showFileMenu,
      fileSelected,
      optionsPosition,
      showVideo,
      indexPartaker,
      isShowResume,
      isShowChat,
      showShare,
      shareEmail,
      isSending,
      audition,
      actualRound,
    } = this.state;
    console.log("audition :>> ", audition);
    const widthFirstContainer = viewSplitAnimatedValue.interpolate({
      inputRange: [-resize(405), 0],
      outputRange: [-resize(405), 0],
    });

    let newRoles = [];
    if (roles != undefined && roles.length > 0) {
      this.selected = this.props.navigation.state.params.partakers[
        indexPartaker
      ].rol.split(",");

      roles.map((data) => {
        this.selected.map((item) => {
          if (item == data.id) {
            newRoles[newRoles.length] = data;
          }
        });
      });
    }
    let isManger = this.props.navigation.getParam("isManger", true);

    if (loading) {
      return <Loading />;
    }
    console.log(this.state.contract);
    console.log("Team Feedback");
    console.log(JSON.stringify(teamFeedback));
    console.log(JSON.stringify(this.state.actualRound));
    const source = { uri: this.state.resume, cache: true };
    return (
      <Container style={styles.container}>
        <NavigationEvents onWillBlur={this.onWillBlur} />
        <LightBar />
        {this.renderRenameModal()}
        {this.renderMediaItemModal()}
        <Share
          sendInvitation={this.sendInvitation}
          style={{ right: resize(110) }}
          isSending={isSending}
          handleEmail={this.handleEmail}
          email={shareEmail}
          handleShare={this.handleShare}
          showShare={showShare}
          headerHeight={headerHeight}
        />
        <FileMenu
          show={showFileMenu}
          dismiss={() => this.onPressFileMore({})}
          posy={optionsPosition.py}
          posx={optionsPosition.px}
          showOptionShare
          share={() => this.shareAsset(fileSelected)}
          showOptionOpen
          openIn={() => this.openAsset(fileSelected)}
        >
          {"name" in fileSelected && (
            <File
              activeOpacity={1}
              filename={fileSelected.name}
              fileType={"doc"}
              type={this.getIconText(fileSelected.name)}
              onPressMore={() => this.onPressFileMore({})}
            />
          )}
        </FileMenu>
        <View style={{ position: "absolute", zIndex: 100 }}>
          <Header
            left={<BackButton onPress={() => this.handlePressBackButton()} />}
            center={<HeaderText title={auditionName} />}
            right={
              // <HeaderBorderedButton
              //   title={en.record_audition_button_text}
              //   onPress={() => this.onPressRecordAudition()}
              //   onPressStar={() => this.onPressStar()}
              //   showIconStar={favorited ? false : true}
              //   showFavoriteIcon={favorited ? true : false}
              //   loading={loadingButtonVideo}
              //   online={online}
              // />
              <View style={styles.headerContainer}>
                {online !== 1 ? (
                  <HeaderIcons
                    image={Assets.camera}
                    onPress={() => this.onPressRecordAudition()}
                  />
                ) : null}
                <HeaderIcons
                  image={Assets.chat}
                  onPress={() => {
                    // this.handleSideMenu();
                    this.openSideMenu();
                    this.setState({ isShowChat: true });
                  }}
                />
                <HeaderIcons image={Assets.share} onPress={this.handleShare} />
                <HeaderIcons
                  image={favorited ? Assets.starSelected : Assets.star}
                  onPress={() => this.onPressStar()}
                />
              </View>
            }
          />
        </View>

        {/* Principal information */}
        <KeyboardAvoidingView
          behavior="position"
          style={styles.principalInformation}
        >
          <View style={styles.displayColums}>
            {/* Calendar */}
            <View style={styles.availabilityContainer}>
              <Text style={styles.titles}>{en.available_title}</Text>
              <Calendar
                calendarWidth={styles.availabilityContainer.width}
                dates={dates}
              />
            </View>

            {/* Roles */}
            <View style={styles.rolesContainer}>
              <Text style={styles.titles}> {en.roles_title}</Text>
              <Content
                showsVerticalScrollIndicator={false}
                style={styles.rolesContent}
              >
                <View style={styles.roles}>
                  {newRoles.map((role, index) => {
                    return (
                      <RoleSelected
                        key={index}
                        name={role.name}
                        image={role.image.url}
                        selected={role_id === role.id}
                      />
                    );
                  })}
                </View>
              </Content>
            </View>

            {/* Team Feedback  */}
            <View style={styles.teamFeedbackContainer}>
              <Text style={styles.titles}>
                {online === 0 ? en.team_feedback_title : en.audition_docs}{" "}
              </Text>
              {online === 1 ? (
                documents.length > 0 || videos.length > 0 ? (
                  <>
                    <ScrollView
                      horizontal
                      bounces={false}
                      // refreshControl={
                      //   <RefreshControl
                      //     refreshing={this.state.refresh}
                      //     // onRefresh={() => this.onRefresh()}
                      //     // tintColor="red"
                      //   />
                      // }
                      showsHorizontalScrollIndicator={false}
                      contentContainerStyle={styles.documentsContent}
                    >
                      {videos.map((video, index) => {
                        return (
                          <>
                            <VideoOnline
                              hasButtom
                              key={`video1${index}`}
                              thumbnail={video.thumbnail}
                              onPress={() => this.playVideo(index, video)}
                              videoName={
                                video.filename ? video.filename : video.name
                              }
                            />
                            {showVideo[index] ? (
                              <Video
                                fullscreenOrientatio="landscape"
                                key={`video2${index}`}
                                fullscreen
                                onFullscreenPlayerDidDismiss={() =>
                                  this.onFullscreenPlayerDidDismiss(index)
                                }
                                ignoreSilentSwitch="ignore"
                                playInBackground={false}
                                playWhenInactive={false}
                                source={{ uri: video.url }}
                                ref={(ref) => (this.videoRef = ref)}
                              />
                            ) : null}
                          </>
                        );
                      })}
                    </ScrollView>
                    <ScrollView
                      horizontal
                      bounces={false}
                      showsHorizontalScrollIndicator={false}
                      contentContainerStyle={styles.documentsContent}
                    >
                      {documents.map((doc, index) => {
                        return (
                          <File
                            type={this.getIconText(doc.name)}
                            key={`docs${index}`}
                            filename={doc.name}
                            fileType={"doc"}
                            file={doc}
                            onPressMore={() => this.onPressFileMore(doc, index)}
                            refs={(fileRef) =>
                              this.handleSetRef(fileRef, index)
                            }
                            containerStyle={styles.fileContainer}
                          />
                        );
                      })}
                    </ScrollView>
                  </>
                ) : (
                    <View style={styles.emptyContainer}>
                      <Text style={styles.emptyText}>{en.no_documents}</Text>
                    </View>
                  )
              ) : teamFeedback.length > 0 ? (
                <Content
                  style={styles.teamFeedbackContent}
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.refresh}
                      onRefresh={() => this.onTeamFeedbackRefresh()}
                    // tintColor="red"
                    />
                  }
                >
                  {teamFeedback.map((feedback, index) => {
                    return (
                      <FeedbackItem
                        key={index}
                        contributorName={feedback.evaluator_name}
                        icon={feedback.evaluation}
                        callback={feedback.callback}
                        workOn={feedback.work}
                        rating={feedback.rating}
                        round={feedback.round}
                        simple_feedback={feedback.simple_feedback}
                      />
                    );
                  })}
                </Content>
              ) : (
                    <Content
                      style={styles.teamFeedbackContent}
                      contentContainerStyle={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      showsVerticalScrollIndicator={false}
                      refreshControl={
                        <RefreshControl
                          refreshing={this.state.refresh}
                          onRefresh={() => this.onTeamFeedbackRefresh()}
                        // tintColor="red"
                        />
                      }
                    >
                      <View style={styles.emptyContainer}>
                        <Text style={styles.emptyText}>{en.no_team_feedback}</Text>
                      </View>
                    </Content>
                  )}
            </View>
          </View>
          <View style={styles.displayColums}>
            {/* Instant Feedback */}
            <View style={styles.instanFeedbackScroll}>
              {this.state.contract != "ACADEMIC" && actualRound.round &&
                actualRound.round !== 1 && (
                  <Text style={styles.titles}> {en.feeback_title} </Text>
                )}
              <ScrollView
                showsVerticalScrollIndicator={false}
                bounces={false}
                contentContainerStyle={styles.instantFeedbackContainer}
              >
                {this.state.contract != "ACADEMIC"
                  ? actualRound.round &&
                  actualRound.round !== 1 && (
                    <View style={styles.feedbackTitleContainer}>
                      <View style={styles.feedbackEmojisContainer}>
                        <EmojiButton
                          emoji={StarEmoji}
                          onPress={() => this.handlePressEmoji(1)}
                          selected={feedbackEmoji === 1}
                        />
                        <EmojiButton
                          emoji={SmileEmoji}
                          onPress={() => this.handlePressEmoji(2)}
                          selected={feedbackEmoji === 2}
                        />
                        <EmojiButton
                          emoji={GoodEmoji}
                          onPress={() => this.handlePressEmoji(3)}
                          selected={feedbackEmoji === 3}
                        />
                        <EmojiButton
                          emoji={DoubtEmoji}
                          onPress={() => this.handlePressEmoji(4)}
                          selected={feedbackEmoji === 4}
                        />
                        <EmojiButton
                          emoji={BadEmoji}
                          onPress={() => this.handlePressEmoji(5)}
                          selected={feedbackEmoji === 5}
                        />
                      </View>
                    </View>
                  )
                  : null}
                <View style={styles.callbackContainer}>
                  {/* {actualRound.round && actualRound.round !== 1 && ( */}
                  <Text style={styles.titles}> {en.callback_title} </Text>
                  {/* )} */}
                  <View
                    style={[
                      styles.callbackButtons,
                      actualRound.round
                        ? actualRound.round === 1
                          ? {
                            paddingLeft: 3,
                            paddingRight: 3,
                            marginBottom: resize(36, "height"),
                          }
                          : {}
                        : {},
                    ]}
                  >
                    {actualRound.round && actualRound.round !== 1 && (
                      <FeedbackButton
                        buttonText={en.yes_button_title}
                        onPress={() => this.handlePressCallbackButton(true)}
                        selected={callback === true}
                      />
                    )}
                    {actualRound.round && actualRound.round !== 1 && (
                      <FeedbackButton
                        buttonText={en.no_button_title}
                        onPress={() => this.handlePressCallbackButton(false)}
                        selected={callback === false}
                      />
                    )}
                    {actualRound.round && actualRound.round === 1 && (
                      <FeedbackButton
                        buttonText={en.called_back_title}
                        onPress={() => this.handlePressCallbackButton(0)}
                        selected={callback === 0}
                      />
                    )}
                    {actualRound.round && actualRound.round === 1 && (
                      <FeedbackButton
                        buttonText={en.on_file_title}
                        onPress={() => this.handlePressCallbackButton(1)}
                        selected={callback === 1}
                      />
                    )}
                    {actualRound.round && actualRound.round === 1 && (
                      <FeedbackButton
                        buttonText={en.not_today_title}
                        onPress={() => this.handlePressCallbackButton(2)}
                        selected={callback === 2}
                      />
                    )}
                  </View>
                </View>
                {actualRound.round && actualRound.round !== 1 && (
                  <View style={styles.workOnContainer}>
                    <Text style={styles.titles}> {en.work_on_title} </Text>
                    <View style={styles.workOnButtons}>
                      <FeedbackButton
                        buttonText={en.vocals_title}
                        onPress={() =>
                          this.handlePressWorkOnButton(en.vocals_title)
                        }
                        selected={workOn === en.vocals_title}
                      />
                      <FeedbackButton
                        buttonText={en.acting_title}
                        onPress={() =>
                          this.handlePressWorkOnButton(en.acting_title)
                        }
                        selected={workOn === en.acting_title}
                      />
                      <FeedbackButton
                        buttonText={en.dancing_title}
                        onPress={() =>
                          this.handlePressWorkOnButton(en.dancing_title)
                        }
                        selected={workOn === en.dancing_title}
                      />
                    </View>
                  </View>
                )}
                <View
                  style={{
                    paddingLeft: resize(20),
                    paddingRight: resize(17),
                    marginTop: 20,
                  }}
                >
                  <Text>{"Rate"}</Text>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Text style={{ color: colors.PRIMARY }}>{"0"}</Text>
                    <Slider
                      style={{ flex: 1, marginHorizontal: 10 }}
                      trackStyle={{
                        backgroundColor: colors.BORDER_COLOR,
                        height: 1,
                      }}
                      thumbStyle={{ backgroundColor: colors.PRIMARY }}
                      value={this.state.value}
                      minimumValue={0}
                      maximumValue={10}
                      animateTransitions={true}
                      step={0.5}
                      onValueChange={(value) => {
                        console.log("value :>> ", value);
                        this.setState({ value });
                      }}
                    />
                    <Text style={{ color: colors.PRIMARY }}>{"10"}</Text>
                  </View>
                  <Text style={{ textAlign: "center", color: colors.PRIMARY }}>
                    {this.state.value}
                  </Text>
                </View>
                {!actualComment ? (
                  <View style={styles.teamFeedbackContent}>
                    <Tags onPress={this.removeComment} title={actualComment} />
                  </View>
                ) : null}
                <View style={[styles.comentInput, { height: 100 }]}>
                  {/* <TouchableOpacity
                    onPress={this.addComment}
                    style={styles.addTag}
                  >
                    <Image resizeMode="contain" source={Plus} />
                  </TouchableOpacity> */}
                  <Input
                    value={this.state.actualComment}
                    onChangeText={(value) =>
                      this.setState({ actualComment: value })
                    }
                    style={{ color: "#5f2543", height: 100, marginTop: 10 }}
                    multiline={true}
                    placeholderTextColor="#5f2543"
                    //Modify comment
                    placeholder={!actualComment ? "Add Comment" : "Add Comment"}
                  />
                </View>
              </ScrollView>
            </View>

            {/* Tags */}
            <View style={styles.tagsContainer}>
              <Text style={styles.titles}> {en.tags_title} </Text>
              <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
                <View style={styles.comentInput}>
                  <TouchableOpacity
                    style={styles.addTag}
                    onPress={() =>
                      this.addTags("tags", {
                        title: actualTags,
                        id: null,
                        user_id,
                      })
                    }
                  >
                    <Image resizeMode="contain" source={Plus} />
                  </TouchableOpacity>
                  <Input
                    value={actualTags}
                    onChangeText={(value) => this.onChange(value, "actualTags")}
                    style={{ color: "#5f2543" }}
                    placeholderTextColor="#5f2543"
                    placeholder="Add Tags"
                  />
                </View>
                {tags.map((snap, index) => (
                  <Tags
                    key={"tags" + index}
                    onPress={() => this.removeTags(index, "tags")}
                    title={snap.title}
                  />
                ))}
                {tags.length === 0 && (
                  <Text style={styles.emptyText}>{en.empty_tags}</Text>
                )}
              </ScrollView>
            </View>

            <View style={{ justifyContent: "space-between" }}>
              {/* Recomendation */}
              <View style={styles.recomendationContainer}>
                {/* <Text style={styles.titles}>{en.title_recome} </Text> */}
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  bounces={false}
                >
                  {/* <TouchableOpacity
                    onPress={this.handleSearchModal}
                    style={styles.comentInput}
                  >
                    <Image resizeMode="contain" source={Search} />
                    <Text numberOfLines={1} style={styles.search}>
                      Search Marketplace
                    </Text>
                  </TouchableOpacity>
                  {markets.map((snap, index) => (
                    <Tags
                      key={"recomen" + index}
                      onPress={() => this.removeTags(index, "markets")}
                      title={snap.markeplace.title}
                    />
                  ))}
                  {markets.length === 0 && (
                    <Text style={styles.emptyText}>{en.empty_mark}</Text>
                  )} */}
                  <View style={[styles.comentInput, { height: 100 }]}>
                    <Input
                      value={this.state.recommendation}
                      onChangeText={(value) =>
                        this.setState({ recommendation: value })
                      }
                      style={{ color: "#5f2543", height: 100, marginTop: 10 }}
                      multiline={true}
                      placeholderTextColor="#5f2543"
                      //Modify comment
                      placeholder={"Recommendation"}
                    />
                  </View>
                </ScrollView>
              </View>

              {/* Buttons */}
              <GradientButton
                // buttonText={isEdit ? en.edit_button_text : en.save_button_text}
                buttonText={isEdit ? en.save_button_text : en.save_button_text}
                onPress={() => {
                  isEdit ? this.handlePressEdit() : this.handlePressSave();
                }}
                containerStyle={styles.buttonSaveContainer}
                gradientStyle={styles.buttonSaveGradient}
                textStyle={styles.buttonSaveText}
                loading={loadingButtonSend}
                disabled={loadingButtonVideo}
              />

              <GradientButton
                light
                buttonText={en.next_button_text}
                onPress={() => this.handleNextData()}
                containerStyle={styles.buttonNextontainer}
                gradientStyle={styles.buttonSaveGradient}
                textStyle={[styles.buttonSaveText, { color: "#5f2543" }]}
                loading={loadingButtonSend}
                disabled={loadingButtonVideo}
              />
            </View>
          </View>
        </KeyboardAvoidingView>

        {isShowResume ? (
          <View style={styles.pdfContainer}>
            <TouchableOpacity
              style={{
                alignSelf: "baseline",
                position: "absolute",
                zIndex: 999,
                top: 10,
                left: 20,
              }}
              onPress={() => this.setState({ isShowResume: false })}
            >
              <Image source={Assets.close} style={styles.closeImage} />
            </TouchableOpacity>
            <View style={[styles.pdfSubContainer, general_styles.cardStyle]}>
              <Pdf
                source={source}
                onLoadComplete={(numberOfPages, filePath) => {
                  console.log(`number of pages: ${numberOfPages}`);
                }}
                onPageChanged={(page, numberOfPages) => {
                  console.log(`current page: ${page}`);
                }}
                onError={(error) => {
                  console.log(error);
                }}
                onPressLink={(uri) => {
                  console.log(`Link presse: ${uri}`);
                }}
                style={styles.pdfView}
              />
            </View>
          </View>
        ) : null}

        {/* Lateral Menu */}
        <Animated.View
          style={[styles.lateralLayout, { right: widthFirstContainer }]}
        >
          <TouchableOpacity
            style={styles.spiltContainer}
            onPress={this.handleSideMenu}
          >
            <View style={styles.spiltIcon} />
          </TouchableOpacity>
          {!isShowChat ? (
            <View style={styles.contentLeft}>
              {online == 0 ? (
                <View
                  style={styles.comentInput1}
                  pointerEvents={this.state.isAssignDisable ? "none" : "auto"}
                >
                  <TouchableOpacity
                    style={styles.addTag}
                    onPress={() => this.callAssignNumberApi()}
                  >
                    <Image resizeMode="contain" source={Assets.plus} />
                  </TouchableOpacity>
                  <Input
                    placeholderTextColor="#5f2543"
                    placeholder="Assign Number"
                    value={String(this.state.assignNumber)}
                    onChangeText={this.handleAssignNumber}
                    style={{ color: "#5f2543" }}
                    keyboardType={"number-pad"}
                  />
                  {/* {console.log(" CHECK ASSIGN NUMBER ", this.state.assignNumber)} */}
                </View>
              ) : null}
              {console.log(" this.state.details ", this.state.details)}
              {/* <PartakerCard
              name={name}
              company={stageName}
              country={city}
              details={this.state.details}
              image={user_image}
              onPressInfo={() => this.handlePressPartakerSection(en.info_title)}
              onPressCredits={() =>
                this.handlePressPartakerSection(en.credits_title)
              }
              onPressEducation={() =>
                this.handlePressPartakerSection(en.education_title)
              }
              onPressAppearance={() =>
                this.handlePressPartakerSection(en.appearance_title)
              }
            /> */}
              <ScrollView>
                <View style={styles.userDetailContainer}>
                  <View style={styles.imageContainer}>
                    {user_image && (
                      <Image
                        source={{ uri: user_image }}
                        style={styles.image}
                      />
                    )}
                  </View>
                  {this.state.details != undefined ? (
                    <View
                      style={{
                        flexDirection: "row",
                        alignSelf: "center",
                        justifyContent: "space-between",
                      }}
                    >
                      {console.log(" DETAIL ", this.state.details)}
                      {this.state.details.facebook != null ? (
                        <SocialLink
                          icon={Assets.fb}
                          onClick={() =>
                            this.state.details.facebook != null
                              ? Linking.openURL(
                                setUrl(this.state.details.facebook)
                              )
                              : {}
                          }
                        />
                      ) : null}
                      {this.state.details.instagram != null ? (
                        <SocialLink
                          icon={Assets.instagram}
                          onClick={() =>
                            this.state.details.instagram != null
                              ? Linking.openURL(
                                setUrl(this.state.details.instagram)
                              )
                              : {}
                          }
                        />
                      ) : null}
                      {this.state.details.twitter != null ? (
                        <SocialLink
                          icon={Assets.twitter}
                          onClick={() =>
                            this.state.details.twitter != null
                              ? Linking.openURL(
                                setUrl(this.state.details.twitter)
                              )
                              : {}
                          }
                        />
                      ) : null}
                      {this.state.details.linkedin != null ? (
                        <SocialLink
                          icon={Assets.linkedin}
                          onClick={() =>
                            this.state.details.linkedin != null
                              ? Linking.openURL(
                                setUrl(this.state.details.linkedin)
                              )
                              : {}
                          }
                        />
                      ) : null}
                    </View>
                  ) : null}
                  <View style={styles.userNameContainer}>
                    <Text style={styles.nameText}>{name}</Text>
                    <Text style={styles.cityText}>{city}</Text>

                    <TouchableOpacity
                      style={styles.contactContainer}
                      onPress={() =>
                        Linking.openURL("mailto:" + this.state.email)
                      }
                    >
                      <Image source={Assets.mail} />
                      <Text style={styles.contactText}>{"Contact"}</Text>
                    </TouchableOpacity>

                    {/* <TouchableOpacity style={styles.resumeContainer}
                  onPress={() => {
                    if (this.state.resume == null) {
                      showMessage("danger", "This performer doesn't have any resume uploaded yet.")
                      return
                    }
                    this.setState({ isShowResume: true })
                  }}>
                  <Image source={Assets.resume} style={{ width: 30, height: 30 }} resizeMode={"contain"} defaultSource={Assets.resume} />
                  <Text style={styles.resumeText}>{'Resume'}</Text>
                </TouchableOpacity> */}

                    <View style={{ marginTop: 20 }}>
                      {isManger && (
                        <CardItem
                          title="Resume & Docs"
                          icon={Assets.resume}
                          onPress={() => {
                            this.setState({
                              isMusicModal: true,
                              selectedMediaType: "doc",
                              modalTitle: "Resume & Docs",
                            });
                          }}
                        />
                      )}
                      <CardItem
                        title="Appearance"
                        icon={AppearanceIcon}
                        onPress={() => {
                          this.handlePressPartakerSection(en.appearance_title);
                        }}
                      />
                      {isManger && (
                        <CardItem
                          title="Videos"
                          icon={VideoIcon}
                          onPress={() => {
                            this.setState({
                              isMusicModal: true,
                              selectedMediaType: "video",
                              modalTitle: "Video",
                            });
                          }}
                        />
                      )}
                      {isManger && (
                        <CardItem
                          title="Music"
                          icon={MusicIcon}
                          onPress={() => {
                            this.setState({
                              isMusicModal: true,
                              selectedMediaType: "audio",
                              modalTitle: "Music",
                            });
                          }}
                        />
                      )}
                      {isManger && (
                        <CardItem
                          title="Photos"
                          icon={PhotoIcon}
                          onPress={() => {
                            this.setState({
                              isMusicModal: true,
                              selectedMediaType: "image",
                              modalTitle: "Photos",
                            });
                          }}
                        />
                      )}
                      <CardItem
                        title="Sheet Music"
                        icon={SheetIcon}
                        onPress={() => {
                          this.setState({
                            isMusicModal: true,
                            selectedMediaType: "sheet",
                            modalTitle: "Sheet Music",
                          });
                        }}
                      />
                    </View>
                  </View>
                </View>
              </ScrollView>
            </View>
          ) : null}
          <KeyboardAvoidingView
            behavior={"padding"}
            keyboardShouldPersistTaps={"always"}
            keyboardVerticalOffset={70}
            style={{ flex: 1, backgroundColor: "#F0F0F0" }}
          >
            <View style={styles.chatViewContainer}>
              <View style={styles.auditionTextContainer}>
                <Text style={styles.auditionText}>{"Audition Chat"}</Text>
                <TouchableOpacity
                  onPress={() => {
                    Keyboard.dismiss();
                    this.handleSideMenu();
                    this.setState({ isShowChat: false });
                  }}
                >
                  <Image source={Assets.close} style={styles.closeImage} />
                </TouchableOpacity>
              </View>

              <View style={{ width: "100%", flex: 1 }}>
                <FlatList
                  data={this.state.messageList}
                  extraData={this.state}
                  style={{ flexGrow: 1 }}
                  showsVerticalScrollIndicator={false}
                  renderItem={this.renderItems}
                  keyExtractor={(item) => item.uniqueId}
                  ref="flatList"
                  onScroll={this.handleScroll}
                  onLayout={() => {
                    this.refs.flatList.scrollToEnd();
                  }}
                  onContentSizeChange={() => this.refs.flatList.scrollToEnd()}
                />
              </View>

              {this.state.actualRound != "" &&
                this.state.actualRound.status == 1 ? (
                  <View
                    style={[styles.messageContainer, general_styles.cardStyle]}
                  >
                    <TextInput
                      placeholder={"Write Something..."}
                      style={styles.messageTextInput}
                      value={this.state.message}
                      multiline={true}
                      onChangeText={(message) => this.setState({ message })}
                    />
                    <TouchableOpacity
                      style={styles.sendButtonContainer}
                      onPress={this.onMessageSend}
                    >
                      <Image source={Assets.send} style={styles.sendImage} />
                    </TouchableOpacity>
                  </View>
                ) : null}
            </View>
          </KeyboardAvoidingView>
        </Animated.View>

        {/* Modals */}
        <PartakerModal
          show={partakerDetailModal}
          scrollTo={partakerDetailModalSectionSelected}
          dismiss={() => this.handlePressPartakerSection(en.info_title)}
          data={modalData}
          isFromFeedback={true}
        />
        <ModalMarket
          user_id={user_id}
          addTags={this.addTags}
          show={isSearching}
          appointment_id={this.state.audition_id}
          dismiss={this.handleSearchModal}
        />
      </Container>
    );
  }
}

const SocialLink = (props) => {
  return (
    <TouchableOpacity style={styles.iconContainer} onPress={props.onClick}>
      <Image
        source={props.icon}
        style={{ width: 20, height: 20 }}
        resizeMode={"contain"}
      />
    </TouchableOpacity>
  );
};

const mapStateToProps = (state) => ({
  user: state.user.data,
});

export default connect(mapStateToProps)(PartakerDetail);
