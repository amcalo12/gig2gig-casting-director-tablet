import React, { Component } from 'react';
import { View, Text, FlatList, Image, Animated, TouchableOpacity, ScrollView } from 'react-native';
import { Container } from 'native-base';
import BackButton from '../../../../components/commons/back_button';
import HeaderText from '../../../../components/commons/header_text';
import Header from '../../../../components/commons/header';
import Partaker from '../../../../components/auditions/partaker';
import en from '../../../../locale/en';
import showMessage from '../../../../utils/show_message';
import { withNavigationFocus } from "react-navigation";
import { NEXABOLD, resize, NEXALIGHT } from '../../../../assets/styles';
import { connect } from "react-redux";
import styles from './style';
import parseError from '../../../../utils/parse_error';
import ProgressLoader from '../../../../components/commons/progress_loading';
import { getAllPerformerAppointment, getAllSlotAppointment, completeCheckin, dropPerformer } from '../../../../api/auditions';
import { Assets } from '../../../../assets';
import { DragContainer, Draggable, DropZone } from 'react-native-drag-drop-and-swap';
import PartakerDrag from "../../../../components/auditions/partaker/draggable";
import colors from '../../../../utils/colors';
import Loading from '../../../../components/commons/loading';
import EmptyState from '../../../../components/commons/empty_state';

var scrollOffset = 0;
var refDropZones = [];

class Appointment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            performerList: [],
            appointment: [],
            actualRound: this.props.navigation.state.params.round,
            audition_id: this.props.navigation.state.params.audition_id,
            status: this.props.navigation.state.params.status,
            refreshing: false,
            isLoading: false,
            viewSplitAnimatedValueA: new Animated.Value(0),
            zIndex: 2,
            indexItem: []
        };
    }

    async componentDidMount() {
        this.getAllPerformer()
        this.getAllSlots()
    }

    getAllPerformer = async () => {
        try {
            const performers = await getAllPerformerAppointment(this.state.audition_id)
            console.log('performers :', performers);
            this.setState({ performerList: performers.data.data })
        } catch (error) {
            const parsedError = parseError(error);
            showMessage("danger", parsedError);
            console.log(error);
        }
    }

    getAllSlots = async () => {
        try {
            const slots = await getAllSlotAppointment(this.state.audition_id)
            console.log('slots :', slots);
            this.setState({ appointment: slots.data.data.slots })
        } catch (error) {
            const parsedError = parseError(error);
            showMessage("danger", parsedError);
            console.log(error);
        }
    }


    setDropZoneRef = (value, index) => {
        refDropZones[index] = value;
    };

    handleScroll = event => {
        console.log("Event ====>" + JSON.stringify(event));
        scrollOffset = event.nativeEvent.contentOffset.y;
    };

    animateToCoord = (event) => {
        console.log('event :', event);

        this.state.appointment[event.dropZone].status = 1
        this.state.appointment[event.dropZone].user_slot = this.state.performerList[event.id]

        console.log('this.state :', this.state);

        this.state.performerList.splice(event.id, 1)
        this.setState({ appointment: this.state.appointment, performerList: this.state.performerList })

        this.checkInAPICall(this.state.appointment[event.dropZone])

        this.onAnimationEnds();
    }

    checkInAPICall = async (slot_detail) => {
        console.log('slot_detail :>> ', slot_detail);
        this.setState({ loadingButton: true });
        try {
            // const data = {
            //     auditions: this.state.audition_id,
            //     appointment_id: slot_detail.appointment_id,
            //     slot: slot_detail.id,
            //     user: slot_detail.user_slot.user_id,
            //     rol: slot_detail.user_slot.rol,
            //     nonRevert: false
            // };

            const data = {
                id: slot_detail.user_slot.id,
                slot: slot_detail.id,
                user: slot_detail.user_slot.user_id,
                auditions: this.state.audition_id,
                rol: slot_detail.user_slot.rol,
                appointment_id: slot_detail.appointment_id
            }

            // if (checkinData.role !== null || checkinData.role !== "") {
            //     data.rol = checkinData.role;
            // }
            console.log(" CHECK IN DATA PARAMS ", data)
            // return
            await dropPerformer(data);

        } catch (error) {
            console.log(error);
            const parsedError = parseError(error);
            showMessage("danger", parsedError);
        }
    };

    onAnimationEnds = () => {
        this.setState({ zIndex: 2 });
    }

    dragStart = () => {
        console.log('drag start :');
        this.setState({ zIndex: 1 });
    };

    goBack = () => {
        this.props.navigation.goBack()
    }

    onItemClick = (item, index) => {
        console.log('item :', {
            personId: item.user_id,
            image: item.image,
            audition_id: item.appointment_id,
            audition: item.auditions_id
        });
        this.props.navigation.navigate("ManagerTalentDatabase", {
            personId: item.user_id,
            image: item.image,
            audition_id: item.appointment_id,
            audition: item.auditions_id
        });
    }

    render() {
        const widthFirstContainerA = this.state.viewSplitAnimatedValueA.interpolate({
            inputRange: [-resize(405), 0],
            outputRange: [-resize(405), 0]
        });
        const { indexItem } = this.state
        return (
            <Container>
                <Header
                    left={<BackButton onPress={() => this.goBack()} />}
                    center={<HeaderText title={""} />}
                />
                {this.state.isLoading ? <ProgressLoader isLoading={true} /> : null}
                <View style={{ height: "100%" }}>

                    <DragContainer
                        onAnimationEnds={this.onAnimationEnds}
                        style={styles.upper}
                        onDragStart={this.dragStart}
                    >
                        {/* Person container */}
                        <DropZone
                            id={"A"}
                            style={styles.one}
                            onDrop={event => this.animateToCoord(event)}
                        />

                        {this.state.performerList.length > 0 ? <FlatList
                            contentContainerStyle={{ padding: 10 }}
                            numColumns={3}
                            data={this.state.performerList}
                            extraData={this.state}
                            keyExtractor={(item, index) => item + index}
                            renderItem={({ item, index }) => (
                                <Draggable
                                    key={"Drag" + index}
                                    id={index}
                                    // position={position[index]}
                                    style={styles.dragable}
                                    onAnimationEnds={this.onAnimationEnds}
                                    onLongPress={"onPressIn"}
                                    onPress={() => this.onItemClick(item, index)}
                                >
                                    <PartakerDrag
                                        name={item.name}
                                        image={item.image}
                                        styleManage={
                                            indexItem[index] === 0 ? false : !indexItem[index]
                                        }
                                    />
                                </Draggable>
                            )}
                            showsVerticalScrollIndicator={false}
                        // ListEmptyComponent={
                        //     this.state.isLoading
                        //         ? <Loading />
                        //         : <EmptyState text={en.empty_state_performer} />
                        // }
                        /> :
                            <View style={{ flex: 1, right: "22%", justifyContent: 'center', alignItems: 'center' }}>
                                <EmptyState text={en.empty_state_performer} />
                            </View>
                        }


                        <View style={[styles.two, { zIndex: this.state.zIndex }]}>
                            <TouchableOpacity style={styles.spiltContainer}>
                                <View style={styles.spiltIcon} />
                            </TouchableOpacity>
                            <View style={styles.twoP}>
                                <ScrollView
                                    bounces={false}
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={styles.twoI}
                                >
                                    {/* {console.log("rolesAvailable======>", rolesAvailable)} */}
                                    {this.state.appointment.map((role, index) => {
                                        if (role.user_slot == null) {
                                            return <View key={`DropZone${index}`}>
                                                <DropZone
                                                    setDropZoneRef={this.setDropZoneRef}
                                                    id={index}
                                                    style={styles.dragRoles}
                                                    onDrop={event => this.animateToCoord(event)}
                                                />
                                                <View style={styles.nameRol}>
                                                    <Text style={styles.name}>{role.time}</Text>
                                                </View>
                                            </View>
                                        }
                                        return (
                                            <View key={`DragCustom${index}`}>
                                                {/* <Draggable
                                                    id={1000 + index}
                                                    style={styles.dragRoles}
                                                    onAnimationEnds={this.onAnimationEnds}
                                                > */}
                                                <View
                                                    id={1000 + index}
                                                    style={styles.dragRoles}
                                                    onAnimationEnds={this.onAnimationEnds}>
                                                    {/* DROP AREA  */}
                                                    <PartakerDrag
                                                        name={role.user_slot.name ||
                                                            role.user_slot.user.details.first_name + " " + role.user_slot.user.details.last_name}
                                                        image={role.user_slot.image || role.user_slot.user.image.url}
                                                        styleManage
                                                    />
                                                </View>
                                                {/* </Draggable> */}
                                                <View style={styles.nameRol}>
                                                    <Text style={styles.name}>{role.time}</Text>
                                                </View>
                                                {/* <View style={styles.nameRol}>
                                                    <Text style={styles.name}>{role.name}</Text>
                                                </View> */}
                                            </View>
                                        );
                                    })}
                                </ScrollView>
                            </View>
                        </View>
                    </DragContainer>
                    {/* <DragContainer style={{ flexDirection: "row" }}
                    onAnimationEnds={this.onAnimationEnds}
                    onDragStart={this.dragStart}
                >
                    <View style={{ width: "100%" }}>
                        <DropZone
                            id={"A"}
                            style={styles.one}
                        // onDrop={event => this.animateToCoord(event)}
                        />
                        <FlatList
                            contentContainerStyle={{ padding: 10 }}
                            numColumns={3}
                            data={this.state.performerList}
                            extraData={this.state}
                            keyExtractor={(item, index) => item + index}
                            renderItem={({ item, index }) => (
                                <View style={styles.container}>
                                    <Draggable key={"Drag" + index}
                                        id={index}
                                        style={styles.dragable}
                                        onAnimationEnds={this.onAnimationEnds}
                                    >
                                        <View style={styles.imageContainer}>
                                            <Image source={{ uri: item.image }} style={styles.image} />
                                        </View>

                                        <Text style={styles.name} numberOfLines={1}>
                                            {item.name}
                                        </Text>
                                    </Draggable>
                                </View>
                            )}
                            showsVerticalScrollIndicator={false}
                        />
                        <View style={[styles.lateralLayout, { right: widthFirstContainerA }]}>
                            <TouchableOpacity
                                style={styles.spiltContainer}
                                onPress={this.handleSideMenu}
                            >
                                <View style={styles.spiltIcon} />
                            </TouchableOpacity>

                            <FlatList
                                contentContainerStyle={{ padding: 10 }}
                                numColumns={2}
                                data={this.state.performerList}
                                extraData={this.state}
                                keyExtractor={(item, index) => item + index}
                                renderItem={({ item, index }) => (
                                    <View style={styles.container}>
                                        <View style={styles.imageContainer}>
                                            <Image source={{ uri: item.image }} style={styles.image} />
                                        </View>

                                        <Text style={styles.name} numberOfLines={1}>
                                            {item.name}
                                        </Text>
                                    </View>
                                )}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                    </View>
                </DragContainer> */}
                </View>
            </Container >
        );
    }
}

const mapStateToProps = state => {
    console.log(" REDUX DATA CREATE GROUP=========>", state);

    return {
        user: state.user.data,
        feedback: state.user.instantFeedback
    };
};

export default withNavigationFocus(connect(mapStateToProps)(Appointment));