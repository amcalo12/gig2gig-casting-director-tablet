import { StyleSheet, Platform } from 'react-native';
import { resize, NEXABOLD, height, STATUS_BAR_HEIGHT, NEXALIGHT } from '../../../../assets/styles';
import colors from '../../../../utils/colors';

const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT;
export const headerHeight =
    Platform.OS === "ios"
        ? resize(82 + 20, "height")
        : resize(82 + STATUS_BAR_HEIGHT, "height");

const styles = StyleSheet.create({
    container: {
        width: resize(200),
        flexGrow: 0,
        flexShrink: 1,
        flexBasis: "auto",
        marginBottom: resize(30, "height"),
        margin: 10,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: "#d6d6d6",
    },
    imageContainer: {
        width: "100%",
        height: resize(154, "height")
    },
    image: {
        resizeMode: "cover",
        width: "100%",
        height: "100%",
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
    },
    name: {
        color: "#4d2545",
        fontFamily: NEXABOLD,
        fontSize: resize(16),
        fontWeight: "400",
        marginLeft: 5,
        paddingBottom: resize(1, "height")
    },
    buttonContainer: {
        flexDirection: "row", backgroundColor: colors.LIGHT_GREY,
        alignItems: "center", padding: 10, justifyContent: "flex-end", alignSelf: "center",
        width: "70%", borderRadius: 8, marginTop: 10,
        marginBottom: 10
    },
    buttonText: {
        color: "#4d2545",
        fontFamily: NEXALIGHT,
        letterSpacing: 2,
        fontSize: resize(14), marginLeft: 8
    },
    lateralLayout: {
        position: "absolute",
        zIndex: 2,
        width: resize(425),
        height: resize(765, "height"),
        flexDirection: "row"
    },
    spiltIcon: {
        width: resize(1),
        height: resize(40, "height"),
        borderColor: "#707070",
        borderWidth: resize(2.5),
        borderRadius: resize(30)
    },
    spiltContainer: {
        width: resize(22),
        height: resize(752, "height"),
        backgroundColor: "#bfbfbf",
        alignItems: "center",
        justifyContent: "center"
    },
    one: {
        width: "59%",
        height: "99%",
        position: "absolute",
        marginVertical: resize(15, "height"),
        marginHorizontal: resize(15)
    },
    dragable: {
        width: resize(178),
        height: resize(201, "height"),
        marginRight: resize(33),
        marginBottom: resize(15, "height")
    },
    upper: {
        width: "100%",
        height: height - headerHeight,
        position: "absolute",
        zIndex: 4,
        // paddingVertical: resize(15, 'height'),
        paddingHorizontal: resize(15)
    },
    two: {
        width: "42%",
        height: "100%",
        alignSelf: "flex-end",
        // paddingVertical: resize(15, 'height'),
        paddingHorizontal: resize(10),
        flexDirection: "row",
        position: "absolute"
    },
    twoP: {
        // width: "100%",
        flex: 1,
        height: "92%"
        // backgroundColor: 'blue'
    },
    twoI: {
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-around"
    },
    dragRoles: {
        width: resize(178),
        height: resize(201, "height"),
        borderWidth: 1,
        borderRadius: 4,
        borderColor: "#d6d6d6",
        // backgroundColor: colors.LIGHT_GREY,
        marginVertical: resize(10)
    },
    nameRol: {
        width: "100%",
        height: 25,
        alignItems: "center"
    },
    scrollPerson: {
        width: "100%",
        height: "100%",
        marginVertical: resize(15, "height"),
        marginHorizontal: resize(15),
        position: "absolute",
        zIndex: 2
    },
})
export default styles;