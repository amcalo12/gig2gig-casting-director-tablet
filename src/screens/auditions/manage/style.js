import { StyleSheet, Platform } from "react-native";

//custom
import {
  width,
  height,
  NEXABOLD,
  NEXALIGHT,
  resize,
  STATUS_BAR_HEIGHT
} from "../../../assets/styles";

export const headerHeight =
  Platform.OS === "ios"
    ? resize(82 + 20, "height")
    : resize(82 + STATUS_BAR_HEIGHT, "height");

const styles = StyleSheet.create({
  wrapperContent: {
    width,
    flexDirection: "row",
    flex: 1
    // height: "100%"
  },
  firstSectionAnimated: {
    flex: 1,
    width: "100%"
  },
  firstSection: {
    flex: 1,
    paddingTop: resize(22, "height")
  },
  partakerContainer: {
    flexDirection: "row",
    marginLeft: resize(31),
    flexWrap: "wrap",
    marginBottom: resize(125, "height")
  },
  spiltContainer: {
    width: resize(22),
    height: resize(752, "height"),
    backgroundColor: "#bfbfbf",
    alignItems: "center",
    justifyContent: "center"
  },
  spiltIcon: {
    width: resize(1),
    height: resize(40, "height"),
    borderColor: "#707070",
    borderWidth: resize(2.5),
    borderRadius: resize(30)
  },
  secondSection: {
    width: resize(416),
    backgroundColor: "#f0f0f0"
  },
  tabsContainer: {
    height: resize(58, "height")
  },
  tabBarUnderlineStyle: {
    backgroundColor: "#4d2545",
    borderBottomColor: "#bfbfbf"
  },
  tabStyle: {
    backgroundColor: "#f0f0f0",
    borderBottomColor: "white",
    borderBottomWidth: 2
  },
  tabStyle1: {
    backgroundColor: "#f0f0f0"
  },
  tabTextStyle: {
    color: "#bfbfbf",
    fontFamily: NEXALIGHT,
    fontSize: resize(20),
    fontWeight: "400",
    paddingVertical: resize(5, "height")
  },
  tabActiveTextStyle: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: "400",
    paddingVertical: resize(5, "height")
  },
  activeTabStyle: {
    backgroundColor: "#f0f0f0",
    borderBottomColor: "#4d2545",
    borderBottomWidth: 4
  },
  infoContainer: {
    height: height * 0.06,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#d6d6d6"
  },
  infoTitle: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: 20,
    fontWeight: "400"
  },
  contentRight: {
    paddingLeft: resize(42),
    marginBottom: resize(30, "height"),
    backgroundColor: "#f0f0f0"
  },
  contentManage: {
    backgroundColor: "#f0f0f0"
  },
  auditionInfoContainer: {
    paddingTop: resize(31, "height"),
    paddingBottom: resize(26, "height"),
    paddingRight: resize(91),
    borderBottomWidth: 1,
    borderBottomColor: "#d6d6d6"
  },
  auditionName: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(28),
    fontWeight: "400",
    paddingVertical: resize(5, "height")
  },
  auditionDateContainer: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  auditionDate: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(17),
    fontWeight: "400",
    paddingVertical: resize(5, "height")
  },
  auditionCompany: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(17),
    fontWeight: "400",
    paddingVertical: resize(5, "height")
  },
  appointmentsContainer: {
    paddingRight: resize(91),
    borderBottomWidth: 1,
    borderBottomColor: "#d6d6d6",
    paddingVertical: resize(16, "height")
  },
  appointments: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: "400"
  },
  tagsContainer: {
    paddingRight: resize(96),
    borderBottomWidth: 1,
    borderBottomColor: "#d6d6d6",
    paddingVertical: resize(16, "height"),
    flexDirection: "row"
  },
  tagPurple: {
    borderRadius: 19,
    borderColor: "#4d2545",
    borderWidth: 1,
    backgroundColor: "#4d2545",
    paddingTop: resize(14, "height"),
    paddingBottom: resize(10, "height"),
    paddingHorizontal: resize(10),
    marginRight: resize(10)
  },
  tagYellow: {
    borderRadius: 19,
    borderColor: "#d8893a",
    borderWidth: 1,
    backgroundColor: "#d8893a",
    paddingTop: resize(14, "height"),
    paddingBottom: resize(10, "height"),
    paddingHorizontal: resize(10),
    marginRight: resize(10)
  },
  tagRed: {
    borderRadius: 19,
    borderColor: "#93183e",
    borderWidth: 1,
    backgroundColor: "#93183e",
    paddingTop: resize(14, "height"),
    paddingBottom: resize(10, "height"),
    paddingHorizontal: resize(10),
    marginRight: resize(10)
  },
  tagsText: {
    color: "#ffffff",
    fontFamily: NEXABOLD,
    fontSize: 13,
    fontWeight: "400"
  },
  auditionDescriptionContainer: {
    paddingRight: resize(42),
    borderBottomWidth: 1,
    borderBottomColor: "#d6d6d6",
    paddingVertical: resize(27, "height")
  },
  auditionDescription: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(13),
    fontWeight: "400",
    textAlign: "justify",
    paddingVertical: resize(5, "height")
  },
  auditionUrlContainer: {
    paddingRight: resize(42),
    borderBottomWidth: 1,
    borderBottomColor: "#d6d6d6",
    paddingVertical: resize(22, "height")
  },
  auditionUrl: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: "400"
  },
  rolesContainer: {
    marginRight: resize(20)
  },
  resourcesContainer: {
    borderBottomWidth: 1,
    borderBottomColor: "#d6d6d6",
    borderTopWidth: 1,
    borderTopColor: "#d6d6d6",
    paddingVertical: resize(37, "height"),
    paddingRight: resize(40)
  },
  contributorsContainer: {
    paddingTop: resize(26, "height"),
    paddingBottom: resize(30, "height"),
    borderBottomWidth: 1,
    borderBottomColor: "#d6d6d6"
  },
  editContainer: {
    paddingVertical: resize(34, "height"),
    justifyContent: "center",
    alignItems: "center",
    marginBottom: resize(75, "height")
  },
  addContributorContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  addContributorButton: {
    flexDirection: "row",
    borderRadius: 18,
    borderColor: "#4d2545",
    borderWidth: 1,
    backgroundColor: "#ffffff",
    width: resize(185),
    height: resize(36, "height"),
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    width: resize(16),
    height: resize(17, "height")
  },
  addContributorText: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: "400",
    paddingLeft: resize(11)
  },
  auditionVideosContainer: {
    backgroundColor: "#f0f0f0",
    height
  },
  auditionVideosHeader: {
    flexDirection: "row",
    width: "100%",
    height: resize(67, "height")
  },
  iconArrowLeftContainer: {
    width: "20%",
    justifyContent: "center",
    alignItems: "flex-start",
    paddingLeft: resize(22)
  },
  iconArrowLeft: {
    resizeMode: "contain",
    width: resize(13),
    height: resize(22, "height")
  },
  auditionVideosTitleContainer: {
    width: "80%",
    justifyContent: "center",
    alignItems: "flex-start",
    paddingLeft: resize(60)
  },
  auditionVideosTitle: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(19),
    fontWeight: "400"
  },
  auditionVideosContent: {
    paddingLeft: resize(39),
    paddingRight: resize(41),
    paddingVertical: resize(10, "height")
  },
  videosContainer: {
    marginBottom: resize(125, "height")
  },
  noAuditions: {
    width: "90%",
    height: "90%",
    justifyContent: "center",
    alignItems: "center"
  },
  noAuditionsText: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: 28,
    fontWeight: "400",
    textAlign: "center"
  },
  monitorModeText: {
    paddingHorizontal: resize(50),
    marginTop: resize(10, "height"),
    color: "#000000",
    fontSize: resize(13),
    fontWeight: "400",
    lineHeight: resize(18),
    textAlign: "center",
    ...Platform.select({
      android: {
        paddingBottom: resize(40, "height")
      }
    })
  },
  emptyText: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(17),
    fontWeight: "400"
  },
  pinLabelContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: resize(15, "height")
  },
  pin: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(18),
    fontWeight: "400"
  },
  lateralLayout: {
    position: "absolute",
    zIndex: 2,
    width: resize(425),
    height: resize(765, "height"),
    flexDirection: "row"
  },
  upper: {
    width: "100%",
    height: height - headerHeight,
    position: "absolute",
    zIndex: 4,
    // paddingVertical: resize(15, 'height'),
    paddingHorizontal: resize(15)
  },
  one: {
    width: "59%",
    height: "99%",
    position: "absolute",
    marginVertical: resize(15, "height"),
    marginHorizontal: resize(15)
  },
  two: {
    width: "37%",
    height: "100%",
    alignSelf: "flex-end",
    // paddingVertical: resize(15, 'height'),
    paddingHorizontal: resize(15),
    position: "absolute"
  },
  twoP: {
    width: "100%",
    height: "92%"
    // backgroundColor: 'blue'
  },
  twoI: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-around"
  },
  absoluteContainer: {
    flexDirection: "row",
    width: "100%",
    height: "100%",
    position: "absolute",
    zIndex: 5,
    marginVertical: resize(15, "height"),
    marginHorizontal: resize(15)
  },
  layoutDrag: {
    position: "absolute",
    zIndex: 2,
    alignItems: "flex-start",
    width: "100%",
    height: "100%"
  },
  dragable: {
    width: resize(178),
    height: resize(201, "height"),
    marginRight: resize(33),
    marginBottom: resize(15, "height")
  },
  dragRoles: {
    width: resize(178),
    height: resize(201, "height"),
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#d6d6d6",
    marginBottom: resize(15)
  },
  scrollPerson: {
    width: "100%",
    height: "100%",
    // flex: 1,
    marginVertical: resize(15, "height"),
    marginHorizontal: resize(15),
    position: "absolute",
    zIndex: 2
  },
  scrollPerson2: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingRight: resize(255)
  },
  nameRol: {
    width: "100%",
    height: 25,
    alignItems: "center"
  },
  name: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(16),
    paddingBottom: resize(1, "height")
  },
  options: {
    width: "100%",
    height: "8%",
    borderTopColor: "#707070",
    borderTopWidth: 1,
    backgroundColor: "#F0F0F0",
    justifyContent: "center"
  },
  export: {
    width: resize(80),
    height: "100%",
    justifyContent: "center",
    alignItems: "flex-start"
  },
  manageVideoText: {
    fontFamily: NEXABOLD,
    color: "#4d2545",
    fontSize: resize(22),
    fontWeight: "400",
    alignSelf: "center"
  },
  viewVideo: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  backButtonStyle: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    width: 50,
    left: 0,
    position: "absolute"
  },
  ShareStyle: {
    //check
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: 'white',
    // borderWidth: 0.5,
    // borderColor: '#fff',
    //height: 40,
    width: 150,
    // borderRadius: 5,
    margin: 5,
    top: 25,
    marginLeft: 25,
    marginTop: 25,
  },
  OpenIn: {
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: 'white',
    // borderWidth: 0.5,
    // borderColor: '#fff',
    // height: 40,
    width: 150,
    //borderRadius: 5,
    margin: 5,
    top: 20,
    right: 3,
    marginLeft: 25
  },
  Delete: {
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: 'white',
    // borderWidth: 0.5,
    // borderColor: '#fff',
    // height: 40,
    width: 150,
    //borderRadius: 5,
    margin: 5,
    top: 15,
    right: 3,
    marginLeft: 25
  },

  ImageIconStyle: {
    // padding: 15,
    margin: 5,
    height: 22,
    width: 22,
    marginLeft: 20,
    resizeMode: "contain",
    tintColor:"#4d2545"
  },
  TextStyle: {
    color: "#4d2545",
    // marginBottom: 4,
    // marginRight: 20,
    textAlign: "center",
    alignSelf: "center",
    top: 3,
    fontSize: resize(16),
    alignItems: "center",
    fontWeight: "400",
    fontFamily: NEXALIGHT,
    marginLeft: 10
  },
  SeparatorLine: {
    backgroundColor: "#fff",
    width: 1,
    height: 40
  },
  newGroupContainer: {
    marginTop: 10,
    flexDirection: "row", paddingHorizontal: 20, paddingVertical: 8, borderRadius: 4,
    alignSelf: "center", alignItems: "center", borderColor: "white", borderWidth: 2
  },
  newGroupText: {
    fontSize: resize(16), marginLeft: 10, color: "white",
    fontFamily: NEXALIGHT
  },
  iconSearch: {
    marginTop: 10,
    paddingHorizontal: 30, paddingVertical: 8,
    alignSelf: "center", alignItems: "center",
  },
  recordVideoContainer: {
    marginTop: 10,
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 8,
    borderRadius: 4,
    alignSelf: "center",
    alignItems: "center",
    borderColor: "white",
    borderWidth: 2,
    right: 20
  },

});

export default styles;
