import { Container, Content, Tab, Tabs } from "native-base";
import React, { Component } from "react";
import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  PermissionsAndroid,
  View,
  Animated,
  Linking,
  Keyboard,
  FlatList,
  Platform,
  TextInput,
} from "react-native";
import moment from "moment";
import { connect } from "react-redux";

//custom
import styles, { headerHeight } from "./style";
import AlertIOS from "../../../components/commons/alert_ios";
import FileMenu from "../../../components/auditions/file_menu";
import ArrowLeft from "../../../assets/icons/auditions/arrow-left-icon.png";
import iconSearch from "../../../assets/icons/search/search.png";
import Contributor from "../../../components/auditions/contributor";
import File from "../../../components/auditions/file";
import ManageButton from "../../../components/auditions/manage_button";
import Partaker from "../../../components/auditions/partaker";
import PartakerDrag from "../../../components/auditions/partaker/draggable";
import Role from "../../../components/auditions/role";
import BackButton from "../../../components/commons/back_button";
import GradientButton from "../../../components/commons/gradient_button";
import Header from "../../../components/commons/header";
import HeaderButton from "../../../components/commons/header_button";
import HeaderText from "../../../components/commons/header_text";
import Search from "../../../components/commons/search";
import PinKeyboard from "../../../components/commons/pin_keyboard";
import Loading from "../../../components/commons/loading";
import UnionTag from "../../../components/auditions/union_tag";
import ContractTag from "../../../components/auditions/contract_tag";
import ProductionTag from "../../../components/auditions/production_tag";
import RoleIcon from "../../../assets/icons/auditions/role-icon.png";
import parseError from "../../../utils/parse_error";
import showMessage from "../../../utils/show_message";
import shareContent from "../../../utils/share_content";
import { withNavigationFocus } from "react-navigation";
import RNFetchBlob from "react-native-fetch-blob";
import recordVideo from "../../../utils/record_video/index";
import AsyncStorage from "@react-native-community/async-storage";
import { ProcessingManager } from "react-native-video-processing";

import {
  DragContainer,
  Draggable,
  DropZone,
} from "react-native-drag-drop-and-swap";

//api
import {
  getAudition,
  openAudition,
  closeAudition,
  getAuditionAppointments,
  getAuditionVideos,
  reOpenRound,
  deleteMedia,
  deleteAuditionVideo,
  addPersonToRol,
  getFinalList,
  removePersonToRol,
  closeRoound,
  createGroup,
  groupStatus,
  closeGroup,
  createAuditionVideo,
  instantFeedbacks,
  getPerformanceProfile,
  getHiddenPerformer,
  getExportAnalyticData,
  getfeedback,
  addIndividualComment,
  getAllAuditionUsers,
  getExportUrl,
} from "../../../api/auditions";

//locale
import en from "../../../locale/en";

// actions
import {
  SET_CHECKIN_MODE_PIN,
  SET_MONITOR_UPDATES_PIN,
  SET_AUDITION_ID_TO_CHECKIN,
} from "../../../actions/manage";
import { resize, NEXALIGHT } from "../../../assets/styles";
import FakeDropDown from "../../../components/commons/fakeSelect";
import { getRoundsByAudition, createNewRound } from "../../../api/rounds";
import { SET_AUDITION_INFO } from "../../../actions/checkin";
import videoList from "./videoList";
import { Assets } from "../../../assets";
import InstanceFeedbackInfoModal from "../instance_feedback/index";
import InstantFeedbackAdd from "../instant_feedback_add/index";
import { addInstantFeedback } from "../../../api/instant_feedback";
import { addKeepForFuture } from "../../../api/instant_feedback";
import uploadAsset from "../../../utils/upload_asset";
import parseDataModalPartaker from "../../../utils/auditions/parse_data_modal_partaker";
import ProgressLoader from "../../../components/commons/progress_loading";
import FeedbackSelection from "../feedbackselection";
import { DEFAULT_INSTANT_FEEDBACK } from "../../../actions/user";
import { sortByTime } from "../../../utils/constant";
import RenameModal from "../../../components/auditions/rename_file";
import RNThumbnail from "react-native-thumbnail";
import SaveForFutureModal from "../savefor_future";
import settings from "../../../../settings";
import colors from "../../../utils/colors";

const AnimatedView = Animated.View;
var scrollOffset = 0;
var refDropZones = [];

class AuditionsManage extends Component {
  constructor(props) {
    super(props);
    this.selectedVideo = null;
    this.isDontShow = false;
    this.isDontShowAccept = false;
    this.isDontShowFuture = false;
    this.isCheck = false;
  }

  state = {
    audition_id: this.props.navigation.getParam("audition_id", ""),
    isManger: this.props.navigation.getParam("isManger", true),
    userId: [],
    title: "",
    date: "",
    time: "",
    description: "",
    url: "",
    union: "",
    contract: "",
    production: [],
    companyName: "",
    appointmentsNumber: 0,
    submissions: 0,
    roles: [],
    files: [],
    contributors: [],
    loading: true,
    search: "",
    showSearch: false,
    partakers: [],
    contributors: [],
    currentTab: 0,
    fileSelected: {},
    openAuditions: false,
    showAuditionVideos: false,
    showOptions: false,
    optionsPosition: 0,
    optionsPosition2: 0,
    enterMonitorModeModal: false,
    enterCheckinModeModal: false,
    pinModalCheckin: false,
    pinModalMonitor: false,
    checkinPin: "",
    updatesPin: "",
    viewSplit: false,
    viewSplitAnimatedValueA: new Animated.Value(0),
    viewSplitAnimatedValue: new Animated.Value(255),
    status: 0,
    loadingButton: false,
    loadingButtonRound: false,
    auditionVideos: [],
    isAuditionVideo: false,
    isOpen: true,
    scrollOffset: 0,
    isFinalList: false,
    position: [],
    rolesAvailable: [],
    indexItem: [],
    zIndex: 2,
    finalList: [],
    listData: [],
    slots: [],
    showAppointmentsModal: false,
    audition_rounds: [],
    actualRound: {},
    modalCloseAudition: false,
    modalOpenNewRound: false,
    entreConfirm: false,
    hasFinal: false,
    modalAudition: false,
    loadingButtonC: false,
    showFinal: false,
    loadingRoundStart: false,
    refreshing: false,
    isSelectionMode: false,
    isOnpress: false,
    isSelected: false,
    isGroup: false,
    groupList: [],
    createGroupResponse: [],
    isInfoVisible: false,
    isAddFeedback: false,
    checkGroupStatusRes: [],
    video: null,
    videos: [],
    searchText: "",
    performerName: "",
    isLoading: false,
    isFeedbackSelection: false,
    isRejectClick: false,
    isList: false,
    isVisible: false,
    fileName: "",
    isRename: false,
    isSaveLater: false,
    auditionUser: [],
  };

  animatedValue = new Animated.Value(0);

  async componentDidMount() {
    this.getInstantFeedback();
    if (Platform.OS === "android") {
      const granted = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
      );

      if (granted) {
        console.log("You can use the WRITE_EXTERNAL_STORAGE");
        this.isCheck = true;
      } else {
        console.log("WRITE_EXTERNAL_STORAGE permission denied");
      }
    }

    AsyncStorage.getItem("isDontShow").then(
      (data) => {
        if (data != null) {
          let asyncData = JSON.parse(data);
          console.log(" get data ", data);
          this.isDontShow = asyncData.isDontShow;
          console.log(" this.isDontShow ", this.isDontShow);
        }
      },
      (fail) => {}
    );
    AsyncStorage.getItem("isDontShowAccept").then(
      (data) => {
        if (data != null) {
          let asyncData = JSON.parse(data);
          console.log(" get data ", data);
          this.isDontShowAccept = asyncData.isDontShowAccept;
          console.log(" this.isDontShow ", this.isDontShowAccept);
        }
      },
      (fail) => {}
    );

    AsyncStorage.getItem("isDontShowLater").then(
      (data) => {
        if (data != null) {
          let asyncData = JSON.parse(data);
          console.log(" get data ", data);
          this.isDontShowFuture = asyncData.isDontShowFuture;
          console.log(" this.isDontShow ", this.isDontShowFuture);
        }
      },
      (fail) => {}
    );
    console.log(
      " AUDITION ID ",
      this.props.navigation.getParam("audition_id", "")
    );
    console.log(
      "APPOINTMENT ID=====>",
      this.props.navigation.getParam("appointment_id", "")
    );
    // this.getRounds();
    this.getAllAuditionUserDetail();
    this.fetchAudition();
    console.log("isAddFeedback CHECK=====>", this.state.isAddFeedback);
  }

  getAllAuditionUserDetail = async () => {
    const { audition_id } = this.state;
    // this.setState({ loading: true });
    try {
      const response = await getAllAuditionUsers(audition_id);
      console.log("response :>> ", response.data.data);
      this.setState({ auditionUser: response.data.data });
      console.log(" getAllAuditionUsers success =====>", response);
      // this.setState({ loading: false });
      // return true
    } catch (error) {
      this.setState({ loading: false });
      console.log(" getAllAuditionUsers error =====>", error);
      // return false
    }
  };

  //GET INSTANT FEEDBACK
  getInstantFeedback = async () => {
    const { dispatch } = this.props;
    const { user } = this.props;
    console.log("<===== user id =====>", user.id);

    // this.setState({ loading: true });
    try {
      const response = await getfeedback(user.id);

      this.setState({
        instantFeedback: response.data.data,
      });
      console.log("SUCCESS Get Instant Feedback =====>", response);
      let defaultMessage = {};
      defaultMessage["reject"] = response.data.data.comment;
      defaultMessage["accept"] = response.data.data.positiveComment;
      dispatch({
        type: DEFAULT_INSTANT_FEEDBACK,
        payload: defaultMessage,
      });
    } catch (error) {
      console.log("ERROR Get Instant Feedback =====>", error);
    }

    // this.setState({ loading: false });
  };

  componentDidUpdate = (prevProps) => {
    let { isFocused } = this.props;
    if (prevProps.isFocused !== isFocused && isFocused) {
      this.getRounds();
    }
  };

  //OPEN MODAL

  onFeedbackClose = (item, index) => {
    console.log(" ITEM ", item);
    if (item.is_feedback_sent == 1) {
      showMessage("danger", "Feedback Already sent");
    } else {
      this.currentItem = item;
      this.currentIndex = index;
      if (this.isDontShow) {
        this.rejectFeedback();
      } else {
        this.setState({ isInfoVisible: true, isRejectClick: true });
      }
    }
  };

  onFeedbackSaveLater = (item, index) => {
    console.log(" ITEM ", item);
    if (item.is_feedback_sent == 1) {
      showMessage("danger", "Feedback Already sent");
    } else {
      this.currentItem = item;
      this.currentIndex = index;
      if (this.isDontShowFuture) {
        this.saveForFutureCall();
      } else {
        this.setState({ isSaveLater: true });
      }
    }
    // this.currentItem = item;
    // this.currentIndex = index;
    // this.setState({ isSaveLater: true })
  };

  onModalClose = () => {
    this.setState({
      isInfoVisible: false,
      isAddFeedback: false,
      isFeedbackSelection: false,
      isSaveLater: false,
    });
  };

  // onDoneClick = () => {
  // this.setState({ isInfoVisible: false })
  // }

  onDoneClick = async () => {
    if (this.state.isRejectClick) {
      this.rejectFeedback();
    } else {
      this.feedbackSubmit();
    }
  };

  feedbackSubmit = async () => {
    // console.log('this.currentItem :>> ', this.currentItem);
    // console.log('params :>> ', {
    //   appointment_id: this.state.actualRound.id,
    //   user: this.currentItem.user_id,
    //   evaluator: this.props.user.id,
    //   slot_id: this.currentItem.slot_id
    // });
    let params = {
      appointment_id: this.state.actualRound.id,
      user: this.currentItem.user_id,
      evaluator: this.props.user.id,
      comment: this.props.feedback.accept,
      suggested_appointment_id: "",
      accepted: 1,
    };
    console.log("ACCEPT FEEDBACK PARAMS========>", params);

    try {
      const response = await addInstantFeedback(params);
      console.log(" ACCEPT FEEDBACK SUCCESS ", response);
      this.onModalClose();
      showMessage("success", response.data.data);
      this.feedbackSuccess();
    } catch (error) {
      console.log(" ACCEPT FEEDBACK ERROR ", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  rejectFeedback = async () => {
    console.log(" FEEBACK MESSAGE CHECK ", this.props.feedback.reject);
    const { user_id, actualRound } = this.state;
    let params = {
      appointment_id: actualRound.id, //this.state.appointment_id,
      user: this.currentItem.user_id,
      evaluator: this.props.user.id,
      // comment: "Thanks for attending That's all we need today.",
      comment: this.props.feedback.reject,
      suggested_appointment_id: "",
      accepted: 0,
    };
    console.log("INSTANT FEEDBACK PARAMS=========>", params);
    try {
      const rejectResponse = await addInstantFeedback(params);
      console.log("REJECT FEEDBACK SUCCESS ", rejectResponse);
      if (this.state.isGroup) {
        this.state.groupList.splice(this.currentIndex, 1);
      } else {
        this.state.partakers.splice(this.currentIndex, 1);
      }
      this.setState({ isInfoVisible: false });
    } catch (error) {
      console.log(" REJECT FEEDBACK ERROR ", error.response);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }

    this.setState({ isInfoVisible: false });
  };

  onDontShow = () => {
    this.setState({ isInfoVisible: false });
    if (this.state.isReject) {
      AsyncStorage.setItem(
        "isDontShow",
        JSON.stringify({ isDontShow: true })
      ).then(
        (data) => {
          console.log(" data ", data);
          this.isDontShow = true;
          this.rejectFeedback();
        },
        (fail) => {}
      );
    } else {
      AsyncStorage.setItem(
        "isDontShowAccept",
        JSON.stringify({ isDontShowAccept: true })
      ).then(
        (data) => {
          console.log(" data ", data);
          this.isDontShowAccept = true;
          this.feedbackSubmit();
        },
        (fail) => {}
      );
    }
  };

  renderInfoModal = () => {
    console.log("this.props.user.instantFeedback", this.state.isRejectClick);
    return (
      <InstanceFeedbackInfoModal
        isVisible={this.state.isInfoVisible}
        onCloseClick={this.onModalClose}
        dontShowClick={this.onDontShow}
        doneClick={this.onDoneClick}
        isReject={this.state.isRejectClick}
        message={
          this.state.isRejectClick
            ? this.props.feedback.reject
            : this.props.feedback.accept
        }
      />
    );
  };

  onDontShowLater = () => {
    this.setState({ isSaveLater: false });
    AsyncStorage.setItem(
      "isDontShowLater",
      JSON.stringify({ isDontShowFuture: true })
    ).then(
      (data) => {
        console.log(" data ", data);
        this.isDontShowFuture = true;
        this.saveForFutureCall();
      },
      (fail) => {}
    );
  };

  onDoneClickLater = async () => {
    this.saveForFutureCall();
    this.setState({ isSaveLater: false });
  };

  saveForFutureCall = async () => {
    // console.log('this.currentItem :>> ', this.currentItem);
    // console.log('params :>> ', {
    //   appointment_id: this.state.actualRound.id,
    //   user: this.currentItem.user_id,
    //   evaluator: this.props.user.id,
    //   slot_id: this.currentItem.slot_id
    // });
    let params = {
      appointment_id: this.state.actualRound.id,
      user: this.currentItem.user_id,
      evaluator: this.props.user.id,
      comment: this.props.feedback.accept,
      slot_id: this.currentItem.slot_id,
      suggested_appointment_id: "",
      accepted: 2,
    };
    console.log("saveForFutureCall========>", params);
    // return
    try {
      const response = await addInstantFeedback(params);
      console.log(" ACCEPT FEEDBACK SUCCESS ", response);
      showMessage("success", response.data.data);
      this.feedbackSuccess();
    } catch (error) {
      console.log(" ACCEPT FEEDBACK ERROR ", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  renderSaveLaterModal = () => {
    return (
      <SaveForFutureModal
        isVisible={this.state.isSaveLater}
        onCloseClick={this.onModalClose}
        dontShowClick={this.onDontShowLater}
        doneClick={this.onDoneClickLater}
        message={this.props.feedback.accept}
      />
    );
  };

  onMessageChange = (text) => {
    this.setState({ feedbackMessage: text });
  };

  onSearchText = (text) => {
    this.setState({ searchText: text });
  };

  feedbackSuccess = () => {
    if (this.isGroup) {
      var item = this.currentItem;
      item.is_feedback_sent = 1;
      this.state.groupList[this.currentIndex] = item;
      this.setState({ groupList: this.state.groupList });
    } else {
      var item = this.currentItem;
      item.is_feedback_sent = 1;
      this.state.partakers[this.currentIndex] = item;
      this.setState({ partakers: this.state.partakers });
    }
  };

  renderFeedbackAddModal = () => {
    return (
      <InstantFeedbackAdd
        isVisible={this.state.isAddFeedback}
        onMessageChange={this.onMessageChange}
        onCloseClick={this.onModalClose}
        currentUser={this.currentItem}
        appointment_id={this.state.actualRound.id}
        evaluator={this.props.user.id}
        feedbackSuccess={this.feedbackSuccess}
        // submitClick={this.onFeedbackSubmit}
        // message={this.state.feedbackMessage}
        // searchText={""}
        // onSearchText={this.onSearchText}
      />
    );
  };

  onCustomClick = () => {
    this.setState({ isFeedbackSelection: false, isAddFeedback: true });
  };

  onStandardClick = () => {
    if (this.isDontShowAccept) {
      this.feedbackSubmit();
    } else {
      this.setState({
        isFeedbackSelection: false,
        isInfoVisible: true,
        isRejectClick: false,
      });
    }
  };

  renderFeedbackSelection = () => {
    return (
      <FeedbackSelection
        isVisible={this.state.isFeedbackSelection}
        onCloseClick={this.onModalClose}
        onCustomClick={this.onCustomClick}
        onStandardClick={this.onStandardClick}
      />
    );
  };

  //   //INSTANT FEEDBACK API CALL
  //  instantFeedbackApi = async () => {
  //     const { user_id,  actualRound} = this.state;
  //     this.setState({ loading: true });
  //     try {

  //       // const params = { appointment_id: audition_id, user_ids: this.state.userId };
  //       // console.log("PARAMS =====> ", JSON.stringify(params))

  //     let params = {
  //       appointment_id: actualRound.id,
  //       user: user_id,
  //       evaluator: user_id,
  //       comment: "frewrerer",
  //       suggested_appointment_id:actualRound.id
  //     }
  //     console.log("APPOINMENT ID=====> ",params)
  //     console.log("PARAMS INSTANT FEEDBACK API CALL=====> ", params)
  //       const response = await this.instantFeedbackApi(params);
  //       this.state.groupList = this.state.partakers.filter(data => data.selected)
  //       console.log(" CHECK GROUP LIST ", this.state.groupList)
  //       this.setState({ isGroup: true, isSelectionMode: false, groupList: this.state.groupList })
  //       console.log("SUCCESS GROUP LIST API=====>", JSON.stringify(response))
  //       this.setState({ loading: false });
  //     } catch (error) {
  //       this.setState({ loading: false });
  //       console.log("ERROR GROUP LIST API =====>",error);
  //     }
  //   };

  //GET ROUNDS API
  getRounds = async () => {
    let { audition_id } = this.state;
    this.setState({ loading: true });
    try {
      let responseRounds = await getRoundsByAudition(audition_id);
      let audition_rounds = responseRounds.data.data;
      console.log(
        "audition_rounds===========>" + JSON.stringify(audition_rounds)
      );
      let actualRound = {};
      if (this.state.status != 2) {
        // actualRound = audition_rounds.filter(data => data.status == 1)[0]

        let temp = audition_rounds.filter((data) => data.status == 1);
        if (temp.length > 0) {
          actualRound = temp[0];
        } else {
          actualRound = audition_rounds[audition_rounds.length - 1];
        }
      } else {
        actualRound = audition_rounds[audition_rounds.length - 1];
      }

      // let actualRound = audition_rounds.filter(data => data.status == 1)[0]
      console.log("actualRound :", actualRound);
      this.setState({ audition_rounds, actualRound, currentTab: 1 }, () => {
        this.fetchPartakers(actualRound);
        this.checkGroupStatusApi();
      });
      // this.fetchAudition(actualRound);
    } catch (error) {
      const parsedError = parseError(error);
      console.log("GET ROUNDS ERROR=====>");
      showMessage("danger", parsedError);
    }
  };

  fetchFinalList = async () => {
    //Evaluate if final cast list Database has roles prev asigned
    let { audition_id, roles, listData, partakers } = this.state;
    let listA = [...listData];
    try {
      let response = await getFinalList(audition_id);
      let finalList = response.data.data;
      // let finalList = sortByTime(response.data.data);
      console.log(" FINAL LIST -----", finalList);

      for (let index = 0; index < finalList.length; index++) {
        let dropZone = roles.findIndex((element) => {
          return finalList[index].rol_id === element.id;
        });
        let idPerson = partakers.findIndex((element) => {
          return finalList[index].user_id === element.user_id;
        });
        if (dropZone >= 0) {
          listA[dropZone] = {
            dropZone,
            id: idPerson,
            finalList: finalList[index],
          };
        }
      }
      // console.log(" listA LIST -----", JSON.stringify(listA));
      this.setState({ finalList, listData: listA, hasFinal: true });
    } catch (e) {
      //  console.log(" FETCH FINAL ERROR -----", e);
      console.log(e);
    }
  };

  fetchAudition = async () => {
    const { audition_id } = this.state;
    try {
      const response = await getAudition(audition_id);
      const data = response.data.data;
      console.log("Data============>" + JSON.stringify(data));

      let showFinal = data.status === 2 ? true : false;
      this.setState(
        {
          title: data.title,
          date: moment(data.date).format("LL"),
          // time: moment(data.time, "HH:mm:ss").format("HH:mm A"),
          time:
            data.time != null
              ? moment(data.time, "HH:mm:ss").format("hh:mm A")
              : "",
          companyName: data.agency,
          appointmentsNumber:
            data.apointment.general != null ? data.apointment.general.slots : 0,
          submissions: data.submissions ? data.submissions : 0,
          union: data.union,
          contract: data.contract,
          production: data.production,
          description: data.description,
          url: data.url,
          slots: data.apointment.slots,
          roles: data.roles,
          files: data.media,
          contributors: data.contributors,
          status: data.status,
          showFinal,
          online: data.online,
        },
        () => {
          this.getRounds();
          // this.fetchPartakers(actualRound);
          // this.checkGroupStatusApi();
        }
      );
    } catch (error) {
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      console.log(error);
    }
  };

  fetchPartakers = async (actualRound) => {
    console.log("fetchPartakers ==============>Called ");
    try {
      const response = await getAuditionAppointments(actualRound.id);
      console.log("fetchPartakers ==============>", response);
      this.setState(
        {
          partakers: sortByTime(response.data.data),
          refreshing: false,
        },
        () => {
          //alert("fetchPartakers")
          this.fetchFinalList();
        }
      );
    } catch (error) {
      console.log(" ERROR 1111111111", error);
      console.log(error);
      const parsedError = parseError(error);
      console.log("parsedError 111 :", parsedError);
      showMessage("danger", parsedError);
    }
    this.setState({ loading: false });
  };

  handlePressBackButton = () => {
    let { rolesAvailable, status, hasFinal } = this.state;
    let hasSetFinal = false;
    for (const iterator of rolesAvailable) {
      if (iterator) {
        hasSetFinal = true;
      }
    }
    if (hasFinal) hasSetFinal = true;
    if (!hasSetFinal && status === 2) {
      this.handleConfirmOut();
    } else {
      this.goBack();
    }
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack(null);
  };

  handleSetRef = (file, index) => {
    this[`file_${index.id}`] = file;
  };

  fetchAuditionVideos = async (actualRound) => {
    try {
      const response = await getAuditionVideos(actualRound.id);
      let auditionVideos = [];

      response.data.data.map((file) => {
        return auditionVideos.push({
          ...file,
          type: "video",
        });
      });

      this.setState({
        auditionVideos,
      });
    } catch (error) {
      // console.log(error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  onReOpenRound = async () => {
    console.log("this.state :>> ", this.state.audition_rounds);
    const activeRound = this.state.audition_rounds.filter(
      (data) => data.status == 1
    );
    console.log("activeRound", activeRound);
    if (activeRound.length > 0) {
      showMessage("danger", "Please close round " + activeRound[0].round);
      return;
    }
    this.setState({ loading: true });
    try {
      const response = await reOpenRound(this.state.actualRound.id);
      console.log("response :>> ", response);
      this.fetchAudition();
    } catch (error) {
      this.setState({ loading: false });
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  handleGetMeasure = async (file) => {
    await new Promise((resolve, reject) => {
      const refName = `file_${file.id}`;
      if (refName in this) {
        this[refName].measure((a, b, width, height, px, py) => {
          this.setState(
            {
              optionsPosition: py,
              fileSelected: file,
            },
            () => resolve(true)
          );
        });
      } else {
        this.setState(
          {
            optionsPosition: 0,
            fileSelected: file,
          },
          () => resolve(false)
        );
      }
    });
  };

  handlePressShowSearch = () => {
    this.setState({
      showSearch: true,
    });
  };

  onChangeSearch = (search) => {
    this.setState({ search });
  };

  onPressFileMore = async (file, isAuditionVideo = false) => {
    try {
      const { showOptions } = this.state;

      await this.handleGetMeasure(file);

      this.setState({
        showOptions: !showOptions,
        isAuditionVideo,
      });
    } catch (error) {
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  onPressPartaker = (indexPartaker) => {
    console.log(" CHECK LIST ", this.state.partakers);
    console.log(" CHECK LIST 1111", indexPartaker);
    const { navigation } = this.props;
    const {
      roles,
      title,
      partakers,
      actualRound,
      audition_id,
      online,
      auditionUser,
      contract,
    } = this.state;

    navigation.navigate("ManagerPartakerDetail", {
      audition_id: actualRound.id,
      actualRound,
      roles,
      auditionName: title,
      partakers: this.state.isGroup ? this.state.groupList : partakers,
      indexPartaker,
      audition: audition_id,
      online,
      userData: partakers[indexPartaker],
      auditionUser,
      contract: contract,
    });
    console.log("ON PRESS PARTAKER DATA =====>");
  };

  onFinalListPress = (index) => {
    const { navigation } = this.props;
    const { partakers, audition_id, actualRound } = this.state;
    console.log("onFinalListPress =====>");
    console.log("PERSON ID onFinalListPress =====>", partakers[index].user_id);
    navigation.navigate("ManagerTalentDatabase", {
      personId: partakers[index].user_id,
      image: partakers[index].image,
      audition_id: actualRound.id,
      audition: audition_id,
    });
  };

  onFinalListPressCallBack = (user_id, image) => {
    const { navigation } = this.props;
    const { audition_id, actualRound } = this.state;
    console.log("onFinalListPressCallBack=====>");
    navigation.navigate("ManagerTalentDatabase", {
      personId: user_id,
      image: image,
      audition_id: actualRound.id,
      audition: audition_id,
    });
  };
  handleToggleMonitorMode = () => {
    const { enterMonitorModeModal } = this.state;

    this.setState({
      enterMonitorModeModal: !enterMonitorModeModal,
    });
  };

  handleEnterMonitorMode = () => {
    const { pinModalMonitor, enterMonitorModeModal } = this.state;

    this.setState({
      pinModalMonitor: !pinModalMonitor,
      enterMonitorModeModal: !enterMonitorModeModal,
    });
  };

  handlerDismissCheckinModePin = () => {
    const { pinModalCheckin } = this.state;

    this.setState({
      pinModalCheckin: !pinModalCheckin,
      checkinPin: "",
    });
  };

  handlerDismissMonitorModePin = () => {
    const { pinModalMonitor } = this.state;

    this.setState({
      pinModalMonitor: !pinModalMonitor,
      updatesPin: "",
    });
  };

  onPressSetPinCheckin = () => {
    const { navigation, dispatch } = this.props;
    const {
      checkinPin,
      audition_id,
      title,
      date,
      time,
      actualRound,
    } = this.state;
    if (checkinPin !== "") {
      this.handlerDismissCheckinModePin();

      dispatch({
        type: SET_CHECKIN_MODE_PIN,
        payload: checkinPin,
      });

      dispatch({
        type: SET_AUDITION_ID_TO_CHECKIN,
        payload: actualRound.id,
      });

      dispatch({
        type: SET_AUDITION_INFO,
        payload: { title, date, time, audition: audition_id },
      });

      return navigation.navigate("ManagerCheckinMode", {
        title,
        date,
        time,
        audition: audition_id,
      });
    }
  };
  onPressSetPinMonitorMode = () => {
    const { navigation, dispatch } = this.props;
    const { updatesPin, slots, actualRound, audition_id } = this.state;

    if (updatesPin !== "") {
      this.handlerDismissMonitorModePin();

      dispatch({
        type: SET_MONITOR_UPDATES_PIN,
        payload: updatesPin,
      });

      return navigation.navigate("ManagerMonitorMode", {
        audition_id: actualRound.id,
        slots,
        actualRound,
        audition: audition_id,
      });
    }
  };

  handleChangeTab = (currentTab) => {
    console.log("handleChangeTab=====>", currentTab);
    if (currentTab <= 1) {
      this.setState({ currentTab });
    }
  };

  onPressCheckinMode = () => {
    const { enterCheckinModeModal } = this.state;

    this.setState({
      enterCheckinModeModal: !enterCheckinModeModal,
    });
  };
  onPressManualCheckinMode = () => {
    // const { enterCheckinModeModal } = this.state;

    // this.setState({
    //   enterCheckinModeModal: !enterCheckinModeModal,
    // });
    // const {  slots, actualRound, audition_id } = this.state;

    // const checkinData = {
    //   appointment: response.data.data.slot_id,
    //   time: response.data.data.hour,
    //   appointment_id: data.appointmentId,
    //   id: response.data.data.id,
    //   name: response.data.data.name,
    //   image: response.data.data.image,
    //   role: data.rolId
    // };

    const { user_id, actualRound } = this.state;

    this.props.navigation.navigate("ManualCheckinMode", {
      title: this.state.title,
      date: this.state.date,
      time: this.state.time,
      audition_id: this.state.audition_id,
      appointment_id: actualRound.id,
    });
  };
  handleEnterCheckinMode = () => {
    const { pinModalCheckin, enterCheckinModeModal } = this.state;

    this.setState({
      pinModalCheckin: !pinModalCheckin,
      enterCheckinModeModal: !enterCheckinModeModal,
    });
  };

  onPressMonitorMode = () => {
    this.handleToggleMonitorMode();
  };

  onPressOpenAuditions = async () => {
    const { audition_id } = this.state;

    try {
      this.setState({ loadingButton: true, loading: true });
      const response = await openAudition(audition_id);
      this.fetchAudition();
      this.setState({
        loadingButton: false,
        status: response.data.data.status,
      });
    } catch (error) {
      this.setState({ loadingButton: false });
      const hasError = parseError(error);
      showMessage("danger", hasError);
    }
  };

  onAppointments = () => {
    this.props.navigation.navigate("Appointments", {
      audition_id: this.state.audition_id,
      round: this.state.actualRound,
      status: this.state.status,
    });
  };

  handlePressVideoList = () => {
    const { navigation } = this.props;

    // alert(this.state.audition_id)
    navigation.navigate("videoList", {
      audition_id: this.state.audition_id,
      round: this.state.actualRound,
      status: this.state.status,
      onFinalListPressCallBack: this.onFinalListPressCallBack.bind(this),
    });
  };

  onPushNotification = () => {
    let { actualRound } = this.state;
    const { navigation } = this.props;
    navigation.navigate("PushNotification", { data: actualRound });
  };

  onHiddenPerformers = () => {
    // alert(this.state.audition_id)
    let { actualRound } = this.state;

    const { navigation } = this.props;

    navigation.navigate("HiddenPerformers", {
      audition_id: this.state.audition_id,
      round: this.state.actualRound,
      status: this.state.status,
    });
  };

  onAcceptCloseRound = async () => {
    const { actualRound } = this.state;
    try {
      this.setState({ loadingButtonC: true });
      await closeRoound(actualRound.id);
      showMessage("success", en.round_closed);
      this.setState({
        loadingButtonRound: false,
        loadingButtonC: false,
      });
      this.onPressCloseRound();
      this.getRounds();
    } catch (error) {
      this.setState({ loadingButtonC: false });
      const hasError = parseError(error);
      showMessage("danger", hasError);
    }
  };

  onPressCloseAuditions = async () => {
    const { audition_id, audition_rounds } = this.state;
    try {
      this.setState({ loadingButton: true });
      const response = await closeAudition(audition_id);
      let actualRound = audition_rounds[audition_rounds.length - 1];
      this.setState({
        loadingButton: false,
        status: response.data.data.status,
        showFinal: true,
      });
      this.fetchPartakers(actualRound);
      this.handleCloseAudition();
    } catch (error) {
      this.setState({ loadingButton: false });
      const hasError = parseError(error);

      showMessage("danger", hasError);
    }
  };

  onExportAuditionEdit = async () => {
    const { config, fs } = RNFetchBlob;
    const { audition_id } = this.state;
    // let downloadUrl = settings.apiUrl + `exportAuditionLogs/${audition_id}`
    // console.log('downloadUrl :>> ', downloadUrl);
    // var date = new Date();
    // let PictureDir = fs.dirs.DocumentDir + date.getTime() + ".csv" // this is the Download directory. You can check the available directories in the wiki.
    // let options = {
    //   fileCache: false,
    //   path: PictureDir,
    //   appendExt: ".csv",
    //   addAndroidDownloads: {
    //     useDownloadManager: true, // setting it to true will use the device's native download manager and will be shown in the notification bar.
    //     notification: false,
    //     path: PictureDir, // this is the path where your downloaded file will live in
    //     description: 'Downloading file.'
    //   }
    // }

    // config(options).fetch('GET', downloadUrl).then((res) => {
    //   // do some magic here
    //   console.log('res :>> ', res);
    // })
    // return

    // const { audition_id } = this.state;
    Linking.openURL(settings.apiUrl + `exportAuditionLogs/${audition_id}`);
    try {
      // this.setState({ loadingButton: true });
      const response = await getExportUrl(audition_id);
      console.log("response :>> ", response);
    } catch (error) {
      // this.setState({ loadingButton: false });
      const hasError = parseError(error);
      showMessage("danger", hasError);
    }
  };

  onPressAuditionVideos = () => {
    this.setState({
      showAuditionVideos: true,
    });
  };

  onPressAddContributor = () => {
    console.log("ADD CONTRIBUTOR");
  };

  onPressBackToTabs = () => {
    this.setState({
      showAuditionVideos: false,
    });
  };

  handleChangeCheckinPin = (digit) => {
    const { checkinPin } = this.state;

    this.setState({
      checkinPin: `${checkinPin}${digit}`,
    });
  };

  handleChangeUpdatesPin = (digit) => {
    const { updatesPin } = this.state;

    this.setState({
      updatesPin: `${updatesPin}${digit}`,
    });
  };

  handlePressEnableEdit = () => {
    const { audition_id } = this.state;
    const { navigation } = this.props;

    navigation.navigate("EditAudition", {
      audition_id,
    });
  };

  handlePressSaveChanges = () => {
    this.setState({
      enableEdit: !this.state.enableEdit,
    });
  };

  handleDeleteFile = async (file) => {
    let { files, isAuditionVideo, auditionVideos, audition_id } = this.state;

    try {
      let index;

      if (isAuditionVideo) {
        index = auditionVideos.indexOf(file);

        await deleteAuditionVideo(file.id, audition_id);

        auditionVideos.splice(index, 1);
        this.onPressFileMore({});

        return showMessage("success", en.video_was_deleted);
      } else {
        index = files.indexOf(file);

        await deleteMedia(file.id);

        files.splice(index, 1);
        this.onPressFileMore({});

        return showMessage("success", en.document_was_deleted);
      }
    } catch (error) {
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  handleSideMenu = () => {
    const {
      viewSplitAnimatedValueA,
      isOpen,
      viewSplitAnimatedValue,
    } = this.state;
    const toValue = !isOpen ? 0 : -resize(405);
    const toValueP = isOpen ? 0 : resize(255);

    Animated.timing(viewSplitAnimatedValue, {
      toValue: toValueP,
      duration: 300,
    }).start();

    Animated.timing(viewSplitAnimatedValueA, {
      toValue: toValue,
      duration: 300,
    }).start(() => {
      this.setState((prevState) => ({
        isOpen: !prevState.isOpen,
      }));
    });
  };

  handlePressUrl = () => {
    const { url } = this.state;

    return Linking.openURL(url);
  };

  parseContributorName = (contributor) => {
    const details = contributor.contributor_info.details;
    console.log("contributor", contributor.contributor_info);

    let lastName = details ? details.last_name : false;
    let name = details ? details.first_name : false;

    if (lastName !== "" && lastName !== "null") {
      name =
        !name || lastName
          ? contributor.contributor_info.email
          : `${name} ${lastName}`;
    }

    // return name;
    return details.first_name + " " + details.last_name;
  };

  shareAsset = async (file) => {
    try {
      await shareContent(`Open this link: \n ${file.url}`);
    } catch (error) {
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  // shareContentWithTitle = async file => {
  //   try {
  //     await shareContentWithTitle(
  //       `Open this link: \n ${file.url}`,
  //       "Video shared from Gig2Gig Casting"
  //     );
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  openAsset = (file) => {
    return Linking.openURL(file.url);
  };

  animateToCoord = async (mesure, udpate = true) => {
    // console.log("abc9");
    console.log("mesure", mesure);
    let coord = mesure;
    // console.log("abc11" + JSON.stringify(coord));
    //  console.log("abc10");
    let { position, rolesAvailable, indexItem } = this.state;

    //Removing reference from variable to don't mutate directly state
    let indexA = [...indexItem];
    let rolesA = [...rolesAvailable];
    let posA = [...position];
    //Validate if card is outing of Rol
    if (Number(coord.id) >= 1000) {
      let displayPerson = Number(coord.id) - 1000;
      coord = { ...mesure, id: rolesA[displayPerson].id };
    }
    //Adjust layout to enter and exit of dragables
    let adjustY =
      coord.dropZone !== "A"
        ? this.selectiveLayoutY(coord.id) - scrollOffset
        : 0;
    let yModify = coord.zone.y - headerHeight - resize(15, "height") - adjustY;
    let xModify =
      coord.zone.x -
      resize(15) -
      (coord.dropZone !== "A" ? this.selectiveLayoutX(coord.id) : 0);
    let positionC = { x: xModify, y: yModify };
    posA[coord.id] = positionC;

    //Validating if role is empty or not
    if (!rolesA[coord.dropZone] && coord.dropZone !== "A") {
      //   console.log("abc8");
      //this is for visual feedback and important to display card correctly
      rolesA[coord.dropZone] = coord;

      //Leave the past box available in case you move from one role to another
      //   console.log(indexA[coord.id]);
      //    console.log("abc6" + indexA[coord.id]);
      if (rolesA[indexA[coord.id]]) {
        //     console.log("abc7");
        this.removePerson(rolesA[indexA[coord.id]].finalList.id);
        rolesA[indexA[coord.id]] = false;
      }

      // do unavailable slot of role in what person realease card
      indexA[coord.id] = coord.dropZone;
      this.setState({ position: posA });
      if (udpate) {
        //      console.log("abc4");
        let finalList = await this.addPerson(coord);
        //     console.log("abc3");
        rolesA[coord.dropZone] = { ...coord, finalList };
        //     console.log("abc5");
        this.setState({ rolesAvailable: rolesA });
      } else {
        this.setState({ rolesAvailable: rolesA });
      }

      //This is just for a animation to look good,
      //Can be replace in a future for set that state when animations ends
      setTimeout(() => {
        this.setState({ indexItem: indexA });
      }, 600);
    } else {
      //This is in case person is drag out Role field into a person drag
      if (coord.dropZone === "A") {
        //       console.log("abc2");
        this.removePerson(rolesA[indexA[coord.id]].finalList.id);
        rolesA[indexA[coord.id]] = false;
        indexA[coord.id] = false;
        this.setState({
          position: posA,
          rolesAvailable: rolesA,
          indexItem: indexA,
          hasFinal: false,
        });
      } else {
        //      console.log("abc1");
        //this is for prevent scroll take to much time to user to interact
        this.onAnimationEnds();
      }
    }
  };

  selectiveLayoutX = (index) => {
    if (index === 0) {
      return 0;
    } else {
      let containx = index / 3;
      let filaX = Math.trunc(containx);
      let containy = filaX * 3;
      if (index % 3 === 0) {
        return 0;
      }
      if (containy + 1 === index) {
        return resize(178) * 1 + resize(33) * 1;
      }
      if (containy + 2 === index) {
        return resize(178) * 2 + resize(33) * 2;
      }
      return resize(178) * index + resize(33) * index;
    }
  };

  addPerson = async (coord) => {
    //   console("abc15");
    let { partakers, roles, audition_id } = this.state;
    let valor = await addPersonToRol({
      audition_id: audition_id,
      performer_id: partakers[coord.id].user_id,
      rol_id: roles[coord.dropZone].id,
    });
    console.log("addPerson=====>", valor.data.data);
    return valor.data.data;
  };

  removePerson = async (id) => {
    try {
      await removePersonToRol(id);
    } catch (e) {
      console.log(e);
    }
  };

  selectiveLayoutY = (index) => {
    let containx = index / 3;
    let filaX = Math.trunc(containx);
    return resize(201, "height") * filaX + resize(15, "height") * filaX;
  };

  dragStart = () => {
    console.log("drag start :");
    this.setState({ zIndex: 1 });
  };

  onAnimationEnds = () => {
    this.setState({ zIndex: 2 });
  };

  handleScroll = (event) => {
    console.log("Event ====>" + JSON.stringify(event));
    scrollOffset = event.nativeEvent.contentOffset.y;
  };

  onPressFinaliList = () => {
    this.setState(
      (presvState) => ({
        isFinalList: !presvState.isFinalList,
      }),
      () => {
        this.autolayoutCards();
      }
    );
  };

  autolayoutCards = async () => {
    let { isFinalList, listData } = this.state;
    //let { isFinalList } = this.state;
    console.log("isFinalList 111", isFinalList);
    // let listData = [
    //   {
    //     dropZone: 0,
    //     id: 0,
    //     finalList: {
    //       id: 19,
    //       user_id: 29,
    //       rol_id: 135,
    //       name: "Jakayla Beatty",
    //       rol_name: "role1"
    //     }
    //   }
    // ];
    console.log("listData", listData);
    if (isFinalList) {
      for (let index = 0; index < listData.length; index++) {
        if (listData[index]) {
          await new Promise((resolve) => setTimeout(resolve, 600));
          let customData = {
            ...listData[index],
            zone: { ...refDropZones[listData[index].dropZone] },
          };
          this.animateToCoord(customData, false);
          //This is for animations problems
        }
      }
    }
  };

  setDropZoneRef = (value, index) => {
    refDropZones[index] = value;
  };

  changeTypeOfRounds = (chagingType, customOption = false) => {
    //	alert("123")
    Keyboard.dismiss();
    this.fakedrop.measure((a, b, width, height, px, py) => {
      this.setState(
        {
          optionsPosition2: { py, px, width, height },
        },
        () => {
          if (chagingType) {
            this.setState({ chagingType }, () => {
              Animated.timing(this.animatedValue, {
                toValue: 10,
                duration: 400,
              }).start();
            });
          } else {
            // make animation
            Animated.timing(this.animatedValue, {
              toValue: 0,
              duration: 400,
            }).start(() => {
              this.setState({ chagingType });
              if (customOption) {
                this.onPressNewRound();
              }
            });
          }
        }
      );
    });
  };

  onRequestNewRound = async () => {
    const { audition_id, actualRound, audition_rounds, online } = this.state;
    const { navigation } = this.props;
    this.onPressNewRound();
    let isAccept = true;
    for (const iterator of audition_rounds) {
      if (iterator.status === 1) {
        isAccept = false;
      }
    }
    if (isAccept) {
      if (online === 1) {
        const data = {
          date: "",
          time: "0",
          location: {
            latitude: "0",
            latitudeDelta: "0",
            longitude: "0",
            longitudeDelta: "0",
          },
          number_slots: 0,
          type: 1,
          online: true,
          length: 0,
          start: "0",
          end: "0",
          round: actualRound.round + 1,
          status: true,
        };
        try {
          await createNewRound(data, audition_id);
          showMessage("success", "Round created successfully");
          this.setState({ currentTab: 0 });
          this.getRounds();
        } catch (error) {
          console.log("eroor", error);
          const parsedError = parseError(error);
          showMessage("danger", parsedError);
        }
      } else {
        navigation.navigate("ManagerRounds", {
          audition_id,
          round: actualRound.round + 1,
        });
      }
    } else {
      showMessage("danger", "You must close all rounds before open a new one");
    }
  };

  changeRoundOptions = (actualRound) => {
    this.fetchPartakers(actualRound);
    this.fetchAuditionVideos(actualRound);
    this.setState({ actualRound, showFinal: false });
  };

  onPressCloseRound = () => {
    if (this.state.isGroup) {
      showMessage("danger", "Please close group first");
      return;
    } else {
      //this.setState({ currentTab: 0 });
      this.setState((prevState) => {
        return { modalCloseAudition: !prevState.modalCloseAudition };
      });
    }
  };

  onPressNewRound = () =>
    this.setState((prevState) => {
      return { modalOpenNewRound: !prevState.modalOpenNewRound };
    });

  handleConfirmOut = () => {
    this.setState((prevState) => {
      return { entreConfirm: !prevState.entreConfirm };
    });
  };

  handleCloseAudition = () => {
    if (this.state.isGroup) {
      if (this.state.isGroup) {
        showMessage("danger", "Please close group first");
        return;
      }
    } else {
      this.setState((prevState) => {
        return { modalAudition: !prevState.modalAudition };
      });
    }
  };

  onPresFinalRound = () => {
    let { chagingType, audition_rounds } = this.state;
    let actualRound = audition_rounds[audition_rounds.length - 1];
    this.fetchPartakers(actualRound);
    this.setState({ showFinal: true });
    this.changeTypeOfRounds(!chagingType);
  };

  async requestExternalStoragePermission(isAnalytics) {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: "Storage Permission",
          message: "Needs access to your Storage access ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK",
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the storage");
        if (isAnalytics) {
          this.onExportClick();
        } else {
          this.exportCsv();
        }
        return true;
      } else {
        console.log("storage permission denied");
        return false;
      }
    } catch (err) {
      console.warn(err);
    }
  }

  onExportAnalytic = async () => {
    if (Platform.OS == "android" && !this.isCheck) {
      this.requestExternalStoragePermission(true);
    } else {
      this.onExportClick();
    }
  };

  onExport = async () => {
    if (Platform.OS == "android" && !this.isCheck) {
      this.requestExternalStoragePermission(false);
    } else {
      this.exportCsv();
    }
  };

  onExportClick = async () => {
    console.log(" appointment_id  :", this.state);
    this.setState({ isLoading: true });
    try {
      const response = await getExportAnalyticData(this.state.audition_id);
      this.exportAnalyticFile(response.data.data);
      this.setState({ isLoading: false });
    } catch (error) {
      this.setState({ isLoading: false });
      console.log("error :", error);
    }
  };

  exportAnalyticFile = (exportData) => {
    const headerString =
      "Round,Total Auditioners,Gender breakdown,Starred Performers\n";
    let temp = "";
    exportData.map((data, index) => {
      temp = temp + data.toString() + "\n";
    });
    console.log("temp :", temp);
    const csvString = `${headerString}${temp}`;

    let pathToWrite = "";
    if (Platform.OS == "android") {
      pathToWrite = `${RNFetchBlob.fs.dirs.DownloadDir}/Analytics.csv`;
    } else {
      pathToWrite = `${RNFetchBlob.fs.dirs.DocumentDir}/Analytics.csv`; //`${RNFetchBlob.fs.dirs.DocumentDir}/data.csv`;
    }
    console.log("pathToWrite", pathToWrite);

    RNFetchBlob.fs
      .writeFile(pathToWrite, csvString, "utf8")
      .then(() => {
        showMessage("success", en.csv_dowloaded);
        console.log(`wrote file ${pathToWrite}`);
        // wrote file /storage/emulated/0/Download/data.csv
      })
      .catch((error) => console.error(error));
  };

  exportCsv = () => {
    console.log(" ROLES ARRAY ", this.state.roles);
    console.log(" rolesAvailable ARRAY ", this.state.rolesAvailable);
    console.log(" PERFORMER ARRAY ", this.state.partakers);
    // return;
    let performerArray = [];

    this.state.partakers.map((performer) => {
      this.state.rolesAvailable.map((rolesAvailable) => {
        if (rolesAvailable != false) {
          let roleName = this.state.roles.filter(
            (role) => role.id == rolesAvailable.finalList.rol_id
          );
          if (
            performer.user_id == rolesAvailable.finalList.user_id ||
            performer.user_id == rolesAvailable.finalList.performer_id
          ) {
            let union = "";
            performer.union_array.map((data) => (union = union + "," + data));

            performerArray[performerArray.length] = [
              roleName[0].name,
              performer.name || "",
              performer.email || "",
              performer.birth || "",
              performer.website,
              performer.representation_name,
              performer.representation_email,
              '"' + performer.union_string + '"',
            ];
          }
        }
      });
    });
    console.log(" performerArray ", performerArray);
    // return;
    // this.state.roles.map((data, index) => {
    //   this.state.rolesAvailable.map((item, roleIndex) => {
    //     if (data.id == item.finalList.rol_id) {
    //       let performerDetail = this.state.partakers.filter(
    //         data => data.user_id == item.finalList.user_id
    //       );
    //       console.log("performerDetail=====>", performerDetail),
    //         (performerArray[performerArray.length] = [
    //           data.name,
    //           performerDetail[0].name || "" || null || undefined,
    //           performerDetail[0].email || "" || null || undefined,
    //           performerDetail[0].birth || "" || null || undefined,
    //           console.log(
    //             "performerDetail.name=====>",
    //             performerDetail[0].name
    //           ),
    //           console.log(
    //             "performerDetail.email=====>",
    //             performerDetail[0].email
    //           ),
    //           console.log(
    //             "performerDetail.birth=====>",
    //             performerDetail[0].birth
    //           ),
    //           console.log("item=====>", item),
    //           console.log("item.finalList=====>", item.finalList),
    //           console.log("DATA =====>", data)
    //           // partakersFilter[rolesAvailable[index].id].name,
    //           // "item.finalList.name",
    //           // "abc@yopmail.com",
    //           // "12-05-1995"
    //         ]);
    //     }
    //   });
    // });
    //this.fetchUser(performerArray[0].id)
    // console.log("NAME=====> EXPORT CSV", partakersFilter[rolesAvailable[index].id].name);
    console.log("ROLES=====> EXPORT CSV", this.state.roles);
    console.log("rolesAvailable EXPORT CSV=====> ", this.state.rolesAvailable);
    console.log("performerArray EXPORT CSV=====> ", performerArray);
    // roles.map((role, index) => {
    //   if (!rolesAvailable[index]) {
    //    this.setState({performerName:role.name})
    //   }
    // })
    const values = [
      //  ["build", "2017-11-05T05:40:35.515Z"],
      // ["deploy", "2017-11-05T05:42:04.810Z"]
      performerArray,
      // [performerArray],
      // [performerArray],
      // [performerArray]
      // [this.state.performerName],
    ];

    console.log("values=====> ", values); // construct csvString
    // const headerString = "Role Name,Performer Name,Email,Date of Birth\n";
    const headerString =
      "Role Name,Performer Name,Email,Date of Birth,Website,Representation Name,Representation Email,Union Status\n";

    let temp = "";
    values.map((data) => {
      data.map((performer) => {
        console.log(" CHECK PERFORMER DATA ", performer);
        temp = temp + performer + "\n";
        console.log(" CHECK temp DATA ", temp);
      });
    });

    const rowString = values.map((d) => `${d[0]}\n`).join("");
    console.log(" ROW STRING ", temp);
    const csvString = `${headerString}${temp}`;
    let dirs = RNFetchBlob.fs.dirs;

    // write the current list of answers to a local csv file
    //const pathToWrite = `${RNFetchBlob.fs.dirs.DownloadDir}/data.csv`;
    console.log("pathToWrite", pathToWrite);

    let pathToWrite = "";
    if (Platform.OS == "android") {
      pathToWrite = `${RNFetchBlob.fs.dirs.DownloadDir}/data.csv`;
    } else {
      pathToWrite = `${RNFetchBlob.fs.dirs.DocumentDir}/data.csv`; //`${RNFetchBlob.fs.dirs.DocumentDir}/data.csv`;
    }
    // pathToWrite /storage/emulated/0/Download/data.csv

    RNFetchBlob.fs
      .writeFile(pathToWrite, csvString, "utf8")
      .then(() => {
        showMessage("success", en.csv_dowloaded);
        console.log(`wrote file ${pathToWrite}`);
        // wrote file /storage/emulated/0/Download/data.csv
      })
      .catch((error) => console.error(error));
  };

  refreshPartakers = () => {
    let { actualRound } = this.state;
    this.setState({ refreshing: true });
    this.fetchPartakers(actualRound);
  };

  onNewGroupPress = () => {
    this.state.partakers.map((data) => (data.selected = false));
    this.setState({ isSelectionMode: true });
  };

  //ON SELECTION OF IMAGE
  onSelection = (item, index) => {
    item.selected = !item.selected;
    this.state.partakers[index] = item;
    this.setState({ partakers: this.state.partakers });

    //   for (let userObject of item.user_id) {
    //     console.log("Item userObject=====>",userObject.user_id);
    // }
    this.setState({ userId: item.user_id });
    console.log("Item userId=====>" + JSON.stringify(this.state.userId));
    console.log("Item=====>" + JSON.stringify(item));
    console.log("Index=====>" + JSON.stringify(index));
  };

  //CANCEL GROUP
  cancelGroup = () => {
    this.state.partakers.map((data) => (data.selected = false));
    this.setState({
      isSelectionMode: false,
      partakers: this.state.partakers,
      currentTab: 0,
    });
  };

  //CREATE GROUP
  onCreateGroupPress = () => {
    this.createGroupApi();
  };

  onCloseGroup = () => {
    //this.setState({ isGroup: false })
    this.closeGroupApi();
  };

  // onRecordVideo = async () => {
  //   try {
  //     const video = await recordVideo();
  //     console.log(" VIDEO RECORD SUCCESSFULLY ", video);
  //   } catch (error) {
  //     console.log(" VIDEO RECORD ERROR ", error);
  //   }
  // }

  onRecordVideo = async () => {
    try {
      const response = await recordVideo();
      let uri = Platform.OS === "android" ? response.path : response.uri;
      let result = await RNThumbnail.get(uri);
      response["thumbnail"] = result.path;
      console.log(" record response ", response);
      this.setState({
        isVisible: true,
        video: response,
        isRename: false,
        fileName: "",
      });
      // if (Platform.OS === "android") {
      //   path = response.path;
      // } else {
      //   path = response.uri;
      // }
      // // this.setState({ loading: true });
      // // this.setState({ video: response }, () => this.uploadAuditionVideo());
      // this.setState({ video: response, loading: true }, () =>
      //   this.compressVideo(path)
      // );
    } catch (error) {
      console.log(" record error ", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      console.log(error);
    }
  };

  compressVideo(source) {
    const options = {
      // width: 720,
      // height: 1280,
      bitrateMultiplier: 3,
      saveToCameraRoll: true, // default is false, iOS only
      saveWithCurrentDate: true, // default is false, iOS only
      minimumBitrate: 300000,
      removeAudio: false, // default is false
    };
    ProcessingManager.compress(source, options) // like VideoPlayer compress options
      .then((data) => {
        this.setState({ loading: false });
        console.log("Compress result", data);
        let path = data;
        if (Platform.OS === "android") {
          path = path.source;
        }
        this.uploadAuditionVideo(path);
      })
      .catch((error) => {
        console.log("Compress error", error);
        const parsedError = parseError(error);
        showMessage("danger", parsedError);
        this.setState({ loading: false });
      });
  }

  uploadAuditionVideo = async (uri) => {
    const {
      video,
      name,
      audition_id,
      user_id,
      slot_id,
      actualRound,
    } = this.state;
    // let path = video.uri;
    let path = uri;
    let metadata = {
      contentType: "video/quicktime",
    };
    if (Platform.OS === "android") {
      // path = video.path;
      // path = path;
      metadata["contentType"] = "video/mp4";
    }
    this.setState({ loadingButtonVideo: true });
    this.setState({ loading: true });
    try {
      const url = await uploadAsset(
        `tablet/auditions/${audition_id}/videos`,
        path,
        name,
        metadata
      );
      const thumbnail = await uploadAsset(
        `tablet/auditions/${audition_id}/videos`,
        video.thumbnail,
        name,
        metadata
      );
      console.log("  FIREBASE URL ", url);
      const data = {
        url,
        appointment_id: actualRound.id, //audition_id,
        performer: "",
        slot_id: this.state.groupList[0].slot_id,
        // name: Date.now() //"Test 1"
        name: this.state.fileName,
        thumbnail,
      };
      console.log("Date.now()=========>", Date.now());
      await createAuditionVideo(data);
      showMessage("success", "Record audition saved");
      this.setState({ loading: false, currentTab: 0 });
    } catch (error) {
      this.setState({ loading: false });
      console.log(error);
      console.log(" upload error ", error.response);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
    this.setState({ loadingButtonVideo: false });
  };

  //CHECK RIGHT HEADER BUTTON
  renderrightIcon = () => {
    if (this.state.showSearch) {
      return (
        <Search
          placeholder={en.search_audition_text}
          value={this.state.search}
          onChange={this.onChangeSearch}
        />
      );
    } else {
      return (
        <View style={{ flexDirection: "row" }}>
          {!this.state.isGroup &&
          !this.state.isSelectionMode &&
          this.state.partakers.length > 0 &&
          this.state.online == 0 &&
          this.state.actualRound.status === 1 &&
          this.state.status == 1 ? (
            <TouchableOpacity
              style={styles.newGroupContainer}
              onPress={() => this.onNewGroupPress()}
            >
              <Image
                source={require("../../../assets/icons/auditions/group.png")}
              />
              <Text style={styles.newGroupText}>{en.new_group}</Text>
            </TouchableOpacity>
          ) : null}
          {this.state.isGroup &&
          this.state.online == 0 &&
          this.state.groupList &&
          this.state.actualRound.status === 1 &&
          this.state.status == 1 ? (
            <TouchableOpacity
              style={styles.recordVideoContainer}
              onPress={() => this.onRecordVideo()}
            >
              <Image source={Assets.record} />
              <Text style={styles.newGroupText}>{en.record_group}</Text>
            </TouchableOpacity>
          ) : null}
          {this.state.groupList &&
          this.state.online == 0 &&
          !this.state.isGroup ? (
            <HeaderButton
              icon={iconSearch}
              onPress={() => this.handlePressShowSearch()}
              containerStyle={styles.iconSearch}
            />
          ) : null}
        </View>
      );
    }
  };

  //CHECK GROUP STATUS
  checkGroupStatusApi = async () => {
    const { actualRound } = this.state;
    this.setState({ loading: true });
    try {
      const response = await groupStatus(actualRound.id);
      console.log(
        "check GroupStatus Api Response=====>",
        JSON.stringify(response)
      );
      console.log("is_group_open :", response.data.is_group_open);
      if (response.data.is_group_open) {
        const groupStatusRes =
          response.data.data.length > 0 ? response.data.data : [];

        this.state.partakers.map((partaker) => {
          groupStatusRes.map((data) => {
            if (data.user_id == partaker.user_id) {
              data["time"] = partaker.time;
            }
          });
        });

        this.setState({
          groupList: sortByTime(groupStatusRes),
          isSelectionMode: false,
          loading: false,
          isGroup: true,
        });

        console.log(
          "checkGroupStatusRes=====>",
          JSON.stringify(this.state.checkGroupStatusRes)
        );
      } else {
        this.setState({ loading: false });
      }

      this.setState({ loading: false });
    } catch (error) {
      this.setState({ loading: false });
      console.log("check GroupStatus Api ERROR=====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  // CLOSE GROUP
  closeGroupApi = async () => {
    const { actualRound } = this.state;
    this.setState({ loading: true });
    try {
      const response = await closeGroup(actualRound.id);
      console.log("CLOSE GROUP Api =====>", JSON.stringify(response));
      this.setState({ isGroup: false, loading: false, currentTab: 0 });
    } catch (error) {
      this.setState({ loading: false });
      console.log("CLOSE GROUP API ERROR=====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  //CREATE GROUP API CALL
  createGroupApi = async () => {
    const { actualRound } = this.state;
    this.state.groupList = this.state.partakers.filter((data) => data.selected);

    if (this.state.groupList.length == 0) {
      showMessage("danger", "Please select performer");
      return;
    }
    // const params = { appointment_id: audition_id, user_ids: this.state.userId };
    // console.log("PARAMS =====> ", JSON.stringify(params))

    let userId1 = [];
    this.state.partakers.map((data) => {
      if (data.selected) {
        userId1[userId1.length] = data.user_id;
      }
    });

    let params = {
      appointment_id: actualRound.id,
      user_ids: userId1,
    };
    console.log(" CHECK params ", params);

    this.setState({ loading: true });
    try {
      console.log("PARAMS =====> ", JSON.stringify(params));
      const response = await createGroup(params);
      this.state.groupList = this.state.partakers.filter(
        (data) => data.selected
      );
      console.log(" CHECK GROUP LIST ", this.state.groupList);
      this.setState({
        loading: false,
        isGroup: true,
        isSelectionMode: false,
        groupList: this.state.groupList,
      });

      console.log("SUCCESS GROUP LIST API=====>", JSON.stringify(response));
      // console.log(
      //   "SUCCESS isSelectionMode GROUP LIST API=====>",
      //   isSelectionMode
      // );
      this.setState({ loading: false, currentTab: 0 });
    } catch (error) {
      console.log(" CREATE GROUP ERROR ", error);
      this.setState({ loading: false, isSelectionMode: false, groupList: [] });
      console.log("ERROR GROUP LIST API=====>", parseError(error));
      // console.log(
      //   "ERROR GROUP LIST API isSelectionMode=====>",
      //   isSelectionMode
      // );
      showMessage("danger", parseError(error));
    }
  };

  fetchUser = async (userId) => {
    this.setState({ loading: true });
    let audition_id = this.props.navigation.getParam("audition_id", "");
    try {
      const response = await getPerformanceProfile(userId, audition_id);
      const details = response.data.data.details;
      let person = {
        details: {
          first_name: details.first_name,
          last_name: details.last_name,
          city: details.city,
          stage_name: details.stage_name,
        },
        image,
      };
      const modalData = parseDataModalPartaker(response.data.data);
      this.setState({ modalData, person, loading: false });
      console.log(
        "FETCH USER SUCCESS MANAGER TALENT DATABASE=====>",
        JSON.stringify(response.data.data)
      );
    } catch (error) {
      console.log("FetchUser ERROR =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      this.setState({ loading: false });
    }
  };

  addIndividualComment = async (params, index) => {
    this.setState({ loading: true });
    try {
      const response = await addIndividualComment(params);
      console.log("response :", response);
      showMessage("success", response.data.data);
      if (this.state.isGroup) {
        this.state.groupList[index].comment = "";
        this.setState({ groupList: this.state.groupList });
      } else {
        this.state.partakers[index].comment = "";
        this.setState({ partakers: this.state.partakers });
      }
      this.setState({ loading: false });
    } catch (error) {
      console.log("FetchUser ERROR =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      this.setState({ loading: false });
    }
  };

  onGridClick = () => {
    this.setState({ isList: false });
  };

  onListClick = () => {
    this.setState({ isList: true });
  };

  onCloseClick = () => {
    this.setState({ isVisible: false });
  };

  onSubmitClick = () => {
    if (this.state.isRename) {
    } else {
      if (Platform.OS === "android") {
        path = this.state.video.path;
      } else {
        path = this.state.video.uri;
      }
      // this.setState({ loading: true });
      // this.setState({ video: response }, () => this.uploadAuditionVideo());
      console.log("this.state 222:", this.state.video);
      // return
      this.setState({ loading: true }, () => this.compressVideo(path));
    }
    this.setState({ isVisible: false });
  };

  onChangeText = (fileName) => {
    this.setState({ fileName });
  };

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    );
  };

  render() {
    const {
      showSearch,
      search,
      partakers,
      showAuditionVideos,
      currentTab,
      showOptions,
      fileSelected,
      optionsPosition,
      enterMonitorModeModal,
      pinModalCheckin,
      pinModalMonitor,
      enterCheckinModeModal,
      loading,
      title,
      date,
      time,
      appointmentsNumber,
      union,
      contract,
      production,
      description,
      url,
      contributors,
      roles,
      files,
      status,
      loadingButton,
      companyName,
      auditionVideos,
      checkinPin,
      updatesPin,
      viewSplitAnimatedValue,
      viewSplitAnimatedValueA,
      position,
      zIndex,
      rolesAvailable,
      indexItem,
      isFinalList,
      chagingType,
      optionsPosition2,
      actualRound,
      audition_rounds,
      loadingButtonRound,
      modalCloseAudition,
      modalOpenNewRound,
      entreConfirm,
      modalAudition,
      loadingButtonC,
      showFinal,
      online,
      loadingRoundStart,
      refreshing,
      submissions,
    } = this.state;

    let showFinalCastButton = false;

    if (actualRound.status === 0) {
      showFinalCastButton = true;
    }

    const partakersFilter = partakers.filter((partaker) => {
      let name = partaker.name.toUpperCase();
      //this.setState({performerName:name})
      return name.match(new RegExp(search.toUpperCase(), "g"));
    });

    const widthFirstContainerA = viewSplitAnimatedValueA.interpolate({
      inputRange: [-resize(405), 0],
      outputRange: [-resize(405), 0],
    });

    const widthFirstContainer = viewSplitAnimatedValue.interpolate({
      inputRange: [0, resize(255)],
      outputRange: [0, resize(255)],
    });

    if (loading) {
      return <Loading />;
    }
    console.log("GROUP LIST =====>" + JSON.stringify(this.state.groupList));
    console.log("actual round=====>" + JSON.stringify(actualRound));
    console.log("isFinal list====>" + isFinalList);
    console.log("STATE DATA CREATE GROUP=========>", this.state);
    console.log("AUDITION TITLE=========>", this.state.title);
    return (
      <Container>
        {this.renderFeedbackSelection()}
        {this.renderFeedbackAddModal()}
        {this.renderInfoModal()}
        {this.renderRenameModal()}
        {this.renderSaveLaterModal()}
        {this.state.isLoading ? <ProgressLoader isLoading={true} /> : null}
        <AlertIOS
          title={en.close_audition_title}
          show={entreConfirm}
          dismiss={this.handleConfirmOut}
          agree={this.goBack}
        >
          <Text style={styles.monitorModeText}>{en.confirmation_out}</Text>
        </AlertIOS>
        {/* check-in mode alerts */}
        <AlertIOS
          title={en.open_checkin_title}
          show={enterCheckinModeModal}
          dismiss={this.onPressCheckinMode}
          agree={this.handleEnterCheckinMode}
        >
          <Text style={styles.monitorModeText}>
            {en.open_checkin_description}
          </Text>
        </AlertIOS>
        {/* close audition alert */}
        <AlertIOS
          title={en.close_audition}
          show={modalAudition}
          dismiss={this.handleCloseAudition}
          agree={this.onPressCloseAuditions}
        >
          <Text style={styles.monitorModeText}>{en.open_audition_title}</Text>
        </AlertIOS>
        <AlertIOS
          title={en.close_audition_info_title}
          show={modalCloseAudition}
          dismiss={this.onPressCloseRound}
          agree={this.onAcceptCloseRound}
        >
          <Text style={styles.monitorModeText}>{en.close_audition_info}</Text>
        </AlertIOS>
        {/* open new round alert */}
        <AlertIOS
          title={en.open_audition_round_title}
          show={modalOpenNewRound}
          dismiss={this.onPressNewRound}
          agree={this.onRequestNewRound}
        >
          <Text style={styles.monitorModeText}>{en.open_audition_round}</Text>
        </AlertIOS>
        <AlertIOS
          title={en.set_passcode_title}
          show={pinModalCheckin}
          dismiss={this.handlerDismissCheckinModePin}
          cancelTitle={en.cancel_button_text}
          acceptTitle={en.set_button_text}
          agree={this.onPressSetPinCheckin}
        >
          <View style={styles.pinLabelContainer}>
            <Text style={styles.pin}>{checkinPin}</Text>
          </View>
          <PinKeyboard onPressDigit={this.handleChangeCheckinPin} />
        </AlertIOS>
        {/* monitor mode alerts */}
        <AlertIOS
          title={en.monitor_mode_title}
          show={enterMonitorModeModal}
          dismiss={this.handleToggleMonitorMode}
          agree={this.handleEnterMonitorMode}
        >
          <Text style={styles.monitorModeText}>
            {en.monitor_mode_description}
          </Text>
        </AlertIOS>
        <AlertIOS
          title={en.set_passcode_title}
          show={pinModalMonitor}
          dismiss={this.handlerDismissMonitorModePin}
          cancelTitle={en.cancel_button_text}
          acceptTitle={en.set_button_text}
          agree={this.onPressSetPinMonitorMode}
        >
          <View style={styles.pinLabelContainer}>
            <Text style={styles.pin}>{updatesPin}</Text>
          </View>
          <PinKeyboard onPressDigit={this.handleChangeUpdatesPin} />
        </AlertIOS>
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
          center={<HeaderText title={title} />}
          right={this.renderrightIcon()}
        />
        <View style={styles.wrapperContent}>
          <FileMenu
            show={showOptions}
            dismiss={() => this.onPressFileMore({})}
            posy={optionsPosition}
            showOptionShare={fileSelected.shareable === "yes" ? true : false}
            share={() => this.shareAsset(fileSelected)}
            showOptionOpen
            openIn={() => this.openAsset(fileSelected)}
            showOptionDelete
            delete={() => this.handleDeleteFile(fileSelected)}
          >
            {console.log("fileSelected.file :", fileSelected)}
            {"id" in fileSelected && (
              <File
                filename={fileSelected.name}
                fileType={fileSelected.type}
                file={fileSelected}
                onPressMore={() => this.onPressFileMore({})}
              />
            )}
          </FileMenu>
          {status === 0 && (
            <AnimatedView
              style={[
                styles.firstSectionAnimated,
                {
                  paddingRight: widthFirstContainer,
                },
              ]}
            >
              <View style={styles.noAuditions}>
                <Text style={styles.noAuditionsText}>
                  {!isFinalList ? en.checkin_no_open : ""}
                </Text>
              </View>
            </AnimatedView>
          )}
          {(status === 1 || status === 2) && (
            <AnimatedView
              style={[
                styles.firstSectionAnimated,
                {
                  paddingRight: widthFirstContainer,
                },
              ]}
            >
              {!isFinalList &&
              this.state.status == 1 &&
              (this.state.isGroup
                ? this.state.groupList.length > 0
                : partakersFilter.length > 0) ? (
                <View
                  style={{
                    flexDirection: "row",
                    marginLeft: resize(31),
                    marginVertical: resize(10),
                  }}
                >
                  <TouchableOpacity
                    style={{
                      backgroundColor: !this.state.isList
                        ? "#414041"
                        : "#D8D7D8",
                      padding: 10,
                    }}
                    onPress={this.onGridClick}
                  >
                    <Image
                      source={Assets.grid}
                      style={{
                        width: 20,
                        height: 20,
                        tintColor: !this.state.isList ? "#ffffff" : "#000000",
                      }}
                      resizeMode={"contain"}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      backgroundColor: this.state.isList
                        ? "#414041"
                        : "#D8D7D8",
                      padding: 10,
                    }}
                    onPress={this.onListClick}
                  >
                    <Image
                      source={Assets.list}
                      style={{
                        width: 20,
                        height: 20,
                        tintColor: this.state.isList ? "#ffffff" : "#000000",
                      }}
                      resizeMode={"contain"}
                    />
                  </TouchableOpacity>
                </View>
              ) : null}

              <Content
                bounces={false}
                style={styles.firstSection}
                keyboardShouldPersistTaps="always"
              >
                <View style={[styles.partakerContainer, { marginLeft: 0 }]}>
                  {!isFinalList
                    ? console.log(
                        "partakersFilter =======>" +
                          JSON.stringify(partakersFilter)
                      )
                    : null}

                  {console.log("STATUS=====>", this.state.status)}
                  {console.log("actualRound.status=====>", actualRound.status)}
                  {console.log(
                    "online == 0 && this.state.status == 1 && actualRound.status == 1 ? true : false",
                    online == 0 &&
                      this.state.status == 1 &&
                      actualRound.status == 1
                      ? true
                      : false
                  )}

                  {!isFinalList ? (
                    <FlatList
                      contentContainerStyle={styles.partakerContainer}
                      // style={styles.content}
                      // numColumns={2}
                      keyboardShouldPersistTaps="always"
                      data={
                        this.state.isGroup &&
                        this.state.online == 0 &&
                        actualRound.status == 1
                          ? this.state.groupList
                          : partakersFilter
                      } //&& this.state.checkGroupStatusRes == null && this.state.checkGroupStatusRes == "" && this.state.checkGroupStatusRes == []
                      extraData={this.state}
                      // keyExtractor={item => item.slot_id.toString()}
                      keyExtractor={(item, index) => item + index}
                      renderItem={({ item, index }) => (
                        <Partaker
                          online={online}
                          name={
                            item.name || item.first_name + " " + item.last_name
                          }
                          image={item.image}
                          time={item.time}
                          groupNumber={item.group_number}
                          isSelected={item.selected}
                          isSelectionMode={this.state.isSelectionMode}
                          favorite={item.favorite === 1 ? true : false}
                          isOnline={
                            online == 0 &&
                            this.state.status == 1 &&
                            actualRound.status == 1 &&
                            this.state.isManger
                              ? true
                              : false
                          }
                          onPress={() => {
                            actualRound.status === 1
                              ? this.onPressPartaker(index)
                              : // ? this.onPressPartaker(item)
                                // this.onPressPartaker(index)
                                this.onFinalListPress(index);
                          }}
                          //onSelection={this.onSelection}
                          onSelectClick={() => {
                            this.onSelection(item, index);
                          }}
                          //ON PRESS DONE
                          onPressDone={() => {
                            if (item.is_feedback_sent == 1) {
                              showMessage("danger", "Feedback Already sent");
                            } else {
                              this.currentItem = item;
                              this.currentIndex = index;
                              this.setState({ isFeedbackSelection: true });
                            }
                            // this.onFeedbackDone(item, index);
                          }}
                          onSaveLater={() => {
                            this.onFeedbackSaveLater(item, index);
                          }}
                          //ON PRESS CLOSE
                          onPressClose={() => {
                            //this.setState({isInfoVisible:true})
                            this.onFeedbackClose(item, index);
                          }}
                          isList={this.state.isList}
                          onAddComment={() => {
                            Keyboard.dismiss();

                            if (
                              item.comment == undefined ||
                              item.comment == ""
                            ) {
                              showMessage("danger", "Please add comment");
                              return;
                            }

                            if (
                              item.comment != undefined &&
                              item.comment != ""
                            ) {
                              let params = {
                                appointment_id: this.state.actualRound.id,
                                user_id: item.user_id,
                                comment: item.comment,
                              };
                              console.log("params :", params);

                              this.addIndividualComment(params, index);
                            }
                          }}
                          comment={
                            item.comment != undefined ? item.comment : ""
                          }
                          onCommentChange={(comment) => {
                            if (this.state.isGroup) {
                              this.state.groupList[index].comment = comment;
                              this.setState({
                                groupList: this.state.groupList,
                              });
                            } else {
                              this.state.partakers[index].comment = comment;
                              this.setState({
                                partakers: this.state.partakers,
                              });
                            }
                          }}
                        />
                      )}
                      ListEmptyComponent={
                        <Text style={styles.emptyText}>
                          {en.no_performances}
                        </Text>
                      }
                      refreshing={refreshing}
                      onRefresh={this.refreshPartakers}
                      showsVerticalScrollIndicator={false}
                    />
                  ) : null}
                </View>
              </Content>
              {this.state.isSelectionMode && this.state.online == 0 ? (
                <View
                  style={{
                    paddingHorizontal: 35,
                    flexDirection: "row",
                    marginVertical: 10,
                    // bottom: Platform.OS === "ios" ? 130 : 90,
                    // position: "absolute"
                  }}
                >
                  <TouchableOpacity
                    style={{
                      alignItems: "center",
                      borderColor: "gray",
                      borderWidth: 1,
                      borderRadius: 5,
                      justifyContent: "center",
                      padding: 10,
                    }}
                    onPress={() => {
                      this.cancelGroup();
                    }}
                  >
                    <Text
                      style={{
                        color: "gray",
                        fontSize: resize(18),
                        fontFamily: NEXALIGHT,
                      }}
                    >
                      Cancel Group
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      alignItems: "center",
                      borderColor: "gray",
                      borderWidth: 1,
                      borderRadius: 5,
                      justifyContent: "center",
                      padding: 10,
                      marginLeft: 18,
                      backgroundColor: "#4d2545",
                    }}
                    onPress={() => {
                      this.onCreateGroupPress();
                    }}
                  >
                    <Text
                      style={{
                        fontSize: resize(18),
                        fontFamily: NEXALIGHT,
                        color: "white",
                      }}
                    >
                      Create Group
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : null}
              {this.state.isGroup &&
              this.state.online == 0 &&
              this.state.groupList &&
              this.state.actualRound.status === 1 &&
              this.state.status == 1 ? (
                <View
                  style={{
                    paddingHorizontal: 20,
                    flexDirection: "row",
                    marginVertical: 10,
                    // bottom: Platform.OS === "ios" ? 100 : 80,
                    // position: "absolute"
                  }}
                >
                  <TouchableOpacity
                    style={{
                      alignItems: "center",
                      borderColor: "gray",
                      borderWidth: 1,
                      borderRadius: 5,
                      justifyContent: "center",
                      padding: 10,
                    }}
                    onPress={() => {
                      this.onCloseGroup();
                    }}
                  >
                    <Text
                      style={{
                        color: "gray",
                        fontSize: resize(18),
                        fontFamily: NEXALIGHT,
                      }}
                    >
                      Close Group
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : null}
            </AnimatedView>
          )}

          <Animated.View
            style={[styles.lateralLayout, { right: widthFirstContainerA }]}
          >
            <TouchableOpacity
              style={styles.spiltContainer}
              onPress={this.handleSideMenu}
            >
              <View style={styles.spiltIcon} />
            </TouchableOpacity>
            <View style={styles.secondSection}>
              {showAuditionVideos && (
                <View style={styles.auditionVideosContainer}>
                  <View style={styles.auditionVideosHeader}>
                    <TouchableOpacity
                      onPress={() => this.onPressBackToTabs()}
                      style={styles.iconArrowLeftContainer}
                    >
                      <Image style={styles.iconArrowLeft} source={ArrowLeft} />
                    </TouchableOpacity>
                    <View style={styles.auditionVideosTitleContainer}>
                      <Text style={styles.auditionVideosTitle}>
                        {en.audition_videos_title + "111"}
                      </Text>
                    </View>
                  </View>

                  <Content style={styles.auditionVideosContent}>
                    <View style={styles.videosContainer}>
                      {console.log("Auition videos list 111 :")}
                      {auditionVideos.length > 0 &&
                        auditionVideos.map((file, index) => {
                          return (
                            <File
                              key={index}
                              refs={(fileRef) =>
                                this.handleSetRef(fileRef, file)
                              }
                              filename={file.name}
                              fileType={file.type}
                              file={file}
                              onPressMore={() =>
                                this.onPressFileMore(file, true)
                              }
                            />
                          );
                        })}
                      {auditionVideos.length === 0 && (
                        <Text style={styles.emptyText}>{en.no_videos}</Text>
                      )}
                    </View>
                  </Content>
                </View>
              )}
              {console.log("!showAuditionVideos =====>", !showAuditionVideos)}
              {console.log("!isFinalList =====>", !isFinalList)}
              {console.log("currentTab =====>", currentTab)}
              {!showAuditionVideos && !isFinalList && (
                <Tabs
                  onChangeTab={({ i }) => this.handleChangeTab(i)}
                  tabContainerStyle={styles.tabsContainer}
                  page={!this.state.isManger ? 0 : currentTab}
                  tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
                  locked
                  ref={(c) => {
                    this.tabs = c;
                    return;
                  }}
                >
                  <Tab
                    heading={en.info_title}
                    tabStyle={styles.tabStyle}
                    textStyle={styles.tabTextStyle}
                    activeTextStyle={styles.tabActiveTextStyle}
                    activeTabStyle={styles.activeTabStyle}
                  >
                    <Content style={styles.contentRight}>
                      <View style={styles.auditionInfoContainer}>
                        <Text style={styles.auditionName}>{title}</Text>
                        {online === 1 ? null : (
                          <View style={styles.auditionDateContainer}>
                            <Text style={styles.auditionDate}>{date}</Text>
                            <Text style={styles.auditionDate}>{time}</Text>
                          </View>
                        )}
                        <Text style={styles.auditionCompany}>
                          {companyName}
                        </Text>
                      </View>
                      <View style={styles.appointmentsContainer}>
                        <Text style={styles.appointments}>
                          {`${
                            this.state.online === 0
                              ? appointmentsNumber
                                ? appointmentsNumber
                                : 0
                              : submissions
                              ? submissions
                              : 0
                          } ${
                            this.state.online === 0
                              ? en.appointments
                              : en.submissions
                          }`}
                        </Text>
                      </View>
                      <ScrollView
                        style={styles.tagsContainer}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        bounces={false}
                      >
                        <UnionTag
                          name={union}
                          selected
                          containerSelectedStyle={styles.tagPurple}
                          disabled
                        />
                        <ContractTag
                          name={contract}
                          selected
                          containerSelectedStyle={styles.tagYellow}
                          disabled
                        />
                        {production.length > 0 &&
                          production.map((tag, index) => {
                            return (
                              <ProductionTag
                                key={index}
                                name={tag}
                                selected
                                containerSelectedStyle={styles.tagRed}
                                disabled
                              />
                            );
                          })}
                      </ScrollView>
                      <View style={styles.auditionDescriptionContainer}>
                        <Text style={styles.auditionDescription}>
                          {description}
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={styles.auditionUrlContainer}
                        onPress={() => this.handlePressUrl()}
                      >
                        <Text style={styles.auditionUrl}>{url}</Text>
                      </TouchableOpacity>
                      <ScrollView
                        horizontal
                        bounces={false}
                        showsHorizontalScrollIndicator={false}
                        style={styles.rolesContainer}
                      >
                        {roles.length > 0 &&
                          roles.map((role, index) => {
                            return (
                              <Role
                                key={index}
                                text={role.name}
                                icon={
                                  role.image
                                    ? { uri: role.image.url }
                                    : RoleIcon
                                }
                                disabled
                              />
                            );
                          })}
                        {roles.length === 0 && (
                          <Text style={styles.emptyText}>{en.no_roles}</Text>
                        )}
                      </ScrollView>
                      <View style={styles.resourcesContainer}>
                        {files.length > 0 &&
                          files.map((file, index) => {
                            return (
                              <File
                                key={index}
                                refs={(fileRef) =>
                                  this.handleSetRef(fileRef, file)
                                }
                                filename={file.name}
                                fileType={file.type}
                                file={file}
                                onPressMore={() => this.onPressFileMore(file)}
                              />
                            );
                          })}
                        {files.length === 0 && (
                          <Text style={styles.emptyText}>{en.no_files}</Text>
                        )}
                      </View>
                      <View style={styles.contributorsContainer}>
                        {contributors.length > 0 &&
                          contributors.map((contributor, index) => {
                            return (
                              <Contributor
                                key={index}
                                name={this.parseContributorName(contributor)}
                              />
                            );
                          })}
                        {contributors.length === 0 && (
                          <Text style={styles.emptyText}>
                            {en.no_contributors}
                          </Text>
                        )}
                      </View>
                      <View style={styles.editContainer}>
                        {status === 0 || status === 1 ? (
                          <GradientButton
                            buttonText={en.edit_button_text}
                            onPress={() => this.handlePressEnableEdit()}
                          />
                        ) : null}
                      </View>
                    </Content>
                  </Tab>
                  {this.state.isManger ? (
                    <Tab
                      heading={en.manage_title}
                      tabStyle={styles.tabStyle}
                      textStyle={styles.tabTextStyle}
                      activeTextStyle={styles.tabActiveTextStyle}
                      activeTabStyle={styles.activeTabStyle}
                    >
                      <Content style={styles.contentManage}>
                        <FakeDropDown
                          showFinal={showFinal}
                          status={status}
                          refs={(fakedrop) => (this.fakedrop = fakedrop)}
                          animation={this.animatedValue}
                          chagingType={chagingType}
                          position={optionsPosition2}
                          onRequestNewRound={() =>
                            this.changeTypeOfRounds(false, true)
                          }
                          options={audition_rounds}
                          text={actualRound.round}
                          changeTypeOfFeddOptions={this.changeTypeOfRounds}
                          onChangeValue={this.changeRoundOptions}
                          onPresFinalRound={this.onPresFinalRound}
                        />
                        {/* <Text>
                        showFinal ==>{JSON.stringify(showFinal)} \n status ==>
                        {status} \n chagingType ==>{JSON.stringify(chagingType)}{" "}
                        \n audition_rounds ==>{JSON.stringify(audition_rounds)}{" "}
                        \n actualRound.round ==>
                        {JSON.stringify(actualRound.round)} \n
                        actualRound.status ==>
                        {JSON.stringify(actualRound.status)} \n online ==>
                        {online} \n showFinalCastButton ==>
                        {JSON.stringify(showFinalCastButton)} \n
                      </Text> */}

                        {actualRound.status === 0 && !showFinal && (
                          <ManageButton
                            title={en.closed_round}
                            bordered
                            onPress={() => {}}
                          />
                        )}
                        {actualRound.status === 0 && !showFinal && (
                          <ManageButton
                            title={en.reopen_round_button_text}
                            bordered
                            onPress={this.onReOpenRound}
                          />
                        )}
                        {actualRound.status === 1 &&
                          status !== 2 &&
                          online === 0 &&
                          status !== 0 && (
                            <ManageButton
                              title={en.checkin_mode_button_text}
                              bordered
                              onPress={() => this.onPressCheckinMode()}
                            />
                          )}
                        {actualRound.status === 1 &&
                          status !== 2 &&
                          online === 0 &&
                          status !== 0 && (
                            <ManageButton
                              title={en.manual_checkIn_mode_button_text}
                              bordered
                              onPress={() => this.onPressManualCheckinMode()}
                            />
                          )}
                        {actualRound.status === 1 &&
                          status !== 2 &&
                          online === 0 &&
                          status !== 0 && (
                            <ManageButton
                              title={en.monitor_mode_button_text}
                              bordered
                              onPress={() => this.onPressMonitorMode()}
                            />
                          )}
                        {status === 0 || status === 2 ? (
                          <ManageButton
                            title={en.open_audition_button_text}
                            bordered
                            onPress={() => this.onPressOpenAuditions()}
                            loading={loadingButton}
                          />
                        ) : null}

                        {status === 0 && online === 0 && (
                          <ManageButton
                            title={en.appointments_button_text}
                            bordered
                            onPress={() => this.onAppointments()}
                            loading={loadingButton}
                          />
                        )}

                        {online === 0 ? (
                          <ManageButton
                            title={en.push_notification_button_text}
                            bordered
                            onPress={this.onPushNotification}
                            loading={loadingButton}
                          />
                        ) : null}

                        {actualRound.status === 1 && status !== 2 && (
                          <ManageButton
                            title={`${en.close_round_button_text} ${actualRound.round}`}
                            bordered
                            onPress={() => this.onPressCloseRound()}
                            loading={loadingButtonRound}
                          />
                        )}
                        {/* {(!showFinal && online === 0) && (
													<ManageButton
														title={en.audition_videos_title}
														bordered
														onPress={() => this.onPressAuditionVideos()}
													/>
												)} */}
                        {/* {
													(status === 2 && showFinal) && (
														<ManageButton
															title={en.final_cating_title}
															bordered
															onPress={() => this.onPressFinaliList()}
														/>
													)
												} */}

                        <ManageButton
                          title={en.audition_videos}
                          bordered
                          onPress={() => this.handlePressVideoList()}

                          // onPress={() => this.handlePress()}
                          // loading={loadingButton}
                        />

                        <ManageButton
                          title={en.audition_track}
                          bordered
                          onPress={() => this.onExportAuditionEdit()}
                        />
                        {/* {((status === 2 && showFinal) || showFinalCastButton) && (
                        <ManageButton
                          title={en.final_cating_title}
                          bordered
                          onPress={() => this.onPressFinaliList()}
                        />
                      )} */}

                        {status !== 1 && (
                          <ManageButton
                            title={en.final_cating_title}
                            bordered
                            onPress={() => this.onPressFinaliList()}
                          />
                        )}

                        {status !== 0 && (
                          <ManageButton
                            title={en.export_analytics}
                            bordered
                            onPress={this.onExportAnalytic}
                          />
                        )}

                        {actualRound.status === 1 &&
                          status !== 2 &&
                          online === 0 &&
                          status !== 0 && (
                            <ManageButton
                              title={en.hidden_performers}
                              bordered
                              onPress={this.onHiddenPerformers}
                            />
                          )}

                        {status === 1 && (
                          <ManageButton
                            fully
                            title={en.end_auditions}
                            onPress={() => this.handleCloseAudition()}
                            loading={loadingButton}
                          />
                        )}
                      </Content>
                    </Tab>
                  ) : null}
                </Tabs>
              )}
            </View>
          </Animated.View>
          {!isFinalList ? null : (
            <DragContainer
              onAnimationEnds={this.onAnimationEnds}
              style={styles.upper}
              onDragStart={this.dragStart}
            >
              {/* Person container */}
              <DropZone
                id={"A"}
                style={styles.one}
                onDrop={(event) => this.animateToCoord(event)}
              />

              <ScrollView
                scrollEventThrottle={10}
                onScroll={this.handleScroll}
                bounces={false}
                showsVerticalScrollIndicator={false}
                style={styles.scrollPerson}
                contentContainerStyle={styles.scrollPerson2}
              >
                {partakersFilter.map((partaker, index) => {
                  return (
                    <Draggable
                      key={"Drag" + index}
                      id={index}
                      position={position[index]}
                      style={styles.dragable}
                      onAnimationEnds={this.onAnimationEnds}
                      onLongPress={"onPressIn"}
                    >
                      <PartakerDrag
                        name={partaker.name}
                        image={partaker.image}
                        styleManage={
                          indexItem[index] === 0 ? false : !indexItem[index]
                        }
                      />
                    </Draggable>
                  );
                })}
              </ScrollView>

              {/* Roles container */}
              <View style={[styles.two, { zIndex }]}>
                <View style={styles.twoP}>
                  <ScrollView
                    bounces={false}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={styles.twoI}
                  >
                    <View style={styles.auditionVideosHeader}>
                      <TouchableOpacity
                        onPress={() => this.onPressFinaliList()}
                        style={styles.iconArrowLeftContainer}
                      >
                        <Image
                          style={styles.iconArrowLeft}
                          source={ArrowLeft}
                        />
                      </TouchableOpacity>
                      <View style={styles.auditionVideosTitleContainer}>
                        <Text style={styles.auditionVideosTitle}>
                          {en.final_cating_title}
                        </Text>
                      </View>
                    </View>
                    {console.log("rolesAvailable======>", rolesAvailable)}
                    {roles.map((role, index) => {
                      if (!rolesAvailable[index]) {
                        return (
                          <View key={`DropZone${index}`}>
                            <DropZone
                              setDropZoneRef={this.setDropZoneRef}
                              id={index}
                              style={styles.dragRoles}
                              onDrop={(event) => this.animateToCoord(event)}
                            />
                            <View style={styles.nameRol}>
                              <Text style={styles.name}>{role.name}</Text>
                            </View>
                          </View>
                        );
                      }
                      return (
                        <View key={`DragCustom${index}`}>
                          <Draggable
                            id={1000 + index}
                            style={styles.dragRoles}
                            onAnimationEnds={this.onAnimationEnds}
                          >
                            {/* DROP AREA  */}
                            <PartakerDrag
                              name={
                                partakersFilter[rolesAvailable[index].id].name
                              }
                              image={
                                partakersFilter[rolesAvailable[index].id].image
                              }
                              styleManage
                            />
                          </Draggable>
                          <View style={styles.nameRol}>
                            <Text style={styles.name}>{role.name}</Text>
                          </View>
                        </View>
                      );
                    })}
                  </ScrollView>
                </View>
                <View style={styles.options}>
                  <TouchableOpacity
                    onPress={() => this.onExport()}
                    style={styles.export}
                  >
                    <Text style={styles.nameRol}>{en.export}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </DragContainer>
          )}
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(" REDUX DATA CREATE GROUP=========>", state);

  return {
    user: state.user.data,
    feedback: state.user.instantFeedback,
  };
};

export default withNavigationFocus(connect(mapStateToProps)(AuditionsManage));
