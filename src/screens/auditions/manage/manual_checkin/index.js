import React, { Component } from "react";
import {
  View,
  Text,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  Modal,
} from "react-native";
import { Content, Input } from "native-base";
import { StackActions, NavigationActions } from "react-navigation";
import moment from "moment";
import { connect } from "react-redux";
import DraggableFlatList from "react-native-draggable-flatlist";

//custom
import styles from "./style";
import LightBar from "../../../../components/commons/light_bar";
import Header from "../../../../components/commons/header";
import HeaderText from "../../../../components/commons/header_text";
import CheckButton from "../../../../components/auditions/check_button";
import AppointmentsItem from "../../../../components/auditions/appointment_item";
import UpdatesButton from "../../../../components/auditions/updates_button";
import GradientButton from "../../../../components/commons/gradient_button";
import TimelineItem from "../../../../components/auditions/timeline_item";
import AlertIOS from "../../../../components/commons/alert_ios";
import PinKeyboard from "../../../../components/commons/pin_keyboard";
import Loading from "../../../../components/commons/loading";
import showMessage from "../../../../utils/show_message";
import parseError from "../../../../utils/parse_error";
import GigIcon from "../../../../assets/icons/auditions/gigIcon.png";
import BackgroundWrapper from "../../../../components/commons/background_wrapper";
import ModalDropdown from "react-native-modal-dropdown";
import picker_downflag from "../../../../assets/icons/select_radius/dropdown.png";
import { completeCheckin } from "../../../../api/auditions";

//locale
import en from "../../../../locale/en";

//actions
import { DESTROY_MONITOR_UPDATES_PIN } from "../../../../actions/manage";

//api
import {
  getAuditionAppointments,
  getAuditionNotifications,
  createAuditionUpdate,
  getAuditionPreNotifications,
  reorderAppointments,
  getRoundSlots,
  getAllPerformers,
  getWalkAppointments,
} from "../../../../api/auditions";
import {
  BREAK_BOLD,
  NEXABOLD,
  NEXALIGHT,
  resize,
} from "../../../../assets/styles";
import colors from "../../../../utils/colors";
import ManageButton from "../../../../components/auditions/manage_button";

class ManualCheckInMode extends Component {
  state = {
    audition_id: this.props.navigation.getParam("audition_id", ""),
    appointment_id: this.props.navigation.getParam("appointment_id", ""),
    slots: {},
    actualRound: this.props.navigation.getParam("actualRound", ""),
    audition: this.props.navigation.getParam("audition", ""),
    appointments: [],
    loading: true,
    loadingButton: false,
    updates: [],
    preUpdates: [],
    updateSelected: {},
    dismissOutManualCheckIn: false,
    pinModalMonitor: false,
    pin: "",
    newNotification: "",
    showNewNotificationInput: false,
    // performerList: ["0", "1", "2", "3", "4", "5", "6", "7"],
    performerList: [],
    onManualCheckIn: false,
    onTimeSlotSelection: false,
    selectedTimeSlotIndex: null,
    timeSlots: ["10:00 AM", "12:00 PM", "1:00 PM", "4:00 PM"],
    selectedPerformer: null,
    isDescName: true,
    isDescStatus: true,
    isDescAppointmentTime: true,
    loading: false,
  };

  componentDidMount() {
    // this.fetchNotification();
    // this.fetchPreNotification();
    // this.getSlotsByRound();
    this.getAvailableTimeSlots();
    this.getAllPerformerList(true);
  }
  getAvailableTimeSlots = async () => {
    let { appointment_id, audition_id } = this.state;
    this.setState({ loading: true });
    try {
      const responseSlotsList = await getWalkAppointments(appointment_id);
      if (responseSlotsList.data && responseSlotsList.status == 200) {
        let slotList = responseSlotsList.data
          ? responseSlotsList.data.data
            ? responseSlotsList.data.data
              ? responseSlotsList.data.data.slots
                ? responseSlotsList.data.data.slots
                : []
              : []
            : []
          : [];
        if (slotList.length > 0) {
          this.setState({ timeSlots: slotList });
        }
      }
      this.setState({ loading: false });
      console.log(JSON.stringify(responseSlotsList));
    } catch (error) {
      console.log("get All performer =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      this.setState({ loading: false, timeSlots: [] });
    }
  };
  getAllPerformerList = async (isFirst) => {
    let { appointment_id, audition_id } = this.state;
    this.setState({ loading: true });
    try {
      // const responseSlotsList = await getWalkAppointments(appointment_id);
      // if (responseSlotsList.data && responseSlotsList.status == 200) {
      //   let slotList = responseSlotsList.data
      //     ? responseSlotsList.data.data
      //       ? responseSlotsList.data.data
      //         ? responseSlotsList.data.data.slots
      //           ? responseSlotsList.data.data.slots
      //           : []
      //         : []
      //       : []
      //     : [];
      //   if (slotList.length > 0) {
      //     this.setState({ timeSlots: slotList });
      //   }
      // }
      // console.log(JSON.stringify(responseSlotsList));

      let responsePerformerList = await getAllPerformers(
        appointment_id,
        isFirst ? "" : this.state.isDescName ? "DESC" : "ASC"
      );
      if (responsePerformerList.data && responsePerformerList.status == 200) {
        let data = responsePerformerList.data
          ? responsePerformerList.data.data
            ? responsePerformerList.data.data
            : []
          : [];
        if (data.length > 0) {
          this.setState({ performerList: data });
        }
        console.log(JSON.stringify(responsePerformerList));
      }
      this.setState({ loading: false });
    } catch (error) {
      console.log("get All performer =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      this.setState({ loading: false });
    }
  };
  getSlotsByRound = async () => {
    let { actualRound } = this.state;
    try {
      let responseSlots = await getRoundSlots(actualRound.id);
      let newSlots = [];
      let slots = responseSlots.data.data;
      if (slots.length > 0) {
        this.setState({ slots: slots }, () => {
          this.fetchSelectedAppointments();
        });
      } else {
        for (const key in slots) {
          newSlots.push(slots[key]);
        }
        this.setState({ slots: newSlots }, () => {
          this.fetchSelectedAppointments();
        });
      }
    } catch (error) {
      console.log("getSlotsByRound =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  fetchSelectedAppointments = async () => {
    const { actualRound, slots } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getAuditionAppointments(actualRound.id);
      let search = response.data.data;
      console.log("fetchSelectedAppointments SEARCH =====>", search);
      let appointments = [];
      for (const iterator of slots) {
        let findTime = search.find((snap) => {
          return snap.time === iterator.time;
        });
        if (findTime) {
          appointments.push(findTime);
        } else {
          appointments.push({ name: "Free" });
        }
      }

      this.setState({
        appointments,
      });
      console.log(
        "fetchSelectedAppointments appointments =====>",
        this.state.appointments
      );
    } catch (error) {
      console.log("fetchSelectedAppointments ERROR =====>", error);
    }

    this.setState({ loading: false });
  };

  fetchNotification = async () => {
    const { audition_id } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getAuditionNotifications(audition_id);

      this.setState({
        updates: response.data.data,
      });
    } catch (error) {
      console.log("fetchNotification=====>", error);
    }

    this.setState({ loading: false });
  };

  fetchPreNotification = async () => {
    const { audition_id } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getAuditionPreNotifications(audition_id);

      this.setState({
        preUpdates: response.data.data,
      });
    } catch (error) {
      console.log("fetchNotification=====>", error);
    }

    this.setState({ loading: false });
  };

  handlePressCheckButton = () => {
    this.setState({
      dismissOutManualCheckIn: true,
      pinModalMonitor: false,
      pin: "",
    });
  };

  handlePressUpdate = (updateSelected) => {
    if (this.state.updateSelected === updateSelected) {
      return this.setState({ updateSelected: {} });
    }

    return this.setState({ updateSelected });
  };

  handlePressAddUpdate = () => {
    this.setState({
      showNewNotificationInput: true,
      updateSelected: {},
    });
  };

  handlePressSendUpdate = async () => {
    const {
      updates,
      newNotification,
      updateSelected,
      audition_id,
      preUpdates,
    } = this.state;

    const hasError = this.validateSendNotification();

    if (hasError !== "") {
      return showMessage("danger", hasError);
    }

    this.setState({ loadingButton: true });

    try {
      let notification = {
        time: moment().format("HH:mm:ss"),
        appointment: audition_id,
      };
      let preUpdate = "";
      let newPreUpdates = [...preUpdates];

      if (updateSelected.hasOwnProperty("title")) {
        notification["title"] = updateSelected.title;
      } else {
        notification["title"] = newNotification;
        preUpdate = newNotification;
      }
      await createAuditionUpdate(notification);

      if (preUpdate !== "") {
        newPreUpdates = [
          {
            title: preUpdate,
          },
          ...newPreUpdates,
        ];
      }

      this.setState({
        showNewNotificationInput: false,
        newNotification: "",
        updates: [notification, ...updates],
        updateSelected: {},
        preUpdates: newPreUpdates,
      });

      showMessage("success", en.update_was_sent);
    } catch (error) {
      console.log("handlePressSendUpdate=====>", error);

      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }

    this.setState({ loadingButton: false });
  };

  validateSendNotification = () => {
    const {
      showNewNotificationInput,
      updateSelected,
      newNotification,
    } = this.state;
    let errorMessage = "";

    Keyboard.dismiss();

    if (showNewNotificationInput) {
      if (newNotification === "") {
        errorMessage = en.require_new_notification;
      }
    } else {
      if (Object.entries(updateSelected).length === 0) {
        errorMessage = en.select_an_update;
      }
    }

    return errorMessage;
  };

  agreeOutManualCheckIn = () => {
    this.setState({
      dismissOutManualCheckIn: false,
      pinModalMonitor: true,
      pin: "",
    });
    this.props.navigation.goBack(null);
  };

  dismissOutManualCheckIn = () => {
    this.setState({
      dismissOutManualCheckIn: false,
      pinModalMonitor: false,
      pin: "",
    });
  };

  onPressSetPinMonitor = () => {
    const { audition, pin } = this.state;
    const { navigation, dispatch, updatesPin } = this.props;

    if (pin !== "") {
      if (pin === updatesPin) {
        this.setState({
          dismissOutManualCheckIn: false,
          pinModalMonitor: false,
          pin: "",
        });

        dispatch({
          type: DESTROY_MONITOR_UPDATES_PIN,
          payload: "",
        });

        const resetManagerAction = StackActions.reset({
          index: 1,
          key: null,
          actions: [
            NavigationActions.navigate({
              routeName: "Home",
            }),
            NavigationActions.navigate({
              routeName: "ManagerAudition",
              params: { audition_id: audition },
            }),
          ],
        });

        return navigation.dispatch(resetManagerAction);
      }

      return this.setState({
        pin: "",
      });
    }
  };

  handleChangePin = (digit) => {
    const { pin } = this.state;

    this.setState({ pin: `${pin}${digit}` });
  };

  onChangeNewNotification = (newNotification) => {
    this.setState({ newNotification });
  };

  onMoveEnd = async ({ data, from, to }) => {
    console.log(" DATA ", data);
    console.log(" from ", from);
    console.log(" to ", to);
    // return
    // comment replace item of list logic
    // let oldList = this.state.appointments;
    // let newList = [];
    // newList = oldList;
    // let fromObj = newList[from];
    // let toObj = newList[to];
    // newList[to] = fromObj;
    // newList[from] = toObj;
    console.log(
      "====================== ON MOVE END CALL ====================== "
    );
    let { slots, audition_id } = this.state;
    console.log("ON MOVE END slots data=====");
    console.log("ON MOVE END slots data=====>", JSON.stringify(slots));
    console.log("ON MOVE END Appointments==========");
    console.log("ON MOVE END DATA", JSON.stringify(data));
    let finalData = [];
    // for (let index = 0; index < data.length; index++) {
    //   const element = data[index];
    //   if (element.user_id) {
    //     finalData.push({
    //       slot_id: slots[index].id,
    //       user_id: element.user_id
    //     });
    //   }
    // }

    if (
      this.state.appointments[from].name != undefined &&
      this.state.appointments[from].name != "Free"
    ) {
      finalData[finalData.length] = {
        slot_id: slots[to].id,
        user_id: this.state.appointments[from].user_id,
      };
    }

    if (
      this.state.appointments[to].name != undefined &&
      this.state.appointments[to].name != "Free"
    ) {
      finalData[finalData.length] = {
        slot_id: slots[from].id,
        user_id: this.state.appointments[to].user_id,
      };
    }

    console.log(" CHECK NEW DATA ", finalData);
    this.setState({ appointments: data, loading: true });
    try {
      console.log("ON MOVE END FINAL DATA=====>", finalData);
      let test = await reorderAppointments(audition_id, finalData);
      console.log("ON MOVE END TEST=====>", test);
      this.getSlotsByRound();
    } catch (error) {
      this.setState({ loading: false });
      console.log("ON MOVE END ERROR=====>", error);
    }
  };

  // onMoveEnd_Old = async ({ data, from, to }) => {

  //   console.log(" DATA ", data);
  //   console.log(" from ", from);
  //   console.log(" to ", to);
  //   // return
  //   // comment replace item of list logic
  //   // let oldList = this.state.appointments;
  //   // let newList = [];
  //   // newList = oldList;
  //   // let fromObj = newList[from];
  //   // let toObj = newList[to];
  //   // newList[to] = fromObj;
  //   // newList[from] = toObj;
  //   console.log(
  //     "====================== ON MOVE END CALL ====================== "
  //   );
  //   this.setState({ appointments: data });
  //   let { slots, audition_id } = this.state;
  //   console.log("ON MOVE END slots data=====");
  //   console.log("ON MOVE END slots data=====>", JSON.stringify(slots));
  //   console.log("ON MOVE END Appointments==========");
  //   console.log("ON MOVE END DATA", JSON.stringify(data));
  //   let finalData = [];
  //   for (let index = 0; index < data.length; index++) {
  //     const element = data[index];
  //     if (element.user_id) {
  //       finalData.push({
  //         slot_id: slots[index].id,
  //         user_id: element.user_id
  //       });
  //     }
  //   }
  //   try {
  //     console.log("ON MOVE END FINAL DATA=====>", finalData);
  //     let test = await reorderAppointments(audition_id, finalData);
  //     console.log("ON MOVE END TEST=====>", test);
  //   } catch (error) {
  //     console.log("ON MOVE END ERROR=====>", error);
  //   }
  // };

  onSlotsMoveEnd = async ({ data, from, to }) => {
    console.log(
      "====================== ON SLOTS MOVE END CALL ====================== "
    );
    console.log(" DATA ", data);
    console.log(" from ", from);
    console.log(" to ", to);

    let { appointments, audition_id } = this.state;
    console.log("ON SLOTS MOVE END DATA onSlotsMoveEnd", data);
    let finalData = [];

    if (
      this.state.appointments[from].name != undefined &&
      this.state.appointments[from].name != "Free"
    ) {
      finalData[finalData.length] = {
        slot_id: this.state.slots[to].id,
        user_id: this.state.appointments[from].user_id,
      };
    }

    if (
      this.state.appointments[to].name != undefined &&
      this.state.appointments[to].name != "Free"
    ) {
      finalData[finalData.length] = {
        slot_id: this.state.slots[from].id,
        user_id: this.state.appointments[to].user_id,
      };
    }

    console.log(" CHECK NEW DATA ", finalData);
    this.setState({ slots: data });
    // for (let index = 0; index < appointments.length; index++) {
    //   const element = appointments[index];
    //   if (element.user_id) {
    //     finalData.push({
    //       slot_id: data[index].id,
    //       user_id: element.user_id
    //     });
    //   }
    // }
    this.setState({ loading: true });
    try {
      console.log(
        "ON SLOTS MOVE END FINAL DATA onSlotsMoveEnd=====>",
        finalData
      );
      let test = await reorderAppointments(audition_id, finalData);
      console.log("ON SLOTS MOVE END TEST onSlotsMoveEnd=====>", test);
      this.getSlotsByRound();
    } catch (error) {
      this.setState({ loading: false });
      console.log("ON SLOTS MOVE END ERROR=====>", error);
    }
  };
  onPressManualCheckinMode(item) {
    if (item.appointment_time === "N/A") {
      this.getAvailableTimeSlots();
      setTimeout(() => {
        this.setState({ onTimeSlotSelection: true, selectedPerformer: item });
      }, 500);
    } else {
      this.setState({ onManualCheckIn: true, selectedPerformer: item });
    }
  }
  renderPerformerItem = ({ item, index }) => {
    let checkIn = "";
    if (item.is_checked_in) {
      checkIn = en.title_Checked_in;
    }
    return (
      <View
        style={[
          styles.itemContainer,
          { backgroundColor: index % 2 === 0 ? "lightgrey" : "darkgrey" },
        ]}
      >
        <View style={styles.profileContainer}>
          <Image style={styles.image} source={{ uri: item.image }} />
        </View>

        <View style={styles.verticalSeparator} />
        <View style={{ flex: 1.5, marginLeft: resize(7) }}>
          <Text style={styles.nameValue}>{item.name}</Text>
        </View>
        <View style={styles.verticalSeparator} />
        <View style={{ flex: 2, marginLeft: resize(7) }}>
          <Text style={styles.statusValue}>{item.status}</Text>
        </View>
        <View style={styles.verticalSeparator} />
        <View style={{ flex: 1.8, marginLeft: resize(7) }}>
          <Text style={styles.appointmentValue}>
            {item.appointment_time + checkIn}
          </Text>
        </View>
        <View style={styles.verticalSeparator} />
        <View style={styles.actionContainer}>
          {!item.is_checked_in && (
            <TouchableOpacity
              onPress={() => {
                this.onPressManualCheckinMode(item);
              }}
              style={styles.buttonContainer}
            >
              <Text style={styles.buttonText}>Check-In</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };
  dismissManualCheckIn = () => {
    this.setState({ onManualCheckIn: false });
  };
  agreeManualCheckIn = async () => {
    this.setState({ onManualCheckIn: false });
    const { navigation } = this.props;
    const { selectedPerformer, selectedTimeSlotIndex, timeSlots } = this.state;
    if (selectedPerformer !== null) {
      this.setState({ loading: true });
      console.log(JSON.stringify(selectedPerformer));
      try {
        const data = {
          slot: selectedPerformer.slot
            ? selectedPerformer.slot
            : timeSlots.length > 0
            ? timeSlots[selectedTimeSlotIndex]
              ? timeSlots[selectedTimeSlotIndex].id
              : ""
            : "",
          user: selectedPerformer.user,
          appointment_id: selectedPerformer.appointment_id,
        };

        if (selectedPerformer.rol !== null || selectedPerformer.rol !== "") {
          data.rol = selectedPerformer.rol;
        }
        console.log(" CHECK IN DATA PARAMS ", data);
        // return
        await completeCheckin(data);

        showMessage("success", en.checkin_completed);
        this.getAvailableTimeSlots();
        this.getAllPerformerList(true);
        this.setState({ loading: false });

        // return navigation.dispatch(resetCheckinMode);
      } catch (error) {
        console.log(error);

        const parsedError = parseError(error);
        this.setState({ loading: false });

        showMessage("danger", parsedError);
      }

      this.setState({ loadingButton: false });
    }
  };
  renderMessageRow = (rowData, index, highlighted) => {
    return (
      <View style={styles.dropDownItemContainer}>
        <Text style={styles.dropDownItem}>
          {rowData.time ? rowData.time : ""}
        </Text>
      </View>
    );
  };
  onItemClick(option, index) {
    this.setState({ selectedTimeSlotIndex: index });
  }

  renderSlotSelectionModal = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.onTimeSlotSelection}
        onRequestClose={() => {
          this.setState({ onTimeSlotSelection: false });
        }}
      >
        <View style={styles.container}>
          <TouchableOpacity onPress={() => {}} style={styles.dismiss} />
          <View style={styles.containerAlert}>
            <View>
              <Text style={styles.modalTitle}>{en.manual_checkin_mode}</Text>
            </View>
            <ModalDropdown
              options={this.state.timeSlots}
              onSelect={(index, option) => {
                this.onItemClick(option, index);
              }}
              renderRow={this.renderMessageRow}
              renderSeparator={() => {
                <View style={{ backgroundColor: "white" }} />;
              }}
              dropdownStyle={styles.dropDownStyle}
              // scrollEnabled={false}
            >
              <View
                style={{
                  marginBottom: resize(10, "height"),
                  marginTop: resize(10, "height"),
                }}
              >
                <View style={styles.roundContainer}>
                  <Text style={styles.selectedValue}>
                    {this.state.selectedTimeSlotIndex
                      ? this.state.timeSlots[this.state.selectedTimeSlotIndex]
                        ? this.state.timeSlots[this.state.selectedTimeSlotIndex]
                            .time
                        : en.select_available_time_slot
                      : en.select_available_time_slot}
                  </Text>
                  <Image source={picker_downflag} resizeMode="contain" />
                </View>
              </View>
            </ModalDropdown>
            <View style={styles.actionBar}>
              <TouchableOpacity
                style={styles.buttonDefault}
                activeOpacity={1}
                onPress={() => {
                  this.setState({ onTimeSlotSelection: false });
                }}
              >
                <Text style={styles.cancelText}>{en.cancel_button_text}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  if (this.state.selectedTimeSlotIndex === null) {
                    showMessage("danger", "Please select appointment time");
                  } else {
                    if (
                      this.state.selectedTimeSlotIndex &&
                      this.state.timeSlots[this.state.selectedTimeSlotIndex] &&
                      this.state.timeSlots[this.state.selectedTimeSlotIndex]
                        .time
                    ) {
                      this.setState({
                        onTimeSlotSelection: false,
                        // onManualCheckIn: true,
                      });
                      this.agreeManualCheckIn();
                    } else {
                      showMessage("danger", "Please select appointment time");
                    }
                  }
                }}
                activeOpacity={1}
                style={[styles.buttonDefault, styles.buttonMargin]}
              >
                <Text style={styles.accept}>{en.button_Check_in}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };
  render() {
    const {
      appointments,
      updates,
      updateSelected,
      dismissOutManualCheckIn,
      onManualCheckIn,
      pinModalMonitor,
      loading,
      showNewNotificationInput,
      newNotification,
      loadingButton,
      preUpdates,
      pin,
      slots,
    } = this.state;

    // if (loading || Object.entries(slots).length === 0) {
    //   return <Loading />;
    // }

    return (
      <BackgroundWrapper>
        <AlertIOS
          // title={"hello"}
          title={en.close_manual_checkIn_mode_title}
          show={dismissOutManualCheckIn}
          dismiss={this.dismissOutManualCheckIn}
          agree={this.agreeOutManualCheckIn}
        >
          <Text style={styles.monitorModeText}>
            {en.close_manual_checkIn_mode_description}
          </Text>
        </AlertIOS>
        <AlertIOS
          // title={"hello"}
          title={en.close_manual_checkIn_mode_title}
          show={dismissOutManualCheckIn}
          dismiss={this.dismissOutManualCheckIn}
          agree={this.agreeOutManualCheckIn}
        >
          <Text style={styles.monitorModeText}>
            {en.close_manual_checkIn_mode_description}
          </Text>
        </AlertIOS>
        <AlertIOS
          // title={"hello"}
          title={en.manual_checkin_mode}
          show={onManualCheckIn}
          dismiss={this.dismissManualCheckIn}
          agree={this.agreeManualCheckIn}
        >
          <Text style={styles.monitorModeText}>
            {en.manual_check_in_description}
          </Text>
        </AlertIOS>
        {this.renderSlotSelectionModal()}
        {/* <AlertIOS
          title={en.enter_passcode}
          show={pinModalMonitor}
          dismiss={this.dismissOutManualCheckIn}
          cancelTitle={en.cancel_button_text}
          acceptTitle={en.set_button_text}
          agree={this.onPressSetPinMonitor}
        >
          <View style={styles.pinLabelContainer}>
            <Text style={styles.pin}>{pin}</Text>
          </View>
          <PinKeyboard onPressDigit={this.handleChangePin} />
        </AlertIOS> */}
        <LightBar />
        <Header
          center={<HeaderText title={en.checkin_title} />}
          right={<CheckButton onPress={() => this.handlePressCheckButton()} />}
        />
        <View style={styles.wrapper}>
          <View style={styles.title}>
            <Text style={styles.titleText}>{en.manual_checkin_mode}</Text>
          </View>
          <View style={styles.tableContainer}>
            {this.state.performerList.length > 0 ? (
              <View style={styles.tableHeaderContainer}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.pictureHeader}>{en.title_picture}</Text>
                </View>
                <View style={styles.verticalSeparator} />
                <TouchableOpacity
                  style={{
                    flex: 1.5,
                    marginLeft: resize(7),
                    flexDirection: "row",
                  }}
                  onPress={() => {
                    // this.state.isDescName = !this.state.isDescName;
                    this.setState({ isDescName: !this.state.isDescName });
                    setTimeout(() => {
                      this.getAllPerformerList(false);
                    }, 300);
                  }}
                >
                  <Text style={styles.nameHeader}>{en.title_name}</Text>
                  <Image
                    source={picker_downflag}
                    style={
                      this.state.isDescName
                        ? {}
                        : { transform: [{ rotate: "-180deg" }] }
                    }
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <View style={styles.verticalSeparator} />
                <TouchableOpacity
                  style={{
                    flex: 2,
                    marginLeft: resize(7),
                    flexDirection: "row",
                  }}
                  onPress={() => {
                    this.setState({ isDescStatus: !this.state.isDescStatus });
                  }}
                >
                  <Text style={styles.statusHeader}>{en.title_status}</Text>
                  {/* <Image source={picker_downflag} resizeMode="contain" /> */}
                </TouchableOpacity>
                <View style={styles.verticalSeparator} />
                <TouchableOpacity
                  style={{
                    flex: 1.8,
                    marginLeft: resize(7),
                    flexDirection: "row",
                  }}
                  onPress={() => {
                    this.setState({
                      isDescAppointmentTime: !this.state.isDescAppointmentTime,
                    });
                  }}
                >
                  <Text style={styles.appointmentHeader}>
                    {en.title_appointment}
                  </Text>
                  {/* <Image source={picker_downflag} resizeMode="contain" /> */}
                </TouchableOpacity>
                <View style={styles.verticalSeparator} />
                <View style={{ flex: 1.2, marginLeft: resize(7) }}>
                  <Text style={styles.actionsHeader}>{en.title_action}</Text>
                </View>
              </View>
            ) : null}
            {this.state.performerList.length > 0 ? (
              <FlatList
                showsVerticalScrollIndicator={false}
                data={this.state.performerList}
                extraData={this.state}
                bounces={false}
                nestedScrollEnabled={true}
                renderItem={this.renderPerformerItem}
                keyExtractor={(item, index) => item + index}
              />
            ) : (
              <Text style={styles.emptyText}>{en.no_performers}</Text>
            )}
            {this.state.loading && <Loading />}
          </View>
          <View style={styles.logoContainer}>
            <Image resizeMode="contain" source={GigIcon} />
          </View>
        </View>
      </BackgroundWrapper>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
  updatesPin: state.manage.updatesPin,
});

export default connect(mapStateToProps)(ManualCheckInMode);
