import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { StackActions, NavigationActions } from "react-navigation";
import { connect } from "react-redux";

//custom
import styles from "./style";
import BackgroundWrapper from "../../../../components/commons/background_wrapper";
import LightBar from "../../../../components/commons/light_bar";
import Header from "../../../../components/commons/header";
import HeaderText from "../../../../components/commons/header_text";
import CheckButton from "../../../../components/auditions/check_button";
import GradientButton from "../../../../components/commons/gradient_button";
import AlertIOS from "../../../../components/commons/alert_ios";
import PinKeyboard from "../../../../components/commons/pin_keyboard";
import Loading from "../../../../components/commons/loading";
import { gradientLight } from "../../../../assets/styles";
import CheckInButtonOptions from "../../../../components/auditions/checkin_options";

//Images
import GigIcon from "../../../../assets/icons/auditions/gigIcon.png";

//locale
import en from "../../../../locale/en";

// actions
import {
  DESTROY_CHECKIN_MODE_PIN,
  DESTROY_AUDITION_ID,
  SET_AUDITIONS_APPOINTMENTS,
  DESTROY_AUDITIONS_APPOINTMENTS
} from "../../../../actions/manage";

//api
import { getAuditionAppointments } from "../../../../api/auditions";

class CheckinMode extends Component {
  constructor(props) {
    super(props);
    let { navigation } = this.props;
    let title = navigation.getParam("title", "");
    let date = navigation.getParam("date", "");
    let time = navigation.getParam("time", "");
    let audition = navigation.getParam("audition", "");
    this.state = {
      pinModalCheckin: false,
      dismissOutCheckin: false,
      pin: "",
      loading: false,
      showOptions: false,
      title,
      date,
      time,
      audition
    };
  }

  componentDidMount() {
    this.fetchSelectedAppointments();
  }

  fetchSelectedAppointments = async () => {
    const { audition_id, dispatch } = this.props;

    this.setState({ loading: true });

    try {
      const response = await getAuditionAppointments(audition_id);

      dispatch({
        type: SET_AUDITIONS_APPOINTMENTS,
        payload: response.data.data
      });
    } catch (error) {
      console.log(error);
    }
    this.setState({ loading: false });
  };
  validateOptionsCloseOrOpen = () => {
    if (this.state.showOptions) {
      this.setState({ showOptions: false });
    } else {
      this.setState({ showOptions: true });
    }
  };
  handlePressCheckButton = () => {
    this.setState({ dismissOutCheckin: true });
  };

  handlePressButtonQr = () => {
    const { navigation } = this.props;
    // console.log("PROPS WHEN NAVIGATE ScanQr=====>", this.props);
    console.log("PROPS WHEN NAVIGATE ScanQr=====>", navigation);
    navigation.navigate("ScanQr", { value: navigation.state.params });
  };

  handlePressButtonGuest = () => {
    const { navigation } = this.props;

    navigation.navigate("CheckinGuest");
  };

  agreeOutCheckin = () => {
    this.setState({
      pinModalCheckin: true,
      dismissOutCheckin: false,
      pin: ""
    });
  };

  dismissOutCheckin = () => {
    this.setState({
      pinModalCheckin: false,
      dismissOutCheckin: false,
      pin: ""
    });
  };

  onPressSetPinCheckin = () => {
    const { pin } = this.state;
    const { navigation, checkinPin, dispatch, infoAuditon } = this.props;

    if (pin !== "") {
      if (pin === checkinPin) {
        this.setState({
          pinModalCheckin: false,
          dismissOutCheckin: false,
          pin: ""
        });

        dispatch({
          type: DESTROY_CHECKIN_MODE_PIN,
          payload: ""
        });

        const resetManagerAction = StackActions.reset({
          index: 1,
          key: null,
          actions: [
            NavigationActions.navigate({
              routeName: "Home"
            }),
            NavigationActions.navigate({
              routeName: "ManagerAudition",
              params: { audition_id: infoAuditon.audition }
            })
          ]
        });

        dispatch({
          type: DESTROY_AUDITION_ID,
          payload: null
        });
        dispatch({
          type: DESTROY_AUDITIONS_APPOINTMENTS,
          payload: []
        });

        return navigation.dispatch(resetManagerAction);
      }

      return this.setState({
        pin: ""
      });
    }
  };

  handleChangePin = digit => {
    const { pin } = this.state;

    this.setState({ pin: `${pin}${digit}` });
  };

  render() {
    const {
      pinModalCheckin,
      dismissOutCheckin,
      loading,
      pin,
      showOptions,
      title,
      time
    } = this.state;

    let { infoAuditon } = this.props;

    if (loading) {
      return <Loading />;
    }

    return (
      <BackgroundWrapper>
        <LightBar />
        <AlertIOS
          title={en.close_checkin_title}
          show={dismissOutCheckin}
          dismiss={this.dismissOutCheckin}
          agree={this.agreeOutCheckin}
        >
          <Text style={styles.monitorModeText}>
            {en.close_checkin_description}
          </Text>
        </AlertIOS>
        <AlertIOS
          title={en.enter_passcode}
          show={pinModalCheckin}
          dismiss={this.dismissOutCheckin}
          cancelTitle={en.cancel_button_text}
          acceptTitle={en.set_button_text}
          agree={this.onPressSetPinCheckin}
        >
          <View style={styles.pinLabelContainer}>
            <Text style={styles.pin}>{pin}</Text>
          </View>
          <PinKeyboard onPressDigit={this.handleChangePin} />
        </AlertIOS>
        <Header
          center={<HeaderText title={en.checkin_title} />}
          right={
            <CheckButton onPress={() => this.validateOptionsCloseOrOpen()} />
          }
        />
        {showOptions && (
          <CheckInButtonOptions
            backBtn
            OnPressExit={() => this.handlePressCheckButton()}
          />
        )}
        <View style={styles.wrapper}>
          <View style={styles.title}>
            <Text style={styles.titleText}>{`${infoAuditon.title} | ${
              infoAuditon.time
            }`}</Text>
          </View>
          <View style={styles.buttonsContainer}>
            <GradientButton
              buttonText={en.have_appointment_title}
              containerStyle={styles.buttonContainer}
              gradientStyle={styles.buttonQr}
              colors={gradientLight}
              textStyle={styles.buttonQrText}
              onPress={() => this.handlePressButtonQr()}
            />
            <GradientButton
              buttonText={en.walk_title}
              containerStyle={styles.buttonContainer}
              gradientStyle={styles.buttonGuest}
              textStyle={styles.buttonGuestText}
              onPress={() => this.handlePressButtonGuest()}
            />
          </View>
          <View style={styles.logoContainer}>
            <Image resizeMode="contain" source={GigIcon} />
          </View>
        </View>
      </BackgroundWrapper>
    );
  }
}

const mapStateToProps = state => ({
  checkinPin: state.manage.checkinPin,
  audition_id: state.manage.audition_id,
  infoAuditon: state.checkin.infoAuditon
});

export default connect(mapStateToProps)(CheckinMode);
