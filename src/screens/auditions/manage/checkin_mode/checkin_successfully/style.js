import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXABOLD, width, height, BREAK_REGULAR } from '../../../../../assets/styles';

const styles = StyleSheet.create({
	wrapper: {
		width,
		height,
		flexDirection: 'row'
	},
	contentLeft: {
		width: resize(741),
		height: '100%',
	},
	partakerContainer: {
		width: '100%',
		height: resize(668, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
	},
	successTitleContainer: {
		marginBottom: resize(25, 'height')
	},
	successTitle: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(19),
		fontWeight: '400',
	},
	partakerInfoContainer: {
		width: resize(337),
		marginBottom: resize(79, 'height'),
		borderRadius: 4,
		backgroundColor: '#ffffff',
		justifyContent: 'center',
		alignItems: 'center',
		...Platform.select({
			ios: {
				height: resize(250, 'height'),
			},
			android: {
				height: resize(290, 'height'),
			}
		})
	},
	imageContainer: {
		width: resize(116),
		height: resize(116, 'height'),
		marginBottom: resize(19, 'height'),
	},
	image: {
		resizeMode: 'contain',
		width: '100%',
		height: '100%',
		borderRadius: 4,
	},
	partakerName: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(36),
		fontWeight: '400',
		paddingBottom: resize(10, 'height'),
		textAlign: 'center'
	},
	partakerAppointment: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
	},
	buttonSubmitContainer: {
		width: resize(337),
		height: resize(56, 'height'),
	},
	gradientSubmit: {
		width: '100%',
		height: '100%',
		borderRadius: 28,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonSubmitText: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
	},
	logoContainer: {
		marginBottom: resize(30, 'height'),
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'flex-start',
		marginLeft: resize(34),
	},
	title: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(32),
		...Platform.select({
			ios: {
				fontWeight: '600'
			},
			android: {
				fontWeight: '400'
			}
		}),
		paddingBottom: resize(5, 'height'),
	},
	titleNumber: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(36),
		paddingTop: resize(15, 'height'),
		paddingBottom: resize(5, 'height'),
		...Platform.select({
			ios: {
				fontWeight: '700'
			},
			android: {
				fontWeight: '400'
			}
		})
	},
	contentRight: {
		width: resize(371),
		height: '100%',
		backgroundColor: '#fff',
	},
	appointmentsTitleContainer: {
		flexDirection: 'row',
		paddingTop: resize(45, 'height'),
		paddingBottom: resize(20, 'height'),
		paddingHorizontal: resize(95),
		justifyContent: 'center',
		alignItems: 'center',
	},
	appointmentsTitle: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(23),
		fontWeight: '400',
		textAlign: 'center'
	},
	appointmentsContainer: {
		paddingTop: resize(38, 'height')
	},
	appointmentsContent: {
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		marginBottom: resize(175, 'height'),
		paddingHorizontal: resize(50),
	},
});

export default styles;