import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Content } from "native-base";
import { connect } from "react-redux";

//custom
import styles from "./style";
import BackgroundWrapper from "../../../../../components/commons/background_wrapper";
import LightBar from "../../../../../components/commons/light_bar";
import Header from "../../../../../components/commons/header";
import BackButton from "../../../../../components/commons/back_button";
import HeaderText from "../../../../../components/commons/header_text";
import CheckButton from "../../../../../components/auditions/check_button";
import Logo from "../../../../../components/commons/logo";
import AppointmentsItem from "../../../../../components/auditions/appointment_item";
import GradientButton from "../../../../../components/commons/gradient_button";
import showMessage from "../../../../../utils/show_message";
import parseError from "../../../../../utils/parse_error";

//locale
import en from "../../../../../locale/en";

//reset actions
import { resetCheckinMode } from "../../../../../utils/reset_actions/manager_checkin_mode";

//api
import { completeCheckin } from "../../../../../api/auditions";

class CheckinSuccessfully extends Component {
	state = {
		checkinData: this.props.navigation.getParam("checkinData", {}),
		loadingButton: false
	};

	handlePressBackButton = () => {
		const { navigation } = this.props;

		navigation.goBack();
	};

	handlePressCheckButton = () => {
		const { navigation } = this.props;

		navigation.dispatch(resetCheckinMode);
	};

	handlePressDone = async () => {
		const { navigation } = this.props;
		const { checkinData } = this.state;

		this.setState({ loadingButton: true });
		console.log(checkinData);
		try {
			const data = {
				slot: checkinData.appointment,
				user: checkinData.id,
				appointment_id: checkinData.appointment_id
			};

			if (checkinData.role !== null || checkinData.role !== "") {
				data.rol = checkinData.role;
			}
			console.log(" CHECK IN DATA PARAMS ", data)
			// return
			await completeCheckin(data);

			showMessage("success", en.checkin_completed);

			return navigation.dispatch(resetCheckinMode);
		} catch (error) {
			console.log(error);

			const parsedError = parseError(error);

			showMessage("danger", parsedError);
		}

		this.setState({ loadingButton: false });
	};

	render() {
		const { checkinData, loadingButton } = this.state;
		const { appointments } = this.props;

		return (
			<BackgroundWrapper>
				<LightBar />
				<Header
					left={<BackButton onPress={() => this.handlePressBackButton()} />}
					center={<HeaderText title={en.checkin_title} />}
					right={<CheckButton onPress={() => this.handlePressCheckButton()} />}
				/>
				<View style={styles.wrapper}>
					<View style={styles.contentLeft}>
						<View style={styles.partakerContainer}>
							<View style={styles.successTitleContainer}>
								<Text style={styles.successTitle}>
									{en.checkin_succefully_title}
								</Text>
							</View>
							<View style={styles.partakerInfoContainer}>
								<View style={styles.imageContainer}>
									{checkinData.hasOwnProperty("image") && (
										<Image
											source={{ uri: checkinData.image }}
											style={styles.image}
										/>
									)}
								</View>
								<Text style={styles.partakerName}>{checkinData.name}</Text>
								<Text style={styles.partakerAppointment}>
									{checkinData.time}
								</Text>
							</View>
							<GradientButton
								buttonText={en.done_button_text}
								containerStyle={styles.buttonSubmitContainer}
								gradientStyle={styles.gradientSubmit}
								textStyle={styles.buttonSubmitText}
								onPress={() => this.handlePressDone()}
								loading={loadingButton}
							/>
						</View>
						<View style={styles.logoContainer}>
							<Logo
								titleStyle={styles.title}
								titleNumberStyle={styles.titleNumber}
							/>
						</View>
					</View>

					<View style={styles.contentRight}>
						<View style={styles.appointmentsTitleContainer}>
							<Text style={styles.appointmentsTitle}>
								{en.audition_appointments_title}
							</Text>
						</View>
						<Content style={styles.appointmentsContainer}>
							<View style={styles.appointmentsContent}>
								{appointments.map((appointment, index) => {
									return <AppointmentsItem key={index} {...appointment} />;
								})}
							</View>
						</Content>
					</View>

				</View>
			</BackgroundWrapper>
		);
	}
}

const mapStateToProps = state => ({
	appointments: state.manage.appointments
});

export default connect(mapStateToProps)(CheckinSuccessfully);
