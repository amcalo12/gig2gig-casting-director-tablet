import React, { Component } from 'react';
import { View, Text, Keyboard } from 'react-native';
import { Content } from 'native-base';
import { connect } from 'react-redux';

//custom
import styles from './style';
import BackgroundWrapper from '../../../../../components/commons/background_wrapper';
import LightBar from '../../../../../components/commons/light_bar';
import Header from '../../../../../components/commons/header';
import BackButton from '../../../../../components/commons/back_button';
import HeaderText from '../../../../../components/commons/header_text';
import CheckButton from '../../../../../components/auditions/check_button';
import Logo from '../../../../../components/commons/logo';
import AppointmentsItem from '../../../../../components/auditions/appointment_item';
import BorderedSelect from '../../../../../components/auditions/bordered_select';
import GradientButton from '../../../../../components/commons/gradient_button';
import Loading from '../../../../../components/commons/loading';
import showMessage from '../../../../../utils/show_message';

//locale
import en from '../../../../../locale/en';

//reset actions
import { resetCheckinMode } from '../../../../../utils/reset_actions/manager_checkin_mode';

//api
import {
	getNoWalkAppointments,
	getWalkAppointments,
} from '../../../../../api/auditions';

class ChooseAppointment extends Component {
	state = {
		appointment: '',
		loading: false,
		loadingButton: false,
		noWalkAppointments: [],
		id: this.props.navigation.getParam('id', ''),
		image: this.props.navigation.getParam('image', ''),
		name: this.props.navigation.getParam('name', ''),
		role: this.props.navigation.getParam('role', ''),
		appointment_id: this.props.navigation.getParam('appointment_id', ''),
	};

	componentDidMount() {
		console.log(this.state);
		this.fetchNoWalkAppointments();
	}

	fetchNoWalkAppointments = async () => {
		const { audition_id } = this.props;

		this.setState({ loading: true });

		try {
			const response = await getWalkAppointments(audition_id);
			const slots = response.data.data.slots;
			let noWalkAppointments = [];

			slots.map(slot => {
				return noWalkAppointments.push({
					label: slot.time,
					value: slot.id,
				});
			});

			this.setState({ noWalkAppointments });
		} catch (error) {
			console.log(error);
		}

		this.setState({ loading: false });
	};

	handlePressBackButton = () => {
		const { navigation } = this.props;

		navigation.goBack();
	};

	handlePressCheckButton = () => {
		const { navigation } = this.props;

		navigation.dispatch(resetCheckinMode);
	};

	onChangeAppointment = appointment => {
		this.setState({ appointment });
	};

	handlePressSubmit = async () => {
		const { navigation, audition_id } = this.props;
		const {
			appointment,
			noWalkAppointments,
			id,
			image,
			name,
			role,
			appointment_id,
		} = this.state;
		const hasError = this.validateFields();

		if (hasError !== '') {
			return showMessage('danger', hasError);
		}

		this.setState({ loadingButton: true });

		try {
			let time = '';

			noWalkAppointments.map(item => {
				if (item.value === appointment) {
					return (time = item.label);
				}
			});

			const checkinData = {
				appointment,
				time,
				audition_id,
				id,
				name,
				image,
				role,
				appointment_id,
			};

			this.setState({ loadingButton: false });

			return navigation.navigate('CheckinSuccessfully', {
				checkinData,
			});
		} catch (error) {
			console.log(error);
		}

		this.setState({ loadingButton: false });
	};

	validateFields = () => {
		const { appointment } = this.state;
		let errorMessage = '';

		Keyboard.dismiss();

		if (appointment === null || appointment === '') {
			errorMessage = en.select_appointment;
		}

		return errorMessage;
	};

	render() {
		const {
			appointment,
			loading,
			noWalkAppointments,
			loadingButton,
		} = this.state;
		const { appointments } = this.props;

		if (loading) {
			return <Loading />;
		}

		return (
			<BackgroundWrapper>
				<LightBar />
				<Header
					left={<BackButton onPress={() => this.handlePressBackButton()} />}
					center={<HeaderText title={en.checkin_title} />}
					right={<CheckButton onPress={() => this.handlePressCheckButton()} />}
				/>
				<View style={styles.wrapper}>
					<View style={styles.contentLeft}>
						<View style={styles.chooseContainer}>
							<BorderedSelect
								containerStyle={styles.appointmentsInput}
								placeholder={en.select_appointment_placeholder}
								onChange={this.onChangeAppointment}
								value={appointment}
								data={noWalkAppointments}
							/>
							<GradientButton
								buttonText={en.submit_text}
								containerStyle={styles.buttonSubmitContainer}
								gradientStyle={styles.gradientSubmit}
								textStyle={styles.buttonSubmitText}
								onPress={() => this.handlePressSubmit()}
								loading={loadingButton}
							/>
						</View>
						<View style={styles.logoContainer}>
							<Logo
								titleStyle={styles.title}
								titleNumberStyle={styles.titleNumber}
							/>
						</View>
					</View>
					<View style={styles.contentRight}>
						<View style={styles.appointmentsTitleContainer}>
							<Text style={styles.appointmentsTitle}>
								{en.audition_appointments_title}
							</Text>
						</View>
						<Content style={styles.appointmentsContainer}>
							<View style={styles.appointmentsContent}>
								{appointments.map((appointment, index) => {
									return <AppointmentsItem key={index} {...appointment} />;
								})}
							</View>
						</Content>
					</View>
				</View>
			</BackgroundWrapper>
		);
	}
}

const mapStateToProps = state => ({
	audition_id: state.manage.audition_id,
	appointments: state.manage.appointments,
});

export default connect(mapStateToProps)(ChooseAppointment);
