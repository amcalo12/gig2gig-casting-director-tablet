import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, BREAK_REGULAR, NEXABOLD, NEXALIGHT } from '../../../../assets/styles';

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    height: '100%',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  buttonsContainer: {
    width: '100%',
    height: resize(230, 'height'),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: resize(139),
  },
  buttonContainer: {
    width: resize(377),
    height: resize(104, 'height'),
  },
  buttonQr: {
    width: '100%',
    height: '100%',
    borderRadius: 52,
    borderColor: '#4d2545',
    borderStyle: 'solid',
    borderWidth: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonQrText: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(24),
    fontWeight: '400',
  },
  buttonGuest: {
    width: '100%',
    height: '100%',
    borderRadius: 52,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonGuestText: {
    color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: resize(24),
    fontWeight: '400',
  },
  logoContainer: {
    marginBottom: resize(30, 'height'),
    width: '100%',
    height: '20%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    marginLeft: resize(34)
  },
  title: {
    color: '#ffffff',
    fontFamily: BREAK_REGULAR,
    fontSize: resize(32),
    ...Platform.select({
      ios: {
        fontWeight: '600'
      },
      android: {
        fontWeight: '400'
      }
    }),
    paddingBottom: resize(5, 'height'),
  },
  titleNumber: {
    color: '#ffffff',
    fontFamily: BREAK_REGULAR,
    fontSize: resize(36),
    paddingTop: resize(15, 'height'),
    paddingBottom: resize(5, 'height'),
    ...Platform.select({
      ios: {
        fontWeight: '700'
      },
      android: {
        fontWeight: '400'
      }
    })
  },
  monitorModeText: {
    paddingHorizontal: resize(50),
    marginTop: resize(10, 'height'),
    color: '#000000',
    fontSize: resize(13),
    fontWeight: '400',
    lineHeight: resize(18),
    textAlign: 'center',
    ...Platform.select({
      android: {
        paddingBottom: resize(40, 'height')
      }
    })
  },
  pinLabelContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: resize(15, 'height')
  },
  pin: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(18),
    fontWeight: '400',
  },
  title: {
    marginBottom: resize(20, 'h'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    color: 'white',
    fontSize: resize(28),
    fontFamily: NEXABOLD,
    fontWeight: '400'
  }
});

export default styles;