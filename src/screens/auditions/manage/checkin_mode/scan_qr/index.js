import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Content } from "native-base";
import QRCodeScanner from "react-native-qrcode-scanner";
import { connect } from "react-redux";

//custom
import styles from "./style";
import BackgroundWrapper from "../../../../../components/commons/background_wrapper";
import LightBar from "../../../../../components/commons/light_bar";
import Header from "../../../../../components/commons/header";
import BackButton from "../../../../../components/commons/back_button";
import HeaderText from "../../../../../components/commons/header_text";
import CheckButton from "../../../../../components/auditions/check_button";
import AppointmentsItem from "../../../../../components/auditions/appointment_item";
import showMessage from "../../../../../utils/show_message";
import CheckInButtonOptions from "../../../../../components/auditions/checkin_options";
import moment from "moment";

//locale
import en from "../../../../../locale/en";

//reset actions
import { resetCheckinMode } from "../../../../../utils/reset_actions/manager_checkin_mode";

//api
import { getCheckinData } from "../../../../../api/auditions";

//Images
import GigIcon from "../../../../../assets/icons/auditions/gigIcon.png";
import { NEXABOLD, resize } from "../../../../../assets/styles";
const currentTime = moment(new Date(), "HH::mm A").format("HH:mm");
class ScanQr extends Component {
  constructor(props) {
    super(props);
    console.log("PROPS ==========>", props);
    this.state = {
      pinModalCheckin: false,
      dismissOutCheckin: false,
      pin: "",
      loading: false,
      showOptions: false,
      // switchFrontAndBackCamera: false, //false is back, true is front
      // cameraType: 'back',
      switchFrontAndBackCamera: true, //false is back, true is front
      cameraType: "front",
      appoint: [],
      title: "",
      date: "",
      time: currentTime
    };
  }

  componentDidMount = () => {
    this.sortAppointments();
    // let { checkin } = this.props;

    // if (
    //   navigation.state.params != null &&
    //   navigation.state.params != undefined &&
    //   navigation.state.params.value != null &&
    //   navigation.state.params.value != undefined
    // ) {
    //   let title = navigation.state.params.value.title;
    //   let date = navigation.state.params.value.date;
    //   this.setState({ title: title, date: date });
    // } else {
    //   this.setState({ title: "", date: "" });
    // }

    // const { navigation } = this.props;
    // console.log("TITLE =====>", this.state.title);
    // console.log("this.props =====>", this.props);
    //this.setState({ title: this.props.navigation.state.params.title });
    //this.setState({ date: this.props.navigation.state.params.date });
    //this.setState({ time: this.props.navigation.state.params.time });
  };

  componentDidUpdate = prevProps => {
    let { appointments } = this.props;
    if (prevProps.appointments !== appointments) {
      this.sortAppointments();
    }
  };

  sortAppointments = () => {
    let { appointments } = this.props;
    let appoint = appointments.sort((a, b) => a.slot_id - b.slot_id);
    this.setState({ appoint });
  };

  validateOptionsCloseOrOpen = () => {
    if (this.state.showOptions) {
      this.setState({ showOptions: false });
    } else {
      this.setState({ showOptions: true });
    }
  };

  switchBetweenFrontAndBackCamera = () => {
    if (this.state.switchFrontAndBackCamera) {
      this.setState({ switchFrontAndBackCamera: false, cameraType: "back" });
    } else {
      this.setState({ switchFrontAndBackCamera: true, cameraType: "front" });
    }
  };

  handlePressCheckButton = () => {
    this.setState({ dismissOutCheckin: true });
  };

  agreeOutCheckin = () => {
    this.setState({
      pinModalCheckin: true,
      dismissOutCheckin: false,
      pin: ""
    });
  };

  dismissOutCheckin = () => {
    this.setState({
      pinModalCheckin: false,
      dismissOutCheckin: false,
      pin: ""
    });
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;

    navigation.goBack();
  };

  handlePressCheckButton = () => {
    const { navigation } = this.props;

    navigation.dispatch(resetCheckinMode);
  };

  onSuccessScan = async e => {
    const { navigation, audition_id } = this.props;
    const data = JSON.parse(e.data);
    console.log("qr", data);
    console.log("audition_id", audition_id);
    if (data.appointmentId !== audition_id) {
      return showMessage("danger", en.invalid_qr_data);
    }

    try {
      const response = await getCheckinData(
        data.userId,
        data.rolId,
        audition_id
      );
      console.log({ response });
      const hasAppointment = this.props.appointments.filter(
        appointment => appointment.user_id === data.userId
      );
      if ("slot_id" in response.data.data) {
        const checkinData = {
          appointment: response.data.data.slot_id,
          time: response.data.data.hour,
          appointment_id: data.appointmentId,
          id: response.data.data.id,
          name: response.data.data.name,
          image: response.data.data.image,
          role: data.rolId
        };

        return navigation.navigate("CheckinSuccessfully", {
          checkinData
        });
      }
      navigation.navigate("ChooseAppointment", {
        ...response.data.data,
        role: data.rolId,
        appointment_id: data.appointmentId
      });
      // navigation.navigate("ChooseAppointment", {
      //   ...response.data.data,
      //   role: data.rolId
      // });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { showOptions, cameraType, appoint } = this.state;

    const { appointments } = this.props;

    // console.log("date =====>", this.state.date);
    // console.log("time =====>", this.state.time);
    return (
      <BackgroundWrapper>
        <LightBar />
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
          center={<HeaderText title={en.checkin_title} />}
          right={
            <CheckButton onPress={() => this.validateOptionsCloseOrOpen()} />
          }
        />
        {showOptions && (
          <CheckInButtonOptions
            OnPressFlipCamera={() => this.switchBetweenFrontAndBackCamera()}
            flipCamera
            OnPressExit={() => this.handlePressBackButton()}
          />
        )}

        <View style={styles.wrapper}>
          <View style={styles.contentLeft}>
            <View
              style={{
                justifyContent: "center",
                marginTop: 50,

                alignItems: "center"
              }}
            >
              <View
                style={{
                  // alignItems: "center",
                  flexDirection: "row",
                  paddingHorizontal: 15
                }}
              >
                {/* <View style={{ padding: 20, flexDirection: "row" }}> */}
                {/* <Text
                  style={{
                    color: "#ffffff",
                    fontFamily: NEXABOLD,
                    fontSize: resize(24),
                    fontWeight: "400",
                    alignSelf: "center",
                    backgroundColor: "red"
                  }}
                  numberOfLines={1}
                >
                  {"Audition Title"}
                </Text> */}
                <View
                  style={{
                    borderRightColor: "white",
                    borderRightWidth: 2.0,
                    paddingHorizontal: 5
                  }}
                >
                  <Text
                    style={{
                      color: "#ffffff",
                      fontFamily: NEXABOLD,
                      fontSize: resize(24),
                      fontWeight: "400",
                      alignSelf: "flex-start"
                    }}
                  >
                    {this.props.checkin.title}
                    {/* {this.state.title} */}
                    {/* {this.state.title ? this.state.title : null} */}
                  </Text>
                </View>
                <View
                  style={{
                    borderRightColor: "white",
                    borderRightWidth: 2.0,
                    paddingHorizontal: 5
                  }}
                >
                  <Text
                    style={{
                      color: "#ffffff",
                      fontFamily: NEXABOLD,
                      fontSize: resize(24),
                      fontWeight: "400",
                      alignSelf: "flex-start"
                    }}
                  >
                    {this.props.checkin.date}
                    {/* {this.state.date} */}
                  </Text>
                </View>
                <View
                  style={{
                    borderRightColor: "white",
                    // borderRightWidth: 2.0,
                    paddingHorizontal: 5
                  }}
                >
                  <Text
                    style={{
                      color: "#ffffff",
                      fontFamily: NEXABOLD,
                      fontSize: resize(24),
                      fontWeight: "400",
                      alignSelf: "flex-start"
                    }}
                  >
                    {this.state.time}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.qrContainer}>
              <View style={styles.qrTitleContainer}>
                <Text style={styles.qrTitle}>{en.scan_your_qr_title}</Text>
              </View>
              <View style={styles.scanQrContainer}>
                <QRCodeScanner
                  cameraType={cameraType}
                  onRead={e => this.onSuccessScan(e)}
                  cameraStyle={styles.scanQrContainer}
                  vibrate
                  reactivate
                  reactivateTimeout={3000}
                />
              </View>
            </View>
            <View style={styles.logoContainer}>
              <Image resizeMode="contain" source={GigIcon} />
            </View>
          </View>

          <View style={styles.contentRight}>
            <View style={styles.appointmentsTitleContainer}>
              <Text style={styles.appointmentsTitle}>
                {en.audition_appointments_title}
              </Text>
            </View>
            <Content style={styles.appointmentsContainer}>
              <View style={styles.appointmentsContent}>
                {appoint.map((appointment, index) => {
                  return <AppointmentsItem key={index} {...appointment} />;
                })}
              </View>
            </Content>
          </View>
        </View>
      </BackgroundWrapper>
    );
  }
}

const mapStateToProps = state => {
  console.log(" SCAN QR STATE ", state);
  return {
    audition_id: state.manage.audition_id,
    appointments: state.manage.appointments,
    checkin: state.checkin.infoAuditon
  };
};

export default connect(mapStateToProps)(ScanQr);
