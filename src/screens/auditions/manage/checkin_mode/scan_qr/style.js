import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXABOLD, width, height, BREAK_REGULAR } from '../../../../../assets/styles';

const styles = StyleSheet.create({
	wrapper: {
		width,
		height,
		flexDirection: 'row'
	},
	contentLeft: {
		width: resize(741),
		height: '100%',
	},
	qrContainer: {
		width: '100%',
		height: resize(668, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
	},
	qrTitleContainer: {
		marginBottom: resize(30, 'height')
	},
	qrTitle: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(24),
		fontWeight: '400',
	},
	scanQrContainer: {
		width: resize(358),
		height: resize(349, 'height'),
		backgroundColor: '#000'
	},
	logoContainer: {
		marginBottom: resize(30, 'height'),
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'flex-start',
		marginLeft: resize(34),
	},
	title: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(32),
		...Platform.select({
			ios: {
				fontWeight: '600'
			},
			android: {
				fontWeight: '400'
			}
		}),
		paddingBottom: resize(5, 'height'),
	},
	titleNumber: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(36),
		paddingTop: resize(15, 'height'),
		paddingBottom: resize(5, 'height'),
		...Platform.select({
			ios: {
				fontWeight: '700'
			},
			android: {
				fontWeight: '400'
			}
		})
	},
	contentRight: {
		width: resize(371),
		height: '100%',
		backgroundColor: '#fff',
	},
	appointmentsTitleContainer: {
		flexDirection: 'row',
		paddingTop: resize(45, 'height'),
		paddingBottom: resize(20, 'height'),
		paddingHorizontal: resize(95),
		justifyContent: 'center',
		alignItems: 'center',
	},
	appointmentsTitle: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(24),
		fontWeight: '400',
		textAlign: 'center'
	},
	appointmentsContainer: {
		paddingTop: resize(38, 'height')
	},
	appointmentsContent: {
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		marginBottom: resize(175, 'height'),
		paddingHorizontal: resize(50),
	},
});

export default styles;