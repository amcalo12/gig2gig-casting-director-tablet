import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXABOLD, width, height, BREAK_REGULAR } from '../../../../../assets/styles';

const styles = StyleSheet.create({
	wrapper: {
		width,
		height,
		flexDirection: 'row'
	},
	contentLeft: {
		width: resize(741),
		height: '100%',
		paddingVertical: resize(52.5, 'height'),
	},
	chooseContainer: {
		width: '100%',
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	titleGuest: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(24),
		fontWeight: '400',
		paddingBottom: resize(33, 'height'),
	},
	titleScan: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(24),
		fontWeight: '400',
	},
	inputContainer: {
		width: resize(337),
		height: resize(56, 'height'),
		marginBottom: resize(18, 'height'),
	},
	buttonSubmitContainer: {
		width: resize(337),
		height: resize(56, 'height'),
	},
	gradientSubmit: {
		width: '100%',
		height: '100%',
		borderRadius: 28,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonSubmitText: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
	},
	contentRight: {
		width: resize(371),
		height: '100%',
		backgroundColor: '#fff',
	},
	appointmentsTitleContainer: {
		flexDirection: 'row',
		paddingTop: resize(45, 'height'),
		paddingBottom: resize(10, 'height'),
		paddingHorizontal: resize(90),
		justifyContent: 'center',
		alignItems: 'center',
	},
	appointmentsTitle: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(24),
		fontWeight: '400',
		textAlign: 'center'
	},
	appointmentsContainer: {
		paddingTop: resize(38, 'height')
	},
	appointmentsContent: {
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		marginBottom: resize(175, 'height'),
		paddingHorizontal: resize(15),
	},
	title: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(32),
		...Platform.select({
			ios: {
				fontWeight: '600'
			},
			android: {
				fontWeight: '400'
			}
		}),
		paddingBottom: resize(5, 'height'),
	},
	titleNumber: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(36),
		paddingTop: resize(15, 'height'),
		paddingBottom: resize(5, 'height'),
		...Platform.select({
			ios: {
				fontWeight: '700'
			},
			android: {
				fontWeight: '400'
			}
		})
	},
	qrContainer: {
		marginTop: resize(31, 'height'),
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	imageQrContainer: {
		width: resize(87),
		height: resize(88, 'height'),
		marginTop: resize(16, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row',
	},
	qrImage: {
		width: '100%',
		height: '100%',
		resizeMode: 'contain'
	},
	tabsContainer: {
		height: resize(58, 'height'),
		backgroundColor: 'transparent',
		borderBottomWidth: 0,
		elevation: 0
	},
	tabBarUnderlineStyle: {
		backgroundColor: 'white',
		width: '25%',
		marginLeft: resize(741 / 8)
	},
	tabStyle: {
		backgroundColor: 'transparent',
	},
	transparentContainer: {
		backgroundColor: 'transparent',
	},
	activeTabStyle: {
		backgroundColor: 'transparent',
		width: '100%'
	},
	tabTextStyle: {
		color: 'white',
		fontFamily: NEXABOLD,
		fontSize: resize(17)
	},
	center: {
		width: '100%',
		height: '80%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	textInfo: {
		marginTop: resize(34.6, 'height'),
		color: 'white',
		fontFamily: NEXABOLD,
		fontSize: resize(19),
		textAlign: 'center'
	},
	scanQrContainer: {
		width: resize(358),
		height: resize(349, 'height'),
		backgroundColor: '#000'
	},
	qrTitle: {
		marginBottom: resize(32, 'height'),
		color: 'white',
		fontFamily: NEXABOLD,
		fontSize: resize(24),
		textAlign: 'center'
	},
	logoContainer: {
		position: 'absolute',
		zIndex: 1,
		top: '94%',
		marginLeft: resize(10),
		justifyContent: 'flex-end',
		height: '5%',
	}
});

export default styles;