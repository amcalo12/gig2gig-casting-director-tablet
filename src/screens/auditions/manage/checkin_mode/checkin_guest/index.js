import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import moment from "moment";
import { Tab, Tabs } from "native-base";
import QRCode from "react-native-qrcode-svg";
import QRCodeScanner from "react-native-qrcode-scanner";

//custom
import styles from "./style";
import BackgroundWrapper from "../../../../../components/commons/background_wrapper";
import LightBar from "../../../../../components/commons/light_bar";
import Header from "../../../../../components/commons/header";
import BackButton from "../../../../../components/commons/back_button";
import HeaderText from "../../../../../components/commons/header_text";
import CheckButton from "../../../../../components/auditions/check_button";
import AppointmentsItem from "../../../../../components/auditions/appointment_item";
import Loading from "../../../../../components/commons/loading";

//locale
import en from "../../../../../locale/en";

//api
import {
  getWalkAppointments,
  getCheckinData
} from "../../../../../api/auditions";
import { resize } from "../../../../../assets/styles";
import CheckInButtonOptions from "../../../../../components/auditions/checkin_options";
import showMessage from "../../../../../utils/show_message";

//Images
import GigIcon from "../../../../../assets/icons/auditions/gigIcon.png";
import { Assets } from "../../../../../assets";

class CheckinGuest extends Component {
  state = {
    walkAppointments: [],
    loading: false,
    currentTab: 0,
    // cameraType: 'back',
    cameraType: "front",
    switchFrontAndBackCamera: true
  };

  componentDidMount() {
    this.fetchWalkAppointments();
  }

  fetchWalkAppointments = async () => {
    const { audition_id } = this.props;

    this.setState({ loading: true });

    try {
      const response = await getWalkAppointments(audition_id);
      const slots = response.data.data.slots;
      let walkAppointments = [];

      slots.map(slot => {
        return walkAppointments.push({
          label: slot.time,
          value: slot.id
        });
      });

      this.setState({ walkAppointments });
    } catch (error) {
      console.log(error);
    }

    this.setState({ loading: false });
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;

    navigation.goBack();
  };

  switchBetweenFrontAndBackCamera = () => {
    if (this.state.switchFrontAndBackCamera) {
      this.setState({ switchFrontAndBackCamera: false, cameraType: "back" });
    } else {
      this.setState({ switchFrontAndBackCamera: true, cameraType: "front" });
    }
  };

  sortAppointments = () => {
    const { appointments } = this.props;
    const appointmentsSort = appointments.sort((a, b) =>
      a.time < b.time ? -1 : 1
    );
    appointments.map((o, i) => {
      console.log(moment(o.time, "hh:mm a").format("YYYYMMDDHHmm"));
    });
    const sortedArray = _.orderBy(
      appointments,
      o => {
        console.log(moment(o.time, "hh:mm a").format("YYYYMMDDHHmm"));
        return moment(o.time, "hh:mm a").format("YYYYMMDDHHmm");
      },
      ["asc"]
    );
    console.log({
      appointmentsSort,
      sortedArray
    });
    return sortedArray;
  };

  handleChangeTab = currentTab => {
    this.setState({ currentTab });
  };

  validateOptionsCloseOrOpen = () => {
    if (this.state.showOptions) {
      this.setState({ showOptions: false });
    } else {
      this.setState({ showOptions: true });
    }
  };

  onSuccessScan = async e => {
    const { navigation, audition_id } = this.props;
    const data = JSON.parse(e.data);
    if (data.appointmentId !== audition_id) {
      return showMessage("danger", en.invalid_qr_data);
    }
    try {
      const response = await getCheckinData(
        data.userId,
        data.rolId,
        audition_id
      );
      const hasAppointment = this.props.appointments.filter(
        appointment => appointment.user_id === data.userId
      );
      if ("slot_id" in response.data.data) {
        const checkinData = {
          appointment: response.data.data.slot_id,
          time: response.data.data.hour,
          appointment_id: data.appointmentId,
          id: response.data.data.id,
          name: response.data.data.name,
          image: response.data.data.image,
          role: data.rolId
        };

        return navigation.navigate("CheckinSuccessfully", {
          checkinData
        });
      }
      navigation.navigate("ChooseAppointment", {
        ...response.data.data,
        role: data.rolId,
        appointment_id: data.appointmentId
      });
      // navigation.navigate("ChooseAppointment", {
      //   ...response.data.data,
      //   role: data.rolId
      // });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { loading, currentTab, cameraType, showOptions } = this.state;
    const { appointments } = this.props;
    if (loading) {
      return <Loading />;
    }
    return (
      <BackgroundWrapper>
        <LightBar />
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
          center={<HeaderText title={en.checkin_title} />}
          // right={
          //   <CheckButton onPress={() => this.validateOptionsCloseOrOpen()} />
          // }
        />
        {showOptions && (
          <CheckInButtonOptions
            backBtn={false}
            OnPressFlipCamera={() => this.switchBetweenFrontAndBackCamera()}
            flipCamera
            OnPressExit={() => this.handlePressBackButton()}
          />
        )}
        <View style={styles.wrapper}>
          <View style={styles.contentLeft}>
            <View style={styles.center}>
              {/* <QRCode
										getRef={(c) => (this.svg = c)}
										value="Just some string value"
										size={resize(250)}
									/> */}
              <Image
                source={Assets.qr_new}
                style={{ width: resize(250), height: resize(250) }}
                resizeMode={"contain"}
              />
              <Text
                style={styles.textInfo}
              >{`Scan to Download \n and Create your Account!`}</Text>
            </View>
            {/* <Tabs
              style={styles.tabStyle}
              onChangeTab={({ i }) => this.handleChangeTab(i)}
              tabContainerStyle={styles.tabsContainer}
              page={currentTab}
              tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
            >
              <Tab
                style={styles.transparentContainer}
                tabStyle={styles.tabStyle}
                heading="I Have an Account"
                activeTabStyle={styles.activeTabStyle}
                textStyle={styles.tabTextStyle}
                activeTextStyle={styles.tabTextStyle}
              >
                <View style={styles.center}>
                  <Text style={styles.qrTitle}>{"Scan QR Code"}</Text>
                  <View style={styles.scanQrContainer}>
                    <QRCodeScanner
                      cameraType={cameraType}
                      onRead={e => this.onSuccessScan(e)}
                      cameraStyle={styles.scanQrContainer}
                      vibrate
                      reactivate
                      reactivateTimeout={3000}
                    />
                  </View>
                </View>
              </Tab>
              <Tab
                style={styles.transparentContainer}
                tabStyle={styles.tabStyle}
                heading="I’m a New User"
                activeTabStyle={styles.activeTabStyle}
                textStyle={styles.tabTextStyle}
                activeTextStyle={styles.tabTextStyle}
              >
                <View style={styles.center}>
                  <Image
                    source={Assets.qr_new}
                    style={{ width: resize(250), height: resize(250) }}
                    resizeMode={"contain"}
                  />
                  <Text
                    style={styles.textInfo}
                  >{`Scan to Download \n and Create your Account!`}</Text>
                </View>
              </Tab>
            </Tabs>
           */}
          </View>
          <View style={styles.contentRight}>
            <View style={styles.appointmentsTitleContainer}>
              <Text style={styles.appointmentsTitle}>
                {" "}
                {en.audition_appointments_title}{" "}
              </Text>
            </View>
            <View style={styles.appointmentsContainer}>
              <View style={styles.appointmentsContent}>
                {appointments.length > 0 &&
                  this.sortAppointments().map((appointment, index) => {
                    if (appointment.length > 0) {
                      if (
                        appointment.time[index] > appointment.time[index - 1]
                      ) {
                        console.log(appointment.name);
                      }
                    }
                    return <AppointmentsItem key={index} {...appointment} />;
                  })}
              </View>
            </View>
          </View>
        </View>
        <View style={styles.logoContainer}>
          <Image resizeMode="contain" source={GigIcon} />
        </View>
      </BackgroundWrapper>
    );
  }
}

const mapStateToProps = state => ({
  audition_id: state.manage.audition_id,
  appointments: state.manage.appointments
});

export default connect(mapStateToProps)(CheckinGuest);
