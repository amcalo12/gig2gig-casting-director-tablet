import React, { Component } from "react";
import {
    View,
    Text,
    Modal,
    TouchableOpacity,
    Image,
    TextInput,
    Platform,
    Keyboard
} from "react-native";
import { resize, NEXABOLD, NEXALIGHT } from "../../../assets/styles";
import en from "../../../locale/en";
import colors from "../../../utils/colors";
import { Assets } from "../../../assets";
import Autocomplete from "react-native-autocomplete-input";
import {
    addInstantFeedback,
    getSuggestedAudition
} from "../../../api/instant_feedback";
import showMessage from "../../../utils/show_message/index";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import parseError from "../../../utils/parse_error";
import { styles } from "./styles";

export default class FeedbackSelection extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal
                // animationType="slide"
                animationType="none"
                transparent={true}
                visible={this.props.isVisible}
                onRequestClose={this.props.onCloseClick}
            >
                <View
                    style={styles.modalContainer}
                    keyboardShouldPersistTaps="never"
                    showsVerticalScrollIndicator={false}
                >
                    <TouchableOpacity style={styles.modalSubContainer} activeOpacity={1}>
                        <View
                            style={{
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                        >
                            <TouchableOpacity
                                style={{ position: "absolute", left: 5 }}
                                onPress={this.props.onCloseClick}
                            >
                                <Image source={Assets.close} />
                            </TouchableOpacity>
                            <Text style={{ color: colors.PRIMARY, fontSize: resize(18) }}>
                                {en.instant_feedback}
                            </Text>
                        </View>

                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                            <Button title={"Custom"} onClick={this.props.onCustomClick} />
                            <Button title={"Standard"} onClick={this.props.onStandardClick} />
                        </View>

                    </TouchableOpacity>
                </View>
            </Modal>
        );
    }
}

export const Button = props => {
    return (
        <TouchableOpacity
            style={styles.buttonContainer}
            onPress={props.onClick}
        >
            <Text style={{ fontFamily: NEXABOLD, color: colors.WHITE }}>
                {props.title}
            </Text>
        </TouchableOpacity>
    )
}