import React, { Component, Fragment } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Keyboard,
  Linking,
} from "react-native";
import { StackActions, NavigationActions } from "react-navigation";
import { Container, Content } from "native-base";
import moment from "moment";
import { connect } from "react-redux";

//custom
import styles from "./style";
import LightBar from "../../../components/commons/light_bar";
import Header from "../../../components/commons/header";
import BackButton from "../../../components/commons/back_button";
import HeaderText from "../../../components/commons/header_text";
import InvitationButton from "../../../components/auditions/invitation_button";
import BorderedInput from "../../../components/auditions/bordered_input";
import PicIcon from "../../../assets/icons/auditions/cover-icon.png";
import BorderedDatepicker from "../../../components/auditions/bordered_datepicker";
import BorderedTimepicker from "../../../components/auditions/bordered_timepicker";
import UnionTag from "../../../components/auditions/union_tag";
import ContractTag from "../../../components/auditions/contract_tag";
import ProductionTag from "../../../components/auditions/production_tag";
import Role from "../../../components/auditions/role";
import File from "../../../components/auditions/file";
import GradientButton from "../../../components/commons/gradient_button";
import FileMenu from "../../../components/auditions/file_menu";
import AddRoleModal from "../../../components/auditions/add_role_modal";
import EditRoleModal from "../../../components/auditions/edit_role_modal";
import AvailableAppointmentsModal from "../available_appointments_modal";
import AddInvitationModal from "../../../components/auditions/add_invitation_modal";
import Loading from "../../../components/commons/loading";
import getImage from "../../../utils/get_image";
import roleIcon from "../../../assets/icons/auditions/role-icon.png";
import LocationModal from "../location_modal";
import LocationButton from "../../../components/auditions/location_button";
import showMessage from "../../../utils/show_message";
import parseError from "../../../utils/parse_error";
import showAlert from "../../../utils/show_alert";
import shareContent from "../../../utils/share_content";
import getFile from "../../../utils/get_file";
import BlockLoading from "../../../components/auditions/block_loading";
import validateEmail from "../../../utils/validate_email";

//api
import {
  getAudition,
  updateAudition,
  deleteMedia,
} from "../../../api/auditions";

//locale
import en from "../../../locale/en";
import isUrl from "../../../utils/validate_url";

//Images
import Online from "../../../assets/icons/auditions/online.png";
import Ofline from "../../../assets/icons/auditions/offline.png";
import { BorderInputSelect } from "../../../components/auditions/bordered_input_v2";
import ImagePicker from "react-native-image-crop-picker";
import RenameModal from "../../../components/auditions/rename_file";
import { getThumbnail } from "../../../utils/constant";
import ModalDropdown from "react-native-modal-dropdown";
import SwitchToggle from "react-native-switch-toggle";
import { NEXABOLD, NEXALIGHT, resize } from "../../../assets/styles";
import colors from "../../../utils/colors";
import { general_styles } from "../../../utils/general_style";
import picker_downflag from "../../../assets/icons/select_radius/dropdown.png";
import DeviceInfo from "react-native-device-info";

class EditAudition extends Component {
  state = {
    audition_id: this.props.navigation.getParam("audition_id", ""),
    title: "",
    location: null,
    description: "",
    contractDateStart: "",
    contractDateEnd: "",
    cover: null,
    cover_name: "",
    rehearsalDateStart: "",
    rehearsalDateEnd: "",
    url: "",
    date: null,
    time: null,
    roles: [],
    files: [],
    union: "",
    contract: "",
    production: [],
    unionTags: ["UNION", "NONUNION"],
    contractTags: ["PAID", "UNPAID", "ACADEMIC"],
    productionTags: [
      "THEATER",
      "FILM",
      "VOICEOVER",
      "COMMERCIALS",
      "PERFORMING ARTS",
      "MODELING",
      "TV & VIDEO",
    ],
    invitations: [],
    enableEditRoles: false,
    roleSelected: null,
    roleName: "",
    roleDescription: "",
    roleImage: null,
    roleImageName: "",
    optionsPosition: { py: 0, px: 0 },
    fileSelected: {},
    showModalAddRole: false,
    editRoleModal: false,
    showAppointmentsModal: false,
    showInvitationModal: false,
    shouldBeInvite: false,
    newInvitationEmail: "",
    showFileMenu: false,
    loading: false,
    blockLoading: false,
    roleNameError: false,
    roleDescriptionError: false,
    showLocationModal: false,
    appointments: {},
    // appointments: undefined,
    dates: [],
    idCover: null,
    personalInformation: "",
    additionalInformation: "",
    phone: "",
    email: "",
    other: "",
    isVisible: false,
    fileName: "",
    isRename: false,
    endDate: "",
    isSocialDistancing: false,
    groupSize: null,
  };

  componentDidMount() {
    this.fetchAudition();
  }

  fetchAudition = async () => {
    const { audition_id } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getAudition(audition_id);
      console.log(response);
      const data = response.data.data;
      let roles = [];

      data.roles.map((role) => {
        roles.push({
          id: role.id,
          name: role.name,
          description: role.description,
          cover: { uri: role.image.url, thumbnail: role.image.thumbnail },
          cover_name: role.image.name,
        });
      });

      if (data.dates.length > 0) {
        data.dates.map((data) => {
          if (data.type == "contract") {
            this.state.contractDateStart =
              data.from != null ? moment(data.from).format() : "";
            this.state.contractDateEnd =
              data.to != null ? moment(data.to).format() : "";
          }

          if (data.type == "rehearsal") {
            this.state.rehearsalDateStart =
              data.from != null ? moment(data.from).format() : "";
            this.state.rehearsalDateEnd =
              data.to != null ? moment(data.to).format() : "";
          }
        });
      }

      // contractDateStart: data.dates.length > 0 && data.dates[0] != undefined ? moment(data.dates[0].from).format() : "",
      // 	contractDateEnd: data.dates.length > 0 && data.dates[0] != undefined ? moment(data.dates[0].to).format() : "",
      // 	rehearsalDateStart: data.dates.length > 1 && data.dates[1] != undefined ? moment(data.dates[1].from).format() : "",
      // 	rehearsalDateEnd: data.dates.length > 1 && data.dates[1] != undefined ? moment(data.dates[1].to).format() : "",
      console.log("new Date(data.end_date) :>> ", data.end_date);
      console.log("new Date(data.end_date) :>> ", moment(data.end_date));
      this.setState({
        title: data.title,
        date: moment(data.date).format(),
        time: moment(data.time, "HH:mm:ss").format("hh:mm A"),
        location: data.location,
        description: data.description,
        cover: { uri: data.cover },
        cover_thumbnail: data.cover_thumbnail,
        dates: data.dates,
        url: data.url,
        appointments: data.apointment,
        union: data.union,
        contract: data.contract,
        production: data.production,
        roles,
        files: data.media,
        idCover: data.id_cover,
        invitations: data.contributors,
        personalInformation: data.personal_information,
        additionalInformation: data.additional_info,
        phone: data.phone,
        email: data.email,
        other: data.other_info,
        online: data.online,
        // endDate: moment(data.end_date),
        endDate: data.end_date,
        isSocialDistancing: data.grouping_enabled == 1 ? true : false,
        groupSize: data.grouping_capacity,
        // contractDateStart: data.dates.length > 0 && data.dates[0] != undefined ? moment(data.dates[0].from).format() : "",
        // contractDateEnd: data.dates.length > 0 && data.dates[0] != undefined ? moment(data.dates[0].to).format() : "",
        // rehearsalDateStart: data.dates.length > 1 && data.dates[1] != undefined ? moment(data.dates[1].from).format() : "",
        // rehearsalDateEnd: data.dates.length > 1 && data.dates[1] != undefined ? moment(data.dates[1].to).format() : "",
      });
    } catch (error) {
      console.log(error);
    }

    this.setState({ loading: false });
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;

    navigation.goBack(null);
  };

  handlePressManageInvitations = () => {
    const { showInvitationModal } = this.state;

    this.setState({
      showInvitationModal: !showInvitationModal,
    });
  };

  onChangePersonnelInformation = (personalInformation) => {
    this.setState({ personalInformation });
  };
  onChangeAdditionalInformation = (additionalInformation) => {
    this.setState({ additionalInformation });
  };

  onChangePhone = (phone) => {
    if (phone.length < 14) {
      let customPhone = String(phone).replace(/[^0-9]/g, "");
      this.setState({ phone: customPhone });
    }
  };

  onChangeEmail = (email) => {
    this.setState({ email });
  };

  onChangeOther = (other) => {
    this.setState({ other });
  };

  onChangeTitle = (title) => {
    this.setState({ title });
  };

  onChangeLocation = (location) => {
    this.setState({ location });
  };

  onChangeDescription = (description) => {
    if (description.length <= 200) {
      this.setState({ description });
    }
  };

  handlePressCoverPhoto = async () => {
    try {
      const response = await getImage();

      ImagePicker.openCropper({
        path: response.uri,
        // width: image.width,
        // height: image.height
      })
        .then(async (data) => {
          console.log("Crop image", data);
          let newData = response;
          newData["uri"] = data.path;

          let thumbnailImage = await getThumbnail(data.path);
          console.log("thumbnail :", thumbnailImage);

          this.setState({
            cover: { uri: newData.uri, thumbnail: thumbnailImage.uri },
            isVisible: false,
            isRename: false,
            fileName: "",
            cover_name: response.fileName,
          });
        })
        .catch(async (error) => {
          let thumbnailImage = await getThumbnail(data.path);
          console.log("thumbnail :", thumbnailImage);
          this.setState({
            cover: { uri: response.uri, thumbnail: thumbnailImage.uri },
            isVisible: false,
            isRename: false,
            fileName: "",
            cover_name: response.fileName,
          });
        });

      // this.setState({
      // 	cover: { uri: response.uri },
      // });
    } catch (error) {
      console.log(error);
    }
  };

  onChangeUrl = (url) => {
    this.setState({ url });
  };

  onChangeDate = (date) => {
    this.setState({ date });
  };

  onChangeTime = (time) => {
    var coeff = 1000 * 60 * 10;
    var date = new Date("02/02/2019 " + time);
    var rounded = new Date(Math.round(date.getTime() / coeff) * coeff);
    this.setState({ time: rounded });
  };

  handlePressAvailableAppointmnets = () => {
    const { showAppointmentsModal } = this.state;

    this.setState({
      showAppointmentsModal: !showAppointmentsModal,
    });
  };

  handlePressEditRoles = () => {
    this.setState({
      enableEditRoles: !this.state.enableEditRoles,
    });
  };

  handleValidateURL = async () => {
    const { url } = this.state;
    return new Promise((resolve, reject) => {
      if (!isUrl(url)) {
        reject({ response: { data: { data: "Enter a valid url" } } });
      }
      resolve();
    });
  };

  handlePressEnableEditDocuments = async () => {
    try {
      let { files } = this.state;
      const file = await getFile();
      files.push({
        url: file.url,
        type: file.type,
        metadata: file.metadata,
        name: file.filename,
        shareable: "yes",
      });
      this.setState({ files });
    } catch (error) {
      console.log(error);
    }
  };

  handlePressUnionTag = (union) => {
    this.setState({ union });
  };

  handlePressContractTag = (contract) => {
    this.setState({ contract });
  };

  handlePressProductionTag = (newTag) => {
    let { production } = this.state;

    if (production.includes(newTag)) {
      let index = production.indexOf(newTag);

      if (index !== -1) {
        production.splice(index, 1);
      }

      return this.setState({ production });
    }

    return this.setState({
      production: [...production, newTag],
    });
  };

  validateFields = () => {
    const {
      title,
      date,
      time,
      location,
      description,
      cover,
      contractDateStart,
      contractDateEnd,
      rehearsalDateStart,
      rehearsalDateEnd,
      url,
      union,
      production,
      contract,
      personalInformation,
      additionalInformation,
      phone,
      email,
      other,
      online,
    } = this.state;
    let errorMessage = "";

    Keyboard.dismiss();

    if (title === "") {
      errorMessage = en.require_title;
    } else if (!date && !online) {
      errorMessage = en.require_date;
    } else if (!time && !online) {
      errorMessage = en.require_time;
    } else if (!location && !online) {
      errorMessage = en.require_location;
    } else if (description === "") {
      errorMessage = en.require_description;
    } else if (cover === null) {
      errorMessage = en.require_cover;
    } else if (
      contractDateStart != null &&
      contractDateStart != "" &&
      (contractDateEnd == "" || contractDateEnd == null)
    ) {
      errorMessage = en.require_contract_date_end;
    } else if (
      rehearsalDateStart != null &&
      rehearsalDateStart != "" &&
      (rehearsalDateEnd == "" || rehearsalDateEnd == null)
    ) {
      errorMessage = en.require_rehearsal_date_end;
    }

    // else if (contractDateStart === null) {
    // 	errorMessage = en.require_contract_date_start;
    // } else if (contractDateEnd === null) {
    // 	errorMessage = en.require_contract_date_end;
    // } else if (rehearsalDateStart === null) {
    // 	errorMessage = en.require_rehearsal_date_start;
    // } else if (rehearsalDateEnd === null) {
    // 	errorMessage = en.require_rehearsal_date_end;
    // }
    // else if (url === '') {
    // 	errorMessage = en.require_audition_url;
    // }
    else if (union === "") {
      errorMessage = en.require_union_tag;
    } else if (contract === "") {
      errorMessage = en.require_contract_tag;
    } else if (production.length === 0) {
      errorMessage = en.require_production_tag;
    }
    // else if (additionalInformation == '') {
    // 	errorMessage = en.require_additional_nformation;
    // } else if (personalInformation == '') {
    // 	errorMessage = en.require_personal_information;
    // }
    // else if (phone == '') {
    // 	errorMessage = en.require_phone;
    // } else if (email == '') {
    // 	errorMessage = en.require_email;
    // } else if (other == '') {
    // 	errorMessage = en.require_other;
    // }
    // if (!validateEmail(email)) {
    // 	errorMessage = en.valid_email;
    // }
    return errorMessage;
  };

  handlePressSave = async () => {
    const { audition_id } = this.state;
    const { navigation } = this.props;
    const hasError = this.validateFields();
    console.log(hasError);
    if (hasError !== "") {
      return showMessage("danger", hasError);
    }

    this.setState({ blockLoading: true });

    try {
      const formData = this.parseFormData();
      console.log("edit=====");
      console.log(JSON.stringify(formData));
      // await this.handleValidateURL();
      //await updateAudition(audition_id, formData);
      await updateAudition(audition_id, formData);
      this.setState({ blockLoading: false });

      const resetManagerAction = StackActions.reset({
        index: 1,
        key: null,
        actions: [
          NavigationActions.navigate({
            routeName: "Home",
          }),
          NavigationActions.navigate({
            routeName: "ManagerAudition",
            params: { audition_id },
          }),
        ],
      });

      navigation.dispatch(resetManagerAction);
    } catch (error) {
      console.log(error);
      this.setState({ blockLoading: false });
      const parsedError = parseError(error);
      console.log("parsedError", { parsedError });
      showMessage("danger", parsedError);
    }
  };

  parseFormData = () => {
    const {
      title,
      date,
      time,
      location,
      description,
      cover,
      cover_name,
      contractDateStart,
      contractDateEnd,
      rehearsalDateStart,
      rehearsalDateEnd,
      roles,
      url,
      production,
      contract,
      union,
      dates,
      idCover,
      files,
      invitations,
      personalInformation,
      additionalInformation,
      phone,
      email,
      other,
      online,
      endDate,
    } = this.state;

    let formData = {
      title,
      date: online ? null : date,
      time: online ? null : moment(time, "HH::mm A").format("HH:mm"),
      description,
      cover,
      cover_name,
      roles,
      url,
      contract,
      union,
      id_cover: idCover,
      media: files,
      contributors: invitations,
      personal_information: personalInformation,
      additional_info: additionalInformation,
      phone: phone,
      email: email,
      other_info: other,
      online,
      location: online ? null : location,
    };
    formData["production"] = production.join(",");

    let parseDates = [...dates];
    console.log("STATE  :", this.state);
    console.log("rehearsalDateStart :", rehearsalDateStart);

    let isContract = false;
    let isRehearsal = false;

    if (parseDates.length > 0) {
      parseDates.map((data, index) => {
        if (data.type == "contract") {
          isContract = true;
          let contract = data;
          contract["from"] =
            contractDateStart != null
              ? this.getformatedDate(contractDateStart)
              : "";
          contract["to"] =
            contractDateEnd != null
              ? this.getformatedDate(contractDateEnd)
              : "";

          parseDates[index] = contract;
        }

        if (data.type == "rehearsal") {
          isRehearsal = true;
          let contract = data;
          contract["from"] =
            rehearsalDateStart != null
              ? this.getformatedDate(rehearsalDateStart)
              : "";
          contract["to"] =
            rehearsalDateEnd != null
              ? this.getformatedDate(rehearsalDateEnd)
              : "";

          parseDates[index] = contract;
          // if (rehearsalDateStart != null && rehearsalDateStart != "") {
          // 	parseDates[1]['from'] = rehearsalDateStart
          // }
          // if (rehearsalDateEnd != null && rehearsalDateEnd != "") {
          // 	parseDates[1]['to'] = rehearsalDateEnd
          // }
        }
      });

      // if (contractDateStart != null && contractDateStart != "") {
      // 	parseDates[0]['from'] = contractDateStart != null ? contractDateStart : "";
      // }
      // if (contractDateEnd != null && contractDateEnd != "") {
      // 	parseDates[0]['to'] = contractDateEnd != null ? contractDateEnd : "";
      // }
    }

    if (!isContract) {
      parseDates[parseDates.length] = {
        to: this.getformatedDate(contractDateEnd),
        from: this.getformatedDate(contractDateStart),
        type: "contract",
      };
    }

    if (!isRehearsal) {
      parseDates[parseDates.length] = {
        to: this.getformatedDate(rehearsalDateEnd),
        from: this.getformatedDate(rehearsalDateStart),
        type: "rehearsal",
      };
    }

    // else {
    // 	dateType = {
    // 		to: contractDateEnd,
    // 		from: contractDateStart,
    // 		type: 1
    // 	};
    // 	parseDates.push(dateType);
    // }

    // if (parseDates.length > 1) {
    // 	if (rehearsalDateStart != null && rehearsalDateStart != "") {
    // 		parseDates[1]['from'] = rehearsalDateStart
    // 	}
    // 	if (rehearsalDateEnd != null && rehearsalDateEnd != "") {
    // 		parseDates[1]['to'] = rehearsalDateEnd
    // 	}
    // } else {
    // 	dateType = {
    // 		to: rehearsalDateEnd,
    // 		from: rehearsalDateStart,
    // 		type: 2
    // 	};
    // 	parseDates.push(dateType);
    // }

    formData["dates"] = parseDates;
    if (online) {
      formData["end_date"] = moment(endDate).format("YYYY-MM-DD HH:mm:ss");
      // formData["end_date"] = moment(endDate).format("LL hh:mm a");
    }
    console.log("end date===");
    console.log(moment(endDate).format("YYYY-MM-DD HH:mm:ss"));
    console.log(moment(endDate).format("LL hh:mm a"));
    return formData;
  };

  getformatedDate = (date) => {
    return moment(date).format("YYYY-MM-DD");
  };

  onPressFileMore = async (file, index) => {
    try {
      const { showFileMenu } = this.state;

      await this.handleGetMeasure(file, index);

      this.setState({
        showFileMenu: !showFileMenu,
      });
    } catch (error) {}
  };

  onPressRole = (id = null) => {
    const { editRoleModal, roles } = this.state;

    const role = roles[id];

    if (role) {
      this.setState({
        editRoleModal: !editRoleModal,
        roleSelected: id,
        roleName: role.name,
        roleDescription: role.description,
        roleImage: role.cover,
        roleImageName: role.cover_name,
      });
    }

    return null;
  };

  handleEditRoleInfo = async () => {
    let {
      roleDescription,
      roleName,
      roleImage,
      showModalAddRole,
      roleSelected,
      roles,
      roleImageName,
    } = this.state;

    await this.handleValidateRolAndDescription();
    await this.handleValidateDescription();
    // if (this.validateRole()) {
    let role = roles[roleSelected];

    role["name"] = roleName;
    role["description"] = roleDescription;
    role["cover"] = roleImage;
    role["cover_name"] = roleImageName;

    this.setState({
      showModalAddRole: !showModalAddRole,
      roles,
      roleDescription: "",
      roleName: "",
      roleSelected: null,
      roleImage: null,
    });
    // }
  };

  handleValidateRolAndDescription = async () => {
    const { roleName, roleDescription } = this.state;
    return new Promise((resolve, reject) => {
      if (String(roleName).trim() === "") {
        // reject(showAlert(en.require_role_name))
        reject({ response: { data: { data: en.require_role_name } } });
      }

      resolve();
    });
  };

  handleValidateDescription = async () => {
    const { roleDescription } = this.state;

    return new Promise((resolve, reject) => {
      if (String(roleDescription).trim() === "") {
        // reject(showAlert(en.require_role_description))
        reject({ response: { data: { data: en.require_role_description } } });
      }

      resolve();
    });
  };

  validateRole = async () => {
    await this.handleValidateRolAndDescription();
    await this.handleValidateDescription();
    // const { roleName, roleDescription } = this.state;

    // if (roleName === '') {
    //   showAlert(en.require_role_name)

    //   return false;
    // }

    // if (roleDescription === '') {
    //   showAlert(en.require_role_description)

    //   return false;
    // }

    // return true;
  };

  handleAddAnotherRole = () => {
    let { roleDescription, roleName, roles, roleSelected } = this.state;

    if (roleSelected === null) {
      roles.push(roleName);
    } else {
      roles[roleSelected] = roleName;
    }

    this.setState({
      roleSelected: null,
      roleDescription: "",
      roleName: "",
      roles,
    });
  };

  handlePressCloseAddRoleModal = () => {
    const { showModalAddRole } = this.state;

    this.setState({
      showModalAddRole: !showModalAddRole,
      roleName: "",
      roleDescription: "",
      roleImage: null,
      roleSelected: null,
    });
  };

  onChangeRoleName = (roleName) => {
    if (roleName.length <= 35) {
      this.setState({ roleName });
    }
  };

  onChangeRoleImageName = (roleImageName) => {
    this.setState({ roleImageName });
  };

  onChangeRoleDescription = (roleDescription) => {
    if (roleDescription.length <= 150) {
      this.setState({ roleDescription });
    }
  };

  handlePressUploadRoleImage = async () => {
    try {
      const response = await getImage();

      let thumbnailImage = await getThumbnail(response.uri);
      console.log("thumbnail :", thumbnailImage);

      this.setState({
        roleImage: { uri: response.uri, thumbnail: thumbnailImage.uri },
        roleImageName: response.fileName,
      });
    } catch (error) {
      console.log(error);
    }
  };

  handleDeleteRole = (index = null) => {
    let { roles } = this.state;

    if (index !== null) {
      roles.splice(index, 1);
    }

    this.setState({ roles });

    this.onPressRole();
  };

  handleEditRole = () => {
    this.setState({
      editRoleModal: false,
      showModalAddRole: true,
    });
  };

  onPressCloseEditModal = () => {
    const { editRoleModal } = this.state;

    this.setState({
      editRoleModal: !editRoleModal,
      roleName: "",
      roleDescription: "",
      roleImage: null,
      roleSelected: null,
    });
  };

  handlePressManageAppointments = () => {
    const { showAppointmentsModal } = this.state;

    this.setState({
      showAppointmentsModal: !showAppointmentsModal,
    });
  };

  handleSetRef = (file, index) => {
    this[`file_${index}`] = file;
  };

  handleGetMeasure = async (file, index) => {
    await new Promise((resolve, reject) => {
      const refName = `file_${index}`;
      if (refName in this) {
        this[refName].measure((a, b, width, height, px, py) => {
          this.setState(
            {
              optionsPosition: { py, px },
              fileSelected: file,
            },
            () => resolve(true)
          );
        });
      } else {
        this.setState(
          {
            optionsPosition: { py: 0, px: 0 },
            fileSelected: file,
          },
          () => resolve(false)
        );
      }
    });
  };

  handleDismissInvitationModal = () => {
    this.setState(
      {
        newInvitationEmail: "",
        shouldBeInvite: false,
      },
      () => this.handlePressManageInvitations()
    );
  };

  handleChangeEmailInvitation = (value) => {
    this.setState({
      newInvitationEmail: value,
    });
  };

  handlePressLocation = () => {
    const { showLocationModal } = this.state;

    this.setState({
      showLocationModal: !showLocationModal,
    });
  };

  handlePressSetLocation = (location) => {
    const { showLocationModal } = this.state;

    this.setState({
      location,
      showLocationModal: !showLocationModal,
    });
  };

  onChangeContractDateStart = (contractDateStart) => {
    this.setState({ contractDateStart, contractDateEnd: "" });
    return;
    const ifAfter = moment(contractDateStart).isAfter(
      moment(this.state.contractDateEnd).format("LL")
    );
    if (ifAfter) {
      showMessage(
        "danger",
        "The start time cannot be later than the end time."
      );
    } else {
      this.setState({ contractDateStart });
    }
    // console.log({ contractDateStart: moment(contractDateStart), contractDateEnd: moment(this.state.contractDateEnd) })
  };

  onChangeContractDateEnd = (contractDateEnd) => {
    this.setState({ contractDateEnd });
  };

  onChangeRehearsalDateStart = (rehearsalDateStart) => {
    this.setState({ rehearsalDateStart, rehearsalDateEnd: "" });
    return;
    const ifAfter = moment(rehearsalDateStart).isAfter(
      moment(this.state.rehearsalDateEnd).format("LL")
    );
    if (ifAfter) {
      showMessage(
        "danger",
        "The start time cannot be later than the end time."
      );
    } else {
      this.setState({ rehearsalDateStart });
    }
  };

  onChangeRehearsalDateEnd = (rehearsalDateEnd) => {
    this.setState({ rehearsalDateEnd });
  };

  shareAsset = async (file) => {
    try {
      await shareContent(`Open this link: \n ${file.url}`);
    } catch (error) {
      console.log(error);
    }
  };

  openAsset = (file) => {
    return Linking.openURL(file.url);
  };

  handleDeleteFile = async (file) => {
    let { files } = this.state;
    const index = files.indexOf(file);

    if (file.hasOwnProperty("id")) {
      await deleteMedia(file.id);
    }

    files.splice(index, 1);
    this.onPressFileMore({});

    return showMessage("success", en.document_was_deleted);
  };

  handleSendInvitation = async () => {
    let { invitations, newInvitationEmail } = this.state;

    if (newInvitationEmail === "") {
      return showAlert(en.require_email);
    }

    if (!validateEmail(newInvitationEmail)) {
      return showAlert(en.valid_email);
    }

    try {
      await new Promise((resolve) => {
        invitations.push({
          email: newInvitationEmail,
          status: 0,
        });

        this.setState(
          {
            invitations,
            newInvitationEmail: "",
          },
          () => resolve(true)
        );
      });

      this.handlePressShouldAddInvitation();
    } catch (error) {
      console.log(error);
    }
  };

  handleDismissInvitationModal = () => {
    this.setState(
      {
        newInvitationEmail: "",
        shouldBeInvite: false,
      },
      () => this.handlePressManageInvitations()
    );
  };

  handlePressShouldAddInvitation = () => {
    const { shouldBeInvite } = this.state;

    this.setState({
      shouldBeInvite: !shouldBeInvite,
    });
  };

  handleChangeEmailInvitation = (value) => {
    this.setState({
      newInvitationEmail: value,
    });
  };

  handleDeleteInvitation = (invitation) => {
    let { invitations } = this.state;
    const index = invitations.indexOf(invitation);

    invitations.splice(index, 1);

    this.setState({ invitations });
  };

  handleFileShare = (handle) => {
    let value = handle ? "yes" : "No";
    let { fileSelected, files } = this.state;
    const file = { ...fileSelected };
    const actualFiles = [...files];
    let index = -1;
    for (let i = 0; i < files.length; i++) {
      if (files[i].url == fileSelected.url) {
        index = i;
      }
    }
    actualFiles[index].shareable = value;
    file.shareable = value;
    this.setState({ fileSelected: file, files: actualFiles });
  };

  onOnlinePress = () => {
    this.setState((prevState) => {
      return { online: !prevState.online };
    });
  };

  onCloseClick = () => {
    this.setState({ isVisible: false });
  };

  onSubmitClick = () => {
    if (this.state.isRename) {
      let index = this.state.files.findIndex(
        (x) => x.url == this.state.fileSelected.url
      );

      this.state.files[index].name = this.state.fileName;
      this.state.fileName = "";
    } else {
      this.setState({ cover_name: this.state.fileName });
    }
    this.setState({ isVisible: false });
  };

  onChangeText = (fileName) => {
    this.setState({ fileName });
  };

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    );
  };

  onEndChangeDate = (endDate) => {
    // this.state.option[this.selectedIndex].date = date
    // this.setState({ option: this.state.option });
    this.setState({ endDate });
    console.log("on change date====");
    console.log(endDate);
  };

  render() {
    const {
      title,
      location,
      description,
      contractDateStart,
      contractDateEnd,
      rehearsalDateStart,
      rehearsalDateEnd,
      url,
      date,
      time,
      union,
      unionTags,
      contract,
      contractTags,
      production,
      productionTags,
      roles,
      files,
      enableEditRoles,
      showAppointmentsModal,
      showInvitationModal,
      shouldBeInvite,
      newInvitationEmail,
      invitations,
      showModalAddRole,
      editRoleModal,
      roleName,
      roleDescription,
      roleImage,
      roleImageName,
      roleSelected,
      showFileMenu,
      fileSelected,
      optionsPosition,
      loading,
      roleNameError,
      roleDescriptionError,
      cover,
      showLocationModal,
      appointments,
      blockLoading,
      personalInformation,
      additionalInformation,
      phone,
      email,
      other,
      online,
    } = this.state;

    if (loading) {
      return <Loading />;
    }

    return (
      <Container style={styles.container}>
        <LightBar />
        {/* modal invitation */}
        <AddInvitationModal
          invitations={invitations}
          show={showInvitationModal}
          shouldBeInvite={shouldBeInvite}
          newInvitationEmail={newInvitationEmail}
          sendInvitation={this.handleSendInvitation}
          dismiss={this.handleDismissInvitationModal}
          enableAddInvitation={this.handlePressShouldAddInvitation}
          onChangeEmail={this.handleChangeEmailInvitation}
          delete={this.handleDeleteInvitation}
        />
        {this.renderRenameModal()}
        <FileMenu
          show={showFileMenu}
          dismiss={() => this.onPressFileMore({})}
          posy={optionsPosition.py}
          posx={optionsPosition.px}
          delete={() => this.handleDeleteFile(fileSelected)}
          // showOptionShare
          // share={() => this.shareAsset(fileSelected)}
          // showOptionOpen
          // openIn={() => this.openAsset(fileSelected)}
          // showOptionDelete
          // delete={() => this.handleDeleteFile(fileSelected)}
          showShareable
          isShareable={fileSelected.shareable === "yes"}
          rename={true}
          onRename={() => {
            console.log("fileSelected :", fileSelected);
            this.setState({
              isVisible: true,
              showFileMenu: false,
              isRename: true,
              fileName: fileSelected.name,
            });
          }}
          handleFileShare={this.handleFileShare}
        >
          {"name" in fileSelected && (
            <File
              activeOpacity={1}
              filename={fileSelected.name}
              fileType={fileSelected.type}
              file={fileSelected}
              onPressMore={() => this.onPressFileMore({})}
            />
          )}
        </FileMenu>
        {/* add role modal */}
        <AddRoleModal
          done={this.handleEditRoleInfo}
          addAnother={this.handleAddAnotherRole}
          show={showModalAddRole}
          dismiss={this.handlePressCloseAddRoleModal}
          name={roleName}
          description={roleDescription}
          roleImage={roleImage}
          changeRoleName={this.onChangeRoleName}
          onChangeRoleImageName={this.onChangeRoleImageName}
          changeRoleDescription={this.onChangeRoleDescription}
          showAddAnotherRole={false}
          onPressTakeImage={() => this.handlePressUploadRoleImage()}
          nameError={roleNameError}
          roleImageName={roleImageName}
          descriptionError={roleDescriptionError}
        />
        {/* edit role modal */}
        <EditRoleModal
          // delete={() => this.handleDeleteRole(roleSelected)} TODO: phase 2
          name={roleName}
          edit={this.handleEditRole}
          description={roleDescription}
          show={editRoleModal}
          dismiss={() => this.onPressCloseEditModal()}
          cover={roleImage}
        />
        {/* appointment modal */}
        {console.log("appointments :>> ", appointments)}
        {appointments.slots != undefined && appointments.slots.length > 0 ? (
          <AvailableAppointmentsModal
            show={showAppointmentsModal}
            dismiss={this.handlePressManageAppointments}
            details={appointments.general}
            appointments={appointments.slots}
          />
        ) : null}
        {/* location modal */}
        <LocationModal
          show={showLocationModal}
          dismiss={this.handlePressLocation}
          agree={this.handlePressSetLocation}
          location={location}
        />
        {/* block loading */}
        <BlockLoading show={blockLoading} />
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
          center={<HeaderText title={en.edit_title} />}
        />
        <View style={styles.content}>
          <Content disableKBDismissScroll style={styles.paddingContent}>
            <InvitationButton
              onPress={() => this.handlePressManageInvitations()}
              title={en.manage_invitation_title}
            />
            <View style={styles.row}>
              <BorderedInput
                placeholder={en.title_placeholder}
                value={title}
                onChange={this.onChangeTitle}
                containerStyle={styles.dateInputContainer2}
              />
              <BorderInputSelect
                placeholder={en.online_sub}
                value={title}
                onChange={this.onChangeTitle}
                containerStyle={styles.onlineContainer}
                withIcon
                icon={online ? Online : Ofline}
                // onPress={this.onOnlinePress}
              />
            </View>
            {online ? (
              <View style={styles.row}>
                {console.log("this.state.endDate :>> ", this.state.endDate)}
                <BorderedDatepicker
                  mode={"datetime"}
                  format={"LL hh:mm a"}
                  placeholder={en.end_date_placeholder}
                  value={moment(this.state.endDate).format("LL hh:mm a")}
                  // value={this.state.endDate}
                  onChange={this.onEndChangeDate}
                  containerStyle={styles.endDateInputContainer}
                  minDate={moment(new Date()).add(1, "day")}
                />
              </View>
            ) : null}
            {online ? null : (
              <View style={styles.row}>
                <BorderedDatepicker
                  placeholder={en.date_placeholder}
                  value={moment(date).format("LL")}
                  onChange={this.onChangeDate}
                  containerStyle={styles.dateInputContainer}
                />
                <BorderedTimepicker
                  placeholder={en.time_placeholder}
                  dateA={moment(date).format("LL")}
                  value={time}
                  onChange={this.onChangeTime}
                  containerStyle={styles.timeInputContainer}
                />
                <LocationButton
                  title={
                    location !== null
                      ? en.location_saved_placeholder
                      : en.location_placeholder
                  }
                  withIcon
                  onPress={() => this.handlePressLocation()}
                />
              </View>
            )}
            <View style={styles.row}>
              <BorderedInput
                placeholder={en.description_placeholder}
                value={description}
                onChange={this.onChangeDescription}
                containerStyle={styles.descriptionInputContainer}
                style={styles.descriptionInput}
                multiline
              />
              <TouchableOpacity
                style={styles.coverContainer}
                onPress={() => this.handlePressCoverPhoto()}
              >
                {cover === null && (
                  <Fragment>
                    <View style={styles.coverPhotoContainer}>
                      <Text style={styles.coverPhoto}>
                        {en.cover_photo_placeholder}
                      </Text>
                    </View>
                    <View style={styles.picIconContainer}>
                      <Image source={PicIcon} style={styles.picIcon} />
                    </View>
                  </Fragment>
                )}
                {cover !== null && (
                  <Image source={cover} style={styles.coverImage} />
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.viewForPersonnelInformationContainer}>
              <BorderedInput
                placeholder={en.personnel_information_placeholder}
                value={personalInformation}
                onChange={this.onChangePersonnelInformation}
                containerStyle={styles.personnelInformationInputContainer}
                style={styles.descriptionInput}
                multiline
              />
              <View style={styles.viewForAdditionalInformationContainer}>
                <BorderedInput
                  placeholder={en.additional_information_placeholder}
                  value={additionalInformation}
                  onChange={this.onChangeAdditionalInformation}
                  containerStyle={styles.additionallInformationInputContainer}
                  style={styles.descriptionInput}
                  multiline
                />
              </View>
              <View style={styles.contractInformationTitle}>
                <Text style={styles.contractInformationText}>
                  Contract Information
                </Text>
              </View>
            </View>
            <View style={styles.row}>
              <BorderedDatepicker
                placeholder={en.contract_date_start_placeholder}
                value={
                  contractDateStart != ""
                    ? moment(contractDateStart).format("LL")
                    : ""
                }
                onChange={this.onChangeContractDateStart}
                containerStyle={styles.contractDatesInputContainer}
              />
              <BorderedDatepicker
                placeholder={en.contract_date_end_placeholder}
                value={
                  contractDateEnd != ""
                    ? moment(contractDateEnd).format("LL")
                    : ""
                }
                minDate={
                  contractDateStart != ""
                    ? moment(contractDateStart).format("LL")
                    : new Date()
                }
                onChange={this.onChangeContractDateEnd}
                containerStyle={styles.contractDatesInputContainer}
              />
            </View>
            <View style={styles.row}>
              <BorderedDatepicker
                placeholder={en.rehearsal_date_start_placeholder}
                value={
                  rehearsalDateStart != ""
                    ? moment(rehearsalDateStart).format("LL")
                    : ""
                }
                onChange={this.onChangeRehearsalDateStart}
                // minDate={
                // 	contractDateStart !== "" ? contractDateStart : new Date()
                // }
                containerStyle={styles.contractDatesInputContainer}
              />
              <BorderedDatepicker
                placeholder={en.rehearsal_date_end_placeholder}
                value={
                  rehearsalDateEnd != ""
                    ? moment(rehearsalDateEnd).format("LL")
                    : ""
                }
                minDate={
                  rehearsalDateStart != ""
                    ? moment(rehearsalDateStart).format("LL")
                    : new Date()
                }
                onChange={this.onChangeRehearsalDateEnd}
                containerStyle={styles.contractDatesInputContainer}
              />
            </View>
            <BorderedInput
              placeholder={en.audition_url_placeholder}
              value={url}
              onChange={this.onChangeUrl}
            />
            <View style={styles.row}>
              <BorderedInput
                placeholder={en.phone_placeholder}
                value={phone}
                onChange={this.onChangePhone}
                containerStyle={styles.phoneInputContainer}
                keyboardType="numeric"
              />
              <BorderedInput
                placeholder={en.email_placeholder}
                value={email}
                onChange={this.onChangeEmail}
                containerStyle={styles.emailInputContainer}
                keyboardType="email-address"
              />
              <BorderedInput
                placeholder={en.other_placeholder}
                value={other}
                onChange={this.onChangeOther}
                containerStyle={styles.otherInputContainer}
              />
            </View>
            {!online ? (
              <View style={{ marginVertical: 20 }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                  pointerEvents={"none"}
                >
                  <Text
                    style={{
                      fontFamily: NEXABOLD,
                      fontSize: 18,
                      color: colors.PRIMARY,
                    }}
                  >
                    {
                      "Will this audition require socially distanced group entrances?"
                    }
                  </Text>
                  <SwitchToggle
                    backgroundColorOn={
                      styles.switchSettingsActive.backgroundColor
                    }
                    backgroundColorOff={styles.switchSettings.backgroundColor}
                    circleColorOff={styles.switchSettings.color}
                    circleColorOn={styles.switchSettingsActive.color}
                    containerStyle={styles.containerStyle}
                    circleStyle={styles.circleStyle}
                    switchOn={this.state.isSocialDistancing}
                    onPress={this.onSwitchChange}
                  />
                </View>
                <Text
                  style={{
                    fontFamily: NEXALIGHT,
                    fontSize: 18,
                    marginTop: 30,
                    letterSpacing: 1,
                    color: colors.PRIMARY,
                    lineHeight: 25,
                  }}
                >
                  {
                    "Allow for automatic assignment of grouping of performers, based on the parameters set below. Groups can be used to notify performers of updates and changes."
                  }
                </Text>
              </View>
            ) : null}

            {this.state.isSocialDistancing ? (
              // <ModalDropdown
              // 	options={this.state.groupOptions}
              // 	onSelect={(index, option) => {
              // 		// this.onItemClick(option, index)
              // 		console.log('Group option :>> ', option);
              // 		this.setState({ groupSize: option.value })
              // 	}}
              // 	// renderRow={this.renderGroupRow}
              // 	renderSeparator={() => { <View style={{ backgroundColor: 'white' }} /> }}
              // 	dropdownStyle={styles.dropDownStyle}
              // >
              <View
                style={{
                  marginBottom: resize(10, "height"),
                  marginTop: resize(10, "height"),
                }}
              >
                <View style={styles.roundContainer}>
                  <Text style={general_styles.title}>
                    {this.state.groupSize != null
                      ? this.state.groupSize
                      : "Group Size"}
                  </Text>
                  <Image source={picker_downflag} resizeMode="contain" />
                </View>
              </View>
            ) : // </ModalDropdown>
            null}
            <View style={styles.row}>
              <View style={styles.tagsContainer}>
                <Text style={styles.tagTitle}>
                  {en.union_status_tags_title}
                </Text>
                <View style={styles.tags}>
                  {unionTags.map((tag, index) => {
                    return (
                      <UnionTag
                        key={index}
                        name={tag}
                        selected={union === tag}
                        onPress={() => this.handlePressUnionTag(tag)}
                      />
                    );
                  })}
                </View>
                <Text style={styles.tagTitle}>
                  {en.contract_type_tags_title}
                </Text>
                <View style={styles.tags}>
                  {contractTags.map((tag, index) => {
                    console.log("contract === tag :>> ", contract === tag);
                    console.log("tag :>> ", tag);
                    console.log("contract :>> ", contract);
                    return (
                      <ContractTag
                        key={index}
                        name={tag}
                        selected={contract === tag}
                        onPress={() => this.handlePressContractTag(tag)}
                      />
                    );
                  })}
                </View>
                <Text style={styles.tagTitle}>
                  {en.production_type_tags_title}
                </Text>
                <View style={styles.tags}>
                  {productionTags.map((tag, index) => {
                    return (
                      <ProductionTag
                        key={index}
                        name={tag}
                        selected={production.includes(tag)}
                        onPress={() => this.handlePressProductionTag(tag)}
                      />
                    );
                  })}
                </View>
              </View>
              <View style={styles.wrapperRight}>
                {!online ? (
                  <View style={styles.buttonRightContainer}>
                    <TouchableOpacity
                      style={styles.buttonRight}
                      onPress={() => this.handlePressAvailableAppointmnets()}
                    >
                      <Text style={styles.buttonRightText}>
                        {en.available_appointments}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null}
                {enableEditRoles && roles.length > 0 && (
                  <View style={styles.rolesContainer}>
                    <View style={styles.titleContainer}>
                      <Text style={styles.title}>{en.roles_title}</Text>
                    </View>
                    <ScrollView
                      horizontal
                      bounces={false}
                      showsHorizontalScrollIndicator={false}
                    >
                      {roles.map((role, index) => {
                        return (
                          <Role
                            key={index}
                            text={role.name}
                            onPress={() => this.onPressRole(index)}
                            icon={role.cover ? role.cover : roleIcon}
                          />
                        );
                      })}
                    </ScrollView>
                  </View>
                )}
                <View style={styles.buttonRightContainer}>
                  <TouchableOpacity
                    style={styles.buttonRight}
                    onPress={() => this.handlePressEditRoles()}
                  >
                    <Text style={styles.buttonRightText}>
                      {en.edit_role_title}
                    </Text>
                  </TouchableOpacity>
                </View>
                {files.length > 0 && (
                  <View style={styles.documentsContainer}>
                    <View style={styles.titleContainer}>
                      <Text style={styles.title}>{en.documents_title}</Text>
                    </View>
                    <ScrollView
                      horizontal
                      bounces={false}
                      showsHorizontalScrollIndicator={false}
                      style={styles.documentsContent}
                    >
                      {files.map((file, index) => {
                        return (
                          <File
                            key={index}
                            refs={(fileRef) =>
                              this.handleSetRef(fileRef, index)
                            }
                            filename={file.name}
                            fileType={file.type}
                            file={file}
                            onPressMore={() =>
                              this.onPressFileMore(file, index)
                            }
                            containerStyle={styles.fileContainer}
                          />
                        );
                      })}
                    </ScrollView>
                  </View>
                )}
                {/* <View style={styles.buttonRightContainer}>
									<TouchableOpacity
										style={styles.buttonRight}
										onPress={() => this.handlePressEnableEditDocuments()}
									>
										<Text style={styles.buttonRightText}>
											{en.manage_document_title}
										</Text>
									</TouchableOpacity>
								</View> */}
                <View style={styles.buttonSaveContainer}>
                  <GradientButton
                    buttonText={en.save_changes_title}
                    containerStyle={styles.buttonSave}
                    gradientStyle={styles.gradientButtonSave}
                    textStyle={styles.buttonSaveText}
                    onPress={this.handlePressSave}
                  />
                </View>
              </View>
            </View>
          </Content>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  authorize: state.auth.authorize,
  user: state.user.data,
});

export default connect(mapStateToProps)(EditAudition);
