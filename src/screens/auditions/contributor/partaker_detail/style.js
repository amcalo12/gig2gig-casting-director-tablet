import { StyleSheet } from 'react-native';

//custom 
import { width, height, resize, NEXABOLD } from '../../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width,
    height,
  },
  content: {
    marginTop: resize(23, 'height'),
    marginBottom: resize(40, 'height'),
    marginLeft: resize(25),
    marginRight: resize(17),
    flex: 1,
    flexDirection: 'row',
  },
  contentLeft: {
    width: resize(337),
    height: '100%',
    shadowColor: 'rgba(0, 0, 0, 0.4)',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
    borderRadius: 4,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#d6d6d6',
  },
  contentRight: {
    width: resize(733),
    height: '100%',
    paddingLeft: resize(30)
  },
  contributorContainer: {
    height: resize(109, 'height'),
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  contributorButtonText: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: resize(395, 'height'),
  },
  availabilityContainer: {
    width: resize(233),
    shadowColor: 'rgba(0, 0, 0, 0.4)',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
    borderRadius: 4,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#d6d6d6',
    paddingTop: resize(18, 'height')
  },
  availabilityTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  availabilityTitle: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
  },
  availabilityCalendar: {

  },
  feedbackContainer: {
    width: resize(450),
    shadowColor: 'rgba(0, 0, 0, 0.4)',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
    borderRadius: 4,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#d6d6d6',
    paddingVertical: resize(34, 'height'),
  },
  feedbackTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  feedbackTitle: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
  },
  feedbackEmojisContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingLeft: resize(61),
    paddingRight: resize(57),
    marginTop: resize(23, 'height'),
    marginBottom: resize(29, 'height'),
  },
  callbackContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  callback: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
  },
  callbackButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingLeft: resize(90),
    paddingRight: resize(87),
    marginTop: resize(29, 'height'),
  },
  workOnContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: resize(36, 'height')
  },
  workOn: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
  },
  workOnButtons: {
    marginTop: resize(27, 'height'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingLeft: resize(20),
    paddingRight: resize(17)
  },
  sendContainer: {
    width: '100%',
    height: resize(165, 'height'),
    paddingTop: resize(139, 'height'),
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  containerButtonSend: {
    width: resize(204),
    height: resize(50, 'height'),
  },
  gradientButtonSend: {
    width: resize(204),
    height: resize(50, 'height'),
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.18)',
    shadowOffset: { width: 3, height: 0 },
    shadowRadius: 8,
    borderRadius: 28,
  },
  buttonSendText: {
    color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
  }
});

export default styles;