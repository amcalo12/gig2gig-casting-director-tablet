import React, { Component } from "react";
import { View, Text, Platform, Linking } from "react-native";
import { Container } from "native-base";
import { StackActions, NavigationActions } from "react-navigation";
import { connect } from "react-redux";

//custom
import styles from "./style";
import Header from "../../../../components/commons/header";
import BackButton from "../../../../components/commons/back_button";
import HeaderText from "../../../../components/commons/header_text";
import HeaderBorderedButton from "../../../../components/commons/header_bordered_button";
import LightBar from "../../../../components/commons/dark_bar";
import GradientButton from "../../../../components/commons/gradient_button";
import EmojiButton from "../../../../components/auditions/emoji_button";
import StarEmoji from "../../../../assets/icons/auditions/star-emoji.png";
import SmileEmoji from "../../../../assets/icons/auditions/smile-emoji.png";
import GoodEmoji from "../../../../assets/icons/auditions/good-emoji.png";
import DoubtEmoji from "../../../../assets/icons/auditions/doubt-emoji.png";
import BadEmoji from "../../../../assets/icons/auditions/bad-emoji.png";
import FeedbackButton from "../../../../components/auditions/feedback_button";
import PartakerCard from "../../../../components/auditions/partaker_card";
import Calendar from "../../../../components/commons/calendar_list";
import PartakerModal from "../../partaker_modal";
import recordVideo from "../../../../utils/record_video";
import parseDataModalPartaker from "../../../../utils/auditions/parse_data_modal_partaker";
import Loading from "../../../../components/commons/loading";
import showMessage from "../../../../utils//show_message";
import parseError from "../../../../utils/parse_error";
import uploadAsset from "../../../../utils/upload_asset";

//api
import {
  getPerformanceProfile,
  createFeedback,
  getPerformanceCalendar,
  createAuditionVideo,
} from "../../../../api/auditions";

//locale
import en from "../../../../locale/en";

class PartakerDetail extends Component {
  state = {
    user_id: this.props.navigation.getParam("user_id", ""),
    audition_id: this.props.navigation.getParam("audition_id", ""),
    slot_id: this.props.navigation.getParam("slot_id", ""),
    user_image: this.props.navigation.getParam("image", ""),
    auditionName: this.props.navigation.getParam("auditionName", ""),
    loading: false,
    feedbackEmoji: null,
    callback: null,
    workOn: "",
    partakerDetailModal: false,
    partakerDetailModalSectionSelected: "Info",
    video: null,
    modalData: [],
    name: "",
    stageName: "",
    city: "",
    loading: false,
    loadingButtonSend: false,
    loadingButtonVideo: false,
    dates: [],
  };

  componentDidMount() {
    this.fetchUser();
    this.fetchUserCalendar();
  }

  fetchUser = async () => {
    const { user_id, audition_id } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getPerformanceProfile(user_id, audition_id);
      const details = response.data.data.details;
      const name = `${details.first_name} ${details.last_name}`;
      const stageName = details.stage_name;
      const city = details.city;
      const modalData = parseDataModalPartaker(response.data.data);

      this.setState({
        modalData,
        name,
        stageName,
        city,
      });
      console.log("FetchUser SUCCESS =====>", response);
    } catch (error) {
      console.log("FetchUser ERROR =====>", error);
    }

    this.setState({ loading: false });
  };

  fetchUserCalendar = async () => {
    const { user_id } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getPerformanceCalendar(user_id);

      this.setState({
        dates: response.data.data,
      });
    } catch (error) {
      console.log(error);
    }

    this.setState({ loading: false });
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;

    navigation.goBack();
  };

  onPressRecordAudition = async () => {
    try {
      const response = await recordVideo();

      this.setState(
        {
          video: response,
        },
        () => this.uploadAuditionVideo()
      );
    } catch (error) {
      console.log(error);
    }
  };

  uploadAuditionVideo = async () => {
    const { video, name, audition_id, user_id, slot_id } = this.state;

    let path = video.uri;
    let metadata = {
      contentType: "video/quicktime",
    };

    if (Platform.OS === "android") {
      path = video.path;
      metadata["contentType"] = "video/mp4";
    }

    this.setState({ loadingButtonVideo: true });

    try {
      const url = await uploadAsset(
        `tablet/auditions/${audition_id}/videos`,
        path,
        name,
        metadata
      );
      const data = {
        url,
        audition: audition_id,
        performer: user_id,
        slot_id,
        name: name,
      };

      await createAuditionVideo(data);

      showMessage("success", "Record audition saved");
    } catch (error) {
      console.log(error);

      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }

    this.setState({ loadingButtonVideo: false });
  };

  handlePressSendAudition = async () => {
    const hasError = this.validateFeedback();

    if (hasError !== "") {
      return showMessage("danger", hasError);
    }

    const {
      feedbackEmoji,
      callback,
      workOn,
      audition_id,
      user_id,
      slot_id,
    } = this.state;
    const { user, navigation } = this.props;
    const currentUserId = user.id;

    this.setState({ loadingButtonSend: true });

    try {
      const data = {
        auditions: audition_id,
        user: user_id,
        evaluator: currentUserId,
        evaluation: feedbackEmoji,
        callback,
        work: workOn,
        favorite: false,
        slot_id,
      };

      await createFeedback(data);

      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "ContributorAuditionDetail",
            params: {
              audition_id,
            },
          }),
        ],
      });

      navigation.dispatch(resetAction);

      return showMessage("success", en.feedback_created);
    } catch (error) {
      console.log(error);

      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }

    this.setState({ loadingButtonSend: false });
  };

  validateFeedback = () => {
    const { feedbackEmoji, callback, workOn } = this.state;
    let errorMessage = "";

    if (feedbackEmoji === null) {
      errorMessage = en.require_feedback_emoji;
    } else if (callback === null) {
      errorMessage = en.require_callback;
    } else if (workOn === "") {
      errorMessage = en.require_workon;
    }

    return errorMessage;
  };

  handlePressEmoji = (feedbackEmoji) => {
    this.setState({ feedbackEmoji });
  };

  handlePressCallbackButton = (callback) => {
    this.setState({ callback });
  };

  handlePressWorkOnButton = (workOn) => {
    this.setState({ workOn });
  };

  handlePressPartakerSection = (section = null) => {
    const { partakerDetailModal } = this.state;

    this.setState({
      partakerDetailModal: !partakerDetailModal,
      partakerDetailModalSectionSelected: section,
    });
  };

  render() {
    const {
      feedbackEmoji,
      callback,
      workOn,
      partakerDetailModal,
      partakerDetailModalSectionSelected,
      modalData,
      name,
      stageName,
      city,
      user_image,
      loading,
      auditionName,
      loadingButtonSend,
      loadingButtonVideo,
      dates,
    } = this.state;
    let isManger = this.props.navigation.getParam("isManger", true);
    if (loading) {
      return <Loading />;
    }

    return (
      <Container style={styles.container}>
        <LightBar />
        <PartakerModal
          show={partakerDetailModal}
          scrollTo={partakerDetailModalSectionSelected}
          dismiss={() => this.handlePressPartakerSection(en.info_title)}
          data={modalData}
        />
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
          center={<HeaderText title={auditionName} />}
          right={
            <HeaderBorderedButton
              title={en.record_audition_button_text}
              onPress={() => this.onPressRecordAudition()}
              loading={loadingButtonVideo}
            />
          }
        />
        <View style={styles.content}>
          <View style={styles.contentLeft}>
            <PartakerCard
              name={name}
              company={stageName}
              country={city}
              image={user_image}
              isManger={isManger}
              onPressInfo={() => this.handlePressPartakerSection(en.info_title)}
              onPressCredits={() =>
                this.handlePressPartakerSection(en.credits_title)
              }
              onPressEducation={() =>
                this.handlePressPartakerSection(en.education_title)
              }
              onPressAppearance={() =>
                this.handlePressPartakerSection(en.appearance_title)
              }
            />
          </View>
          <View style={styles.contentRight}>
            <View style={styles.contributorContainer}>
              <View>
                <Text style={styles.contributorButtonText}>
                  {en.contributor_text}
                </Text>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.availabilityContainer}>
                <View style={styles.availabilityTitleContainer}>
                  <Text style={styles.availabilityTitle}>
                    {en.available_title}
                  </Text>
                </View>
                <Calendar
                  calendarWidth={styles.availabilityContainer.width}
                  dates={dates}
                />
              </View>
              <View style={styles.feedbackContainer}>
                <View style={styles.feedbackTitleContainer}>
                  <Text style={styles.feedbackTitle}>
                    {en.instant_feeback_title}
                  </Text>
                  <View style={styles.feedbackEmojisContainer}>
                    <EmojiButton
                      emoji={StarEmoji}
                      onPress={() => this.handlePressEmoji(1)}
                      selected={feedbackEmoji === 1}
                    />
                    <EmojiButton
                      emoji={SmileEmoji}
                      onPress={() => this.handlePressEmoji(2)}
                      selected={feedbackEmoji === 2}
                    />
                    <EmojiButton
                      emoji={GoodEmoji}
                      onPress={() => this.handlePressEmoji(3)}
                      selected={feedbackEmoji === 3}
                    />
                    <EmojiButton
                      emoji={DoubtEmoji}
                      onPress={() => this.handlePressEmoji(4)}
                      selected={feedbackEmoji === 4}
                    />
                    <EmojiButton
                      emoji={BadEmoji}
                      onPress={() => this.handlePressEmoji(5)}
                      selected={feedbackEmoji === 5}
                    />
                  </View>
                </View>
                <View style={styles.callbackContainer}>
                  <Text style={styles.callback}>{en.callback_title}</Text>
                  <View style={styles.callbackButtons}>
                    <FeedbackButton
                      buttonText={en.yes_button_title}
                      onPress={() => this.handlePressCallbackButton(true)}
                      selected={callback === true}
                    />
                    <FeedbackButton
                      buttonText={en.no_button_title}
                      onPress={() => this.handlePressCallbackButton(false)}
                      selected={callback === false}
                    />
                  </View>
                </View>
                <View style={styles.workOnContainer}>
                  <Text style={styles.workOn}>{en.work_on_title}</Text>
                  <View style={styles.workOnButtons}>
                    <FeedbackButton
                      buttonText={en.vocals_title}
                      onPress={() =>
                        this.handlePressWorkOnButton(en.vocals_title)
                      }
                      selected={workOn === en.vocals_title}
                    />
                    <FeedbackButton
                      buttonText={en.acting_title}
                      onPress={() =>
                        this.handlePressWorkOnButton(en.acting_title)
                      }
                      selected={workOn === en.acting_title}
                    />
                    <FeedbackButton
                      buttonText={en.dancing_title}
                      onPress={() =>
                        this.handlePressWorkOnButton(en.dancing_title)
                      }
                      selected={workOn === en.dancing_title}
                    />
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.sendContainer}>
              <GradientButton
                buttonText={en.send_button_title}
                containerStyle={styles.containerButtonSend}
                gradientStyle={styles.gradientButtonSend}
                textStyle={styles.buttonSendText}
                onPress={() => this.handlePressSendAudition()}
                loading={loadingButtonSend}
                disabled={loadingButtonVideo}
              />
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
});

export default connect(mapStateToProps)(PartakerDetail);
