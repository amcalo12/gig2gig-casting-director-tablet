import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  Animated,
  TouchableOpacity,
  Linking
} from "react-native";
import { Container, Content } from "native-base";
import moment from "moment";
import { connect } from "react-redux";

//custom
import styles from "./style";
import Header from "../../../components/commons/header";
import BackButton from "../../../components/commons/back_button";
import HeaderText from "../../../components/commons/header_text";
import HeaderButton from "../../../components/commons/header_button";
import iconSearch from "../../../assets/icons/search/search.png";
import Search from "../../../components/commons/search";
import Role from "../../../components/auditions/role";
import File from "../../../components/auditions/file";
import Partaker from "../../../components/auditions/partaker";
import FileMenu from "../../../components/auditions/file_menu";
import Loading from "../../../components/commons/loading";
import UnionTag from "../../../components/auditions/union_tag";
import ContractTag from "../../../components/auditions/contract_tag";
import ProductionTag from "../../../components/auditions/production_tag";
import RoleIcon from "../../../assets/icons/auditions/role-icon.png";
import shareContent from "../../../utils/share_content";

//api
import { getAudition, getAuditionAppointments } from "../../../api/auditions";

//locale
import en from "../../../locale/en";

const AnimatedView = Animated.View;

class AuditionsContributor extends Component {
  state = {
    audition_id: this.props.navigation.getParam("audition_id", ""),
    title: "",
    date: "",
    time: "",
    description: "",
    url: "",
    union: "",
    contract: "",
    production: [],
    companyName: "",
    appointmentsNumber: 0,
    roles: [],
    files: [],
    loading: false,
    search: "",
    showSearch: false,
    partakers: [],
    showOptions: false,
    fileSelected: {},
    optionsPosition: 0,
    viewSplit: false,
    viewSplitAnimatedValue: new Animated.Value(0),
    status: 0,
    appointment_id: 0
  };

  componentDidMount() {
    this.fetchAudition();
  }

  fetchAudition = async () => {
    const { audition_id } = this.state;

    this.setState({ loading: true });

    try {
      const response = await getAudition(audition_id);
      const data = response.data.data;
      console.log(" AUDITION DETAIL ", data)
      this.setState({
        title: data.title,
        date: moment(data.date).format("LL"),
        time:
          data.time != null && data.time != undefined && data.time != ""
            ? moment(data.time, "HH:mm:ss").format("hh:mm A")
            : "",
        companyName: data.agency,
        appointmentsNumber: data.apointment.general.slots,
        union: data.union,
        contract: data.contract,
        production: data.production,
        description: data.description,
        url: data.url,
        roles: data.roles,
        files: data.media,
        status: data.status,
        appointment_id: data.appointment_id
      }, () => {
        this.fetchPartakers();
      });
    } catch (error) {
      console.log(error);
    }

    this.setState({ loading: false });
  };

  fetchPartakers = async () => {
    const { audition_id } = this.state;

    this.setState({ loading: true });

    try {
      // const response = await getAuditionAppointments(audition_id);
      const response = await getAuditionAppointments(this.state.appointment_id);

      this.setState({
        partakers: response.data.data
      });
    } catch (error) {
      console.log(error);
    }

    this.setState({ loading: false });
  };

  handleSetRef = (file, index) => {
    this[`file_${index.id}`] = file;
  };

  handleGetMeasure = async file => {
    await new Promise((resolve, reject) => {
      const refName = `file_${file.id}`;
      if (refName in this) {
        this[refName].measure((a, b, width, height, px, py) => {
          this.setState(
            {
              optionsPosition: py,
              fileSelected: file
            },
            () => resolve(true)
          );
        });
      } else {
        this.setState(
          {
            optionsPosition: 0,
            fileSelected: file
          },
          () => resolve(false)
        );
      }
    });
  };

  onPressFileMore = async file => {
    try {
      const { showOptions } = this.state;

      await this.handleGetMeasure(file);

      this.setState({
        showOptions: !showOptions
      });
    } catch (error) { }
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;

    navigation.goBack(null);
  };

  handlePressShowSearch = () => {
    this.setState({
      showSearch: true
    });
  };

  onChangeSearch = search => {
    this.setState({ search });
  };

  onPressPartaker = user => {
    const { navigation } = this.props;
    const { audition_id, title } = this.state;

    navigation.navigate("ContributorPartakerDetail", {
      user_id: user.user_id,
      image: user.image,
      audition_id,
      auditionName: title,
      slot_id: user.slot_id
    });
  };

  handleAnimatedSplit = () => {
    const { viewSplit, viewSplitAnimatedValue } = this.state;
    const toValue = viewSplit ? 0 : 1;

    Animated.timing(viewSplitAnimatedValue, {
      toValue: toValue,
      duration: 300
    }).start(() => {
      this.setState({
        viewSplit: !viewSplit
      });
    });
  };

  handlePressUrl = () => {
    const { url } = this.state;

    return Linking.openURL(url);
  };

  shareAsset = async file => {
    try {
      await shareContent(`Open this link: \n ${file.url}`);
    } catch (error) {
      console.log(error);
    }
  };

  openAsset = file => {
    return Linking.openURL(file.url);
  };

  render() {
    const {
      showSearch,
      search,
      partakers,
      showOptions,
      optionsPosition,
      fileSelected,
      viewSplitAnimatedValue,
      loading,
      title,
      date,
      time,
      appointmentsNumber,
      union,
      contract,
      production,
      description,
      url,
      roles,
      files,
      status,
      companyName
    } = this.state;
    const partakersFilter = partakers.filter(partaker => {
      let name = partaker.name.toUpperCase();

      return name.match(new RegExp(search.toUpperCase(), "g"));
    });

    const widthFirstContainer = viewSplitAnimatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [
        styles.firstSectionAnimated.width,
        styles.firstSectionAnimated.width + styles.secondSection.width
      ]
    });

    if (loading) {
      return <Loading />;
    }

    return (
      <Container>
        <FileMenu
          show={showOptions}
          dismiss={() => this.onPressFileMore({})}
          posy={optionsPosition}
          showOptionShare
          share={() => this.shareAsset(fileSelected)}
          showOptionOpen
          openIn={() => this.openAsset(fileSelected)}
        >
          {"id" in fileSelected && (
            <File
              filename={fileSelected.name}
              fileType={fileSelected.type}
              onPressMore={() => this.onPressFileMore({})}
            />
          )}
        </FileMenu>
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
          center={<HeaderText title={title} />}
          right={
            showSearch ? (
              <Search
                placeholder={en.search_audition_text}
                value={search}
                onChange={this.onChangeSearch}
              />
            ) : (
                <HeaderButton
                  icon={iconSearch}
                  onPress={() => this.handlePressShowSearch()}
                  containerStyle={styles.searchIcon}
                />
              )
          }
        />
        <View style={styles.wrapperContent}>
          {status === 0 && (
            <AnimatedView
              style={[
                styles.firstSectionAnimated,
                {
                  width: widthFirstContainer
                }
              ]}
            >
              <View style={styles.noAuditions}>
                <Text style={styles.noAuditionsText}>{en.checkin_no_open}</Text>
              </View>
            </AnimatedView>
          )}
          {status === 1 && (
            <AnimatedView
              style={[
                styles.firstSectionAnimated,
                {
                  width: widthFirstContainer
                }
              ]}
            >
              <Content style={styles.firstSection}>
                <View style={styles.partakerContainer}>
                  {partakersFilter.length > 0 &&
                    partakersFilter.map((partaker, index) => {
                      {
                        console.log("DATE===>", this.state.date);
                      }
                      {
                        console.log("TIME===>", this.state.time);
                      }
                      return (
                        <Partaker
                          key={index}
                          name={partaker.name}
                          image={partaker.image}
                          time={partaker.time}
                          favorite={partaker.favorite === 1 ? true : false}
                          onPress={() => this.onPressPartaker(partaker)}
                        />
                      );
                    })}
                  {partakersFilter.length === 0 && (
                    <Text style={styles.emptyText}>{en.no_performances}</Text>
                  )}
                </View>
              </Content>
            </AnimatedView>
          )}
          {status === 2 && (
            <AnimatedView
              style={[
                styles.firstSectionAnimated,
                {
                  width: widthFirstContainer
                }
              ]}
            >
              <View style={styles.noAuditions}>
                <Text style={styles.noAuditionsText}>
                  {en.checkin_was_closed}
                </Text>
              </View>
            </AnimatedView>
          )}
          <TouchableOpacity
            style={styles.spiltContainer}
            onPress={this.handleAnimatedSplit}
          >
            <View style={styles.spiltIcon} />
          </TouchableOpacity>
          <View style={styles.secondSection}>
            <View style={styles.infoContainer}>
              <Text style={styles.infoTitle}>{en.info_title}</Text>
            </View>
            <Content style={styles.contentRight}>
              <View style={styles.auditionInfoContainer}>
                <Text style={styles.auditionName}>{title}</Text>
                <View style={styles.auditionDateContainer}>
                  <Text style={styles.auditionDate}>{date}</Text>
                  <Text style={styles.auditionDate}>{time}</Text>
                </View>
                <Text style={styles.auditionCompany}>{companyName}</Text>
              </View>
              <View style={styles.appointmentsContainer}>
                <Text style={styles.appointments}>
                  {`${appointmentsNumber} ${en.appointments}`}
                </Text>
              </View>
              <ScrollView
                style={styles.tagsContainer}
                horizontal
                showsHorizontalScrollIndicator={false}
                bounces={false}
              >
                <UnionTag
                  name={union}
                  selected
                  containerSelectedStyle={styles.tagPurple}
                  disabled
                />
                <ContractTag
                  name={contract}
                  selected
                  containerSelectedStyle={styles.tagYellow}
                  disabled
                />
                {production.length > 0 &&
                  production.map((tag, index) => {
                    return (
                      <ProductionTag
                        key={index}
                        name={tag}
                        selected
                        containerSelectedStyle={styles.tagRed}
                        disabled
                      />
                    );
                  })}
              </ScrollView>
              <View style={styles.auditionDescriptionContainer}>
                <Text style={styles.auditionDescription}>{description}</Text>
              </View>
              <TouchableOpacity
                style={styles.auditionUrlContainer}
                onPress={() => this.handlePressUrl()}
              >
                <Text style={styles.auditionUrl}>{url}</Text>
              </TouchableOpacity>
              <ScrollView
                horizontal
                bounces={false}
                showsHorizontalScrollIndicator={false}
                style={styles.rolesContainer}
              >
                {roles.length > 0 &&
                  roles.map((role, index) => {
                    return (
                      <Role
                        key={index}
                        text={role.name}
                        icon={role.image ? { uri: role.image.url } : RoleIcon}
                        disabled
                      />
                    );
                  })}
                {roles.length === 0 && (
                  <Text style={styles.emptyText}>{en.no_roles}</Text>
                )}
              </ScrollView>
              <View style={styles.resourcesContainer}>
                {files.length > 0 &&
                  files.map((file, index) => {
                    return (
                      <File
                        key={index}
                        refs={fileRef => this.handleSetRef(fileRef, file)}
                        filename={file.name}
                        fileType={file.type}
                        onPressMore={() => this.onPressFileMore(file)}
                      />
                    );
                  })}
                {files.length === 0 && (
                  <Text style={styles.emptyText}>{en.no_files}</Text>
                )}
              </View>
            </Content>
          </View>
        </View>
      </Container>
    );
  }
}

export default connect(null)(AuditionsContributor);
