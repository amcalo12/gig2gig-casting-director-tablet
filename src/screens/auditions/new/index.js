import React, { Component, Fragment } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  Keyboard,
  Linking,
} from "react-native";
import { Container, Content } from "native-base";
import { connect } from "react-redux";
import moment from "moment";

//custom
import styles from "./style";
import LightBar from "../../../components/commons/light_bar";
import Header from "../../../components/commons/header";
import ButtonMenu from "../../../components/commons/button_menu";
import HeaderText from "../../../components/commons/header_text";
import HeaderBorderedButton from "../../../components/commons/header_bordered_button";
import InvitationButton from "../../../components/auditions/invitation_button";
import BorderedInput from "../../../components/auditions/bordered_input";
import { BorderInputSelect } from "../../../components/auditions/bordered_input_v2";
import PicIcon from "../../../assets/icons/auditions/cover-icon.png";
import BorderedDatepicker from "../../../components/auditions/bordered_datepicker";
import BorderedTimepicker from "../../../components/auditions/bordered_timepicker";
import UnionTag from "../../../components/auditions/union_tag";
import ContractTag from "../../../components/auditions/contract_tag";
import ProductionTag from "../../../components/auditions/production_tag";
import Role from "../../../components/auditions/role";
import File from "../../../components/auditions/file";
import AppointmentManagerModal from "../appointment_management_modal/";
import AddInvitationModal from "../../../components/auditions/add_invitation_modal";
import AddRoleModal from "../../../components/auditions/add_role_modal";
import EditRoleModal from "../../../components/auditions/edit_role_modal";
import FileMenu from "../../../components/auditions/file_menu";
import getFile, { getFileMultiple } from "../../../utils/get_file";
import LocationButton from "../../../components/auditions/location_button";
import LocationModal from "../location_modal";
import getImage from "../../../utils/get_image";
import showMessage from "../../../utils/show_message";
import validateAuditionFields from "../../../utils/auditions/validate_audition";
import parseError from "../../../utils/parse_error";
import parseFormData from "../../../utils/auditions/parse_form_data";
import showAlert from "../../../utils/show_alert";
import validateEmail from "../../../utils/validate_email";
import BlockLoading from "../../../components/auditions/block_loading";

//reset actions
import { resetHomeAction } from "../../../utils/reset_actions/auditions";

//api
import { createAudition } from "../../../api/auditions";

//locale
import en from "../../../locale/en";
import isUrl from "../../../utils/validate_url";

//Images
import Online from "../../../assets/icons/auditions/online.png";
import Ofline from "../../../assets/icons/auditions/offline.png";
import { phoneValidation } from "../../../utils/phoneValidator/phoneValidator";
import ImagePicker from "react-native-image-crop-picker";
import DocumentSelectModal from "../../../components/auditions/DocumentSelectModal/DocumentSelectModal";
import AddDocumentLink from "../../../components/auditions/AddDocumentLink/AddDocumentLink";
import { resize, NEXABOLD, NEXALIGHT } from "../../../assets/styles";
import { general_styles } from "../../../utils/general_style";
//assets
import picker_downflag from "../../../assets/icons/select_radius/dropdown.png";
import ModalDropdown from "react-native-modal-dropdown";
import Plus from "../../../assets/icons/auditions/icon-plus.png";
import { getThumbnail } from "../../../utils/constant";
import RenameModal from "../../../components/auditions/rename_file";
import uploadFiles from "../../../utils/auditions/upload_documents";
import colors from "../../../utils/colors";
import SwitchToggle from "react-native-switch-toggle";
import FileViewer from "react-native-file-viewer";

const maxLength = 100;
class NewAudition extends Component {
  constructor(props) {
    super(props);
    this.groupArray = [];
    for (var i = 0; i < 50; i++) {
      let value = i + 1;
      data = {
        type: value,
        value,
      };
      this.groupArray[this.groupArray.length] = data;
    }
    this.state = {
      title: "",
      location: null,
      description: "",
      contractDateStart: null,
      contractDateEnd: null,
      cover: null,
      rehearsalDateStart: null,
      rehearsalDateEnd: null,
      url: "",
      date: null,
      time: null,
      roles: [],
      files: [],
      union: "",
      contract: "",
      production: [],
      unionTags: ["UNION", "NONUNION"],
      contractTags: ["PAID", "UNPAID", "ACADEMIC"],
      productionTags: [
        "THEATER",
        "FILM",
        "VOICEOVER",
        "COMMERCIALS",
        "PERFORMING ARTS",
        "MODELING",
        "TV & VIDEO",
      ],
      invitations: [],
      roleName: "",
      roleDescription: "",
      roleImage: null,
      roleImageName: "",
      optionsPosition: { py: 0, px: 0 },
      fileSelected: {},
      showModalAddRole: false,
      editRoleModal: false,
      showAppointmentsModal: false,
      showInvitationModal: false,
      shouldBeInvite: false,
      newInvitationEmail: "",
      showFileMenu: false,
      roleSelected: null,
      showLocationModal: false,
      appointment: {},
      blockLoading: false,
      personalInformation: "",
      additionalInformation: "",
      phone: "",
      email: "",
      other: "",
      online: false,
      isDocumentVisible: false,
      isLinkVisible: false,
      refreshCount: 0,
      fileName: "",
      cover_name: "",
      isRename: false,
      isVisible: false,
      option: [
        {
          type: 1,
          value: "Round 1",
          date: "",
          time: "",
          location: null,
          appointment: null,
        },
        {
          type: 2,
          value: "Open New Round",
        },
      ],
      groupOptions: this.groupArray,
      groupSize: null,
      endDate: null,
      isSocialDistancing: false,
    };
    this.selectedRound = this.state.option[0];
    this.selectedIndex = 0;
  }

  componentDidMount() {
    // setTimeout(() => {
    //   console.log('this.myComponent` :', this.myComponent);
    //   this.myComponent.measure((fx, fy, width, height, px, py) => {
    //     console.log('Component width is: ' + width)
    //     console.log('Component height is: ' + height)
    //     console.log('X offset to frame: ' + fx)
    //     console.log('Y offset to frame: ' + fy)
    //     console.log('X offset to page: ' + px)
    //     console.log('Y offset to page: ' + py)
    //   })
    // }, 3000);
  }

  handlePressOpenDrawer = () => {
    const { navigation } = this.props;

    navigation.openDrawer();
  };

  handleDeleteFile = (file) => {
    let { files } = this.state;
    const index = files.indexOf(file);
    files.splice(index, 1);
    this.onPressFileMore({});
  };

  handleValidateURL = async () => {
    const { url } = this.state;
    return new Promise((resolve, reject) => {
      if (!isUrl(url)) {
        reject({ response: { data: { data: "Enter a valid url" } } });
      }
      resolve();
    });
  };

  handlePressCreateAudition = async () => {
    // let data = {
    //   media: []
    // }
    // this.state.files.map(file => {
    //   let row = {
    //     url: file.url,
    //     type: file.type,
    //     metadata: file.metadata,
    //     name: file.filename,
    //     share: file.shareable,
    //     thumbnail: file.thumbnail
    //   };

    //   data["media"].push(row);
    // });

    // const media = await uploadFiles(data.media);
    // console.log('media uploaded url :', media);

    const { navigation } = this.props;
    const hasError = validateAuditionFields(this.state);

    if (hasError != "") {
      return showMessage("danger", hasError);
    } else {
      if (this.state.blockLoading) return "";
      this.setState({ blockLoading: true });
      try {
        const formData = parseFormData(this.state);
        console.log("formData Request:", formData);
        // return
        // await this.handleValidateURL();
        await createAudition(formData);
        this.setState({ blockLoading: false });

        navigation.dispatch(resetHomeAction);
      } catch (error) {
        console.log({ error });
        this.setState({ blockLoading: false });
        const parsedError = parseError(error);

        showMessage("danger", parsedError);
      }
    }
  };

  handlePressAddInvitation = () => {
    const { showInvitationModal } = this.state;

    this.setState({
      showInvitationModal: !showInvitationModal,
    });
  };

  handleDismissInvitationModal = () => {
    this.setState(
      {
        newInvitationEmail: "",
        shouldBeInvite: false,
      },
      () => this.handlePressAddInvitation()
    );
  };

  onChangeTitle = (title) => {
    if (title.length <= 50) {
      this.setState({ title });
    }
  };

  onChangeLocation = (location) => {
    this.setState({ location });
  };

  onChangeDescription = (description) => {
    if (description.length <= 1000) {
      this.setState({ description });
    }
  };

  onChangePersonnelInformation = (personalInformation) => {
    this.setState({ personalInformation });
  };

  onChangeAdditionalInformation = (additionalInformation) => {
    this.setState({ additionalInformation });
  };

  onChangePhone = (phone) => {
    if (phone.length < 14) {
      let customPhone = String(phone).replace(/[^0-9]/g, "");
      let formatPhone = phoneValidation(customPhone);
      this.setState({ phone: formatPhone });
    }
  };

  onChangeEmail = (email) => {
    this.setState({ email });
  };
  onChangeOther = (other) => {
    this.setState({ other });
  };

  handlePressCoverPhoto = async () => {
    try {
      const response = await getImage();
      console.log("response :", response);
      ImagePicker.openCropper({
        path: response.uri,
        width: 400,
        height: 400,
      })
        .then(async (data) => {
          console.log("Crop image", data);
          let newData = response;
          newData["uri"] = data.path;

          let thumbnailImage = await getThumbnail(data.path);
          console.log("thumbnail :", thumbnailImage);

          this.setState({
            cover: { uri: data.path, thumbnail: thumbnailImage.uri },
            isVisible: false,
            isRename: false,
            fileName: "",
            cover_name: response.fileName,
          });
          // this.setState({
          //   cover: { uri: response.uri },
          // });
        })
        .catch(async (error) => {
          let thumbnailImage = await getThumbnail(data.path);
          console.log("thumbnail :", thumbnailImage);
          console.log("Crop image error", error);
          this.setState({
            cover: { uri: response.uri, thumbnail: thumbnailImage.uri },
            isVisible: false,
            isRename: false,
            fileName: "",
            cover_name: response.fileName,
          });
        });
      // this.setState({ cover: { uri: response.uri } });
    } catch (error) {
      console.log(error);
    }
  };

  onCloseClick = () => {
    this.setState({ isVisible: false });
  };

  onSubmitClick = () => {
    if (this.state.isRename) {
      let index = this.state.files.findIndex(
        (x) => x.url == this.state.fileSelected.url
      );

      this.state.files[index].filename = this.state.fileName;
      this.state.fileName = "";
    } else {
      this.setState({ cover_name: this.state.fileName });
    }
    this.setState({ isVisible: false });
  };

  onChangeText = (fileName) => {
    this.setState({ fileName });
  };

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    );
  };

  onChangeContractDateStart = (contractDateStart) => {
    if (this.state.contractDateEnd !== null) {
      const ifAfter = moment(contractDateStart).isAfter(
        moment(this.state.contractDateEnd).format("LL")
      );
      if (ifAfter) {
        showMessage(
          "danger",
          "The start time cannot be later than the end time."
        );
      } else {
        this.setState({
          contractDateStart,
          rehearsalDateStart: null,
          rehearsalDateEnd: null,
        });
      }
    } else {
      this.setState({
        contractDateStart,
        rehearsalDateStart: null,
        rehearsalDateEnd: null,
      });
    }
  };

  onChangeContractDateEnd = (contractDateEnd) => {
    this.setState({ contractDateEnd });
  };

  onChangeRehearsalDateStart = (rehearsalDateStart) => {
    console.log("rehearsalDateStart", rehearsalDateStart);
    if (this.state.rehearsalDateEnd !== null) {
      const ifAfter = moment(rehearsalDateStart).isAfter(
        moment(this.state.rehearsalDateEnd).format("LL")
      );
      if (ifAfter) {
        showMessage(
          "danger",
          "The start time cannot be later than the end time."
        );
      } else {
        this.setState({ rehearsalDateStart });
      }
    } else {
      this.setState({ rehearsalDateStart });
    }
  };

  onChangeRehearsalDateEnd = (rehearsalDateEnd) => {
    this.setState({ rehearsalDateEnd });
  };

  onChangeUrl = (url) => {
    this.setState({ url });
  };

  onChangeDate = (date) => {
    this.state.option[this.selectedIndex].date = date;
    this.setState({ option: this.state.option });
    // this.setState({ date });
  };

  onEndChangeDate = (endDate) => {
    // this.state.option[this.selectedIndex].date = date
    // this.setState({ option: this.state.option });
    this.setState({ endDate });
  };

  onChangeTime = (time) => {
    var coeff = 1000 * 60 * 10;
    var date = new Date("02/02/2019 " + time);
    var rounded = new Date(Math.round(date.getTime() / coeff) * coeff);
    console.log("rounded :", rounded);
    let newTime = moment(rounded).format("h:mm:ss A");
    console.log("rounded 111:", newTime);
    console.log("rounded 222:", moment(newTime, "h:mm:ss A").format("HH:mm"));
    this.state.option[this.selectedIndex].time = newTime;
    this.setState({ option: this.state.option });
    // this.setState({ time: rounded });
  };

  handlePressManageAppointments = () => {
    const { showAppointmentsModal } = this.state;

    this.setState({
      showAppointmentsModal: !showAppointmentsModal,
    });
  };

  handlePressAddRole = () => {
    const { showModalAddRole } = this.state;

    this.setState({
      showModalAddRole: !showModalAddRole,
      roleName: "",
      roleDescription: "",
      roleImage: null,
      roleImageName: "",
    });
  };

  // handlePressUploadDocument = async () => {
  //   try {
  //     let { files } = this.state;
  //     const file = await getFile();
  //     let reference = [...files];
  //     file.shareable = "yes";
  //     reference.push(file);
  //     this.setState({ files: reference });
  //   } catch (error) {
  //     if (typeof error === "object" && "errorCode" in error) {
  //       showMessage("danger", error.string);
  //     }
  //     console.log(error);
  //   }
  // };

  onUploadDocument = () => {
    this.setState({ isDocumentVisible: true });
  };

  handlePressUploadDocument = async () => {
    try {
      let { files } = this.state;
      const file = await getFileMultiple();
      // FileViewer.open(file.uri)
      //   .then(() => {
      //     // success
      //   })
      //   .catch((error) => {
      //     console.log("file open error===" + error);
      //   });
      let reference = [...files];
      console.log("reference :", reference);
      // file.shareable = "yes";
      // reference.push(file);
      this.setState(
        { files: [...files, ...file], isDocumentVisible: false },
        () => {
          console.log("state ======>>>>:", this.state);
        }
      );
    } catch (error) {
      if (typeof error === "object" && "errorCode" in error) {
        showMessage("danger", error.string);
      }
      console.log(error);
    }
  };

  onCloseModal = () => {
    this.setState({ isDocumentVisible: false, isLinkVisible: false });
  };

  onLinkAddClick = () => {
    this.setState({ isDocumentVisible: false, isLinkVisible: true });
    // this.props.navigation.navigate("AddDocumentLink")
  };

  onFileUpload = async () => {
    this.setState({ isDocumentVisible: false }, () => {
      console.log("onLinkAddClick :");
      setTimeout(() => {
        this.handlePressUploadDocument();
      }, 1000);
    });
  };

  onLinkDone = (data) => {
    console.log("data :", data);
    let { files } = this.state;
    this.setState({ files: [...files, ...data] });
  };

  showDocumentSelectModal = () => {
    return (
      <DocumentSelectModal
        isVisible={this.state.isDocumentVisible}
        title={"Select Document Type"}
        onCloseClick={this.onCloseModal}
        onFileUpload={this.onFileUpload}
        onAddLink={this.onLinkAddClick}
      />
    );
  };

  showAddDocumentLink = () => {
    return (
      <AddDocumentLink
        isVisible={this.state.isLinkVisible}
        title={"Add Document Link"}
        onCloseClick={this.onCloseModal}
        onDoneClick={this.onLinkDone}
      />
    );
  };

  handlePressShouldAddInvitation = () => {
    const { shouldBeInvite } = this.state;

    this.setState({
      shouldBeInvite: !shouldBeInvite,
    });
  };

  handleDeleteInvitation = (invitation) => {
    let { invitations } = this.state;
    const index = invitations.indexOf(invitation);

    invitations.splice(index, 1);

    this.setState({ invitations });
  };

  handleChangeEmailInvitation = (value) => {
    this.setState({
      newInvitationEmail: value,
    });
  };

  handleChangeShouldBeDelete = (index) => {
    let { invitations } = this.state;

    invitations[index].shouldBeDelete = !invitations[index].shouldBeDelete;

    this.setState({ invitations });
  };

  handleSendInvitation = async () => {
    let { invitations, newInvitationEmail } = this.state;

    if (newInvitationEmail === "") {
      return showAlert(en.require_email);
    }

    if (!validateEmail(newInvitationEmail)) {
      return showAlert(en.valid_email);
    }

    try {
      await new Promise((resolve) => {
        invitations.push({
          email: newInvitationEmail,
          status: 0,
        });

        this.setState(
          {
            invitations,
            newInvitationEmail: "",
          },
          () => resolve(true)
        );
      });

      this.handlePressShouldAddInvitation();
    } catch (error) {
      console.log(error);
    }
  };

  handlePressUnionTag = (union) => {
    this.setState({ union });
  };

  handlePressContractTag = (contract) => {
    this.setState({ contract });
  };

  handlePressProductionTag = (newTag) => {
    let { production } = this.state;

    if (production.includes(newTag)) {
      let index = production.indexOf(newTag);

      if (index !== -1) {
        production.splice(index, 1);
      }

      return this.setState({ production });
    }

    return this.setState({
      production: [...production, newTag],
    });
  };

  handleSetRef = (file, index) => {
    this[`file_${index}`] = file;
  };

  handleGetMeasure = async (file, index) => {
    await new Promise((resolve, reject) => {
      const refName = `file_${index}`;
      if (refName in this) {
        this[refName].measure((a, b, width, height, px, py) => {
          this.setState(
            {
              optionsPosition: { py, px },
              fileSelected: file,
            },
            () => resolve(true)
          );
        });
      } else {
        this.setState(
          {
            optionsPosition: { py: 0, px: 0 },
            fileSelected: file,
          },
          () => resolve(false)
        );
      }
    });
  };

  onPressFileMore = async (file, index) => {
    try {
      const { showFileMenu } = this.state;

      await this.handleGetMeasure(file, index);

      this.setState({
        showFileMenu: !showFileMenu,
      });
    } catch (error) { }
  };

  onPressRole = (id = null) => {
    const { editRoleModal, roles } = this.state;

    const role = roles[id];

    if (role) {
      this.setState({
        editRoleModal: !editRoleModal,
        roleSelected: id,
        roleName: role.name,
        roleDescription: role.description,
        roleImage: role.cover,
        roleImageName: role.cover_name,
      });
    }

    return null;
  };

  onPressCloseEditModal = () => {
    const { editRoleModal } = this.state;

    this.setState({
      editRoleModal: !editRoleModal,
      roleName: "",
      roleDescription: "",
      roleImage: null,
      roleSelected: null,
    });
  };

  handleEditRole = () => {
    this.setState({
      editRoleModal: false,
      showModalAddRole: true,
    });
  };

  onChangeRoleName = (roleName) => {
    if (roleName.length <= 35) {
      this.setState({ roleName });
    }
  };

  onChangeRoleImageName = (roleImageName) => {
    this.setState({ roleImageName });
  };

  onChangeRoleDescription = (roleDescription) => {
    if (roleDescription.length <= 150) {
      this.setState({ roleDescription });
    }
  };

  handlePressUploadRoleImage = async () => {
    try {
      const response = await getImage();
      console.log("response :", response);
      let thumbnailImage = await getThumbnail(response.uri);
      console.log("thumbnail :", thumbnailImage);

      this.setState(
        {
          roleImage: { uri: response.uri, thumbnail: thumbnailImage.uri },
          roleImageName: response.fileName,
        },
        () => {
          console.log("STATE AFTER ROLE ADD :", this.state);
        }
      );
    } catch (error) {
      console.log(error);
    }
  };

  handleAddRole = async () => {
    let { showModalAddRole } = this.state;

    await this.handleValidateRolAndDescription();
    await this.handleValidateDescription();
    this.pushRole();

    this.setState({
      roleSelected: null,
      roleDescription: "",
      roleName: "",
      roleImage: null,
      roleImageName: "",
      showModalAddRole: !showModalAddRole,
    });
    // if (this.validateRole()) {

    // }
  };

  handleDeleteRole = (index = null) => {
    const { editRoleModal } = this.state;
    let { roles } = this.state;

    if (index !== null) {
      roles.splice(index, 1);
    }

    this.setState({
      editRoleModal: !editRoleModal,
      roleName: "",
      roleDescription: "",
      roleImage: null,
      roleSelected: null,
      roles,
    });
  };

  validateRole = () => {
    const { roleName, roleDescription, roleImage } = this.state;

    // if (roleImage === null) {
    //   showAlert(en.require_role_image);

    //   return false;
    // }

    if (roleName === "") {
      showAlert(en.require_role_name);

      return false;
    }

    if (roleDescription === "") {
      showAlert(en.require_role_description);

      return false;
    }

    return true;
  };

  handleValidateRolAndDescription = async () => {
    const { roleName, roleDescription } = this.state;
    return new Promise((resolve, reject) => {
      if (String(roleName).trim() === "") {
        // reject(showAlert(en.require_role_name))
        reject({ response: { data: { data: en.require_role_name } } });
      }

      resolve();
    });
  };

  handleValidateDescription = async () => {
    const { roleDescription } = this.state;

    return new Promise((resolve, reject) => {
      if (String(roleDescription).trim() === "") {
        // reject(showAlert(en.require_role_description))
        reject({ response: { data: { data: en.require_role_description } } });
      }

      resolve();
    });
  };

  handleAddAnotherRole = async () => {
    await this.handleValidateRolAndDescription();
    await this.handleValidateDescription();
    this.pushRole();

    this.setState({
      roleSelected: null,
      roleDescription: "",
      roleName: "",
      roleImage: null,
      roleImageName: "",
    });

    // if (this.validateRole()) {
    //   this.pushRole();

    //   this.setState({
    //     roleSelected: null,
    //     roleDescription: '',
    //     roleName: '',
    //     roleImage: null
    //   });
    // }
  };

  pushRole = () => {
    let {
      roleDescription,
      roleName,
      roles,
      roleSelected,
      roleImage,
      roleImageName,
    } = this.state;

    const role = {
      name: roleName,
      description: roleDescription,
      cover: roleImage,
      cover_name: roleImageName,
    };

    if (roleSelected === null) {
      roles.push(role);
    } else {
      roles[roleSelected] = role;
    }

    this.setState({ roles });
  };

  handlePressLocation = () => {
    const { showLocationModal } = this.state;

    this.setState({
      showLocationModal: !showLocationModal,
    });
  };

  handlePressSetLocation = (location) => {
    const { showLocationModal } = this.state;

    this.state.option[this.selectedIndex].location = location;

    this.setState({
      option: this.state.option,
      showLocationModal: !showLocationModal,
    });
    // this.setState({
    //   location,
    //   showLocationModal: !showLocationModal
    // });
  };

  handlePressDoneManageAppointments = (appointment) => {
    this.state.option[this.selectedIndex].appointment = appointment;
    this.setState({ option: this.state.option });
    // this.setState({ appointment });
  };

  handleFileShare = (handle) => {
    let value = handle ? "yes" : "no";
    let { fileSelected, files } = this.state;
    const file = { ...fileSelected };
    const actualFiles = [...files];
    let index = -1;
    for (let i = 0; i < files.length; i++) {
      if (files[i].url == fileSelected.url) {
        index = i;
      }
    }
    actualFiles[index].shareable = value;
    file.shareable = value;
    this.setState({ fileSelected: file, files: actualFiles });
  };

  onOnlinePress = () => {
    this.setState((prevState) => {
      return { online: !prevState.online };
    });
  };

  onItemClick = (data, index) => {
    console.log("ItemClicked :", index);
    if (data.type == 2) {
      let round = parseInt(index) + 1;
      this.state.option.splice(index, 0, {
        type: 1,
        value: "Round " + round,
        date: "",
        time: "",
        location: null,
        appointment: null,
      });
      this.selectedRound = this.state.option[index];
      this.selectedIndex = index;
      this.setState({ option: this.state.option });
    } else {
      this.selectedRound = data;
      this.selectedIndex = index;
      this.setState({ option: this.state.option });
    }
  };

  renderMessageRow = (rowData, index, highlighted) => {
    return (
      <View
        style={{
          margin: 5,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {rowData.type == 2 ? (
          <Image
            resizeMode="contain"
            source={Plus}
            style={{ marginRight: 5 }}
          />
        ) : null}
        <Text
          style={{
            textAlign: "center",
            color: "#4d2545",
            fontFamily: NEXABOLD,
            fontSize: resize(16),
            fontWeight: "400",
          }}
        >
          {rowData.value}
        </Text>
      </View>
    );
  };

  renderGroupRow = (rowData, index, highlighted) => {
    return (
      <View
        style={{
          margin: 5,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Text
          style={{
            textAlign: "center",
            color: "#4d2545",
            fontFamily: NEXABOLD,
            fontSize: resize(16),
            fontWeight: "400",
          }}
        >
          {rowData.value}
        </Text>
      </View>
    );
  };

  onSwitchChange = () => {
    Keyboard.dismiss();
    if (this.state.isSocialDistancing) {
      this.state.groupSize = null;
    }
    this.setState({ isSocialDistancing: !this.state.isSocialDistancing });
  };

  render() {
    const {
      title,
      location,
      description,
      contractDateStart,
      contractDateEnd,
      rehearsalDateStart,
      rehearsalDateEnd,
      url,
      date,
      time,
      union,
      unionTags,
      contract,
      contractTags,
      production,
      productionTags,
      roles,
      files,
      showAppointmentsModal,
      showInvitationModal,
      shouldBeInvite,
      newInvitationEmail,
      invitations,
      showModalAddRole,
      editRoleModal,
      roleName,
      roleDescription,
      roleImage,
      roleImageName,
      roleSelected,
      showFileMenu,
      fileSelected,
      optionsPosition,
      showLocationModal,
      cover,
      blockLoading,
      additionalInformation,
      personalInformation,
      phone,
      email,
      other,
      online,
    } = this.state;

    return (
      <Container style={styles.container}>
        <LightBar />
        {this.showDocumentSelectModal()}
        {this.showAddDocumentLink()}
        {this.renderRenameModal()}
        <FileMenu
          show={showFileMenu}
          dismiss={() => this.onPressFileMore({})}
          posy={optionsPosition.py}
          posx={optionsPosition.px}
          showOptionDelete
          showShareable
          showOptionOpen
          rename={true}
          onRename={() => {
            console.log("fileSelected :", fileSelected);
            this.setState({
              isVisible: true,
              showFileMenu: false,
              isRename: true,
              fileName: fileSelected.filename,
            });
          }}
          isShareable={fileSelected.shareable === "yes"}
          handleFileShare={this.handleFileShare}
          openIn={() => {
            // this.setState({
            //   showFileMenu: false,
            // });
            console.log(JSON.stringify(fileSelected));
            if (fileSelected.type === 5) {
              Linking.openURL(fileSelected.url)
                .then(() => { })
                .catch((error) => { });
            } else {
              FileViewer.open(fileSelected.url)
                .then((data) => {
                  console.log('data :>> ', data);
                  // success
                })
                .catch((error) => {
                  console.log("file open error===" + error);
                });
            }
          }}
          delete={() => {
            // console.log(JSON.stringify(fileSelected));
            this.handleDeleteFile(fileSelected);
          }}
        >
          {"filename" in fileSelected && (
            <File
              activeOpacity={1}
              filename={fileSelected.filename}
              fileType={fileSelected.type}
              file={fileSelected}
              onPressMore={() => this.onPressFileMore({})}
            />
          )}
        </FileMenu>
        {/* add role modal */}
        <AddRoleModal
          done={this.handleAddRole}
          addAnother={this.handleAddAnotherRole}
          show={showModalAddRole}
          dismiss={this.handlePressAddRole}
          name={roleName}
          description={roleDescription}
          changeRoleName={this.onChangeRoleName}
          onChangeRoleImageName={this.onChangeRoleImageName}
          changeRoleDescription={this.onChangeRoleDescription}
          onPressTakeImage={() => this.handlePressUploadRoleImage()}
          roleImage={roleImage}
          roleImageName={roleImageName}
        />
        {/* edit role modal */}
        <EditRoleModal
          delete={() => this.handleDeleteRole(roleSelected)}
          name={roleName}
          edit={this.handleEditRole}
          description={roleDescription}
          show={editRoleModal}
          dismiss={() => this.onPressCloseEditModal()}
          cover={roleImage}
        />
        {console.log(
          "appointment :",
          this.state.option[this.selectedIndex].appointment != null
            ? this.state.option[this.selectedIndex].appointment
            : []
        )}
        {/* appointment modal */}
        <AppointmentManagerModal
          appointment={
            this.state.option[this.selectedIndex].appointment != null
              ? this.state.option[this.selectedIndex].appointment
              : []
          }
          show={showAppointmentsModal}
          dismiss={this.handlePressManageAppointments}
          done={this.handlePressDoneManageAppointments}
        />
        {/* modal invitation */}
        <AddInvitationModal
          invitations={invitations}
          show={showInvitationModal}
          shouldBeInvite={shouldBeInvite}
          newInvitationEmail={newInvitationEmail}
          sendInvitation={this.handleSendInvitation}
          dismiss={this.handleDismissInvitationModal}
          enableAddInvitation={this.handlePressShouldAddInvitation}
          onChangeEmail={this.handleChangeEmailInvitation}
          delete={this.handleDeleteInvitation}
        />
        {/* location modal */}
        <LocationModal
          show={showLocationModal}
          dismiss={this.handlePressLocation}
          agree={this.handlePressSetLocation}
          location={location}
        />
        {/* blog modal */}
        <BlockLoading show={blockLoading} />
        <Header
          left={<ButtonMenu onPress={() => this.handlePressOpenDrawer()} />}
          center={<HeaderText title={en.create_new_audition_title} />}
          right={
            <HeaderBorderedButton
              title={en.create_audtion_button_text}
              withIcon={false}
              onPress={() => this.handlePressCreateAudition()}
              textStyle={styles.headerButtonText}
            />
          }
        />
        <View style={styles.content}>
          <Content disableKBDismissScroll style={styles.paddingContent}>
            <InvitationButton
              onPress={() => this.handlePressAddInvitation()}
              title={en.add_invitation_button_text}
            />
            <View style={styles.row}>
              <BorderedInput
                placeholder={en.title_placeholder}
                value={title}
                onChange={this.onChangeTitle}
                containerStyle={styles.dateInputContainer2}
              />
              <BorderInputSelect
                placeholder={en.online_sub}
                value={title}
                onChange={this.onChangeTitle}
                containerStyle={styles.onlineContainer}
                withIcon
                icon={online ? Online : Ofline}
                onPress={this.onOnlinePress}
              />
            </View>
            {online ? (
              <View style={styles.row}>
                <BorderedDatepicker
                  mode={"datetime"}
                  format={"LL hh:mm a"}
                  placeholder={en.end_date_placeholder}
                  value={this.state.endDate}
                  onChange={this.onEndChangeDate}
                  containerStyle={styles.endDateInputContainer}
                  minDate={moment(new Date()).add(1, "day")}
                />
              </View>
            ) : null}
            {/* {!online ? (
              <View style={styles.row}>
                <BorderedDatepicker
                  placeholder={en.date_placeholder}
                  value={date}
                  onChange={this.onChangeDate}
                  containerStyle={styles.dateInputContainer}
                />
                <BorderedTimepicker
                  placeholder={en.time_placeholder}
                  dateA={date}
                  value={time}
                  onChange={this.onChangeTime}
                  containerStyle={styles.timeInputContainer}
                />
                <LocationButton
                  title={
                    location !== null
                      ? en.location_saved_placeholder
                      : en.location_placeholder
                  }
                  withIcon
                  onPress={() => this.handlePressLocation()}
                />
              </View>
            ) : null} */}
            <View style={styles.productionInformationTitle}>
              <Text style={styles.productionInformationText}>
                Production Information
              </Text>
            </View>
            <View style={styles.row}>
              <BorderedInput
                placeholder={en.description_placeholder}
                value={description}
                onChange={this.onChangeDescription}
                containerStyle={styles.descriptionInputContainer}
                style={styles.descriptionInput}
                maxLength={1000}
                multiline
              />
              <TouchableOpacity
                style={styles.coverContainer}
                onPress={() => this.handlePressCoverPhoto()}
              >
                {cover === null && (
                  <Fragment>
                    <View style={styles.coverPhotoContainer}>
                      <Text style={styles.coverPhoto}>
                        {en.cover_photo_placeholder}
                      </Text>
                    </View>
                    <View style={styles.picIconContainer}>
                      <Image source={PicIcon} style={styles.picIcon} />
                    </View>
                  </Fragment>
                )}
                {cover !== null && (
                  <Image source={cover} style={styles.coverImage} />
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.viewForPersonnelInformationContainer}>
              <BorderedInput
                placeholder={en.personnel_information_placeholder}
                value={personalInformation}
                onChange={this.onChangePersonnelInformation}
                containerStyle={styles.personnelInformationInputContainer}
                style={styles.descriptionInput}
                maxLength={1000}
                multiline
              />
              <View style={styles.viewForAdditionalInformationContainer}>
                <BorderedInput
                  placeholder={en.additional_information_placeholder}
                  value={additionalInformation}
                  onChange={this.onChangeAdditionalInformation}
                  containerStyle={styles.additionallInformationInputContainer}
                  style={styles.descriptionInput}
                  maxLength={1000}
                  multiline
                />
              </View>
              <View style={styles.contractInformationTitle}>
                <Text style={styles.contractInformationText}>
                  Contract Information
                </Text>
              </View>
            </View>
            <View style={styles.row}>
              <BorderedDatepicker
                placeholder={en.contract_date_start_placeholder}
                value={contractDateStart}
                onChange={this.onChangeContractDateStart}
                containerStyle={styles.contractDatesInputContainer}
              />
              <BorderedDatepicker
                placeholder={en.contract_date_end_placeholder}
                value={contractDateEnd}
                disable={contractDateStart === null}
                minDate={
                  contractDateStart !== null ? contractDateStart : new Date()
                }
                onChange={this.onChangeContractDateEnd}
                containerStyle={styles.contractDatesInputContainer}
              />
            </View>
            <View style={styles.row}>
              <BorderedDatepicker
                placeholder={en.rehearsal_date_start_placeholder}
                value={rehearsalDateStart}
                onChange={this.onChangeRehearsalDateStart}
                containerStyle={styles.contractDatesInputContainer}
                minDate={
                  contractDateStart !== null ? contractDateStart : new Date()
                }
              />
              <BorderedDatepicker
                placeholder={en.rehearsal_date_end_placeholder}
                value={rehearsalDateEnd}
                disable={rehearsalDateStart === null}
                minDate={
                  rehearsalDateStart !== null ? rehearsalDateStart : new Date()
                }
                onChange={this.onChangeRehearsalDateEnd}
                containerStyle={styles.contractDatesInputContainer}
              />
            </View>
            <View style={styles.contactInformationTitle}>
              <Text style={styles.contactInformationText}>
                Contact Information
              </Text>
            </View>
            <BorderedInput
              placeholder={en.audition_url_placeholder}
              value={url}
              onChange={this.onChangeUrl}
            />
            <View style={styles.row}>
              <BorderedInput
                placeholder={en.phone_placeholder}
                value={phone}
                onChange={this.onChangePhone}
                containerStyle={styles.phoneInputContainer}
                keyboardType="phone-pad"
              />
              <BorderedInput
                placeholder={en.email_placeholder}
                value={email}
                onChange={this.onChangeEmail}
                containerStyle={styles.emailInputContainer}
                keyboardType="email-address"
              />
              <BorderedInput
                placeholder={en.other_placeholder}
                value={other}
                onChange={this.onChangeOther}
                containerStyle={styles.otherInputContainer}
              />
            </View>
            {!online ? (
              <Text style={general_styles.title}>{"Rounds"}</Text>
            ) : null}
            {!online && this.state.option.length > 0 ? (
              <ModalDropdown
                options={this.state.option}
                onSelect={(index, option) => {
                  this.onItemClick(option, index);
                }}
                renderRow={this.renderMessageRow}
                renderSeparator={() => {
                  <View style={{ backgroundColor: "white" }} />;
                }}
                dropdownStyle={styles.dropDownStyle}
              // scrollEnabled={false}
              >
                <View
                  style={{
                    marginBottom: resize(10, "height"),
                    marginTop: resize(10, "height"),
                  }}
                >
                  {/* <Text style={general_styles.title}>{"Rounds"}</Text> */}
                  <View style={styles.roundContainer}>
                    <Text style={general_styles.title}>
                      {this.selectedRound.value}
                    </Text>
                    <Image source={picker_downflag} resizeMode="contain" />
                  </View>
                </View>
              </ModalDropdown>
            ) : null}

            {!online ? (
              <View style={styles.row}>
                <BorderedDatepicker
                  placeholder={en.date_placeholder}
                  value={this.selectedRound.date}
                  onChange={this.onChangeDate}
                  containerStyle={styles.dateInputContainer}
                />
                <BorderedTimepicker
                  placeholder={en.time_placeholder}
                  dateA={this.selectedRound.date}
                  value={this.selectedRound.time}
                  onChange={this.onChangeTime}
                  containerStyle={styles.timeInputContainer}
                />
                <LocationButton
                  title={
                    this.state.option[this.selectedIndex].location !== null
                      ? en.location_saved_placeholder
                      : en.location_placeholder
                  }
                  withIcon
                  onPress={() => this.handlePressLocation()}
                />
              </View>
            ) : null}
            <View style={{ width: resize(326) }}>
              {!online ? (
                <View style={styles.buttonRightContainer}>
                  <TouchableOpacity
                    style={styles.buttonRight}
                    onPress={() => this.handlePressManageAppointments()}
                  >
                    <Text style={styles.buttonRightText}>
                      {en.manage_appointments_button_text}
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>

            {!online ? (
              <View style={{ marginVertical: 20 }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: NEXABOLD,
                      fontSize: 18,
                      color: colors.PRIMARY,
                    }}
                  >
                    {
                      "Will this audition require socially distanced group entrances?"
                    }
                  </Text>
                  <SwitchToggle
                    backgroundColorOn={
                      styles.switchSettingsActive.backgroundColor
                    }
                    backgroundColorOff={styles.switchSettings.backgroundColor}
                    circleColorOff={styles.switchSettings.color}
                    circleColorOn={styles.switchSettingsActive.color}
                    containerStyle={styles.containerStyle}
                    circleStyle={styles.circleStyle}
                    switchOn={this.state.isSocialDistancing}
                    onPress={this.onSwitchChange}
                  />
                </View>
                <Text
                  style={{
                    fontFamily: NEXALIGHT,
                    fontSize: 18,
                    marginTop: 30,
                    letterSpacing: 1,
                    color: colors.PRIMARY,
                    lineHeight: 25,
                  }}
                >
                  {
                    "Allow for automatic assignment of grouping of performers, based on the parameters set below. Groups can be used to notify performers of updates and changes."
                  }
                </Text>
              </View>
            ) : null}

            {this.state.isSocialDistancing ? (
              <ModalDropdown
                options={this.state.groupOptions}
                onSelect={(index, option) => {
                  // this.onItemClick(option, index)
                  console.log("Group option :>> ", option);
                  this.setState({ groupSize: option.value });
                }}
                renderRow={this.renderGroupRow}
                renderSeparator={() => {
                  <View style={{ backgroundColor: "white" }} />;
                }}
                dropdownStyle={styles.dropDownStyle}
              >
                <View
                  style={{
                    marginBottom: resize(10, "height"),
                    marginTop: resize(10, "height"),
                  }}
                >
                  <View style={styles.roundContainer}>
                    <Text style={general_styles.title}>
                      {this.state.groupSize != null
                        ? this.state.groupSize
                        : "Group Size"}
                    </Text>
                    <Image source={picker_downflag} resizeMode="contain" />
                  </View>
                </View>
              </ModalDropdown>
            ) : null}

            <View style={styles.row}>
              <View style={styles.tagsContainer}>
                <Text style={styles.tagTitle}>
                  {en.union_status_tags_title}
                </Text>
                <View style={styles.tags}>
                  {unionTags.map((tag, index) => {
                    return (
                      <UnionTag
                        key={index}
                        name={tag}
                        selected={union === tag}
                        onPress={() => this.handlePressUnionTag(tag)}
                      />
                    );
                  })}
                </View>
                <Text style={styles.tagTitle}>
                  {en.contract_type_tags_title}
                </Text>
                <View style={styles.tags}>
                  {contractTags.map((tag, index) => {
                    return (
                      <ContractTag
                        key={index}
                        name={tag}
                        selected={contract === tag}
                        onPress={() => this.handlePressContractTag(tag)}
                      />
                    );
                  })}
                </View>
                <Text style={styles.tagTitle}>
                  {en.production_type_tags_title}
                </Text>
                <View style={styles.tags}>
                  {productionTags.map((tag, index) => {
                    return (
                      <ProductionTag
                        key={index}
                        name={tag}
                        selected={production.includes(tag)}
                        onPress={() => this.handlePressProductionTag(tag)}
                      />
                    );
                  })}
                </View>
              </View>
              <View style={styles.wrapperRight}>
                {/* {!online ? (
                  <View style={styles.buttonRightContainer}>
                    <TouchableOpacity
                      style={styles.buttonRight}
                      onPress={() => this.handlePressManageAppointments()}
                    >
                      <Text style={styles.buttonRightText}>
                        {en.manage_appointments_button_text}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null} */}
                {roles.length > 0 && (
                  <View style={styles.rolesContainer}>
                    <View style={styles.titleContainer}>
                      <Text style={styles.title}>{en.roles_title}</Text>
                    </View>
                    <ScrollView
                      horizontal
                      bounces={false}
                      showsHorizontalScrollIndicator={false}
                    >
                      {roles.map((role, index) => {
                        return (
                          <Role
                            key={index}
                            text={role.name}
                            onPress={() => this.onPressRole(index)}
                            icon={role.cover}
                          />
                        );
                      })}
                    </ScrollView>
                  </View>
                )}
                <View style={styles.buttonRightContainer}>
                  <TouchableOpacity
                    style={styles.buttonRight}
                    onPress={() => this.handlePressAddRole()}
                  >
                    <Text style={styles.buttonRightText}>
                      {en.add_role_title}
                    </Text>
                  </TouchableOpacity>
                </View>
                {files.length > 0 && (
                  <View style={styles.documentsContainer}>
                    <View style={styles.titleContainer}>
                      <Text style={styles.title}>{en.documents_title}</Text>
                    </View>
                    <ScrollView
                      horizontal
                      bounces={false}
                      showsHorizontalScrollIndicator={false}
                      style={styles.documentsContent}
                    >
                      {files.map((file, index) => {
                        return (
                          <File
                            key={index}
                            refs={(fileRef) =>
                              this.handleSetRef(fileRef, index)
                            }
                            filename={file.filename}
                            fileType={file.type}
                            file={file}
                            onPressMore={() =>
                              this.onPressFileMore(file, index)
                            }
                            containerStyle={styles.fileContainer}
                          />
                        );
                      })}
                    </ScrollView>
                  </View>
                )}
                <View style={styles.buttonRightContainer}>
                  <TouchableOpacity
                    style={styles.buttonRight}
                    onPress={() => this.onUploadDocument()}
                  >
                    <Text style={styles.buttonRightText}>
                      {/* {en.upload_document_title} */}
                      Manage Audition Materials
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Content>
        </View>
      </Container>
    );
  }
}

export default connect(null)(NewAudition);
