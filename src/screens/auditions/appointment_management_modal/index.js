import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image, Modal, ScrollView, TextInput } from 'react-native';
import { Container, Content } from 'native-base';

//custom
import styles from './styles';
import icon_close from '../../../assets/icons/commons/icon_close.png';
import GradientButton from '../../../components/commons/gradient_button';
import AppointmentRow from '../../../components/auditions/appointment_management_modal/appointment_row';
import moment from 'moment';
import BorderedSelect from '../../../components/auditions/bordered_select';
import BorderedTimepicker from '../../../components/auditions/bordered_timepicker';
//custom
import uncheck from '../../../assets/icons/commons/unCheck.png';
import check from '../../../assets/icons/commons/check.png';

//locale
import en from '../../../locale/en';
import { resize, NEXABOLD } from '../../../assets/styles';

export default class AppointmentManagerModal extends Component {

	constructor(props) {
		super(props)
		console.log(" this.props.timeInterval ", this.props)
		this.appointmentArray = [{ label: "3 minutes", value: 3 }, { label: "5 minutes", value: 5 },]
		this.appointmentArray = [...this.appointmentArray, ...this.props.timeInterval]
		console.log(" this.appointmentArray ", this.appointmentArray)
	}

	state = {
		appointment: this.props.appointment,
		// appointment: [],
		startTime: null,
		endTime: null,
		type: null,
		length: null,
		appointmentNumber: null,
		appointmentNumberError: false,
		typeError: false,
		lengthError: false,
		startTimeError: false,
		isSelectAll: false,
	}

	componentWillReceiveProps(props) {
		if (props.appointment != null && props.appointment.length > 0) {
			this.setState({
				appointment: props.appointment.slots,
				startTime: props.appointment.start,
				endTime: props.appointment.end,
				type: props.appointment.type,
				length: props.appointment.length,
				appointmentNumber: props.appointment.slots.length
			})
		} else {
			this.setState({
				appointment: [],
				startTime: null,
				endTime: null,
				type: null,
				length: null,
				appointmentNumber: null
			})
		}
	}

	onSelectAll = () => {
		let { appointment } = this.state;
		this.state.isSelectAll = !this.state.isSelectAll
		appointment.map(data => data.is_walk = this.state.isSelectAll)

		this.setState({ appointment: appointment })
	}

	markRow = (value, index) => {
		let { appointment } = this.state;

		appointment[index].is_walk = value;

		this.setState({ appointment });
	}

	handleChangeStartTime = (time) => {
		var coeff = 1000 * 60 * 10;
		var date = new Date('02/02/2019 ' + time);
		var rounded = new Date(Math.round(date.getTime() / coeff) * coeff);
		this.setState(
			{
				startTime: rounded
			},
			() => this.handleCreateAppointments()
		);
	}

	handleChangeEndTime = (time = null, update = true) => {
		var coeff = 1000 * 60 * 10;
		var date = new Date('02/02/2019 ' + time);
		var rounded = new Date(Math.round(date.getTime() / coeff) * coeff);
		this.setState({
			endTime: rounded
		},
			() => {
				if (update) {
					this.handleCreateAppointments(true)
				}
			})
	}

	changeAppointmentNumber = (appointmentNumber) => {
		this.setState(
			{
				appointmentNumber
			},
			() => {
				if (appointmentNumber) {
					this.handleCreateAppointments();
				} else {
					this.setState({
						appointment: []
					});
				}
			}
		)
	}

	changeType = (value) => {
		this.setState(
			{
				type: value
			},
			() => this.handleCreateAppointments()
		);
	}

	changeLength = (value) => {
		console.log('value :>> ', value);
		this.setState(
			{
				length: value
			},
			() => {
				this.handleCreateAppointments()
			}
		);
	}

	handleCreateAppointments = (forceTime = false) => {
		console.log('handleCreateAppointments Called:');
		const { appointmentNumber, length, type, startTime, endTime } = this.state;
		if (Number(appointmentNumber === null ? 0 : appointmentNumber) === 0 || forceTime) {

			// this.handleChangeEndTime();
			if (length && startTime && endTime && !appointmentNumber && type || forceTime && length && startTime && endTime && type) {
				var sTime = moment(startTime, "HH:mm:ss a");
				var eTime = moment(endTime, "HH:mm:ss a");
				var duration = moment.duration(eTime.diff(sTime));
				var hours = duration.asMinutes();
				let newAppointmentNumber = String(Math.floor((hours / length)));
				console.log(newAppointmentNumber)
				const newAppointments = Array.from({ length: Number(newAppointmentNumber) }, (v, i) => ({
					numberRow: type === 2 ? i + 1 : null,
					time: moment(startTime, 'HH:mm A').add(length * i, 'minutes').format('LT'),
					namePartaker: null,
					is_walk: false
				}));
				console.log(newAppointments)

				console.log('newAppointments :', newAppointments);
				console.log('newAppointmentNumber :', newAppointmentNumber);
				let lastItems = 3
				if (newAppointments.length >= 4) {
					console.log('length is greater than 3');
					// for (i = newAppointments.length - 1; i >= 0 && lastItems > 0; i--) {
					// 	arr[i] = new_value;
					// 	N--;
					// }
				} else {
					console.log('length is equal 3');
				}

				this.setState({
					appointmentNumber: newAppointmentNumber,
					appointments: this.setLastAppointmentsMark(newAppointments)
					// appointment: newAppointments
				});
			} else {
				this.setState({
					appointment: []
				});
			}

		}
		else if (length && type && startTime) {
			const newAppointments = Array.from({ length: Number(appointmentNumber) }, (v, i) => ({
				numberRow: type === 2 ? i + 1 : null,
				time: moment(startTime, 'HH:mm A').add(length * i, 'minutes').format('LT'),
				namePartaker: null,
				is_walk: false
			}));
			const lastTime = newAppointments[newAppointments.length - 1].time;

			this.handleChangeEndTime(moment(lastTime, 'HH:mm A').add(length, 'minutes').format('LT'), false);

			this.setState({ appointment: this.setLastAppointmentsMark(newAppointments) })
			// this.setState({
			// 	appointment: newAppointments
			// });
		}
		else {

			// this.handleChangeEndTime();

			this.setState({
				appointment: []
			});

		}
	}

	setLastAppointmentsMark = (newAppointments) => {
		// let lastItems = 3
		let lastItems = parseInt(newAppointments.length / 3)
		console.log('lastItems :', lastItems);
		if (newAppointments.length >= 4) {
			for (i = newAppointments.length - 1; i >= 0 && lastItems > 0; i--) {
				newAppointments[i].is_walk = true
				lastItems--;
			}
			return newAppointments
		} else {
			return newAppointments
		}
	}

	parseDataToSend = () => {
		const {
			appointmentNumber,
			type,
			length,
			startTime,
			endTime,
			appointment
		} = this.state;

		let data = {
			spaces: appointmentNumber,
			type,
			length,
			start: moment(startTime, 'HH:mm A').format('HH:mm'),
			end: moment(endTime, 'HH:mm A').format('HH:mm'),
			status: true,
			round: 1
		};

		data['slots'] = [];
		appointment.map((item) => {
			let _row = {};

			_row['time'] = item.time;
			_row['status'] = false;
			_row['is_walk'] = item.is_walk;
			_row['number'] = null;

			if (item.numberRow !== null) {
				_row['number'] = item.numberRow
			}

			return data['slots'].push(_row);
		});

		if (this.validateFields()) {
			this.setState({
				appointmentNumberError: false,
				typeError: false,
				lengthError: false,
				startTimeError: false
			});

			this.props.done(data);
			this.props.dismiss()
		}
	}

	validateFields = () => {
		const {
			appointmentNumber,
			type,
			length,
			startTime,
		} = this.state;

		if (appointmentNumber === '' || appointmentNumber === null) {
			this.setState({
				appointmentNumberError: true
			});

			return false;
		} else {
			this.setState({
				appointmentNumberError: false
			});
		}

		if (type === null || type === '') {
			this.setState({
				typeError: true
			});

			return false;
		} else {
			this.setState({
				typeError: false
			});
		}

		if (length === null || length === '') {
			this.setState({
				lengthError: true
			});

			return false;
		} else {
			this.setState({
				lengthError: false
			});
		}

		if (startTime === null) {
			this.setState({
				startTimeError: true
			});

			return false;
		} else {
			this.setState({
				startTimeError: false
			});
		}

		return true;
	}

	render() {
		// console.log('this.props :', this.props);
		// console.log('this.state :', this.state);
		const {
			appointment,
			startTime,
			endTime,
			type,
			length,
			appointmentNumber,
			appointmentNumberError,
			typeError,
			lengthError,
			startTimeError
		} = this.state;

		return (
			<Modal
				animationType='fade'
				animated={true}
				visible={this.props.show}
				transparent={true}
				onRequestClose={this.props.dismiss}
			>
				<Container style={styles.nBContainer}>
					<TouchableOpacity
						activeOpacity={1}
						style={styles.dismissButton}
						onPress={this.props.dismiss}
					>
						<View style={styles.blurView} />
					</TouchableOpacity>
					<Content bounces={false}>
						<View style={styles.cardContainer}>
							<View style={styles.card}>
								<TouchableOpacity
									style={styles.closeButton}
									onPress={this.props.dismiss}
								>
									<Image source={icon_close} />
								</TouchableOpacity>
								<View style={styles.leftContainer}>
									<Text style={styles.titleLeftContainer}>
										{en.mark_as_walk}
									</Text>
									{appointment.length !== 0 ? <View style={styles.selectAllContainer}>
										<Text style={styles.selectAllText}>
											{en.select_all}
										</Text>
										<TouchableOpacity onPress={this.onSelectAll}>
											<Image source={this.state.isSelectAll ? check : uncheck} />
										</TouchableOpacity>
									</View> : null}
									<ScrollView style={styles.appointmentScroll}>
										{
											appointment.length !== 0 && (
												appointment.map((item, index) => {
													return (
														<AppointmentRow
															{...item}
															mark={
																(value) => this.markRow(value, index)
															}
															key={'AppointmentRow' + index}
														/>
													)
												})
											)
										}
									</ScrollView>
								</View>
								<View style={styles.rightContainer}>
									<Text style={styles.titleSection}>
										{en.appointment_slots}
									</Text>
									<View
										style={
											[
												styles.gradientContainer,
												appointmentNumberError ? styles.inputError : {}
											]
										}
									>
										<TextInput
											placeholder={'0'}
											value={this.state.appointmentNumber}
											keyboardType='number-pad'
											placeholderTextColor={this.props.inputPlaceholder}
											onChangeText={this.changeAppointmentNumber}
											style={styles.gradientInput}
										/>
										<GradientButton
											activeOpacity={1}
											containerStyle={styles.gradientLabelContainer}
											gradientStyle={styles.gradientLabelContainer}
											buttonText={en.appointments}
										/>
									</View>
									<Text style={styles.titleSection}>
										{en.appointments_type}
									</Text>
									<View style={styles.inputsContainer}>
										<BorderedSelect
											onChange={this.changeType}
											data={this.props.appointmentType}
											placeholder={en.select_type_placeholder}
											value={type}
											style={
												[
													styles.input,
													typeError ? styles.inputError : {}
												]
											}
										/>
									</View>
									<Text style={styles.titleSection}>
										{en.appointments_length}
									</Text>
									<View style={styles.inputsContainer}>
										<BorderedSelect
											onChange={this.changeLength}
											// data={this.props.timeInterval}
											data={this.appointmentArray}
											placeholder={en.select_length_placeholder}
											value={length}
											style={
												[
													styles.input,
													lengthError ? styles.inputError : {}
												]
											}
										/>
									</View>
									<Text style={styles.titleSection}>
										{en.start_time}
									</Text>
									<View style={styles.inputsContainer}>
										<BorderedTimepicker
											value={startTime}
											placeholder={en.select_start_time_placeholder}
											onChange={this.handleChangeStartTime}
											inputStyle={startTimeError ? styles.inputTimeError : styles.inputTime}
										/>
									</View>
									<Text style={styles.titleSection}>
										{en.end_time}
									</Text>
									<View style={styles.inputsContainer}>
										<BorderedTimepicker
											value={endTime}
											placeholder={en.select_end_time_placeholder}
											onChange={this.handleChangeEndTime}
											// disabled={false}
											disabledInput={styles.disabledInput}
										/>
									</View>
									<GradientButton
										onPress={() => this.parseDataToSend()}
										containerStyle={styles.inputsContainer}
										gradientStyle={styles.gradientButton}
										activeOpacity={1}
										buttonText={en.done_button_text}
									/>
								</View>
							</View>
						</View>
					</Content>
				</Container>
			</Modal>
		)
	}
}

AppointmentManagerModal.defaultProps = {
	dismiss: () => { },
	show: true,
	appointment: [],
	inputPlaceholder: '#4d2545',
	appointmentType: [{ label: 'Time', value: 1 }, { label: 'Numeric', value: 2 }],
	timeInterval: Array.from({ length: 6 }, (v, i) => ({ label: `${10 * (i + 1)} minutes`, value: 10 * (i + 1) })),
	done: () => null
}
