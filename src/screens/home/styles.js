import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../assets/styles';

const styles = StyleSheet.create({
	scrollview: {
		paddingVertical: resize(8, 'height')
	},
	searchIcon: {
		width: resize(150),
		justifyContent: 'center',
		marginRight: resize(25),
		height: resize(50, 'height'),
		alignItems: 'flex-end'
	},
	content: {
		marginLeft: resize(46),
		paddingBottom: resize(20),
	},
	noAuditions: {
		paddingVertical: resize(8, 'height'),
		paddingLeft: resize(17),
	},
	noAuditionsText: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(18),
	}
});

export default styles;