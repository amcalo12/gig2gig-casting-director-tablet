import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Platform } from 'react-native';
import { Container } from 'native-base';
import { connect } from 'react-redux';

//customs
import styles from './style';
import ItemMenu from '../../components/sidebar/item_menu';
import auditionIcon from '../../assets/icons/sidebar/auditionIcon.png';
import plusIcon from '../../assets/icons/sidebar/plusIcon.png';
import settingIcon from '../../assets/icons/sidebar/settingsIcon.png';
import logoutIcon from '../../assets/icons/sidebar/logoutIcon.png';
import TalentDataBaseIcon from '../../assets/images/auditions/Group41.png';
import NewsAndUpdateIcon from '../../assets/images/auditions/np_news_75.png';
import NotificationsIcon from '../../assets/images/auditions/Group54.png';
import AlertIOS from '../../components/commons/alert_ios';

//actions
import { DESTROY_SESSION } from '../../actions/auth';

//locale
import en from '../../locale/en';
import requestDeviceToken from '../../utils/device_token';
import { setDeviceToken } from '../../api/login';
import { DEVICE_ID, subscriptionAlert, openSubscriptionDialog, SUBSCRIPTION_ALERT } from '../../utils/constant';
import { Assets } from '../../assets';
import { NavigationEvents } from 'react-navigation';
import { fetchCurrentUser } from '../../api/settings';
import { SET_USER } from '../../actions/user';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.props.user.isPaidUser = this.props.user.isPaidUser
  }
  state = {
    signOutModal: false
  }

  handlePressUser = () => {
    const { navigation } = this.props;

    navigation.navigate('Settings');
  }

  handlePressNavigateTo = (view) => {
    const { navigation, activeItemKey } = this.props;

    if (activeItemKey === view) {
      return navigation.closeDrawer();
    }

    return navigation.navigate(view);
  }

  handlePressSignOut = () => {
    this.setState({ signOutModal: true });
  }

  dismissLogOut = () => {
    this.setState({ signOutModal: false });
  }

  onSetDeviceToken = async () => {
    const { dispatch } = this.props;
    console.log(" set device called ")
    try {
      const token = await requestDeviceToken();
      if (token) {
        await setDeviceToken({ pushkey: "", device_id: DEVICE_ID, device_type: Platform.OS });
        console.log("DEVICE ID DEVICE ID logout=====>", DEVICE_ID);
        dispatch({
          type: DESTROY_SESSION,
          payload: {}
        });
      }
    } catch (error) {
      console.log(" set device called 111", error.response)
      console.log({ error });
    } // ignoring token's errors to not disturb users
  };
  agreeLogOut = () => {

    this.onSetDeviceToken()
    this.dismissLogOut();


  }
 
 
  render() {
    const { signOutModal } = this.state;
    const { user, activeItemKey } = this.props;

    return (
      <Container style={styles.container}>
        
        <AlertIOS
          title=''
          show={signOutModal}
          dismiss={this.dismissLogOut}
          agree={this.agreeLogOut}
        >
          <Text style={styles.monitorModeText}>
            {en.sign_out_message}
          </Text>
        </AlertIOS>
        <TouchableOpacity
          style={styles.userContainer}
          onPress={() => this.handlePressUser()}
        >
          <View style={styles.userImageContainer}>
            {
              user.imageUrl != null ? (
                <Image
                  source={{ uri: user.imageUrl }}
                  style={styles.userImage}
                />
              ) : null
            }
          </View>
          <View style={styles.usernameContainer}>
            <Text style={styles.username}>
              {user.name != null ? String(user.name).replace('null', '') : ''}
            </Text>
          </View>
        </TouchableOpacity>
        <View>
          <ItemMenu
            title={en.audition_title}
            icon={auditionIcon}
            count={this.props.auditionCount}
            onPress={() => this.handlePressNavigateTo('Home')}
            active={activeItemKey === 'Home'}
          />
          <ItemMenu
            title={en.past_audition_title}
            icon={Assets.restore}
            // count={this.props.auditionCount}
            onPress={() => this.handlePressNavigateTo('PastAudition')}
            active={activeItemKey === 'PastAudition'}
          />
          <ItemMenu
            title={en.create_new_title}
            icon={plusIcon}
            onPress={() => this.props.user.isPaidUser ? this.handlePressNavigateTo('CreateAudition') : openSubscriptionDialog()}
            active={activeItemKey === 'CreateAudition'}
          />
          <ItemMenu
            title={en.talent_database}
            icon={TalentDataBaseIcon}
            onPress={() => this.props.user.isPaidUser ? this.handlePressNavigateTo('TalentDatabase') : openSubscriptionDialog()}
            active={activeItemKey === 'TalentDatabase'}
            Height={styles.heightOfItem}
          />
          <ItemMenu
            title={en.news_and_updates}
            icon={NewsAndUpdateIcon}
            onPress={() => this.props.user.isPaidUser ? this.handlePressNavigateTo('NewsAndUpdates') : openSubscriptionDialog()}
            active={activeItemKey === 'NewsAndUpdates'}
            Height={styles.heightOfItem}
          />
          <ItemMenu
            title={en.notifications}
            icon={NotificationsIcon}
            onPress={() => this.props.user.isPaidUser ? this.handlePressNavigateTo('Notifications') : openSubscriptionDialog()}
            active={activeItemKey === 'Notifications'}
          />
          <ItemMenu
            title={en.settings_title}
            icon={settingIcon}
            onPress={() => this.handlePressNavigateTo('Settings')}
            active={activeItemKey === 'Settings'}
          />
          <ItemMenu
            title={en.sign_out_button_text}
            icon={logoutIcon}
            onPress={() => this.handlePressSignOut()}
          />
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
  auditionCount: state.manage.audition_count
});

export default connect(mapStateToProps)(Sidebar);