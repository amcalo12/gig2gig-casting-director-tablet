import { StyleSheet, Platform } from 'react-native';

//customs
import { height, width, NEXALIGHT, resize } from '../../assets/styles';

const styles = StyleSheet.create({
  heightOfItem: {
    height: resize(73, 'h')
  },
  container: {
    height,
    marginTop: resize(23, 'height'),
  },
  userContainer: {
    flexDirection: 'row',
    marginHorizontal: resize(23),
    marginBottom: resize(60, 'height'),
    height: resize(40, 'height'),
  },
  userImageContainer: {
    width: resize(40),
    height: '100%'
  },
  userImage: {
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    borderRadius: 4,
  },
  usernameContainer: {
    paddingTop: resize(12, 'height'),
    paddingLeft: resize(15),
  },
  username: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(18),
    fontWeight: '400',
    height: '100%'
  },
  monitorModeText: {
    paddingHorizontal: resize(50),
    color: '#000000',
    fontSize: resize(15),
    fontWeight: '400',
    lineHeight: resize(18),
    textAlign: 'center',
    ...Platform.select({
      android: {
        paddingBottom: resize(40, 'height')
      }
    })
  }
});

export default styles;