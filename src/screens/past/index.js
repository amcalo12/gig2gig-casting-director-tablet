import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import { connect } from 'react-redux';
import { ScrollView, View, Text, RefreshControl, Platform } from 'react-native';

//custom
import styles from './styles';
import Logo, { LogoHeader } from '../../components/commons/logo';
import Header from '../../components/commons/header';
import Card from '../../components/commons/audition_card';
import LightBar from '../../components/commons/light_bar';
import ButtonMenu from '../../components/commons/button_menu';
import HeaderButton from '../../components/commons/header_button';
import SectionHeader from '../../components/commons/section_header';
import iconSearch from '../../assets/icons/search/search.png';
import Search from '../../components/commons/search';
import Loading from '../../components/commons/loading';

//api
import { upcomingAuditions, pastAuditions, getfeedback } from '../../api/auditions';
import { setDeviceToken } from '../../api/login';
//locale
import en from '../../locale/en';

//actions
import { AUDITION_COUNT } from '../../actions/manage';

//messaging
import requestDeviceToken from '../../utils/device_token';
import { DEFAULT_INSTANT_FEEDBACK } from '../../actions/user';
import { DEVICE_ID, openSubscriptionDialog } from '../../utils/constant';

class PastAudition extends Component {
    constructor(props) {
        super(props);
        this.props.user.isPaidUser = this.props.user.isPaidUser
    }

    state = {
        search: '',
        showSearch: false,
        upcomingAuditions: [],
        loading: false,
        pastAuditions: [],
        refreshing: false,
        instantFeedback: ""
    }

    componentDidMount() {
        // this.fetchUpcomingAudition();
        this.fetchPastAudition();
        // this.onSetDeviceToken();
        // this.getInstantFeedback();
    }

    onSetDeviceToken = async () => {
        try {
            const token = await requestDeviceToken()

            if (token) {
                await setDeviceToken({ pushkey: token, device_id: DEVICE_ID, device_type: Platform.OS })
                console.log("DEVICE ID=====>", DEVICE_ID)
            }
        } catch (error) {
            console.log({ error })
        } // ignoring token's errors to not disturb users
    }

    // //GET INSTANT FEEDBACK
    // getInstantFeedback = async () => {
    // 	const { dispatch } = this.props;
    // 	const { user } = this.props;
    // 	console.log("<===== user id =====>", user.id)

    // 	this.setState({ loading: true });
    // 	try {
    // 		const response = await getfeedback(user.id);

    // 		this.setState({
    // 			instantFeedback: response.data.data
    // 		});
    // 		console.log("SUCCESS Get Instant Feedback =====>", response)
    // 		let defaultMessage = {}
    // 		defaultMessage['reject'] = response.data.data.comment
    // 		defaultMessage['accept'] = response.data.data.positiveComment
    // 		dispatch({
    // 			type: DEFAULT_INSTANT_FEEDBACK,
    // 			payload: defaultMessage
    // 		});
    // 	} catch (error) {
    // 		console.log("ERROR Get Instant Feedback =====>", error);
    // 	}

    // 	this.setState({ loading: false });
    // }

    fetchUpcomingAudition = async () => {
        const { dispatch } = this.props;

        this.setState({ loading: true });

        try {
            const response = await upcomingAuditions();

            this.setState({
                upcomingAuditions: response.data.data
            });

            dispatch({
                type: AUDITION_COUNT,
                payload: response.data.data.length
            });
        } catch (error) {
            console.log(error);
        }

        this.setState({ loading: false });
    }

    fetchPastAudition = async () => {
        this.setState({ loading: true });

        try {
            const response = await pastAuditions();

            this.setState({
                pastAuditions: response.data.data
            });
        } catch (error) {
            console.log(error);
        }

        this.setState({ loading: false });
    }

    handlePressOpenDrawer = () => {
        const { navigation } = this.props;

        navigation.openDrawer();
    }

    handlePressShowSearch = () => {
        this.setState({
            showSearch: true
        });
    }

    onChangeSearch = (search) => {
        this.setState({ search });
    }

    handlePressPast = (audition_id) => {
        const { navigation } = this.props;

        navigation.navigate('PastDetail', { audition_id });
    }

    navigateToAudition = (audition_id, isManger) => {
        const { navigation } = this.props;
        return navigation.navigate('ManagerAuditionDetail', { audition_id, isManger });
        // if (isManger) {
        // 	return navigation.navigate('ManagerAuditionDetail', { audition_id });
        // }

        // return navigation.navigate('ContributorAuditionDetail', { audition_id });
    }

    handlePressAdminTag = () => {
        const { navigation } = this.props;

        navigation.navigate('AuditionManage');
    }

    onRefresh = async () => {
        const { dispatch } = this.props;

        this.setState({ refreshing: true });

        try {
            const upcomings = await upcomingAuditions();
            this.setState({
                upcomingAuditions: upcomings.data.data
            });
            dispatch({
                type: AUDITION_COUNT,
                payload: upcomings.data.data.length
            });
        } catch (error) {
            console.log(error);
        }

        try {
            const past = await pastAuditions();
            this.setState({
                pastAuditions: past.data.data
            });
        } catch (error) {

        }

        this.setState({ refreshing: false });
    }

    render() {

        const {
            search,
            showSearch,
            upcomingAuditions,
            pastAuditions,
            loading,
            refreshing
        } = this.state;
        const { user } = this.props;
        const upcomingAuditionsFilter = upcomingAuditions.filter((audition) => {
            let title = audition.title.toUpperCase();

            return title.match(new RegExp(search.toUpperCase(), 'g'));
        });
        const pastAuditionsFilter = pastAuditions.filter((audition) => {
            let title = audition.title.toUpperCase();

            return title.match(new RegExp(search.toUpperCase(), 'g'));
        });

        if (loading) {
            return <Loading />
        }

        return (
            <Container>
                <LightBar />
                <Header
                    left={
                        <ButtonMenu
                            onPress={() => this.handlePressOpenDrawer()}
                        />
                    }
                    center={
                        <LogoHeader />
                    }
                    right={
                        showSearch ? (
                            <Search
                                placeholder={en.search_placeholder}
                                value={search}
                                onChange={this.onChangeSearch}
                            />
                        ) : (
                                <HeaderButton
                                    icon={iconSearch}
                                    onPress={() => this.handlePressShowSearch()}
                                    containerStyle={styles.searchIcon}
                                />
                            )
                    }
                />
                <Content
                    style={styles.content}
                    disableKBDismissScroll
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={() => this.onRefresh()}
                        />
                    }
                >
                    <SectionHeader
                        title={en.past_title}
                    />
                    {
                        pastAuditionsFilter.length > 0 && (
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                bounces={false}
                                style={styles.scrollview}
                            >
                                {
                                    pastAuditionsFilter.map((audition, index) => {
                                        // let isManger = user.id === audition.user_id;
                                        let isManger = (user.id === audition.user_id || user.id === audition.admin_id);
                                        return (
                                            <Card
                                                key={index}
                                                title={audition.title}
                                                image={{ uri: audition.cover_thumbnail != null ? audition.cover_thumbnail : audition.cover }}
                                                date={audition.date}
                                                onPressManageTag={() => this.props.user.isPaidUser ? this.navigateToAudition(audition.id, isManger) : openSubscriptionDialog()}
                                                onPressContributorTag={() => this.props.user.isPaidUser ? this.navigateToAudition(audition.id, isManger) : openSubscriptionDialog()}
                                                onPress={() => this.props.user.isPaidUser ? this.navigateToAudition(audition.id, isManger) : openSubscriptionDialog()}
                                            />
                                        )
                                    })
                                }
                            </ScrollView>
                        )
                    }
                    {
                        pastAuditionsFilter.length === 0 && (
                            <View style={styles.noAuditions}>
                                <Text style={styles.noAuditionsText}>
                                    {en.no_past_auditions}
                                </Text>
                            </View>
                        )
                    }
                </Content>
            </Container>
        );
    }
}

// const mapStateToProps = (state) => ({
// 	user: state.user.data
// });
const mapStateToProps = state => {
    console.log("STORE REDUX=====>", state)
    return ({
        user: state.user.data
    });
}
export default connect(mapStateToProps)(PastAudition);
