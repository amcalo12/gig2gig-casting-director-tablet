import { StyleSheet } from 'react-native';

// customs
import { NEXABOLD, resize } from '../../assets/styles';

const styles = StyleSheet.create({
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: resize(65, 'height'),
  },
  title: {
    color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: resize(25),
    fontWeight: '400',
  },
  formContainer: {
    marginTop: resize(134, 'height'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer2: {
    marginTop: resize(55, 'height'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainerLast: {
    marginTop: resize(31, 'height'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    marginTop: resize(103, 'height')
  },
  formGroup: {
    marginBottom: resize(7, 'height'),
  },
  imageContainer: {
    width: resize(142),
    height: resize(142, 'height'),
    borderRadius: 100,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: resize(33, 'height')
  },
  iconProfile: {
    resizeMode: 'contain',
    width: '50%',
    height: '50%',
    marginLeft: 20,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  photo: {
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    borderRadius: 70,
  },
  inputZipContainer: {
    width: resize(140),
    height: resize(55, 'height'),
    flexDirection: 'row',
    marginBottom: resize(17),
    marginLeft: resize(49),
  },
  inputZipStyle: {
    shadowRadius: 8,
    backgroundColor: '#ffffff',
    borderRadius: 50,
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(19),
    fontWeight: '400',
    paddingLeft: resize(20),
    width: resize(140),
    height: resize(55, 'height'),
  }
});

export default styles;