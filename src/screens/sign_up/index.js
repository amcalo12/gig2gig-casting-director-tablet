import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  BackHandler,
  TouchableOpacity,
  Keyboard,
} from "react-native";
import { Form } from "native-base";
import { connect } from "react-redux";

//custom
import styles from "./styles";
import BackgroundWrapper from "../../components/commons/background_wrapper";
import LightBar from "../../components/commons/light_bar";
import Header from "../../components/commons/header";
import BackButton from "../../components/commons/back_button";
import Logo from "../../components/commons/logo";
import InputRadius from "../../components/commons/input_radius";
import GradientButton from "../../components/commons/gradient_button";
import IconProfile from "../../assets/icons/sign_up/icon-profile.png";
import DatepickerRadius from "../../components/commons/datepicker_radius";
import SelectRadius from "../../components/commons/select_radius";
import getImage from "../../utils/get_image";
import showMessage from "../../utils/show_message";
import statesList from "../../utils/states_list";

//api
import { createAccount } from "../../api/create_account";
import { fetchAuth } from "../../api/login";

//actions
import { INIT_SESSION } from "../../actions/auth";
import { SET_USER } from "../../actions/user";

//validators
import validateEmail from "../../utils/validate_email";
import validatePasswords from "../../utils/validate_passwords";
import parseError from "../../utils/parse_error";

//locale
import en from "../../locale/en";
import ImagePicker from "react-native-image-crop-picker";
import { getThumbnail } from "../../utils/constant";
import RenameModal from "../../components/auditions/rename_file";
import { getAllTeamAdmins } from "../../api/auditions";

// const genders = [
// 	{
// 		label: 'Male',
// 		value: 'male'
// 	},
// 	{
// 		label: 'Female',
// 		value: 'female'
// 	},
// 	{
// 		label: 'Other',
// 		value: 'other'
// 	},
// ];

const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*#?&:()._-]{8,}$/;

const countryList = require("../../utils/country_list.json");
class SignUp extends Component {
  state = {
    showFirstStep: true,
    showSecondtStep: false,
    showThirdStep: false,
    name: "",
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    confirmPassword: "",
    address: "",
    address2: "",
    city: "",
    birthDate: null,
    agency: "",
    job: "",
    state: "",
    country: "",
    //gender: '',
    zip: "",
    image: null,
    loading: false,
    fileName: "",
    isVisible: false,
    country: "",
  };

  componentDidMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handlePressBackButton
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handlePressBackButton
    );
  }

  handlePressBackButton = () => {
    const { navigation } = this.props;
    const { showFirstStep, showSecondtStep, showThirdStep } = this.state;

    if (showFirstStep) {
      navigation.goBack(null);

      return true;
    } else if (showSecondtStep) {
      this.setState({
        showSecondtStep: false,
        showFirstStep: true,
      });

      return true;
    } else if (showThirdStep) {
      this.setState({
        showSecondtStep: true,
        showThirdStep: false,
      });

      return true;
    }

    return false;
  };

  onChangeName = (name) => {
    this.setState({ name });
  };

  onFirstNameChange = (first_name) => {
    this.setState({ first_name });
  };

  onLastNameChange = (last_name) => {
    this.setState({ last_name });
  };

  onChangeEmail = (email) => {
    this.setState({ email });
  };

  onChangePassword = (password) => {
    this.setState({ password });
  };

  onChangeConfirmPassword = (confirmPassword) => {
    this.setState({ confirmPassword });
  };

  onChangeAddress = (address) => {
    this.setState({ address });
  };

  onAddressLine2Change = (address2) => {
    this.setState({ address2 });
  };

  onChangeCity = (city) => {
    this.setState({ city });
  };

  onChangeBirthDate = (birthDate) => {
    this.setState({ birthDate });
  };

  onChangeAgency = (agency) => {
    this.setState({ agency });
  };

  onChangeJob = (job) => {
    this.setState({ job });
  };

  onChangeState = (state) => {
    this.setState({ state });
  };

  onChangeCountry = (country) => {
    console.log("country :>> ", country);
    this.setState({ country });
  };

  // onChangeGender = (gender) => {
  // 	this.setState({ gender });
  // }

  onChangeZip = (value) => {
    if (value.length <= 5) {
      let zip = String(value).replace(/[^0-9]/g, "");
      this.setState({ zip });
    }
  };

  onCloseClick = () => {
    this.setState({ isVisible: false });
  };

  onSubmitClick = async () => {
    // console.log('this.props :', this.props.user);
    // console.log('this.props 11:', this.state.imageData);
    // return

    this.setState({ isVisible: false });
  };

  onChangeText = (fileName) => {
    this.setState({ fileName });
  };

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    );
  };

  handlePressTakePhoto = async () => {
    try {
      const response = await getImage();
      ImagePicker.openCropper({
        path: response.uri,
        // width: image.width,
        // height: image.height
      })
        .then(async (data) => {
          console.log("Crop image", data);
          let newData = response;
          newData["uri"] = data.path;

          let thumbnailImage = await getThumbnail(data.path);
          console.log("thumbnail :", thumbnailImage);

          this.setState({
            image: { uri: data.path, thumbnail: thumbnailImage.uri },
            isVisible: true,
            fileName: "",
          });
        })
        .catch(async (error) => {
          let thumbnailImage = await getThumbnail(response.uri);
          console.log("thumbnail :", thumbnailImage);
          console.log("Crop image error", error);
          this.setState({
            image: { uri: response.uri, thumbnail: thumbnailImage.uri },
            isVisible: true,
            fileName: "",
          });
        });
      // this.setState({ image: { uri: response.uri } });
    } catch (error) {
      console.log(error);
    }
  };

  validateFirstStep = () => {
    const {
      name,
      email,
      password,
      confirmPassword,
      first_name,
      last_name,
    } = this.state;
    let errorMessage = "";

    Keyboard.dismiss();

    // if (name === "" || name < 2) {
    //   errorMessage = en.require_name;
    // }
    if (first_name === "" || first_name < 2) {
      errorMessage = en.require_first_name;
    } else if (last_name === "" || last_name < 2) {
      errorMessage = en.require_last_name;
    } else if (email === "") {
      errorMessage = en.require_email;
    } else if (!validateEmail(email)) {
      errorMessage = en.valid_email;
    } else if (password === "") {
      errorMessage = en.require_password;
    } else if (confirmPassword === "") {
      errorMessage = en.require_confirm_password;
    } else if (!validatePasswords(password, confirmPassword)) {
      errorMessage = en.passwords_no_match;
    }

    return errorMessage;
  };

  handleValidatePassword = async () => {
    return new Promise((resolve, reject) => {
      if (!regex.test(this.state.password)) {
        reject(
          showMessage(
            "danger",
            "Your password must contain at least 7 characters with digits and at least one uppercase letter"
          )
        );
      }

      resolve();
    });
  };

  handlePressStep1 = async () => {
    const hasError = this.validateFirstStep();

    if (hasError !== "") {
      return showMessage("danger", hasError);
    }

    await this.handleValidatePassword();

    this.setState({
      showFirstStep: false,
      showSecondtStep: true,
    });
  };

  validateSecondStep = () => {
    const { address, city, state, zip, birthDate, country } = this.state;
    let errorMessage = "";

    Keyboard.dismiss();

    // if (address === "") {
    //   errorMessage = en.require_address;
    // } else if (city === "") {
    //   errorMessage = en.require_city;
    // } else if (state === "" || state === null) {
    //   errorMessage = en.require_state;
    // } else if (zip === "") {
    //   errorMessage = en.require_zip;
    // }
    if (country === "" || country === null) {
      errorMessage = en.require_country;
    }
    // else if (birthDate === null) {
    //   errorMessage = en.require_birthdate;
    // }

    return errorMessage;
  };

  handlePressStep2 = () => {
    const hasError = this.validateSecondStep();

    if (hasError !== "") {
      return showMessage("danger", hasError);
    }

    this.setState({
      showSecondtStep: false,
      showThirdStep: true,
    });
  };

  validateLastStep = () => {
    const {
      image,
      agency,
      job,
      //gender
    } = this.state;
    let errorMessage = "";

    Keyboard.dismiss();

    if (image === null) {
      errorMessage = en.require_image;
    } else if (agency === "") {
      errorMessage = en.require_agency;
    } else if (job === "") {
      errorMessage = en.require_job;
    }
    // else if (gender === '') {
    // 	errorMessage = en.require_gender;
    // }
    return errorMessage;
  };

  handleCreateAccount = () => {
    const hasError = this.validateLastStep();

    if (hasError !== "") {
      return showMessage("danger", hasError);
    }

    return this.completeCreateAccount();
  };

  completeCreateAccount = async () => {
    const {
      name,
      first_name,
      last_name,
      email,
      password,
      address,
      city,
      state,
      zip,
      birthDate,
      image,
      job,
      agency,
      address2,
      fileName,
      country,
      //	gender
    } = this.state;
    const formData = {
      // name,
      first_name,
      last_name,
      email,
      password,
      address: address + "," + address2,
      city,
      state,
      country,
      //gender,
      zip,
      // birth: birthDate,
      type: "1",
      image,
      profesion: job,
      agency_name: agency,
      fileName,
    };
    const { dispatch } = this.props;

    this.setState({ loading: true });
    try {
      await createAccount(formData);

      const response = await fetchAuth(email, password);
      console.log("response", response);
      dispatch({
        type: SET_USER,
        payload: {
          email: response.data.data.email,
          id: response.data.data.id,
          user_type: response.data.data.details.type,
          first_name: response.data.data.details.first_name,
          last_name: response.data.data.details.last_name,
          name: `${response.data.data.details.first_name} ${response.data.data.details.last_name}`,
          imageUrl: response.data.data.image.url,
          thumbnail: response.data.data.image.thumbnail,
          agencyName: response.data.data.details.agency_name,
          isPaidUser: response.data.data.is_premium == 1 ? true : false,
          subscriptionDetail:
            response.data.data.subscription != undefined
              ? response.data.data.subscription
              : undefined,
          isAdminSelect: response.data.data.selected_admin,
        },
      });

      // this.setState({ loading: false });

      dispatch({
        type: INIT_SESSION,
        payload: {
          authorize: true,
          isAppTour: true,
          token: `Bearer ${response.data.access_token}`,
        },
      });
      // this.getTeamAdminList(dispatch, `Bearer ${response.data.access_token}`);
    } catch (error) {
      console.log(error);
      this.setState({ loading: false });
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };
  getTeamAdminList = async (dispatch, accessToken) => {
    // this.setState({ loading: true });
    try {
      let adminList = await getAllTeamAdmins();
      if (adminList.data && adminList.status == 200) {
        let data = adminList.data
          ? adminList.data.data
            ? adminList.data.data
            : []
          : [];
        let isSelectAdmin = false;
        if (data.length > 0) {
          data.map((item) => {
            if (item.is_selected && item.is_selected === 1) {
              isSelectAdmin = true;
            }
          });
        }
        dispatch({
          type: INIT_SESSION,
          payload: {
            authorize: true,
            token: accessToken,
            isAppTour: true,
            isAdminSelect: isSelectAdmin,
          },
        });
        this.setState({ loading: false });
      }
    } catch (error) {
      console.log("get All team Admins =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      dispatch({
        type: INIT_SESSION,
        payload: {
          authorize: true,
          token: accessToken,
          isAppTour: true,
          isAdminSelect: true,
        },
      });
      this.setState({ loading: false });
    }
  };
  render() {
    const {
      showFirstStep,
      showSecondtStep,
      showThirdStep,
      name,
      first_name,
      last_name,
      email,
      password,
      confirmPassword,
      address,
      address2,
      agency,
      job,
      state,
      country,
      zip,
      image,
      city,
      loading,
      birthDate,
    } = this.state; //birthDate,, gender
    let date = new Date();
    date.setFullYear(date.getFullYear() - 18);
    console.log(date);
    console.log(
      "past",
      "Mon Oct 28 2019 21:46:54 GMT-0600 (Central Standard Time)"
    );
    return (
      <BackgroundWrapper>
        <LightBar />
        <Header
          left={<BackButton onPress={this.handlePressBackButton} />}
          center={<Logo />}
        />
        {this.renderRenameModal()}
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{en.create_account_title}</Text>
        </View>
        <View>
          {showFirstStep && (
            <Form style={styles.formContainer}>
              {/* <InputRadius
                placeholder={en.name_placeholder}
                value={name}
                onChange={this.onChangeName}
              /> */}
              <InputRadius
                placeholder={en.first_name}
                value={first_name}
                onChange={this.onFirstNameChange}
              />
              <InputRadius
                placeholder={en.last_name}
                value={last_name}
                onChange={this.onLastNameChange}
              />
              <InputRadius
                placeholder={en.email_placeholder}
                value={email}
                onChange={this.onChangeEmail}
                keyboardType="email-address"
              />
              <InputRadius
                placeholder={en.password_placeholder}
                value={password}
                onChange={this.onChangePassword}
                secureTextEntry
              />
              <InputRadius
                placeholder={en.confirm_password}
                value={confirmPassword}
                onChange={this.onChangeConfirmPassword}
                secureTextEntry
              />
              <View style={styles.buttonContainer}>
                <GradientButton
                  buttonText={en.next_button_text}
                  onPress={() => this.handlePressStep1()}
                />
              </View>
            </Form>
          )}
          {showSecondtStep && (
            <Form style={styles.formContainer2}>
              {/* <InputRadius
                placeholder={en.address_placeholder}
                value={address}
                onChange={this.onChangeAddress}
              />

              <InputRadius
                placeholder={en.address2_placeholder}
                // placeholder={"Address Line 2"}
                value={address2}
                onChange={this.onAddressLine2Change}
              />
              <InputRadius
                placeholder={en.city_placeholder}
                value={city}
                onChange={this.onChangeCity}
              /> */}
              <View style={styles.formGroup}>
                {/* <InputRadius
                  placeholder={en.zip_placeholder}
                  value={zip}
                  onChange={this.onChangeZip}
                  keyboardType="number-pad"
                />
                <SelectRadius
                  placeholder={en.state_placeholder}
                  value={state}
                  onChange={this.onChangeState}
                  data={statesList}
                /> */}
                {/* <SelectRadius
                  placeholder={en.state_placeholder}
                  value={country}
                  onChange={this.onChangeCountry}
                  data={countryList}
                /> */}
              </View>
              <SelectRadius
                placeholder={en.country_placeholder}
                value={country}
                onChange={this.onChangeCountry}
                data={countryList}
              />
              {/* <DatepickerRadius
                placeholder={en.birthdate_placeholder}
                value={birthDate}
                maxDate={date}
                onChange={this.onChangeBirthDate}
              /> */}
              <View style={styles.buttonContainer}>
                <GradientButton
                  buttonText={en.next_button_text}
                  onPress={() => this.handlePressStep2()}
                />
              </View>
            </Form>
          )}
          {showThirdStep && (
            <Form style={styles.formContainerLast}>
              <TouchableOpacity
                style={styles.imageContainer}
                onPress={() => this.handlePressTakePhoto()}
              >
                {image === null && (
                  <Image source={IconProfile} style={styles.iconProfile} />
                )}
                {image !== null && (
                  <Image source={image} style={styles.photo} />
                )}
              </TouchableOpacity>
              <InputRadius
                placeholder={en.agency_name_placeholder}
                value={agency}
                onChange={this.onChangeAgency}
              />
              <InputRadius
                placeholder={en.job_title_placeholder}
                value={job}
                onChange={this.onChangeJob}
              />
              {/* <SelectRadius
									placeholder={en.gender_placeholder}
									value={gender}
									onChange={this.onChangeGender}
									data={genders}
								/> */}
              <View style={styles.buttonContainer}>
                <GradientButton
                  buttonText={en.finish_button_text}
                  onPress={() => this.handleCreateAccount()}
                  loading={loading}
                />
              </View>
            </Form>
          )}
        </View>
      </BackgroundWrapper>
    );
  }
}

export default connect(null)(SignUp);
