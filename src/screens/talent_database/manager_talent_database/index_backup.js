import React, { Component } from 'react';
import { View, Text, Platform, ScrollView, Image, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { Container, Input, Toast } from 'native-base';
import { connect } from 'react-redux';
import styles, { headerHeight } from './styles';

//Custom
import LightBar from '../../../components/commons/light_bar';
import BackButton from '../../../components/commons/back_button';
import Header from '../../../components/commons/header';
import ShareButton from '../../../components/commons/share_button';
import shareContent from '../../../utils/share_content';
import PartakerCard from '../../../components/auditions/partaker_card';
import Calendar from '../../../components/commons/calendar_list';
import en from '../../../locale/en';
import Tags from '../../../components/auditions/tags';
import GradientButton from '../../../components/commons/gradient_button';
import PartakerModal from '../../auditions/partaker_modal';
import parseDataModalPartaker from '../../../utils/auditions/parse_data_modal_partaker';
import { Loading } from '../../../components/commons/empty_state';


//Images
import Plus from '../../../assets/icons/auditions/icon-plus.png';
import File from '../../../components/auditions/file';
import FileMenu from '../../../components/auditions/file_menu';
import getFile from '../../../utils/get_file';
import showMessage from '../../../utils/show_message';
import { getPerformanceProfile, getPerformanceCalendar, getUserInstantFeedback, getUserTags, removePastTag, addTagfeedback } from '../../../api/auditions';
import { getContract, createContract, deleteContract, getCommetsTalents, getTagsTalents, getContractUser, sharePerformance } from '../../../api/talents';
import uploadAsset from '../../../utils/upload_asset';
import { uploadSingleFile } from '../../../utils/auditions/upload_documents';
import Share from '../../../components/commons/share';
import validateEmail from '../../../utils/validate_email';


class PartakerDetail extends Component {
	constructor(props) {
		super(props);
		let { navigation } = this.props;
		let showBtn = navigation.getParam('showBtn', true);
		this.state = {
			actualTags: '',
			person: {},
			tags: [],
			files: [],
			comment: '',
			comments: [],
			actualComment: '',
			showFileMenu: false,
			optionsPosition: { py: 0, px: 0 },
			fileSelected: {},
			partakerDetailModal: false,
			partakerDetailModalSectionSelected: 'Info',
			loading: false,
			loadingCustom: false,
			dates: [],
			isEdit: false,
			isUploading: false,
			isDeleting: false,
			showShare: false,
			email: '',
			isSending: false,
			showBtn
		}
	}

	componentDidMount = () => {
		let { navigation } = this.props;
		let person = navigation.getParam('person', null);
		let personId = navigation.getParam('personId', '');
		let audition_id = navigation.getParam('audition_id', '');
		let image = navigation.getParam('image', '');
		let audition = navigation.getParam('audition', {});
		if (person) {
			this.modifyData();
		} else {
			if (personId) {
				this.setState({ loadingCustom: true, user_id: personId, audition_id })
				this.fetchUser(personId, image);
				this.fetchUserCalendar(personId, audition_id);
				this.getContractInformation(personId, audition);
			}
		}
	}

	componentDidUpdate = (prevProps) => {
		let { navigation } = this.props;
		let person = navigation.getParam('person', null);
		let personId = navigation.getParam('personId', '');
		let audition_id = navigation.getParam('audition_id', '');
		let image = navigation.getParam('image', '');
		let personAnt = prevProps.navigation.getParam('person', null);
		let personIdAnt = prevProps.navigation.getParam('personId', '');
		let audition = navigation.getParam('audition', {});
		if (person !== personAnt) {
			this.modifyData();
		}
		if (personId !== personIdAnt) {
			this.setState({ loadingCustom: true, user_id: personId, audition_id })
			this.fetchUser(personId, image);
			this.fetchUserCalendar(personId, audition_id);
			this.getContractInformation(personId, audition);
		}
	}

	fetchUserCalendar = async (personId, audition_id) => {
		try {
			console.log('personId', personId)
			const response = await getPerformanceCalendar(personId);
			this.setState({
				dates: response.data.data
			});
		} catch (error) {
			console.log(error);
		}
		this.validateUserDataExist(personId, audition_id);
	}

	validateUserDataExist = async (personId, audition_id) => {
		try {
			let data = { audition_id, user_id: personId }
			let responseIns = await getUserInstantFeedback(data);
			let responseTag = await getUserTags(data);
			let { comment } = responseIns.data.data;
			let comments = [{ comment }];
			this.setState({
				loadingCustom: false,
				isEdit: true,
				tags: responseTag.data.data,
				comments
			});
		} catch (error) {
			this.setState({ loadingCustom: false, isEdit: false });
		}
	}

	fetchUser = async (user_id, image) => {
		this.setState({ loading: true });
		let audition_id = navigation.getParam('audition_id', '');
		try {
			const response = await getPerformanceProfile(user_id,audition_id);
			const details = response.data.data.details;
			let person = {
				details: {
					first_name: details.first_name,
					last_name: details.last_name,
					city: details.city,
					stage_name: details.stage_name
				},
				image
			}
			const modalData = parseDataModalPartaker(response.data.data);
			this.setState({ modalData, person, loading: false });
			console.log("FETCH USER SUCCESS MANAGER TALENT DATABASE=====>",JSON.stringify(response.data.data));
		} catch (error) {
			console.log("FetchUser ERROR =====>",error);
			this.setState({ loading: false });
		}
	}

	getContractInformation = async (personId, audition_id) => {
		try {
			let contractResponse = await getContract({ personId, audition_id });
			if (contractResponse.data.data.length !== 0 && !contractResponse.data.data.length) {
				this.setState({ files: [contractResponse.data.data] })
			}
		} catch (error) {
			console.log(error)
		}
	}

	modifyData = () => {
		let { navigation } = this.props;
		let person = navigation.getParam('person', {});
		const modalData = parseDataModalPartaker(person);
		this.setState({ modalData, person });
		this.getComments(person);
		this.getTags(person);
		this.getContractByUser(person);
	}

	getComments = async (person) => {
		this.setState({ loading: true });
		let user = { user: person.details.id };
		try {
			let responseComments = await getCommetsTalents(user);
			this.setState({ comments: responseComments.data.data, loading: false })
		} catch (error) {
			console.log(error)
		}
	}

	getTags = async (person) => {
		this.setState({ loading: true });
		let user = { user: person.details.id };
		try {
			let responseTags = await getTagsTalents(user);
			this.setState({ tags: responseTags.data.data, loading: false })
		} catch (error) {
			console.log(error)
		}
	}

	getContractByUser = async (person) => {
		this.setState({ loading: true });
		let user = { user: person.details.id };
		try {
			let responseContracts = await getContractUser(user);
			this.setState({ files: responseContracts.data.data, loading: false })
		} catch (error) {
			console.log(error)
		}
	}

	handlePressBackButton = () => {
		const { navigation } = this.props;
		navigation.goBack();
	}

	shareAsset = async () => {
		try {
			await shareContent(
				'React Native | A framework for building native apps using React'
			);
		} catch (error) {
			console.log(error);
		}
	}

	addTags = async (state, value) => {
		let { audition_id, user_id, isEdit } = this.state;
		if (value !== '') {
			let actualTag = [...this.state[state]];
			actualTag.push(value);
			this.setState({ actualTags: '', [state]: actualTag })
			try {
				if (isEdit) {
					let valor = await addTagfeedback({
						audition_id,
						user_id,
						title: value.title
					})
					let actualTag2 = [...this.state[state]];
					actualTag2[actualTag2.length - 1] = valor.data.data;
					this.setState({ [state]: actualTag2 });
				}
			} catch (error) {
				console.log(error)
			}
		}
	}

	onChange = (value, state) => {
		this.setState({ [state]: value })
	}

	addComment = (comment) => {
		if (comment) {
			this.setState({ actualComment: comment, comment: '' });
		}
	}

	removeComment = () => {
		this.setState({ actualComment: '' })
	}

	handleSetRef = (file, index) => {
		this[`file_${index}`] = file;
	};

	onPressFileMore = async (file, index) => {
		try {
			const { showFileMenu } = this.state;

			await this.handleGetMeasure(file, index);

			this.setState({
				showFileMenu: !showFileMenu
			});
		} catch (error) { }
	};

	handleGetMeasure = async (file, index) => {
		await new Promise((resolve, reject) => {
			const refName = `file_${index}`;
			if (refName in this) {
				this[refName].measure((a, b, width, height, px, py) => {
					this.setState(
						{
							optionsPosition: { py, px },
							fileSelected: file
						},
						() => resolve(true)
					);
				});
			} else {
				this.setState(
					{
						optionsPosition: { py: 0, px: 0 },
						fileSelected: file
					},
					() => resolve(false)
				);
			}
		});
	};

	handleDeleteFile = async file => {
		let { files, fileSelected } = this.state;
		this.setState({ isDeleting: true })
		await deleteContract(fileSelected.id)
		const index = files.indexOf(file);
		files.splice(index, 1);
		this.setState({ isDeleting: false })
		this.onPressFileMore({});
	};

	handlePressUploadDocument = async () => {
		try {
			let { files, user_id } = this.state;
			let { navigation } = this.props;
			let audition = navigation.getParam('audition', {});
			const file = await getFile(3, 'pdf');
			this.setState({ isUploading: true });
			let reference = [...files];
			file.shareable = 'yes';
			let url = await uploadSingleFile(file);
			console.log(url)
			try {
				let responseContract = await createContract({
					url: url.url,
					audition,
					performer: user_id
				});
				reference = [responseContract.data.data];
				Toast.show({
					text: 'Contract uploaded successfully',
					buttonText: 'Okay'
				})
				this.setState({ files: reference, isUploading: false });
			} catch (error) {
				this.setState({ isUploading: false })
			}
		} catch (error) {
			if (typeof error === 'object' && 'errorCode' in error) {
				showMessage('danger', error.string);
			}
			console.log('error', error);
		}
	};

	shareFile = async (file) => {
		try {
			await shareContent(
				`Open this link: \n ${file.url}`
			);
		} catch (error) {
			console.log(error);
		}
	}

	handlePressPartakerSection = (section = null) => {
		const { partakerDetailModal, } = this.state;
		this.setState({
			partakerDetailModal: !partakerDetailModal,
			partakerDetailModalSectionSelected: section
		});
	}

	removeTags = (index, state) => {
		let { isEdit } = this.state;
		let actualTag = [...this.state[state]];
		if (actualTag[index].id && actualTag[index].audition_id && isEdit) {
			removePastTag(actualTag[index].id);
		}
		actualTag.splice(index, 1);
		this.setState({ [state]: actualTag })
	}

	handleShare = () => {
		this.setState(prevState => {
			return { showShare: !prevState.showShare }
		})
	}

	handleEmail = email => {
		this.setState({ email })
	}

	sendInvitation = async () => {
		let { email, isSending, person } = this.state;
		if (isSending) {
			return;
		}
		this.setState({ isSending: true });
		if (!validateEmail(email)) {
			this.setState({ isSending: false });
			return showMessage('danger', en.valid_email);
		}
		try {
			await sharePerformance({
				code: person.share_code,
				email
			})
			this.setState({ isSending: false });
			this.handleShare();
			showMessage('success', en.performer_shared);
		} catch (error) {
			this.setState({ isSending: false });
			showMessage('danger', 'There was an error sending the code');
		}
	}

	render() {
		const { tags, actualTags, actualComment, comment, files, showFileMenu, optionsPosition,
			fileSelected, partakerDetailModalSectionSelected, partakerDetailModal, modalData,
			person, loading, dates, user_id, loadingCustom, isUploading, isDeleting, comments,
			showShare, email, isSending, showBtn } = this.state;
		if (Object.entries(person).length !== 0 && !loading && !loadingCustom) {
			return (
				<Container style={styles.container}>
					<LightBar />

					{/* Modal to selecten file */}
					<FileMenu
						show={showFileMenu}
						dismiss={() => this.onPressFileMore({})}
						posy={optionsPosition.py}
						posx={optionsPosition.px}
						hasPadding={false}
						showOptionDelete
						isLoading={isDeleting}
						handleFileShare={this.handleFileShare}
						delete={() => this.handleDeleteFile(fileSelected)}
					>
						{fileSelected && (
							<File
								activeOpacity={1}
								filename={'PDF'}
								fileType={'pdf'}
								onPressMore={() => this.onPressFileMore({})}
							/>
						)}
					</FileMenu>
					<View style={{ position: 'absolute', zIndex: 2 }}>
						<Header
							left={<BackButton onPress={() => this.handlePressBackButton()} />}
							right={person.share_code ? <ShareButton onPress={this.handleShare} /> : null}
						/>
					</View>
					{/* modal to share performance */}
					<Share
						sendInvitation={this.sendInvitation}
						isSending={isSending}
						handleEmail={this.handleEmail}
						email={email}
						handleShare={this.handleShare}
						showShare={showShare}
						headerHeight={headerHeight}
					/>
					<KeyboardAvoidingView behavior='position' style={styles.containerBody}>
						<View style={styles.insideContainer}>
							<View style={styles.element}>
								<PartakerCard
									name={`${person.details.first_name} ${person.details.last_name}`}
									company={person.details.stage_name ? person.details.stage_name : ''}
									country={person.details.city}
									image={person.image}
									onPressInfo={() => this.handlePressPartakerSection(en.info_title)}
									onPressCredits={() => this.handlePressPartakerSection(en.credits_title)}
									onPressEducation={() => this.handlePressPartakerSection(en.education_title)}
									onPressAppearance={() => this.handlePressPartakerSection(en.appearance_title)}
								/>
							</View>
							<View style={styles.element} >

								{/* Calendar */}
								<View style={styles.availabilityContainer}>
									<Text style={styles.titles}>{en.available_title}</Text>
									<Calendar
										calendarWidth={styles.availabilityContainer.width}
										dates={person.calendar}
									/>
								</View>

								{/* Tags */}
								<View style={styles.tagsContainer}>
									<Text style={styles.titles}> {en.tags_title} </Text>
									<ScrollView showsVerticalScrollIndicator={false} bounces={false}>
										{
											tags.map((snap) => (
												<Tags showDelete={false} title={snap.title} />
											))
										}
										{tags.length === 0 && <Text style={styles.emptyText}>{en.empty_tags}</Text>}
									</ScrollView>
								</View>
							</View>
							<View style={styles.element} >

								{/* Contract Information  */}
								<View style={styles.availabilityContainer}>
									<Text style={styles.titles}>{en.contract_title}</Text>
									<ScrollView style={styles.media}>
										{files.map((file, index) => {
											return (
												<File
													key={index}
													refs={fileRef => this.handleSetRef(fileRef, index)}
													filename={'PDF'}
													fileType={'pdf'}
													onPressMore={() =>
														this.onPressFileMore(file, index)
													}
													containerStyle={styles.fileContainer}
												/>
											);
										})}
									</ScrollView>
									{showBtn
										? <GradientButton
											light
											buttonText={en.updload_button_text}
											onPress={() => this.handlePressUploadDocument()}
											containerStyle={styles.buttonSaveContainer}
											gradientStyle={styles.buttonSaveGradient}
											textStyle={[styles.buttonSaveText, { color: '#5f2543' }]}
											loading={isUploading}
											spinnerColor='black'
										/>
										: null
									}
								</View>
								{/* Comments */}
								<View style={styles.availabilityContainer}>
									<Text style={styles.titles}>{en.comment_title}</Text>
									{
										comments.map((snap) => (
											<Tags showDelete={false} title={snap.comment} />
										))
									}
									{comments.length === 0 && <Text style={styles.emptyText}>{en.empty_comments}</Text>}
								</View>
							</View>
						</View>
					</KeyboardAvoidingView>
					<PartakerModal
						show={partakerDetailModal}
						scrollTo={partakerDetailModalSectionSelected}
						dismiss={() => this.handlePressPartakerSection(en.info_title)}
						data={modalData}
					/>
				</Container>
			);
		} else {
			return <Loading />;
		}

	}
}

const mapStateToProps = (state) => ({
	user: state.user.data
});

export default connect(mapStateToProps)(PartakerDetail);
