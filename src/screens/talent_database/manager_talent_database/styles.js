import { StyleSheet, Platform } from "react-native";

//custom
import {
  resize,
  NEXABOLD,
  NEXALIGHT,
  STATUS_BAR_HEIGHT,
  width,
  height
} from "../../../assets/styles";

export const headerHeight =
  Platform.OS === "ios"
    ? resize(82 + 20, "height")
    : resize(82 + STATUS_BAR_HEIGHT, "height");

const styles = StyleSheet.create({
  container: {
    width,
    height
  },
  containerBody: {
    flex: 1,
    marginHorizontal: resize(20),
    marginVertical: resize(20, "height"),
    paddingTop: headerHeight
  },
  insideContainer: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  availabilityContainer: {
    height: "48%",
    width: "100%",
    shadowColor: "rgba(0, 0, 0, 0.4)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "#d6d6d6",
    paddingTop: resize(18, "height")
  },
  titles: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: 20,
    fontWeight: "400",
    textAlign: "center",
    marginBottom: resize(5, "height")
  },
  tagsContainer: {
    height: "48%",
    width: "100%",
    shadowColor: "rgba(0, 0, 0, 0.4)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "#d6d6d6",
    paddingVertical: resize(18, "height"),
    paddingHorizontal: resize(15)
  },
  comentInput: {
    width: "92%",
    height: resize(50, "height"),
    marginTop: resize(30, "height"),
    flexDirection: "row",
    paddingHorizontal: resize(10),
    borderWidth: 4,
    borderColor: "#5f2543",
    borderRadius: resize(30),
    alignItems: "center",
    alignSelf: "center",
    marginBottom: resize(10, "height")
  },
  addTag: {
    width: resize(30),
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  emptyText: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(17),
    fontWeight: "400",
    textAlign: "center"
  },
  element: {
    width: "32%",
    height: "100%"
    // justifyContent: "space-between"
  },
  teamFeedbackContent: {
    marginTop: resize(9, "height")
  },
  fileContainer: {
    flexDirection: "row",
    width: "90%",
    marginHorizontal: resize(10),
    height: resize(66, "height"),
    borderRadius: 10,
    backgroundColor: "#ffffff",
    marginBottom: resize(21, "height"),
    marginRight: resize(31),
    borderColor: "#d6d6d6",
    shadowColor: "rgba(0, 0, 0, 0.4)",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 3
  },
  buttonSaveContainer: {
    width: "70%",
    alignSelf: "flex-end",
    height: resize(50, "height"),
    borderRadius: resize(40),
    borderColor: "#5f2543",
    borderWidth: 4,
    marginBottom: resize(20, "height"),
    marginHorizontal: resize(10)
  },
  media: {
    marginVertical: resize(10, "height"),
    width: "100%",
    height: "45%"
  },
  buttonSaveGradient: {
    width: "100%",
    height: "100%",
    borderRadius: 28,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonSaveText: {
    color: "#ffffff",
    fontFamily: NEXABOLD,
    fontSize: resize(17)
  },
  auditionVideoItemContainer: {
    flexDirection: "row",
    // backgroundColor: "red",
    // width: width * 0.893333333333,
    width: "100%",
    height: resize(80)
  },
  auditionVideoImageContainer: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    borderBottomLeftRadius: 19,
    backgroundColor: "#f1f1f1",
    overflow: "hidden",
    alignSelf: "flex-end",
    height: resize(70),
    width: "30%",
    marginBottom: 10
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  videoTitle: { fontFamily: NEXABOLD, fontSize: resize(16), color: "#4d2545" },
  numberOfVideos: { fontFamily: NEXALIGHT, marginTop: 10, color: "#4d2545" },
  lateralLayout: {
    position: "absolute",
    zIndex: 2,
    width: resize(425),
    height: resize(765, "height"),
    flexDirection: "row",
    marginTop: headerHeight,
    backgroundColor: "#fff"
  },
  spiltContainer: {
    width: resize(22),
    height: resize(752, "height"),
    backgroundColor: "#bfbfbf",
    alignItems: "center",
    justifyContent: "center"
  },
  spiltIcon: {
    width: resize(1),
    height: resize(40, "height"),
    borderColor: "#707070",
    borderWidth: resize(2.5),
    borderRadius: resize(30)
  },
  auditionVideosContent: {
    paddingLeft: resize(39),
    paddingRight: resize(41),
    paddingVertical: resize(10, "height")
  },
  videosContainer: {
    marginBottom: resize(125, "height")
  },
  ShareStyle: {
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: 'white',
    // borderWidth: 0.5,
    // borderColor: '#fff',
    //height: 40,
    width: 150,
    // borderRadius: 5,
    margin: 5,
    top: 10,
    marginLeft: 25
  },
  OpenIn: {
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: 'white',
    // borderWidth: 0.5,
    // borderColor: '#fff',
    // height: 40,
    width: 150,
    //borderRadius: 5,
    margin: 5,
    // top: 10,
    right: 3,
    marginLeft: 25
  },
  ImageIconStyle: {
    // padding: 15,
    margin: 5,
    height: 22,
    width: 22,
    marginLeft: 20,
    resizeMode: "contain"
  }
});

export default styles;
