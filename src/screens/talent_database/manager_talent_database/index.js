import React, { Component } from "react";
import {
  View,
  Text,
  Platform,
  ScrollView,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  FlatList,
  TouchableOpacity,
  Animated,
  Share as RNShare,
} from "react-native";
import { Container, Input, Toast, Card, Content, Button } from "native-base";
import { connect } from "react-redux";
import styles, { headerHeight } from "./styles";

//Custom
import LightBar from "../../../components/commons/light_bar";
import BackButton from "../../../components/commons/back_button";
import Header from "../../../components/commons/header";
import ShareButton from "../../../components/commons/share_button";
import shareContent from "../../../utils/share_content";
import PartakerCard from "../../../components/auditions/partaker_card";
import Calendar from "../../../components/commons/calendar_list";
import en from "../../../locale/en";
import Tags from "../../../components/auditions/tags";
import GradientButton from "../../../components/commons/gradient_button";
import PartakerModal from "../../auditions/partaker_modal";
import parseDataModalPartaker from "../../../utils/auditions/parse_data_modal_partaker";
import { Loading } from "../../../components/commons/empty_state";

//Images
import Plus from "../../../assets/icons/auditions/icon-plus.png";
import File from "../../../components/auditions/file";
import FileMenu from "../../../components/auditions/file_menu";
import getFile from "../../../utils/get_file";
import showMessage from "../../../utils/show_message";
import {
  getPerformanceProfile,
  getPerformanceCalendar,
  getUserInstantFeedback,
  getUserTags,
  removePastTag,
  addTagfeedback,
  renameAuditionVideo,
} from "../../../api/auditions";
import {
  getContract,
  createContract,
  deleteContract,
  getCommetsTalents,
  getTagsTalents,
  getContractUser,
  sharePerformance,
  getAuditionVideoListByPerformer,
  getVideoListByAudition,
} from "../../../api/talents";
import uploadAsset from "../../../utils/upload_asset";
import { uploadSingleFile } from "../../../utils/auditions/upload_documents";
import Share from "../../../components/commons/share";
import validateEmail from "../../../utils/validate_email";
import { height, resize } from "../../../assets/styles";
import settings from "../../settings";
import { webUrl } from "../../../../settings";
import Base64 from "../../../../Base64";
import { Assets } from "../../../assets";
import RenameModal from "../../../components/auditions/rename_file";
import colors from "../../../utils/colors";
import parseError from "../../../utils/parse_error";
import Music from "../allMediaModal/Music";
const DEVICE_HEIGHT = Math.round(Dimensions.get("window").height);

class PartakerDetail extends Component {
  constructor(props) {
    super(props);

    this.feedPost = [];
    this.positionX = 0;
    this.positionY = 0;
    let { navigation } = this.props;
    let showBtn = navigation.getParam("showBtn", true);
    console.log(" CODE WORKING HERE........ 222222");
    this.state = {
      actualTags: "",
      person: {},
      tags: [],
      files: [],
      comment: "",
      comments: [],
      actualComment: "",
      showFileMenu: false,
      optionsPosition: { py: 0, px: 0 },
      fileSelected: {},
      partakerDetailModal: false,
      partakerDetailModalSectionSelected: "Info",
      loading: false,
      loadingCustom: false,
      dates: [],
      isEdit: false,
      isUploading: false,
      isDeleting: false,
      showShare: false,
      email: "",
      isSending: false,
      showBtn,
      auditionVideoList: [],
      videoListByAudition: [],
      viewSplitAnimatedValueA: new Animated.Value(0),
      viewSplitAnimatedValue: new Animated.Value(255),
      isOpen: true,
      isShowPopup: false,
      clickedAudition: null,
      personId: null,
      isVisible: false,
      fileName: "",
      audition_id: "",
      isMusicModal: false,
      isSheetMusicModal: false,
      isVideoModal: false,
      isPhotoModal: false,
      isAuditionMaterialModal: false,
      isResumeAndDocModal: false,
      selectedMediaType: "",
      modalTitle: ""
    };
  }

  componentDidMount = () => {
    let { navigation } = this.props;
    let person = navigation.getParam("person", null);
    let personId = navigation.getParam("personId", "");
    let audition_id = navigation.getParam("audition_id", "");
    let image = navigation.getParam("image", "");
    let audition = navigation.getParam("audition", {});
    this.state.audition_id = audition_id;
    if (person) {
      this.modifyData();
    } else {
      if (personId) {
        this.setState({ loadingCustom: true, user_id: personId, audition_id });
        console.log("componentDidMount USER ID=====>", this.state.user_id);
        this.fetchUser(personId, image);
        this.fetchUserCalendar(personId, audition_id);
        this.getContractInformation(personId, audition);
        this.getAuditionVideoByPerformer(personId);
      }
    }
    //by default close side menu
    this.handleSideMenu();
  };

  componentDidUpdate = (prevProps) => {
    let { navigation } = this.props;
    let person = navigation.getParam("person", null);
    let personId = navigation.getParam("personId", "");
    let audition_id = navigation.getParam("audition_id", "");
    let image = navigation.getParam("image", "");
    let personAnt = prevProps.navigation.getParam("person", null);
    let personIdAnt = prevProps.navigation.getParam("personId", "");
    let audition = navigation.getParam("audition", {});
    console.log("componentDidUpdate personId=====>", personId);
    if (person !== personAnt) {
      this.modifyData();
    }
    if (personId !== personIdAnt) {
      this.setState({ loadingCustom: true, user_id: personId, audition_id });
      this.fetchUser(personId, image);
      this.fetchUserCalendar(personId, audition_id);
      this.getContractInformation(personId, audition);
      this.getAuditionVideoByPerformer(personId);
      console.log("componentDidUpdate user id=====>", this.state.user_id);
    }
  };

  fetchUserCalendar = async (personId, audition_id) => {
    try {
      console.log("personId", personId);
      const response = await getPerformanceCalendar(personId);
      this.setState({
        dates: response.data.data,
      });
    } catch (error) {
      console.log(error);
    }
    this.validateUserDataExist(personId, audition_id);
  };

  validateUserDataExist = async (personId, audition_id) => {
    try {
      let data = { audition_id, user_id: personId };
      let responseIns = await getUserInstantFeedback(data);
      let responseTag = await getUserTags(data);
      let { comment } = responseIns.data.data;
      let comments = [{ comment }];
      this.setState({
        loadingCustom: false,
        isEdit: true,
        tags: responseTag.data.data,
        comments,
      });
    } catch (error) {
      this.setState({ loadingCustom: false, isEdit: false });
    }
  };



  fetchUser = async (user_id, image) => {
    this.setState({ loading: true });
    let audition_id = this.props.navigation.getParam("audition_id", "");
    try {
      const response = await getPerformanceProfile(user_id, audition_id);
      const details = response.data.data.details;
      console.log('details :>> ', details);
      let person = {
        details: {
          first_name: details.first_name,
          last_name: details.last_name,
          city: details.city,
          stage_name: details.stage_name,
        },
        image,
      };
      const modalData = parseDataModalPartaker(response.data.data);
      console.log('modalData :>> ', modalData);
      this.setState({ modalData, person, loading: false });
      console.log(
        "FETCH USER SUCCESS MANAGER TALENT DATABASE=====>",
        JSON.stringify(response.data.data)
      );
    } catch (error) {
      console.log("FetchUser ERROR =====>", error);
      this.setState({ loading: false });
    }
  };

  getContractInformation = async (personId, audition_id) => {
    try {
      let contractResponse = await getContract({ personId, audition_id });
      if (
        contractResponse.data.data.length !== 0 &&
        !contractResponse.data.data.length
      ) {
        this.setState({ files: [contractResponse.data.data] });
      }
    } catch (error) {
      console.log(error);
    }
  };
  getAuditionVideoByPerformer = async (personId) => {
    try {
      let auditionVideoResponse = await getAuditionVideoListByPerformer(
        personId
      );
      if (auditionVideoResponse) {
        if (auditionVideoResponse.data) {
          if (auditionVideoResponse.data.data) {
            this.setState({
              auditionVideoList: auditionVideoResponse.data.data,
            });
          }
        }
      }
    } catch (error) {
      console.log(error);
      this.setState({ auditionVideoList: [] });
    }
  };

  getVideoList = async (audition_id) => {
    let { user_id } = this.state;
    let { navigation } = this.props;
    let personId = navigation.getParam("personId", "");
    try {
      let user = Object.keys(this.state.person).length;
      if (user !== 0) {
        let videoListResponse = await getVideoListByAudition(
          audition_id,
          personId || this.state.person.details.user_id
          // this.state.person.details.id
        );
        console.log("this.state.person.details.user_id=====>", personId);
        // console.log(
        //   "this.state.person.details.user_id=====>",
        //   this.state.person.details.user_id
        // );
        if (videoListResponse) {
          if (videoListResponse.data) {
            if (videoListResponse.data.data) {
              console.log(
                "video list by audition=======>>>",
                videoListResponse.data.data
              );
              console.log(JSON.stringify(videoListResponse));
              this.setState({
                videoListByAudition: videoListResponse.data.data,
                audition_id: audition_id,
                // isOpen: true
              });
              if (!this.state.isOpen) this.handleSideMenu();
            }
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  modifyData = () => {
    let { navigation } = this.props;
    let person = navigation.getParam("person", {});
    const modalData = parseDataModalPartaker(person);
    this.setState({ modalData, person });
    this.getComments(person);
    this.getTags(person);
    this.getContractByUser(person);
    if (person.details) {
      if (person.details.user_id) {
        this.getAuditionVideoByPerformer(person.details.user_id);
      }
    }
  };

  getComments = async (person) => {
    this.setState({ loading: true });
    // let user = { user: person.details.id };
    let user = { user: person.details.user_id };

    try {
      let responseComments = await getCommetsTalents(user);
      this.setState({ comments: responseComments.data.data, loading: false });
    } catch (error) {
      console.log(error);
    }
  };

  getTags = async (person) => {
    this.setState({ loading: true });
    let user = {
      // user: person.details.id
      user: person.details.user_id,
    };
    try {
      let responseTags = await getTagsTalents(user);
      this.setState({ tags: responseTags.data.data, loading: false });
    } catch (error) {
      console.log(error);
    }
  };

  getContractByUser = async (person) => {
    this.setState({ loading: true });
    let user = { user: person.details.id };
    try {
      let responseContracts = await getContractUser(user);
      this.setState({ files: responseContracts.data.data, loading: false });
    } catch (error) {
      console.log(error);
    }
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  shareAsset = async () => {
    try {
      await shareContent(
        "React Native | A framework for building native apps using React"
      );
    } catch (error) {
      console.log(error);
    }
  };

  addTags = async (state, value) => {
    let { audition_id, user_id, isEdit } = this.state;
    if (value !== "") {
      let actualTag = [...this.state[state]];
      actualTag.push(value);
      this.setState({ actualTags: "", [state]: actualTag });
      try {
        if (isEdit) {
          let valor = await addTagfeedback({
            audition_id,
            user_id,
            title: value.title,
          });
          let actualTag2 = [...this.state[state]];
          actualTag2[actualTag2.length - 1] = valor.data.data;
          this.setState({ [state]: actualTag2 });
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  onChange = (value, state) => {
    this.setState({ [state]: value });
  };

  addComment = (comment) => {
    if (comment) {
      this.setState({ actualComment: comment, comment: "" });
    }
  };

  removeComment = () => {
    this.setState({ actualComment: "" });
  };

  handleSetRef = (file, index) => {
    this[`file_${index}`] = file;
  };

  onPressFileMore = async (file, index) => {
    try {
      const { showFileMenu } = this.state;

      await this.handleGetMeasure(file, index);

      this.setState({
        showFileMenu: !showFileMenu,
      });
    } catch (error) { }
  };

  handleGetMeasure = async (file, index) => {
    await new Promise((resolve, reject) => {
      const refName = `file_${index}`;
      if (refName in this) {
        this[refName].measure((a, b, width, height, px, py) => {
          this.setState(
            {
              optionsPosition: { py, px },
              fileSelected: file,
            },
            () => resolve(true)
          );
        });
      } else {
        this.setState(
          {
            optionsPosition: { py: 0, px: 0 },
            fileSelected: file,
          },
          () => resolve(false)
        );
      }
    });
  };

  handleDeleteFile = async (file) => {
    let { files, fileSelected } = this.state;
    this.setState({ isDeleting: true });
    await deleteContract(fileSelected.id);
    const index = files.indexOf(file);
    files.splice(index, 1);
    this.setState({ isDeleting: false });
    this.onPressFileMore({});
  };

  handlePressUploadDocument = async () => {
    try {
      let { files, user_id } = this.state;
      let { navigation } = this.props;
      let audition = navigation.getParam("audition", {});
      const file = await getFile(3, "pdf");
      this.setState({ isUploading: true });
      let reference = [...files];
      file.shareable = "yes";
      let url = await uploadSingleFile(file);
      console.log(url);
      try {
        let responseContract = await createContract({
          url: url.url,
          audition,
          performer: user_id,
        });
        reference = [responseContract.data.data];
        Toast.show({
          text: "Contract uploaded successfully",
          buttonText: "Okay",
        });
        this.setState({ files: reference, isUploading: false });
      } catch (error) {
        this.setState({ isUploading: false });
      }
    } catch (error) {
      if (typeof error === "object" && "errorCode" in error) {
        showMessage("danger", error.string);
      }
      console.log("error", error);
    }
  };

  shareFile = async (file) => {
    try {
      await shareContent(`Open this link: \n ${file.url}`);
    } catch (error) {
      console.log(error);
    }
  };

  handlePressPartakerSection = (section = null) => {
    const { partakerDetailModal } = this.state;
    this.setState({
      partakerDetailModal: !partakerDetailModal,
      partakerDetailModalSectionSelected: section,
    });
  };

  removeTags = (index, state) => {
    let { isEdit } = this.state;
    let actualTag = [...this.state[state]];
    if (actualTag[index].id && actualTag[index].audition_id && isEdit) {
      removePastTag(actualTag[index].id);
    }
    actualTag.splice(index, 1);
    this.setState({ [state]: actualTag });
  };

  handleShare = () => {
    this.setState((prevState) => {
      return { showShare: !prevState.showShare };
    });
  };

  handleEmail = (email) => {
    this.setState({ email });
  };

  sendInvitation = async () => {
    let { email, isSending, person } = this.state;
    console.log("person :", person.details.user_id);
    console.log(
      "person :",
      webUrl + "/talent-shared/" + Base64.btoa(String(person.details.user_id))
    );
    if (isSending) {
      return;
    }
    this.setState({ isSending: true });
    if (!validateEmail(email)) {
      this.setState({ isSending: false });
      return showMessage("danger", en.valid_email);
    }
    try {
      await sharePerformance({
        code: person.share_code,
        email,
        link:
          webUrl +
          "/talent-shared/" +
          Base64.btoa(String(person.details.user_id)),
      });
      this.setState({ isSending: false });
      this.handleShare();
      showMessage("success", en.performer_shared);
    } catch (error) {
      this.setState({ isSending: false });
      showMessage("danger", "There was an error sending the code");
    }
  };
  renderPerformerAuditionVideoItem = ({ item, index }) => {
    return (
      <Card style={{ width: "100%" }}>
        <TouchableOpacity
          style={styles.auditionVideoItemContainer}
          onPress={() => {
            if (item.videos > 0) {
              this.getVideoList(item.id);
            }
          }}
        >
          <View style={{ justifyContent: "center", flex: 1, marginLeft: 20 }}>
            <Text style={styles.videoTitle}>{item.title}</Text>
            <Text style={styles.numberOfVideos}>{`${item.videos} Videos`}</Text>
          </View>

          <View style={styles.auditionVideoImageContainer}>
            <Image style={styles.image} source={{ uri: item.cover }} />
          </View>
        </TouchableOpacity>
      </Card>
    );
  };
  getAuditionVideoList() {
    return (
      <FlatList
        style={{ marginHorizontal: 20, marginBottom: 10 }}
        showsVerticalScrollIndicator={false}
        data={this.state.auditionVideoList}
        extraData={this.state}
        nestedScrollEnabled={true}
        renderItem={this.renderPerformerAuditionVideoItem}
        keyExtractor={(item, index) => item + index}
      />
    );
  }
  handleSideMenu = () => {
    const {
      viewSplitAnimatedValueA,
      isOpen,
      viewSplitAnimatedValue,
    } = this.state;
    const toValue = !isOpen ? 0 : -resize(405);
    const toValueP = isOpen ? 0 : resize(255);

    Animated.timing(viewSplitAnimatedValue, {
      toValue: toValueP,
      duration: 300,
    }).start();

    Animated.timing(viewSplitAnimatedValueA, {
      toValue: toValue,
      duration: 300,
    }).start(() => {
      this.setState((prevState) => ({
        isOpen: !prevState.isOpen,
      }));
    });
  };
  ShowModalFunction(visible) {
    this.setState({ isShowPopup: false });
  }
  shareTextMessage(message, item) {
    RNShare.share(
      {
        message:
          "Video shared from Gig2Gig Casting " + this.state.clickedAudition.url,
        url:
          "Video shared from Gig2Gig Casting " + this.state.clickedAudition.url, //'http://bam.tech',
        title: "Video shared from Gig2Gig Casting",
      },
      {
        // Android only:
        dialogTitle: "Share BAM goodness",
        // iOS only:
        excludedActivityTypes: ["com.apple.UIKit.activity.PostToTwitter"],
      }
    );
  }
  onCloseClick = () => {
    this.setState({ isVisible: false });
  };

  onSubmitClick = async () => {
    console.log("audition_id :", this.state.audition_id);
    this.setState({ loading: true });
    try {
      let params = {
        id: this.state.clickedAudition.id,
        name: this.state.fileName,
        audition_id: this.state.audition_id,
      };
      const response = await renameAuditionVideo(params);
      console.log("response :", response);
      // this.props.onVideoDeleteSuccess();
      this.getVideoList(this.state.audition_id);
      this.setState({ loading: false, isVisible: false, isShowPopup: false });
    } catch (error) {
      this.setState({ loading: false, isVisible: false });
      console.log(error.response);
      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }
    // this.setState({ isVisible: false })
  };

  onChangeText = (fileName) => {
    this.setState({ fileName });
  };

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    );
  };
  showPopup() {
    // console.log(" CHECK Y POSITION ", this.positionY);
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "flex-end",
          position: "absolute",
          left: resize(100),
          top: this.positionY,
          width: resize(140),
          height: 50,
          //borderColor: "blue",
          borderWidth: 2,
          zIndex: 999,
        }}
      >
        <View
          style={{
            // height: 150,
            height: resize(150),
            // right: "10%",
            // justifyContent: "center",
            alignItems: "center",
            backgroundColor: "white",
            // width: 190,
            width: resize(140),
            borderRadius: 10,
            borderWidth: 1,
            position: "absolute",
            // borderColor: "white",
          }}
        >
          <View>
            {/* {SHARE} */}
            <TouchableOpacity
              style={styles.ShareStyle}
              activeOpacity={0.5}
              onPress={() => {
                this.shareTextMessage(
                  this,
                  this.state.message,
                  this.state.clickedAudition
                );
                //alert(JSON.stringify(this.state.currObj))
                setTimeout(() => {
                  this.ShowModalFunction(false);
                }, 5000);
              }}
            >
              <Image
                source={require("../../../assets/icons/auditions/share-icon.png")}
                style={styles.ImageIconStyle}
              />
              <Text style={styles.TextStyle}> Share </Text>
            </TouchableOpacity>

            {/* {OPEN IN} */}
            <TouchableOpacity
              style={styles.OpenIn}
              activeOpacity={0.5}
              onPress={() => {
                console.log(" CHECK LIST PROPS ", this.props);
                const { navigation } = this.props;
                //Pass dynamic video URL
                if (this.state.clickedAudition !== null) {
                  console.log(" check url  ", this.state.clickedAudition.url);
                  navigation.navigate("VideoPlayerOpen", {
                    video_url: this.state.clickedAudition.url
                      ? this.state.clickedAudition.url
                      : "",
                  });
                }
                setTimeout(() => {
                  this.ShowModalFunction(false);
                }, 5000);
              }}
            >
              <Image
                source={require("../../../assets/icons/auditions/open-in-icon.png")}
                style={styles.ImageIconStyle}
              />
              <Text style={styles.TextStyle}> Open in </Text>
            </TouchableOpacity>

            {/* {OPEN IN} */}
            <TouchableOpacity
              style={styles.OpenIn}
              activeOpacity={0.5}
              onPress={() => {
                console.log("this.state.current :", this.state.clickedAudition);
                this.setState({
                  isVisible: true,
                  fileName: this.state.clickedAudition.name,
                });
              }}
            >
              <Image
                source={Assets.rename}
                style={[
                  styles.ImageIconStyle,
                  { width: 18, height: 18, tintColor: colors.PRIMARY },
                ]}
              />
              <Text style={[styles.TextStyle, { marginLeft: 5 }]}>
                {" "}
                Rename{" "}
              </Text>
            </TouchableOpacity>
          </View>
          <Button
            title="Click Here To Hide Modal"
            onPress={() => {
              this.ShowModalFunction(!this.state.ModalVisibleStatus);
            }}
          />
        </View>
      </View>
    );
  }

  render() {
    const {
      tags,
      actualTags,
      actualComment,
      comment,
      files,
      showFileMenu,
      optionsPosition,
      fileSelected,
      partakerDetailModalSectionSelected,
      partakerDetailModal,
      modalData,
      person,
      loading,
      dates,
      user_id,
      loadingCustom,
      isUploading,
      isDeleting,
      comments,
      showShare,
      email,
      isSending,
      showBtn,
      viewSplitAnimatedValueA,
      viewSplitAnimatedValue,
    } = this.state;
    const widthFirstContainerA = viewSplitAnimatedValueA.interpolate({
      inputRange: [-resize(405), 0],
      outputRange: [-resize(405), 0],
    });
    const widthFirstContainer = viewSplitAnimatedValue.interpolate({
      inputRange: [0, resize(255)],
      outputRange: [0, resize(255)],
    });
    if (Object.entries(person).length !== 0 && !loading && !loadingCustom) {
      return (
        <Container style={styles.container} nestedScrollEnabled={true}>
          <LightBar />
          {this.renderRenameModal()}
          {/* Modal to selecten file */}
          <FileMenu
            show={showFileMenu}
            dismiss={() => this.onPressFileMore({})}
            posy={optionsPosition.py}
            posx={optionsPosition.px}
            hasPadding={false}
            showOptionDelete
            isLoading={isDeleting}
            handleFileShare={this.handleFileShare}
            delete={() => this.handleDeleteFile(fileSelected)}
          >
            {fileSelected && (
              <File
                activeOpacity={1}
                filename={"PDF"}
                fileType={"pdf"}
                onPressMore={() => this.onPressFileMore({})}
              />
            )}
          </FileMenu>
          <View style={{ position: "absolute", zIndex: 2 }}>
            <Header
              left={<BackButton onPress={() => this.handlePressBackButton()} />}
              right={
                person.share_code ? (
                  <ShareButton onPress={this.handleShare} />
                ) : null
              }
            />
          </View>
          {/* modal to share performance */}
          <Share
            sendInvitation={this.sendInvitation}
            isSending={isSending}
            handleEmail={this.handleEmail}
            email={email}
            handleShare={this.handleShare}
            showShare={showShare}
            headerHeight={headerHeight}
          />
          <KeyboardAvoidingView
            behavior="position"
            style={styles.containerBody}
          >
            <View style={styles.insideContainer}>
              <View style={styles.element}>
                <PartakerCard
                  name={`${person.details.first_name} ${person.details.last_name}`}
                  company={
                    person.details.stage_name ? person.details.stage_name : ""
                  }
                  details={person.details}
                  country={person.details.city}
                  image={person.image}
                  onPressInfo={() =>
                    this.handlePressPartakerSection(en.info_title)
                  }
                  onPressCredits={() =>
                    this.handlePressPartakerSection(en.credits_title)
                  }
                  onPressEducation={() =>
                    this.handlePressPartakerSection(en.education_title)
                  }
                  onPressAppearance={() =>
                    this.handlePressPartakerSection(en.appearance_title)
                  }
                  onPressResumeAndDoc={() =>
                    // this.setState({ isResumeAndDocModal: true })
                    this.setState({ isMusicModal: true, selectedMediaType: "doc", modalTitle: "Resume & Docs" })
                  }
                  onPressMusic={() => this.setState({ isMusicModal: true, selectedMediaType: "audio", modalTitle: "Music" })}
                  onPressVideo={() => this.setState({ isMusicModal: true, selectedMediaType: "video", modalTitle: "Video" })}
                  onPressSheetMusic={() =>
                    // this.setState({ isSheetMusicModal: true })
                    this.setState({ isMusicModal: true, selectedMediaType: "sheet", modalTitle: "Sheet Music" })
                  }
                  onPressPhoto={() => this.setState({ isMusicModal: true, selectedMediaType: "image", modalTitle: "Photos" })}
                />
              </View>

              <ScrollView
                style={{
                  width: "66%",
                  marginLeft: 20,
                }}
              >
                <View
                  style={{
                    width: "100%",
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                    }}
                  >
                    <View
                      style={[
                        styles.element,
                        { flex: 1, height: height - 150 },
                      ]}
                    >
                      {/* Calendar */}
                      <View
                        style={[
                          styles.availabilityContainer,
                          { marginBottom: 20 },
                        ]}
                      >
                        <Text style={styles.titles}>{en.available_title}</Text>
                        <Calendar
                          calendarWidth={styles.availabilityContainer.width}
                          dates={person.calendar}
                        />
                      </View>

                      {/* Tags */}
                      <View style={styles.tagsContainer}>
                        <Text style={styles.titles}> {en.tags_title} </Text>
                        <ScrollView
                          showsVerticalScrollIndicator={false}
                          bounces={false}
                        >
                          {tags.map((snap) => (
                            <Tags showDelete={false} title={snap.title} />
                          ))}
                          {tags.length === 0 && (
                            <Text style={styles.emptyText}>
                              {en.empty_tags}
                            </Text>
                          )}
                        </ScrollView>
                      </View>
                    </View>
                    <View
                      style={[
                        styles.element,
                        { flex: 1, marginLeft: 20, height: height - 150 },
                      ]}
                    >
                      {/* Contract Information  */}
                      <View
                        style={[
                          styles.availabilityContainer,
                          { marginBottom: 20 },
                        ]}
                      >
                        <Text style={styles.titles}>{en.contract_title}</Text>
                        <ScrollView style={styles.media}>
                          {files.map((file, index) => {
                            return (
                              <File
                                key={index}
                                refs={(fileRef) =>
                                  this.handleSetRef(fileRef, index)
                                }
                                filename={"PDF"}
                                fileType={"pdf"}
                                onPressMore={() =>
                                  this.onPressFileMore(file, index)
                                }
                                containerStyle={styles.fileContainer}
                              />
                            );
                          })}
                        </ScrollView>
                        {showBtn ? (
                          <GradientButton
                            light
                            buttonText={en.updload_button_text}
                            onPress={() => this.handlePressUploadDocument()}
                            containerStyle={styles.buttonSaveContainer}
                            gradientStyle={styles.buttonSaveGradient}
                            textStyle={[
                              styles.buttonSaveText,
                              { color: "#5f2543" },
                            ]}
                            loading={isUploading}
                            spinnerColor="black"
                          />
                        ) : null}
                      </View>
                      {/* Comments */}
                      <View style={styles.availabilityContainer}>
                        <Text style={styles.titles}>{en.comment_title}</Text>
                        {comments.map((snap) => (
                          <Tags showDelete={false} title={snap.comment} />
                        ))}
                        {comments.length === 0 && (
                          <Text style={styles.emptyText}>
                            {en.empty_comments}
                          </Text>
                        )}
                      </View>
                    </View>
                  </View>
                  <View
                    style={[
                      styles.availabilityContainer,
                      {
                        marginTop: 20,
                        height: (height - 150) / 2,
                        width: "50%",
                      },
                    ]}
                  >
                    <Text style={[styles.titles, { marginBottom: 15 }]}>
                      {en.audition_videos}
                    </Text>
                    {this.getAuditionVideoList()}
                  </View>
                </View>
              </ScrollView>
            </View>
          </KeyboardAvoidingView>
          <Animated.View
            style={[styles.lateralLayout, { right: widthFirstContainerA }]}
          >
            <TouchableOpacity
              style={styles.spiltContainer}
              onPress={this.handleSideMenu}
            >
              <View style={styles.spiltIcon} />
            </TouchableOpacity>

            <View
              style={styles.auditionVideosContent}
              scrollEnabled={true}
              nestedScrollEnabled={true}
            >
              {/* <View style={styles.videosContainer}>
                {this.state.videoListByAudition.length > 0 &&
                  this.state.videoListByAudition.map((file, index) => {
                    return (
                      <File
                        key={index}
                        refs={fileRef => {}}
                        collapsable={false}
                        filename={file.name}
                        fileType={file.type}
                        onPressMore={() => {
                          console.log(" ============== Video List clicked ============== ")
                          this.setState({
                            isShowPopup: true,
                            clickedAudition: file
                          });
                        }}
                      />
                    );
                  })}
                {this.state.isShowPopup ? this.showPopup() : null}
              </View> */}
              {this.state.videoListByAudition.length > 0 ? (
                <View style={{ flex: 1 }}>
                  {console.log(
                    " FLAT LIST CALLED ",
                    this.state.videoListByAudition.length
                  )}
                  <FlatList
                    data={this.state.videoListByAudition}
                    contentContainerStyle={{
                      // flex: 1,
                      paddingBottom: 50,
                    }}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => item + index}
                    renderItem={({ item, index }) => (
                      <View
                        collapsable={false}
                        ref={(view) => {
                          this.feedPost[index] = view;
                        }}
                        style={{
                          paddingHorizontal: 5,
                          paddingVertical: 10,
                        }}
                      >
                        <View
                          style={{
                            height: 50,
                            // width: "90%",
                            flexDirection: "row",
                            marginTop: 25,

                            alignSelf: "center",
                            marginLeft: 0,
                          }}
                        >
                          <TouchableOpacity
                            style={{
                              height: 70,
                              backgroundColor: "white",
                              flexDirection: "row",
                              width: "100%",
                              // borderColor: "gray",
                              // borderWidth: 1,
                              overflow: "hidden",
                              borderRadius: 10,
                            }}
                            onPress={(event) => {
                              // console.log(" CHECK EVENT POSITION ", event.nativeEvent)
                              // { this.openModal1() }
                              console.log("item=====>" + JSON.stringify(item)),
                                console.log(
                                  "index=====>" + JSON.stringify(index)
                                );

                              this.feedPost[index].measure(
                                (fx, fy, width, height, px, py) => {
                                  console.log("Component width is: " + width);
                                  console.log("Component height is: " + height);
                                  console.log("X offset to page: " + px);
                                  console.log("Y offset to page: " + py);
                                  console.log(" Page index: " + index);

                                  console.log(
                                    " DEVICE HEIGHT: " + DEVICE_HEIGHT
                                  );
                                  console.log(
                                    " HEADER HEIGHT: " + headerHeight
                                  );

                                  // this.positionX = px;
                                  if (DEVICE_HEIGHT < py + headerHeight + 55) {
                                    console.log("Component 111");
                                    this.positionY = py - (100 + 55);
                                  } else {
                                    console.log("Component 222");
                                    this.positionY = py + 40;
                                  }

                                  this.setState({
                                    currObj: item,
                                    isShowPopup: true,
                                    clickedAudition: item,
                                  });
                                }
                              );
                            }}
                          >
                            {item.thumbnail == null ? (
                              <View
                                style={{
                                  height: "100%",
                                  width: 65,
                                  backgroundColor: "#4d2545",
                                  justifyContent: "center",
                                  alignItems: "center",
                                  alignSelf: "center",
                                }}
                              >
                                <Image
                                  style={{
                                    resizeMode: "center",
                                    height: 40,
                                    width: 40,
                                    // backgroundColor: "#4d2545"
                                  }}
                                  source={require("../../../assets/icons/auditions/mp4Icon.png")}
                                />
                              </View>
                            ) : (
                                <Image
                                  style={{
                                    resizeMode: "center",
                                    height: "100%",
                                    width: 65,
                                    // backgroundColor: "#4d2545"
                                  }}
                                  source={
                                    item.thumbnail != null
                                      ? { uri: item.thumbnail }
                                      : require("../../../assets/icons/auditions/mp4Icon.png")
                                  }
                                />
                              )}
                            {console.log("item :", item)}
                            <View
                              style={{
                                alignItems: "center",
                                justifyContent: "center",
                                alignSelf: "center",
                              }}
                            >
                              <Text
                                style={{ color: "#4d2545", marginStart: 30 }}
                              >
                                {item.name ? item.name : ""}
                              </Text>
                            </View>
                            <Image
                              style={{
                                resizeMode: "center",
                                height: 20,
                                width: 45,
                                marginBottom: 15,
                                alignSelf: "flex-end",
                                right: 0,
                                position: "absolute",
                              }}
                              source={require("../../../assets/icons/auditions/more-icon.png")}
                            />
                          </TouchableOpacity>
                          {/* {this.openModal1()} */}

                          {/* {(item.isVisible) && this.showModal(item)}   */}
                        </View>
                      </View>
                    )}
                  />
                  {this.state.isShowPopup ? this.showPopup() : null}
                </View>
              ) : null}
            </View>
          </Animated.View>
          <PartakerModal
            show={partakerDetailModal}
            scrollTo={partakerDetailModalSectionSelected}
            dismiss={() => this.handlePressPartakerSection(en.info_title)}
            data={modalData}
          />
          <Music
            show={this.state.isMusicModal}
            userId={person.details.user_id}
            title={this.state.modalTitle}
            mediaType={this.state.selectedMediaType}
            onCloseClick={() => {
              this.setState({ isMusicModal: false });
            }}
          />
        </Container>
      );
    } else {
      return <Loading />;
    }
  }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
});

export default connect(mapStateToProps)(PartakerDetail);
