import { StyleSheet, Platform } from "react-native";

//custom
import {
  resize,
  NEXABOLD,
  height,
  NEXALIGHT,
  STATUS_BAR_HEIGHT,
} from "../../../../assets/styles";

const containersHeight =
  Platform.OS === "ios" ? height : height - STATUS_BAR_HEIGHT;

export default StyleSheet.create({
  nBContainer: {
    backgroundColor: "transparent",
  },
  dismissButton: {
    width: "100%",
    height: containersHeight,
    position: "absolute",
    justifyContent: "flex-end",
  },
  blurView: {
    width: "100%",
    height: resize(Platform.OS === "ios" ? 732 : 729, "height"),
    backgroundColor: "#f0f0f0",
    opacity: 0.84,
  },
  cardContainer: {
    width: "100%",
    height: containersHeight,
    justifyContent: "flex-end",
  },
  card: {
    alignSelf: "center",
    width: resize(389),
    justifyContent: "space-between",
    height: resize(664),
    shadowColor: "rgba(0, 0, 0, 0.16)",
    shadowOffset: { width: 3, height: 0 },
    shadowRadius: resize(6),
    borderRadius: resize(13),
    backgroundColor: "#ffffff",
    marginBottom: resize(33, "height"),
  },
  closeButton: {
    position: "absolute",
    zIndex: 2,
    paddingHorizontal: resize(18),
    paddingVertical: resize(21, "height"),
  },
  title: {
    marginTop: resize(21, "height"),
    marginBottom: resize(20, "height"),
    alignSelf: "center",
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(23),
    fontWeight: "400",
    paddingVertical: resize(5, "height"),
  },
  itemsContainers: {
    paddingLeft: resize(98),
    paddingRight: resize(20),
  },
  item: {
    paddingTop: resize(15, "height"),
    paddingBottom: resize(12, "height"),
    borderBottomWidth: 1,
    borderColor: "#d6d6d6",
    justifyContent: "center",
    marginBottom: resize(21, "height"),
  },
  itemText: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: "400",
    paddingVertical: resize(5, "height"),
  },
  itemSubText: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(13),
    fontWeight: "400",
    paddingVertical: resize(5, "height"),
  },
});
