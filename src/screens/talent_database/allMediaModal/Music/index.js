import React, { Component, Fragment } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  Modal,
  ScrollView,
  StyleSheet,
  Platform,
  FlatList,
  Linking
} from "react-native";
import { Container, Content } from "native-base";

//custom
// import styles from "./styles";
import icon_close from "../../../../assets/icons/commons/icon_close.png";
import { getAllMediaByType } from "../../../../api/auditions";
import { Assets } from "../../../../assets";
import colors from "../../../../utils/colors";
import { resize, NEXABOLD } from "../../../../assets/styles";
import MediaCards from "../../../../components/media/mediaCards";
import ProgressLoader from "../../../../components/commons/progress_loading";
import showMessage from "../../../../utils/show_message";
import { getMediaByType } from "../../../../api/talents";
import VideoCard from "../../../../components/media/videoCard";
import isYoutubeUrl from "../../../../utils/isYoutubeUrl";
import openPlayer from "../../../../components/media/videoCard/playVideo";

export default class Music extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      data: []
    }
  }
  getMeasure = (y) => {
    this.container.scrollTo({ x: 0, y, animated: true });
  };
  async componentDidMount() {
    // console.log('this.props Music:>> ', this.props);
    // const responseMusicList = await getAllMediaByType(
    //   this.props.performerId,
    //   "audio"
    // );
    // console.log(JSON.stringify(responseMusicList));
  }

  componentDidUpdate(prevProp) {
    console.log('this.props Music:>> ', this.props);
    const { mediaType } = this.props
    if (this.props.show && prevProp.mediaType != this.props.mediaType) {
      this.getFiles(mediaType)
      // if (this.props.mediaType == "video") {

      // } else if (mediaType == "audio" || mediaType == "doc" || mediaType == "sheet" || mediaType == "image") {
      //   this.getFiles(mediaType)
      // }
    }
  }

  getFiles = async (type) => {
    this.setState({ isLoading: true, data: [] });
    try {
      const response = await getMediaByType(type, this.props.userId);
      console.log('response :>> ', response);
      this.setState({ data: response.data.data, isLoading: false });
    } catch (e) {
      console.log('e :>> ', e);
      this.setState({ isLoading: false, data: [] });
      const parsedError = parseError(error);
      return showMessage("danger", parsedError);
    }
  }

  openFile = (url) => {
    console.log('url :>> ', url);
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
        this.props.onCloseClick()
      } else {
        console.log("Don't know how to open URI: " + url);
        showMsj("Can't open your file", true);
      }
    });
  }

  onVideoPlay = (url, isYouTubeUrl) => {
    this.props.onCloseClick()
    setTimeout(() => {
      if (isYouTubeUrl) {
        openPlayer(url);
      } else {
        try {
          Linking.openURL(url);
        } catch (e) { }
      }
    }, 500);

  }

  renderItem = ({ item, index }) => {
    if (this.props.mediaType == "audio") {
      return <MediaCards
        key={index}
        type="audio"
        fileText={item.name}
        data={item}
        openFile={this.openFile}
      />
    } else if (this.props.mediaType == "doc") {
      return <MediaCards
        key={index}
        type="doc"
        fileText={item.name
        }
        data={item}
        openFile={this.openFile}
      />
    } else if (this.props.mediaType == "image") {
      return <MediaCards
        key={index}
        type="image"
        fileText={item.name
        }
        data={item}
        openFile={this.openFile}
      />
    } else if (this.props.mediaType == "sheet") {
      return <MediaCards
        key={index}
        type="sheet"
        fileText={item.name
        }
        data={item}
        openFile={this.openFile}
      />
    } else if (this.props.mediaType == "video") {
      // return <MediaCards
      //   key={index}
      //   type="sheet"
      //   fileText={item.name
      //   }
      //   data={item}
      //   openFile={this.openFile}
      // />
      return <VideoCard
        key={index}
        name={item.name}
        url={
          isYoutubeUrl(item.url)
            ? isYoutubeUrl(item.url)
            : item.url
        }
        onPress={() => {
          this.onVideoPlay(isYoutubeUrl(item.url)
            ? isYoutubeUrl(item.url)
            : item.url, isYoutubeUrl(item.url))
        }}
        isYouTubeUrl={isYoutubeUrl(item.url)}
      // url={isYoutubeUrl(option.url)}
      // isYouTubeUrl={true}
      />
    }
  }

  render() {
    return (
      <Modal
        animationType="fade"
        animated={true}
        visible={this.props.show}
        transparent={true}
        onRequestClose={this.props.dismiss}
      >
        {this.state.isLoading ? <ProgressLoader isLoading={true} /> : null}
        <TouchableOpacity
          style={styles.modalContainer}
          onPress={this.props.onCloseClick}
          activeOpacity={1}
        >
          <TouchableOpacity style={styles.modalSubContainer} activeOpacity={1}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ position: "absolute", left: 5 }}
                onPress={this.props.onCloseClick}
              >
                <Image source={Assets.close} />
              </TouchableOpacity>
              <Text style={{ color: colors.PRIMARY, fontSize: resize(18) }}>
                {this.props.title}
              </Text>
            </View>

            <ScrollView style={{ flex: 1, marginVertical: 10 }} keyboardShouldPersistTaps={"always"} showsVerticalScrollIndicator={false}>
              <View onStartShouldSetResponder={() => true}>
                {this.state.data.length > 0 ? <FlatList
                  // contentContainerStyle={{ padding: 10 }}
                  // style={{ flex: 1 }}
                  data={this.state.data}
                  extraData={this.state}
                  keyExtractor={(item, index) => item + index}
                  renderItem={this.renderItem}
                  // refreshing={this.state.refreshing}
                  // onRefresh={this.refreshPartakers}
                  showsVerticalScrollIndicator={false}
                />
                  : this.state.data.length == 0 && !this.state.isLoading ? <Text style={{
                    fontFamily: NEXABOLD,
                    fontSize: 18,
                    color: colors.PRIMARY,
                    marginTop: resize(40, "height")
                  }}>No Media Added</Text>
                    : null
                }
              </View>
              {/* {
                this.state.data.map((data, index) => {
                  // this.renderItem(data, index)
                  return <MediaCards
                    key={index}
                    type="audio"
                    fileText={data.name}
                    data={data}
                    openFile={this.openFile}
                  />
                })
              } */}
            </ScrollView>
            {/* <FlatList
              // contentContainerStyle={{ padding: 10 }}
              style={{ flex: 1 }}
              data={this.state.data}
              extraData={this.state}
              keyExtractor={(item, index) => item + index}
              renderItem={this.renderItem}
              // refreshing={this.state.refreshing}
              // onRefresh={this.refreshPartakers}
              showsVerticalScrollIndicator={false}
            /> */}
          </TouchableOpacity>
        </TouchableOpacity>
        {/* <Container style={styles.nBContainer}>
          <TouchableOpacity
            activeOpacity={1}
            style={styles.dismissButton}
            onPress={this.props.dismiss}
          >
            <View style={styles.blurView} />
          </TouchableOpacity>
          <Content bounces={false}>
            <View style={styles.cardContainer}>
              <View style={styles.card}>
                <TouchableOpacity
                  style={styles.closeButton}
                  onPress={this.props.dismiss}
                >
                  <Image source={icon_close} />
                </TouchableOpacity>
              </View>
            </View>
          </Content>
        </Container> */}
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.SHADOW_COLOR
  },
  modalSubContainer: {
    borderRadius: 8,
    backgroundColor: colors.WHITE,
    shadowOpacity: 0.2,
    elevation: 5,
    width: "35%",
    height: resize(Platform.OS === "ios" ? 732 : 729, "height"),
    padding: 20
  },
})
