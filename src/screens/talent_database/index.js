import React, { Component } from 'react';
import { Container } from 'native-base';
import { connect } from 'react-redux';
import { Text, FlatList, Animated, TouchableOpacity, Keyboard } from 'react-native';

//custom
import styles from './styles';
import Header from '../../components/commons/header';
import LightBar from '../../components/commons/light_bar';
import ButtonMenu from '../../components/commons/button_menu';
import EmptyState, { Loading } from '../../components/commons/empty_state';
import AnimatedSearch, { SearchFixed } from '../../components/commons/animatedSearch';
import PartakerDrag from '../../components/auditions/partaker/draggable';

//locale
import en from '../../locale/en';
import { resize } from '../../assets/styles';
import FilterOptions from '../../components/commons/filterOptions';
import { getTalents, filterTalent, validateCode } from '../../api/talents';
import Share from '../../components/commons/share';
import { headerHeight } from './manager_talent_database/styles';
import showMessage from '../../utils/show_message';

var searchTime;

class AuditionsManage extends Component {

	state = {
		talents: [],
		talentsBack: [],
		isLoading: true,
		refreshing: false,
		displayOverlay: false,
		width: 0,
		px: 0,
		py: 0,
		height: 0,
		heightValue: new Animated.Value(0),
		opacityValue: new Animated.Value(0),
		search: '',
		union: 2,
		gender: 'ANY',
		isSending: false,
		code: '',
		showShare: false
	}

	componentDidMount = () => {
		this.fetchAvailableNotifications();
	}

	handlePressOpenDrawer = () => {
		const { navigation } = this.props;
		navigation.openDrawer();
	}

	handlePressDeleteNotification = () => {
		if (this.state.showInvitationModal) {
			this.setState({ showInvitationModal: false });
		} else {
			this.setState({ showInvitationModal: true });
		}
	}

	fetchAvailableNotifications = async (isRefres = false) => {
		try {
			isRefres ? this.setState({ refreshing: true }) : this.setState({ isLoading: true, videos: [] });
			let categoryResponse = await getTalents();
			console.log('categoryResponse.data.data :>> ', categoryResponse.data.data);
			this.setState({
				talents: categoryResponse.data.data,
				talentsBack: categoryResponse.data.data,
				isLoading: false
			});
			isRefres ? this.setState({ refreshing: false }) : null;
		} catch (error) {
			this.setState({ isLoading: false });
		}
	}

	removeDeletedItem = (id) => {
		let { talents } = this.state;
		let removeReference = [...talents];
		let indexDelete = removeReference.findIndex(snap => {
			return snap.id === id
		});
		removeReference.splice(indexDelete, 1);
		this.setState({ talents: removeReference });
	}

	showOverlay = () => {
		//This function can easy use in anothe component just move and translate state and funcitions relationated
		let { displayOverlay } = this.state;
		Keyboard.dismiss();
		if (!displayOverlay) {
			this.search.measure((a, b, width, height, px, py) => {
				this.setState({ width, px, py, height }, () => {
					this.setState(prevState => ({
						displayOverlay: !prevState.displayOverlay
					}), () => {
						let { heightValue, displayOverlay, opacityValue } = this.state;
						if (displayOverlay) {
							Animated.timing(heightValue, {
								toValue: resize(500, 'height'),
								duration: 700,
							}).start();
							Animated.timing(opacityValue, {
								toValue: 1,
								duration: 700
							}).start();
						}
					})
				})
			})
		} else {
			let { heightValue, opacityValue } = this.state;
			Animated.timing(heightValue, {
				toValue: 0,
				duration: 700,
			}).start(() => {
				this.setState(prevState => ({
					displayOverlay: !prevState.displayOverlay
				}))
			});
			Animated.timing(opacityValue, {
				toValue: 0,
				duration: 500,
			}).start();
		}
	}

	handleSetRef = (file) => {
		this.search = file;
	};

	onChangeText = (search) => {
		let { union, gender } = this.state;
		let isClear = search.length >= this.state.search.length;
		let customData = {
			base: search,
			union,
			gender
		}
		this.setState({ search });
		if (search !== '') {
			this.callFunction(customData, isClear);
		} else {
			let { talentsBack } = this.state;
			clearTimeout(searchTime);
			this.setState({ talents: talentsBack });
		}
	}

	searchByoptions = () => {
		let { search, union, gender } = this.state;
		let customData = {
			base: search,
			union,
			gender
		}
		this.callFunction(customData, true);
	}

	callFunction = async (value, isClear) => {
		clearTimeout(searchTime);
		searchTime = setTimeout(async () => {
			filterTalent(value).then(response => {
				let talents = response.data.data;
				console.log(talents);
				this.setState({ talents });
			}).catch(e => {
				console.log(e);
				isClear ? Keyboard.dismiss() : '';
				this.setState({ talents: [] });
			})
		}, 400);
	}

	goToDetails = (person) => {
		let { navigation } = this.props;
		navigation.navigate('ManagerTalentDatabase', { person, showBtn: false });
	}

	handleState = (state, value) => {
		this.setState({ [state]: value }, () => {
			this.searchByoptions();
		})
	}

	handleEmail = code => {
		this.setState({ code })
	}

	verificationCode = async () => {
		let { code, isSending } = this.state;
		if (isSending) {
			return;
		}
		this.setState({ isSending: true });
		try {
			await validateCode({
				code
			})
			this.setState({ isSending: false, code: '' });
			this.handleShare();
			this.fetchAvailableNotifications();
			showMessage('success', en.performer_added);
		} catch (error) {
			this.setState({ isSending: false });
			showMessage('danger', 'There was an error validating the code');
		}
	}

	handleShare = () => {
		this.setState(prevState => {
			return { showShare: !prevState.showShare }
		})
	}

	render() {
		const { isLoading, refreshing, talents, displayOverlay, width, px, py, heightValue,
			opacityValue, search, gender, union, isSending, code, showShare } = this.state;
		const heightContainer = heightValue.interpolate({
			inputRange: [0, resize(500, 'height')],
			outputRange: [0, resize(500, 'height')],
		});
		const opacityContainer = opacityValue.interpolate({
			inputRange: [0, 1],
			outputRange: [0, 1],
		});
		return (
			<Container>
				<LightBar />
				<Header
					left={<ButtonMenu onPress={() => this.handlePressOpenDrawer()} />}
					center={<Text style={styles.titleNotifications}>{en.talent_database}</Text>}
					right={<AnimatedSearch
						handleAdd={this.handleShare}
						search={search}
						onChangeText={this.onChangeText}
						handleSetRef={this.handleSetRef}
						showOverlay={this.showOverlay}
					/>}
				/>
				{displayOverlay
					? <Animated.View style={[styles.alingItem, { width, left: px, top: py, height: heightContainer, opacity: opacityContainer }]}>
						<SearchFixed
							search={search}
							onChangeText={this.onChangeText}
							showOverlay={this.showOverlay} />
						<FilterOptions
							union={union}
							gender={gender}
							handleState={this.handleState}
							onSearch={this.showOverlay}
						/>
					</Animated.View>
					: null
				}

				{/* modal to share performance */}
				<Share
					handleShare={this.handleShare}
					verificationCode={this.verificationCode}
					sendInvitation={() => { }}
					isSending={isSending}
					handleEmail={this.handleEmail}
					email={code}
					showShare={showShare}
					headerHeight={headerHeight}
					title={''}
					light={false}
					colorText='white'
					alignSelf='center'
					placeHolder='Enter performer’s code'
					submit
				/>
				{console.log('talents :>> ', talents)}
				<FlatList
					contentContainerStyle={styles.pastContent}
					data={talents}
					columnWrapperStyle={styles.rows}
					numColumns={5}
					keyExtractor={(item) => item.details.id.toString()}
					renderItem={({ item }) => (
						<TouchableOpacity style={styles.separator} activeOpacity={0.8} onPress={() => this.goToDetails(item)}>
							<PartakerDrag
								name={`${item.details.first_name} ${item.details.last_name}`}
								image={item.image}
								styleManage
							/>
						</TouchableOpacity>
					)}
					ListEmptyComponent={
						isLoading
							? <Loading />
							: <EmptyState text={en.empty_state_talents} />
					}
					refreshing={refreshing}
					onRefresh={() => this.fetchAvailableNotifications(true)}
					showsVerticalScrollIndicator={false}
				/>
			</Container>
		);
	}
}

const mapStateToProps = (state) => ({
	user: state.user.data
});

export default connect(mapStateToProps)(AuditionsManage);