import { StyleSheet } from 'react-native';
import { resize, NEXABOLD } from '../../assets/styles';

const styles = StyleSheet.create({
	pastContent: {
		flexGrow: 1
	},
	titleNotifications: {
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		color: 'white'
	},
	alingItem: {
		position: 'absolute',
		backgroundColor: 'white',
		borderRadius: resize(25, 'height'),
		zIndex: 2,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
	},
	rows: {
		justifyContent: 'flex-start',
		marginVertical: resize(20, 'height'),
		paddingHorizontal: resize(25),
	},
	separator: {
		marginRight: resize(40),
	}
})
export default styles;
