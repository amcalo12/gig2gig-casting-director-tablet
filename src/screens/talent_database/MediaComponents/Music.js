import React, { Component } from 'react';
import { View, Text } from 'react-native';
import MediaCards from '../../../components/media/mediaCards';

export default class Music extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <MediaCards
                key={i}
                type="audio"
                fileText={this.props.name}
                data={this.props}
                openFile={this.props.openFile}
            />
        );
    }
}
