import { StyleSheet } from 'react-native';
import { resize, NEXABOLD } from '../../assets/styles';

const styles = StyleSheet.create({
    infoText: {
        color: 'white',
        fontSize: resize(20),
        fontFamily: NEXABOLD
    },
    searchIcon: {
        marginTop: resize(10, 'height'),
        width: '30%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    pastContent: {
        flexGrow: 1
    },
})
export default styles;
