import { StyleSheet } from 'react-native';
import { resize, NEXABOLD, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginTop: resize(40, 'height')
    },
    content: {
        width: '65%',
        alignSelf: 'center'
    },
    img: {
        width: '100%',
        height: resize(285, 'height'),
        borderRadius: resize(10),
        marginBottom: resize(30, 'height')
    },
    title: {
        color: '#4D2545',
        fontSize: resize(28),
        fontFamily: NEXABOLD,
        marginVertical: resize(10, 'height')
    },
    posted: {
        color: '#4D2545',
        fontSize: resize(20),
        fontFamily: NEXALIGHT,
        marginBottom: resize(30, 'height')
    },
    info: {
        color: '#757575',
        fontSize: resize(18),
        fontFamily: NEXALIGHT,
        textAlign: 'justify'
    }
})
export default styles;
