import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { Container, Content } from 'native-base';
import styles from './styles';
import { connect } from 'react-redux';

//Custom
import BackButton from '../../../components/commons/back_button';
import LightBar from '../../../components/commons/light_bar';
import Header from '../../../components/commons/header';

//Temporal
import Test from '../../../assets/images/background_card/news.png';
import { SET_NEWS_ACTUAL } from '../../../actions/news';

class NewsDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    handlePressBackButton = () => {
        const { navigation } = this.props;
        navigation.goBack(null);
    }

    render() {
        let { news } = this.props;
        console.log(news)
        return (
            <Container>
                <LightBar />
                <Header
                    left={
                        <BackButton
                            onPress={() => this.handlePressBackButton()}
                        />
                    }
                />
                <Content showsVerticalScrollIndicator={false} style={styles.container}>
                    <View style={styles.content}>
                        <Text style={styles.title}>{news.title}</Text>
                        <Text style={styles.posted}>{news.time_ago}</Text>
                        <Image style={styles.img} resizeMode='cover' source={{ uri: news.url_media ? news.url_media : '' }} />
                        <Text style={styles.info}>{news.body}</Text>
                    </View>
                </Content>
            </Container>
        )
    }
}
const mapStateToProps = (state) => ({
    news: state.news.news
})
export default connect(mapStateToProps)(NewsDetails)
