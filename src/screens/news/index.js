import React, { Component } from 'react';
import { Container, View } from 'native-base';
import { FlatList, Text } from 'react-native';
import { connect } from 'react-redux';

//Custom
import LightBar from '../../components/commons/light_bar';
import Header from '../../components/commons/header';
import ButtonMenu from '../../components/commons/button_menu';
import HeaderButton from '../../components/commons/header_button';
import EmptyState, { Loading } from '../../components/settings/empty_state';
import NewsUpdates from '../../components/news'

//Images
import en from '../../locale/en';
import Filter from '../../assets/icons/news/filter.png';

import styles from './styles';
import { getNews, getNewsFilter } from '../../api/news';
import { SET_NEWS_ACTUAL } from '../../actions/news';

class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news: [],
            isLoading: true,
            refreshing: false,
            filter: 'desc'
        }
    }

    componentDidMount = () => {
        this.fetchAvailablePost();
    }

    handlePressOpenDrawer = () => {
        const { navigation } = this.props;
        navigation.openDrawer();
    }

    fetchAvailablePost = async (isRefres = false) => {
        try {
            isRefres ? this.setState({ refreshing: true }) : this.setState({ isLoading: true, videos: [] });
            let newsResponse = await getNews();
            let news = newsResponse.data.data.filter(item => {
                if (item.search_to === 'director' || item.search_to === 'both') {
                    return item;
                }
            })
            this.setState({
                news,
                isLoading: false
            });
            isRefres ? this.setState({ refreshing: false }) : null;
        } catch (error) {
            this.setState({ isLoading: false });
        }
    }

    fetchAvailablePostFilter = async () => {
        let { filter } = this.state;
        try {
            let newsResponse = await getNewsFilter({
                order_by: filter
            });
            let actual = filter === 'asc' ? 'desc' : 'asc';
            this.setState({
                news: newsResponse.data.data,
                isLoading: false,
                filter: actual
            });
        } catch (error) {
            this.setState({ isLoading: false });
        }
    }



    onPressNews = (item) => {
        const { navigation, dispatch } = this.props;
        dispatch({
            type: SET_NEWS_ACTUAL,
            payload: item
        });
        navigation.navigate("NewsDetails")
    }

    render() {
        let { isLoading, refreshing, news } = this.state;
        return (
            <Container>
                <LightBar />
                <Header
                    left={<ButtonMenu onPress={() => this.handlePressOpenDrawer()} />}
                    center={<Text style={styles.infoText}>News & Updates</Text>}
                    right={
                        <HeaderButton
                            icon={Filter}
                            onPress={this.fetchAvailablePostFilter}
                            containerStyle={styles.searchIcon}
                        />
                    }
                />
                <FlatList
                    contentContainerStyle={styles.pastContent}
                    data={news}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({ item }) => (
                        <NewsUpdates {...item} onPress={() => this.onPressNews(item)} />
                    )}
                    ListEmptyComponent={
                        isLoading
                            ? <Loading />
                            : <EmptyState text={en.empty_news} />
                    }
                    refreshing={refreshing}
                    onRefresh={() => this.fetchAvailablePost(true)}
                    showsVerticalScrollIndicator={false}
                />
            </Container>
        )
    }
}
export default connect()(News);
