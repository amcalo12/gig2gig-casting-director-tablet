//import liraries
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Platform,
} from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import { Assets } from "../../assets";
const { width, height } = Dimensions.get("window");
import { NavigationActions, StackActions } from "react-navigation";
import { NEXABOLD, resize, NEXALIGHT } from "../../assets/styles";
import { connect } from "react-redux";
import { INIT_SESSION } from "../../actions/auth";
import { getAllTeamAdmins } from "../../api/auditions";

const slides = [
  {
    key: "somethun",
    text: "Create Your Audition",
    // text1:
    //   "Add your production information, tags, contributors, and documents for performer download to get started!",
    text1:
      "Add your production information, tags, contributors, and upload production documents to get started!",
    image: Assets.image1,
    backgroundColor: "white",
  },
  {
    key: "somethun-dos",
    text: "Instant Feedback",
    text1:
      "Once checked-in, instantly inform talent of your immediate reaction. Press check to send a 'Great Job' and recommend additional productions, or press X to let them know that's all you need today.",
    image: Assets.image2,
    backgroundColor: "white",
  },
  {
    key: "somethun1",
    text: "Feedback",
    text1:
      "Full feedback can be sent to talent once an Audition Round has been completed. Collaborate with contributors, and add tags for future reference.",
    image: Assets.image3,
    backgroundColor: "white",
  },
  {
    key: "somethun2",
    text: "Manage Rounds",
    text1:
      "Easily manage multiple round auditions. Star performers who have earned a second look, and they’ll receive an invitation for a new appointment.",
    image: Assets.image4,
    backgroundColor: "white",
  },
  {
    key: "somethun3",
    text: "Final Casting List",
    text1:
      "Place available talent into roles. Easily edit as needed, and export Final List to CSV.",
    image: Assets.image5,
    backgroundColor: "white",
  },
];

// create a component
class SliderImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
    };
  }

  //   _renderItem1 = ({ item }) => {
  //     return (
  //       <View style={styles.container}>
  //         <Image style={styles.image} source={item.image} />
  //         <Text style={styles.text}>{item.text}</Text>
  //         <Text style={styles.text1}>{item.text1}</Text>
  //       </View>
  //     );
  //   };

  _renderItem = ({ item }) => {
    return (
      <View style={{ width: width, height: height }}>
        <Image source={item.image} style={styles.imageStyle} />
        <View style={styles.introContainer}>
          <View style={styles.textContainer}>
            <Text style={styles.titleText}>{item.text}</Text>
            <Text style={styles.subTitleText}>{item.text1}</Text>
          </View>
        </View>
      </View>
    );
  };

  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    // this.setState({ showRealApp: true });
    this.navigateToApp();
  };
  // navigateToApp = async () => {
  //   //========================================
  //   // this.setState({ loading: true });
  //   try {
  //     let adminList = await getAllTeamAdmins();
  //     if (adminList.data && adminList.status == 200) {
  //       let data = adminList.data
  //         ? adminList.data.data
  //           ? adminList.data.data
  //           : []
  //         : [];
  //       let isSelectAdmin = false;
  //       if (data.length > 0) {
  //         data.map((item) => {
  //           if (item.is_selected && item.is_selected === 1) {
  //             isSelectAdmin = true;
  //           }
  //         });
  //       }

  //       const { dispatch } = this.props;
  //       let authData = {
  //         authorize: true,
  //         isAppTour: false,
  //         token: this.props.auth.token,
  //         isAdminSelect: isSelectAdmin,
  //       };

  //       dispatch({
  //         type: INIT_SESSION,
  //         payload: authData,
  //       });
  //       this.setState({ loading: false });
  //     }
  //   } catch (error) {
  //     console.log("get All team Admins =====>", error);
  //     const parsedError = parseError(error);
  //     showMessage("danger", parsedError);
  //     const { dispatch } = this.props;
  //     let authData = {
  //       authorize: true,
  //       isAppTour: false,
  //       token: this.props.auth.token,
  //       isAdminSelect: true,
  //     };

  //     dispatch({
  //       type: INIT_SESSION,
  //       payload: authData,
  //     });

  //     this.setState({ loading: false });
  //   }

  //   // this.props.navigation.dispatch(
  //   //   StackActions.reset({
  //   //     index: 0,
  //   //     actions: [NavigationActions.navigate({ routeName: "App" })]
  //   //   })
  //   // );
  // };
  navigateToApp() {
    const { dispatch } = this.props;
    let authData = {
      authorize: true,
      isAppTour: false,
      token: this.props.auth.token,
    };

    dispatch({
      type: INIT_SESSION,
      payload: authData,
    });

    // this.props.navigation.dispatch(
    //   StackActions.reset({
    //     index: 0,
    //     actions: [NavigationActions.navigate({ routeName: "App" })]
    //   })
    // );
  }

  _onSkip = () => {
    this.navigateToApp();
  };
  render() {
    if (this.state.showRealApp) {
      return <App />;
    } else {
      return (
        // <AppIntroSlider
        //   renderItem={this._renderItem}
        //   slides={slides}
        //   onDone={this._onDone}
        // />
        <AppIntroSlider
          renderItem={this._renderItem}
          slides={slides}
          dotStyle={styles.dotStyle}
          activeDotStyle={styles.activeDotStyle}
          showSkipButton
          onDone={this._onDone}
          // showSkipButton={true}
          onSkip={this._onSkip}
          // showNextButton={false}
          buttonTextStyle={{
            color: "#4d2545",
            fontSize: 14,
            fontFamily: NEXABOLD,
          }}
        />
      );
    }
  }
}
// define your styles
const styles = StyleSheet.create({
  dotStyle: {
    backgroundColor: "#4d2545",
    width: 6,
    height: 6,
    borderRadius: 3,
    marginHorizontal: 6,
  },
  activeDotStyle: {
    backgroundColor: "#4d2545",
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 6,
  },
  imageStyle: {
    width: width,
    flex: 1,
    resizeMode: "contain",
  },

  titleText: {
    textAlign: "center",
    fontFamily: NEXABOLD,
    fontSize: 20,
    letterSpacing: 1.2,
    marginBottom: 15,
    color: "#4d2545",
  },
  subTitleText: {
    textAlign: "center",
    fontFamily: NEXALIGHT,
    fontSize: 16,
    letterSpacing: 0.55,
    color: "#4d2545",
  },
  introContainer: {
    width: "100%",
    paddingBottom: 60,
    justifyContent: "center",
    alignItems: "center",
  },
  textContainer: {
    width: "80%",
    justifyContent: "center",
    alignContent: "center",
    paddingTop: 15,
    paddingBottom: 15,
  },
});
// define your styles
// const styles1 = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: "center",
//     alignItems: "center",
//     backgroundColor: "white"
//   },
//   text: {
//     color: "black",
//     textAlign: "center",
//     paddingHorizontal: 25,
//     fontSize: 25,
//     justifyContent: "center",
//     alignItems: "center",
//     marginTop: 5,
//     backgroundColor: "blue"
//   },
//   text1: {
//     color: "black",
//     textAlign: "center",
//     fontSize: 18,
//     justifyContent: "center",
//     alignItems: "center",
//     backgroundColor: "orange"
//   },
//   image: {
//     height: "80%",
//     width: "80%",
//     resizeMode: "contain"
//   }
// });

//make this component available to the app

const mapStateToProps = (state) => {
  // console.log("STORE REDUX=====>", state);
  return {
    user: state.user.data,
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(SliderImage);

// export default SliderImage;
