import { StyleSheet } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../assets/styles';

const styles = StyleSheet.create({
	titleContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: resize(65, 'height')
	},
	title: {
		color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: resize(25),
		fontWeight: '400',
	},
	formContainer: {
		marginTop: resize(176, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
	}
});

export default styles;