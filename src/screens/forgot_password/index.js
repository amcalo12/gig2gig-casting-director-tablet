import React, { Component } from 'react';
import { View, Text, Keyboard } from 'react-native';
import { Form } from 'native-base';
import { connect } from 'react-redux';

// custom
import styles from './styles';
import Logo from '../../components/commons/logo';
import Header from '../../components/commons/header';
import LightBar from '../../components/commons/light_bar';
import BackButton from '../../components/commons/back_button';
import InputRadius from '../../components/commons/input_radius';
import GradientButton from '../../components/commons/gradient_button';
import BackgroundWrapper from '../../components/commons/background_wrapper';
import showMessage from '../../utils/show_message';

//api
import { resetPassword } from '../../api/forgot_password';

//locale
import en from '../../locale/en';

//validators
import validateEmail from '../../utils/validate_email';
import parseError from '../../utils/parse_error';

class ForgotPassword extends Component {

  state = {
    email: '',
    loading: false
  }

  onChangeEmail = (email) => {
    this.setState({ email });
  }

  validateFields = () => {
    const { email } = this.state;
    let errorMessage = '';

    Keyboard.dismiss();

    if (email === '') {
      errorMessage = en.require_email;
    } else if (!validateEmail(email)) {
      errorMessage = en.valid_email;
    }

    return errorMessage;
  }

  handlePressSubmit = () => {
    const hasError = this.validateFields();

    if (hasError !== '') {
      return showMessage('danger', hasError);
    }

    return this.completePasswordRecovery();
  }

  completePasswordRecovery = async () => {
    const { email } = this.state;
    const { navigation } = this.props;

    this.setState({ loading: true });

    try {
      await resetPassword(email);
      this.setState({ loading: false });

      showMessage('success', 'Please check your email inbox');

      navigation.navigate('Login');
    } catch (error) {
      console.log(error.response)
      this.setState({ loading: false });
      const parsedError = parseError(error);

      showMessage('danger', parsedError);
    }
  }

  handlePressBack = () => {
    const { navigation } = this.props;

    navigation.goBack(null);
  }

  render() {
    const {
      email,
      loading
    } = this.state;

    return (
      <BackgroundWrapper>
        <LightBar />
        <Header
          left={
            <BackButton
              onPress={() => this.handlePressBack()}
            />
          }
          center={
            <Logo />
          }
        />
        <View style={styles.titleContainer}>
          <Text style={styles.title}>
            {en.forgot_password_title}
          </Text>
        </View>
        <Form style={styles.formContainer}>
          <InputRadius
            placeholder={en.email_placeholder}
            value={email}
            onChange={this.onChangeEmail}
            keyboardType='email-address'
          />
          <GradientButton
            buttonText={en.submit_text}
            onPress={() => this.handlePressSubmit()}
            loading={loading}
          />
        </Form>
      </BackgroundWrapper>
    );
  }
}

export default connect(null)(ForgotPassword);