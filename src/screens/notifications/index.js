import React, { Component } from "react";
import { Container, Content } from "native-base";
import { connect } from "react-redux";
import { ScrollView, View, Text, RefreshControl, FlatList } from "react-native";

//custom
import styles from "./styles";
import Header from "../../components/commons/header";
import LightBar from "../../components/commons/light_bar";
import ButtonMenu from "../../components/commons/button_menu";
import EmptyState, { Loading } from "../../components/commons/empty_state";
import NotificationList from "./itemsNotifications";

//locale
import en from "../../locale/en";
import {
  getNotifications,
  markNotificationsRead
} from "../../api/notifications";

class Notifications extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    notifications: [],
    isLoading: true,
    refreshing: false
  };

  componentDidMount = () => {
    this.fetchAvailableNotifications();
  };

  handlePressOpenDrawer = () => {
    const { navigation } = this.props;
    navigation.openDrawer();
  };

  handlePressDeleteNotification = () => {
    if (this.state.showInvitationModal) {
      this.setState({ showInvitationModal: false });
    } else {
      this.setState({ showInvitationModal: true });
    }
  };

  fetchAvailableNotifications = async (isRefres = false) => {
    try {
      isRefres
        ? this.setState({ refreshing: true })
        : this.setState({ isLoading: true, videos: [] });
      let categoryResponse = await getNotifications();
      console.log('categoryResponse :>> ', categoryResponse);
      let markNotification = await markNotificationsRead();
      console.log('markNotification :>> ', markNotification);
      this.setState({
        notifications: categoryResponse.data.data,
        isLoading: false
      });
      isRefres ? this.setState({ refreshing: false }) : null;
    } catch (error) {
      console.log('error ewegrge:>> ', error);
      this.setState({ isLoading: false });
    }
  };

  onAction = () => {
    console.log(" ON ACTION CALLED ")
    this.props.navigation.goBack()
  }

  removeDeletedItem = id => {
    let { notifications } = this.state;
    let removeReference = [...notifications];
    let indexDelete = removeReference.findIndex(snap => {
      return snap.id === id;
    });
    removeReference.splice(indexDelete, 1);
    this.setState({ notifications: removeReference });
  };

  render() {
    const { isLoading, refreshing, notifications } = this.state;
    return (
      <Container>
        <LightBar />
        <Header
          left={<ButtonMenu onPress={() => this.handlePressOpenDrawer()} />}
          center={
            <Text style={styles.titleNotifications}>{en.notifications}</Text>
          }
        />
        <FlatList
          contentContainerStyle={styles.pastContent}
          data={notifications}
          keyExtractor={item => item.id.toString()}
          renderItem={({ item }) => (
            <NotificationList
              {...this.props}
              fetchAvailableNotifications={this.fetchAvailableNotifications}
              removeDeletedItem={this.removeDeletedItem}
              onActionPerformed={this.onAction}
              {...item}
            />
          )}
          ListEmptyComponent={
            isLoading ? (
              <Loading />
            ) : (
                <EmptyState text={en.empty_state_past_videos} />
              )
          }
          refreshing={refreshing}
          onRefresh={() => this.fetchAvailableNotifications(true)}
          showsVerticalScrollIndicator={false}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.data
});

export default connect(mapStateToProps)(Notifications);
