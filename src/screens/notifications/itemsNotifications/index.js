import React, { Component } from "react";
import { TouchableOpacity, View, Text, Image, Modal } from "react-native";
import styles from "./styles";
import en from "../../../locale/en.js";

//Images
import notificationIcon from "../../../assets/icons/notifications/Group2243.png";
import notificationsOptions from "../../../assets/icons/notifications/Group2010.png";
import GradientButton from "../../../components/commons/gradient_button";
import DeleteNotification from "../../../assets/icons/notifications/Group1961.png";
import FileMenu from "../../../components/auditions/file_menu";
import {
  deleteNotifications,
  hadleNotification
} from "../../../api/notifications";
import { showConfirmationDialogYesNoClick } from "../../../utils/constant";

class ItemNotification extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    showInvitationModal: false,
    showOptions: false,
    optionsPosition: { py: 0, px: 0 },
    isLoading: false,
    btnA: false,
    btnR: false
  };

  handlePressDeleteNotification = () => {
    if (this.state.showInvitationModal) {
      this.setState({ showInvitationModal: false });
    } else {
      this.setState({ showInvitationModal: true });
    }
  };

  handleGetMeasure = async index => {
    const refName = `options_${index}`;
    if (refName in this) {
      this[refName].measure((a, b, width, height, px, py) => {
        this.setState({ optionsPosition: { py, px } }, () => {
          this.handleOptions();
        });
      });
    } else {
      this.setState({ optionsPosition: { py: 0, px: 0 } });
    }
  };

  handleSetRef = (file, index) => {
    this[`options_${index}`] = file;
  };

  handleOptions = () => {
    this.setState(prevState => ({
      showOptions: !prevState.showOptions
    }));
  };

  handleDeleteNoti = async id => {
    let { removeDeletedItem } = this.props;
    try {
      this.setState({ isLoading: true });
      await deleteNotifications(id);
      this.setState({ isLoading: false });
      removeDeletedItem(id);
      this.handleOptions();
    } catch (error) {
      console.log(error);
      this.setState({ isLoading: false });
    }
  };

  manageInvitation = async (custom_data, id, status) => {
    console.log(" CHECK PROPS ", this.props);

    let { fetchAvailableNotifications } = this.props;
    try {
      status === 1
        ? this.setState({ btnA: false })
        : this.setState({ btnR: false });
      await hadleNotification({
        custom_data,
        id,
        status
      });
      // if (status == 0) {
      //   showConfirmationDialogYesNoClick("You have Decline Invitation", () => {
      //     this.props.navigation.goBack();
      //   });
      //   this.setState({ isLoading: false });
      // } else {
      //   showConfirmationDialogYesNoClick("You have Accepted Invitation", () => {
      //     //   if (this.props.navigation.state.params.callback != undefined) {
      //     // 	this.props.navigation.state.params.callback()
      //     //   }
      //     this.setState({ isLoading: false });
      //     this.props.navigation.goBack();
      //   });
      // }

      fetchAvailableNotifications();
      // status === 1 ? this.setState({ btnA: false }) : this.setState({ btnR: false });
    } catch (error) {
      console.log(error);
      this.setState({ isLoading: false });
    }
  };

  capitalize = s => {
    if (typeof s !== "string") return "";
    console.log("<========= toUpperCase ==========>");
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  render() {
    let { showOptions, optionsPosition, isLoading, btnA, btnR } = this.state;
    let { title, time_ago, code, id, status, custom_data } = this.props;
    let titleCustom = title.toString();
    titleCustom = titleCustom.replace(/<strong>/g, "");
    titleCustom = titleCustom.replace(/<\/strong>/g, "");
    return (
      <View style={styles.borderBottom}>
        <View style={styles.containerOne}>
          <View style={styles.iconAndMessage}>
            <Image source={notificationIcon} />
            <Text style={styles.notificationMessage}>
              {" "}
              {titleCustom}{" "}
              {status === "accepted" || status === "rejected"
                ? ` - ${this.capitalize(status)}`
                : ""}
            </Text>
          </View>
          {code === "autidion_add_contribuidor" &&
            status !== "accepted" &&
            status !== "rejected" && (
              <View style={styles.AcceptAndDeclineBox}>
                <GradientButton
                  loading={btnA}
                  buttonText={en.accept_button}
                  containerStyle={styles.buttonAccept}
                  gradientStyle={styles.gradientButtonAccept}
                  textStyle={styles.buttonAcceptText}
                  onPress={() => this.manageInvitation(custom_data, id, 1)}
                />
                <GradientButton
                  light
                  spinnerColor="black"
                  buttonText={en.decline_button}
                  onPress={() => this.manageInvitation(custom_data, id, 0)}
                  loading={btnR}
                  containerStyle={styles.buttonNextontainer}
                  gradientStyle={styles.buttonSaveGradient}
                  textStyle={[styles.buttonSaveText, { color: "#5f2543" }]}
                />
              </View>
            )}
        </View>
        <View
          renderToHardwareTextureAndroid={true}
          ref={fileRef => this.handleSetRef(fileRef, id)}
          style={styles.containerTwo}
        >
          <TouchableOpacity onPress={() => this.handleGetMeasure(id)}>
            <Image
              style={styles.showDeleteOptionButtonImage}
              source={notificationsOptions}
            />
          </TouchableOpacity>
          <Text style={styles.notificationDay}>{time_ago}</Text>
        </View>
        <View style={{ position: "relative" }}>
          <FileMenu
            show={showOptions}
            dismiss={() => this.handleOptions()}
            posy={optionsPosition.py}
            posx={optionsPosition.px}
            showOptionDelete
            delete={() => this.handleDeleteNoti(id)}
            hasPadding={false}
            isLoading={isLoading}
          >
            <View style={styles.containerTwo}>
              <TouchableOpacity onPress={() => this.handleOptions()}>
                <Image
                  style={styles.showDeleteOptionButtonImage}
                  source={notificationsOptions}
                />
              </TouchableOpacity>
              <Text style={styles.notificationDay}>{time_ago}</Text>
            </View>
          </FileMenu>
        </View>
      </View>
    );
  }
}
export default ItemNotification;
