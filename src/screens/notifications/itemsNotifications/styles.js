import { StyleSheet, Platform } from 'react-native';

//custom 
import { resize, NEXABOLD, height, NEXALIGHT, STATUS_BAR_HEIGHT } from '../../../assets/styles';
const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT;

const styles = StyleSheet.create({
    scrollview: {
        paddingVertical: resize(8, 'height')
    },
    content: {
        paddingBottom: resize(20),
    },
    titleNotifications: {
        fontFamily: NEXABOLD,
        fontSize: resize(20),
        color: 'white'
    },
    borderBottom: {
        width: '100%',
        borderBottomColor: '#F1F1F1',
        borderBottomWidth: 2,
        paddingHorizontal: resize(40),
        paddingVertical: resize(35, 'height'),
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    itemContainer: {

    },
    containerOne: {
        flex: 5,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    iconAndMessage: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    notificationMessage: {
        fontFamily: NEXABOLD,
        fontSize: resize(15),
        color: '#4D2545',
        marginLeft: resize(20),
    },
    notificationDay: {
        fontFamily: NEXABOLD,
        fontSize: resize(14),
        color: '#5f2543'
    },
    AcceptButtonImage: {
        resizeMode: 'contain',
    },
    containerTwo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    AcceptAndDeclineBox: {
        marginLeft: resize(65),
        marginTop: resize(25, 'height'),
        flexDirection: 'row',
        alignItems: 'center'
    },
    DeclineButton: {
        marginLeft: resize(10),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderRadius: 25,
        borderColor: '#4D2545',
        width: resize(180),
        height: resize(54, 'height')
    },
    buttonTextDecline: {
        fontFamily: NEXABOLD,
        fontSize: resize(15),
        color: '#4D2545'
    },
    showDeleteOptionButtonImage: {
        resizeMode: 'contain',
    },
    showDeleteNotificationImage: {
        resizeMode: 'contain',
    },
    deleteNotificationButton: {
        position: 'absolute',
        zIndex: 2,
        top: resize(25, 'height'),
        right: resize(10)
    },
    nBContainer: {
        backgroundColor: 'transparent'
    },
    dismissButton: {
        width: '100%',
        height: containersHeight,
        position: 'absolute',
        justifyContent: 'flex-end'
    },
    blurView: {
        width: '100%',
        position: 'absolute',
        height: resize(730, 'height'),
        backgroundColor: '#f0f0f0',
        opacity: Platform.OS === 'ios' ? 0.84 : .60,
    },
    cardContainer: {
        width: '100%',
        height: containersHeight,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        position: 'absolute',
        zIndex: 2,
    },
    shape: {
        alignSelf: 'center',
        width: 0,
        height: 0,
        borderLeftWidth: resize(20),
        borderRightWidth: resize(20),
        borderBottomWidth: resize(Platform.OS === 'ios' ? 40 : 20, 'height'),
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: '#FFF',
        position: 'absolute',
        top: -resize(Platform.OS === 'ios' ? 40 : 20, 'height')
    },
    card: {
        width: resize(175),
        height: resize(68, 'height'),
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: { width: 3, height: 0 },
        shadowRadius: resize(6),
        borderRadius: resize(13),
        backgroundColor: '#ffffff',
        marginBottom: resize(Platform.OS === 'ios' ? 330 : 330, 'height'),
        marginRight: resize(28),
        elevation: 20
    },
    buttonText: {
        textAlign: 'center',
        color: '#4d2545',
        fontFamily: NEXALIGHT,
        fontSize: resize(15),
        marginLeft: resize(8)
    },
    deleteNotificationContentBox: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginVertical: resize(20, 'height')
    },
    deleteIcon: {
        height: resize(25, 'height'),
        width: resize(19),
        resizeMode: 'contain'
    },
    gradientButtonAccept: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.4)',
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        borderRadius: 28,
    },
    buttonAcceptText: {
        color: '#ffffff',
        fontFamily: NEXABOLD,
        fontSize: resize(15),
        paddingVertical: resize(5, 'height')
    },
    buttonAccept: {
        width: resize(180),
        height: resize(54, 'height')
    },
    buttonNextontainer: {
        width: resize(180),
        height: resize(50, 'height'),
        borderRadius: resize(40),
        borderColor: '#5f2543',
        borderWidth: 2,
        marginLeft: resize(15)
    },
    buttonSaveGradient: {
        width: '100%',
        height: '100%',
        borderRadius: 28,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonSaveText: {
        color: '#ffffff',
        fontFamily: NEXABOLD,
        fontSize: resize(15),
        paddingVertical: resize(5, 'height')
    },
});

export default styles;