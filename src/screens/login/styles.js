import { StyleSheet, Platform } from 'react-native';

//custom
import { BREAK_REGULAR, NEXABOLD, resize } from '../../assets/styles';

const styles = StyleSheet.create({
	container: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	titleContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		height: '50%'
	},
	title: {
		borderColor: 'rgba(0, 0, 0, 0)',
		borderStyle: 'solid',
		borderWidth: 2,
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(92),
		...Platform.select({
			android: {
				fontWeight: '400'
			},
			ios: {
				fontWeight: '600'
			}
		}),
		height: '100%'
	},
	titleNumber: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(101),
		paddingTop: resize(20, 'height'),
		...Platform.select({
			android: {
				fontWeight: '400',
			},
			ios: {
				fontWeight: '700',
			}
		}),
		height: '100%'
	},
	forgotPasswordButton: {
		marginTop: resize(16, 'height'),
		width: resize(125),
		height: resize(16, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonTitle: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(13),
		fontWeight: '400',
	},
	signUpButton: {
		marginTop: resize(111, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
		width: resize(125),
		height: resize(16, 'height')
	},
});

export default styles;