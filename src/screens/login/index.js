import React, { Component } from "react";
import { Text, TouchableOpacity, Keyboard } from "react-native";
import { Form } from "native-base";
import { connect } from "react-redux";

//custom
import styles from "./styles";
import LightBar from "../../components/commons/light_bar";
import GradientButton from "../../components/commons/gradient_button";
import InputRadius from "../../components/commons/input_radius";
import BackgroundWrapper from "../../components/commons/background_wrapper";
import showMessage from "../../utils/show_message";
import Logo from "../../components/commons/logo";
import { getAllTeamAdmins } from "../../api/auditions";

// api
import { fetchAuth } from "../../api/login";

//actions
import { INIT_SESSION } from "../../actions/auth";
import { SET_USER } from "../../actions/user";

// locale
import en from "../../locale/en";

//validators
import validateEmail from "../../utils/validate_email";
import parseError from "../../utils/parse_error";

class Login extends Component {
  state = {
    email: "",
    password: "",
    loading: false,
  };

  onChangeEmail = (email) => {
    this.setState({ email });
  };

  onChangePassword = (password) => {
    this.setState({ password });
  };

  validateFields = () => {
    const { email, password } = this.state;
    let errorMessage = "";

    Keyboard.dismiss();

    if (email === "") {
      errorMessage = en.require_email;
    } else if (!validateEmail(email)) {
      errorMessage = en.valid_email;
    } else if (password === "") {
      errorMessage = en.require_password;
    }

    return errorMessage;
  };

  handlePressLogin = () => {
    const hasError = this.validateFields();

    if (hasError != "") {
      return showMessage("danger", hasError);
    }

    return this.completeLogin();
  };

  completeLogin = async () => {
    const { email, password } = this.state;
    const { dispatch } = this.props;

    this.setState({ loading: true });
    try {
      const response = await fetchAuth(email, password);

      console.log(response.data);
      console.log("LOGIN RESPONSE=====>", response.data);

      const type = response.data.data.details;

      if (type.type === "1" || type.type === 1) {
        dispatch({
          type: SET_USER,
          payload: {
            email: response.data.data.email,
            id: response.data.data.id,
            user_type: response.data.data.details.type,
            name: `${response.data.data.details.first_name} ${response.data.data.details.last_name}`,
            imageUrl:
              response.data.data.image != null
                ? response.data.data.image.url
                : "",
            thumbnail:
              response.data.data.image != null
                ? response.data.data.image.thumbnail
                : "",
            agencyName: response.data.data.details.agency_name,
            // isPaidUser: true
            is_invited: response.data.data.is_invited,
            // admin_id: response.data.data.admin_id,
            isPaidUser: response.data.data.is_premium == 1 ? true : false,
            subscriptionDetail:
              response.data.data.subscription != undefined
                ? response.data.data.subscription
                : undefined,
            isAdminSelect: response.data.data.selected_admin,
          },
        });

        dispatch({
          type: INIT_SESSION,
          payload: {
            authorize: true,
            token: `Bearer ${response.data.access_token}`,
          },
        });
        // this.getTeamAdminList(dispatch, `Bearer ${response.data.access_token}`);
      } else {
        this.setState({ loading: false });
        showMessage("danger", "Unauthorized");
      }
    } catch (error) {
      console.log("error :>> ", error);
      this.setState({ loading: false });
      const parsedError = parseError(error);

      showMessage("danger", parsedError);
    }
  };

  getTeamAdminList = async (dispatch, accessToken) => {
    console.log("accessToken :>> ", accessToken);
    // this.setState({ loading: true });
    try {
      let adminList = await getAllTeamAdmins();
      this.setState({ loading: false });
      if (adminList.data && adminList.status == 200) {
        let data = adminList.data
          ? adminList.data.data
            ? adminList.data.data
            : []
          : [];
        let isSelectAdmin = false;

        if (data.length > 0) {
          data.map((item) => {
            if (item.is_selected && item.is_selected === 1) {
              isSelectAdmin = true;
            }
          });
        }
        dispatch({
          type: INIT_SESSION,
          payload: {
            authorize: true,
            token: accessToken,
            isAdminSelect: isSelectAdmin,
          },
        });
      }
    } catch (error) {
      console.log("get All team Admins =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      dispatch({
        type: INIT_SESSION,
        payload: {
          authorize: true,
          token: accessToken,
          isAdminSelect: true,
        },
      });
      this.setState({ loading: false });
    }
  };
  handlePressForgotPassword = () => {
    const { navigation } = this.props;

    navigation.navigate("ForgotPassword");
  };

  handlePressSignUp = () => {
    const { navigation } = this.props;

    navigation.navigate("SignUp");
  };

  render() {
    const { email, password, loading } = this.state;

    return (
      <BackgroundWrapper style={styles.container}>
        <LightBar />
        <Logo
          displayCasting
          titleStyle={styles.title}
          titleNumberStyle={styles.titleNumber}
          containerStyle={styles.titleContainer}
        />
        <Form>
          <InputRadius
            placeholder={en.email_placeholder}
            value={email}
            onChange={this.onChangeEmail}
            keyboardType="email-address"
          />
          <InputRadius
            placeholder={en.password_placeholder}
            value={password}
            onChange={this.onChangePassword}
            secureTextEntry
          />
          <GradientButton
            buttonText={en.login_button_text}
            onPress={() => this.handlePressLogin()}
            loading={loading}
          />
        </Form>
        <TouchableOpacity
          style={styles.forgotPasswordButton}
          onPress={() => this.handlePressForgotPassword()}
        >
          <Text style={styles.buttonTitle}>{en.forgot_password_text}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.signUpButton}
          onPress={() => this.handlePressSignUp()}
        >
          <Text style={styles.buttonTitle}>{en.create_account_text}</Text>
        </TouchableOpacity>
      </BackgroundWrapper>
    );
  }
}

export default connect(null)(Login);
