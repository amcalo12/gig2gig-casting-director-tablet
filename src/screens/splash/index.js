import React, { Component } from "react";
import { Platform, View } from "react-native";
import { connect } from "react-redux";

//routes
import AuthNavigation from "../../routes/auth";
import AppNavigation from "../../routes/app";
import { INIT_SESSION } from "../../actions/auth";

import firebase from "react-native-firebase";
// Optional: Flow type
import type {
  Notification,
  NotificationOpen,
  RemoteMessage,
} from "react-native-firebase";

// components
import SplashView from "../../components/commons/splash";
import { AppTourNavigator } from "../../routes/app_tour";
// import AppTourNavigator from "../../routes/app_tour";
import { getAllTeamAdmins } from "../../api/auditions";
import { SelectAdminNavigator } from "../../routes/select_admin";

class Splash extends Component {
  state = {
    shouldDisplayMainScreen: true,
    isSelectAdminScreen: false,
    loading: false,
    // isFetch: false,
  };

  componentDidMount() {
    this.openNotification();
    this.openNotificationWileAppIsClosed();
    this.showNotification();

    // setTimeout(() => {
    // 	this.setState({
    // 		shouldDisplayMainScreen: true
    // 	});
    // 	clearTimeout(this)
    // }, 4000);
  }

  openNotificationWileAppIsClosed = async () => {
    const notificationOpen: NotificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      // App was opened by a notification
      // Get the action triggered by the notification being opened
      const action = notificationOpen.action;
      // Get information about the notification that was opened
      const notification: Notification = notificationOpen.notification;
      console.log({ action, notification });
    }
  };

  showNotification = () => {
    this.messageListener = firebase
      .messaging()
      .onMessage((message: RemoteMessage) => {
        console.log("messageListener", { message });
        this.displayNotification(message);
      });
    this.removeNotificationListener = firebase
      .notifications()
      .onNotification((notification: Notification) => {
        console.log("removeNotificationListener", { notification });
        this.displayNotification(notification);
      });
  };

  displayNotification = (notification) => {
    console.log(notification);
    const channelId = new firebase.notifications.Android.Channel(
      "Default",
      "Default",
      firebase.notifications.Android.Importance.High
    );
    firebase.notifications().android.createChannel(channelId);

    let notification_to_be_displayed = new firebase.notifications.Notification({
      data: notification.data,
      sound: "default",
      show_in_foreground: true,
      title: notification.title,
      body: notification.body,
    });
    console.log(notification_to_be_displayed);
    if (Platform.OS == "android") {
      notification_to_be_displayed.android
        .setPriority(firebase.notifications.Android.Priority.High)
        .android.setChannelId("Default")
        .android.setVibrate(1000)
        .android.setSmallIcon("ic_stat_name");
    }

    firebase.notifications().displayNotification(notification_to_be_displayed);
  };

  openNotification = () => {
    this.removeNotificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen: NotificationOpen) => {
        console.log(notificationOpen);
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification;
      });
  };

  render() {
    const { authorize, isAppTour, isAdminSelect } = this.props;
    const { shouldDisplayMainScreen, isSelectAdminScreen } = this.state;
    console.log("this.props===");
    console.log(this.props);
    if (!authorize) {
      return <AuthNavigation />;
    }

    if (authorize && isAppTour) {
      return <AppTourNavigator />;
    }
    // if (
    //   !this.state.loading &&
    //   this.props.user.is_invited &&
    //   authorize &&
    //   !isAppTour &&
    //   isAdminSelect !== null &&
    //   !isAdminSelect
    // ) {
    //   return <SelectAdminNavigator />;
    // }
    // if (
    //   authorize &&
    //   !isAppTour &&
    //   isAdminSelect !== null &&
    //   !isAdminSelect &&
    //   !this.props.user.is_invited
    // ) {
    //   return <AppNavigation />;
    // }
    // if (authorize && !isAppTour && isAdminSelect !== null && isAdminSelect) {
    //   return <AppNavigation />;
    // }

    // return (
    //   <View
    //     style={{ backgroundColor: "#fff", width: "100%", height: "100%" }}
    //   />
    // );
    return <AppNavigation />;

    // if (shouldDisplayMainScreen) {

    // }

    // return <SplashView />
  }
}

const mapStateToProps = (state) => ({
  authorize: state.auth.authorize,
  isAppTour: state.auth.isAppTour,
  isAdminSelect: state.auth.isAdminSelect,
  user: state.user.data,
});

export default connect(mapStateToProps)(Splash);
