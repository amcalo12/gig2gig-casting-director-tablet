import React, { Component } from "react";
import { View, Text, Image, Linking, Platform } from "react-native";
import { Container, Content } from "native-base";
import { connect } from "react-redux";

//custom
import styles from "./style";
import Header from "../../components/commons/header";
import ButtonMenu from "../../components/commons/button_menu";
import HeaderText from "../../components/commons/header_text";
import GradientButton from "../../components/commons/gradient_button";
import ItemMenu from "../../components/settings/item_menu";
import AlertIOS from "../../components/commons/alert_ios";
import settings from "../../../settings";

//actions
import { DESTROY_SESSION } from "../../actions/auth";

//locale
import en from "../../locale/en";
import requestDeviceToken from "../../utils/device_token";
import {
  DEVICE_ID,
  subscriptionAlert,
  openSubscriptionDialog,
  SUBSCRIPTION_ALERT,
} from "../../utils/constant";
import { setDeviceToken } from "../../api/login";
import { getfeedback } from "../../api/auditions";
import { DEFAULT_INSTANT_FEEDBACK } from "../../actions/user";
import { Assets } from "../../assets";

class Settings extends Component {
  constructor(props) {
    super(props);
    // this.props.user.isPaidUser = this.props.user.isPaidUser
  }
  state = {
    signOutModal: false,
  };

  // componentDidMount() {
  //   this.getInstantFeedback()
  // }

  // //GET INSTANT FEEDBACK
  // getInstantFeedback = async () => {
  //   const { dispatch } = this.props;
  //   const { user } = this.props;
  //   console.log("<===== user id =====>", user.id)

  //   this.setState({ loading: true });
  //   try {
  //     const response = await getfeedback(user.id);

  //     this.setState({
  //       instantFeedback: response.data.data
  //     });
  //     console.log("SUCCESS Get Instant Feedback =====>", response)
  //     let defaultMessage = {}
  //     defaultMessage['reject'] = response.data.data.comment
  //     defaultMessage['accept'] = response.data.data.positiveComment
  //     dispatch({
  //       type: DEFAULT_INSTANT_FEEDBACK,
  //       payload: defaultMessage
  //     });
  //   } catch (error) {
  //     console.log("ERROR Get Instant Feedback =====>", error);
  //   }

  //   this.setState({ loading: false });
  // }

  handlePressOpenDrawer = () => {
    const { navigation } = this.props;

    navigation.openDrawer();
  };

  handlePressNavigateTo = (view) => {
    const { navigation } = this.props;

    navigation.navigate(view);
  };

  handlePressSignOut = () => {
    this.setState({ signOutModal: true });
  };

  dismissLogOut = () => {
    this.setState({ signOutModal: false });
  };
  onSetDeviceToken = async () => {
    console.log(" set device called ");
    try {
      const token = await requestDeviceToken();
      if (token) {
        const { dispatch } = this.props;
        await setDeviceToken({
          pushkey: "",
          device_id: DEVICE_ID,
          device_type: Platform.OS,
        });
        console.log("DEVICE ID DEVICE ID logout=====>", DEVICE_ID);
        dispatch({
          type: DESTROY_SESSION,
          payload: {},
        });
      }
    } catch (error) {
      console.log(" set device called 111", error);
      console.log({ error });
    } // ignoring token's errors to not disturb users
  };
  agreeLogOut = async () => {
    console.log(" set device called ");

    this.onSetDeviceToken();
    this.dismissLogOut();
  };

  sendMailContact = () => {
    Linking.openURL(`mailto:${settings.contactEmail}`);
  };

  render() {
    const { signOutModal } = this.state;
    const { user } = this.props;

    return (
      <Container>
        <AlertIOS
          title=""
          show={signOutModal}
          dismiss={this.dismissLogOut}
          agree={this.agreeLogOut}
        >
          <Text style={styles.monitorModeText}>{en.sign_out_message}</Text>
        </AlertIOS>
        <Header
          left={<ButtonMenu onPress={() => this.handlePressOpenDrawer()} />}
          center={<HeaderText title={en.settings_title} />}
        />
        <Content style={styles.content}>
          <View style={styles.userMain}>
            <View style={styles.imageContainer}>
              {/* {user.imageUrl && (
                <Image source={{ uri: user.thumbnail != null ? user.thumbnail : user.imageUrl }} style={styles.image} />
              )} */}
              <Image
                source={
                  user.imageUrl != null && user.thumbnail
                    ? {
                        uri:
                          user.thumbnail != null
                            ? user.thumbnail
                            : user.imageUrl,
                      }
                    : Assets.role
                }
                style={styles.image}
              />
            </View>
            <View style={styles.userInfoContainer}>
              <Text style={styles.userName}>
                {String(user.name).replace("null", "")}
              </Text>
              <Text numberOfLines={3} style={styles.userCompany}>
                {user.agencyName}
              </Text>
            </View>
            <View style={styles.logoutContainer}>
              <GradientButton
                buttonText={en.sign_out_button_text}
                containerStyle={styles.gradientButtonContainer}
                gradientStyle={styles.gradientButton}
                textStyle={styles.gradientButtonText}
                onPress={() => this.handlePressSignOut()}
              />
            </View>
          </View>
          <View style={styles.menuContainer}>
            <ItemMenu
              title={en.my_info_title}
              // onPress={() => this.handlePressNavigateTo("Profile")}
              onPress={() =>
                this.props.navigation.navigate("Profile", { isFromHome: false })
              }
            />
            {!this.props.user.is_invited ? (
              <ItemMenu
                title={en.subscription_title}
                onPress={() => this.handlePressNavigateTo("Subscription")}
              />
            ) : null}
            <ItemMenu
              title={en.app_info_title}
              onPress={() => this.handlePressNavigateTo("AppHelp")}
            />
            <ItemMenu
              title={en.push_title}
              onPress={() =>
                this.props.user.isPaidUser
                  ? this.handlePressNavigateTo("Push")
                  : openSubscriptionDialog()
              }
            />
            <ItemMenu
              title={en.instant_feeback_title}
              onPress={() =>
                this.props.user.isPaidUser
                  ? this.handlePressNavigateTo("DefaultFeedbackMessages")
                  : openSubscriptionDialog()
              }
            />
            <ItemMenu
              title={en.qr_title}
              onPress={() =>
                this.props.user.isPaidUser
                  ? this.handlePressNavigateTo("QrCode")
                  : openSubscriptionDialog()
              }
            />
            {this.props.user.is_invited ? (
              <ItemMenu
                title={en.select_team_admin}
                onPress={() =>
                  this.props.navigation.navigate("SelectTeamAdmin", {
                    isFromSettings: true,
                  })
                }
              />
            ) : null}

            <ItemMenu
              title={en.help_title}
              onPress={() => this.handlePressNavigateTo("Help")}
            />
            <ItemMenu
              title={en.terms_of_use_title}
              onPress={() => this.handlePressNavigateTo("TermsOfUse")}
            />
            <ItemMenu
              title={en.privacy_policy_title}
              onPress={() => this.handlePressNavigateTo("PrivacyPolicy")}
            />
            <ItemMenu
              title={en.contact_us_title}
              onPress={() => this.sendMailContact()}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
});

export default connect(mapStateToProps)(Settings);
