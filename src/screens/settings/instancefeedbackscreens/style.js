import { StyleSheet } from 'react-native';

//custom
import { width, height, resize, NEXABOLD, NEXALIGHT } from '../../../assets/styles';
import colors from '../../../utils/colors';

const styles = StyleSheet.create({
  content: {
    paddingTop: resize(33, 'height'),
  },
  performersFont:{
    color: '#4d2545',
		fontFamily: NEXALIGHT,
		fontSize: resize(16),
    marginTop:20,
    alignItems:'center',
    alignSelf:'center',
    padding:20
  },
  messageContainer: { 
    borderRadius: 10,
     backgroundColor: "#f5f5f5",
      marginTop: 30, 
      marginHorizontal: 180,
      alignSelf:'center'
    },
  messageText: {
      color: colors.PRIMARY, 
      textAlign: "center",
      alignSelf: "center", 
      // lineHeight: 25,
      backgroundColor: "#f5f5f5", 
      padding: 30,
     fontFamily: NEXALIGHT,
     fontSize: resize(18),
  },
  newGroupContainer: {
    marginTop: 10,
    flexDirection: "row", paddingHorizontal: 20, paddingVertical: 8, borderRadius: 4,
    alignSelf: "center", alignItems: "center", 
  },
  newGroupText: {
    fontSize: resize(20), marginLeft: 10, color: "white",
    fontFamily: NEXALIGHT
  },
});

export default styles;