import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Keyboard,
  TextInput
} from "react-native";
import { Container, Content, Form } from "native-base";
import moment from "moment";
import { connect } from "react-redux";

//custom
import styles from "./style";
import Header from "../../../components/commons/header";
import BackButton from "../../../components/commons/back_button";
import HeaderText from "../../../components/commons/header_text";
import Loading from "../../../components/commons/loading";
import { feedbackUpdate } from "../../../api/settings";

//locale
import en from "../../../locale/en";
import showMessage from "../../../utils/show_message";
import { DEFAULT_INSTANT_FEEDBACK } from "../../../actions/user";
import { withNavigationFocus } from "react-navigation";

class InstanceFeedback extends Component {
  constructor(props) {
    super(props);
    console.log('this.props :', this.props);
    this.isPositive = this.props.navigation.state.params.isPositive
    this.state = {
      loading: false,
      isEdit: false,
      editedFeedback: this.isPositive ? this.props.instantFeedback.accept
        : this.props.instantFeedback.reject
    };
  }

  componentDidMount() { }

  handlePressBackButton = () => {
    const { navigation } = this.props;

    navigation.goBack(null);
  };

  //CHECK RIGHT HEADER BUTTON
  renderrightIcon = () => {
    return (
      <TouchableOpacity
        style={styles.newGroupContainer}
        onPress={() => {
          // this.setState({ isEdit: true }), this.editFeedbackApi();
          if (this.state.isEdit == false) {
            this.setState({ isEdit: true });
          } else {
            this.editFeedbackApi();
          }
        }}
      >
        <Text style={styles.newGroupText}>
          {this.state.isEdit ? "Save" : "Edit"}
        </Text>
      </TouchableOpacity>
    );
  };

  onEditedFeedback = text => {
    this.setState({ editedFeedback: text });
  };
  //EDIT INSTANT FEEDBACK API CALL
  editFeedbackApi = async () => {
    this.setState({ loading: true });
    try {
      let params = {
        feedback: this.state.editedFeedback,
        type: this.isPositive ? "positive" : "negative"
      };
      console.log(
        "PARAMS EDIT FEEDBACK API CALL=====> ",
        JSON.stringify(params)
      );
      const response = await feedbackUpdate(params);
      //this.setState({ isEdit: true })
      console.log(
        "RESPONSE EDIT INSTANT FEEDBACK API CALL =====>",
        JSON.stringify(response)
      );
      this.setState({ loading: false, isEdit: false });
      const { navigation, dispatch } = this.props;

      // let defaultMessage = {}
      // defaultMessage['reject'] = this.state.editedFeedback
      // defaultMessage['accept'] = "Positive message"

      if (this.isPositive) {
        this.props.instantFeedback['accept'] = this.state.editedFeedback
      } else {
        this.props.instantFeedback['reject'] = this.state.editedFeedback
      }

      dispatch({
        type: DEFAULT_INSTANT_FEEDBACK,
        payload: this.props.instantFeedback
      });

      showMessage("success", "InstantFeedback Updated");
    } catch (error) {
      this.setState({ loading: false });
      console.log("EDIT INSTANT FEEDBACK API CALL ERROR =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
    }
  };

  render() {
    return (
      <Container>
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
          center={<HeaderText title={en.instant_feeback_title} />}
          right={this.renderrightIcon()}
        />
        <View style={{ backgroundColor: "white", flex: 1 }}>
          <Text style={styles.performersFont}>
            {this.isPositive ? en.instant_feedback_select_message : en.instant_feedback_reject_message}
          </Text>

          <View
            style={styles.messageContainer}
            pointerEvents={this.state.isEdit ? "auto" : "none"}
          >
            <TextInput
              style={styles.messageText}
              value={this.state.editedFeedback}
              onChangeText={this.onEditedFeedback}
            />
            {/* {"Thanks for attending. That's all we need today."} */}
          </View>
        </View>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  console.log("STORE REDUX=====>", state);
  console.log("STORE REDUX instantFeedback=====>", state.user.instantFeedback);
  return {
    user: state.user.data,
    instantFeedback: state.user.instantFeedback
  };
};

export default withNavigationFocus(connect(mapStateToProps)(InstanceFeedback));
