import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, Keyboard } from "react-native";
import { Container, Content, Form } from "native-base";
import moment from "moment";
import { connect } from "react-redux";

//custom
import styles from "./style";
import { gradient, gradientLight, resize } from "../../../assets/styles";
import Header from "../../../components/commons/header";
import BackButton from "../../../components/commons/back_button";
import HeaderText from "../../../components/commons/header_text";
import InputDisable from "../../../components/settings/profile/input_disable";
import GradientButtom from "../../../components/commons/gradient_button";
import SelectDisable from "../../../components/settings/profile/select_disable";
import DatepickerDisable from "../../../components/settings/profile/datepicker_disable";
import statesList from "../../../utils/states_list";
import Loading from "../../../components/commons/loading";
import getImage from "../../../utils/get_image";
import showMessage from "../../../utils/show_message";
import parseError from "../../../utils/parse_error";

//api
import { fetchCurrentUser, updateCurrentUser } from "../../../api/settings";

//actions
import { SET_USER } from "../../../actions/user";

// validators
import validateEmail from "../../../utils/validate_email";

import ImagePicker from "react-native-image-crop-picker";

//locale
import en from "../../../locale/en";
import { getThumbnail } from "../../../utils/constant";
import RenameModal from "../../../components/auditions/rename_file";
import { renameResource } from "../../../api/auditions";
import { Assets } from "../../../assets";
const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*#?&:()._-]{8,}$/;
import { NavigationActions, StackActions } from "react-navigation";

const countryList = require("../../../utils/country_list.json");
class Profile extends Component {
  constructor(props) {
    super(props);
    console.log("this.props :>> ", this.props);
    this.isFromHome = this.props.navigation.state.params.isFromHome;
  }
  state = {
    name: "",
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    agencyName: "",
    job: "",
    address: "",
    city: "",
    state: "",
    country: "",
    zip: "",
    birthDate: null,
    // disableEdit: this.props.navigation.state.params.isFromHome ? false : true,
    disableEdit: false,
    loading: false,
    loadingButton: false,
    image: null,
    isVisible: false,
    fileName: "",
    imageData: null,
  };

  componentDidMount() {
    this.fetchUser();
  }

  fetchUser = async () => {
    const { user, dispatch } = this.props;

    this.setState({ loading: true });

    try {
      const response = await fetchCurrentUser(user.id);
      const data = response.data.data;
      const details = data.details;
      let firstName = details.first_name;
      let lastName = details.last_name;
      let name = String(`${firstName} ${lastName}`).replace("null", "");

      dispatch({
        type: SET_USER,
        payload: {
          email: data.email,
          id: data.id,
          user_type: data.details.type,
          first_name: data.details.first_name,
          last_name: data.details.last_name,
          name: `${data.details.first_name} ${data.details.last_name}`,
          imageUrl: data.image.url,
          thumbnail: data.image.thumbnail,
          agencyName: data.details.agency_name,
          is_invited: data.is_invited,
          isAdminSelect: data.selected_admin,
          // admin_id: data.admin_id,
          isPaidUser: data.is_premium == 1 ? true : false,
          subscriptionDetail:
            data.subscription != undefined ? data.subscription : undefined,
        },
      });

      this.setState({
        // name,
        first_name: details.first_name,
        last_name: details.last_name,
        email: data.email,
        agencyName: details.agency_name,
        job: details.profesion,
        address: details.address,
        city: details.city,
        state: details.state,
        country: details.country,
        zip: details.zip,
        imageData: data.image,
        birthDate:
          details.birth != null ? moment(details.birth).format() : null,
      });
    } catch (error) {
      console.log(error);
    }

    this.setState({ loading: false });
  };

  onCloseClick = () => {
    this.setState({ isVisible: false });
  };

  onSubmitClick = async () => {
    // console.log('this.props :', this.props.user);
    // console.log('this.props 11:', this.state.imageData);
    // return

    this.setState({ isVisible: false });
  };

  onChangeText = (fileName) => {
    this.setState({ fileName });
  };

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    );
  };

  handleChangeImage = async () => {
    try {
      const response = await getImage();
      ImagePicker.openCropper({
        path: response.uri,
        // width: image.width,
        // height: image.height
      })
        .then(async (data) => {
          console.log("Crop image", data);
          let newData = response;
          newData["uri"] = data.path;

          let thumbnailImage = await getThumbnail(data.path);
          console.log("thumbnail :", thumbnailImage);

          this.setState({
            image: { uri: data.path, thumbnail: thumbnailImage.uri },
            isVisible: true,
            fileName: "",
          });
        })
        .catch(async (error) => {
          let thumbnailImage = await getThumbnail(response.uri);
          console.log("thumbnail :", thumbnailImage);

          console.log("Crop image error", error);
          this.setState({
            image: { uri: response.uri, thumbnail: thumbnailImage.uri },
            isVisible: true,
            fileName: "",
          });
        });
      // this.setState({ image: { uri: response.uri } });
    } catch (error) {
      console.log(error);
    }
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;

    navigation.goBack(null);
  };

  onChangeName = (name) => {
    this.setState({ name });
  };

  onFirstNameChange = (first_name) => {
    this.setState({ first_name });
  };

  onLastNameChange = (last_name) => {
    this.setState({ last_name });
  };

  onChangeEmail = (email) => {
    this.setState({ email });
  };

  onChangePassword = (password) => {
    this.setState({ password });
  };

  onChangeAgencyName = (agencyName) => {
    if (agencyName.length <= 150) {
      this.setState({ agencyName });
    }
  };

  onChangeJob = (job) => {
    if (job.length <= 150) {
      this.setState({ job });
    }
  };

  onChangeAddress = (address) => {
    if (address.length <= 150) {
      this.setState({ address });
    }
  };

  onChangeCity = (city) => {
    this.setState({ city });
  };

  onChangeState = (state) => {
    this.setState({ state });
  };

  onChangeCountry = (country) => {
    this.setState({ country });
  };

  onChangeZip = (zip) => {
    this.setState({ zip });
  };

  // onChangeBirthDate = (birthDate) => {
  // 	this.setState({ birthDate });
  // }

  handlePressEditButton = () => {
    this.setState({
      disableEdit: false,
    });
  };

  handleValidatePassword = async () => {
    return new Promise((resolve, reject) => {
      if (!regex.test(this.state.password)) {
        reject(
          showMessage(
            "danger",
            "Your password must contain at least 7 characters with digits and at least one uppercase letter"
          )
        );
      }

      resolve();
    });
  };

  handlePressSaveButton = async () => {
    const { disableEdit } = this.state;
    const hasError = this.validateFields();

    if (hasError !== "") {
      console.log("hasError :>> ", hasError);
      showMessage("danger", hasError);
    } else {
      if (this.state.password !== "") {
        await this.handleValidatePassword();
      }

      const { dispatch } = this.props;
      if (this.state.loadingButton) return "";
      this.setState({
        loadingButton: true,
      });

      try {
        const { user } = this.props;
        const formData = this.parseFormData();
        console.log("formData :>> ", formData);
        // return
        const response = await updateCurrentUser(user.id, formData);
        console.log("response :>> ", response);
        const data = response.data.data;
        console.log("data :>> ", data);
        dispatch({
          type: SET_USER,
          payload: {
            email: data.email,
            id: data.id,
            user_type: data.details.type,
            first_name: data.details.first_name,
            last_name: data.details.last_name,
            name: `${data.details.first_name} ${data.details.last_name}`,
            imageUrl: data.image.url,
            thumbnail: data.image.thumbnail,
            agencyName: data.details.agency_name,
            is_invited: data.is_invited,
            isAdminSelect: data.selected_admin,
            // admin_id: data.admin_id,
            isPaidUser: data.is_premium == 1 ? true : false,
            subscriptionDetail:
              data.subscription != undefined ? data.subscription : undefined,
          },
        });
      } catch (error) {
        console.log("error :>> ", error);
        console.log(error);

        const parsedError = parseError(error);

        this.setState({
          disableEdit: false,
          loadingButton: false,
        });

        return showMessage("danger", parsedError);
      }

      this.setState(
        {
          // disableEdit: !disableEdit,
          loadingButton: false,
        },
        () => {
          if (this.isFromHome) {
            // this.props.navigation.dispatch(
            //   NavigationActions.navigate({
            //     index: 0,
            //     routeName: "Home",
            //   })
            // );
            // this.props.navigation.goBack();
            if (
              this.props.user.isAdminSelect === null &&
              this.props.user.is_invited
            ) {
              this.props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "SelectTeamAdmin",
                      params: {
                        isFromSettings: false,
                      },
                    }),
                  ],
                })
              );
            } else {
              this.props.navigation.goBack();
            }
            return;
            // this.props.navigation.goBack()
            // const resetAction = StackActions.reset({
            // 	index: 0,
            // 	actions: [NavigationActions.navigate({ routeName: 'Home' })]
            // });
            // this.props.navigation.dispatch(resetAction);
          }
          this.props.navigation.goBack();
          return;
          if (this.isFromHome) {
            this.props.navigation.goBack();
            // this.props.navigation.goBack()
            // const resetAction = StackActions.reset({
            // 	index: 0,
            // 	actions: [NavigationActions.navigate({ routeName: 'Home' })]
            // });
            // this.props.navigation.dispatch(resetAction);
          }
        }
      );
    }
  };

  parseFormData = () => {
    const {
      email,
      name,
      first_name,
      last_name,
      address,
      city,
      state,
      country,
      birthDate,
      agencyName,
      job,
      zip,
      image,
      password,
      fileName,
      thumbnail,
    } = this.state;
    const { user } = this.props;

    let data = {
      email,
      name,
      first_name,
      last_name,
      address,
      city,
      state,
      country,
      // birth: birthDate,
      agency_name: agencyName,
      profesion: job,
      zip,
      image: user.imageUrl,
      // file_name: this.state.imageData.name,
      // thumbnail: this.state.imageData.thumbnail
    };

    if (image !== null) {
      data["image"] = image;
      data["thumbnail"] = thumbnail;
      data["file_name"] = this.state.fileName;
    } else {
      data["image"] = user.imageUrl;
      data["file_name"] = this.state.imageData.name;
      data["thumbnail"] = this.state.imageData.thumbnail;
    }

    if (password !== "") {
      data["password"] = password;
    }

    return data;
  };

  validateFields = () => {
    const {
      name,
      first_name,
      last_name,
      email,
      agencyName,
      address,
      city,
      state,
      country,
      zip,
      job,
      birthDate,
      image,
      imageData,
    } = this.state;
    let errorMessage = "";
    Keyboard.dismiss();

    // if (name === '') {
    // 	errorMessage = en.require_name
    // }
    console.log("image :>> ", image);
    console.log("imageData :>> ", imageData);
    if (first_name === "") {
      errorMessage = en.require_first_name;
    } else if (last_name === "") {
      errorMessage = en.require_last_name;
    } else if (email === "") {
      errorMessage = en.require_email;
    } else if (!validateEmail(email)) {
      errorMessage = en.valid_email;
    } else if (agencyName === "") {
      errorMessage = en.require_agency;
    } else if (job === "") {
      errorMessage = en.require_job;
    } else if (country === "" || country === null) {
      errorMessage = en.require_country;
    }
    // else if (address === '') {
    // 	errorMessage = en.require_address
    // } else if (city === '') {
    // 	errorMessage = en.require_city
    // } else if (state === '' || state === null) {
    // 	errorMessage = en.require_state
    // } else if (zip === '') {
    // 	errorMessage = en.require_zip
    // 	}
    // else if (birthDate === '' || birthDate === null) {
    // 	errorMessage = en.require_birthdate
    // }
    else if (image == null && imageData == null) {
      errorMessage = en.require_image;
    }

    return errorMessage;
  };

  render() {
    const { user } = this.props;
    const {
      email,
      name,
      first_name,
      last_name,
      password,
      disableEdit,
      agencyName,
      job,
      address,
      city,
      state,
      country,
      zip,
      birthDate,
      loading,
      image,
      loadingButton,
    } = this.state;

    if (loading) {
      return <Loading />;
    }

    return (
      <Container>
        <Header
          left={
            !this.isFromHome ? (
              <BackButton onPress={() => this.handlePressBackButton()} />
            ) : null
          }
          center={<HeaderText title={en.my_info_title} />}
        />
        {this.renderRenameModal()}
        <Content style={styles.content} disableKBDismissScroll>
          <View style={styles.userMain}>
            {disableEdit && (
              <View style={styles.imageContainer}>
                {/* {
										user.imageUrl && (
											<Image
												source={{
													uri: user.thumbnail != null ?
														user.thumbnail : user.imageUrl
												}}
												defaultSource={Assets.role}
												style={styles.image}
											/>
										)
									} */}
                <Image
                  source={
                    user.imageUrl != null || user.thumbnail != null
                      ? {
                          uri:
                            user.thumbnail != null
                              ? user.thumbnail
                              : user.imageUrl,
                        }
                      : Assets.role
                  }
                  defaultSource={Assets.role}
                  style={styles.image}
                />
              </View>
            )}
            {!disableEdit && (
              <TouchableOpacity
                style={styles.imageContainer}
                onPress={() => this.handleChangeImage()}
              >
                {image === null && (
                  <Image
                    source={
                      user.imageUrl != null || user.thumbnail != null
                        ? {
                            uri:
                              user.thumbnail != null
                                ? user.thumbnail
                                : user.imageUrl,
                          }
                        : Assets.role
                    }
                    defaultSource={Assets.role}
                    style={styles.image}
                  />
                )}
                {image !== null && (
                  <Image source={image} style={styles.image} />
                )}
              </TouchableOpacity>
            )}
            <View style={styles.userInfoContainer}>
              <Text style={styles.userName}>
                {String(user.name).replace("null", "")}
              </Text>
              <Text style={styles.userCompany}>{user.agencyName}</Text>
            </View>
          </View>
          <Form style={styles.formContainer}>
            <View style={styles.firstItems}>
              {/* <InputDisable
								placeholder={en.name_placeholder}
								onChange={this.onChangeName}
								value={name}
								disable={disableEdit}
							/> */}
              <InputDisable
                placeholder={en.first_name}
                value={first_name}
                onChange={this.onFirstNameChange}
                disable={disableEdit}
              />
              <InputDisable
                placeholder={en.last_name}
                value={last_name}
                onChange={this.onLastNameChange}
                disable={disableEdit}
              />
              <InputDisable
                placeholder={en.email_placeholder}
                onChange={this.onChangeEmail}
                value={email}
                disable={disableEdit}
                keyboardType="email-address"
              />
              <InputDisable
                placeholder={en.password_placeholder}
                onChange={this.onChangePassword}
                value={password}
                disable={disableEdit}
                secureTextEntry
              />
              {/* <InputDisable
								placeholder={en.agency_name_placeholder}
								onChange={this.onChangeAgencyName}
								value={agencyName}
								disable={disableEdit}
							/>
							{console.log('birthDate :>> ', birthDate)}
							<DatepickerDisable
								placeholder={en.birthdate_placeholder}
								onChange={this.onChangeBirthDate}
								value={birthDate != null ? moment(birthDate).format('LL') : null}
								disable={disableEdit}
							/> */}
            </View>
            <View style={styles.secondItems}>
              <InputDisable
                placeholder={en.agency_name_placeholder}
                onChange={this.onChangeAgencyName}
                value={agencyName}
                disable={disableEdit}
              />
              {console.log("birthDate :>> ", birthDate)}
              {/* <DatepickerDisable
								placeholder={en.birthdate_placeholder}
								onChange={this.onChangeBirthDate}
								value={birthDate != null ? moment(birthDate).format('LL') : null}
								disable={disableEdit}
							/> */}
              <InputDisable
                placeholder={en.job_title_placeholder}
                onChange={this.onChangeJob}
                value={job}
                disable={disableEdit}
              />
              <SelectDisable
                placeholder={en.country_placeholder}
                data={countryList}
                value={String(country)}
                onChange={this.onChangeCountry}
                disable={disableEdit}
              />
              {/* <InputDisable
								placeholder={en.address_placeholder}
								onChange={this.onChangeAddress}
								value={address}
								disable={disableEdit}
							/>
							<InputDisable
								placeholder={en.city_placeholder}
								onChange={this.onChangeCity}
								value={city}
								disable={disableEdit}
							/>
							<SelectDisable
								placeholder={en.state_placeholder}
								data={statesList}
								value={state}
								onChange={this.onChangeState}
								disable={disableEdit}
							/>
							<InputDisable
								placeholder={en.zip_placeholder}
								onChange={this.onChangeZip}
								value={zip}
								disable={disableEdit}
								keyboardType='number-pad'
							/> */}
              {/* <View
								style={{
									borderWidth:1,
									width: resize(290),
									height: resize(55, 'height'),
									marginBottom: resize(20, 'height'),
								}}
							/> */}
              {/* <InputDisable
								placeholder={""}
								value={""}
								isBorder={false}
								onChange={() => { }} /> */}
            </View>
          </Form>
          <View style={styles.buttonSubmit}>
            {disableEdit && (
              <GradientButtom
                // buttonText={en.edit_button_text}
                buttonText={en.save_button_text}
                colors={gradientLight}
                containerStyle={styles.gradientButtonContainer}
                gradientStyle={styles.gradientButtonEdit}
                textStyle={styles.gradientButtonTextEdit}
                onPress={() => this.handlePressEditButton()}
              />
            )}
            {!disableEdit && (
              <GradientButtom
                buttonText={en.save_button_text}
                colors={gradient}
                containerStyle={styles.gradientButtonContainer}
                gradientStyle={styles.gradientButtonSave}
                textStyle={styles.gradientButtonTextSave}
                onPress={() => this.handlePressSaveButton()}
                loading={loadingButton}
              />
            )}
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
});

export default connect(mapStateToProps)(Profile);
