import { StyleSheet } from 'react-native';

//custom
import { width, NEXABOLD, NEXALIGHT, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
	content: {
		paddingTop: resize(33, 'height'),
	},
	userMain: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		width
	},
	image: {
		width: resize(126),
		height: resize(126, 'height'),
		borderRadius: 4,
		resizeMode: 'contain'
	},
	userInfoContainer: {
		paddingLeft: resize(39),
	},
	userName: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
		paddingVertical: resize(5, 'height'),
	},
	userCompany: {
		color: '#4d2545',
		fontFamily: NEXALIGHT,
		fontSize: resize(20),
		fontWeight: '400',
		paddingVertical: resize(5, 'height'),
		width: resize(400)
	},
	formContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		// alignItems: 'center',
		width,
		marginTop: resize(60, 'height'),
		paddingHorizontal: resize(150),
	},
	firstItems: {
		width: '50%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	secondItems: {
		width: '50%',
		// justifyContent: 'center',
		// alignItems: 'center',
	},
	buttonSubmit: {
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: resize(62, 'height'),
		marginBottom: resize(50, 'height')
	},
	gradientButtonContainer: {
		width: resize(225),
		height: resize(49, 'height')
	},
	gradientButtonSave: {
		height: resize(49, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 50,
	},
	gradientButtonTextSave: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(18),
		fontWeight: '400',
	},
	gradientButtonEdit: {
		height: resize(49, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 2,
		backgroundColor: '#ffffff',
		borderRadius: 50,
	},
	gradientButtonTextEdit: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(18),
		fontWeight: '400'
	},
	agencyNameContainer: {
		width: resize(290),
		height: resize(55, 'height'),
		flexDirection: 'row',
		marginBottom: resize(20, 'height'),
	},
	agencyNameDisable: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(18),
		fontWeight: '400',
		paddingLeft: resize(20),
		width: resize(290),
		height: resize(55, 'height'),
		backgroundColor: '#fff',
		borderBottomWidth: 1,
		borderBottomColor: '#d6d6d6',
	},
	agencyNameEnable: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(18),
		fontWeight: '400',
		paddingLeft: resize(20),
		width: resize(290),
		height: resize(55, 'height'),
		backgroundColor: '#fff',
		borderWidth: 1,
		borderColor: '#d6d6d6',
		borderRadius: 50,
	},
	formGroup: {
		width: resize(290),
		height: resize(55, 'height'),
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: resize(20, 'height')
	},
	withBorderBottom: {
		borderBottomWidth: 1,
		borderBottomColor: '#d6d6d6',
	},
	zipDisable: {
		width: resize(145),
		height: resize(55, 'height'),
	},
	zipEnable: {
		width: resize(145),
		height: resize(55, 'height'),
		borderWidth: 1,
		borderColor: '#d6d6d6',
		borderRadius: 50,
	},
	inputZip: {
		width: resize(145),
		height: resize(55, 'height'),
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(18),
		fontWeight: '400',
		paddingLeft: resize(20),
	}
});

export default styles;