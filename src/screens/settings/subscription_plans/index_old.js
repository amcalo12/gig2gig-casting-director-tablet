import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, FlatList, TouchableOpacity } from 'react-native';
import Header from '../../../components/commons/header';
import ButtonMenu from '../../../components/commons/button_menu';
import HeaderText from '../../../components/commons/header_text';
import { Container, Content } from 'native-base';
import styles from "./style";
import en from "../../../locale/en";
import { NEXABOLD, resize, NEXALIGHT } from '../../../assets/styles';
import colors from '../../../utils/colors';
import BackButton from '../../../components/commons/back_button';

export default class SubscriptionPlans extends Component {
    constructor(props) {
        super(props);
        this.state = {
            plansList: ['1', '1', '1', '1', '1']
        };
    }

    onBackPress = () => {
        const { navigation } = this.props;

        navigation.goBack();
    };

    renderItem = ({ item, index }) => {
        return (
            <View style={{ marginLeft: 30, width: 250, borderRadius: 8, backgroundColor: colors.WHITE }}>
                <View style={{
                    backgroundColor: colors.ORANGE, justifyContent: 'center',
                    alignItems: 'center', paddingVertical: 20, borderTopLeftRadius: 8, borderTopRightRadius: 8
                }}>
                    <Text style={{ fontFamily: NEXALIGHT, color: colors.WHITE }}>{'Tier 1'}</Text>
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 60 }}>
                    <Text style={{ fontFamily: NEXABOLD, color: colors.BLACK, fontSize: 40 }}>{'$160'}</Text>
                    <Text style={{ fontFamily: NEXALIGHT, color: colors.BLACK, fontSize: 20, marginTop: 8 }}>{'per month'}</Text>
                </View>

                <TouchableOpacity style={{
                    backgroundColor: colors.PRIMARY, borderWidth: 1, marginHorizontal: 30,
                    borderRadius: 20, paddingHorizontal: 30, paddingVertical: 10
                }}>
                    <Text style={{ color: colors.WHITE, fontSize: 16, fontFamily: NEXABOLD, textAlign: 'center' }}>{"SELECT"}</Text>
                </TouchableOpacity>

                <Text style={{
                    textAlign: 'center', fontFamily: NEXALIGHT, fontSize: 12,
                    marginVertical: 40, marginHorizontal: 30,
                }}>{'Store Up to 500 Performer Profiles in the Talent Database.'}</Text>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <Header
                    left={<BackButton onPress={this.onBackPress} />}
                    center={<HeaderText title={en.subscription_plan_title} />}
                />
                <ImageBackground source={require('../../../assets/images/commons/bg.png')} style={styles.container}
                    defaultSource={require('../../../assets/images/commons/bg.png')}>
                    <Content style={styles.content}>
                        <FlatList
                            style={{ flex: 1, paddingBottom: 20 }}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            data={this.state.plansList}
                            extraData={this.state}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => item + index}
                        />

                        <Text style={{
                            fontFamily: NEXALIGHT, color: colors.WHITE, textAlign: 'center',
                            marginTop: 20
                        }}>{'All subscriptions are billed monthly. If your Talent Database exceeds the number of\nperformers assigned to your account tier, your tier will automatically be upgraded.'}</Text>
                    </Content>
                </ImageBackground>

            </Container>
        );
    }
}
