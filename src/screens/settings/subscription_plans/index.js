import React, { Component } from 'react';
import { View, Text, ImageBackground, Linking, Image, FlatList, TouchableOpacity, Button } from 'react-native';
import Header from '../../../components/commons/header';
import ButtonMenu from '../../../components/commons/button_menu';
import HeaderText from '../../../components/commons/header_text';
import { Container, Content, Icon } from 'native-base';
import styles from "./style";
import en from "../../../locale/en";
import { NEXABOLD, resize, NEXALIGHT } from '../../../assets/styles';
import colors from '../../../utils/colors';
import BackButton from '../../../components/commons/back_button';
import { connect } from 'react-redux';
import { getAllPlanList } from "../../../api/subscription";
import ProgressLoader from '../../../components/commons/progress_loading';

class SubscriptionPlans extends Component {
    constructor(props) {
        super(props);
        this.currentRightIndex = 0
        this.currentLeftIndex = 0
        this.state = {
            isLoading: false,
            plansList: undefined,
            currentIndex: 0
        };
    }

    onBackPress = () => {
        const { navigation } = this.props;
        navigation.goBack();
    };

    componentDidMount() {
        this.getAllPlans()
    }

    //GET ALL AVAILABLE PLANS
    getAllPlans = async () => {
        this.setState({ isLoading: true })
        try {
            const response = await getAllPlanList();
            console.log('response :>> ', response.data.data.data);
            let list = response.data.data;
            // let list = response.data.data.sort((a, b) => (a.amount - b.amount))
            // list.splice(list.length - 1, 1)
            console.log('list :>> ', list);
            this.setState({ plansList: list, isLoading: false });
        } catch (error) {
            this.setState({ isLoading: false })
            console.log(" Plan Error =====>", error);
        }
    }

    renderItem = ({ item, index }) => {
        return (
            <View style={{ marginLeft: 30, width: 250, borderRadius: 8, backgroundColor: colors.WHITE }}>
                <View style={{
                    backgroundColor: colors.ORANGE, justifyContent: 'center',
                    alignItems: 'center', paddingVertical: 20, borderTopLeftRadius: 8, borderTopRightRadius: 8
                }}>
                    {/* <Text style={{ fontFamily: NEXALIGHT, color: colors.WHITE }}>
                        {item.amount != null ? "Up to " + Number(item.allowed_performers).toLocaleString() + " Performers" :
                            "Up to " + Number(item.allowed_performers).toLocaleString() + "+ Performers"}</Text> */}
                    <Text style={{ fontFamily: NEXALIGHT, color: colors.WHITE }}>
                        {item.header}</Text>
                </View>

                {item.amount != null ? <View style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 60 }}>
                    <Text style={{ fontFamily: NEXABOLD, color: colors.BLACK, fontSize: 40 }}>{'$' + (item.amount)}</Text>
                    <Text style={{ fontFamily: NEXALIGHT, color: colors.BLACK, fontSize: 20, marginTop: 8 }}>{item.type == 'daily' ? 'per day' : 'per month'}</Text>
                </View> : null}

                {item.amount != null ? <TouchableOpacity style={{
                    backgroundColor: colors.PRIMARY, borderWidth: 1, marginHorizontal: 30,
                    borderRadius: 20, paddingHorizontal: 30, paddingVertical: 10
                }} onPress={() => Linking.openURL('https://casting.gig2gig.com/')}>
                    <Text style={{ color: colors.WHITE, fontSize: 16, fontFamily: NEXABOLD, textAlign: 'center' }}>{"SELECT"}</Text>
                </TouchableOpacity> : null}

                {item.amount == null ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        {/* <Text style={{ color: colors.PRIMARY, fontSize: 16, fontFamily: NEXABOLD, textAlign: 'center' }}>{"Contact Us"}</Text> */}
                        <TouchableOpacity style={{
                            backgroundColor: colors.PRIMARY, marginHorizontal: 30,
                            borderRadius: 20, paddingHorizontal: 30, paddingVertical: 10
                        }} onPress={() => Linking.openURL('mailto:accounts@gig2gig.com')}>
                            <Text style={{ color: colors.WHITE, fontSize: 16, fontFamily: NEXABOLD, textAlign: 'center' }}>{"Contact Us"}</Text>
                        </TouchableOpacity>
                    </View>
                    : null
                }

                <Text style={{
                    textAlign: 'center', fontFamily: NEXALIGHT, fontSize: 12,
                    marginVertical: 40, marginHorizontal: 30,
                }}>{item.description}</Text>
            </View >
        )
    }
    onViewableItemsChanged = ({ viewableItems, changed }) => {
        // console.log("Visible items are", viewableItems);
        // console.log("Changed in this iteration", changed);
        if (viewableItems.length >= 3) {
            this.currentRightIndex = viewableItems[2].index
            // console.log('this.currentRightIndex :>> ', this.currentRightIndex);
            this.currentLeftIndex = viewableItems[0].index
            // console.log('this.currentLeftIndex :>> ', this.currentLeftIndex);
        }
    }
    render() {
        return (
            <Container>
                <ProgressLoader isLoading={this.state.isLoading} />
                <Header
                    left={<BackButton onPress={this.onBackPress} />}
                    center={<HeaderText title={en.subscription_plan_title} />}
                />
                <ImageBackground source={require('../../../assets/images/commons/bg.png')} style={styles.container}
                    defaultSource={require('../../../assets/images/commons/bg.png')}>
                    {this.state.plansList != undefined && this.state.plansList.length > 0 ? <Content style={styles.content}>
                        <FlatList
                            ref={ref => (this.flatList = ref)}
                            style={{ flex: 1, paddingBottom: 20, width: 840, alignSelf: "center" }}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            pagingEnabled={true}
                            snapToInterval={3}
                            data={this.state.plansList}
                            extraData={this.state}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => item + index}
                            // initialScrollIndex={0}
                            onViewableItemsChanged={this.onViewableItemsChanged}
                            viewabilityConfig={{
                                itemVisiblePercentThreshold: 50
                            }}
                        />

                        <View style={{ marginTop: 20, flexDirection: "row", alignSelf: 'center', alignItems: 'center', justifyContent: 'space-between' }}>
                            <TouchableOpacity onPress={() => {
                                console.log('this.currentLeftIndex :>> ', this.currentLeftIndex);
                                if (this.currentLeftIndex > 2) {
                                    this.flatList.scrollToIndex({ animated: true, index: this.currentLeftIndex - 3 })
                                } else if (this.currentLeftIndex == 2) {
                                    this.flatList.scrollToIndex({ animated: true, index: this.currentLeftIndex - 2 })
                                } else if (this.currentLeftIndex == 1) {
                                    this.flatList.scrollToIndex({ animated: true, index: this.currentLeftIndex - 1 })
                                }
                            }}>
                                <Icon name='left' type='AntDesign'
                                    style={{ color: "#fff", alignSelf: 'center', }} />
                            </TouchableOpacity>
                            <Text style={{
                                fontFamily: NEXALIGHT, color: colors.WHITE, textAlign: 'center'
                            }}>{'Slide For More Options'}</Text>
                            <TouchableOpacity onPress={() => {
                                if (this.currentRightIndex <= 5) {
                                    this.flatList.scrollToIndex({ animated: true, index: this.currentRightIndex + 1 })
                                }
                            }}>
                                <Icon name='right' type='AntDesign'
                                    style={{ color: "#fff", alignSelf: 'center', }} />
                            </TouchableOpacity>
                        </View>

                        <Text style={{
                            fontFamily: NEXALIGHT, color: colors.WHITE, textAlign: 'center',
                            marginTop: 20
                        }}>{'All subscriptions are billed monthly. If your Talent Database exceeds the number of\nperformers assigned to your account tier, your tier will automatically be upgraded.'}</Text>
                    </Content> : null}
                </ImageBackground>

            </Container>
        );
    }
}

const mapStateToProps = state => {
    console.log(" REDUX DATA CREATE GROUP=========>", state);

    return {
        user: state.user.data,
        feedback: state.user.instantFeedback
    };
};

export default connect(mapStateToProps)(SubscriptionPlans)