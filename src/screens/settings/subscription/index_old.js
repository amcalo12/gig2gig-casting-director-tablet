import React, { Component } from 'react';
import { View, Text, Image, FlatList, Linking, TouchableOpacity } from 'react-native';
import Header from '../../../components/commons/header';
import ButtonMenu from '../../../components/commons/button_menu';
import HeaderText from '../../../components/commons/header_text';
import { Container, Content } from 'native-base';
import styles from "./style";
import en from "../../../locale/en";
import { NEXABOLD, resize, NEXALIGHT } from '../../../assets/styles';
import GradientButton from '../../../components/commons/gradient_button';
import colors from '../../../utils/colors';
import roleIcon from '../../../assets/icons/auditions/role-icon.png';
import BackButton from '../../../components/commons/back_button';
import { connect } from 'react-redux';

class Subscription extends Component {
    constructor(props) {
        super(props);
        this.isPaid = this.props.user.isPaidUser
        this.state = {
            userList: ['1', '1', '1', '1', '1']
        };
    }

    onBackPress = () => {
        const { navigation } = this.props;

        navigation.goBack();
    };

    renderItem = ({ item, index }) => {
        return (
            <View style={styles.itemContainer}>
                <View style={styles.nameContainer}>
                    <Image source={roleIcon} style={styles.userImage} defaultSource={roleIcon} resizeMode={'contain'} />
                    <View>
                        <Text style={styles.nameText}>{"John Smith"}</Text>
                        <Text style={styles.emailText}>{"name@mail.com"}</Text>
                    </View>
                </View>
                <Text style={styles.dateText}>{"May 7, 2020"}</Text>
                <View style={{ flex: 1.5 }} />
                <TouchableOpacity style={styles.inviteContainer}>
                    <Text style={styles.inviteText}>{"Deactivate"}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    onInviteUser = () => {
        this.props.navigation.navigate('InviteUsers')
    }

    render() {
        return (
            <Container>
                <Header
                    left={<BackButton onPress={this.onBackPress} />}
                    center={<HeaderText title={en.subscription_title} />}
                />
                {this.isPaid ? <Content style={styles.content}>
                    <View style={styles.rowContainer}>
                        <Text style={styles.title}>{en.subscription_title}</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SubscriptionPlans')}>
                            <Text style={styles.title}>{"View Plans"}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.tierContainer}>
                        <Text style={styles.currentTierText}>{'Current Tier: Tier 1'}</Text>
                        <View style={styles.rowContainer}>
                            <Text style={styles.cardText}>{'Payment: Master Card 0057'}</Text>
                            <Text style={styles.changeText}>{'Change'}</Text>
                        </View>
                    </View>

                    <View style={styles.userTitleContainer}>
                        <Text style={styles.title}>{'Users'}</Text>
                        <TouchableOpacity style={styles.inviteContainer} onPress={this.onInviteUser}>
                            <Text style={styles.inviteText}>{"Invite Users"}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.tableHeaderContainer}>
                        <Text style={styles.tableTitle}>{"Name"}</Text>
                        <Text style={styles.tableTitle}>{"Joined"}</Text>
                        <View style={{ flex: 1 }} />
                    </View>

                    <FlatList
                        style={{ flex: 1, paddingBottom: 20 }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.userList}
                        extraData={this.state}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => item + index}
                    />
                </Content> :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontFamily: NEXABOLD }} onPress={() => Linking.openURL('https://www.google.com/')}>{"You don't have any subscription."}</Text>
                        <TouchableOpacity style={[styles.inviteContainer, { marginTop: 10 }]} onPress={this.onInviteUser}>
                            <Text style={[styles.inviteText, { fontSize: 18 }]}>{"SUBSCRIBE"}</Text>
                        </TouchableOpacity>
                    </View>
                }
            </Container>
        );
    }
}

const mapStateToProps = state => {
    console.log("STORE REDUX=====>", state)
    return ({
        user: state.user.data
    });
}
export default connect(mapStateToProps)(Subscription);