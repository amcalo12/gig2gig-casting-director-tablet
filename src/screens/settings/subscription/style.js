import { StyleSheet, Platform } from 'react-native';

//custom
import { width, NEXABOLD, NEXALIGHT, resize } from '../../../assets/styles';
import colors from '../../../utils/colors';

const styles = StyleSheet.create({
    content: {
        paddingVertical: resize(33, 'height'),
        marginHorizontal: 20,
        marginTop: 20,
        marginBottom: 40
    },
    rowContainer: { flexDirection: 'row', justifyContent: 'space-between' },
    tierContainer: { flexDirection: "row", marginTop: 10, justifyContent: "space-between" },
    currentTierText: { fontFamily: NEXALIGHT, fontSize: resize(16) },
    cardText: { fontFamily: NEXALIGHT, fontSize: resize(16) },
    changeText: { fontFamily: NEXABOLD, fontSize: resize(16), marginLeft: 20 },
    title: { fontFamily: NEXABOLD, fontSize: resize(16) },
    userTitleContainer: {
        flexDirection: 'row', justifyContent: 'space-between',
        alignItems: 'center', marginTop: 40
    },
    inviteContainer: {
        // width: "15%",
        backgroundColor: colors.PRIMARY,
        borderRadius: 20, paddingHorizontal: 30, paddingVertical: 5
    },
    inviteText: { color: colors.WHITE, textAlign: 'center' },
    tableHeaderContainer: {
        flexDirection: 'row', borderRadius: 4, paddingHorizontal: 60,
        justifyContent: 'space-between', backgroundColor: '#F0F0F0',
        paddingTop: 20, paddingBottom: 10, marginTop: 20
    },
    tableTitle: { flex: 1, fontFamily: NEXALIGHT },
    itemContainer: {
        flexDirection: 'row', borderRadius: 4, paddingHorizontal: 40,
        justifyContent: 'space-between', alignItems: 'center',
        paddingTop: 20, paddingBottom: 10, marginTop: 20
    },
    nameContainer: { flex: 1.5, flexDirection: 'row', alignItems: "center" },
    userImage: { width: 30, height: 30, marginRight: 15, borderRadius: 15, borderWidth: 0 },
    nameText: { fontFamily: NEXALIGHT, fontSize: 18 },
    emailText: { fontFamily: NEXALIGHT, fontSize: 12, marginTop: 5 },
    dateText: { flex: 1, fontFamily: NEXALIGHT, textAlign: 'center' }
});

export default styles;