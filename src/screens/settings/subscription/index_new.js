import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  Linking,
  TouchableOpacity,
} from "react-native";
import Header from "../../../components/commons/header";
import ButtonMenu from "../../../components/commons/button_menu";
import HeaderText from "../../../components/commons/header_text";
import { Container, Content } from "native-base";
import styles from "./style";
import en from "../../../locale/en";
import { NEXABOLD, resize, NEXALIGHT } from "../../../assets/styles";
import GradientButton from "../../../components/commons/gradient_button";
import colors from "../../../utils/colors";
import roleIcon from "../../../assets/icons/auditions/role-icon.png";
import BackButton from "../../../components/commons/back_button";
import { connect } from "react-redux";
import {
  getSubscriptionDetail,
  changeStatus,
  cancelSubscription,
  resumeSubscription,
  resendEmail,
} from "../../../api/subscription";
import ProgressLoader from "../../../components/commons/progress_loading";
import moment from "moment";
import {
  showConfirmationDialog,
  CANCEL_SUBSCRIPTION,
  RESUME_SUBSCRIPTION,
  showConfirmationDialogYesNoClick,
} from "../../../utils/constant";
import showMessage from "../../../utils/show_message";
import { fetchCurrentUser } from "../../../api/settings";
import { SET_USER } from "../../../actions/user";

class Subscription extends Component {
  constructor(props) {
    super(props);
    this.isPaid = this.props.user.isPaidUser;
    this.state = {
      isLoading: false,
      isDetailGet: false,
      userList: [],
      subscriptionDetail: undefined,
    };
  }

  onBackPress = () => {
    const { navigation } = this.props;

    navigation.goBack();
  };

  componentDidMount() {
    // if (this.isPaid) {
    this.getSubscriptionDetail();
    this.fetchUser();
    // }
  }

  //GET SUBSCRIPTION DETAILS
  getSubscriptionDetail = async () => {
    this.setState({ isLoading: true });
    try {
      const response = await getSubscriptionDetail();
      this.setState({
        userList: response.data.data.invitedUsers,
        subscriptionDetail: response.data.data.subscription,
        isLoading: false,
        isDetailGet: true,
      });
    } catch (error) {
      console.log("error :>> ", error.response);
      this.setState({
        isLoading: false,
        userList: [],
        subscriptionDetail: undefined,
        isDetailGet: true,
      });
    }
  };

  onItemClick = async (item, index) => {
    showConfirmationDialog(
      "Are you sure you want to " +
        (item.is_active ? "Deactivate" : "Activate") +
        " this user?",
      async () => {
        this.setState({ isLoading: true });
        try {
          let params = {
            id: item.id,
            status: item.is_active == 1 ? 0 : 1,
          };
          const response = await changeStatus(params);
          console.log("response :>> ", response);
          this.getSubscriptionDetail();
        } catch (error) {
          console.log("error :>> ", error.response);
          this.setState({ isLoading: false });
        }
      }
    );
  };

  onResendClick = async (item, index) => {
    this.setState({ isLoading: true });
    try {
      const response = await resendEmail(item.id);
      console.log("response :>> ", response);
      showMessage("success", response.data.message);
      this.setState({ isLoading: false });
    } catch (error) {
      console.log("error :>> ", error.response);
      showMessage("danger", error.response.data.message);
      this.setState({ isLoading: false });
    }
  };

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.itemContainer}>
        <View style={styles.nameContainer}>
          <Image
            source={
              item.image != null ? { uri: item.image.thumbnail } : roleIcon
            }
            style={styles.userImage}
            defaultSource={roleIcon}
            resizeMode={"contain"}
          />
          <View>
            {item.details.first_name != "" ? (
              <Text style={styles.nameText}>
                {(item.details.first_name != null
                  ? item.details.first_name
                  : "") +
                  " " +
                  (item.details.last_name != null
                    ? item.details.last_name
                    : "")}
              </Text>
            ) : null}
            <Text style={styles.emailText}>{item.email}</Text>
          </View>
        </View>
        <Text style={styles.dateText}>
          {moment(item.details.created_at).format("MMM DD, YYYY") ||
            "May 7, 2020"}
        </Text>
        <View style={{ flex: 1 }} />
        <View
          style={{ flex: 2, flexDirection: "row", justifyContent: "flex-end" }}
        >
          {item.is_profile_completed == 0 ? (
            <TouchableOpacity
              style={[styles.inviteContainer, { marginRight: 10 }]}
              onPress={() => this.onResendClick(item, index)}
            >
              <Text style={[styles.inviteText, { fontSize: 12 }]}>
                {"Resend Email"}
              </Text>
            </TouchableOpacity>
          ) : (
            <View />
          )}
          <TouchableOpacity
            style={styles.inviteContainer}
            onPress={() => this.onItemClick(item, index)}
          >
            <Text style={styles.inviteText}>
              {item.is_active == 1 ? "Deactivate" : "Activate"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  onInviteUser = () => {
    this.props.navigation.navigate("InviteUsers", { callback: this.callback });
  };

  callback = () => {
    this.getSubscriptionDetail();
  };

  onCancelSubscription = async () => {
    console.log(
      "this.state.subscriptionDetail :>> ",
      this.state.subscriptionDetail
    );
    if (
      this.state.subscriptionDetail &&
      this.state.subscriptionDetail.grace_period == 1
    ) {
      showMessage(
        "danger",
        "You have already request for cancel subscription."
      );
      return;
    }
    showConfirmationDialog(CANCEL_SUBSCRIPTION, async () => {
      this.setState({ isLoading: true });
      try {
        const response = await cancelSubscription();
        console.log("response :>> ", response);
        showMessage("success", response.data.message);
        this.getSubscriptionDetail();
        // this.setState({ isLoading: false })
      } catch (error) {
        console.log("error :>> ", error.response);
        this.setState({ isLoading: false });
      }
    });
  };

  onResumeSubscription = async () => {
    console.log(
      "this.state.subscriptionDetail :>> ",
      this.state.subscriptionDetail
    );
    showConfirmationDialog(RESUME_SUBSCRIPTION, async () => {
      this.setState({ isLoading: true });
      try {
        const response = await resumeSubscription();
        console.log("response :>> ", response);
        showMessage("success", response.data.message);
        this.getSubscriptionDetail();
        // this.setState({ isLoading: false })
      } catch (error) {
        console.log("error :>> ", error.response);
        this.setState({ isLoading: false });
      }
    });
  };

  fetchUser = async () => {
    const { user, dispatch } = this.props;

    this.setState({ loading: true });

    try {
      const response = await fetchCurrentUser(user.id);
      const data = response.data.data;
      // const details = data.details
      console.log("response 2222:>> ", response);
      // const data = response.data.data;
      console.log("data 3333:>> ", data);

      dispatch({
        type: SET_USER,
        payload: {
          email: data.email,
          id: data.id,
          user_type: data.details.type,
          first_name: data.details.first_name,
          last_name: data.details.last_name,
          name: `${data.details.first_name} ${data.details.last_name}`,
          imageUrl: data.image.url,
          thumbnail: data.image.thumbnail,
          agencyName: data.details.agency_name,
          is_invited: data.is_invited,
          // admin_id: data.admin_id,
          isPaidUser: data.is_premium == 1 ? true : false,
          subscriptionDetail:
            data.subscription != undefined ? data.subscription : undefined,
        },
      });
    } catch (error) {
      console.log(error);
    }

    this.setState({ loading: false });
  };

  onPress = () => {
    if (this.state.subscriptionDetail.grace_period == 0) {
      this.onCancelSubscription();
    } else if (this.state.subscriptionDetail.grace_period == 1) {
      this.onResumeSubscription();
    }
  };

  render() {
    return (
      <Container>
        <ProgressLoader isLoading={this.state.isLoading} />
        <Header
          left={<BackButton onPress={this.onBackPress} />}
          center={<HeaderText title={en.subscription_title} />}
        />
        {console.log("this.isPaid :>> ", this.isPaid)}
        {console.log("this.state.userList :>> ", this.state.userList)}

        {console.log(
          "this.state.subscriptionDetail :>> ",
          this.state.subscriptionDetail
        )}
        {this.state.isDetailGet &&
        this.state.userList != undefined &&
        this.state.subscriptionDetail != undefined ? (
          <Content style={styles.content} showsVerticalScrollIndicator={false}>
            <View style={styles.rowContainer}>
              <Text style={styles.title}>{en.subscription_title}</Text>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("SubscriptionPlans")
                }
              >
                <Text style={styles.title}>{"View Plans"}</Text>
              </TouchableOpacity>
            </View>

            {this.state.subscriptionDetail != undefined ? (
              <View style={styles.tierContainer}>
                <Text style={styles.currentTierText}>
                  {"Current Tier: " + this.state.subscriptionDetail.name}
                </Text>
                <View style={styles.rowContainer}>
                  {this.state.subscriptionDetail.card_brand != null ||
                  this.state.subscriptionDetail.card_last_four != null ? (
                    <Text style={styles.cardText}>
                      {"Payment: " +
                        (this.state.subscriptionDetail.card_brand != null
                          ? this.state.subscriptionDetail.card_brand
                          : "") +
                        " " +
                        (this.state.subscriptionDetail.card_last_four != null
                          ? this.state.subscriptionDetail.card_last_four
                          : "")}
                    </Text>
                  ) : null}
                  {/* <Text style={styles.changeText}>{'Change'}</Text> */}
                </View>
              </View>
            ) : null}

            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              {this.state.subscriptionDetail.ends_at != null ? (
                <Text style={[styles.currentTierText, { marginTop: 20 }]}>
                  {(this.state.subscriptionDetail.grace_period == 0
                    ? "Renewal Date: "
                    : "Expiration Date: ") +
                    moment(this.state.subscriptionDetail.ends_at).format(
                      "MM/DD/YYYY"
                    )}
                </Text>
              ) : (
                <View />
              )}
              {/* {this.state.subscriptionDetail != undefined ?
                                <TouchableOpacity style={{ alignSelf: 'flex-end', marginTop: 20 }} onPress={this.onPress}>
                                    <Text style={styles.title}>{this.state.subscriptionDetail.grace_period == 0 ?
                                        "Cancel Subscription" :
                                        "Resume Subscription"}</Text>
                                </TouchableOpacity> : null} */}
            </View>
            <View style={styles.userTitleContainer}>
              <Text style={styles.title}>{"Users"}</Text>
              <TouchableOpacity
                style={styles.inviteContainer}
                onPress={this.onInviteUser}
              >
                <Text style={styles.inviteText}>{"Invite Users"}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.tableHeaderContainer}>
              <Text style={styles.tableTitle}>{"Name"}</Text>
              <Text style={styles.tableTitle}>{"Joined"}</Text>
              <View style={{ flex: 1 }} />
            </View>

            {this.state.userList.length > 0 ? (
              <FlatList
                style={{ flex: 1, paddingBottom: 20 }}
                showsVerticalScrollIndicator={false}
                data={this.state.userList}
                extraData={this.state}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => item + index}
              />
            ) : (
              <View
                style={{
                  height: "200%",
                  justifyContent: "center",
                  alignItems: "center",
                  borderWidth: 0,
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    textAlign: "center",
                    fontFamily: NEXALIGHT,
                  }}
                >
                  {"You haven't invited any users yet"}
                </Text>
              </View>
            )}
          </Content>
        ) : this.state.isDetailGet ? (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Text
              style={{ fontSize: 18, fontFamily: NEXABOLD }}
              // onPress={() => Linking.openURL('https://casting.gig2gig.com/')}
            >
              {"You don't have any subscription."}
            </Text>
            <TouchableOpacity
              style={[styles.inviteContainer, { marginTop: 10 }]}
              onPress={() => Linking.openURL("https://casting.gig2gig.com/")}
            >
              <Text style={[styles.inviteText, { fontSize: 18 }]}>
                {"SUBSCRIBE"}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user.data,
  };
};
export default connect(mapStateToProps)(Subscription);
