import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import { Container, Content } from 'native-base';
import { connect } from 'react-redux';

//custom
import styles from './style';
import Header from '../../../components/commons/header';
import BackButton from '../../../components/commons/back_button';
import HeaderText from '../../../components/commons/header_text';
import Loading from '../../../components/commons/loading';

//locale
import en from '../../../locale/en';

//api
import { getSettingsInfo } from '../../../api/settings';

class TermsOfUse extends Component {
  state = {
    loading: false,
    contentHtml: ''
  };

  componentDidMount() {
    this.getInfo();
  }

  getInfo = async () => {
    this.setState({ loading: true });

    try {
      const response = await getSettingsInfo();

      this.setState({
        contentHtml: response.data.data[0].term_of_use
      });
    } catch (error) {
      console.log(error);
    }

    this.setState({ loading: false });
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;

    navigation.goBack(null);
  };

  render() {
    const { loading, contentHtml } = this.state;

    if (loading) {
      return <Loading />;
    }

    return (
      <Container>
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
          center={<HeaderText title={en.terms_of_use_title} />}
        />
        <Content style={styles.content} bounces={false}>
          <WebView source={{ html: contentHtml }} style={styles.webView} />
        </Content>
      </Container>
    );
  }
}

export default connect(null)(TermsOfUse);
