import { StyleSheet } from 'react-native';
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	container2: {
		marginTop: resize(50, 'h'),
		width: '40%',
		alignSelf: 'center',
	}
})
export default styles;
