import React, { Component } from 'react';
import { View, CameraRoll, Platform, Text } from 'react-native';
import { Container, Toast, Content } from 'native-base';
import styles from './styles';

import RNFS from "react-native-fs";

//Custom
import LightBar from '../../../components/commons/light_bar';
import BackButton from '../../../components/commons/back_button';
import Header from '../../../components/commons/header';
import HeaderText from '../../../components/commons/header_text';
import en from '../../../locale/en';
import { Spinner } from 'native-base';
import Label from '../../../components/commons/pushNotificationLabel';
import { getPushConfig, setPushConfig } from '../../../api/settings';

class QrCode extends Component {
	state = {
		listNotification: [],
		isLoading: true
	}

	componentDidMount = () => {
		this.getNotification();
	}

	handlePressBackButton = () => {
		const { navigation } = this.props;
		navigation.goBack();
	}

	updateNotification = async (id) => {
		const { listNotification } = this.state
		const index = listNotification.findIndex(res => res.id === id)
		const status = listNotification[index].value === 1 ? 0 : 1;
		const statusSave = listNotification[index].value === 1 ? false : true;
		listNotification[index] = { ...listNotification[index], value: status }
		this.setState(listNotification)
		try {
			const res = await setPushConfig(id, { value: statusSave })
			console.log(res)
		} catch (error) {
			console.log(error);
		}
	}
	getNotification = async () => {
		this.setState({ isLoading: true })
		try {
			const res = await getPushConfig();
			const listNotification = res.data.data;
			this.setState({ listNotification, isLoading: false });
		} catch (error) {
			this.setState({ isLoading: false });
			showMsj('something went wrong', true);
		}
	}

	renderNotifications = () => {
		return this.state.listNotification.map((res, i) => {
			const a = 'fdasfdsafdsa'
			return <Label key={i} text={res.setting} value={res.value !== 1 ? false : true} onPress={() => this.updateNotification(res.id)} />
		})
	}

	changeValue = async (id) => {
		await pushNotifiactionViewModel.updateNotification(this, id)
	};

	render() {
		let { isLoading } = this.state;
		return (
			<Container style={styles.container}>
				<LightBar />
				<Header
					center={
						<HeaderText
							title={en.push_title}
						/>
					}
					left={<BackButton onPress={() => this.handlePressBackButton()} />}
				/>
				<Content>
					{
						isLoading ?
							<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
								<Spinner
									color={'#4d2545'}
								/>
							</View>
							:
							<View style={styles.container2}>
								{this.renderNotifications()}
							</View>
					}
				</Content>
			</Container>
		)
	}
}
export default QrCode;
