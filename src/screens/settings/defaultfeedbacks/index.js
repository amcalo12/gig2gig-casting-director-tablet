import React, { Component } from "react";
import { View, Text, Image, Linking } from "react-native";
import { Container, Content } from "native-base";
import { connect } from "react-redux";

//custom
// import Header from "../../components/commons/header";
import Header from "../../../components/commons/header";
import ButtonMenu from "../../../components/commons/button_menu";
import HeaderText from "../../../components/commons/header_text";
import GradientButton from "../../../components/commons/gradient_button";
import ItemMenu from "../../../components/settings/item_menu";
import AlertIOS from "../../../components/commons/alert_ios";
import settings from "../../../../settings";

//actions
import { DESTROY_SESSION } from "../../../actions/auth";

//locale
import en from "../../../locale/en";
import requestDeviceToken from "../../../utils/device_token";
import { DEVICE_ID } from "../../../utils/constant";
import { setDeviceToken } from "../../../api/login";
import styles from "./styles";
import BackButton from "../../../components/commons/back_button";
import { getfeedback } from "../../../api/auditions";
import { DEFAULT_INSTANT_FEEDBACK } from "../../../actions/user";

class DefaultMessageSelection extends Component {
    state = {
    };

    componentDidMount() {
        this.getInstantFeedback()
    }

    //GET INSTANT FEEDBACK
    getInstantFeedback = async () => {
        const { dispatch } = this.props;
        const { user } = this.props;
        console.log("<===== user id =====>", user.id)

        this.setState({ loading: true });
        try {
            const response = await getfeedback(user.id);

            this.setState({
                instantFeedback: response.data.data
            });
            console.log("SUCCESS Get Instant Feedback =====>", response)
            let defaultMessage = {}
            defaultMessage['reject'] = response.data.data.comment
            defaultMessage['accept'] = response.data.data.positiveComment
            dispatch({
                type: DEFAULT_INSTANT_FEEDBACK,
                payload: defaultMessage
            });
        } catch (error) {
            console.log("ERROR Get Instant Feedback =====>", error);
        }

        this.setState({ loading: false });
    }


    handlePressOpenDrawer = () => {
        const { navigation } = this.props;

        navigation.goBack();
    };

    handlePressNavigateTo = (view, data) => {
        const { navigation } = this.props;

        navigation.navigate(view, data);
    };

    render() {
        const { signOutModal } = this.state;
        const { user } = this.props;

        return (
            <Container>
                <Header
                    left={<BackButton onPress={() => this.handlePressOpenDrawer()} />}
                    center={<HeaderText title={en.settings_title} />}
                />
                <Content style={styles.content}>
                    <View style={styles.menuContainer}>
                        <ItemMenu
                            title={en.green_check}
                            onPress={() => this.handlePressNavigateTo("InstanceFeedback", { isPositive: true })}
                        />
                        <ItemMenu
                            title={en.red_x}
                            onPress={() => this.handlePressNavigateTo("InstanceFeedback", { isPositive: false })}
                        />
                    </View>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.data
});

export default connect(mapStateToProps)(DefaultMessageSelection);
