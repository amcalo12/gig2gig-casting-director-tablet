import { StyleSheet } from 'react-native';

//custom
import { width, height, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  content: {
    paddingTop: resize(33, 'height'),
  },
  webView: {
    width,
    height
  }
});

export default styles;