import { StyleSheet } from 'react-native';
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	center: {
		width: '100%',
		height: '55%',
		justifyContent: 'flex-end',
		alignItems: 'center',
		paddingVertical: resize(25, 'height')
	},
	buttoms: {
		width: '100%',
		height: '45%',
		justifyContent: 'flex-start',
		alignItems: 'center',
		marginTop: resize(25, 'height'),
	}
})
export default styles;
