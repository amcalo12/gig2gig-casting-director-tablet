import React, { Component } from "react";
import { View, CameraRoll, Platform, Text, Image } from "react-native";
import { Container, Toast } from "native-base";
import styles from "./styles";

import QRCode from "react-native-qrcode-svg";
import RNFS from "react-native-fs";

//Custom
import LightBar from "../../../components/commons/light_bar";
import BackButton from "../../../components/commons/back_button";
import Header from "../../../components/commons/header";
import { resize } from "../../../assets/styles";
import GradientButton from "../../../components/commons/gradient_button";
import en from "../../../locale/en";
import { Assets } from "../../../assets";

class QrCode extends Component {
  state = {
    isLoading: false
  };

  handlePressBackButton = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  saveQr = () => {
    this.setState({ isLoading: true });
    let source =
      Platform.OS === "android"
        ? RNFS.CachesDirectoryPath
        : RNFS.DocumentDirectoryPath;
    this.svg.toDataURL(data => {
      RNFS.writeFile(source + "/qr_code.png", data, "base64")
        .then(success => {
          return CameraRoll.saveToCameraRoll(source + "/qr_code.png", "photo");
        })
        .then(() => {
          Toast.show({
            text: "QR code saved in gallerry",
            buttonText: "Okay"
          });
          this.setState({ isLoading: false });
        });
    });
  };

  render() {
    let { isLoading } = this.state;
    return (
      <Container style={styles.container}>
        <LightBar />
        <Header
          left={<BackButton onPress={() => this.handlePressBackButton()} />}
        />
        <View style={styles.center}>
          {/* <QRCode
						getRef={(c) => (this.svg = c)}
						value="Just some string value"
						size={resize(250)}
					/> */}
          <Image
            source={Assets.qr_new}
            style={{ width: resize(250), height: resize(250) }}
            resizeMode={"contain"}
          />
        </View>
        {/* <View style={styles.buttoms}>
          <GradientButton
            loading={isLoading}
            buttonText={en.save_button_text}
            onPress={() => this.saveQr()}
          />
        </View> */}
      </Container>
    );
  }
}
export default QrCode;
