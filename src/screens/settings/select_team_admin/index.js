import React, { Component } from "react";
import { Text, View, FlatList, TouchableOpacity, Button } from "react-native";
import styles from "./styles";
import LightBar from "../../../components/commons/light_bar";
import Header from "../../../components/commons/header";
import HeaderText from "../../../components/commons/header_text";
import BackgroundWrapper from "../../../components/commons/background_wrapper";
import en from "../../../locale/en";
import Loading from "../../../components/commons/loading";
import BackButton from "../../../components/commons/back_button";
import GradientButton from "../../../components/commons/gradient_button";
import { Container, Toast, Content } from "native-base";
import { INIT_SESSION } from "../../../actions/auth";

//api
import { getAllTeamAdmins, updateTeamAdmin } from "../../../api/auditions";
import { resize } from "../../../assets/styles";
import { connect } from "react-redux";
import { SET_USER } from "../../../actions/user";
import parseError from "../../../utils/parse_error";

class SelectTeamAdmin extends Component {
  state = {
    loading: false,
    teamAdminList: [],
    isFetch: false,
    updateAdminLoading: false,
    selectedItem: null,
  };
  componentDidMount() {
    this.getAdminList();
  }
  getAdminList = async () => {
    this.setState({ loading: true });
    try {
      let adminList = await getAllTeamAdmins();
      if (adminList.data && adminList.status == 200) {
        let data = adminList.data
          ? adminList.data.data
            ? adminList.data.data
            : []
          : [];
        if (data.length > 0) {
          let payload = this.props.user;
          data.map((item) => {
            if (item.is_selected && item.is_selected === 1) {
              payload["isPaidUser"] = item.is_premium == 1 ? true : false;
              this.setState({ selectedItem: item });
            }
          });
          this.props.dispatch({
            type: SET_USER,
            payload: payload,
          });
          this.setState({ teamAdminList: data });
        }
        console.log(JSON.stringify(adminList));
      }
      this.setState({ loading: false, isFetch: true });
    } catch (error) {
      console.log("get All team Admins =====>", error);
      const parsedError = parseError(error);
      showMessage("danger", parsedError);
      this.setState({ loading: false, isFetch: true });
    }
  };
  onItemSelect(item, index) {
    // item.is_selected = 1;
    this.setState({ selectedItem: item });
  }
  renderTeamAdminItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => {
          this.onItemSelect(item, index);
        }}
      >
        <View
          style={{
            width: 22,
            height: 22,
            borderRadius: 11,
            borderColor: "#4D2545",
            borderWidth: 1,
            marginRight: 20,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {item === this.state.selectedItem && (
            <View
              style={{
                width: 12,
                height: 12,
                borderRadius: 6,
                backgroundColor: "#4D2545",
              }}
            ></View>
          )}
        </View>
        <Text style={styles.adminName}>
          {item.admins.details.first_name + " " + item.admins.details.last_name}
        </Text>
      </TouchableOpacity>
    );
  };
  handlePressBackButton() {
    this.props.navigation.goBack(null);
  }
  onTeamAdminUpdate = async () => {
    if (this.state.selectedItem && this.state.selectedItem !== null) {
      this.setState({ updateAdminLoading: true });
      try {
        const selectedAdmin = this.state.selectedItem;
        let updateAdminRes = await updateTeamAdmin(selectedAdmin.admins.id);
        if (updateAdminRes.status == 200) {
          this.getAdminList();
          console.log(JSON.stringify(updateAdminRes));
        }
        let payload = this.props.user;
        payload["isAdminSelect"] = selectedAdmin.admins.id;

        this.props.dispatch({
          type: INIT_SESSION,
          payload: {
            isAdminSelect: true,
          },
        });
        this.props.dispatch({
          type: SET_USER,
          payload: payload,
        });
        this.setState({ updateAdminLoading: false });
        let isFromSettings = this.props.navigation.getParam(
          "isFromSettings",
          false
        );
        if (!isFromSettings) {
          this.props.navigation.navigate("Home");
        }
      } catch (error) {
        console.log("updateAdminRes =====>", error);
        const parsedError = parseError(error);
        showMessage("danger", parsedError);
        this.setState({ updateAdminLoading: false });
      }
    }
  };

  render() {
    let isFromSettings = this.props.navigation.getParam(
      "isFromSettings",
      false
    );
    console.log(this.props.navigation.getParam("isFromSettings", false));
    return (
      <Container style={styles.container}>
        <LightBar />
        {isFromSettings ? (
          <Header
            left={<BackButton onPress={() => this.handlePressBackButton()} />}
            center={<HeaderText title={en.select_team_admin} />}
          />
        ) : (
          <Header center={<HeaderText title={en.select_team_admin} />} />
        )}

        <View
          style={{
            flex: 1,
            // justifyContent: "center",
            // alignItems: "center",
          }}
        >
          {this.state.isFetch ? (
            this.state.teamAdminList.length > 0 ? (
              <View
                style={{
                  marginTop: resize(50, "h"),
                  width: "40%",
                  // alignSelf: "center",
                  // backgroundColor: "red",
                }}
              >
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={this.state.teamAdminList}
                  extraData={this.state}
                  bounces={false}
                  nestedScrollEnabled={true}
                  renderItem={this.renderTeamAdminItem}
                  keyExtractor={(item, index) => item + index}
                />
              </View>
            ) : (
              <Text style={styles.emptyText}>{en.no_admin}</Text>
            )
          ) : null}
        </View>
        {this.state.isFetch && (
          <View
            style={{
              height: 100,
              width: "100%",
              alignItems: "center",
              justifyContent: "center",
              marginBottom: 20,
            }}
          >
            <GradientButton
              buttonText={"Update"}
              containerStyle={styles.gradientButtonContainer}
              gradientStyle={styles.gradientButton}
              textStyle={styles.gradientButtonText}
              onPress={() => this.onTeamAdminUpdate()}
            />
          </View>
        )}

        {this.state.loading || this.state.updateAdminLoading ? (
          <Loading />
        ) : null}
      </Container>
    );
  }
}
const mapStateToProps = (state) => {
  // console.log("STORE REDUX=====>", state);
  return {
    user: state.user.data,
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(SelectTeamAdmin);
