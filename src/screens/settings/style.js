import { StyleSheet, Platform } from 'react-native';

//custom
import { width, NEXABOLD, NEXALIGHT, resize } from '../../assets/styles';

const styles = StyleSheet.create({
	content: {
		paddingVertical: resize(33, 'height'),
	},
	userMain: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		width
	},
	image: {
		width: resize(126),
		height: resize(126, 'height'),
		borderRadius: 4,
		resizeMode: 'contain'
	},
	userInfoContainer: {
		paddingLeft: resize(39),
		paddingRight: resize(153)
	},
	userName: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
		paddingVertical: resize(5, 'height'),
	},
	userCompany: {
		color: '#4d2545',
		fontFamily: NEXALIGHT,
		fontSize: resize(20),
		fontWeight: '400',
		paddingVertical: resize(5, 'height'),
		width: resize(200)
	},
	logoutContainer: {
		position: 'absolute',
		right: '4%',
		top: '25%'
	},
	gradientButtonContainer: {
		width: resize(170),
		height: resize(51, 'height'),
	},
	gradientButton: {
		width: resize(170),
		height: resize(51, 'height'),
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: 'rgba(0, 0, 0, 0.18)',
		shadowOffset: { width: 3, height: 0 },
		shadowRadius: 8,
		borderRadius: 6,
	},
	menuContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		marginVertical: resize(59, 'height')
	},
	monitorModeText: {
		paddingHorizontal: resize(50),
		color: '#000000',
		fontSize: resize(15),
		fontWeight: '400',
		lineHeight: resize(18),
		textAlign: 'center',
		...Platform.select({
			android: {
				paddingBottom: resize(40, 'height')
			}
		})
	},
	gradientButtonText: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(18),
		fontWeight: '400',
		paddingVertical: resize(5, 'height')
	},
});

export default styles;