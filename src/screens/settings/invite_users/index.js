import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, TextInput, FlatList, TouchableOpacity, Keyboard } from 'react-native';
import Header from '../../../components/commons/header';
import ButtonMenu from '../../../components/commons/button_menu';
import HeaderText from '../../../components/commons/header_text';
import { Container, Content } from 'native-base';
import styles from "./style";
import en from "../../../locale/en";
import { NEXABOLD, resize, NEXALIGHT } from '../../../assets/styles';
import colors from '../../../utils/colors';
import BackButton from '../../../components/commons/back_button';
import { Assets } from '../../../assets';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import showMessage from '../../../utils/show_message';
import { inviteUser } from '../../../api/subscription';
import ProgressLoader from '../../../components/commons/progress_loading';
import { connect } from 'react-redux';
import { withNavigationFocus } from "react-navigation";
import validateEmail from '../../../utils/validate_email';

class InviteUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            userList: [
                {
                    email: '',
                    name: ''
                },
                {
                    email: '',
                    name: ''
                },
                {
                    email: '',
                    name: ''
                },
                {
                    email: '',
                    name: ''
                }
            ]
        };
    }

    onBackPress = () => {
        const { navigation } = this.props;

        navigation.goBack();
    };

    onAddNewPress = () => {
        this.state.userList[this.state.userList.length] = {
            email: '',
            name: ''
        }
        this.setState({ userList: this.state.userList })
    }

    onRemoveItem = (item, index) => {
        this.state.userList.splice(index, 1)
        this.setState({ userList: this.state.userList })
    }

    onEmailChange = (data, index, text) => {
        this.state.userList[index].email = text;
        this.setState({ userList: this.state.userList })
    }

    onNameChange = (data, index, text) => {
        this.state.userList[index].name = text;
        this.setState({ userList: this.state.userList })
    }

    checkValidation() {
        Keyboard.dismiss();
        var isValid = true
        this.state.userList.map((data, index) => {

            if (data.email == '') {
                showMessage('danger', 'Please enter user email id')
                isValid = false;
            } else if (!validateEmail(data.email)) {
                showMessage('danger', en.valid_email)
                isValid = false;
            }
        })

        var valueArr = this.state.userList.map(function (item) { return item.email });
        var isDuplicate = valueArr.some(function (item, idx) {
            return valueArr.indexOf(item) != idx
        });
        console.log('isDuplicate :>> ', isDuplicate);
        if (isDuplicate) {
            isValid = false
            showMessage('danger', 'Please enter only unique email ids')
        }
        return isValid;
    }
    onSendInvitation = async () => {
        if (!this.checkValidation()) {
            return
        }
        // alert("success")
        // return
        this.setState({ isLoading: true })
        try {
            let params = {
                user_id: this.props.user.id,
                data: this.state.userList
            }
            const response = await inviteUser(params);
            console.log('response :>> ', response);
            if (this.props.navigation.state.params.callback != undefined) {
                this.props.navigation.state.params.callback()
            }
            this.props.navigation.goBack()
            this.setState({ isLoading: false });
        } catch (error) {
            this.setState({ isLoading: false })
            console.log(" Plan Error =====>", error.response);
        }

    }

    render() {
        return (
            <Container>
                <ProgressLoader isLoading={this.state.isLoading} />
                <Header
                    left={<BackButton onPress={this.onBackPress} />}
                    center={<HeaderText title={en.invite_users} />}
                />
                <ImageBackground source={require('../../../assets/images/commons/bg.png')} style={styles.container}
                    defaultSource={require('../../../assets/images/commons/bg.png')}>
                    <KeyboardAwareScrollView
                        contentContainerStyle={{
                            flexGrow: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps={'always'}
                        enableOnAndroid
                        scrollEnabled>
                        <View style={{ backgroundColor: colors.WHITE, borderRadius: 8, padding: 20, width: '70%' }}>
                            <Text style={{ fontSize: 20, textAlign: 'center', fontFamily: NEXALIGHT }}>{'INVITE YOUR CASTING TEAM'}</Text>

                            <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                <Text style={{ flex: 1, marginRight: 10, fontSize: 16, fontFamily: NEXALIGHT, color: colors.PRIMARY }}>{'Email Address'}</Text>
                                <Text style={{ flex: 1, marginLeft: 10, fontSize: 16, fontFamily: NEXALIGHT, color: colors.PRIMARY }}>{'Name (Optional)'}</Text>
                            </View>

                            {this.state.userList.map((data, index) => {
                                return (
                                    <View key={index} style={{ flexDirection: 'row', marginTop: 16, alignItems: 'center' }}>
                                        <TextInput
                                            style={{
                                                fontFamily: NEXALIGHT, flex: 1, marginRight: 10,
                                                paddingLeft: 15, paddingVertical: 10, borderRadius: 20,
                                                borderColor: colors.PRIMARY, borderWidth: 0.3
                                            }}
                                            value={data.email}
                                            keyboardType={'email-address'}
                                            autoCapitalize={'none'}
                                            onChangeText={(text) => this.onEmailChange(data, index, text)}
                                            placeholder={'Eg. john@mail.com'}
                                        />
                                        <TextInput
                                            style={{
                                                fontFamily: NEXALIGHT, flex: 1, marginLeft: 10,
                                                paddingLeft: 15, paddingVertical: 10, borderRadius: 20,
                                                borderColor: colors.PRIMARY, borderWidth: 0.3
                                            }}
                                            value={data.name}
                                            onChangeText={(text) => this.onNameChange(data, index, text)}
                                            placeholder={'Eg. John'} />
                                        {index != 0 ? <TouchableOpacity onPress={() => this.onRemoveItem(data, index)}>
                                            <Image
                                                source={Assets.cancel}
                                                style={{ width: 20, height: 20, tintColor: colors.BLACK, marginLeft: 10 }}
                                                resizeMode={'contain'} />
                                        </TouchableOpacity> : <View style={{ width: 20, height: 20, tintColor: colors.BLACK, marginLeft: 10 }} />}
                                    </View>
                                )
                            })}

                            <View style={{
                                flexDirection: 'row', marginTop: 20,
                                alignItems: 'center'
                            }}>
                                <Image source={Assets.add} defaultSource={Assets.add} />
                                <TouchableOpacity onPress={this.onAddNewPress}>
                                    <Text style={{ marginLeft: 10, color: colors.PRIMARY }}>{'Add New'}</Text>
                                </TouchableOpacity>
                                <Text style={{ marginHorizontal: 10, fontFamily: NEXALIGHT, fontSize: 16, color: colors.PRIMARY }}>{'OR'}</Text>
                                <Text style={{ color: colors.PRIMARY }}>{'Add Multiple at Once'}</Text>
                            </View>

                            <TouchableOpacity style={{
                                backgroundColor: colors.PRIMARY, alignSelf: 'flex-end', marginTop: 20,
                                borderRadius: 24, paddingHorizontal: 35, paddingVertical: 15
                            }} onPress={this.onSendInvitation}>
                                <Text style={{ color: colors.WHITE, fontSize: 18 }}>{"Send Invitation"}</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    console.log(" REDUX DATA CREATE GROUP=========>", state);

    return {
        user: state.user.data
    };
};

export default withNavigationFocus(connect(mapStateToProps)(InviteUsers));