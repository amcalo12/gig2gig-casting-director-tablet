import { StyleSheet, Platform } from 'react-native';

//custom
import { width, height, NEXABOLD, NEXALIGHT, resize } from '../../../assets/styles';
import colors from '../../../utils/colors';

const styles = StyleSheet.create({
    content: {
        // paddingVertical: resize(33, 'height'),
        marginHorizontal: 20,
        marginTop: 20,
        borderWidth: 2,
        borderColor: 'red'
    },
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center'
    }
});

export default styles;