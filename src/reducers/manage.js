import {
  SET_CHECKIN_MODE_PIN,
  SET_MONITOR_UPDATES_PIN,
  DESTROY_CHECKIN_MODE_PIN,
  DESTROY_MONITOR_UPDATES_PIN,
  SET_AUDITION_ID_TO_CHECKIN,
  DESTROY_AUDITION_ID,
  SET_AUDITIONS_APPOINTMENTS,
  DESTROY_AUDITIONS_APPOINTMENTS,
  AUDITION_COUNT,
  IS_MANAGER
} from '../actions/manage';
import { DESTROY_SESSION } from '../actions/auth';

const INITIAL_STATE = {
  checkinPin: '',
  updatesPin: '',
  audition_id: null,
  appointments: [],
  audition_count: 0,
  isManager: false
};

function manage(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_CHECKIN_MODE_PIN:
      return {
        ...state,
        checkinPin: action.payload
      }
    case DESTROY_CHECKIN_MODE_PIN:
      return {
        ...state,
        checkinPin: ''
      }
    case SET_MONITOR_UPDATES_PIN:
      return {
        ...state,
        updatesPin: action.payload
      }
    case DESTROY_MONITOR_UPDATES_PIN:
      return {
        ...state,
        checkinPin: ''
      }
    case SET_AUDITION_ID_TO_CHECKIN:
      return {
        ...state,
        audition_id: action.payload
      }
    case IS_MANAGER:
      return {
        ...state,
        isManager: action.payload
      }
    case DESTROY_AUDITION_ID:
      return {
        ...state,
        audition_id: null
      }
    case DESTROY_AUDITION_ID:
      return {
        ...state,
        audition_id: null
      }
    case SET_AUDITIONS_APPOINTMENTS:
      return {
        ...state,
        appointments: action.payload
      }
    case DESTROY_AUDITIONS_APPOINTMENTS:
      return {
        ...state,
        appointments: []
      }
    case AUDITION_COUNT:
      return {
        ...state,
        audition_count: action.payload
      }
    case DESTROY_SESSION:
      return INITIAL_STATE;
    default:
      return state;
  }
}

export default manage;