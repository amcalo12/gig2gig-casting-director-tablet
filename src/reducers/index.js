import { combineReducers } from 'redux';

//reducers
import auth from './auth';
import user from './user';
import manage from './manage';
import news from './news';
import checkin from './checkin'

export default combineReducers({
	auth,
	user,
	manage,
	news,
	checkin
});