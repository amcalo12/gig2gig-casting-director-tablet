import { SET_USER, DEFAULT_INSTANT_FEEDBACK } from '../actions/user';
import { DESTROY_SESSION } from '../actions/auth'

const INITIAL_STATE = {
  data: {},
  instantFeedback: {
    reject: "Thanks for attending. That's all we need today",
    accept: "Thanks for attending. That's all we need today"
  }

};

function user(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,

        data: {
          ...action.payload
        }
      }
    case DEFAULT_INSTANT_FEEDBACK:
      return {
        ...state,
        instantFeedback: action.payload
      }
    case DESTROY_SESSION:
      return INITIAL_STATE;
    default:
      return state;
  }
}

export default user;