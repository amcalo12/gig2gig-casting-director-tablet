import { SET_NEWS_ACTUAL } from '../actions/news';

const INITIAL_STATE = {
    news: {}
};

function news(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_NEWS_ACTUAL:
            return {
                ...state,
                news: { ...action.payload }
            }
        default:
            return state;
    }
}

export default news;