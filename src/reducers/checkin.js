import { SET_AUDITION_INFO } from '../actions/checkin';

const INITIAL_STATE = {
	infoAuditon: {}
};

function checkin(state = INITIAL_STATE, action) {
	switch (action.type) {
		case SET_AUDITION_INFO:
			return {
				...state,
				infoAuditon: action.payload
			}
		default:
			return state;
	}
}

export default checkin;