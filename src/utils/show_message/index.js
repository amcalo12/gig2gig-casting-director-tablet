import { Toast } from 'native-base';

const showMessage = (type = 'success', message = '', duration = 4000) => {
  return (
    Toast.show({
      type: type,
      text: message,
      duration: duration
    })
  );
}

export default showMessage;