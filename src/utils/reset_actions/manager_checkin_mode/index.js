import { StackActions, NavigationActions } from 'react-navigation';

export const resetCheckinMode = StackActions.reset({
	index: 0,
	actions: [
		NavigationActions.navigate({ routeName: 'ManagerCheckinMode' })
	]
});