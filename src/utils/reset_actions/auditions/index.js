import { StackActions, NavigationActions } from 'react-navigation';

export const resetHomeAction = StackActions.reset({
  index: 0,
  key: null,
  actions: [
    NavigationActions.navigate({ routeName: 'Home' })
  ]
});