const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const validateEmail = (email) => {
  if (reg.test(email)) {
    return true
  }

  return false;
}

export default validateEmail;