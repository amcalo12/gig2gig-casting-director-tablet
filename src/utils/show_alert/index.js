import { Alert } from 'react-native';

const showAlert = (message = '', title = '') => {
  return (
    Alert.alert(title, message)
  );
}

export default showAlert;