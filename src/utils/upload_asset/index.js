import firebase from "react-native-firebase";
import moment from "moment";

const defaultMetadata = {
  contentType: "image/jpeg"
};

const defaultName = moment().format();

const instance = firebase.storage();

const uploadAsset = async (
  reference,
  uri,
  name = defaultName,
  metadata = defaultMetadata,
  realName = null
) => {
  // console.log('name :', name);
  // return
  const currentTime = moment().format();
  const currentTimeIsDiffToDefaultName = currentTime !== defaultName;
  const curentNameDiffToDefaultName = name !== defaultName;
  let finalName = name;
  if (currentTimeIsDiffToDefaultName && !curentNameDiffToDefaultName) {
    finalName = currentTime;
  }

  let ext = "." + uri.substring(uri.lastIndexOf('.') + 1, uri.length)

  console.log("Video Data", {
    finalName,
    name,
    currentTimeIsDiffToDefaultName,
    curentNameDiffToDefaultName,
    currentTime,
    defaultName,
    uri
  });
  console.log('file name new :', finalName + ext);
  return new Promise((resolve, reject) => {
    instance
      .ref(reference)
      .child(finalName + ext)
      .putFile(uri, metadata)
      .then(uploadedFile => {
        resolve(uploadedFile.downloadURL);
      }) 
      .catch(error => {
        console.log("=============================");
        console.log({ error });
        console.log("ERROR =============================", error);
        console.log(
          "ERROR.RESPONSE =============================",
          error.response
        );
        reject({
          response: {
            data: { data: `We don't have permission to access the ${realName}` }
          }
        });
      });
  });
};

export default uploadAsset;
