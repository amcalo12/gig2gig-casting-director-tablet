const isUrl = url => {
	var regexp = /^[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
	var regexp2 = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	let valor = false;
	if (regexp.test(url) || regexp2.test(url)) {
		valor = true;
	}
	return valor;
};

export default isUrl;
