const colors = {
  PRIMARY: "#4d2545",
  SHADOW_COLOR: "rgba(0, 0, 0, 0.50)",
  WHITE: "#ffffff",
  BLACK: "#000000",
  TABLE_HEADER_LIGHT_GREY: "#aaaaaa", //#D5D5D5,
  LIGHT_GREY: "#D8D7D8",
  ORANGE: "#D8893A",
  BORDER_COLOR: "#D6D6D6",
  DARK_BACKGROUND: "#BFBFBF",
};

export default colors;
