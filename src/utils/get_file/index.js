import { Platform } from "react-native";

import DocumentPicker from "react-native-document-picker";

//custom
import { width, height } from "../../assets/styles";
import { getThumbnail } from "../constant";
import RNThumbnail from "react-native-thumbnail";
var RNFS = require("react-native-fs");

const getFile = async (typeC, typeCustom = false) => {
  try {
    const file = await new Promise((resolve, reject) => {
      DocumentPicker.pick({
        top: height,
        left: width,
        type: [DocumentPicker.types.allFiles],
      })
        .then((res) => {
          console.log(
            res.uri,
            res.type, // mime type
            res.name,
            res.size
          );
          resolve(res);
        })
        .catch((err) => {
          if (DocumentPicker.isCancel(err)) {
            // User cancelled the picker, exit any dialogs or menus and move on
          } else {
            reject(err);
          }
        });
    });
    console.log({ file });
    let type = file.name.split(".");
    type = type[type.length - 1].toLowerCase();
    let metadata = {
      contentType: `application/${type}`,
    };

    if (!typeCustom) {
      if (type === "mp3") {
        type = 1;
        metadata["contentType"] = "audio/mpeg";
      } else if (type === "mp4") {
        type = 2;
        metadata["contentType"] = "video/mp4";
      } else if (type === "mov") {
        type = 2;
        metadata["contentType"] = "video/quicktime";
      } else if (type === "pdf" || type === "doc" || type === "docx") {
        type = 3;
      } else if (
        type === "jpg" ||
        type === "jpeg" ||
        type === "gif" ||
        type === "png"
      ) {
        type = 4;
      } else {
        return new Promise.reject({
          errorCode: "FNR",
          string: "The type of extension is not allowed.",
        });
      }
    } else {
      if (type === typeCustom) {
        type = typeC;
      } else {
        return new Promise.reject({
          errorCode: "FNR",
          string: "The type of extension is not allowed.",
        });
      }
    }

    const newFile = {
      filename: file.name,
      type,
      metadata,
      url: decodeURI(
        Platform.OS === "ios" ? String(file.uri).replace("file:", "") : file.uri
      ),
    };

    return new Promise.resolve(newFile);
  } catch (error) {
    return new Promise.reject(error);
  }
};

export const getFileMultiple = async (typeC, typeCustom = false) => {
  try {
    const file = await new Promise((resolve, reject) => {
      DocumentPicker.pickMultiple({
        top: height,
        left: width,
        type: [DocumentPicker.types.allFiles],
      })
        .then((res) => {
          console.log("res 123454:", res);
          console.log(
            res.uri,
            res.type, // mime type
            res.name,
            res.size
          );
          resolve(res);
        })
        .catch((err) => {
          if (DocumentPicker.isCancel(err)) {
            // User cancelled the picker, exit any dialogs or menus and move on
          } else {
            reject(err);
          }
        });
    });
    let newFiles = [];
    const files = file.map(async (file) => {
      console.log({ file });
      let type = file.name.split(".");
      type = type[type.length - 1].toLowerCase();
      let metadata = {
        contentType: `application/${type}`,
      };
      let uri = Platform.OS === "android" ? file.path : file.uri;
      let thumbnail = "";
      if (!typeCustom) {
        if (type === "mp3") {
          type = 1;
          metadata["contentType"] = "audio/mpeg";
        } else if (type === "mp4") {
          type = 2;
          metadata["contentType"] = "video/mp4";
          let result = await RNThumbnail.get(uri);
          console.log("result :", result);
          thumbnail = result.path;
        } else if (type === "mov") {
          type = 2;
          metadata["contentType"] = "video/quicktime";
          let result = await RNThumbnail.get(uri);
          console.log("result :", result);
          thumbnail = result.path;
        } else if (type === "pdf" || type === "doc" || type === "docx") {
          type = 3;
          // copyFile = await RNFS.copyFile(file.uri, RNFS.DocumentDirectoryPath + new Date())
          // console.log('copyFile :', copyFile);
        } else if (
          type === "jpg" ||
          type === "jpeg" ||
          type === "gif" ||
          type === "png"
        ) {
          type = 4;
          let thumbnailImage = await getThumbnail(file.uri);
          console.log("thumbnail :", thumbnailImage);
          thumbnail = thumbnailImage.uri;
        } else {
          return new Promise.reject({
            errorCode: "FNR",
            string: "The type of extension is not allowed.",
          });
        }
      } else {
        if (type === typeCustom) {
          type = typeC;
        } else {
          return new Promise.reject({
            errorCode: "FNR",
            string: "The type of extension is not allowed.",
          });
        }
      }
      let newFile = {
        filename: file.name,
        type,
        metadata,
        url: decodeURI(
          Platform.OS === "ios"
            ? String(file.uri).replace("file://", "")
            : file.uri
        ),
        // url: decodeURIComponent(Platform.OS === 'ios' ? String(file.uri).replace('file://', '') : file.uri),
        thumbnail: decodeURI(
          Platform.OS === "ios"
            ? String(thumbnail).replace("file://", "")
            : thumbnail
        ),
        // url: file.uri,
        shareable: "yes",
        // thumbnail: "file:///var/mobile/Containers/Data/Application/B129DDE3-F0AC-4F52-B65A-5E490CFED5EC/Library/Caches/AD2562B3-CD12-43AE-82A9-45405A3A2CF4.png"
      };

      console.log("new change :");

      // newFiles[newFiles.length] = newFile;
      // console.log('newFiles :', newFiles);
      return newFile;
    });
    // console.log({ file });
    // let type = file.name.split('.');
    // type = type[type.length - 1].toLowerCase();
    // let metadata = {
    // 	contentType: `application/${type}`
    // };

    // if (!typeCustom) {
    // 	if (type === 'mp3') {
    // 		type = 1;
    // 		metadata['contentType'] = 'audio/mpeg';
    // 	} else if (type === 'mp4') {
    // 		type = 2;
    // 		metadata['contentType'] = 'video/mp4';
    // 	} else if (type === 'mov') {
    // 		type = 2;
    // 		metadata['contentType'] = 'video/quicktime';
    // 	} else if (type === 'pdf' || type === 'doc' || type === 'docx') {
    // 		type = 3;
    // 	} else if (
    // 		type === 'jpg' ||
    // 		type === 'jpeg' ||
    // 		type === 'gif' ||
    // 		type === 'png'
    // 	) {
    // 		type = 4;
    // 	} else {
    // 		return new Promise.reject({
    // 			errorCode: 'FNR',
    // 			string: 'The type of extension is not allowed.'
    // 		});
    // 	}
    // } else {
    // 	if (type === typeCustom) {
    // 		type = typeC;
    // 	} else {
    // 		return new Promise.reject({
    // 			errorCode: 'FNR',
    // 			string: 'The type of extension is not allowed.'
    // 		});
    // 	}
    // }

    // const newFile = {
    // 	filename: file.name,
    // 	type,
    // 	metadata,
    // 	url: decodeURI(
    // 		Platform.OS === 'ios' ? String(file.uri).replace('file:', '') : file.uri
    // 	)
    // };

    // return new Promise.resolve(newFile);
    console.log("promise pass :");
    return new Promise.all(files);
  } catch (error) {
    console.log("error :", error);
    return new Promise.reject(error);
  }
};

export default getFile;
