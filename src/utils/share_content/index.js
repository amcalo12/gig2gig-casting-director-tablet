import { Share } from 'react-native';

const shareContent = async (message) => {
	try {
		console.log(message);
		const result = await Share.share({
			message,
		});

		if (result.action === Share.sharedAction) {
			if (result.activityType) {
				// shared with activity type of result.activityType
				return true
			} else {
				// shared
				return true
			}
		} else if (result.action === Share.dismissedAction) {
			// dismissed
			return false
		}
	} catch (error) {
		return false
	}
};


export default shareContent;