import { StyleSheet } from 'react-native';
import { NEXABOLD, resize } from '../assets/styles';

//custom

export const general_styles = StyleSheet.create({
    title: {
        color: '#4d2545',
        fontFamily: NEXABOLD,
        fontSize: resize(20),
        fontWeight: '400',
    },
    cardStyle: {
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 5,
    }
});