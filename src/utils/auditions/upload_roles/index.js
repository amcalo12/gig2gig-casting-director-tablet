import moment from 'moment';

//custom
import uploadAsset from '../../upload_asset';
import validateUrl from '../../validate_url';

const uploadRoles = async (collection = []) => {
  const name = moment().format('HHmmss');
  let _collection = [...collection];

  const roles = _collection.map(async (role, index) => {
    let _role = { ...role };

    if (_role.cover && _role.cover.hasOwnProperty('uri')) {
      if (!validateUrl(_role.cover.uri)) {
        let url = await uploadAsset('tablet/auditions/roles', _role.cover.uri, `${name}${index}`);
        let thumbnail = await uploadAsset('tablet/auditions/roles', _role.cover.thumbnail, `${name}${index}`);
        console.log('role thumbnail :', thumbnail);

        _role['cover'] = url;
        _role['cover_thumbnail'] = thumbnail
        _role['cover_name'] = _role.cover_name
        // _role['name_cover'] = `${name}${index}`;
      } else {
        _role['cover'] = _role['cover'].uri;
        _role['cover_thumbnail'] = _role['cover'].thumbnail
        _role['cover_name'] = _role['cover_name']
      }
    }

    return _role;
  });

  return await Promise.all(roles)
}

export default uploadRoles;