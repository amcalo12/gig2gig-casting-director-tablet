import moment from 'moment';

//custom
import uploadAsset from '../../upload_asset';
import validateUrl from '../../validate_url';

const name = moment().format('HHmmss');

const uploadFiles = async (collection = []) => {
  console.log('collection 111:', collection);
  let _collection = [...collection];

  const files = _collection.map(async (file, index) => {
    let _file = { ...file };
    let extension = '';
    if (!validateUrl(_file.url)) {
      if ('metadata' in _file) {
        let type = _file.metadata.contentType.split('/');
        extension = `.${type[type.length - 1].toLowerCase()}`;
      }
      console.log({ file: _file, extension });
      let url = await uploadAsset(
        'tablet/auditions/files',
        _file.url,
        `${name}${index}${extension}`,
        _file.metadata,
        _file.name
      );
      let thumbnail = "";

      if (_file.thumbnail != "" && _file.thumbnail != null) {
        thumbnail = await uploadAsset(
          'tablet/auditions/files',
          _file.thumbnail,
          `${name}${index}${extension}`,
          _file.metadata,
          _file.name
        );
      }

      return (_file = {
        url,
        thumbnail,
        type: _file.type,
        name: _file.name,
        share: _file.share
      })
    }

    return _file;
  });

  return await Promise.all(files);
};

export const uploadSingleFile = async (collection = {}) => {
  let _file = { ...collection };
  let extension = '';
  if (!validateUrl(_file.url)) {
    if ('metadata' in _file) {
      let type = _file.metadata.contentType.split('/');
      extension = `.${type[type.length - 1].toLowerCase()}`;
    }

    let url = await uploadAsset(
      'tablet/contract/files',
      _file.url,
      `${name}${extension}`,
      _file.metadata,
      _file.name
    );

    return (_file = {
      url,
      type: _file.type,
      name: _file.name,
      share: _file.share
    })
  }

  return _file;

}

export default uploadFiles;
