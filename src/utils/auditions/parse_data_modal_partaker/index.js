const parseDataModalPartaker = (data = {}) => {
  console.log("parseDataModalPartaker  :", parseDataModalPartaker);
  let credits = [];
  let education = [];
  let appearance = [];
  //credicts
  if (data.credits.length > 0) {
    data.credits.map((credit) => {
      credits.push({
        label: "Production Type",
        value: credit.type,
      });

      credits.push({
        label: "Project Name",
        value: credit.name,
      });

      credits.push({
        label: "Role",
        value: credit.rol,
      });

      credits.push({
        label: "Director/Production Company",
        value: credit.production,
      });
    });
  } else {
    credits = [
      {
        label: "Production Type",
        value: "",
      },
      {
        label: "Project Name",
        value: "",
      },
      {
        label: "Role",
        value: "",
      },
      {
        label: "Director/Production Company",
        value: "",
      },
    ];
  }

  if (data.education && data.education.length > 0) {
    data.education.map((item) => {
      education.push({
        label: "School",
        value: item.school,
      });

      education.push({
        label: "Degree/Course",
        value: item.degree,
      });

      education.push({
        label: "Instructor",
        value: item.instructor,
      });

      education.push({
        label: "Location",
        value: item.location,
      });

      education.push({
        label: "Year",
        value: item.year,
      });
    });
  } else {
    education = [
      {
        label: "School",
        value: "",
      },
      {
        label: "Degree/Course",
        value: "",
      },
      {
        label: "Instructor",
        value: "",
      },
      {
        label: "Location",
        value: "",
      },
      {
        label: "Year",
        value: "",
      },
    ];
  }

  if (data.aparence) {
    appearance = [
      {
        label: "Height",
        value: data.aparence.height,
      },
      {
        label: "Weight",
        value: data.aparence.weight,
      },
      {
        label: "Hair Color",
        value: data.aparence.hair,
      },
      {
        label: "Eye Color",
        value: data.aparence.eyes,
      },
      {
        label: "Race",
        value: data.aparence.race,
      },
      {
        label: "Personal Flair",
        value: data.aparence.personal_flare,
      },
      {
        label: "Gender Pronouns",
        value: data.aparence.gender_pronouns,
      },
    ];
  } else {
    if (data.appearance) {
      appearance = [
        {
          label: "Height",
          value: data.appearance.height,
        },
        {
          label: "Weight",
          value: data.appearance.weight,
        },
        {
          label: "Hair Color",
          value: data.appearance.hair,
        },
        {
          label: "Eye Color",
          value: data.appearance.eyes,
        },
        {
          label: "Race",
          value: data.appearance.race,
        },
        {
          label: "Personal Flair",
          value: data.appearance.personal_flare,
        },
        {
          label: "Gender Pronouns",
          value: data.appearance.gender_pronouns,
        },
      ];
    } else {
      appearance = [
        {
          label: "Height",
          value: "",
        },
        {
          label: "Weight",
          value: "",
        },
        {
          label: "Hair Color",
          value: "",
        },
        {
          label: "Eye Color",
          value: "",
        },
        {
          label: "Race",
          value: "",
        },
        {
          label: "Personal Flair",
          value: "",
        },
        {
          label: "Gender Pronouns",
          value: "",
        },
      ];
    }
  }

  let _data = [
    {
      title: "Info",
      items: [
        {
          label: "Stage Name",
          value: data.details.stage_name,
        },
        {
          label: "Professional/ Working Title",
          value: data.details.profesion,
        },
      ],
    },
    {
      title: "Credits",
      items: credits,
    },
    {
      title: "Education & Training",
      items: education,
    },
    {
      title: "Appearance",
      items: appearance,
    },
  ];

  return _data;
};

export default parseDataModalPartaker;
