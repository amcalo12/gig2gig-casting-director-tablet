import moment from "moment";

const parseFormData = (state) => {
  const {
    title,
    date,
    time,
    location,
    description,
    cover,
    contractDateStart,
    contractDateEnd,
    rehearsalDateStart,
    rehearsalDateEnd,
    roles,
    url,
    production,
    contract,
    union,
    appointment,
    files,
    invitations,
    additionalInformation,
    personalInformation,
    phone,
    email,
    other,
    online,
    option,
    cover_name,
    endDate,
    isSocialDistancing,
    groupSize,
  } = state;

  let formData = {
    title,
    // date: online ? null : date,
    // time: online
    //   ? null
    //   : time != null || time != "" || time != "Invalid date"
    //     ? null
    //     : moment(time, "HH::mm A").format("HH:mm"),
    // location: online ? null : location,
    description,
    cover,
    cover_name,
    roles,
    url,
    // appointment: online
    //   ? {
    //     spaces: 10,
    //     type: 1,
    //     length: "20",
    //     start: "10:00",
    //     end: "18:00",
    //     slots: null
    //   }
    //   : appointment,
    contract,
    union,
    online,
    contributors: invitations,
    additional_info: additionalInformation,
    personal_information: personalInformation,
    phone,
    email,
    other_info: other,
  };

  if (online) {
    formData["end_date"] = moment(endDate).format("YYYY-MM-DD HH:mm:ss");
  }

  let dates = [];
  let dateType = {};

  dateType = {
    to: contractDateEnd,
    from: contractDateStart,
    type: 1,
  };
  dates.push(dateType);
  dateType = {
    to: rehearsalDateEnd,
    from: rehearsalDateStart,
    type: 2,
  };
  dates.push(dateType);

  formData["dates"] = dates;
  formData["production"] = production.join(",");
  formData["media"] = [];

  files.map((file) => {
    let row = {
      url: file.url,
      type: file.type,
      metadata: file.metadata,
      name: file.filename,
      share: file.shareable,
      thumbnail: file.thumbnail,
    };

    formData["media"].push(row);
  });

  let roundArray = [];
  if (online) {
    roundArray[roundArray.length] = {
      date: "",
      time: "",
      location: null,
      appointment: {
        spaces: 10,
        type: 1,
        length: "20",
        start: "10:00",
        end: "18:00",
        slots: null,
      },
    };
    formData["rounds"] = roundArray;
    console.log("roundArray :", roundArray);
    console.log(JSON.stringify(formData));
  } else {
    option.map((data) => {
      if (data.type == 1) {
        roundArray[roundArray.length] = {
          date: data.date,
          time: moment(data.time, "h:mm:ss A").format("HH:mm"),
          location: data.location,
          appointment: data.appointment,
          grouping_enabled: isSocialDistancing,
          grouping_capacity: groupSize,
        };
      }
    });
    formData["rounds"] = roundArray;
    console.log("roundArray :", roundArray);
  }
  return formData;
};

export default parseFormData;
