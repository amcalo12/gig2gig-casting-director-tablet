import moment from "moment"

export const dateNativeFormar = (momentObj) => {
    const year = momentObj.get('year')
    const month = momentObj.get('month');
    const days = momentObj.get('date');
    return new Date(year, month, days);
}