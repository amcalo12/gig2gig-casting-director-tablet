import { Keyboard } from 'react-native';

//locale
import en from '../../../locale/en';
import validateEmail from '../../validate_email';

export default function validateAuditionFields(state) {
	const {
		title,
		date,
		time,
		location,
		description,
		cover,
		contractDateStart,
		contractDateEnd,
		rehearsalDateStart,
		rehearsalDateEnd,
		url,
		union,
		production,
		contract,
		roles,
		appointment,
		additionalInformation,
		personalInformation,
		online,
		email,
		option
	} = state;
	let errorMessage = '';

	Keyboard.dismiss();

	if (title === '') {
		errorMessage = en.require_title;
	}
	// else if (!date && !online) {
	// 	errorMessage = en.require_date;
	// } else if (!time && !online) {
	// 	errorMessage = en.require_time;
	// } else if (!location && !online) {
	// 	errorMessage = en.require_location;
	// } 
	else if (description === '') {
		errorMessage = en.require_description;
	} else if (cover === null) {
		errorMessage = en.require_cover;
	}
	// else if (contractDateStart === null) {
	// 	errorMessage = en.require_contract_date_start;
	// } else if (contractDateEnd === null) {
	// 	errorMessage = en.require_contract_date_end;
	// } else if (rehearsalDateStart === null) {
	// 	errorMessage = en.require_rehearsal_date_start;
	// } else if (rehearsalDateEnd === null) {
	// 	errorMessage = en.require_rehearsal_date_end;
	// } 
	// else if (url === '') {
	// 	errorMessage = en.require_audition_url;
	// }
	// else if (Object.entries(appointment).length === 0 && !online) {
	// 	errorMessage = en.require_appointment;
	// } 

	else if (union === '') {
		console.log('union :');
		errorMessage = en.require_union_tag;
	} else if (contract === '') {
		errorMessage = en.require_contract_tag;
	} else if (production.length === 0) {
		errorMessage = en.require_production_tag;
	} else if (roles.length === 0) {
		errorMessage = en.require_roles;
	}
	// else if (additionalInformation == '') {
	// 	errorMessage = en.require_additional_nformation;
	// } else if (personalInformation == '') {
	// 	errorMessage = en.require_personal_information;
	// }
	if (email !== '' && !validateEmail(email)) {
		errorMessage = en.valid_email;
	}


	console.log('option length :');
	if (!state.online) {
		for (var i = 0; i < option.length; i++) {
			console.log('for loop :', option[i]);
			if (option[i].type == 1) {
				console.log('type 1 :');
				if (option[i].date == "") {
					console.log('date :');
					errorMessage = "Please enter date for " + option[i].value
					break;
				} else if (option[i].time == "") {
					errorMessage = "Please enter time for " + option[i].value
					break;
				} else if (option[i].location == null) {
					errorMessage = "Please enter location for " + option[i].value
					break;
				} else if (option[i].appointment == null) {
					errorMessage = "Please enter appointment for " + option[i].value
					break;
				}
			}
		}
	}

	return errorMessage;
}
