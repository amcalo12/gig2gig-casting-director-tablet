import ImagePicker from 'react-native-image-picker';

const getImage = async () => {
  const options = {
    title: 'Select image',
    cameraType: 'back',
    mediaType: 'photo',
    maxWidth: 640,
    maxHeight: 427,
    storageOptions: {
      cameraRoll: false,
      skipBackup: true,
      path: 'gg2gg',
    },
  };

  return new Promise((resolve, reject) => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        reject(response);
      } else if (response.error) {
        reject(response);
      } else {
        resolve(response);
      }
    });
  });
}

export default getImage;