import ImagePicker from 'react-native-image-picker';

const recordVideo = async () => {
  const options = {
    title: 'Upload Video',
    takePhotoButtonTitle: 'Take video',
    cameraType: 'back',
    mediaType: 'video',
    videoQuality: 'high',
    storageOptions: {
      cameraRoll: false,
      skipBackup: true,
      path: 'gg2gg',
    },
  };

  return new Promise((resolve, reject) => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        reject(response);
      } else if (response.error) {
        reject(response);
      } else {
        resolve(response);
      }
    });
  });
}

export default recordVideo;