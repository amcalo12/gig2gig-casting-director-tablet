import DeviceInfo from "react-native-device-info";
import { Dimensions, Alert, Platform, Linking } from "react-native";
import ImageResizer from "react-native-image-resizer";
import { store } from "../../store";

export const DEVICE_ID = DeviceInfo.getUniqueId();
var { height, width } = Dimensions.get("window");

export const gradientColors = ["#4d2545", "#782541"];

export const DEVICE = {
  DEVICE_HEIGHT: height,
  DEVICE_WIDTH: width,
};

// export const SUBSCRIPTION_ALERT = "You don't have any subscription to access this feature."
export const SUBSCRIPTION_ALERT =
  "In order to subscribe to Gig2Gig Casting, please visit our website.";
export const SUBSCRIPTION_ALERT_SUB_USER =
  "Please contact your Admin to upgrade to access these features.";
export const SUBSCRIPTION_SIGNUP_ALERT =
  "For access premium feature you need to purchase subscription.";
export const CANCEL_SUBSCRIPTION =
  "Are you sure, you want to cancel the subscription?";
export const RESUME_SUBSCRIPTION =
  "Are you sure, you want to resume the subscription?";

export function openSubscriptionDialog() {
  let messageToShow = SUBSCRIPTION_ALERT;
  let isSubUser = store.getState().user.data.is_invited;
  console.log("isSubUser :>> ", isSubUser);
  if (isSubUser) {
    messageToShow = SUBSCRIPTION_ALERT_SUB_USER;
    showAlert(messageToShow);
    return;
  }

  subscriptionAlert(messageToShow, () => {
    Linking.openURL("https://casting.gig2gig.com/");
  });
}

export function signupSubscriptionAlert() {
  subscriptionAlertSignup(() => {
    Linking.openURL("https://casting.gig2gig.com/");
  });
}

export function subscriptionAlertSignup(onYesClick) {
  Alert.alert(
    "Subscription",
    "For access premium feature you need to purchase subscription.",
    [
      {
        text: "Cancel",
        style: "destructive",
      },
      {
        text: "Subscribe",
        onPress: () => {
          onYesClick();
        },
      },
    ],
    { cancelable: false }
  );
}

export function showAlert(msg) {
  Alert.alert(
    "Subscription",
    msg,
    [
      {
        text: "OK",
        onPress: () => {},
      },
    ],
    {
      cancelable: false,
    }
  );
}

export function subscriptionAlert(message, onYesClick) {
  Alert.alert(
    "Subscription",
    message,
    [
      {
        text: "Cancel",
        style: "destructive",
      },
      {
        text: "OK",
        onPress: () => {
          onYesClick();
        },
      },
    ],
    { cancelable: false }
  );
}

export function restorePerformer(onYesClick) {
  Alert.alert(
    "Restore Performer?",
    "This will remove the performer from the hidden list.",
    [
      {
        text: "Cancel",
        style: "destructive",
      },
      {
        text: "Restore",
        onPress: () => {
          onYesClick();
        },
      },
    ],
    { cancelable: false }
  );
}

export function showConfirmationDialogYesNoClick(msg, onYesClick) {
  Alert.alert(
    "Gig2Gig",
    msg,
    [
      {
        text: "Ok",
        onPress: () => {
          onYesClick();
        },
      },
    ],
    { cancelable: false }
  );
}

export function sortByTime(array) {
  console.log(
    " CHECK SORT VALUE ",
    array.sort((a, b) => {
      return new Date(b.time) - new Date(a.time);
    })
  );
  return array.sort((a, b) => {
    return new Date("1970/01/01 " + a.time) - new Date("1970/01/01 " + b.time);
  });
}

export function setUrl(url) {
  var pattern = /^((http|https|ftp):\/\/)/;
  if (!pattern.test(url)) {
    url = "http://" + url;
  }
  return url;
}

export function showConfirmationDialog(msg, onYesClick) {
  Alert.alert(
    "Gig2Gig",
    msg,
    [
      {
        text: "OK",
        onPress: () => {
          console.log(" LOGOUT YES CLICK ");
          onYesClick();
        },
      },
      {
        text: "Cancel",
        style: "cancel",
      },
    ],
    { cancelable: false }
  );
}

export async function getThumbnail(uri) {
  try {
    const response = await ImageResizer.createResizedImage(
      uri,
      200,
      200,
      "PNG",
      50
    );
    console.log("response :", response);
    return response;
  } catch (error) {
    console.log("error thumbnail :", error);
    return "";
  }

  // ImageResizer.createResizedImage(image.uri, 250, 250, "PNG", 100).then((response) => {
  //   // response.uri is the URI of the new image that can now be displayed, uploaded...
  //   // response.path is the path of the new image
  //   // response.name is the name of the new image with the extension
  //   // response.size is the size of the new image
  //   console.log('response :', response);
  // }).catch((err) => {
  //   console.log('err 111 :', err);
  //   // Oops, something went wrong. Check that the filename is correct and
  //   // inspect err to get more details.
  // });
}

// export const openPlayer = videoId => {
//   Platform.OS === "ios"
//     ? YouTubeStandaloneIOS.playVideo(videoId)
//     : YouTubeStandaloneAndroid.playVideo({
//       apiKey: "AIzaSyDc8IqTh0xqtg87gvYXjiaWDIpPbqo6rGY",
//       videoId,
//       autoplay: true
//     });
// };
