import firebase from 'react-native-firebase'

const requestDeviceToken = async () => {
  const granted = await firebase.messaging().hasPermission();
  if (granted) {
    return await firebase.messaging().getToken();
  }
  await firebase.messaging().requestPermission();
}

export default requestDeviceToken;