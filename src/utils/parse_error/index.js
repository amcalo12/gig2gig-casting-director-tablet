
const parseError = (error) => {
	const responseError = error.response;
	if (responseError && responseError.data) {
		console.log(responseError);
		if ('error' in responseError.data) {
			return responseError.data.error
		}
		else if ('errors' in responseError.data) {
			return responseError.data.errors.email[0];
		}
		else if ('message' in responseError.data) {
			return responseError.data.message
		}
		else if ('data' in responseError.data) {
			return responseError.data.data
		}
	}
	return '';
}

export default parseError;