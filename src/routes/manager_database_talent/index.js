import { createStackNavigator } from 'react-navigation';

//screens
import ManagerTalentDatabase from '../../screens/talent_database/manager_talent_database';
import TalentDatabase from '../../screens/talent_database';
import VideoPlayerOpen	from "../../screens/auditions/manage/videoPlayerOpen";

const ManagerTalentDatabaseStack = createStackNavigator(
  {
    TalentDatabase,
    ManagerTalentDatabase,
    VideoPlayerOpen
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    initialRouteName: 'TalentDatabase'
  }
);

export default ManagerTalentDatabaseStack;