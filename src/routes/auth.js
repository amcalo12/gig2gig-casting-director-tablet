import { createAppContainer, createStackNavigator } from 'react-navigation';

// screens
import Login from '../screens/login';
import ForgotPassword from '../screens/forgot_password';
import SignUp from '../screens/sign_up';
import SliderImage from "../screens/TourScreen/sliderimage";

const authStackNavigator = createStackNavigator(
  {
    Login: {
      screen: Login
    },
    ForgotPassword: {
      screen: ForgotPassword
    },
    SignUp: {
      screen: SignUp
    },
    SliderImage: {
      screen: SliderImage
    }
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
      header: null
    }
  }
);

const authNavigator = createAppContainer(authStackNavigator);

export default authNavigator;
