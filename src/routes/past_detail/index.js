import { createStackNavigator } from 'react-navigation';

//screens
import PastDetail from '../../screens/auditions/past_detail';

const pastDetailStack = createStackNavigator(
  {
    PastDetail
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    initialRouteName: 'PastDetail'
  }
);

export default pastDetailStack;