import { createStackNavigator } from 'react-navigation';

//screens
import ContributorAuditionDetail from '../../screens/auditions/contributor';
import ContributorPartakerDetail from '../../screens/auditions/contributor/partaker_detail';

const contributorStack = createStackNavigator(
  {
    ContributorAuditionDetail,
    ContributorPartakerDetail
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    initialRouteName: 'ContributorAuditionDetail'
  }
);

export default contributorStack;