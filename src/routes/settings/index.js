import { createStackNavigator } from "react-navigation";

//screens
import Settings from "../../screens/settings";
import Profile from "../../screens/settings/profile";
import TermsOfUse from "../../screens/settings/terms_of_use";
import PrivacyPolicy from "../../screens/settings/privacy_policy";
import ContactUs from "../../screens/settings/contact_us";
import AppHelp from "../../screens/settings/app_help";
import Help from "../../screens/settings/help";
import Push from "../../screens/settings/push";
import QrCode from "../../screens/settings/qr_code";
import InstanceFeedback from "../../screens/settings/instancefeedbackscreens/instanceFeedback";
import DefaultFeedbackMessages from "../../screens/settings/defaultfeedbacks/index";
import Subscription from "../../screens/settings/subscription/index";
import SubscriptionPlans from "../../screens/settings/subscription_plans/index";
import InviteUsers from "../../screens/settings/invite_users/index";
import SelectTeamAdmin from "../../screens/settings/select_team_admin/index";
const settingsStack = createStackNavigator(
  {
    Settings,
    Profile,
    TermsOfUse,
    PrivacyPolicy,
    ContactUs,
    AppHelp,
    Help,
    QrCode,
    Push,
    InstanceFeedback,
    DefaultFeedbackMessages,
    Subscription,
    SubscriptionPlans,
    InviteUsers,
    SelectTeamAdmin,
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
    initialRouteName: "Settings",
  }
);

export default settingsStack;
