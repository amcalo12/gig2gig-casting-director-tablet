import SelectTeamAdmin from "../screens/settings/select_team_admin";
import { createAppContainer, createStackNavigator } from "react-navigation";

const SelectTeamAdminNavigator = createStackNavigator(
  {
    SelectTeamAdmin,
  },
  {
    initialRouteName: "SelectTeamAdmin",
    defaultNavigationOptions: {
      header: null,
    },
  }
);

export const SelectAdminNavigator = createAppContainer(
  SelectTeamAdminNavigator
);
