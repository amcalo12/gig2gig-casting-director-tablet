import { createStackNavigator } from 'react-navigation';

//screens
import AuditionDetail from '../../screens/auditions/detail';

const auditionsDetailStack = createStackNavigator(
  {
    AuditionDetail
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    initialRouteName: 'AuditionDetail'
  }
);

export default auditionsDetailStack;