import { createStackNavigator } from "react-navigation";

//screens
import ManualCheckInMode from "../../screens/auditions/manage/manual_checkin";

const manualCheckInModeStack = createStackNavigator(
  {
    ManualCheckInMode,
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
    initialRouteName: "ManualCheckInMode",
  }
);

export default manualCheckInModeStack;
