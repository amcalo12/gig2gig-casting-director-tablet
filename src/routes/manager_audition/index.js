import {
	createStackNavigator
} from 'react-navigation';

//screens
import ManagerAuditionDetail from '../../screens/auditions/manage';
import ManagerPartakerDetail from '../../screens/auditions/manage/partaker_detail';
import ManagerTalentDatabase from '../../screens/talent_database/manager_talent_database';
import HiddenPerformers from '../../screens/auditions/manage/hidden_performers/index';
import Appointments from '../../screens/auditions/manage/appointments/index';
import ManagerRounds from '../../screens/auditions/manage/rounds';
import videoList from "../../screens/auditions/manage/videoList";
import VideoPlayerOpen from "../../screens/auditions/manage/videoPlayerOpen";
import PushNotification from '../../screens/auditions/manage/push_notification/index';

const managerStack = createStackNavigator({
	ManagerAuditionDetail,
	ManagerPartakerDetail,
	ManagerTalentDatabase,
	ManagerRounds,
	videoList,
	VideoPlayerOpen,
	HiddenPerformers,
	PushNotification,
	Appointments
}, {
	defaultNavigationOptions: {
		header: null,
		gesturesEnabled: false
	},
	initialRouteName: 'ManagerAuditionDetail' //'ManagerAuditionDetail'
});

export default managerStack;