import { createAppContainer, createStackNavigator } from "react-navigation";

//routes
import HomeStack from "./home";
import AuditionDetailStack from "./audition_detail";
import pastDetailStack from "./past_detail";
import ContributorStack from "./contributor_audition";
import ManagerStack from "./manager_audition";
import ManagerCheckinModeStack from "./manager_checkin_mode";
import ManualCheckinModeStack from "./manual_checkin_mode";
import ManagerMonitorModeStack from "./manager_monitor_mode";
import ManagerTalentDatabaseStack from "./manager_database_talent";
import CreateAuditionStack from "./create_audition";
import SettingsStack from "./settings";
import EditAuditionStack from "./edit_audition";
import { SelectAdminNavigator } from "./select_admin";

const appDrawerNavigator = createStackNavigator(
  {
    // SelectAdmin: {
    //   screen: SelectAdminNavigator,
    // },
    Home: {
      screen: HomeStack,
    },
    AuditionDetail: {
      screen: AuditionDetailStack,
    },
    PastDetail: {
      screen: pastDetailStack,
    },
    ContributorAudition: {
      screen: ContributorStack,
    },
    ManagerAudition: {
      screen: ManagerStack,
    },
    ManagerCheckinMode: {
      screen: ManagerCheckinModeStack,
    },
    ManualCheckinMode: {
      screen: ManualCheckinModeStack,
    },
    ManagerMonitorMode: {
      screen: ManagerMonitorModeStack,
    },
    ManagerTalentDatabase: {
      screen: ManagerTalentDatabaseStack,
    },
    CreateAudition: {
      screen: CreateAuditionStack,
    },
    EditAudition: {
      screen: EditAuditionStack,
    },
    Settings: {
      screen: SettingsStack,
    },
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
    initialRouteName: "Home",
  }
);

const appNavigator = createAppContainer(appDrawerNavigator);

export default appNavigator;
