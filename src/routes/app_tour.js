import SliderImage from "../screens/TourScreen/sliderimage";
import { createAppContainer, createStackNavigator } from "react-navigation";

const AppTourNavigator1 = createStackNavigator(
  {
    SliderImage: {
      screen: SliderImage
    }
  },
  {
    initialRouteName: "SliderImage",
    defaultNavigationOptions: {
      header: null
    }
  }
);

export const AppTourNavigator = createAppContainer(AppTourNavigator1);

// export default appTourNavigator;
