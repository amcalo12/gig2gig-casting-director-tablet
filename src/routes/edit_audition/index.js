import { createStackNavigator } from 'react-navigation';

//screens
import EditAudition from '../../screens/auditions/edit';

const editAuditionStack = createStackNavigator(
  {
    EditAudition
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    initialRouteName: 'EditAudition'
  }
);

export default editAuditionStack;