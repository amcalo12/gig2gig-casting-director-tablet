import { createStackNavigator } from 'react-navigation';

//screens
import Notifications from '../../screens/notifications';

const NotificationsStack = createStackNavigator(
    {
        Notifications
    },
    {
        defaultNavigationOptions: {
            header: null,
            gesturesEnabled: false
        },
        initialRouteName: 'Notifications'
    }
);

export default NotificationsStack;