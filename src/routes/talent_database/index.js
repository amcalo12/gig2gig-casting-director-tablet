import { createStackNavigator } from 'react-navigation';

//screens
import TalentDatabase from '../../screens/talent_database';

const TalentDatabaseStack = createStackNavigator(
    {
        TalentDatabase
    },
    {
        defaultNavigationOptions: {
            header: null,
            gesturesEnabled: false
        },
        initialRouteName: 'TalentDatabase'
    }
);

export default TalentDatabaseStack;