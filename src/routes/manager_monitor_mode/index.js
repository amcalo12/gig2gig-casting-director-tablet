import { createStackNavigator } from 'react-navigation';

//screens
import ManagerMonitorMode from '../../screens/auditions/manage/monitor_mode';

const managerMonitorModeStack = createStackNavigator(
  {
    ManagerMonitorMode
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    initialRouteName: 'ManagerMonitorMode'
  }
);

export default managerMonitorModeStack;