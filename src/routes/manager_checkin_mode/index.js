import { createStackNavigator } from 'react-navigation';

//screens
import ManagerCheckinMode from '../../screens/auditions/manage/checkin_mode';
import ScanQr from '../../screens/auditions/manage/checkin_mode/scan_qr';
import ChooseAppointment from '../../screens/auditions/manage/checkin_mode/choose_appointment';
import CheckinSuccessfully from '../../screens/auditions/manage/checkin_mode/checkin_successfully';
import CheckinGuest from '../../screens/auditions/manage/checkin_mode/checkin_guest';

const managerCheckinModeStack = createStackNavigator(
  {
    ManagerCheckinMode,
    ScanQr,
    ChooseAppointment,
    CheckinSuccessfully,
    CheckinGuest
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    initialRouteName: 'ManagerCheckinMode'
  }
);

export default managerCheckinModeStack;