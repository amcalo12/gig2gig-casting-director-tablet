import { createStackNavigator } from 'react-navigation';

//screens
import CreateAudition from '../../screens/auditions/new';
import AddDocumentLink from '../../components/auditions/AddDocumentLink/AddDocumentLink';

const createAuditionStack = createStackNavigator(
  {
    CreateAudition,
    AddDocumentLink
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false
    },
    initialRouteName: 'CreateAudition'
  }
);

export default createAuditionStack;