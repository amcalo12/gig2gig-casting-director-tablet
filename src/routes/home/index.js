import { createDrawerNavigator } from "react-navigation";

//screens
import Home from "../../screens/home";
import PastAudition from "../../screens/past";

//sidebar
import Sidebar from "../../screens/sidebar";
import settingsStack from "../settings";
import createAuditionStack from "../create_audition";
import NotificationsStack from "../notifications";
import ManagerTalentDatabaseStack from "../manager_database_talent";
import newsStack from "../news";

const homeStack = createDrawerNavigator(
  {
    Home: {
      screen: Home,
    },
    PastAudition: {
      screen: PastAudition,
    },
    CreateAudition: {
      screen: createAuditionStack,
    },
    /*TalentDatabase: {
      screen: talentDatabaseStack
    },*/
    ManagerTalentDatabase: {
      screen: ManagerTalentDatabaseStack,
    },
    NewsAndUpdates: {
      screen: newsStack,
    },
    Notifications: {
      screen: NotificationsStack,
    },
    Settings: {
      screen: settingsStack,
    },
  },
  {
    contentComponent: Sidebar,
    initialRouteName: "Home",
  }
);

export default homeStack;
