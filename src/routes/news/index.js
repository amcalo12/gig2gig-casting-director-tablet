import { createStackNavigator } from 'react-navigation';

//screens
import News from '../../screens/news';
import NewsDetails from '../../screens/news/details';

const NewsStack = createStackNavigator(
    {
        News,
        NewsDetails

    },
    {
        defaultNavigationOptions: {
            header: null,
            gesturesEnabled: false
        },
        initialRouteName: 'News'
    }
);

export default NewsStack;