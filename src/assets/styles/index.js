import { Dimensions, Platform } from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';

let realHeight;
let realWidth;
let status_bar;
let dimensions = Dimensions.get('window');

if (Platform.OS == 'ios') {
  realHeight = dimensions.height;
  realWidth = dimensions.width
} else {
  status_bar = ExtraDimensions.get('STATUS_BAR_HEIGHT');
  SOFT_MENU_BAR_HEIGHT = ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT');
  realHeight = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - SOFT_MENU_BAR_HEIGHT;
  realWidth = ExtraDimensions.get('REAL_WINDOW_WIDTH')
}


export const width = realWidth;
export const height = realHeight
export const STATUS_BAR_HEIGHT = status_bar;

export const gradient = [
  '#4d2545',
  '#782541'
];
export const gradientLight = [
  '#FFF',
  '#FFF'
];

export const NEXABOLD = 'NexaBold';
export const NEXALIGHT = 'NexaLight';
export const BREAK_BOLD = 'Break-Bold';
export const BREAK_EXTRALIGHT = 'Break-Extralight';
export const BREAK_LIGHT = 'Break-Light';
export const BREAK_SEMIBOLD = 'Break-Semibold';
export const BREAK_REGULAR = 'Break';

export const MAIN_COLOR = '#4d2545';

export const resize = (size, type = 'width') => {
  const widthDesign = 1112
  const heightDesign = 834

  const currentSize = type === 'width' ? widthDesign : heightDesign
  const diviceSize = type === 'width' ? width : height
  const percent = (size * 100) / currentSize
  const percentJS = percent / 100
  return diviceSize * percentJS

}