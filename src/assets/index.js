export const Assets = {
  unselected: require("./icons/auditions/selected.png"),
  selected: require("./icons/auditions/selected_hover.png"),
  record: require("./icons/commons/record.png"),
  plus: require("./icons/commons/add.png"),
  search: require("./icons/search/icon-search.png"),
  close: require("./icons/commons/clear.png"),
  image1: require("./images/slider/1.png"),
  image2: require("./images/slider/2.png"),
  image3: require("./images/slider/3.png"),
  image4: require("./images/slider/4.png"),
  image5: require("./images/slider/5.png"),
  // qr_new: require("./images/slider/qr_new.png")
  qr_new: require("./images/commons/qr_new.jpg"),
  restore: require("./images/auditions/refresh.png"),
  fb: require("./images/commons/fb_link.png"),
  instagram: require("./images/commons/instagram_link.png"),
  twitter: require("./images/commons/twitter_link.png"),
  linkedin: require("./images/commons/linkedin_link.png"),
  grid: require("./images/auditions/grid.png"),
  list: require("./images/auditions/list.png"),
  rename: require("./images/auditions/edit.png"),
  add: require("./icons/settings/add.png"),
  cancel: require("./icons/settings/close.png"),
  role: require("./icons/auditions/role-icon.png"),
  saveLater: require("./images/auditions/time.png"),
  camera: require("./images/auditions/camera_icon.png"),
  chat: require("./images/auditions/chat_icon.png"),
  share: require("./images/auditions/share_icon.png"),
  star: require("./icons/auditions/header-star-icon.png"),
  starSelected: require("./icons/auditions/header-favorite-icon.png"),
  mail: require("./images/auditions/mail_icon.png"),
  resume: require("./images/auditions/resume_icon.png"),
  send: require("./images/auditions/send.png"),
  mp4: require("./images/myMedia/mp4-icon.png"),
  mp3: require("./images/myMedia/mp3-icon.png"),
  photo: require("./images/myMedia/photos-icon.png"),
  document: require("./images/myMedia/documents-icon.png"),
  sheetIcon: require("./images/myMedia/sheet-icon.png"),
  urlIcon: require("./images/myMedia/link.png"),
  pointIcon: require("./images/myMedia/points.png"),
  play: require("./images/myMedia/playBtn.png"),
  thumbVideo: require("./images/myMedia/video_thumbnail.png"),

  // import playBtn from "resources/images/playBtn.png";


  //   import IconStar from '../../../assets/icons/auditions/header-star-icon.png';
  // import IconFavorite from '../../../assets/icons/auditions/header-favorite-icon.png';

};
