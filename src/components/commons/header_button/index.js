import React from 'react';
import { TouchableOpacity, Image } from 'react-native';

//custom
import styles from './styles';

HeaderButton = (props) => {
	return (
		<TouchableOpacity
			onPress={props.onPress}
			style={props.containerStyle}
		>
			<Image
				source={props.icon}
				style={styles.icon}
			/>
		</TouchableOpacity>
	);
}

HeaderButton.defaultProps = {
	onPress: () => { },
	icon: null,
	containerStyle: styles.iconContainer
}

export default HeaderButton;