import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	iconContainer: {
		width: '30%',
		height: '100%',
		alignItems: 'center',
	},
});

export default styles;