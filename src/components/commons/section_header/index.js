import React from 'react';
import { View, Text } from 'react-native'

//custom
import styles from './styles';

export default function SectionHeader(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        {props.title}
      </Text>
    </View>
  );
}

SectionHeader.defaultProps = {
  title: ''
}