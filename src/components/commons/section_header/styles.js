import { StyleSheet } from 'react-native';

//custom
import { width, NEXALIGHT, MAIN_COLOR, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {    
    width,    
    height: resize(70),
    justifyContent: 'center',
    alignContent: 'center',
  },
  title: {    
    paddingVertical: resize(13, 'height'),
    fontSize: resize(26),
    marginLeft: resize(5),
    color: MAIN_COLOR,
    fontFamily: NEXALIGHT,
  }
});

export default styles;