import { StyleSheet } from 'react-native';

//custom
import { NEXALIGHT, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  title: {
    color: '#ffffff',
    fontFamily: NEXALIGHT,
    fontSize: resize(28),
    fontWeight: '400',
    textAlign: 'center',
    paddingVertical: resize(5, 'height'),
  }
});

export default styles;