import React from 'react';
import { View, Text } from 'react-native';

//custom
import styles from './styles';

export default function HeaderText(props) {
  return (
    <View style={styles.container}>
      <Text
        style={styles.title}
        numberOfLines={2}
      >
        {props.title}
      </Text>
    </View>
  );
}

HeaderText.defaultProps = {
  title: ''
}