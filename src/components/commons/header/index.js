import React from 'react';
import { View } from 'react-native';

// custom
import styles from './styles';
import { gradient } from '../../../assets/styles';
import LinearGradient from 'react-native-linear-gradient';

export default function Header(props) {
  return (
    <LinearGradient
      style={props.style}
      colors={props.colors}
    >
      <View style={styles.left}>
        {props.left}
      </View>
      <View style={styles.center}>
        {props.center}
      </View>
      <View style={styles.right}>
        {props.right}
      </View>
    </LinearGradient>
  );
}

Header.defaultProps = {
  colors: gradient,
  style: styles.container
}