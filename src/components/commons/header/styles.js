import { StyleSheet, Platform } from 'react-native';

//custom
import { width, resize, STATUS_BAR_HEIGHT } from '../../../assets/styles';

const headerHeight = Platform.OS === 'ios' ? resize((82 + 20), 'height') : resize((82 + STATUS_BAR_HEIGHT), 'height');

const styles = StyleSheet.create({
	container: {
		width,
		height: headerHeight,
		flexDirection: 'row',
		alignItems: 'center',
		paddingTop: resize(20, 'height'),
	},
	left: {
		flex: 1,
		justifyContent: 'flex-start',
		paddingVertical: resize(6, 'height'),
	},
	center: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: resize(6, 'height'),
	},
	right: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
		paddingVertical: resize(6, 'height'),
	}
});

export default styles;