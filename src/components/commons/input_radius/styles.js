import { StyleSheet } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(290),
    height: resize(55, 'height'),
    flexDirection: 'row',
    marginBottom: resize(17, 'height'),
  },
  input: {
    padding: 0,
    margin: 0,
    backgroundColor: '#ffffff',
    borderRadius: 50,
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(19),
    fontWeight: '400',
    paddingLeft: resize(20),
    width: resize(252),
    height: resize(55, 'height'),
  },
  iconContainer: {
    position: 'absolute',
    right: resize(18),
    top: resize(16, 'height'),
    zIndex: 2
  },
  icon: {
    width: resize(18),
    height: resize(19, 'height'),
    resizeMode: 'contain'
  }
});

export default styles;