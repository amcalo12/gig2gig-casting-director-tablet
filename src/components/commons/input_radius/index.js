import React from 'react';
import { View, Image } from 'react-native';
import { Input } from 'native-base';

// custom
import styles from './styles';
import LocationIcon from '../../../assets/icons/auditions/location-icon.png';

export default function InputRadius(props) {
  return (
    <View style={props.containerStyle}>
      <Input
        style={props.style}
        placeholder={props.placeholder}
        placeholderTextColor={props.placeholderTextColor}
        value={props.value}
        onChangeText={(value) => props.onChange(value)}
        keyboardType={props.keyboardType}
        autoCapitalize='none'
        {...props}
      />
      {
        props.withIcon && (
          <View style={styles.iconContainer}>
            <Image
              source={props.icon}
              style={styles.icon}
            />
          </View>
        )
      }
    </View>
  );
}

InputRadius.defaultProps = {
  value: '',
  placeholder: '',
  placeholderTextColor: styles.input.color,
  style: styles.input,
  onChange: () => { },
  keyboardType: 'default',
  containerStyle: styles.container,
  withIcon: false,
  icon: LocationIcon
}
