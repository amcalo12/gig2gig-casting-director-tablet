import React from 'react';
import { Text } from 'native-base';
import { View, Image, TouchableOpacity } from 'react-native';
import moment from 'moment';

//custom
import styles from './styles';
import { gradient } from '../../../assets/styles';
import LinearGradient from 'react-native-linear-gradient';
import backgroundCard from '../../../assets/images/background_card/background_card.png';

export default function AuditionCard(props) {
	return (
		<View style={styles.cardContainer}>
			<TouchableOpacity
				style={props.style}
				onPress={props.onPress}
			>
				<View>
					<View style={styles.cardImage}>
						<Image
							style={styles.image}
							source={props.image}
							resizeMode={"cover"}
						/>
					</View>
					<View style={styles.cardDescription}>
						<Text
							style={[styles.text, styles.title]}
							numberOfLines={1}
						>
							{props.title}
						</Text>
						{props.online === 0
							? <Text style={styles.text}>
								{moment(props.date).format('LL')}
							</Text>
							: null
						}
					</View>
					{
						props.showAdminTag && (
							<View style={styles.adminTag}>
								<View style={styles.adminTagContainer}>
									<Text style={styles.adminTagTitle}>
										Admin
                </Text>
								</View>
							</View>
						)
					}
				</View>
			</TouchableOpacity>
			{
				props.showContributorTag && (
					<TouchableOpacity
						style={styles.contributorTag}
						onPress={props.onPressContributorTag}
					>
						<View style={styles.contributorTagContainer}>
							<Text style={styles.contributorTagTitle}>
								Contributor
                </Text>
						</View>
					</TouchableOpacity>
				)
			}
			{
				props.showManageTag && (
					<LinearGradient
						style={styles.manageTag}
						colors={gradient}
					>
						<TouchableOpacity
							style={styles.manageTagGradient}
							onPress={props.onPressManageTag}
						>
							<Text style={styles.manageTagDescription}>
								Manage
                </Text>
						</TouchableOpacity>
					</LinearGradient>
				)
			}
		</View>
	);
}

AuditionCard.defaultProps = {
	title: '',
	date: new Date(),
	onPress: () => { },
	onPressManageTag: () => { },
	onPressContributorTag: () => { },
	showAdminTag: false,
	showContributorTag: false,
	showManageTag: false,
	style: styles.card,
	image: backgroundCard
}