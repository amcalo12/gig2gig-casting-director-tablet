import { StyleSheet } from 'react-native';

//custom
import { MAIN_COLOR, NEXABOLD, NEXALIGHT, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  cardContainer: {
    marginRight: resize(49),
    marginLeft: resize(5),
    paddingVertical: resize(12, 'height'),
    paddingHorizontal: resize(12),
  },
  card: {
    width: resize(328),
    height: resize(245, 'height'),
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#d6d6d6',
  },
  cardImage: {
    width: '100%',
    height: resize(188, 'height'),
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    overflow: 'hidden'
  },
  image: {
    width: '100%',
    // height: '100%',
    // width: undefined,
    height: undefined,
    aspectRatio: 1,
    // resizeMode: 'cover',
  },
  cardDescription: {
    width: '70%',
    height: resize(57, 'height'),
    justifyContent: 'center',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  text: {
    marginLeft: resize(16),
    color: MAIN_COLOR
  },
  title: {
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    paddingBottom: resize(1, 'height')
  },
  subtile: {
    fontFamily: NEXALIGHT,
    fontSize: resize(14)
  },
  adminTag: {
    flex: 1,
    position: 'absolute',
    right: 0,
    top: 0,
    height: resize(47, 'height'),
    width: resize(122),
    backgroundColor: '#fff',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 18,
    borderBottomRightRadius: 4,
    zIndex: 3
  },
  adminTagContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  adminTagTitle: {
    color: MAIN_COLOR,
    fontFamily: NEXABOLD,
    fontSize: resize(12)
  },
  contributorTag: {
    flex: 1,
    position: 'absolute',
    right: 0,
    top: 0,
    height: resize(47, 'height'),
    width: resize(122),
    backgroundColor: '#fff',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 18,
    borderBottomRightRadius: 4,
    zIndex: 3
  },
  contributorTagContainer: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contributorTagTitle: {
    color: MAIN_COLOR,
    fontFamily: NEXABOLD,
    fontSize: resize(12)
  },
  manageTag: {
    flex: 1,
    width: resize(122),
    height: resize(47, 'height'),
    right: 0,
    bottom: 0,
    position: 'absolute',
    borderTopLeftRadius: 18,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    zIndex: 3
  },
  manageTagGradient: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  manageTagDescription: {
    fontSize: resize(12),
    color: '#ffffff',
    fontFamily: NEXABOLD,
  }
});

export default styles;