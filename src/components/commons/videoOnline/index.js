import React from 'react';
import { View, Image, ImageBackground, TouchableOpacity, Text } from 'react-native';
import styles from './styles';

//Images
import Play from '../../../assets/images/commons/playBtn.png';


const VideoOnline = ({ videoName, hasButtom, onPress, thumbnail }) => (
	<View style={styles.container}>
		{console.log('videoName :', videoName)}
		<ImageBackground resizeMod='cover' source={{ uri: thumbnail }} style={styles.video}>
			<TouchableOpacity onPress={onPress} style={styles.playVideoIndicator}>
				<Image resizeMode='contain' source={Play} />
			</TouchableOpacity>
		</ImageBackground>
		{hasButtom
			? <View style={styles.textContainer}>
				<Text numberOfLines={1} style={styles.textName}>{videoName}</Text>
			</View>
			: null
		}
	</View>
)

VideoOnline.defaultProps = {
	hasButtom: false
}

export default VideoOnline;
