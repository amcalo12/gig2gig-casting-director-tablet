import React, { Component } from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import UnionTag from '../../auditions/union_tag';
import GradientButton from '../gradient_button';
import en from '../../../locale/en';

let unions = [
	{ name: 'ANY', value: 2 },
	{ name: 'UNION', value: 1 },
	{ name: 'NONUNION', value: 0 }
];
let genders = [
	{ name: 'ANY', value: 'ANY' },
	{ name: 'MALE', value: 'male' },
	{ name: 'FEMALE', value: 'female' }
];

FilterOptions = ({ union, gender, handleState, onSearch }) => (
	<View style={styles.container}>
		<Text style={styles.infoText}>Union Status</Text>
		<View style={styles.selector}>
			{unions.map((tag, index) => {
				return (
					<UnionTag
						key={index}
						name={tag.name}
						selected={union === tag.value}
						onPress={() => handleState('union', tag.value)}
					/>
				);
			})}
		</View>

		<Text style={styles.infoText}>Gender</Text>
		<View style={styles.selector}>
			{genders.map((tag, index) => {
				return (
					<UnionTag
						key={index}
						name={tag.name}
						selected={gender === tag.value}
						onPress={() => handleState('gender', tag.value)}
					/>
				);
			})}
		</View>

		<View style={styles.btnButton}>
			<GradientButton
				buttonText={en.search_button_text}
				onPress={() => onSearch()}
				containerStyle={styles.buttonSaveContainer}
			/>
		</View>

	</View>
)

export default FilterOptions;
