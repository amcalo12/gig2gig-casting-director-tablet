import { StyleSheet } from 'react-native';
import { resize, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderBottomRightRadius: resize(25, 'height'),
        borderBottomLeftRadius: resize(25, 'height'),
        paddingHorizontal: resize(20)
    },
    selector: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    infoText: {
        fontSize: resize(16),
        fontFamily: NEXALIGHT,
        color: '#4d2545',
        marginVertical: resize(10, 'height')
    },
    btnButton: {
        width: '95%',
        alignSelf: 'center',
        flex: 0.9,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    buttonSaveContainer: {
        width: '100%',
        height: resize(55, 'height'),
    }
})
export default styles;
