import { StyleSheet, Platform } from 'react-native';
import { NEXALIGHT, resize } from '../../../assets/styles';
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
	container: {
		flexDirection: 'row',
		justifyContent: "space-between",
		alignItems: 'center',
		paddingVertical: isIos ? resize(20, "height") : resize(15, "height"),
		marginBottom: isIos ? resize(15, "height") : resize(10, "height"),
	},
	text: {
		color: '#4D2545',
		fontFamily: NEXALIGHT,
		fontSize: 20,
		letterSpacing: 1,
	},
	switchSettings: {
		backgroundColor: "#e0e6e6",
		color: "#FFFFFF"
	},
	switchSettingsActive: {
		backgroundColor: '#4D2545',
		color: "#FFFFFF"
	},
	containerStyle: {
		width: resize(75),
		height: resize(35, "height"),
		borderRadius: 25,
		padding: 5,
		marginRight: resize(20),
	},
	circleStyle: {
		width: resize(28, "height"),
		height: resize(28, "height"),
		borderRadius: 19,
	}
});