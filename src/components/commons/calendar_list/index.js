import React from 'react';
import { CalendarList, LocaleConfig } from 'react-native-calendars';
import moment from 'moment';

//custom
import styles from './styles'

LocaleConfig.locales['en'] = {
	monthNames: months(),
	monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
	dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
	dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
};

LocaleConfig.defaultLocale = 'en';

export default function Calendar(props) {
	return (
		<CalendarList
			theme={styles(props)}
			current={props.startDate}
			markedDates={allRange(props.dates)}
			// markingType={'period'}
			markingType={'custom'}
			pastScrollRange={0}
			futureScrollRange={calculateFutureScrollRange(props.startDate, props.endDate)}
			scrollsToTop={false}
			removeClippedSubviews={false}
			{...props}
		/>
	);
}

Calendar.defaultProps = {
	headerMonthPosition: 'flex-start',
	ranges: [],
	calendarWidth: 100,
	startDate: moment().format('YYYY-MM-DD'),
	endDate: moment().add(2, 'month').format('YYYY-MM-DD'),
	dates: []
}

function months() {
	return Array.from({ length: 12 }).map((_, i) =>
		moment()
			.set('month', i)
			.format('MMMM')
	);
}

function calculateFutureScrollRange(startDate, lastDate) {
	const startDay = moment(startDate, 'YYYY-MM-DD')
	const endDay = moment(lastDate, 'YYYY-MM-DD')
	const diffDays = endDay.diff(startDay, 'months', false)

	return diffDays + 1
}

function allRange(dates) {
	let ranges = {};

	dates.map((date) => {
		let range = getRange(date.start_date, date.end_date);

		return Object.assign(ranges, range)
	});

	return ranges;
}

function getRange(startDate, lastDate) {
	let allDates = {};
	const startDay = moment(startDate, 'YYYY-MM-DD')
	const endDay = moment(lastDate, 'YYYY-MM-DD')
	const diffDays = endDay.diff(startDay, 'day', false)

	if (diffDays === 0) {
		const endDate = endDay.format('YYYY-MM-DD')
		allDates[endDate] = {
			marked: true,
			// color: '#782541',
			// textColor: 'white',
			startingDay: true,
			endingDay: true,
			customStyles: {
				container: {
					marginTop: 16,
					backgroundColor: "#D8893A",
					width: "100%",
					borderTopLeftRadius: 2,
					borderBottomLeftRadius: 2,
					height: 4,
					borderRadius: 0
				},
				text: {
					position: "absolute",
					marginTop: -9,
					color: "#4D2545"
				}
			}
		}
	}
	else {
		const firstDate = startDay.format('YYYY-MM-DD')
		const lastDayOfWeek = moment(firstDate, 'YYYY-MM-DD').endOf('weeks').format('YYYY-MM-DD');
		allDates[firstDate] = {
			marked: true,
			// color: '#782541',
			// textColor: 'white'
			customStyles: {
				container: {
					marginTop: 16,
					backgroundColor: "#D8893A",
					width: "100%",
					borderTopLeftRadius: 2,
					borderBottomLeftRadius: 2,
					height: 4,
					borderRadius: 0
				},
				text: {
					position: "absolute",
					marginTop: -9,
					color: "#4D2545"
				}
			}
		}

		if (lastDayOfWeek === firstDate) {
			allDates[firstDate].startingDay = true
			allDates[firstDate].endingDay = true
		} else {
			allDates[firstDate].startingDay = true
			allDates[firstDate].endingDay = false
		}

		Array.from({ length: diffDays }).map((_, i) => {
			let newDate = moment(startDate, 'YYYY-MM-DD').add(i + 1, 'days')
			const endOfWeek = moment(startDate, 'YYYY-MM-DD').add(i + 1, 'days').endOf('weeks').format('YYYY-MM-DD')
			const endOfMonth = moment(startDate, 'YYYY-MM-DD').add(i + 1, 'days').endOf('month').format('YYYY-MM-DD')
			const startOfMonth = moment(startDate, 'YYYY-MM-DD').add(i + 1, 'days').startOf('month').format('YYYY-MM-DD')
			const startOfWeek = moment(startDate, 'YYYY-MM-DD').add(i + 1, 'days').startOf('weeks').format('YYYY-MM-DD')
			const numberOfWeek = newDate.day() === 0;
			newDate = newDate.format('YYYY-MM-DD')
			allDates[newDate] = {
				marked: true,
				// color: '#782541',
				// textColor: 'white',
				customStyles: {
					container: {
						marginTop: 16,
						backgroundColor: "#D8893A",
						width: "100%",
						borderTopLeftRadius: 2,
						borderBottomLeftRadius: 2,
						height: 4,
						borderRadius: 0
					},
					text: {
						position: "absolute",
						marginTop: -9,
						color: "#4D2545"
					}
				}
			}
			if (endOfWeek === newDate) {
				allDates[newDate].endingDay = true
			}
			if (startOfWeek === newDate) {
				allDates[newDate].startingDay = true
			}
			if (newDate === endOfMonth) {
				allDates[newDate].startingDay = true
				allDates[newDate].endingDay = true
			}
			if (numberOfWeek) {
				allDates[newDate].startingDay = true
			} else {
				allDates[newDate].startingDay = false
			}
			if (newDate === startOfMonth) {
				allDates[newDate].startingDay = true
			}
		})
		const numberOfWeek = endDay.day() === 0;
		const endDate = endDay.format('YYYY-MM-DD')
		const endOfMonth = endDate === endDay.startOf('weeks').format('YYYY-MM-DD')
		allDates[endDate] = {
			marked: true,
			// textColor: 'white',
			// color: '#782541',
			endingDay: true,
			startingDay: (endOfMonth && numberOfWeek),
			customStyles: {
				container: {
					marginTop: 16,
					backgroundColor: "#D8893A",
					width: "100%",
					borderTopLeftRadius: 2,
					borderBottomLeftRadius: 2,
					height: 4,
					borderRadius: 0
				},
				text: {
					position: "absolute",
					marginTop: -9,
					color: "#4D2545"
				}
			}
		}
	}

	return allDates
}
