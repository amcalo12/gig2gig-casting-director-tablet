// custom
import { resize, NEXABOLD } from '../../../assets/styles/index'

const styles = (props) => ({
	monthTextColor: '#4d2545',
	textMonthFontFamily: NEXABOLD,
	textMonthFontSize: resize(12),
	'stylesheet.calendar.header': {
		header: {
			alignItems: props.headerMonthPosition
		},
		dayHeader: {
			color: '#4d2545',
			fontWeight: '500',
			fontSize: resize(10),
		}
	},
	'stylesheet.day.period': {
		text: {
			width: '100%',
			textAlign: 'center',
			fontSize: resize(8),
			fontFamily: NEXABOLD,
		},
		base: {
			height: 34,
			alignItems: 'center',
			justifyContent: 'center',
			width: resize(37),
		}
	}
})

export default styles