import { StyleSheet } from 'react-native';
import { resize } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		height: resize(50, 'height'),
		backgroundColor: 'white',
		borderRadius: resize(25, 'height'),
		marginRight: resize(15),
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	containerSub: {
		height: '100%',
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingHorizontal: resize(20),
	},
	input: {
		flex: 6,
		height: '100%'
	},
	icon1: {
		flex: 1,
		alignItems: 'flex-start',
		height: '100%',
		justifyContent: 'center',
	},
	icon2: {
		flex: 1,
		height: '100%',
		justifyContent: 'center',
		alignItems: 'flex-end',
	},
	iconAlong: {
		width: resize(50),
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	icons: {
		flexDirection: 'row',
		width: resize(150),
		justifyContent: 'flex-end',
		marginRight: resize(25),
		height: resize(50, 'height'),
	},
	iconsSpand: {
		flexDirection: 'row',
		alignItems: 'center',
		marginRight: resize(25),
		height: resize(50, 'height'),
	}
})
export default styles;
