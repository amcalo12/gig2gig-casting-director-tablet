import React, { Component } from 'react';
import { View, Animated, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import styles from './styles';

//Custom
import { resize } from '../../../assets/styles';

//Images
import ModifySetting from '../../../assets/icons/search/modifySetting.png';
import Search from '../../../assets/icons/search/icon-search.png';
import SearchWhite from '../../../assets/icons/search/search.png';
import Plus from '../../../assets/icons/auditions/plusWhite.png';
import Filter from '../../../assets/icons/news/filter.png';

class AnimatedSearch extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isExtend: false,
			viewSplitAnimatedValue: new Animated.Value(0),
			opacityValue: new Animated.Value(0)
		}
	}

	handleSearch = () => {
		let { isExtend } = this.state;
		if (!isExtend) {
			this.setState(prevState => ({
				isExtend: !prevState.isExtend
			}), () => {
				let { isExtend, viewSplitAnimatedValue, opacityValue } = this.state;
				if (isExtend) {
					Animated.timing(viewSplitAnimatedValue, {
						toValue: resize(300),
						duration: 700,
					}).start();
					Animated.timing(opacityValue, {
						toValue: 1,
						duration: 700,
					}).start();
				}
			})
		}
		else {
			let { viewSplitAnimatedValue, opacityValue } = this.state;
			Animated.timing(viewSplitAnimatedValue, {
				toValue: 0,
				duration: 700,
			}).start(() => {
				this.setState(prevState => ({
					isExtend: !prevState.isExtend
				}))
			});
			Animated.timing(opacityValue, {
				toValue: 0,
				duration: 700,
			}).start();
		}
	}

	render() {
		let { isExtend, viewSplitAnimatedValue, opacityValue } = this.state;
		let { showOverlay, handleSetRef, onChangeText, search, handleAdd } = this.props;
		const widthFirstContainer = viewSplitAnimatedValue.interpolate({
			inputRange: [0, resize(300)],
			outputRange: [0, resize(300)],
		});
		const opacityContainer = opacityValue.interpolate({
			inputRange: [0, 1],
			outputRange: [0, 1],
		});

		if (isExtend) {
			return (
				<View style={styles.iconsSpand}>
					<Animated.View style={[styles.container, { width: widthFirstContainer, opacity: opacityContainer }]}>
						<View renderToHardwareTextureAndroid={true} ref={fileRef => { handleSetRef(fileRef) }} style={styles.containerSub}>
							<TouchableOpacity onPress={this.handleSearch} style={styles.icon1}>
								<Image resizeMode='contain' source={Search} />
							</TouchableOpacity>
							<TextInput
								value={search}
								onChangeText={text => onChangeText(text)}
								placeholderTextColor='#4D2545'
								placeholder='Search'
								style={styles.input}
							/>
							<TouchableOpacity onPress={() => showOverlay()} style={styles.icon2}>
								<Image resizeMode='contain' source={ModifySetting} />
							</TouchableOpacity>
						</View>
					</Animated.View>
					{/* <TouchableOpacity onPress={this.handleSearch} style={styles.iconAlong}>
						<Image resizeMode='contain' source={Filter} />
					</TouchableOpacity> */}
					<TouchableOpacity onPress={this.handleAdd} style={styles.iconAlong}>
						<Image resizeMode='contain' source={Plus} />
					</TouchableOpacity>
				</View>
			)
		}
		return (
			<View style={styles.icons}>
				<TouchableOpacity onPress={this.handleSearch} style={styles.iconAlong}>
					<Image resizeMode='contain' source={SearchWhite} />
				</TouchableOpacity>
				{/* <TouchableOpacity onPress={this.handleSearch} style={styles.iconAlong}>
					<Image resizeMode='contain' source={Filter} />
				</TouchableOpacity> */}
				<TouchableOpacity onPress={() => handleAdd()} style={styles.iconAlong}>
					<Image resizeMode='contain' source={Plus} />
				</TouchableOpacity>
			</View>
		)
	}
}

export const SearchFixed = ({ showOverlay, onChangeText, search }) => (
	<Animated.View style={[styles.container, { width: '100%' }]}>
		<View style={styles.containerSub}>
			<TouchableOpacity onPress={this.handleSearch} style={styles.icon1}>
				<Image resizeMode='contain' source={Search} />
			</TouchableOpacity>
			<TextInput
				autoCapitalize='none'
				value={search}
				onChangeText={text => onChangeText(text)}
				placeholderTextColor='#4D2545'
				placeholder='Search'
				style={styles.input}
			/>
			<TouchableOpacity onPress={() => showOverlay()} style={styles.icon2}>
				<Image resizeMode='contain' source={ModifySetting} />
			</TouchableOpacity>
		</View>
	</Animated.View>
)

export default AnimatedSearch;
