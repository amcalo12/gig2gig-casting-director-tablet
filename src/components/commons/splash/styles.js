import { StyleSheet, Platform } from 'react-native';

//custom
import { width, height, NEXALIGHT, BREAK_REGULAR, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width,
    height,
  },
  logoContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: resize(145, 'height'),
  },
  title: {
    borderColor: 'rgba(0, 0, 0, 0)',
    borderStyle: 'solid',
    borderWidth: 2,
    color: '#ffffff',
    fontFamily: BREAK_REGULAR,
    fontSize: resize(92),
    ...Platform.select({
      android: {
        fontWeight: '400'
      },
      ios: {
        fontWeight: '600'
      }
    }),
    paddingBottom: 10
  },
  titleNumber: {
    color: '#ffffff',
    fontFamily: BREAK_REGULAR,
    fontSize: resize(101),
    paddingTop: resize(40, 'height'),
    ...Platform.select({
      android: {
        fontWeight: '400',
      },
      ios: {
        fontWeight: '700',
      }
    })
  },
  logo: {
    width: resize(200),
    height: resize(200, 'height'),
    resizeMode: 'contain'
  },
  subtitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  subtitle: {
    color: '#ffffff',
    fontFamily: NEXALIGHT,
    fontSize: resize(24),
    fontWeight: '400',
  }
});

export default styles;