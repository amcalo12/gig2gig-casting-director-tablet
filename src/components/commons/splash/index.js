import React from 'react';
import { View, ImageBackground, Text } from 'react-native';

//custom
import styles from './styles';
import LightBar from '../light_bar';
 
export default function SplashView() {
  return (
    <ImageBackground
      style={styles.container}
      source={require('../../../assets/images/commons/bg.png')}
    >
      <LightBar />
      <View style={styles.logoContainer}>
        <Text style={styles.title}>
          gig
        </Text>
        <Text style={styles.titleNumber}>
          2
        </Text>
        <Text style={styles.title}>
          gig
        </Text>
      </View>
      <View style={styles.subtitleContainer}>
        <Text style={styles.subtitle}>
          For Entertainers by Entertainers
        </Text>
      </View>
    </ImageBackground>
  );
}
