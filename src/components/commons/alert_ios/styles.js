import { StyleSheet } from 'react-native';

//custom
import { resize } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		width: '100%',
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(4, 4, 15, 0.4)'
	},
	dismiss: {
		width: '100%',
		height: '100%',
		position: 'absolute'
	},
	containerAlert: {
		width: resize(270),
		minHeight: resize(140, 'height'),
		shadowColor: 'rgba(0, 0, 0, 0.16)',
		shadowOffset: { width: 3, height: 0 },
		shadowRadius: 6,
		borderRadius: resize(14),
		backgroundColor: 'rgba(248, 248, 248, 0.82)',
		justifyContent: 'space-between'
	},
	title: {
		marginTop: resize(20),
		color: '#000000',
		fontSize: resize(17),
		fontWeight: '600',
		letterSpacing: resize(-0.01),
		lineHeight: resize(22),
		alignSelf: 'center'
	},
	actionBar: {
		flexDirection: 'row'
	},
	buttonDefault: {
		position: 'relative',
		height: resize(44),
		width: '50%',
		alignItems: 'center',
		justifyContent: 'center',
		borderTopWidth: 1,
		borderColor: '#3f3f3f'
	},
	buttonMargin: {
		borderLeftWidth: 1
	},
	cancelText: {
		color: '#f96666',
		fontSize: resize(17),
		fontWeight: '400',
		letterSpacing: resize(-0.01),
		lineHeight: resize(22),
	},
	accept: {
		color: '#007aff',
		fontSize: resize(17),
		fontWeight: '600',
		letterSpacing: resize(-0.01),
		lineHeight: resize(22),
	}
});

export default styles;