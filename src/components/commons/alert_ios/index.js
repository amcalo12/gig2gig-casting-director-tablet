import React from 'react'
import { View, Text, Modal, TouchableOpacity } from 'react-native';

//custom
import styles from './styles';

function AlertIOS(props) {
	return (
		<Modal
			animationType='fade'
			transparent={true}
			visible={props.show}
			onRequestClose={props.dismiss}
		>
			<View style={styles.container}>
				<TouchableOpacity
					onPress={props.dismiss}
					style={styles.dismiss}
				/>
				<View style={styles.containerAlert}>
					<View>
						<Text style={styles.title}>
							{props.title}
						</Text>
						{props.children}
					</View>
					<View style={styles.actionBar}>
						<TouchableOpacity
							style={styles.buttonDefault}
							onPress={props.dismiss}
						>
							<Text style={styles.cancelText}>
								{props.cancelTitle}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={props.agree}
							style={[styles.buttonDefault, styles.buttonMargin]}
						>
							<Text style={styles.accept}>
								{props.acceptTitle}
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		</Modal>
	)
}

AlertIOS.defaultProps = {
	dismiss: () => null,
	agree: () => null,
	show: true,
	title: 'default Title',
	cancelTitle: 'No',
	acceptTitle: 'Yes'
}

export default AlertIOS