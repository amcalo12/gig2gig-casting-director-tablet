import React, { Component } from "react";
import { View, Text, Image, StyleSheet, ActivityIndicator } from "react-native"
import colors from "../../../utils/colors";
import { DEVICE } from "../../../utils/constant";
import { resize } from "../../../assets/styles";


class ProgressLoader extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            this.props.isLoading ?
                <View style={styles.container}>
                    <View style={styles.containerOpac}></View>
                    <ActivityIndicator size="large" style={styles.spinner} color={colors.PRIMARY} />
                </View>
                : null
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        paddingVertical: resize(50),
        width: DEVICE.DEVICE_WIDTH,
        height: DEVICE.DEVICE_HEIGHT,
        zIndex: 997
    },
    containerOpac: {
        position: 'absolute',
        width: DEVICE.DEVICE_WIDTH,
        height: DEVICE.DEVICE_HEIGHT,
        backgroundColor: 'transparent',
        zIndex: 998
    },
    spinner: {
        flex: 1,
        alignSelf: 'center',
        zIndex: 999
    }
})

export default ProgressLoader;
