import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Spinner } from 'native-base';

// custom
import styles from './styles';
import { gradient, gradientLight } from '../../../assets/styles';

export default function GradientButton(props) {
  return (
    <TouchableOpacity
      style={props.containerStyle}
      onPress={props.onPress}
      disabled={props.disabled}
    >
      <LinearGradient
        colors={props.light ? gradientLight : props.colors}
        style={props.gradientStyle}
      >
        {
          props.loading && (
            <Spinner
              color={props.spinnerColor}
            />
          )
        }
        {
          !props.loading && (
            <Text style={props.textStyle}>
              {props.buttonText}
            </Text>
          )
        }
      </LinearGradient>
    </TouchableOpacity>
  );
}

GradientButton.defaultProps = {
  buttonText: '',
  colors: gradient,
  onPress: () => { },
  containerStyle: styles.container,
  gradientStyle: styles.button,
  textStyle: styles.buttonText,
  loading: false,
  spinnerColor: styles.spinner.color,
  disabled: false
}
