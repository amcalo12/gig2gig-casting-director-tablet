import { StyleSheet } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(290),
    height: resize(55, 'height'),
  },
  button: {
    width: '100%',
    height: resize(55, 'height'),
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.18)',
    shadowOffset: { width: 3, height: 0 },
    shadowRadius: 8,
    borderRadius: 50,
  },
  buttonText: {
    color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
  },
  spinner: {
    color: '#fff'
  }
});

export default styles;