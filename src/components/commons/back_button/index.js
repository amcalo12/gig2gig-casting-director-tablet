import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

//custom
import styles from './styles';

export default function BackButton(props) {
  return (
      <TouchableOpacity 
        onPress={props.onPress}        
        style={styles.backIcon}
      >
        <Icon
          name='left'
          type='AntDesign'
          style={styles.icon}
        />
      </TouchableOpacity>
  );
}

BackButton.defaultProps = {
  onPress: () => {}
}