import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  backIcon: {
    width: '30%',
    height: '100%',
    justifyContent: 'center',
    alignContent: 'center',
  },
  icon: {
    color: "#fff",
    alignSelf: 'center',
  }
});

export default styles;