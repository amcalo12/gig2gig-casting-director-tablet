import { StyleSheet } from 'react-native';

//custom 
import { NEXABOLD, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		marginRight: resize(34),
		width: resize(250),
		height: resize(43, 'height')
	},
	input: {
		width: resize(250),
		height: resize(43, 'height'),
		backgroundColor: '#fff',
		borderRadius: 22,
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(18),
		fontWeight: '400',
		paddingLeft: resize(48),
	},
	icon: {
		position: 'absolute',
		top: resize(10, 'height'),
		left: resize(15),
		zIndex: 3,
		width: resize(22),
		height: resize(22, 'height'),
	}
});

export default styles;