import React from 'react';
import { View, Image } from 'react-native';
import { Input } from 'native-base';

//custom
import styles from './style';
import searchIcon from '../../../assets/icons/search/icon-search.png';

export default function Search(props) {
  return (
    <View style={props.containerStyle}>
      <Image
        source={searchIcon}
        style={props.iconStyle}
      />
      <Input
        style={props.style}
        placeholder={props.placeholder}
        placeholderTextColor={props.placeholderColor}
        onChangeText={(value) => props.onChange(value)}
        value={props.value}
        autoCapitalize='none'
      />
    </View>
  );
}

Search.defaultProps = {
  onChange: () => { },
  value: '',
  placeholder: '',
  style: styles.input,
  placeholderColor: styles.input.color,
  iconStyle: styles.icon,
  containerStyle: styles.container
}