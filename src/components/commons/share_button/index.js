import React, { Component } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import styles from './styles';

//Images
import Share from '../../../assets/icons/commons/shareData.png'

const ShareButton = ({ onPress }) => (
    <TouchableOpacity onPress={onPress} style={styles.container}>
        <Image resizeMode='contain' source={Share} />
    </TouchableOpacity>
)
export default ShareButton;
