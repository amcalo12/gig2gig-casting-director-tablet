import { StyleSheet } from 'react-native';
import { resize } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		justifyContent: 'center',
		alignItems: 'center',
		width: resize(100),
		height: '100%'
	}
})
export default styles;
