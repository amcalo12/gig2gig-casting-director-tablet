import React from "react";
import { View, Text, Image } from "react-native";

import styles from "./styles";
// import GIGlogowhite from "../../../assets/logos/giglogo.xml";
// import bower from "../../../assets/logos/bower.xml";
function Logo(props) {
  return (
    <View style={styles.containerPrincipal}>
      <View style={props.containerStyle}>
        {/* <Text style={props.titleStyle}>gig</Text>
				<Text style={props.titleNumberStyle}>2</Text>
				<Text style={props.titleStyle}>gig</Text> */}
        <Image
          style={{
            height: 50,
            width: 150,
            resizeMode: "contain",
            justifyContent: "center",
            alignItems: "center",
            alignSelf: "center",
            marginTop: 60
          }}
          source={require("../../../assets/icons/logo/gig_logo_white.png")}
        ></Image>
      </View>
      {props.displayCasting ? (
        <View style={styles.container}>
          <Text style={styles.title2}> Casting </Text>
        </View>
      ) : null}
    </View>
  );
}

export function LogoHeader(props) {
  return (
    <View style={styles.container2}>
      <Text style={styles.title2}></Text>
      {/* <Text style={styles.titleNumber2}> 2</Text>
			<Text style={styles.title2}>gig</Text>   */}
      <Image
        style={{
          height: 50,
          width: 150,
          resizeMode: "contain",
          justifyContent: "center"
        }}
        source={require("../../../assets/icons/logo/gig_logo_white.png")}
      ></Image>
    </View>
  );
}

Logo.defaultProps = {
  titleStyle: styles.title,
  titleNumberStyle: styles.titleNumber,
  containerStyle: styles.container
};

export default Logo;
