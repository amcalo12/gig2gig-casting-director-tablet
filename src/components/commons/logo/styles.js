import { StyleSheet, Platform } from 'react-native';

//custom
import { BREAK_REGULAR, resize, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		height: '50%'
	},
	container2: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		height: '100%'
	},
	title: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(100),
		...Platform.select({
			android: {
				fontWeight: '400'
			},
			ios: {
				fontWeight: '600'
			}
		}),
		height: '100%'
	},
	title2: {
		color: '#ffffff',
		fontFamily: NEXALIGHT,
		fontSize: resize(50),
		...Platform.select({
			android: {
				fontWeight: '400'
			},
			ios: {
				fontWeight: '600'
			}
		}),
		height: '100%'
	},
	titleNumber: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(100),
		paddingTop: resize(20, 'height'),
		...Platform.select({
			android: {
				fontWeight: '400',
			},
			ios: {
				fontWeight: '700',
			}
		}),
		height: '100%'
	},
	titleNumber2: {
		color: '#ffffff',
		fontFamily: BREAK_REGULAR,
		fontSize: resize(50),
		paddingTop: resize(10, 'height'),
		...Platform.select({
			android: {
				fontWeight: '400',
			},
			ios: {
				fontWeight: '700',
			}
		}),
		height: '100%'
	},
	containerPrincipal: {
		width: '50%',
		height: resize(250, 'h'),
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center'
	}
});

export default styles;