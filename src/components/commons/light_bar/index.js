import React from 'react';
import { StatusBar } from 'react-native';

export default function LightBar() {
  return(
    <StatusBar 
      translucent
      backgroundColor='transparent'
      barStyle='light-content'
    />
  );
}