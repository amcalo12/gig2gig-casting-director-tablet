import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

//custom
import styles from './styles';

export default function PinKeyboard(props) {
  return (
    <View style={styles.container}>
      {
        props.numbers.map((number, index) => (
          <TouchableOpacity
            key={`number_${index + 1}`}
            style={styles.button}
            onPress={() => props.onPressDigit(
              RealNumberPadKey(index, props.numbers.length)
            )}
          >
            <Text style={styles.buttonText}>
              {
                RealNumberPadKey(index, props.numbers.length)
              }
            </Text>
          </TouchableOpacity>
        ))
      }
    </View>
  )
}

PinKeyboard.defaultProps = {
  numbers: Array.from({ length: 10 }),
  onPressDigit: () => null
}

function RealNumberPadKey(number, lastNumber) {
  let realNumber = number + 1
  if (realNumber === lastNumber) {
    realNumber = 0
  }
  return realNumber
}