import { StyleSheet } from 'react-native';

//custom
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(270),
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    paddingTop: resize(15, 'height'),
    paddingBottom: resize(30, 'height')
  },
  button: {
    width: `${100 / 3}%`,
    height: resize(60, 'height'),
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#000000',
    fontFamily: NEXABOLD,
    fontSize: resize(32),
    fontWeight: '400',
    letterSpacing: resize(1.6),
  }
});

export default styles;