import React from 'react';
import { TouchableOpacity, Text, View, Image } from 'react-native';
import { Spinner } from 'native-base';

//custom
import styles from './style';
import PicIcon from '../../../assets/icons/auditions/pic-icon.png';
import IconStar from '../../../assets/icons/auditions/header-star-icon.png';
import IconFavorite from '../../../assets/icons/auditions/header-favorite-icon.png';

export default function HeaderBorderedButton(props) {
	return (
		<View style={styles.container}>
			{props.online !== 1
				? <TouchableOpacity
					onPress={props.onPress}
					style={styles.buttonContainer}
				>
					{
						props.loading && (
							<Spinner color={props.spinnerColor} />
						)
					}
					{
						!props.loading && props.withIcon && (
							<Image
								source={props.icon}
								style={styles.icon}
							/>
						)
					}
					{
						!props.loading && (
							<Text style={props.textStyle}>
								{props.title}
							</Text>
						)
					}
				</TouchableOpacity>
				: null

			}
			{
				props.showIconStar && (
					<TouchableOpacity
						style={styles.iconStarContainer}
						onPress={props.onPressStar}
					>
						<Image
							style={styles.iconStar}
							source={IconStar}
						/>
					</TouchableOpacity>

				)
			}
			{
				props.showFavoriteIcon && (
					<TouchableOpacity
						style={styles.iconStarContainer}
						onPress={props.onPressStar}
					>
						<Image
							style={styles.iconStar}
							source={IconFavorite}
						/>
					</TouchableOpacity>

				)
			}
		</View>
	);
}

HeaderBorderedButton.defaultProps = {
	onPress: () => { },
	title: '',
	withIcon: true,
	icon: PicIcon,
	showIconStar: false,
	showFavoriteIcon: false,
	onPressStar: () => { },
	textStyle: styles.text,
	loading: false,
	spinnerColor: styles.spinner.color
}