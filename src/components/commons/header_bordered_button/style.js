import { StyleSheet } from 'react-native';

//custom
import { NEXALIGHT, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: resize(25),
    flexDirection: 'row'
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: resize(237),
    height: resize(50, 'height'),
    borderRadius: 4,
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 3,
    paddingHorizontal: resize(10),
  },
  icon: {
    resizeMode: 'contain',
    width: resize(28),
    height: resize(21, 'height'),
  },
  text: {
    color: '#ffffff',
    fontFamily: NEXALIGHT,
    fontSize: resize(21),
    fontWeight: '400',
    paddingLeft: resize(10),
  },
  iconStarContainer: {
    marginLeft: resize(30)
  },
  iconStar: {
    resizeMode: 'contain',
    width: resize(22),
    height: resize(22, 'height'),
  },
  spinner: {
    color: '#fff'
  }
});

export default styles;