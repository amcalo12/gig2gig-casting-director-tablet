import { StyleSheet } from 'react-native';

// custom
import { width, height } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width,
    height
  }
});

export default styles;