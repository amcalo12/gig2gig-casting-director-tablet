import React from 'react';
import { ImageBackground } from 'react-native';
import { Container, Content } from 'native-base';

// custom
import styles from './styles';

export default function BackgroundWrapper(props) {
  return (
    <Container>
      <Content
        bounces={false}
        disableKBDismissScroll
      >
        <ImageBackground
          source={require('../../../assets/images/commons/bg.png')}
          style={[styles.container, props.style]}
        >
          {props.children}
        </ImageBackground>
      </Content>
    </Container>
  );
}

BackgroundWrapper.defaultProps = {
  style: {}
}
