import { StyleSheet, Platform } from 'react-native';
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		width: resize(350),
		height: resize(200, 'h'),
		backgroundColor: 'white',
		position: 'absolute',
		zIndex: 100,
		right: resize(15),
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
		paddingHorizontal: resize(15),
		paddingVertical: resize(15, 'h'),
		borderRadius: resize(7)
	},
	text: {
		color: '#4D2545',
		fontSize: resize(22),
		letterSpacing: 1,
		fontFamily: NEXABOLD
	},
	dismissButton: {
		width: '100%',
		height: '100%',
		position: 'absolute',
		justifyContent: 'flex-end',
		backgroundColor: 'rgba(0,0,0,0.5)'
	},
	shape: {
		right: resize(10),
		width: 0,
		height: 0,
		borderLeftWidth: resize(30),
		borderRightWidth: resize(30),
		borderBottomWidth: resize(Platform.OS === 'ios' ? 40 : 20, 'height'),
		borderStyle: 'solid',
		backgroundColor: 'transparent',
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#FFF',
		position: 'absolute',
		top: -resize(Platform.OS === 'ios' ? 30 : 20, 'height')
	},
	formInput: {
		textAlignVertical: 'top',
		backgroundColor: '#ffffff',
		borderRadius: 28,
		borderColor: '#4d2545',
		borderWidth: 2,
		height: '100%',
		width: '100%',
		color: '#4d2545',
		marginTop: resize(15, 'h'),
		paddingLeft: resize(20)
	},
	buttonSaveContainer: {
		width: '50%',
		height: resize(50, 'height'),
		borderRadius: resize(40),
		borderColor: '#5f2543',
		borderWidth: 4,
		marginBottom: resize(20, 'height'),
		// marginHorizontal: resize(10)
	},
	buttonSaveGradient: {
		width: '100%',
		height: '100%',
		borderRadius: 28,
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonSaveText: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(17)
	},
})
export default styles;
