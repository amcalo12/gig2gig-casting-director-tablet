import React, { Component } from 'react';
import { View, Text, TextInput, SafeAreaView, Modal, TouchableOpacity } from 'react-native';
import styles from './styles';

//Custom
import BorderedInput from '../../auditions/bordered_input';
import en from '../../../locale/en';
import GradientButton from '../gradient_button';
import { resize } from '../../../assets/styles';

const Share = ({ headerHeight, handleShare, showShare, handleEmail, email, isSending,
	sendInvitation, title, light, colorText, alignSelf, placeHolder, submit, verificationCode, style }) => (
		<Modal
			animationType='fade'
			animated={true}
			visible={showShare}
			transparent={true}
			onRequestClose={handleShare}
		>
			<TouchableOpacity
				style={styles.dismissButton}
				onPress={handleShare}
				activeOpacity={1}
			>
				<View style={styles.blurView} />
			</TouchableOpacity>
			<View style={[styles.container, { top: headerHeight + 15 }, style]}>
				<View style={styles.shape} />
				<Text style={styles.text}>{title}</Text>
				<BorderedInput
					keyboardType='email-address'
					placeholder={placeHolder}
					style={styles.formInput}
					value={email}
					onChange={handleEmail}
				/>
				<GradientButton
					light={light}
					buttonText={submit ? en.submit_text : en.send_button_title}
					onPress={() => { submit ? verificationCode() : sendInvitation() }}
					containerStyle={[styles.buttonSaveContainer, { alignSelf }]}
					gradientStyle={styles.buttonSaveGradient}
					textStyle={[styles.buttonSaveText, { color: colorText }]}
					loading={isSending}
					spinnerColor='black'
				/>
			</View>
		</Modal>

	)

Share.defaultProps = {
	title: 'Share performer',
	light: true,
	colorText: '#5f2543',
	alignSelf: 'flex-end',
	placeHolder: 'name@email.com',
	submit: false
}

export default Share;