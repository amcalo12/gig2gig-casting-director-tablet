import React from 'react';
import { TouchableOpacity, Image } from 'react-native';

//custom
import styles from './styles';
import iconMenu from '../../../assets/icons/menu/menu.png';

export default function ButtonMenu(props) {
  return (
      <TouchableOpacity 
        onPress={props.onPress}        
        style={styles.iconContainer}
      >
        <Image
          source={iconMenu}
          style={styles.icon}
        />        
      </TouchableOpacity>
  );
}

ButtonMenu.defaultProps = {
  onPress: () => {},
}