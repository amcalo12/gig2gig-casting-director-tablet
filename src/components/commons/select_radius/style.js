import { StyleSheet, Platform } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(290),
    height: resize(55, 'height'),
    flexDirection: 'row',
    marginBottom: resize(17, 'height'),
  },
  input: {
    margin: 0,
    padding: 0,
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(19),
    fontWeight: '400',
    paddingLeft: resize(20),
    shadowColor: 'rgba(0, 0, 0, 0.18)',
    shadowOffset: { width: 3, height: 0 },
    shadowRadius: 8,
    width: resize(290),
    height: resize(55, 'height'),
    backgroundColor: '#ffffff',
    borderRadius: 50,
  },
  dropdownIcon: {
    resizeMode: 'contain',
    width: resize(12),
    height: resize(11, 'height'),
  },
  iconContainer: {
    top: resize(23, 'height'),
    right: resize(15),
  },
  modalViewMiddle: {
    backgroundColor: '#FFF',
    borderBottomWidth: 1,
    height: resize(60, 'height'),
    borderBottomColor: '#d9d9d9'
  },
  modalViewBottom: {
    backgroundColor: '#fff',
    paddingTop: resize(60),
  },
  done: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
  }
});

export default styles;