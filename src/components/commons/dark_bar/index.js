import React from 'react';
import { StatusBar } from 'react-native';

export default function DarktBar() {
  return(
    <StatusBar 
      translucent
      backgroundColor='transparent'
      barStyle='light-content'
    />
  );
}