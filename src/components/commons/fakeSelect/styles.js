import { StyleSheet } from 'react-native';

//custom
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		alignSelf: 'center',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		width: resize(220),
		height: resize(50, 'h'),
		marginBottom: -resize(15, 'height'),
	},
	text: {
		color: '#4d2545',
		fontSize: resize(20),
		fontWeight: '400',
		letterSpacing: resize(0.9),
		fontFamily: NEXABOLD
	},
	text2: {
		color: '#4d2545',
		fontSize: resize(14),
		fontWeight: '400',
		letterSpacing: resize(0.9),
		fontFamily: NEXABOLD
	},
	modalContainer: {
		width: '100%',
		height: '100%',
		backgroundColor: '#00000034'
	},
	pickerContainer: {
		backgroundColor: '#FFF',
		paddingVertical: resize(10, 'height'),
		paddingHorizontal: resize(10),
		borderRadius: resize(8)
	},
	buttonContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		borderBottomColor: '#D6D6D6',
		borderBottomWidth: 0.5
	},
	buttonContainerLast: {
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row'
	}
});

export default styles;