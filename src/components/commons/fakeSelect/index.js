import React from "react";
import { TouchableOpacity, Text, Image, Modal, Animated } from "react-native";

//custom
import styles from "./styles";

//assets
import picker_downflag from "../../../assets/icons/select_radius/dropdown.png";
import Plus from "../../../assets/icons/auditions/icon-plus.png";

//custom
import { resize } from "../../../assets/styles";
import en from "../../../locale/en";

type Props = {
  containerStyle: Object,
  textStyles: Object,
  text: String,
  changeTypeOfFeddOptions: Function,
  onChangeValue: Function,
  refs: Function,
  chagingType: Boolean,
  position: Object,
  animation: Any,
  options: Array,
  onRequestNewRound: Function,
  onPresFinalRound: Function,
  showFinal: Boolean,
};

export default function FakeDropDown(props: Props) {
  const {
    containerStyle,
    textStyles,
    text,
    changeTypeOfFeddOptions,
    onChangeValue,
    refs,
    chagingType,
    position,
    animation,
    options,
    status,
    onPresFinalRound,
    showFinal,
    isManger,
  } = props;

  const backgroundColor = animation.interpolate({
    inputRange: [0, 10],
    outputRange: ["#FFFFFF00", "#00000034"],
  });

  const opacity = animation.interpolate({
    inputRange: [0, 10],
    outputRange: [0, 1],
  });

  const width = animation.interpolate({
    inputRange: [0, 10],
    outputRange: [0, position.width + resize(20)],
  });

  const height = animation.interpolate({
    inputRange: [0, 5, 10],
    outputRange: [
      0,
      position.height,
      position.height * (options.length + 2) + resize(20, "h"),
    ],
  });

  return (
    <>
      <TouchableOpacity
        ref={(picker) => refs(picker)}
        activeOpacity={1}
        onPress={() => changeTypeOfFeddOptions(!chagingType)}
        style={[styles.container, containerStyle]}
      >
        <Text style={[styles.text, textStyles]}>
          {!showFinal ? `Round ${text} ` : "Final Casting List"}
        </Text>
        <Image source={picker_downflag} resizeMode="contain" />
      </TouchableOpacity>
      <Modal
        animationType="none"
        transparent={true}
        visible={chagingType}
        onRequestClose={() => changeTypeOfFeddOptions(!chagingType)}
      >
        <Animated.View
          style={[
            styles.modalContainer,
            {
              paddingTop: position.py,
              paddingLeft: position.px,
              backgroundColor,
            },
          ]}
        >
          <Animated.View
            style={[styles.pickerContainer, { width, height, opacity }]}
          >
            <TouchableOpacity
              style={[styles.buttonContainerLast, { height: position.height }]}
              onPress={() => {
                return changeTypeOfFeddOptions(!chagingType);
              }}
            >
              <Text style={[styles.text, textStyles]}>
                {status !== 2 ? `Round ${text} ` : "Final Casting List"}
              </Text>
              <Image source={picker_downflag} resizeMode="contain" />
            </TouchableOpacity>
            {options.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={[styles.buttonContainer, { height: position.height }]}
                  onPress={() => {
                    onChangeValue(item);
                    return changeTypeOfFeddOptions(!chagingType);
                  }}
                >
                  <Text
                    style={[styles.text2, textStyles]}
                  >{`Round ${item.round}`}</Text>
                </TouchableOpacity>
              );
            })}
            {isManger && status !== 2 && (
              <TouchableOpacity
                style={[
                  styles.buttonContainerLast,
                  { height: position.height },
                ]}
                onPress={() => props.onRequestNewRound()}
              >
                <Image resizeMode="contain" source={Plus} />
                <Text
                  style={[styles.text2, textStyles]}
                >{`   ${en.open_title}`}</Text>
              </TouchableOpacity>
            )}
            {status === 2 && (
              <TouchableOpacity
                style={[
                  styles.buttonContainerLast,
                  { height: position.height },
                ]}
                onPress={() => onPresFinalRound()}
              >
                <Text
                  style={[styles.text2, textStyles]}
                >{`Final Casting List`}</Text>
              </TouchableOpacity>
            )}
          </Animated.View>
        </Animated.View>
      </Modal>
    </>
  );
}

FakeDropDown.defaultProps = {
  options: [],
  containerStyle: {},
  textStyles: {},
  text: "DropDown",
  chagingType: false,
  changeTypeOfFeddOptions: () => {},
  onChangeValue: () => {},
  refs: () => {},
  position: { py: 0, px: 0 },
  animation: new Animated.Value(0),
  onPresFinalRound: () => {},
  showFinal: false,
};
