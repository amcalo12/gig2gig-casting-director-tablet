import { StyleSheet } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(290),
    height: resize(55, 'height'),
    marginBottom: resize(17, 'height'),
    flexDirection: 'row',
  },
  inputContainer: {
    width: resize(290),
  },
  input: {
    backgroundColor: '#ffffff',
    borderRadius: 50,
    height: resize(55, 'height'),
  },
  dateText: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(19),
    fontWeight: '400',
    paddingLeft: resize(20),
    textAlign: 'left',
    alignSelf: 'flex-start',
  },
  btnTextConfirm: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
  },
  btnTextCancel: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
  }
});

export default styles;