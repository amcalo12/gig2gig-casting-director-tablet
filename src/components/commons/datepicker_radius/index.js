import React from 'react';
import { View } from 'react-native';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';

//customs
import styles from './style';

DatepickerRadius = (props) => {
	return (
		<View style={props.containerStyle}>
			<DatePicker
				style={props.inputContainer}
				mode='date'
				placeholder={props.placeholder}
				format='LL'
				confirmBtnText='Confirm'
				cancelBtnText='Cancel'
				onDateChange={(dateString, date) => props.onChange(date)}
				customStyles={{
					dateInput: props.dateInput,
					dateText: styles.dateText,
					placeholderText: styles.dateText,
					btnTextConfirm: styles.btnTextConfirm,
					btnTextCancel: styles.btnTextCancel
				}}
				showIcon={false}
				timeZoneOffsetInMinutes={undefined}
				date={props.value !== null ? props.value : null}
				minDate={
					new Date(
						moment()
							.subtract(100, 'years')
							.format('L')
					)
				}
				maxDate={props.maxDate}
			/>
		</View>
	);
}

DatepickerRadius.defaultProps = {
	onChange: () => { },
	value: null,
	dateInput: styles.input,
	dateText: styles.dateText,
	placeholderText: styles.placeholderText,
	btnTextConfirm: styles.btnTextConfirm,
	btnTextCancel: styles.btnTextCancel,
	placeholder: '',
	containerStyle: styles.container,
	inputContainer: styles.inputContainer,
	maxDate: new Date()
}

export default DatepickerRadius;
