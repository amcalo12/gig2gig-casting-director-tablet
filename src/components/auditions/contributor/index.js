import React from 'react';
import { View, Text, Image } from 'react-native';

//custom
import styles from './style';
import userIcon from '../../../assets/icons/auditions/user-icon.png';

export default function Contributor(props) {
  return(
    <View style={styles.container}>
      <View style={styles.iconContainer}>
        <Image 
          source={userIcon}
          style={styles.icon}
        />
      </View>
      <View style={styles.nameContainer}>
        <Text style={styles.name}>
          {props.name}
        </Text>
      </View>
    </View>
  );
}

Contributor.defaultProps = {
  title: ''
}

