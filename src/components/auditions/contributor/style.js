import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    paddingBottom: resize(26, 'height'),
  },
  icon: {
    resizeMode: 'contain',
    width: resize(28),
    height: resize(32, 'height'),
  },
  nameContainer: {
    paddingHorizontal: resize(16),
    justifyContent: 'center',
    alignItems: 'center',
  },
  name: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(15),
    fontWeight: '400',
  }
});

export default styles;