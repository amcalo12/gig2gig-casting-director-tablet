import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';

//custom 
import styles from './style';
import UserIcon from '../../../assets/icons/auditions/user-info-icon.png';

export default function InvitationButton(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={props.onPress}
        style={styles.buttonContainer}
      >
        <Image
          source={UserIcon}
          style={styles.UserIcon}
        />
        <Text style={styles.text}>
          {props.title}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

InvitationButton.defaultProps = {
  onPress: () => { },
  title: ''
}