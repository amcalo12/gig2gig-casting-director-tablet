import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: resize(30, 'height'),
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginBottom: resize(22, 'height'),
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  UserIcon: {
    resizeMode: 'contain',
    width: resize(22),
    height: resize(24, 'height'),
  },
  text: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
    marginLeft: resize(11),
  }
});

export default styles;