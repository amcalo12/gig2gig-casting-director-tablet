import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

//custom 
import styles from './style';

export default function ContractTag(props) {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={
        props.selected ? props.containerSelectedStyle : props.containerStyle
      }
      disabled={props.disabled}
    >
      <Text
        style={
          props.selected ? styles.nameSelected : styles.name
        }
      >
        {props.name}
      </Text>
    </TouchableOpacity>
  );
}

ContractTag.defaultProps = {
  onPress: () => { },
  name: '',
  selected: false,
  containerSelectedStyle: styles.containerSelected,
  containerStyle: styles.container,
  disabled: false
}