import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    borderRadius: 19,
    borderColor: '#d8893a',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
    marginBottom: resize(11, 'height'),
  },
  containerSelected: {
    borderRadius: 19,
    borderColor: '#d8893a',
    borderWidth: 1,
    backgroundColor: '#d8893a',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
    marginBottom: resize(11, 'height'),
  },
  name: {
    color: '#d8893a',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
    textTransform: 'uppercase'
  },
  nameSelected: {
    color: '#fff',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
    textTransform: 'uppercase'
  }
});

export default styles;