import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: resize(55, 'height'),
  },
  input: {
    width: '100%',
    height: resize(55, 'height'),
    borderRadius: 28,
    borderColor: '#4d2545',
    borderWidth: 1,
  },
  text: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
    paddingLeft: resize(20),
    textAlign: 'left',
    alignSelf: 'flex-start',
  },
  btnTextConfirm: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
  },
  btnTextCancel: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
  },
  iconContainer: {
    position: 'absolute',
    right: resize(18),
    top: resize(16, 'height'),
    zIndex: 2
  },
  icon: {
    width: resize(18),
    height: resize(19, 'height'),
    resizeMode: 'contain'
  }
});

export default styles;