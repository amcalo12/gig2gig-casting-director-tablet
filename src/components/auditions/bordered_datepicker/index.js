import React from "react";
import { View, Image, TouchableOpacity } from "react-native";
import DatePicker from "react-native-datepicker";

//custom
import styles from "./style";
import CalendarIcon from "../../../assets/icons/auditions/calendar-icon.png";

export default function BorderedDatepicker(props) {
  return (
    <View style={props.containerStyle}>
      {props.disable && (
        <View
          style={[
            props.containerStyle,
            {
              position: "absolute",
              zIndex: 2,
              top: 0,
              right: 0,
              backgroundColor: "#FFFFFF99",
            },
          ]}
        />
      )}
      <DatePicker
        mode={props.mode ? props.mode : "date"}
        placeholder={props.placeholder}
        style={props.containerStyle}
        format={props.format ? props.format : "LL"}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        date={props.value !== null ? props.value : null}
        onDateChange={(dateString, date) => props.onChange(date)}
        timeZoneOffsetInMinutes={undefined}
        showIcon={false}
        minDate={props.minDate}
        customStyles={{
          dateInput: { ...props.containerStyle, ...props.inputStyle },
          dateText: { ...props.textStyle },
          dateTouchBody: { ...props.containerStyle },
          placeholderText: { ...props.placeholderTextStyle },
          btnTextConfirm: styles.btnTextConfirm,
          btnTextCancel: styles.btnTextCancel,
        }}
        {...props}
      />
      {props.withIcon && (
        <View style={styles.iconContainer}>
          <Image source={props.icon} style={styles.icon} />
        </View>
      )}
      {props.withIcon && props.touchIcon && (
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={props.onPressIcon}
        >
          <Image source={props.icon} style={styles.icon} />
        </TouchableOpacity>
      )}
    </View>
  );
}

BorderedDatepicker.defaultProps = {
  containerStyle: styles.container,
  textStyle: styles.text,
  placeholderTextStyle: styles.text,
  inputStyle: styles.input,
  value: null,
  onChange: () => {},
  onPressIcon: () => {},
  placeholder: "",
  withIcon: true,
  icon: CalendarIcon,
  minDate: new Date(),
  disable: false,
};
