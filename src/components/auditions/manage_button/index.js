import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Spinner } from 'native-base';

//custom 
import styles from './style';

function ManageButton(props) {
	return (
		<View style={[styles.container, props.bordered ? styles.bordered : {}]}>

			<TouchableOpacity
				onPress={props.onPress}
				style={[styles.buttonContainer, props.fully ? styles.fully : {}]}
			>
				{
					props.loading && (
						<Spinner
							color={props.spinnerColor}
							size='small'
						/>
					)
				}
				{
					!props.loading && (
						<Text style={[styles.title, props.fully ? styles.whiteText : {}]}>
							{props.title}
						</Text>
					)
				}
			</TouchableOpacity>
		</View>
	);
}

ManageButton.defaultProps = {
	onPress: () => { },
	title: '',
	bordered: false,
	loading: false,
	fully: false,
	spinnerColor: styles.spinner.color
}

export default ManageButton;
