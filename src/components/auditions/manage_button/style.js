import { StyleSheet } from 'react-native';

//custom
import { resize, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: resize(34, 'height'),
	},
	buttonContainer: {
		width: resize(213),
		height: resize(45, 'height'),
		borderRadius: 4,
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 3,
		justifyContent: 'center',
		alignItems: 'center',
	},
	title: {
		color: '#4d2545',
		fontFamily: NEXALIGHT,
		fontSize: resize(16),
	},
	bordered: {
		borderBottomColor: '#bfbfbf',
		borderBottomWidth: 1,
	},
	spinner: {
		color: '#4d2545'
	},
	fully: {
		backgroundColor: '#4D2545'
	},
	whiteText: {
		color: 'white'
	}
});

export default styles;