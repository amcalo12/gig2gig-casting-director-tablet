import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXABOLD, height, STATUS_BAR_HEIGHT, width } from '../../../assets/styles';

const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT;

export default StyleSheet.create({
	nBContainer: {
		backgroundColor: 'transparent'
	},
	dismissButton: {
		width: '100%',
		height: containersHeight,
		position: 'absolute',
		justifyContent: 'flex-end'
	},
	blurView: {
		width: '100%',
		height: resize(Platform.OS === 'ios' ? 732 : 729, 'height'),
		backgroundColor: '#f0f0f0',
		opacity: 0.84,
	},
	cardContainer: {
		width: '100%',
		height: containersHeight,
		justifyContent: 'flex-end'
	},
	card: {
		alignSelf: 'center',
		width: resize(389),
		// height: resize(664, 'height'),
		shadowColor: 'rgba(0, 0, 0, 0.16)',
		shadowOffset: { width: 3, height: 0 },
		shadowRadius: resize(6),
		borderRadius: resize(13),
		backgroundColor: '#ffffff',
		marginBottom: resize(30, 'height'),
		paddingBottom: resize(20)
	},
	closeButton: {
		paddingHorizontal: resize(18),
		paddingVertical: resize(21, 'height')
	},
	photoText: {
		alignSelf: 'center',
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(24),
		fontWeight: '400',
		letterSpacing: resize(1.2),
	},
	uploadPic: {
		marginTop: resize(33, 'height'),
		alignSelf: 'center',
		width: resize(72),
		height: resize(56, 'height'),
		resizeMode: 'contain'
	},
	roleImage: {
		resizeMode: 'cover',
		alignSelf: 'center',
		width: resize(110),
		height: resize(110),
		...Platform.select({
			ios: {
				borderRadius: (resize(110) / 2)
			},
			android: {
				borderRadius: width / 2
			}
		})
	},
	roleNameContainer: {
		alignSelf: 'center',
		marginTop: resize(33, 'height'),
		width: resize(328),
		height: resize(55, 'height'),
		borderRadius: resize(28),
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 1,
		backgroundColor: '#ffffff',
	},
	descriptionContainer: {
		marginTop: resize(21, 'height'),
		alignSelf: 'center',
		width: resize(327),
		height: resize(208, 'height'),
		borderRadius: resize(28),
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 1,
		backgroundColor: '#ffffff',
		paddingVertical: resize(10, 'height'),
		paddingHorizontal: resize(10)
	},
	descriptionInput: {
		textAlignVertical: 'top',
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
		letterSpacing: 1,
	},
	buttonContainers: {
		marginTop: resize(10, 'height'),
		alignItems: 'center',
		height: resize(131, 'height'),
		justifyContent: 'space-between'
	},
	addButton: {
		shadowColor: 'rgba(0, 0, 0, 0.4)',
		shadowOffset: { width: 0, height: 0 },
		shadowRadius: resize(3),
		borderRadius: resize(28),
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 1,
		backgroundColor: '#ffffff',
		width: resize(290)
	},
	addButtonText: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
		letterSpacing: 1
	},
	inputError: {
		borderColor: 'red'
	}
})