import React, { Fragment } from 'react';
import { View, TouchableOpacity, Text, Image, Modal, Alert } from 'react-native';
import { Container, Content } from 'native-base';

//custom
import styles from './styles';
import icon_close from '../../../assets/icons/commons/icon_close.png';
import icon_pic from '../../../assets/icons/auditions/dark_pic.png';
import InputRadius from '../../commons/input_radius';
import GradientButton from '../../commons/gradient_button';
import { gradientLight } from '../../../assets/styles';

export default function AddRoleModal(props) {
  return (
    <Modal
      animationType='fade'
      animated={true}
      visible={props.show}
      transparent={true}
      onRequestClose={props.dismiss}
    >
      <Container style={styles.nBContainer}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.dismissButton}
          onPress={props.dismiss}
        >
          <View style={styles.blurView} />
        </TouchableOpacity>
        <Content bounces={false}>
          <View style={styles.cardContainer}>
            <View style={styles.card}>
              <TouchableOpacity
                style={styles.closeButton}
                onPress={props.dismiss}
              >
                <Image source={icon_close} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={props.onPressTakeImage}
              >
                {
                  props.roleImage === null && (
                    <Fragment>
                      <Text style={styles.photoText}>
                        Upload Photo
                      </Text>
                      <Image
                        source={icon_pic}
                        style={styles.uploadPic}
                      />
                    </Fragment>
                  )
                }
                {
                  props.roleImage !== null && (
                    <Image
                      source={props.roleImage}
                      style={styles.roleImage}
                    />
                  )
                }
              </TouchableOpacity>
              {console.log('props.roleImage :', props.roleImage)}
              {props.roleImage !== null &&
                <InputRadius
                  value={props.roleImageName}
                  containerStyle={styles.roleNameContainer}
                  placeholder='File name'
                  onChange={props.onChangeRoleImageName}
                />}
              <InputRadius
                value={props.name}
                containerStyle={styles.roleNameContainer}
                placeholder='Role Name (*)'
                onChange={props.changeRoleName}
              />
              <InputRadius
                value={props.description}
                multiline={true}
                containerStyle={styles.descriptionContainer}
                placeholder='Description (*)'
                style={styles.descriptionInput}
                onChange={props.changeRoleDescription}
              />
              <View style={styles.buttonContainers}>
                {
                  props.showAddAnotherRole && (
                    <GradientButton
                      containerStyle={styles.addButton}
                      colors={gradientLight}
                      buttonText='Add Another Role'
                      textStyle={styles.addButtonText}
                      onPress={props.addAnother}

                    />
                  )
                }
                <GradientButton
                  buttonText='Done'
                  onPress={props.done}
                />
              </View>
            </View>
          </View>
        </Content>
      </Container>
    </Modal>
  )
}

AddRoleModal.defaultProps = {
  dismiss: () => null,
  done: () => null,
  addAnother: () => null,
  changeRoleName: () => null,
  changeRoleDescription: () => null,
  show: true,
  name: '',
  description: '',
  onPressTakeImage: () => { },
  roleImage: null,
  showAddAnotherRole: true
}