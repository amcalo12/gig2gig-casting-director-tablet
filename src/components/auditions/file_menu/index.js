import React from "react";
import {
  View,
  Text,
  Image,
  Modal,
  TouchableOpacity,
  Platform,
  Switch,
  ActivityIndicator,
} from "react-native";

//custom
import styles from "./styles";
import { resize, height, STATUS_BAR_HEIGHT } from "../../../assets/styles";
import shareIcon from "../../../assets/icons/auditions/share-icon.png";
import openIcon from "../../../assets/icons/auditions/open-in-icon.png";
import trashIcon from "../../../assets/icons/auditions/trash-icon.png";
import { Assets } from "../../../assets";
import colors from "../../../utils/colors";

export default function FileMenu(props) {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={props.show}
      onRequestClose={props.dismiss}
    >
      <View style={styles.container}>
        <TouchableOpacity style={styles.dismiss} onPress={props.dismiss}>
          <View style={[styles.dismiss, styles.dismissView]} />
        </TouchableOpacity>
        <View
          style={[
            styles.childrenContainer,
            {
              top:
                props.posy - resize(Platform.OS === "ios" ? 0 : 23, "height"),
            },
            "posx" in props && {
              marginLeft: props.posx,
            },
          ]}
        >
          {props.children}
        </View>
        <View
          style={[
            props.showShareable
              ? [styles.card, { height: resize(145, "height") }]
              : styles.card,
            props.showShareable
              ? { width: resize(192) }
              : { width: resize(152) },
            { top: _marginTop(props.posy) },
            "posx" in props &&
              props.hasPadding &&
              {
                // left: (props.posx + resize(336)) - resize(152)
              },
          ]}
        >
          {props.showShareable && (
            <View style={styles.row}>
              <View style={styles.iconContainer}>
                <Switch
                  onValueChange={props.handleFileShare}
                  value={props.isShareable}
                />
              </View>
              <Text style={styles.switch} style={styles.rowText}>
                Shareable
              </Text>
            </View>
          )}
          {props.showOptionShare && (
            <TouchableOpacity style={styles.row} onPress={props.share}>
              <View style={styles.iconContainer}>
                <Image source={shareIcon} />
              </View>
              <Text style={styles.rowText}>Share</Text>
            </TouchableOpacity>
          )}
          {props.showOptionOpen && (
            <TouchableOpacity style={styles.row} onPress={props.openIn}>
              <View style={styles.iconContainer}>
                <Image source={openIcon} />
              </View>
              <Text style={styles.rowText}>Open in</Text>
            </TouchableOpacity>
          )}
          {props.showOptionDelete && !props.isLoading && (
            <TouchableOpacity style={styles.row} onPress={props.delete}>
              <View style={styles.iconContainer}>
                <Image source={trashIcon} />
              </View>
              <Text style={styles.rowText}>Delete</Text>
            </TouchableOpacity>
          )}
          {props.rename && !props.isLoading && (
            <TouchableOpacity style={styles.row} onPress={props.onRename}>
              <View style={styles.iconContainer}>
                <Image
                  source={Assets.rename}
                  style={{ width: 20, height: 20, tintColor: colors.PRIMARY }}
                />
              </View>
              <Text style={styles.rowText}>Rename</Text>
            </TouchableOpacity>
          )}
          {props.isLoading && (
            <View style={styles.rowLoading}>
              <ActivityIndicator color="#4d2545" size="small" />
            </View>
          )}
        </View>
      </View>
    </Modal>
  );
}

FileMenu.defaultProps = {
  show: true,
  dismiss: () => {},
  share: () => {},
  openIn: () => {},
  delete: () => {},
  posy: resize(650),
  showOptionDelete: false,
  showOptionShare: false,
  showOptionOpen: false,
  showShareable: false,
  hasPadding: true,
  isLoading: false,
};

function _marginTop(posy) {
  const minMargin = height - resize(129, "height");
  const margin = posy + resize(87, "height");
  if (minMargin < margin) {
    return posy - resize(Platform.OS === "ios" ? 140 : 160, "height");
  }
  return margin - resize(Platform.OS === "ios" ? 0 : 23, "height");
}
