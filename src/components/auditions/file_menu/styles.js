import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%'
  },
  card: {
    height: resize(129, 'height'),
    shadowRadius: 15,
    borderRadius: 10,
    backgroundColor: '#FFF',
    left: resize(920),
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowOffset: { width: 2, height: 0 },
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 10,
    borderColor: 'rgb(255,255,255)',
    borderWidth: 1,
    justifyContent: 'space-around',
    paddingVertical: resize(10, 'height'),
    position: 'absolute'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: resize(20)
  },
  rowLoading: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: resize(20)
  },
  rowText: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(16),
    fontWeight: '400',
    letterSpacing: resize(0.8)
  },
  dismiss: {
    width: '100%',
    height: '100%'
  },
  dismissView: {
    height: resize(Platform.OS === 'ios' ? 732 : 729, 'height'),
    backgroundColor: 'rgba(255,255,255,.84)',
    bottom: 0,
    position: 'absolute'
  },
  childrenContainer: {
    position: 'absolute',
    zIndex: 12,
    width: resize(336),
    marginLeft: resize(736),
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowOffset: { width: 2, height: 0 },
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 10,
  },
  iconContainer: {
    width: '45%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  switch: {
    marginRight: resize(5)
  }
});

export default styles;
