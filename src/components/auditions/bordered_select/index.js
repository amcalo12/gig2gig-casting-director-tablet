import React from 'react';
import { View, Image, Text } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

//custom
import styles from './style';
import dropdownIcon from '../../../assets/icons/select_radius/dropdown.png';


export default function BorderedSelect(props) {
  const placeholder = {
    label: props.placeholder,
    value: null,
    color: '#9EA0A4',
  };

  return (
    <View style={props.containerStyle}>
      <RNPickerSelect
        placeholder={placeholder}
        items={props.data}
        onValueChange={(value) => props.onChange(value)}
        style={{
          inputIOS: props.style,
          inputAndroid: props.style,
          iconContainer: styles.iconContainer,
          modalViewMiddle: styles.modalViewMiddle,
          modalViewBottom: styles.modalViewBottom,
          done: styles.done
        }}
        value={props.value}
        useNativeAndroidPickerStyle={false}
        placeholderTextColor={props.placeholderTextColor}
        Icon={() => {
          return (
            <Image
              source={dropdownIcon}
              style={styles.dropdownIcon}
            />
          )
        }}
      />
    </View>
  );
}

BorderedSelect.defaultProps = {
  placeholder: '',
  data: [],
  onChange: () => { },
  style: styles.input,
  containerStyle: styles.container,
  placeholderTextColor: '#4d2545',
  value: ''
}