import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: resize(55, 'height'),
    marginBottom: resize(35, 'height'),
  },
  input: {
    margin: 0,
    padding: 0,
    width: '100%',
    height: '100%',
    borderRadius: 28,
    borderColor: '#4d2545',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
    paddingLeft: resize(21),
  },
  dropdownIcon: {
    resizeMode: 'contain',
    width: resize(12),
    height: resize(11, 'height'),
  },
  iconContainer: {
    top: resize(20, 'height'),
    right: resize(22),
  },
  modalViewMiddle: {
    backgroundColor: '#FFF',
    borderBottomWidth: 1,
    height: resize(60, 'height'),
    borderBottomColor: '#d9d9d9'
  },
  modalViewBottom: {
    paddingTop: resize(60),
    backgroundColor: '#FFF',
  },
  done: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
  },
  doneContainer: {
    width: '100%',
    height: 50,
    backgroundColor: 'red',
    paddingHorizontal: resize(25),
    justifyContent: 'center',
    alignItems: 'flex-end',
    borderBottomColor: 'gray',
    borderTopColor: 'gray',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5
  }
});

export default styles;