import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: resize(20, 'height'),
    borderBottomColor: '#F0F0F0',
    borderBottomWidth: 1
  },
  name: {
    fontFamily: NEXALIGHT,
    fontWeight: '400',
    fontSize: resize(18),
    color: '#4d2545',
    textAlign: 'left',
    paddingBottom: resize(5, 'height')
  },
  schedule: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
    textAlign: 'right',
    paddingBottom: resize(5, 'height')
  }
});

export default styles;