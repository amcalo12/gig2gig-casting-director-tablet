import React from 'react';
import { View, Text } from 'react-native';

//custom
import styles from './style';

const AppointmentsItem = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.name} numberOfLines={1} >
        {props.name}
      </Text>
      {props.showTime
        ? <Text style={styles.schedule} numberOfLines={1}  >
          {props.time}
        </Text>
        : null
      }
    </View>
  );
}

AppointmentsItem.defaultProps = {
  name: '',
  time: '',
  showTime: true
}

export default AppointmentsItem;