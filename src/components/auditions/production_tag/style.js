import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    borderRadius: 19,
    borderColor: '#93183e',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
    marginBottom: resize(11, 'height'),
  },
  containerSelected: {
    borderRadius: 19,
    borderColor: '#93183e',
    borderWidth: 1,
    backgroundColor: '#93183e',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
    marginBottom: resize(11, 'height'),
  },
  name: {
    color: '#93183e',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
  },
  nameSelected: {
    color: '#fff',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
  }
});

export default styles;