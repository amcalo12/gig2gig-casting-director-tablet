import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    borderRadius: 19,
    borderColor: '#4d2545',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
    marginBottom: resize(11, 'height'),
  },
  containerSelected: {
    borderRadius: 19,
    borderColor: '#4d2545',
    borderWidth: 1,
    backgroundColor: '#4d2545',
    paddingTop: resize(14, 'height'),
    paddingBottom: resize(10, 'height'),
    paddingHorizontal: resize(10),
    marginRight: resize(10),
    marginBottom: resize(11, 'height'),
  },
  name: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
  },
  nameSelected: {
    color: '#fff',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
  }
});

export default styles;