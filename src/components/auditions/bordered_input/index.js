import React from 'react';
import { View, Image, TextInput } from 'react-native';
import { Input } from 'native-base';

//custom
import styles from './style';

export default function BorderedInput(props) {
	return (
		<View style={props.containerStyle}>
			<TextInput
				style={props.style}
				placeholder={props.placeholder}
				placeholderTextColor={props.placeholderTextColor}
				value={props.value}
				onChangeText={(value) => props.onChange(value)}
				keyboardType={props.keyboardType}
				autoCapitalize='none'
				maxLength={props.maxLength} 
				{...props}
			/>
			{
				props.withIcon && (
					<View style={styles.iconContainer}>
						<Image
							source={props.icon}
							style={styles.icon}
						/>
					</View>
				)
			}
		</View>
	);
}

BorderedInput.defaultProps = {
	containerStyle: styles.container,
	style: styles.input,
	placeholderTextColor: styles.input.color,
	placeholder: '',
	value: '',
	keyboardType: 'default',
	onChange: () => { },
	withIcon: false,
	icon: null
}
