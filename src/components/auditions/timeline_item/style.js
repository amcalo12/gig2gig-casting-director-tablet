import { StyleSheet } from "react-native";

//custom
import { resize, NEXALIGHT, NEXABOLD } from "../../../assets/styles";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginBottom: resize(30, "height")
  },
  updateTitle: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(18),
    fontWeight: "300",
    paddingBottom:10
  },
  updateCreatedAt: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(14),
    fontWeight: "300",
    paddingTop: resize(25, "height")
  }
});

export default styles;
