import React from 'react';
import { View, Text } from 'react-native';

//custom 
import styles from './style';

export default function TimelineItem(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.updateTitle}>
        {props.title}
      </Text>
      <Text style={styles.updateCreatedAt}>
        {props.time}
      </Text>
    </View>
  );
}

TimelineItem.defaultProps = {
  title: '',
  time: ''
}