import { StyleSheet } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  row: {
    borderBottomColor: '#fff',
  },
  invitationRow: {
    flexDirection: 'row',
    width: '100%',
    height: resize(27, 'height'),
    alignItems: 'center',
    paddingLeft: resize(11),
    justifyContent: 'center',
    marginTop: resize(10, 'height'),
  },
  leftContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  pendingIcon: {
    position: 'absolute',
    right: resize(11),
  },
  invitationRowIconPeople: {
    width: resize(24),
    height: resize(27, 'height'),
    resizeMode: 'contain'
  },
  invitationRowName: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
    letterSpacing: resize(0.65),
    paddingHorizontal: resize(14),
    width: resize(175)
  },
  deleteButton: {
    width: '100%',
    height: resize(39, 'height'),
    marginVertical: resize(15, 'height'),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#93183e',
  },
  deleteButtonText: {
    color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: resize(13),
    fontWeight: '400',
    letterSpacing: resize(0.65),
  }
})

export default styles;