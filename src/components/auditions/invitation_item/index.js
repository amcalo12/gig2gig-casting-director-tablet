import React from 'react';
import { Image, Text } from 'react-native';
import { SwipeRow, Button, Icon, View } from 'native-base';

//custom
import styles from './styles';
import user_icon from '../../../assets/icons/auditions/user-icon.png';
import pending_icon from '../../../assets/icons/auditions/pending_icon.png'

export default function InvitationItem(props) {
  return (
    <SwipeRow
      disableLeftSwipe={props.disabled}
      disableRightSwipe
      style={styles.row}
      rightOpenValue={-75}
      body={
        <View
          style={styles.invitationRow}
        >
          <View style={styles.leftContainer}>
            <Image
              source={user_icon}
              style={styles.invitationRowIconPeople}
            />
            <Text
              style={styles.invitationRowName}
              numberOfLines={1}
            >
              {props.email}
            </Text>
          </View>
          {
            !props.status && (
              <Image
                source={pending_icon}
                style={styles.pendingIcon}
              />
            )
          }
        </View>
      }
      right={
        <Button
          danger
          onPress={props.delete}
        >
          <Icon
            active
            name='trash'
          />
        </Button>
      }
    />
  )
}

InvitationItem.defaultProps = {
  email: '',
  shouldBeDelete: false,
  status: false,
  changeShouldBeDelete: () => null,
  delete: () => null,
  cancelDelete: () => null,
  row: null,
  disabled: false
}
