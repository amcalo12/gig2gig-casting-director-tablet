import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import { Input } from 'native-base';

//custom
import styles from './styles';

export default function BorderedInput(props) {
	return (
		<View style={props.containerStyle}>
			<Input
				style={props.style}
				placeholder={props.placeholder}
				placeholderTextColor={props.placeholderTextColor}
				value={props.value}
				onChangeText={props.onChangeValue}
				keyboardType={props.keyboardType}
				autoCapitalize='none'
			/>
			{
				props.withIcon && (
					<View style={styles.iconContainer}>
						<Image
							source={props.icon}
							style={styles.icon}
						/>
					</View>
				)
			}
		</View>
	);
}

export const BorderInputSelect = (props) => (
	<View style={props.containerStyle}>
		<Text style={styles.input2}>{props.placeholder}</Text>
		{
			props.withIcon && (
				<TouchableOpacity onPress={props.onPress}>
					<Image
						source={props.icon}
					/>
				</TouchableOpacity>
			)
		}
	</View>
)

BorderedInput.defaultProps = {
	containerStyle: styles.container,
	style: styles.input,
	placeholderTextColor: styles.input.color,
	placeholder: '',
	value: '',
	keyboardType: 'default',
	onChangeValue: () => { },
	withIcon: false,
	icon: null
}

BorderInputSelect.defaultProps = {
	containerStyle: styles.container,
	style: styles.input,
	placeholderTextColor: styles.input.color,
	placeholder: '',
	value: '',
	keyboardType: 'default',
	onChangeValue: () => { },
	withIcon: false,
	icon: null
}
