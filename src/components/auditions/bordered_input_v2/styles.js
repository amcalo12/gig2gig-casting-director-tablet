import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
	container: {
		width: '100%',
		flexDirection: 'row',
		height: resize(55, 'height'),
		marginBottom: resize(35, 'height'),
		alignItems: 'center'
	},
	input: {
		margin: 0,
		padding: 0,
		backgroundColor: '#ffffff',
		borderRadius: 28,
		borderColor: '#4d2545',
		borderWidth: 1,
		height: '100%',
		width: '100%',
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
		paddingLeft: resize(21),
	},
	input2: {
		width: resize(264),
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
		paddingLeft: resize(21)
	},
	iconContainer: {
		position: 'absolute',
		right: resize(18),
		top: resize(16, 'height'),
		zIndex: 2
	},
	icon: {
		width: resize(18),
		height: resize(19, 'height'),
		resizeMode: 'contain'
	},
	icon2: {
		width: resize(18),
		height: resize(19, 'height'),
		resizeMode: 'contain'
	}
});

export default styles;