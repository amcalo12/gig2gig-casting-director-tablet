import { StyleSheet } from 'react-native';

//custom
import { resize, NEXALIGHT, NEXABOLD } from '../../../assets/styles';

const style = StyleSheet.create({
	formContainer: {
		paddingHorizontal: resize(17),
		alignItems: 'center'
	},
	formInput: {
		width: '90%',
		backgroundColor: 'red',
		height: resize(36, 'height'),
		borderRadius: resize(18),
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 1,
		backgroundColor: '#ffffff',
		textAlign: 'center',
		color: '#4d2545',
		fontFamily: NEXALIGHT,
		fontSize: resize(13),
		fontWeight: '400',
		letterSpacing: resize(0.65),
		padding: 0,
		margin: 0,
		marginTop: resize(22, 'height'),
	},
	gradientContainer: {
		marginTop: -resize(20, 'height'),
		width: '90%',
		height: resize(36, 'height'),
		borderRadius: resize(18),
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 1,
	},
	gradientButton: {
		width: '100%',
		height: resize(36, 'height'),
		borderRadius: resize(18),
		justifyContent: 'center',
		alignItems: 'center'
	},
	gradientButtonText: {
		color: '#ffffff',
		fontFamily: NEXABOLD,
		fontSize: resize(13),
		fontWeight: '400',
		letterSpacing: resize(0.65),
	},
	container2: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'center',
		height: resize(55, 'height'),
		marginBottom: resize(35, 'height'),
	}
})

export default style;