import React from 'react';
import { View } from 'react-native';

//custom
import GradientButton from '../../commons/gradient_button';
import BorderedInput from '../bordered_input';
import styles from './styles';

export default function AddInvitationForm(props) {
	return (
		<View style={styles.formContainer}>
			<BorderedInput
				keyboardType='email-address'
				placeholder='name@email.com'
				containerStyle={styles.container2}
				style={styles.formInput}
				value={props.textValue}
				onChange={props.onChange}
			/>
			<GradientButton
				onPress={props.onPress}
				containerStyle={styles.gradientContainer}
				gradientStyle={styles.gradientButton}
				textStyle={styles.gradientButtonText}
				buttonText='Send'
			/>
		</View>
	)
}