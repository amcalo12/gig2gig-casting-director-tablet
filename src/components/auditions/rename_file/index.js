import React, { Component } from 'react';
import { View, Text, Modal, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import colors from '../../../utils/colors';
import { resize, NEXABOLD, NEXALIGHT } from '../../../assets/styles';

export default class RenameModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.isVisible}
                onRequestClose={this.props.onCloseClick}>
                <TouchableOpacity style={styles.modalContainer} onPress={this.props.onCloseClick} activeOpacity={1}>
                    <TouchableOpacity activeOpacity={1} style={[styles.modalSubContainer, this.props.style]}>
                        <Text style={{
                            fontFamily: NEXABOLD, fontSize: resize(16),
                            marginBottom: 10, alignSelf: "center", color: colors.PRIMARY
                        }}>{"File Name"}</Text>
                        <TextInput
                            style={{
                                borderBottomColor: colors.PRIMARY,
                                borderBottomWidth: 1, paddingBottom: 10, fontSize: resize(16)
                            }}
                            placeholder={"Enter name"}
                            value={this.props.value}
                            onChangeText={this.props.onChangeText}
                            returnKeyType={"done"}
                        />

                        <View style={{ marginTop: 20, flexDirection: 'row' }}>
                            <TouchableOpacity
                                style={styles.cancelContainer}
                                onPress={this.props.onCloseClick}>
                                <Text style={styles.label}>{"Cancel"}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={styles.submitContainer}
                                onPress={this.props.onSubmitClick}>
                                <Text style={styles.label}>{"OK"}</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                </TouchableOpacity>
            </Modal>
        );
    }
}

export const styles = StyleSheet.create({
    modalContainer: {
        flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: colors.SHADOW_COLOR
    },
    modalSubContainer: {
        borderRadius: 8, backgroundColor: colors.WHITE, shadowOpacity: 0.2, elevation: 5, width: "40%", padding: 20, marginBottom: resize(60)
    },
    submitContainer: {
        flex: 1, alignItems: "center",
        justifyContent: "center", marginLeft: 10,
        backgroundColor: colors.PRIMARY,
        paddingVertical: 10, paddingHorizontal: 20,
    },
    cancelContainer: {
        flex: 1, alignItems: "center", justifyContent: "center",
        marginRight: 10, backgroundColor: colors.PRIMARY,
        paddingVertical: 10, paddingHorizontal: 20
    },
    label: {
        color: colors.WHITE, fontFamily: NEXALIGHT, textTransform: "uppercase"
    }
})