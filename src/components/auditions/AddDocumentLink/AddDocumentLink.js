import React, { Component } from 'react';
import { View, Text, Modal, TextInput, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import colors from '../../../utils/colors';
import { NEXABOLD, resize, NEXALIGHT, gradient } from '../../../assets/styles';
import { Assets } from '../../../assets';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import BorderedInput from '../bordered_input';
import en from '../../../locale/en';
import GradientButtom from '../../commons/gradient_button';
import showMessage from '../../../utils/show_message';

export default class AddDocumentLink extends Component {
    constructor(props) {
        super(props);
        this.state = {
            links: [
                {
                    "url": "",
                    "type": 5,
                    "filename": "",
                    "shareable": "yes",
                }
            ]
        };
    }

    onChangeUrl = (value, data, index) => {
        this.state.links[index].url = value

        this.setState({ links: this.state.links })
    }

    onNameChange = (value, data, index) => {
        this.state.links[index].filename = value

        this.setState({ links: this.state.links })
    }

    onAddNew = () => {
        this.state.links[this.state.links.length] = {
            "url": "",
            "type": 5,
            "filename": "",
            "shareable": "yes",
        }

        this.setState({ links: this.state.links })
    }

    checkValidation = () => {
        var isValid = true
        this.state.links.map(data => {
            if (data.filename == "") {
                showMessage("danger", "Please add filename");
                isValid = false
            } else if (data.url == "") {
                showMessage("danger", "Please add url");
                isValid = false
            }
        })
        return isValid
    }

    onDone = () => {
        console.log('this.checkValidation() :', this.checkValidation());
        if (!this.checkValidation()) {
            return
        }
        console.log(" ARRAY OF LINKS ", this.state.links)
        this.props.onDoneClick(this.state.links)
        this.props.onCloseClick()
    }

    removeFromList = (data, index) => {
        let temp = this.state.links;
        temp.splice(index, 1)

        this.setState({ links: temp })
    }

    render() {
        return (
            <Modal
                // animationType="slide"
                animationType="none"
                transparent={true}
                visible={this.props.isVisible}
                onRequestClose={this.props.onCloseClick}>
                <TouchableOpacity style={styles.modalContainer} activeOpacity={1} onPress={this.props.onCloseClick}>
                    <TouchableOpacity style={styles.modalSubContainer} activeOpacity={1}>
                        {/* <TouchableOpacity onPress={this.onAddNew}>
                            <Text>{"Add New"}</Text>
                        </TouchableOpacity> */}
                        <GradientButtom
                            buttonText={"Add New"}
                            colors={gradient}
                            containerStyle={styles.gradientButtonContainer}
                            gradientStyle={styles.gradientButtonSave}
                            textStyle={styles.gradientButtonTextSave}
                            onPress={this.onAddNew}
                        />
                        <KeyboardAwareScrollView
                            keyboardShouldPersistTaps="never"
                            enableAutomaticScroll={true}
                            extraScrollHeight={resize(-150, "height")}
                            showsVerticalScrollIndicator={false}>
                            {
                                this.state.links.map((data, index) => {
                                    return <View style={{
                                        justifyContent: "space-between", marginTop: 5,
                                        alignItems: "center", flexDirection: "row", padding: 5
                                    }}>
                                        <View style={{
                                            justifyContent: "center", flex: 1,
                                            // borderColor: colors.PRIMARY, borderWidth: 0.5,
                                        }}>
                                            <Input placeholder={"Name"} value={data.filename}
                                                onChange={(value) => this.onNameChange(value, data, index)} />
                                            <Input placeholder={en.audition_document_url_placeholder} value={data.url}
                                                onChange={(value) => this.onChangeUrl(value, data, index)} />
                                        </View>

                                        <View style={{ width: 30 }}>
                                            {index != 0 ? <TouchableOpacity onPress={() => this.removeFromList(data, index)}>
                                                <Image source={Assets.close} />
                                            </TouchableOpacity>
                                                : null}
                                        </View>
                                    </View>
                                })
                            }
                        </KeyboardAwareScrollView>
                        <View style={{ flexDirection: "row", justifyContent: "center" }}>
                            <GradientButtom
                                buttonText={"Cancel"}
                                colors={gradient}
                                containerStyle={[styles.gradientButtonContainer, { marginRight: 5 }]}
                                gradientStyle={styles.gradientButtonSave}
                                textStyle={styles.gradientButtonTextSave}
                                onPress={this.props.onCloseClick}
                            />

                            <GradientButtom
                                buttonText={"Done"}
                                colors={gradient}
                                containerStyle={[styles.gradientButtonContainer, { marginLeft: 5 }]}
                                gradientStyle={styles.gradientButtonSave}
                                textStyle={styles.gradientButtonTextSave}
                                onPress={this.onDone}
                            />
                        </View>
                    </TouchableOpacity>
                </TouchableOpacity>
            </Modal >
        );
    }
}

export const styles = StyleSheet.create({
    modalContainer: {
        flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: colors.SHADOW_COLOR
    },
    modalSubContainer: {
        borderRadius: 8, backgroundColor: colors.WHITE, shadowOpacity: 0.2, elevation: 5, width: "50%", height: "50%",
        padding: 20
    },
    closeContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        // marginBottom: resize(10, 'height'),
    },
    input: {
        margin: 5,
        padding: 10,
        backgroundColor: '#ffffff',
        borderRadius: 28,
        borderColor: '#4d2545',
        borderWidth: 1,
        // height: '70%',
        width: '95%',
        color: '#4d2545',
        fontFamily: NEXALIGHT,
        fontSize: resize(14),
        // paddingLeft: resize(21),
    },
    gradientButtonContainer: {
        alignSelf: "flex-end",
        marginBottom: 5
    },
    gradientButtonSave: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        padding: 5
    },
    gradientButtonTextSave: {
        color: '#ffffff',
        fontFamily: NEXABOLD,
        fontSize: resize(12),
    },
})

export const Input = (props) => {
    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                placeholder={props.placeholder}
                placeholderTextColor={'#4d2545'}
                value={props.value}
                onChangeText={(value) => props.onChange(value)}
                keyboardType={props.keyboardType}
                autoCapitalize='none'
                maxLength={props.maxLength}
                {...props}
            />
        </View>
    )
}