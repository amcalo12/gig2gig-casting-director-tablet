import { StyleSheet } from 'react-native';

//custom
import { resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(54),
    height: resize(53, 'height'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  selected: {
    borderRadius: 4,
    borderColor: '#4d2545',
    borderWidth: 3,
  },
  emoji: {
    resizeMode: 'contain',
    width: resize(43),
    height: resize(42, 'height'),
  }
});

export default styles;