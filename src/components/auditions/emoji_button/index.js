import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';

//custom
import styles from './style';

export default function EmojiButton(props) { 
  return(
    <View style={[styles.container, props.selected ? styles.selected : {}]}>
      <TouchableOpacity
        onPress={props.onPress}
        style={styles.container}
      >
        <Image
          source={props.emoji} 
          style={styles.emoji}
        />
      </TouchableOpacity>
    </View>
  );
}

EmojiButton.defaultProps = {
  onPress: () =>{},
  emoji: null,
  selected: false
}
