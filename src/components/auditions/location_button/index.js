import React from 'react';
import { TouchableOpacity, Text, Image, View } from 'react-native';

//custom
import styles from './style';
import LocationIcon from '../../../assets/icons/auditions/location-icon.png';

export default function LocationButton(props) {
  return (
    <TouchableOpacity
      style={props.containerStyle}
      onPress={props.onPress}
    >
      <Text style={props.textStyle}>
        {props.title}
      </Text>
      {
        props.withIcon && (
          <View style={styles.iconContainer}>
            <Image
              source={props.icon}
              style={styles.icon}
            />
          </View>
        )
      }
    </TouchableOpacity>
  );
}

LocationButton.defaultProps = {
  containerStyle: styles.container,
  onPress: () => { },
  title: '',
  textStyle: styles.text,
  withIcon: false,
  icon: LocationIcon
}