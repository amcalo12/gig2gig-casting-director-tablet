import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(328),
    height: resize(55, 'height'),
    backgroundColor: '#ffffff',
    borderRadius: 28,
    borderColor: '#4d2545',
    borderWidth: 1,
    justifyContent: 'center',
  },
  text: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(20),
    fontWeight: '400',
    paddingLeft: resize(21),
  },
  iconContainer: {
    position: 'absolute',
    right: resize(18),
    top: resize(16, 'height'),
    zIndex: 2
  },
  icon: {
    width: resize(18),
    height: resize(19, 'height'),
    resizeMode: 'contain'
  }
});

export default styles;