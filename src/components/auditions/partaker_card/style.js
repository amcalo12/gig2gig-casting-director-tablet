import { StyleSheet } from 'react-native';

//custom
import { resize, NEXABOLD, NEXALIGHT } from '../../../assets/styles';
import colors from '../../../utils/colors';

const style = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#f2f2f2'
  },
  imageContainer: {
    height: resize(250, 'height')
  },
  image: {
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
  partakerInfoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: resize(23, 'height'),
  },
  partakerName: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(23),
    fontWeight: '400',
    alignSelf: 'center',
    textAlign: 'center',
    paddingHorizontal: resize(50),
    paddingVertical: resize(5, 'height')
  },
  companyName: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(16),
    fontWeight: '400',
    paddingVertical: resize(7, 'height'),
    paddingHorizontal: resize(50),
    textAlign: 'center',
    alignSelf: 'center',
  },
  country: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(15),
    fontWeight: '400',
  },
  itemsContainer: {
    paddingLeft: resize(24),
    paddingRight: resize(14),
    marginTop: resize(16, 'height')
  },
  iconContainer: {
    backgroundColor: colors.LIGHT_GREY, justifyContent: "center",
    alignItems: "center", padding: 10, margin: 10, borderRadius: 20
  }
});

export default style;