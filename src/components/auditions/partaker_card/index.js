import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Linking,
  ScrollView,
} from "react-native";

//custom
import styles from "./style";
import PartakerImage from "../../../assets/images/auditions/card-partaker.png";
import CardItem from "../card_item";
import InfoIcon from "../../../assets/icons/auditions/user-info-icon.png";
import CreditIcon from "../../../assets/icons/auditions/credits-icon.png";
import EducationIcon from "../../../assets/icons/auditions/education-icon.png";
import AppearanceIcon from "../../../assets/icons/auditions/appearance-icon.png";
import colors from "../../../utils/colors";
import { Assets } from "../../../assets";
import { setUrl } from "../../../utils/constant";
import VideoIcon from "../../../assets/icons/auditions/video-icon.png";
import MusicIcon from "../../../assets/icons/auditions/music-icon.png";
import PhotoIcon from "../../../assets/icons/auditions/photo-icon.png";
import SheetIcon from "../../../assets/icons/auditions/sheet-icon.png";
import AuditionMediaIcon from "../../../assets/icons/auditions/audition-icon.png";
import myMediaIcon from "../../../assets/icons/auditions/my-media.png";

export default function PartakerCard(props) {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        {props.image && (
          <Image source={{ uri: props.image }} style={styles.image} />
        )}
      </View>
      <View style={styles.partakerInfoContainer}>
        <Text style={styles.partakerName}>{props.name}</Text>
        <Text style={styles.companyName}>{props.company}</Text>
        <Text style={styles.country}>{props.country}</Text>
      </View>

      {props.details != undefined ? (
        <View
          style={{
            flexDirection: "row",
            alignSelf: "center",
            justifyContent: "space-between",
          }}
        >
          {console.log(" DETAIL ", props.details)}
          {props.details.facebook != null ? (
            <SocialLink
              icon={Assets.fb}
              onClick={() =>
                props.details.facebook != null
                  ? Linking.openURL(setUrl(props.details.facebook))
                  : {}
              }
            />
          ) : null}
          {props.details.instagram != null ? (
            <SocialLink
              icon={Assets.instagram}
              onClick={() =>
                props.details.instagram != null
                  ? Linking.openURL(setUrl(props.details.instagram))
                  : {}
              }
            />
          ) : null}
          {props.details.twitter != null ? (
            <SocialLink
              icon={Assets.twitter}
              onClick={() =>
                props.details.twitter != null
                  ? Linking.openURL(setUrl(props.details.twitter))
                  : {}
              }
            />
          ) : null}
          {props.details.linkedin != null ? (
            <SocialLink
              icon={Assets.linkedin}
              onClick={() =>
                props.details.linkedin != null
                  ? Linking.openURL(setUrl(props.details.linkedin))
                  : {}
              }
            />
          ) : null}
        </View>
      ) : null}
      <ScrollView style={styles.itemsContainer}>
        <CardItem title="Info" icon={InfoIcon} onPress={props.onPressInfo} />

        <CardItem
          title="Credits"
          icon={CreditIcon}
          onPress={props.onPressCredits}
        />
        <CardItem
          title="Education & Training"
          icon={EducationIcon}
          onPress={props.onPressEducation}
        />
        <CardItem
          title="Appearance"
          icon={AppearanceIcon}
          onPress={props.onPressAppearance}
        />
        {props.isManger && (
          <CardItem
            title="Resume & Docs"
            icon={myMediaIcon}
            onPress={props.onPressResumeAndDoc}
          />
        )}
        {props.isManger && (
          <CardItem
            title="Videos"
            icon={VideoIcon}
            onPress={props.onPressVideo}
          />
        )}
        {props.isManger && (
          <CardItem
            title="Music"
            icon={MusicIcon}
            onPress={props.onPressMusic}
          />
        )}
        {props.isManger && (
          <CardItem
            title="Photos"
            icon={PhotoIcon}
            onPress={props.onPressPhoto}
          />
        )}
        <CardItem
          title="Sheet Music"
          icon={SheetIcon}
          onPress={props.onPressSheetMusic}
        />
      </ScrollView>
    </View>
  );
}

const SocialLink = (props) => {
  return (
    <TouchableOpacity style={styles.iconContainer} onPress={props.onClick}>
      <Image
        source={props.icon}
        style={{ width: 20, height: 20 }}
        resizeMode={"contain"}
      />
    </TouchableOpacity>
  );
};

PartakerCard.defaultProps = {
  image: null,
  name: "",
  company: "",
  country: "",
  onPressInfo: () => {},
  onPressCredits: () => {},
  onPressEducation: () => {},
  onPressAppearance: () => {},
  onPressResumeAndDoc: () => {},
  onPressMusic: () => {},
  onPressVideo: () => {},
  onPressSheetMusic: () => {},
  onPressAuditionMaterial: () => {},
  onPressPhoto: () => {},
};
