import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

//custom
import styles from './style';
import roleIcon from '../../../assets/icons/auditions/role-icon.png';

export default function Role(props) {
  return (
    <TouchableOpacity
      style={props.containerStyle}
      onPress={props.onPress}
      disabled={props.disabled}
    >
      <View style={styles.iconContainer}>
        <Image
          source={props.icon !== null ? props.icon : roleIcon}
          style={props.iconStyle}
        />
      </View>
      <Text style={props.textStyle}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
}

Role.defaultProps = {
  onPress: () => { },
  icon: roleIcon,
  text: '',
  containerStyle: styles.container,
  textStyle: styles.text,
  iconStyle: styles.icon,
  disabled: false
}