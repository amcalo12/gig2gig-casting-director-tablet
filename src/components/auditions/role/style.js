import { StyleSheet, Platform } from 'react-native';

//custom 
import { NEXABOLD, resize, width } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    paddingVertical: resize(23, 'height'),
    marginRight: resize(29),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(12),
    fontWeight: '400',
  },
  iconContainer: {
    width: resize(47),
    height: resize(47, 'height'),
    marginBottom: resize(5, 'height'),
  },
  icon: {
    width: resize(47),
    height: resize(47, 'height'),
    resizeMode: 'cover',
    ...Platform.select({
      ios: {
        borderRadius: (resize(47) / 2)
      },
      android: {
        borderRadius: width / 2
      }
    })
  }
});

export default styles;