import { StyleSheet } from "react-native";

//custom
import { NEXABOLD, NEXALIGHT, resize } from "../../../assets/styles";

const styles = StyleSheet.create({
  container: {
    width: resize(185),
    flexGrow: 0,
    flexShrink: 1,
    flexBasis: "auto",
    marginBottom: resize(30, "height"),
    height: resize(270, "height"),
    marginRight: resize(33),
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#d6d6d6"
  },
  imageContainer: {
    width: "100%",
    height: resize(154, "height")
  },
  image: {
    resizeMode: "cover",
    width: "100%",
    height: "100%",
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  infoContainer: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignContent: "center",
    flexWrap: "wrap",
    width: "100%",
    paddingHorizontal: resize(7),
    paddingVertical: resize(13, "height")
  },
  buttonContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignContent: "center",
    // flexWrap: 'wrap',
    width: "100%",
    paddingHorizontal: resize(15)
    //  paddingVertical: resize(13, 'height'),
  },
  buttonContainer1: {
    // flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignContent: "center",
    marginTop: 20,
    // flexWrap: 'wrap',
    width: "50%",
    paddingHorizontal: resize(15)
    //  paddingVertical: resize(13, 'height'),
  },
  name: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(16),
    fontWeight: "400",
    paddingBottom: resize(1, "height")
  },
  schedule: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(12),
    fontWeight: "400"
  },
  startIcon: {
    position: "absolute",
    top: resize(9, "height"),
    left: resize(11),
    width: resize(17),
    height: resize(17, "height"),
    zIndex: 2
  }
});

export default styles;
