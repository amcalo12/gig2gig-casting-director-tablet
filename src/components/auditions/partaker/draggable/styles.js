import { StyleSheet } from 'react-native';
import { resize, NEXABOLD } from '../../../../assets/styles';

const styles = StyleSheet.create({
    container: {
        width: resize(178),
        height: resize(201, 'height')
    },
    display: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#d6d6d6',
    },
    imgContainer: {
        width: '100%',
        height: '70%'
    },
    img: {
        width: '100%',
        height: '100%'
    },
    text: {
        width: '100%',
        height: '30%',
        marginLeft: resize(10),
        justifyContent: 'center'
    },
    name: {
        color: '#4d2545',
        fontFamily: NEXABOLD,
        fontSize: resize(16),
        fontWeight: '400',
        paddingBottom: resize(1, 'height'),
    }
});

export default styles;
