import React from 'react';
import { View, Animated, Text, Spinner, Image } from 'react-native';
import styles from './styles';


const PanView = ({ styleManage, image, name }) => {
	let display = styleManage ? styles.display : '';
	return (
		<Animated.View style={[styles.container, display]} >
			<View style={styles.imgContainer}>
				{display
					? <Image
						style={styles.img}
						resizeMode='cover'
						source={{ uri: image }}
					/>
					: null}
			</View>
			<View style={styles.text}>
				<Text numberOfLines={1} style={styles.name}> {display ? name : ''}</Text>
			</View>
		</Animated.View>
	)
}
export default PanView;
