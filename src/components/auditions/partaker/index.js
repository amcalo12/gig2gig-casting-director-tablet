import React from "react";
import { TouchableOpacity, View, Text, Image, TextInput } from "react-native";

//custom
import styles from "./style";
import startIcon from "../../../assets/icons/auditions/star-icon.png";
import { Assets } from "../../../assets";
import { resize, NEXALIGHT } from "../../../assets/styles";
import { Card } from "native-base";
import colors from "../../../utils/colors";
import { DEVICE } from "../../../utils/constant";

export default function Partaker(props) {
  return (
    props.isList ?
      <View style={{
        width: (DEVICE.DEVICE_WIDTH / 2) - resize(70), backgroundColor: "#ffffff", borderRadius: 10,
        marginRight: 10, marginBottom: 10
      }} >

        {props.isSelectionMode ?
          <TouchableOpacity
            style={[
              {
                width: "100%", borderRadius: 10, height: "98%", marginVertical: 5
              },
              {
                position: "absolute",
                backgroundColor: "rgba(0, 0, 0, 0.25)",
                zIndex: 999,
                justifyContent: "center",
                alignItems: "center",
                // bottom: 2
              }
            ]}
            activeOpacity={1}
            onPress={props.onSelectClick}
          >
            <Image
              source={props.isSelected ? Assets.selected : Assets.unselected}
              style={{ height: 40, width: 40 }}
              resizeMode="contain"
            />
          </TouchableOpacity>
          : null
        }

        <Card style={{ backgroundColor: "#ffffff", padding: 10, borderRadius: 10, }}>
          <TouchableOpacity style={{ flexDirection: "row" }} onPress={props.onPress}>
            <View style={[styles.imageContainer, { width: resize(150) }]}>
              {props.favorite && (
                <Image source={startIcon} style={styles.startIcon} />
              )}
              {/*this line had an uri*/}
              {props.image && (
                <Image source={{ uri: props.image }} style={styles.image} />
              )}
            </View>
            <View style={{ marginLeft: 30, marginTop: 10 }}>
              <Text style={styles.name} numberOfLines={1}>
                {props.name}
              </Text>
              {props.online === 0 ? (
                <Text style={[styles.schedule, { marginTop: 10 }]}>{props.time}</Text>
              ) : null}

              {props.online === 0 && props.groupNumber != null ? (
                <Text style={[styles.name, {
                  marginTop: 10
                  // paddingHorizontal: resize(7),
                }]} numberOfLines={1}>
                  {"Group " + props.groupNumber}
                </Text>
              ) : null}

              {props.isOnline ?
                <View style={styles.buttonContainer1}>
                  <TouchableOpacity
                    style={{
                      width: 30,
                      marginBottom: 10
                    }}
                    onPress={() => props.onPressDone()}
                  >
                    <Image
                      source={require("../../../assets/icons/auditions/done.png")}
                      style={{ height: 30, width: 30, resizeMode: "contain" }}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      width: 30,
                      marginBottom: 10
                    }}
                    onPress={() => props.onSaveLater()}
                  >
                    <Image
                      source={Assets.saveLater}
                      style={{ height: 30, width: 30, resizeMode: "contain" }}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      width: 30,
                      marginBottom: 10
                    }}
                    onPress={() => props.onPressClose()}
                  >
                    <Image
                      source={require("../../../assets/icons/auditions/close.png")}
                      style={{ height: 30, width: 30, resizeMode: "contain" }}
                    />
                  </TouchableOpacity>
                </View>
                : null}
            </View>
          </TouchableOpacity>
          <View style={{
            flexDirection: "row", borderRadius: 16, marginTop: 10,
            borderColor: colors.PRIMARY, borderWidth: 2, padding: 5,
          }}>
            <TouchableOpacity onPress={props.onAddComment}>
              <Image source={Assets.plus} style={{ width: 20, height: 20, marginHorizontal: 10 }} resizeMode={"contain"} />
            </TouchableOpacity>
            <TextInput
              placeholder={"Add Comment"}
              placeholderTextColor={colors.PRIMARY}
              style={{ fontFamily: NEXALIGHT, height: resize(100), flex: 1 }}
              multiline={true}
              onChangeText={props.onCommentChange}
              value={props.comment}
            />
          </View>
        </Card>
      </View>
      :
      <View>
        {props.isSelectionMode ? (
          <TouchableOpacity
            style={[
              styles.container,
              {
                position: "absolute",
                backgroundColor: "rgba(0, 0, 0, 0.25)",
                zIndex: 999,
                justifyContent: "center",
                alignItems: "center"
              }
            ]}
            activeOpacity={1}
            onPress={props.onSelectClick}
          >
            <Image
              source={props.isSelected ? Assets.selected : Assets.unselected}
              style={{ height: 40, width: 40 }}
              resizeMode="contain"
            />
          </TouchableOpacity>
        ) : null}
        <TouchableOpacity
          onPress={props.onPress}
          //style={props.containerStyle}
          style={[
            styles.container,
            {
              height: props.isOnline
                ? resize(270, "height")
                : resize(230, "height")
            }
          ]}
          disabled={!props.touchable}
        >
          <View style={styles.imageContainer}>
            {props.favorite && (
              <Image source={startIcon} style={styles.startIcon} />
            )}
            {/*this line had an uri*/}
            {props.image && (
              <Image source={{ uri: props.image }} style={styles.image} />
            )}
          </View>
          <View style={styles.infoContainer}>
            <Text style={styles.name} numberOfLines={1}>
              {props.name}
            </Text>
            {props.online === 0 ? (
              <Text style={styles.schedule}>{props.time}</Text>
            ) : null}
          </View>
          {props.online === 0 && props.groupNumber != null ? (
            <Text style={[styles.name, {
              paddingHorizontal: resize(7)
            }]} numberOfLines={1}>
              {"Group " + props.groupNumber}
            </Text>
          ) : null}
          {props.isOnline ? (
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={{
                  width: 30,
                  marginBottom: 10
                }}
                onPress={() => props.onPressDone()}
              >
                <Image
                  source={require("../../../assets/icons/auditions/done.png")}
                  style={{ height: 30, width: 30, resizeMode: "contain" }}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  width: 30,
                  marginBottom: 10
                }}
                onPress={() => props.onSaveLater()}
              >
                <Image
                  source={Assets.saveLater}
                  style={{ height: 30, width: 30, resizeMode: "contain" }}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  width: 30,
                  marginBottom: 10
                }}
                onPress={() => props.onPressClose()}
              >
                <Image
                  source={require("../../../assets/icons/auditions/close.png")}
                  style={{ height: 30, width: 30, resizeMode: "contain" }}
                />
              </TouchableOpacity>
            </View>

          ) : null}
        </TouchableOpacity>
      </View>
  );
}

Partaker.defaultProps = {
  onPress: () => { },
  name: "",
  // time: "",
  date: "",
  image: null,
  containerStyle: styles.container,
  styleManage: true,
  favorite: false,
  touchable: true,
  online: 1
};
