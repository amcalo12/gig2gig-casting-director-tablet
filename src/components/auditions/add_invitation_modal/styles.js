import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXABOLD, height, NEXALIGHT, STATUS_BAR_HEIGHT } from '../../../assets/styles';

const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT;

const styles = StyleSheet.create({
	nBContainer: {
		backgroundColor: 'transparent'
	},
	dismissButton: {
		width: '100%',
		height: containersHeight,
		position: 'absolute',
		justifyContent: 'flex-end'
	},
	blurView: {
		width: '100%',
		position: 'absolute',
		height: resize(730, 'height'),
		backgroundColor: '#f0f0f0',
		opacity: Platform.OS === 'ios' ? 0.84 : .60,
	},
	cardContainer: {
		width: '100%',
		height: containersHeight,
		justifyContent: 'flex-end',
		alignItems: 'flex-end'
	},
	shape: {
		alignSelf: 'center',
		width: 0,
		height: 0,
		borderLeftWidth: resize(20),
		borderRightWidth: resize(20),
		borderBottomWidth: resize(Platform.OS === 'ios' ? 40 : 20, 'height'),
		borderStyle: 'solid',
		backgroundColor: 'transparent',
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#FFF',
		position: 'absolute',
		top: -resize(Platform.OS === 'ios' ? 40 : 20, 'height')
	},
	card: {
		width: resize(275),
		height: resize(328, 'height'),
		shadowColor: 'rgba(0, 0, 0, 0.16)',
		shadowOffset: { width: 3, height: 0 },
		shadowRadius: resize(6),
		borderRadius: resize(13),
		backgroundColor: '#ffffff',
		marginBottom: resize(Platform.OS === 'ios' ? 330 : 330, 'height'),
		marginRight: resize(28),
		elevation: 20
	},
	button: {
		marginTop: resize(22, 'height'),
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		width: '80%',
		alignSelf: 'center',
		height: resize(40, 'height'),
		borderRadius: resize(18, 'height'),
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 1,
		backgroundColor: '#ffffff',
		flexDirection: 'row',
		marginBottom: resize(5, 'height')
	},
	buttonText: {
		textAlign: 'center',
		color: '#4d2545',
		fontFamily: NEXALIGHT,
		fontSize: resize(38),
		fontWeight: '400',
		letterSpacing: resize(0.65)
	},
	buttonAddText: {
		textAlign: 'center',
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(13),
		fontWeight: '400',
		letterSpacing: resize(0.65)
	}
})

export default styles;