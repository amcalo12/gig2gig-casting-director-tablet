import React from 'react';
import { View, TouchableOpacity, Text, Modal, ScrollView } from 'react-native';
import { Container, Content } from 'native-base';

//custom
import styles from './styles';
import InvitationItem from '../invitation_item';
import AddInvitationForm from '../add_invitation_form';

export default function AddInvitationModal(props) {
	return (
		<Modal
			animationType='fade'
			animated={true}
			visible={props.show}
			transparent={true}
			onRequestClose={props.dismiss}
		>
			<Container style={styles.nBContainer}>
				<Content bounces={false}>
					<View style={styles.cardContainer}>
						<TouchableOpacity
							style={styles.dismissButton}
							onPress={props.dismiss}
							activeOpacity={1}
						>
							<View style={styles.blurView} />
						</TouchableOpacity>
						<View style={styles.card}>
							<View style={styles.shape} />
							{
								!props.shouldBeInvite && (
									<TouchableOpacity
										style={styles.button}
										onPress={props.enableAddInvitation}
									>
										<Text style={styles.buttonText}> + </Text>
										<Text style={styles.buttonAddText}> Add Contributor  </Text>
									</TouchableOpacity>
								)
							}
							{
								props.shouldBeInvite && (
									<AddInvitationForm
										textValue={props.newInvitationEmail}
										onChange={props.onChangeEmail}
										onPress={props.sendInvitation}
									/>
								)
							}
							<ScrollView>
								{
									props.invitations.length !== 0 && (
										props.invitations.map((invitation, index) => (
											<InvitationItem
												{...invitation}
												status={invitation.status === 1 ? true : false}
												email={invitation.email ? invitation.email : invitation.contributor_info.email}
												key={index}
												delete={() => props.delete(invitation)}
												disabled={invitation.hasOwnProperty('id')}
											/>
										))
									)
								}
							</ScrollView>
						</View>
					</View>
				</Content>
			</Container>
		</Modal>
	)
}

AddInvitationModal.defaultProps = {
	show: true,
	invitations: [],
	shouldBeInvite: true,
	newInvitationEmail: '',
	dismiss: () => null,
	enableAddInvitation: () => null,
	onChangeEmail: () => null,
	sendInvitation: () => null,
}