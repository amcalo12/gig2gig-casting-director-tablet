import { StyleSheet } from 'react-native';

//custom
import { resize, BREAK_REGULAR, NEXABOLD, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
    containerLabel: {
        paddingHorizontal: resize(5),
        paddingVertical: resize(5),
        //width: resize(221),
        //height: resize(150, 'height'),
        width: 'auto',
        height: 'auto',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        backgroundColor: 'white',
        borderRadius: 13,
        position: 'absolute',
        top: resize(110, 'height'),
        right: resize(15),
        zIndex: 2,
        alignContent: 'flex-end'
    },
    flipCamera: {
        width: '100%',
        padding: 6,
        borderWidth: 3,
        borderColor: '#4D2545',
        borderRadius: 8,
        marginBottom: resize(8, 'height'),
    },
    flipCameraText: {
        color: '#4D2545',
        fontFamily: NEXABOLD
    },
    exitCheckIn: {
        width: '100%',
        padding: 6,
        borderWidth: 3,
        borderColor: '#4D2545',
        borderRadius: 8,
    },
    exitCheckInText: {
        color: '#4D2545',
        fontFamily: NEXABOLD
    },
    labelsContainer: {
        flexDirection: 'row',
    },
    icon: {
        marginRight: resize(5),
        resizeMode: 'contain',
        width: resize(44),
        height: resize(44, 'height'),
    },
    centerVertical: {
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default styles;