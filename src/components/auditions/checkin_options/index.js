import React from 'react';
import { TouchableOpacity, Image, View, Text } from 'react-native';

//custom
import styles from './styles';
import FlipCamera from '../../../assets/images/auditions/Group2243.png';
import ExitCheckIn from '../../../assets/images/auditions/noun_signout_1157133.png';
import en from '../../../locale/en';

function CheckInButtonOptions(props) {
	return (
		<View style={styles.containerLabel}>
			{
				props.flipCamera && (
					<TouchableOpacity onPress={props.OnPressFlipCamera} style={styles.flipCamera}>
						<View style={styles.labelsContainer}>
							<Image style={styles.icon} source={FlipCamera} />
							<View style={styles.centerVertical}>
								<Text style={styles.flipCameraText}>{en.checkin_flip_camera}</Text>
							</View>
						</View>
					</TouchableOpacity>
				)
			}
			{
				props.backBtn && (
					<TouchableOpacity onPress={props.OnPressExit} style={styles.exitCheckIn}>
						<View style={styles.labelsContainer}>
							<Image style={styles.icon} source={ExitCheckIn} />
							<View style={styles.centerVertical}>
								<Text style={styles.exitCheckInText}>{en.exit_checkin}</Text>
							</View>
						</View>
					</TouchableOpacity>
				)
			}
		</View>
	);
}

CheckInButtonOptions.defaultProps = {
	OnPressExit: () => { },
	flipCamera: false,
	backBtn: true,
	OnPressFlipCamera: () => { }
}

export default CheckInButtonOptions;