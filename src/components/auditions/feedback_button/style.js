import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXALIGHT, NEXABOLD } from '../../../assets/styles';

const style = StyleSheet.create({
  container: {
    width: resize(130),
    height: resize(33, 'height'),
  },
  gradient: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 28,
    borderColor: '#4d2545',
    borderWidth: 3,
  },
  gradientSelected: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 28,
  },
  buttonText: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(14),
    fontWeight: '400',
    paddingVertical: resize(5, 'height')
  },
  buttonTextSelected: {
    color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: resize(14),
    fontWeight: '400',
    paddingVertical: resize(5, 'height')
  }
});

export default style;