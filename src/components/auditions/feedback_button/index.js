import React from 'react';
import { TouchableOpacity, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

//custom 
import styles from './style';
import { gradient } from '../../../assets/styles';

export default function FeedbackButton(props) { 
  return(
    <TouchableOpacity
      style={styles.container}
      onPress={props.onPress}
    >
      <LinearGradient
        colors={
          props.selected ? gradient : props.gradient
        }
        style={
          props.selected ? styles.gradientSelected : styles.gradient
        }
      >
        <Text 
          style={
            props.selected ? styles.buttonTextSelected : styles.buttonText
          }
        >
          { props.buttonText }
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  );
}

FeedbackButton.defaultProps = {
  onPress: () => {},
  buttonText: '',
  selected: false,
  gradient: ['#ffffff', '#ffffff']
}