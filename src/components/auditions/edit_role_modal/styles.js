import { StyleSheet, Platform } from 'react-native';

//custom
import { resize, NEXABOLD, height, width, NEXALIGHT, STATUS_BAR_HEIGHT } from '../../../assets/styles';

const containersHeight = Platform.OS === 'ios' ? height : height - STATUS_BAR_HEIGHT

export default StyleSheet.create({
	nBContainer: { backgroundColor: 'transparent' },
	dismissButton: {
		width: '100%',
		height: containersHeight,
		position: 'absolute',
		justifyContent: 'flex-end'
	},
	blurView: {
		width: '100%',
		height: resize(Platform.OS === 'ios' ? 732 : 729, 'height'),
		backgroundColor: '#f0f0f0',
		opacity: 0.84,
	},
	cardContainer: {
		width: '100%',
		height: containersHeight,
		justifyContent: 'flex-end'
	},
	card: {
		alignSelf: 'center',
		width: resize(389),
		justifyContent: 'space-between',
		height: resize(664, 'height'),
		shadowColor: 'rgba(0, 0, 0, 0.16)',
		shadowOffset: { width: 3, height: 0 },
		shadowRadius: resize(6),
		borderRadius: resize(13),
		backgroundColor: '#ffffff',
		marginBottom: resize(33, 'height')
	},
	closeButton: {
		paddingHorizontal: resize(18),
		paddingVertical: resize(21, 'height')
	},
	image: {
		resizeMode: 'cover',
		alignSelf: 'center',
		width: resize(127),
		height: resize(127),
		...Platform.select({
			ios: {
				borderRadius: (resize(127) / 2)
			},
			android: {
				borderRadius: width / 2
			}
		})
	},
	textContainer: {
		paddingHorizontal: resize(50)
	},
	name: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(29),
		fontWeight: '400',
		letterSpacing: resize(1.45),
		marginTop: resize(47, 'height')
	},
	description: {
		marginTop: resize(18),
		color: '#000000',
		fontFamily: NEXALIGHT,
		fontSize: resize(16),
		fontWeight: '400',
		letterSpacing: resize(0.8)
	},
	buttonContainers: {
		marginBottom: resize(27, 'height'),
		alignItems: 'center',
		height: resize(131, 'height'),
		justifyContent: 'space-between',
		width: '100%',
		textAlign: 'justify'
	},
	addButton: {
		shadowColor: 'rgba(0, 0, 0, 0.4)',
		shadowOffset: { width: 0, height: 0 },
		shadowRadius: resize(3),
		borderRadius: resize(28),
		borderColor: '#4d2545',
		borderStyle: 'solid',
		borderWidth: 1,
		backgroundColor: '#ffffff',
		width: '80%',
		alignSelf: 'center'
	},
	addButtonText: {
		color: '#4d2545',
		fontFamily: NEXABOLD,
		fontSize: resize(20),
		fontWeight: '400',
		letterSpacing: 1,
	}
})