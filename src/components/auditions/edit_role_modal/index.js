import React from 'react';
import { View, TouchableOpacity, Text, Image, Modal } from 'react-native';
import { Container, Content } from 'native-base';

//custom
import styles from './styles';
import icon_close from '../../../assets/icons/commons/icon_close.png';
import defaultRoleImage from '../../../assets/icons/auditions/defaultRoleImage.png';
import GradientButton from '../../commons/gradient_button';

export default function EditRoleModal(props) {
	return (
		<Modal
			animationType='fade'
			animated={true}
			visible={props.show}
			transparent={true}
			onRequestClose={props.dismiss}
		>
			<Container style={styles.nBContainer}>
				<TouchableOpacity
					activeOpacity={1}
					style={styles.dismissButton}
					onPress={props.dismiss}
				>
					<View style={styles.blurView} />
				</TouchableOpacity>
				<Content bounces={false}>
					<View style={styles.cardContainer}>
						<View style={styles.card}>
							<View>
								<TouchableOpacity
									style={styles.closeButton}
									onPress={props.dismiss}
								>
									<Image source={icon_close} />
								</TouchableOpacity>
								{
									props.cover === null && (
										<Image
											source={props.image}
											style={styles.image}
										/>
									)
								}
								{
									props.cover !== null && (
										<Image
											source={props.cover}
											style={styles.image}
										/>
									)
								}
								<View style={styles.textContainer}>
									<Text style={styles.name}>
										{props.name}
									</Text>
									<Text style={styles.description}>
										{props.description}
									</Text>
								</View>
							</View>
							<View style={styles.buttonContainers}>
								<GradientButton
									containerStyle={styles.addButton}
									colors={['#FFF', '#FFF']}
									buttonText='Edit'
									textStyle={styles.addButtonText}
									onPress={props.edit}
								/>
							</View>
						</View>
					</View>
				</Content>
			</Container>
		</Modal>
	)
}

EditRoleModal.defaultProps = {
	dismiss: () => null,
	delete: () => null,
	edit: () => null,
	show: true,
	image: defaultRoleImage,
	name: "Aladdin",
	description: "Aladdin is a loyal, yet mischievous and tricky character, who often uses his agility and brain to get out of trouble, unlike most other Disney heroes. Aladdin is described to be kind-hearted, and also sweet to his friends."
}