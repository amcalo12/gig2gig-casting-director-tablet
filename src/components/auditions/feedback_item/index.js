import React from "react";
import { View, Text, Image } from "react-native";

//custom
import styles from "./style";
import StarEmoji from "../../../assets/icons/auditions/star-emoji.png";
import SmileEmoji from "../../../assets/icons/auditions/smile-emoji.png";
import GoodEmoji from "../../../assets/icons/auditions/good-emoji.png";
import DoubtEmoji from "../../../assets/icons/auditions/doubt-emoji.png";
import BadEmoji from "../../../assets/icons/auditions/bad-emoji.png";
import LinearGradient from "react-native-linear-gradient";
import { gradient } from "../../../assets/styles";

export default function FeedbackItem(props) {
  let icon;

  switch (props.icon) {
    case 1:
      icon = StarEmoji;
      break;
    case 2:
      icon = SmileEmoji;
      break;
    case 3:
      icon = GoodEmoji;
      break;
    case 4:
      icon = DoubtEmoji;
      break;
    case 5:
      icon = BadEmoji;
      break;
    default:
      break;
  }
  return (
    <View style={styles.container}>
      <View style={styles.contributorNameContainer}>
        <Text style={styles.contributorName}>{props.contributorName}</Text>
      </View>
      <View
        style={[
          styles.detailContainer,
          props.round ? (props.round === 1 ? { width: "60%" } : {}) : {},
        ]}
      >
        {console.log("icon :>> " + props.round)}
        {icon != null ? (
          <View style={styles.iconContainer}>
            <Image source={icon} style={styles.icon} />
          </View>
        ) : null}
        {props.round && props.round !== 1 ? (
          <View style={styles.callbackContainer}>
            <View style={styles.callbackTitleContainer}>
              <Text style={styles.title}>Call Back</Text>
            </View>
            <LinearGradient style={styles.buttonFeedback} colors={gradient}>
              <Text style={styles.buttonFeedbackText}>
                {props.callback === 0 ? "No" : "Yes"}
              </Text>
            </LinearGradient>
          </View>
        ) : null}
        {props.round && props.round !== 1 ? (
          <View style={styles.workOnContainer}>
            <View style={styles.workOnTitleContainer}>
              <Text style={styles.title}>Work On</Text>
            </View>
            <LinearGradient style={styles.buttonFeedback} colors={gradient}>
              <Text style={styles.buttonFeedbackText}>{props.workOn}</Text>
            </LinearGradient>
          </View>
        ) : null}
        {props.round && props.round === 1 ? (
          <View style={styles.workOnContainer}>
            <View style={styles.workOnTitleContainer}>
              <Text style={styles.title}>Feedback</Text>
            </View>
            <LinearGradient style={styles.buttonFeedback} colors={gradient}>
              <Text style={styles.buttonFeedbackText}>
                {props.simple_feedback ? props.simple_feedback : ""}
              </Text>
            </LinearGradient>
          </View>
        ) : null}
        <View style={styles.workOnContainer}>
          <View style={styles.workOnTitleContainer}>
            <Text style={styles.title}>Rating</Text>
          </View>
          <LinearGradient style={styles.buttonFeedback} colors={gradient}>
            <Text style={styles.buttonFeedbackText}>{props.rating}</Text>
          </LinearGradient>
        </View>
      </View>
    </View>
  );
}

FeedbackItem.defaultProps = {
  contributorName: "",
  icon: "",
  callback: "",
  workOn: "",
};
