import { StyleSheet } from "react-native";

//custom
import { resize, NEXALIGHT, NEXABOLD } from "../../../assets/styles";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginBottom: resize(28),
    height: resize(50, "height"),
    paddingHorizontal: resize(54),
  },
  contributorNameContainer: {
    marginBottom: resize(9, "height"),
  },
  contributorName: {
    color: "#4d2545",
    fontFamily: NEXALIGHT,
    fontSize: resize(10),
    fontWeight: "400",
  },
  detailContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  iconContainer: {
    width: resize(31),
    height: resize(31, "height"),
    borderRadius: 4,
    borderColor: "#4d2545",
    borderWidth: 3,
  },
  icon: {
    width: resize(24),
    height: resize(25, "height"),
    resizeMode: "contain",
  },
  callbackContainer: {
    // flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
  callbackTitleContainer: {
    marginHorizontal: resize(11),
  },
  title: {
    color: "#4d2545",
    fontFamily: NEXABOLD,
    fontSize: resize(11),
    fontWeight: "400",
  },
  buttonFeedback: {
    width: resize(77),
    height: resize(20, "height"),
    borderRadius: 28,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
  buttonFeedbackText: {
    color: "#ffffff",
    fontFamily: NEXABOLD,
    fontSize: resize(11),
    fontWeight: "400",
    // marginTop: 10,
    textAlign: "center",
    textAlignVertical: "center",
    textTransform: "capitalize",
  },
  workOnContainer: {
    // flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
  workOnTitleContainer: {
    marginHorizontal: resize(11),
  },
});

export default styles;
