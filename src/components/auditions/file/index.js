import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

//custom
import styles from './style';
import { gradient } from '../../../assets/styles';
import iconPdf from '../../../assets/icons/auditions/doc.png';
import iconMp3 from '../../../assets/icons/auditions/music.png';
import iconMore from '../../../assets/icons/auditions/more-icon.png';
import iconMp4 from '../../../assets/icons/auditions/video.png';
import iconImage from '../../../assets/icons/auditions/pic-icon.png';
import iconLink from '../../../assets/icons/auditions/link.png';
import colors from '../../../utils/colors';

export default function File(props) {
	let icon;
	let type;
	let isFullSize = false
	console.log('filetype new:', props.file);
	if (props.fileType === 1 || props.fileType === 'mp3' || props.fileType === 'audio') {
		icon = iconMp3;
		type = 'MP3'
	} else if (props.fileType === 2 || props.fileType === 'mp4' || props.fileType === 'video') {
		// icon = iconMp4;
		console.log('check :', props.file != undefined && props.file.thumbnail != null ? { uri: props.file.thumbnail } : iconMp4);
		icon = props.file != undefined && props.file.thumbnail != null ? { uri: props.file.thumbnail } : iconMp4;
		isFullSize = props.file != undefined && props.file.thumbnail != null ? true : false
		type = 'MP4'
	} else if (props.fileType === 3 || props.fileType === 'pdf' || props.fileType === 'doc') {
		icon = iconPdf;
		// type = props.type ? props.type : 'PDF'
		type = 'PDF'
	} else if (props.fileType === 4 || props.fileType === 'jpeg' || props.fileType === 'jpg' || props.fileType === 'png' || props.fileType === 'gif' || props.fileType === 'image') {
		// icon = iconImage;
		icon = props.file != undefined && props.file.thumbnail != null ? { uri: props.file.thumbnail } : iconImage;
		isFullSize = props.file != undefined && props.file.thumbnail != null ? true : false
		type = 'IMAGE'
	} else if (props.fileType === 5 || props.fileType === 'sheet') {
		icon = iconLink
		type = 'URL'
	} else {
		icon = null
	}

	return (
		<View
			ref={component => props.refs(component)}
			style={props.containerStyle}
		>
			{console.log('isFullSize :', isFullSize)}
			{!isFullSize ? <LinearGradient
				style={styles.iconContainer}
				colors={gradient}
			>
				{console.log('icon :', icon)}
				<Image
					source={icon}
					style={[styles.icon, { tintColor: colors.WHITE }]}
				/>
				<Text style={styles.type}>
					{type}
				</Text>
			</LinearGradient> :
				<Image
					resizeMode={"cover"}
					source={icon}
					style={[styles.iconContainer]}
				/>}
			<View style={styles.filenameContainer}>
				<Text
					style={styles.filename}
					numberOfLines={1}
				>
					{props.filename}
				</Text>
			</View>
			<TouchableOpacity
				style={styles.moreContainer}
				onPress={props.onPressMore}
			>
				<Image
					source={iconMore}
					style={styles.moreIcon}
				/>
			</TouchableOpacity>
		</View>
	);
}

File.defaultProps = {
	containerStyle: styles.container,
	onPressMore: () => { },
	filename: '',
	fileType: '',
	refs: () => null,
};
