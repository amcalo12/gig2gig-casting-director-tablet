import { StyleSheet } from 'react-native';

//custom
import { resize, NEXALIGHT, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    height: resize(66, 'height'),
    borderRadius: 10,
    backgroundColor: '#ffffff',
    marginBottom: resize(21, 'height'),
  },
  iconContainer: {
    height: '100%',
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 0,
  },
  icon: {
    resizeMode: 'contain',
    width: resize(28),
    height: resize(21, 'height')
  },
  filenameContainer: {
    justifyContent: 'center',
    width: '75%',
  },
  filename: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(16),
    fontWeight: '400',
    paddingHorizontal: resize(16),
  },
  moreContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  moreIcon: {
    width: resize(46)
  },
  type: {
    color: '#ffffff',
    fontFamily: NEXABOLD,
    fontSize: resize(16),
    fontWeight: '400',
    paddingTop: resize(8, 'height'),
  }
});

export default styles;