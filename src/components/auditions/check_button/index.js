import React from 'react';
import { TouchableOpacity, Image } from 'react-native';

//custom
import styles from './style';
import CheckIcon from '../../../assets/icons/auditions/setting-icon.png';

export default function CheckButton(props) {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={props.onPress}
    >
      <Image
        source={CheckIcon}
        style={styles.icon}
      />
    </TouchableOpacity>
  );
}

CheckButton.defaultProps = {
  onPress: () => { }
}