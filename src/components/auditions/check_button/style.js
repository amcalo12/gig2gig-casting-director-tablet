import { StyleSheet } from 'react-native';

//custom
import { resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: '30%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    resizeMode: 'contain',
    width: resize(34),
    height: resize(34, 'height')
  }
});

export default styles;