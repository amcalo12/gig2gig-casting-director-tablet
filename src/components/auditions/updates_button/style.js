import { StyleSheet } from 'react-native';

//custom
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    borderRadius: 24,
    borderColor: '#4d2545',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    width: resize(275),
    height: resize(49, 'height'),
    marginRight: resize(25),
    marginBottom: resize(22, 'height'),
    flexDirection: 'row',
  },
  containerSelected: {
    borderRadius: 24,
    borderColor: '#4d2545',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#4d2545',
    justifyContent: 'center',
    alignItems: 'center',
    width: resize(275),
    height: resize(49, 'height'),
    marginRight: resize(25),
    marginBottom: resize(22, 'height'),
    flexDirection: 'row',
  },
  icon: {
    width: resize(16),
    height: resize(17, 'height'),
    resizeMode: 'contain',
    marginRight: resize(6),
  },
  title: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(16),
    fontWeight: '400',
  },
  titleSelected: {
    color: '#fff',
    fontFamily: NEXABOLD,
    fontSize: resize(16),
    fontWeight: '400',
  }
});

export default styles;