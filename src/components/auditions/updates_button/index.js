import React from 'react';
import { TouchableOpacity, Text, Image } from 'react-native';

//custom
import styles from './style';
import PlusIcon from '../../../assets/icons/sidebar/plusIcon.png'

export default function UpdatesButton(props) {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={
        props.selected ? styles.containerSelected : styles.container
      }
    >
      {
        props.showIcon && (
          <Image
            source={PlusIcon}
            style={styles.icon}
          />
        )
      }
      <Text
        style={
          props.selected ? styles.titleSelected : styles.title
        }
      >
        {props.title}
      </Text>
    </TouchableOpacity>
  );
}

UpdatesButton.defaultProps = {
  onPress: () => { },
  title: '',
  selected: false,
  showIcon: false,
  icon: PlusIcon
}