import React from 'react';
import { View, Image } from 'react-native';
import TimePicker from 'react-native-datepicker';

//custom 
import styles from './style';
import ClockIcon from '../../../assets/icons/auditions/clock-icon.png';


const isToday = (value) => {
	if (value) {
		console.log('value', value)
		let someDate = new Date(value);
		const today = new Date();
		console.log('today', today)
		console.log('someDate', someDate)
		return someDate.getDate() == today.getDate() &&
			someDate.getMonth() == today.getMonth() &&
			someDate.getFullYear() == today.getFullYear()
	}
	return false;
}

export default function BorderedTimepicker(props) {
	let today = isToday(props.dateA) ? new Date() : false;
	return (
		<View style={props.containerStyle}>
			<TimePicker
				mode='time'
				placeholder={props.placeholder}
				style={props.containerStyle}
				format='LT'
				confirmBtnText='Confirm'
				minDate={today}
				cancelBtnText='Cancel'
				date={props.value}
				onDateChange={(time) => props.onChange(time)}
				minuteInterval={10}
				timeZoneOffsetInMinutes={undefined}
				showIcon={false}
				customStyles={{
					dateInput: { ...props.containerStyle, ...props.inputStyle },
					dateText: { ...props.textStyle },
					dateTouchBody: { ...props.containerStyle },
					placeholderText: { ...props.placeholderTextStyle },
					btnTextConfirm: styles.btnTextConfirm,
					btnTextCancel: styles.btnTextCancel,
					disabled: props.disabledInput
				}}
				{...props}
			/>
			{
				props.withIcon && (
					<View style={styles.iconContainer}>
						<Image
							source={props.icon}
							style={styles.icon}
						/>
					</View>
				)
			}
		</View>
	);
}

BorderedTimepicker.defaultProps = {
	disabledInput: {},
	containerStyle: styles.container,
	textStyle: styles.text,
	placeholderTextStyle: styles.text,
	inputStyle: styles.input,
	value: null,
	onChange: () => { },
	placeholder: '',
	withIcon: true,
	icon: ClockIcon
}