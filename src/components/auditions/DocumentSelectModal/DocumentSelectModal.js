import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity } from 'react-native';
import colors from '../../../utils/colors';
import { NEXABOLD, resize, NEXALIGHT } from '../../../assets/styles';

export default class DocumentSelectModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Modal
                // animationType="slide"
                animationType="none"
                transparent={true}
                visible={this.props.isVisible}
                onRequestClose={this.props.onCloseClick}>
                <TouchableOpacity style={styles.modalContainer} onPress={this.props.onCloseClick} activeOpacity={1}>
                    <View style={styles.modalSubContainer}>
                        <Text style={{ alignSelf: "center", fontFamily: NEXABOLD, fontSize: resize(16) }}>{this.props.title}</Text>
                        <View style={{ marginVertical: 10, width: "90%", alignSelf: "center" }}>
                            <Button title={"Upload Files"} onClick={this.props.onFileUpload} />
                            <Button title={"Add Links"} onClick={this.props.onAddLink} />
                        </View>
                    </View>
                </TouchableOpacity>
            </Modal>
        );
    }
}

export const styles = StyleSheet.create({
    modalContainer: {
        flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: colors.SHADOW_COLOR
    },
    modalSubContainer: {
        borderRadius: 8, backgroundColor: colors.WHITE, shadowOpacity: 0.2, elevation: 5, width: "30%", padding: 20
    },
    buttonRight: {
        width: '100%',
        // height: '100%',
        marginTop: 10,
        borderRadius: 28,
        borderColor: '#4d2545',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    buttonRightText: {
        color: '#4d2545',
        fontFamily: NEXALIGHT,
        fontSize: resize(19),
        // fontWeight: '400',
    },
})


export const Button = (props) => {
    return (
        <TouchableOpacity
            style={styles.buttonRight}
            onPress={props.onClick}
        >
            <Text style={styles.buttonRightText}>
                {props.title}
            </Text>
        </TouchableOpacity>
    )
}