import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Platform } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { Icon } from 'native-base';

//custom 
import styles, { pickerSelectStyles } from './styles';

export default class BorderedPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedContact: { label: 'Contact Type', value: null },
        };
    }
    render() {
        const sports = [
            {
                label: 'Contact Type',
                value: null,
            },
            {
                label: 'Baseball',
                value: 'baseball',
            },
            {
                label: 'Hockey',
                value: 'hockey',
            },
        ];
        let { selectedContact } = this.state;
        return (
            <RNPickerSelect
                enablesReturnKeyAutomatically
                items={sports}
                onValueChange={(value) => { }}
                style={{
                    ...pickerSelectStyles,
                    iconContainer: styles.iconContainer,
                    placeholder: styles.placeholder,
                    inputAndroidContainer: styles.containerInput,
                    inputIOSContainer: styles.containerInput
                }}
                value={selectedContact}
                Icon={() => {
                    return <Icon style={styles.iconStyles} name={Platform.OS === 'ios' ? 'arrow-down' : "arrow-dropdown"} />
                }}
            />
        );
    }
}
