
import { StyleSheet, Platform } from 'react-native';

//custom 
import { resize, NEXABOLD } from '../../../assets/styles';

const styles = StyleSheet.create({
    iconContainer: {
        ...Platform.select({
            ios: {
                top: resize(3, 'height')
            },
            android: {
                alignSelf: 'flex-end',
            },
        })
    },
    placeholder: {
        ...Platform.select({
            android: {
            },
            ios: {
                paddingLeft: resize(5)
            }
        }),
        color: '#4d2545',
        height: '100%',
        fontFamily: NEXABOLD
    },
    containerInput: {
        flexDirection: 'row',
        width: '100%',
        color: 'red'
    },
    iconStyles: {
        alignSelf: 'flex-end'
    },
});
export const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: resize(16),
        paddingVertical: resize(8, 'height'),
        color: 'black',
        paddingLeft: resize(5),
        width: resize(20),
        width: '100%'
    },
    inputAndroid: {
        fontSize: resize(14),
        color: 'red',
        marginRight: resize(10), // to ensure the text is never behind the ico
        paddingLeft: resize(5),
        justifyContent: 'center',
        flexDirection: 'row',
    },
});
export default styles;
