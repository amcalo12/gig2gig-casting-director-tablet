import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

//Image
import Trash from '../../../assets/icons/auditions/trash-icon.png';

const Tags = ({ title, onPress, showDelete, style }) => (
    <View style={style}>
        <Text numberOfLines={1} style={styles.tagText}>{title}</Text>
        {
            showDelete
                ? <TouchableOpacity onPress={onPress}>
                    <Image resizeMode='contain' source={Trash} />
                </TouchableOpacity>
                : null
        }

    </View>
)

Tags.defaultProps = {
    showDelete: true,
    style: styles.container
}

export default Tags;
