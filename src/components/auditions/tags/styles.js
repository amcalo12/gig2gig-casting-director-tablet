import { StyleSheet } from 'react-native';
import { resize } from '../../../assets/styles';

const styles = StyleSheet.create({
    container: {
        width: '90%',
        alignSelf: 'center',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: resize(50)
    },
    tagText: {
        color: '#5f2543',
        fontSize: resize(16)
    }
})
export default styles;
