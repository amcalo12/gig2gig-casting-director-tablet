import React from 'react';
import { TouchableOpacity, View, Image, Text } from 'react-native';

//custom
import styles from './style';

export default function CardItem(props) {
  return(
    <TouchableOpacity
      onPress={props.onPress}
      style={styles.container}
    >
      <View style={styles.iconContainer}>
        <Image 
          source={props.icon}
          style={styles.icon}
        />
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          { props.title }
        </Text>
      </View>
    </TouchableOpacity>
  );
}

CardItem.defaultProps = {
  onPress: () => {},
  icon: null,
  title: ''
}