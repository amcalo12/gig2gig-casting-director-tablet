import { StyleSheet } from 'react-native';

//custom 
import { resize, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    height: resize(30, 'height'),
    marginBottom: resize(40, 'height'),
  },
  iconContainer: {
    marginRight: resize(15),
    width: resize('28'),
    height: resize(34, 'height'),
  },
  icon: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain'
  },
  titleContainer: {
    justifyContent: 'center',
    height: '100%'
  },
  title: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(23),
    fontWeight: '400',
    paddingVertical: resize(5, 'height')
  }
});

export default styles;