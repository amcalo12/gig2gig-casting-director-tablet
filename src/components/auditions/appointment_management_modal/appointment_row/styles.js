import { StyleSheet } from 'react-native'

//custom
import { resize, NEXABOLD } from '../../../../assets/styles';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%'
  },
  row: {
    flexDirection: 'row',
    width: resize(320),
    height: resize(35, 'height'),
    borderRadius: resize(35, 'height'),
    borderColor: '#4d2545',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    marginBottom: resize(19, 'height')
  },
  rowNumber: {
    width: resize(51),
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    borderColor: '#4d2545',
    borderStyle: 'solid',
    borderRightWidth: 1,
  },
  rowText: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(12),
    fontWeight: '400',
    letterSpacing: resize(0.85),
  },
  rowTime: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    borderColor: '#4d2545',
    borderStyle: 'solid',
    borderRightWidth: 1,
    width: resize(94),
  },
})