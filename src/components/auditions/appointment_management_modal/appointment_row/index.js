import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

//custom
import uncheck from '../../../../assets/icons/commons/unCheck.png';
import check from '../../../../assets/icons/commons/check.png';
import styles from './styles';

export default function AppointmentRow(props) {
  return (
    <View style={styles.container}>
      <View style={styles.row}>
        {
          props.numberRow !== null && (
            <View style={styles.rowNumber}>
              <Text style={styles.rowText}>
                {props.numberRow}
              </Text>
            </View>
          )
        }
        <View style={styles.rowTime}>
          <Text style={styles.rowText}>
            {props.time}
          </Text>
        </View>
        <View>
          <Text></Text>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => props.mark(!props.is_walk)}
        disabled={props.disabled}
      >
        <Image
          source={
            !props.is_walk
              ?
              uncheck
              :
              check
          }
        />
      </TouchableOpacity>
    </View>
  )
}

AppointmentRow.defaultProps = {
  mark: () => null,
  numberRow: null,
  time: "10:00 am",
  namePartaker: null,
  is_walk: false,
  disabled: false
}