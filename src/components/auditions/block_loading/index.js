import React from 'react';
import { Modal, View, Text } from 'react-native';
import { Spinner } from 'native-base';

//custom
import styles from './style';

// locale
import en from '../../../locale/en';

export default function BlockLoading(props) {
  return (
    <Modal
      animationType='fade'
      transparent={true}
      visible={props.show}
    >
      <View style={styles.container}>
        <Spinner
          color={styles.spinner.color}
        />
        <Text style={styles.text}>
          {en.uploading_audition}
        </Text>
      </View>
    </Modal>
  );
}

