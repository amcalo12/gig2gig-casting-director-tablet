import { StyleSheet } from 'react-native';

//custom
import { resize, NEXALIGHT } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(255,255,255,.84)',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spinner: {
    color: '#4d2545'
  },
  text: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(17),
    fontWeight: '400',
    paddingVertical: resize(5, 'height'),
  }
});

export default styles;
