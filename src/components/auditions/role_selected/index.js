import React from 'react';
import { View, Image, Text } from 'react-native';

//custom
import styles from './style';
import RoleSelectedIcon from '../../../assets/icons/auditions/role-selected.png';
import RoleUnSelectedIcon from '../../../assets/icons/auditions/role-unselected.png';

export default function RoleSelected(props) {
  return (
    <View
      style={
        [
          styles.container,
          props.selected ? styles.selected : {}
        ]
      }
    >
      <View style={styles.iconContainer}>
        <Image
          source={{ uri: props.image }}
          style={styles.roleIcon}
        />
      </View>
      <Text
        style={styles.roleName}
        numberOfLines={1}
      >
        {props.name}
      </Text>
    </View>
  );
}

RoleSelected.defaultProps = {
  selected: false,
  image: null,
  name: ''
}