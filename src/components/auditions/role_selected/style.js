import { StyleSheet, Platform } from 'react-native';

//custom 
import { resize, NEXABOLD, width } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    marginBottom: resize(20, 'height'),
    width: resize(74),
    height: resize(74, 'height'),
    paddingHorizontal: resize(13),
    paddingVertical: resize(5, 'height')
  },
  iconContainer: {
    width: '100%',
    height: resize(48, 'height'),
  },
  roleIcon: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    ...Platform.select({
      ios: {
        borderRadius: (resize(47) / 2)
      },
      android: {
        borderRadius: width / 2
      }
    })
  },
  roleName: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(12),
    fontWeight: '400',
    paddingTop: resize(4, 'height'),
    textAlign: 'center'
  },
  selected: {
    borderWidth: 1,
    borderColor: '#4d2545',
  }
});

export default styles;