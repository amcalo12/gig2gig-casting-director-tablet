import React from 'react';
import { View, TouchableOpacity, Text, Modal, ScrollView } from 'react-native';
import { Container, Content } from 'native-base';

//custom
import styles from './styles';
export default function DeleteNotificationModal(props) {
    return (
        <Modal
            animationType='fade'
            animated={true}
            visible={props.show}
            transparent={true}
            onRequestClose={props.dismiss}
        >
            <Container style={styles.nBContainer}>
                <Content bounces={false}>
                    <View style={styles.cardContainer}>
                        <TouchableOpacity
                            style={styles.dismissButton}
                            onPress={props.dismiss}
                            activeOpacity={1}
                        >
                            <View style={styles.blurView} />
                        </TouchableOpacity>
                        <View style={styles.card}>
                            <View style={styles.shape} />
                        </View>
                    </View>
                </Content>
            </Container>
        </Modal>
    )
}

DeleteNotificationModal.defaultProps = {
    show: true,
    dismiss: () => null,
}