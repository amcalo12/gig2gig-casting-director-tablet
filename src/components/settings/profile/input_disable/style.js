import { StyleSheet } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../../../assets//styles';

const styles = StyleSheet.create({
  containerDisable: {
    width: resize(290),
    height: resize(55, 'height'),
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
    marginBottom: resize(20, 'height'),
  },
  containerEnable: {
    width: resize(290),
    height: resize(55, 'height'),
    borderWidth: 1,
    borderColor: '#d6d6d6',
    borderRadius: 50,
    marginBottom: resize(20, 'height'),
  },
  input: {
    width: resize(290),
    height: resize(55, 'height'),
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
    paddingLeft: resize(20),
    borderColor: 'transparent',
    padding: 0,
    margin: 0,
  }
});

export default styles;