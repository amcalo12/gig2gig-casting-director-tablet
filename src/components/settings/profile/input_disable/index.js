import React from 'react';
import { View } from 'react-native';
import { Input } from 'native-base';

//custom
import styles from './style';

export default function InputDisable(props) {
  return (
    <View
      style={props.disable ? props.containerStyleDisable : props.containerStyleEnable}
    >
      <Input
        style={[props.style, { borderBottomWidth: props.isBorder ? 1 : 0 }]}
        placeholder={props.placeholder}
        placeholderTextColor={props.placeholderTextColor}
        value={props.value}
        onChangeText={(value) => props.onChange(value)}
        keyboardType={props.keyboardType}
        disabled={props.disable}
        {...props}
        autoCapitalize='none'
      />
    </View>
  );
}

InputDisable.defaultProps = {
  value: '',
  placeholder: '',
  placeholderTextColor: styles.input.color,
  style: styles.input,
  onChange: () => { },
  keyboardType: 'default',
  containerStyleDisable: styles.containerDisable,
  containerStyleEnable: styles.containerEnable,
  disable: true
}