import { StyleSheet, Platform } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(290),
    height: resize(55, 'height'),
    flexDirection: 'row',
    marginBottom: resize(20, 'height'),
  },
  inputDisable: {
    margin: 0,
    padding: 0,
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
    paddingLeft: resize(20),
    width: resize(290),
    height: resize(55, 'height'),
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
  },
  inputEnable: {
    margin: 0,
    padding: 0,
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
    paddingLeft: resize(20),
    width: resize(290),
    height: resize(55, 'height'),
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#d6d6d6',
    borderRadius: 50,
  },
  dropdownIcon: {
    resizeMode: 'contain',
    width: resize(12),
    height: resize(11, 'height'),
  },
  iconContainer: {
    ...Platform.select({
      ios: {
        right: 15,
      },
      android: {
        right: 20,
      }
    }),
    top: resize(22, 'height')
  },
  modalViewMiddle: {
    backgroundColor: '#FFF',
    borderBottomWidth: 1,
    height: resize(60, 'height'),
    borderBottomColor: '#d9d9d9'
  },
  modalViewBottom: {
    backgroundColor: '#fff',
    paddingTop: resize(60),
  },
  done: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
  }
});

export default styles;