import React from 'react';
import { View, Image } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

//custom
import styles from './style';
import dropdownIcon from '../../../../assets/icons/select_radius/dropdown.png';

export default function SelectDisable(props) {
  const placeholder = {
    label: props.placeholder,
    value: null,
    color: '#9EA0A4',
  };

  return (
    <View style={props.containerStyle}>
      {console.log('props.value :>> ', props.value)}
      <RNPickerSelect
        placeholder={placeholder}
        items={props.data}
        onValueChange={(value) => props.onChange(value)}
        style={{
          inputIOS: props.disable ? props.styleDisable : props.styleEnable,
          inputAndroid: props.disable ? props.styleDisable : props.styleEnable,
          iconContainer: styles.iconContainer,
          modalViewMiddle: styles.modalViewMiddle,
          modalViewBottom: styles.modalViewBottom,
          done: styles.done
        }}
        value={props.value}
        useNativeAndroidPickerStyle={false}
        placeholderTextColor={props.placeholderTextColor}
        Icon={() => {
          if (props.disable) {
            return null;
          }

          return (
            <Image
              source={dropdownIcon}
              style={styles.dropdownIcon}
            />
          )
        }}
        disabled={props.disable}
      />
    </View>
  );
}

SelectDisable.defaultProps = {
  placeholder: '',
  data: [],
  onChange: () => { },
  styleDisable: styles.inputDisable,
  styleEnable: styles.inputEnable,
  containerStyle: styles.container,
  placeholderTextColor: '#4d2545',
  value: '',
  disable: true
}