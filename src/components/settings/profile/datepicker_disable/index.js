import React from 'react';
import { View } from 'react-native';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';

//custom
import styles from './style';

export default function DatepickerDisable(props) {
  return (
    <View style={props.containerStyle}>
      {console.log('props.value :>> ', props.value)}
      <DatePicker
        style={props.inputContainer}
        mode='date'
        placeholder={props.placeholder}
        format='LL'
        confirmBtnText='Confirm'
        cancelBtnText='Cancel'
        onDateChange={(dateString, date) => props.onChange(date)}
        customStyles={{
          disabled: props.dateInputDisable,
          dateInput: props.disable ? props.dateInputDisable : props.dateInputEnable,
          dateText: styles.dateText,
          placeholderText: styles.dateText,
          btnTextConfirm: styles.btnTextConfirm,
          btnTextCancel: styles.btnTextCancel
        }}
        showIcon={false}
        timeZoneOffsetInMinutes={undefined}
        date={props.value !== null ? props.value : null}
        minDate={
          new Date(
            moment()
              .subtract(100, 'years')
              .format('L')
          )
        }
        maxDate={new Date()}
        disabled={props.disable}
      />
    </View>
  );
}

DatepickerDisable.defaultProps = {
  onChange: () => { },
  value: null,
  dateInputDisable: styles.inputDisable,
  dateInputEnable: styles.inputEnable,
  dateText: styles.dateText,
  placeholderText: styles.placeholderText,
  btnTextConfirm: styles.btnTextConfirm,
  btnTextCancel: styles.btnTextCancel,
  placeholder: '',
  containerStyle: styles.container,
  inputContainer: styles.inputContainer,
  disable: true
}