import { StyleSheet } from 'react-native';

//custom
import { NEXABOLD, resize } from '../../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    width: resize(290),
    height: resize(55, 'height'),
    flexDirection: 'row',
    marginBottom: resize(20, 'height'),
  },
  inputContainer: {
    width: resize(290),
    marginTop: resize(5, 'height')
  },
  inputEnable: {
    backgroundColor: '#fff',
    borderRadius: 50,
    height: resize(55, 'height'),
    paddingVertical: 5,
    borderColor: '#d6d6d6',
    borderRadius: 50,
  },
  inputDisable: {
    backgroundColor: '#fff',
    height: resize(55, 'height'),
    borderColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
  },
  dateText: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
    fontSize: resize(18),
    fontWeight: '400',
    paddingLeft: resize(20),
    textAlign: 'left',
    alignSelf: 'flex-start',
  },
  btnTextConfirm: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
  },
  btnTextCancel: {
    color: '#4d2545',
    fontFamily: NEXABOLD,
  }
});

export default styles;