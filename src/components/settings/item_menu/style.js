import { StyleSheet } from 'react-native';

//custom
import { NEXALIGHT, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    height: resize(74, 'height'),
    width: resize(546),
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: resize(5, 'height'),
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  title: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(20),
    fontWeight: '400',
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingRight: resize(15),
  },
  icon: {
    width: resize(12),
    height: resize(20, 'height'),
    resizeMode: 'contain'
  }
});

export default styles;