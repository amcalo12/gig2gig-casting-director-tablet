import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

//custom
import styles from './style';
import iconRight from '../../../assets/icons/settings/arrow_right.png';

export default function ItemMenu(props) {
  return(
    <TouchableOpacity
      onPress={props.onPress}
      style={props.style}
    >
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          {props.title}
        </Text>
      </View>
      <View style={styles.iconContainer}>
        <Image 
          source={iconRight}
          style={styles.icon}
        />
      </View>
    </TouchableOpacity>
  );
}

ItemMenu.defaultProps = {
  onPress: () => {},
  style: styles.container,
  title: ''
}