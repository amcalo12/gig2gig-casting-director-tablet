import { StyleSheet } from 'react-native';

//custom
import { resize, NEXABOLD } from '../../../assets/styles';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontFamily: NEXABOLD,
    fontSize: resize(16),
    fontWeight: '400',
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})