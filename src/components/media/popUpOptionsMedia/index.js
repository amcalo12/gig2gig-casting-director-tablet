import React, { Component } from 'react';
import {
	Platform,
	Modal,
	StatusBar,
	TouchableOpacity,
	Image
} from 'react-native';
import {
	Text,
	Card
} from 'native-base';
import styles from './index.styles';
import { Assets } from '../../../assets';

const isIos = Platform.OS === "ios";

export default class modalAlert extends Component {
	state = {
		modalVisible: false,
		modalStyles: null
	};

	setModalVisible() {

		if (Platform.OS === 'android') {
			if (!this.state.modalVisible) {
				StatusBar.setBackgroundColor('#4d2545', true);
			}
			else {
				StatusBar.setBackgroundColor('#FFFFFF', true)
			}
		}
		this.setState({
			modalVisible: !this.state.modalVisible
		});
	}

	// deleteUserFile() {
	// 	const { deleteFile, data } = this.props;
	// 	this.setModalVisible();
	// 	deleteFile(data.id);
	// }

	// async shareUserFile() {
	// 	const { shareFile, data } = this.props;
	// 	// const message = `${shareMsj.myMedia.message}\n${data.url}`
	// 	const shareresponse = await shareFile(data);
	// 	if (shareresponse) {
	// 		this.setModalVisible()
	// 	}

	// }

	// renameFile = async () => {
	// 	this.setModalVisible()
	// 	await this.props.renameFile(this.props.data)
	// }

	openUserFile() {
		const { openFile, data } = this.props;
		this.setModalVisible();
		openFile(this.setUrl(data.url));
	}

	setUrl = (url) => {
		var pattern = /^((http|https|ftp):\/\/)/;
		if (!pattern.test(url)) {
			url = "http://" + url;
		}
		return url;
	}

	render() {
		const { modalStyles } = this.state
		const { stylesData, data, deleteFile, renameFile, shareFile, openFile } = this.props;
		return (
			<Modal
				transparent={true}
				animationType="fade"
				visible={this.state.modalVisible}
				onRequestClose={() => {
					this.setModalVisible();
				}}>
				<TouchableOpacity activeOpacity={1} style={styles.container} onPress={() => {
					this.setModalVisible();
				}}>
					{console.log('stylesData ? (isIos ? stylesData.px : stylesData.px) : 0 :>> ', stylesData)}
					<Card
						style={[styles.options, { top: stylesData ? (stylesData.py + 15) : 0, left: stylesData ? (isIos ? stylesData.px : stylesData.px) : 0 }]}>
						{/* {shareFile &&
							<TouchableOpacity activeOpacity={0.6} style={styles.optionLabel} onPress={() => {
								this.shareUserFile()

							}}>
								<Image source={ShareIcon} />
								<Text style={styles.optionText}>Share</Text>
							</TouchableOpacity>
						} */}
						{openFile &&
							<TouchableOpacity activeOpacity={0.6} style={styles.optionLabel} onPress={() => this.openUserFile()}>
								<Image source={Assets.open_in} />
								<Text style={styles.optionText}>Open In</Text>
							</TouchableOpacity>
						}
						{/* {
							deleteFile &&
							<TouchableOpacity activeOpacity={0.6} style={styles.optionLabel} onPress={() => this.deleteUserFile()}>
								<Image source={DeleteIcon} />
								<Text style={styles.optionText}>Delete</Text>
							</TouchableOpacity>
						}
						{
							renameFile &&
							<TouchableOpacity activeOpacity={0.6} style={styles.optionLabel} onPress={() => this.renameFile()}>
								<Image source={Assets.rename} style={{ width: 15, height: 15, tintColor: colorPalette.mainButtonBg }}
									resizeMode={"contain"}
								/>
								<Text style={styles.optionText}>Rename</Text>
							</TouchableOpacity>
						} */}
					</Card>
				</TouchableOpacity>
			</Modal>
		);
	}
}