import React from "react";
import { TouchableOpacity, View, Text, Image, Linking } from "react-native";

//custom
import styles from "./styles";
import { Thumbnail } from "react-native-thumbnail-video";
import LinearGradient from "react-native-linear-gradient";

//Images
// import playBtn from "resources/images/playBtn.png";
// import ThumbnailImage from "resources/images/myMedia/video_thumbnail.png";

import openPlayer from "./playVideo";
import { gradientColors } from "../../../utils/constant";
import { Assets } from "../../../assets";
import { resize } from "../../../assets/styles";

const VideoCard = ({
  containerStyle,
  url,
  name,
  onDeletePress,
  onRename,
  isYouTubeUrl,
  onPress
}) => (
    <View style={[containerStyle]}>
      {isYouTubeUrl && (
        <Thumbnail
          imageHeight={resize(126, "h")}
          imageWidth={"100%"}
          containerStyle={styles.principalVideo}
          type="standard"
          showPlayIcon={false}
          onPress={() => { }}
          url={`https://www.youtube.com/watch?v=${url}`}
        />
      )}
      {!isYouTubeUrl && (
        <Image
          style={{
            height: resize(126, "height"),
            width: "100%"
          }}
          source={Assets.thumbVideo}
        />
      )}
      <LinearGradient colors={gradientColors} style={styles.nameContainer}>
        <Text style={styles.name}>{name}</Text>
        {/* <TouchableOpacity
          style={{
            position: "absolute",
            right: 40,
            height: resize(22, "height"),
            width: resize(20)
          }}
          onPress={() => {
            onRename();
          }}
        >
          <Image
            style={{
              height: resize(20, "height"),
              width: resize(20),
              resizeMode: "contain",
              tintColor: "#ffffff"
            }}
            source={Assets.rename}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            position: "absolute",
            right: 13,
            height: resize(22, "height"),
            width: resize(20)
          }}
          onPress={() => {
            onDeletePress();
          }}
        >
          <Image
            style={{
              height: resize(23, "height"),
              width: resize(20),
              resizeMode: "contain"
            }}
            source={DeleteWhiteIcon}
          />
        </TouchableOpacity> */}
      </LinearGradient>

      <View style={styles.btnPlay}>
        <TouchableOpacity
          style={styles.playContainer}
          onPress={onPress}
        >
          <Image style={styles.play} resizeMode="contain" source={Assets.play} />
        </TouchableOpacity>
      </View>
    </View>
  );

VideoCard.defaultProps = {
  containerStyle: styles.container,
  name: "1.31.19"
};

export default VideoCard;
