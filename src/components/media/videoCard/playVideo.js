import { Platform } from 'react-native';
import { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';

export default openPlayer = videoId => {
  Platform.OS === "ios"
    ? YouTubeStandaloneIOS.playVideo(videoId)
    : YouTubeStandaloneAndroid.playVideo({
        apiKey: "AIzaSyDc8IqTh0xqtg87gvYXjiaWDIpPbqo6rGY",
        videoId,
        autoplay: true
      });
};