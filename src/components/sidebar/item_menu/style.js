import { StyleSheet } from 'react-native';

//custom
import { NEXALIGHT, resize } from '../../../assets/styles';

const styles = StyleSheet.create({
  container: {
    height: resize(63, 'height'),
    flexDirection: 'row',
    paddingHorizontal: resize(16),
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: resize(5, 'height'),
    borderLeftWidth: 4,
    borderLeftColor: '#fff',
  },
  iconContainer: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: resize(38),
    height: resize(25, 'height'),
    resizeMode: 'contain'
  },
  titleContainer: {
    paddingLeft: resize(10),
    width: '70%',
    height: '100%',
    justifyContent: 'center',
  },
  title: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(20),
    fontWeight: '400',
    height: '100%',
    paddingTop: resize(21, 'height'),
  },
  countContainer: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  count: {
    color: '#4d2545',
    fontFamily: NEXALIGHT,
    fontSize: resize(16),
    fontWeight: '400',
    paddingTop: resize(5, 'height')
  },
  currectItem: {
    backgroundColor: '#f0f0f0',
    borderLeftWidth: 4,
    borderLeftColor: '#4d2545',
  }
});

export default styles;