import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

//custom
import styles from './style';

export default function ItemMenu(props) {
  return (
    <TouchableOpacity
      style={[styles.container, props.Height, props.active ? styles.currectItem : {}]}
      onPress={props.onPress}
    >
      <View style={styles.iconContainer}>
        <Image
          source={props.icon}
          style={styles.icon}
        />
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          {props.title}
        </Text>
      </View>
      <View style={styles.countContainer}>
        <Text style={styles.count}>
          {props.count}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

ItemMenu.defaultProps = {
  icon: null,
  title: '',
  onPress: () => { },
  current: false,
  count: '',
  active: false
}