import { StyleSheet } from 'react-native';
import { resize, NEXABOLD, NEXALIGHT } from '../../assets/styles';

const styles = StyleSheet.create({
    container: {
        width: '70%',
        alignSelf: 'center',
        height: resize(375, 'height'),
        marginVertical: resize(15),
        borderRadius: resize(10),
        borderColor: '#D6D6D6',
        borderWidth: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        paddingHorizontal: resize(15),
        paddingVertical: resize(15),
        backgroundColor: 'white'
    },
    img: {
        width: '100%',
        alignSelf: 'center',
        height: resize(200, 'height'),
        borderRadius: resize(10)
    },
    titleRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        height: resize(50, 'height'),
        marginTop: resize(15),
    },
    title: {
        color: '#4D2545',
        fontSize: resize(22),
        fontFamily: NEXABOLD,
        width: '75%'
    },
    posted: {
        color: '#4D2545',
        fontSize: resize(16),
        fontFamily: NEXALIGHT,
    },
    info: {
        fontSize: resize(16),
        fontFamily: NEXALIGHT,
    }
})
export default styles;
