import React from 'react';
import { View, Image, Text, TouchableOpacity, Spinner } from 'react-native';
import styles from './styles';

//Temporal
import Test from '../../assets//images/background_card/news.png'

const NewsUpdates = ({ onPress, title, body, time_ago, url_media }) => (
    <TouchableOpacity onPress={onPress} style={styles.container}>
        <Image
            placeholderStyle={{ backgroundColor: 'black' }}
            PlaceholderContent={
                <Spinner
                    color='black' />
            }
            style={styles.img}
            resizeMode='cover'
            source={{ uri: url_media ? url_media : '' }} />
        <View style={styles.titleRow}>
            <Text numberOfLines={1} style={styles.title}>{title}</Text>
            <Text numberOfLines={1} style={styles.posted}>{time_ago}</Text>
        </View>
        <Text numberOfLines={4} style={styles.info} >{body}</Text>
    </TouchableOpacity>
)
export default NewsUpdates;
