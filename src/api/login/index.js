//custom
import { dispatchRequest } from "../../api";

export const fetchAuth = async (email, password) => {
  return await dispatchRequest({
    method: "POST",
    url: "login",
    data: {
      email,
      password,
      type: 1
    }
  });
};

export const setDeviceToken = async (data = {}) =>
  dispatchRequest({
    method: "put",
    url: "t/notification-send-pushkey",
    data
  });
