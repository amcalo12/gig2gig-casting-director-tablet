import axios from "axios";
import { Keyboard } from "react-native";

//custom
import { store } from "../../store";
import settings from "../../settings";
import { INIT_SESSION } from "../actions/auth";

const state = async () => {
  return await store.getState();
};

const instance = axios.create({
  baseURL: settings.apiUrl,
});

export const dispatchRequest = async (request, dismiss = true) => {
  const _state = await state();

  console.log("GET TOKEN ==> " + _state.auth.token);
  console.log("ALL REQUEST ==> " + JSON.stringify(request));
  console.log("ALL URL ==> " + settings.apiUrl + request.url);

  instance.defaults.headers.common["Content-Type"] = "application/json";
  instance.defaults.headers.common["Accept"] = "application/json";
  instance.defaults.headers.common["Authorization"] = _state.auth.token;

  try {
    dismiss ? Keyboard.dismiss() : "";

    const resource = await instance(request);
    console.log("RESPONSE API SUCCESS ==> " + JSON.stringify(resource));
    return Promise.resolve(resource);
  } catch (error) {
    console.log("RESPONSE API ERROR ==> " + JSON.stringify(error));
    // let errorData = error.json()
    console.log("error :>> ", error.response.status);
    if (error.response.status == 401 && request.url != "login") {
      Promise.reject(error);
      store.dispatch({
        type: INIT_SESSION,
        payload: {
          authorize: false,
          token: "",
          isAdminSelect: true,
        },
      });
      return;
      // dispatch({
      //   type: INIT_SESSION,
      //   payload: {
      //     authorize: true,
      //     token: `Bearer ${response.data.access_token}`
      //   }
      // });
    }
    return Promise.reject(error);
  }
};
