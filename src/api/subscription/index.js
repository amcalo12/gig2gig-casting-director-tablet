//custom
import { dispatchRequest } from "../../api";

export const getAllPlanList = async () =>
    dispatchRequest({
        method: "get",
        url: "users/listSubscriptionPlans"
    });

export const getSubscriptionDetail = async () =>
    dispatchRequest({
        method: "get",
        url: "users/subscriptionDetails"
    });

export const inviteUser = async (data) => {
    return await dispatchRequest({
        method: 'post',
        url: `t/users/inviteCaster`,
        data
    });
}

export const changeStatus = async (data) => {
    return await dispatchRequest({
        method: 'post',
        url: `users/changeStatus`,
        data
    });
}

export const cancelSubscription = async () =>
    dispatchRequest({
        method: "get",
        url: "t/users/cancelSubscription"
    });

export const resumeSubscription = async () =>
    dispatchRequest({
        method: "get",
        url: "t/users/resumeSubscription"
    });

export const resendEmail = async (user_id) =>
    dispatchRequest({
        method: "get",
        url: `t/users/resendInvitation/${user_id}`
    });