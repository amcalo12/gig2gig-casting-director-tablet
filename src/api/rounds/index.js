import { dispatchRequest } from '../../api';


export const createNewRound = async (data = {}, audition_id) => {
	return await dispatchRequest({
		method: 'post',
		url: `t/appointment/${audition_id}/rounds`,
		data
	});
}

export const getRoundsByAudition = async (audition_id) => {
	return await dispatchRequest({
		method: 'get',
		url: `t/appointment/${audition_id}/rounds`
	});
}