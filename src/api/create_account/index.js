//custom
import uploadAsset from '../../utils/upload_asset';
import { dispatchRequest } from '../../api';

export const createAccount = async (data = {}) => {
  const imageUrl = await uploadAsset('tablet/users', data.image.uri);
  const thumbnail = await uploadAsset('tablet/users', data.image.thumbnail);

  data['image'] = imageUrl;
  data['thumbnail'] = thumbnail;
  // data['resource_name'] = `${data['name']}.jpg`;
  data['resource_name'] = data.fileName

  return await dispatchRequest({
    method: 'post',
    url: 'users/create',
    data: {
      ...data,
    }
  });
}