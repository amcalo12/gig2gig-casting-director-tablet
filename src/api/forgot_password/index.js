//custom
import { dispatchRequest } from '../../api';

export const resetPassword = async (email) => {
  return await dispatchRequest({
    method: 'POST',
    url: 'remember',
    data: {
      email
    }
  });
}