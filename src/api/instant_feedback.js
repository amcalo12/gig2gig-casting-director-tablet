import { dispatchRequest } from ".";


export const getSuggestedAudition = async (value) => {
    return await dispatchRequest({
        method: 'get',
        url: `t/auditions/find_by_title?value=${value}`
    });
};

export const addInstantFeedback = async (data = {}) =>
    dispatchRequest({
        method: 'post',
        url: `t/instantfeedbacks/add`,
        data
    });

export const addKeepForFuture = async (data = {}) =>
    dispatchRequest({
        method: 'post',
        url: `t/feedbacks/keepForFuture`,
        data
    });
