//custom
import { dispatchRequest } from '../../api';
import uploadAsset from '../../utils/upload_asset';

export const fetchCurrentUser = async (user_id) => {
	return await dispatchRequest({
		method: 'get',
		url: `t/users/show/${user_id}`,
	});
}

export const updateCurrentUser = async (user_id, data = {}) => {
	let dataForm = { ...data };
	console.log('dataForm.image :', dataForm.image);
	if (dataForm.hasOwnProperty('image') && dataForm.image.hasOwnProperty('uri')) {
		console.log('dataForm.image.uri :>> ', dataForm.image.uri);
		const image = await uploadAsset('tablet/users', dataForm.image.uri);
		console.log('dataForm.image.thumbnail :>> ', dataForm.image.thumbnail);
		const thumbnail = await uploadAsset('tablet/users', dataForm.image.thumbnail);

		dataForm['image'] = image
		dataForm['thumbnail'] = thumbnail
	}

	// dataForm['resource_name'] = `${dataForm['name']}.jpg`;
	dataForm['resource_name'] = dataForm.file_name != undefined ? dataForm.file_name : dataForm.name;

	return await dispatchRequest({
		method: 'put',
		url: `t/users/update/${user_id}`,
		data: {
			...dataForm
		}
	});
}

export const getSettingsInfo = async () => {
	return await dispatchRequest({
		method: 'get',
		url: 't/content-settings'
	});
}

export const getPushConfig = async () => {
	return await dispatchRequest({
		method: 'get',
		url: 'users/settings'
	});
}

export const setPushConfig = async (id, data) => {
	return await dispatchRequest({
		method: 'put',
		url: `/users/settings/${id}`,
		data
	});
}

export const feedbackUpdate = async params => {
	return await dispatchRequest({
		method: "post",
		url: `t/instantfeedbacks/changeDefault`,
		params: {
			...params
		}
	});
};

