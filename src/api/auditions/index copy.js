//custom
import { dispatchRequest } from "../../api";
import uploadAsset from "../../utils/upload_asset";
import uploadRoles from "../../utils/auditions/upload_roles";
import uploadFiles from "../../utils/auditions/upload_documents";
import validateUrl from "../../utils/validate_url";

export const createAudition = async (data = {}) => {
  if (data.cover != undefined) {
    // upload cover of audition
    const cover = await uploadAsset("tablet/auditions/cover", data.cover.uri);
    data["cover"] = cover;
    data["cover_name"] = `${data["title"]}.jpg`;
  }

  // upload roles and change url of asset
  const roles = await uploadRoles(data.roles);
  data["roles"] = roles;

  // upload documents and change url of asset
  const media = await uploadFiles(data.media);
  data["media"] = media;
  return await dispatchRequest({
    method: "post",
    url: "t/auditions/create",
    data: {
      ...data,
    },
  });
};

export const upcomingAuditions = async () => {
  return await dispatchRequest({
    method: "get",
    url: "t/auditions/upcoming",
  });
};

export const pastAuditions = async () => {
  return await dispatchRequest({
    method: "get",
    url: "t/auditions/passed",
  });
};

export const getAudition = async (audition_id) => {
  return await dispatchRequest({
    method: "get",
    url: `auditions/show/${audition_id}`,
  });
};

export const openAudition = async (audition_id) => {
  return await dispatchRequest({
    method: "put",
    url: `t/auditions/open/${audition_id}`,
    data: {
      status: 1,
    },
  });
};

export const closeAudition = async (audition_id) => {
  return await dispatchRequest({
    method: "put",
    url: `t/auditions/close/${audition_id}`,
    data: {
      status: 2,
    },
  });
};

export const closeRoound = async (round_id) => {
  return await dispatchRequest({
    method: "put",
    url: `t/appointment/${round_id}/rounds`,
    data: {
      status: false,
    },
  });
};

export const createGroup = async (params) => {
  return await dispatchRequest({
    method: "post",
    url: `t/group`,
    params: {
      ...params,
    },
  });
};

export const groupStatus = async (appointment_id) => {
  return await dispatchRequest({
    method: "get",
    url: `t/group/status/${appointment_id}`,
  });
};

export const closeGroup = async (appointment_id) => {
  return await dispatchRequest({
    method: "get",
    url: `t/group/close/${appointment_id}`,
  });
};

export const updateAudition = async (audition_id, formData = {}) => {
  let data = { ...formData };

  // upload cover of audition
  if (data.cover.hasOwnProperty("uri")) {
    if (!validateUrl(data.cover.uri)) {
      const cover = await uploadAsset("tablet/auditions/cover", data.cover.uri);

      data["cover"] = cover;
      data["cover_name"] = `${data["title"]}.jpg`;
    } else {
      data["cover"] = data["cover"].uri;
    }
  }

  // upload roles and change url of asset
  const roles = await uploadRoles(data.roles);
  data["roles"] = roles;

  // upload documents and change url of asset
  const media = await uploadFiles(data.media);
  data["media"] = media;

  return await dispatchRequest({
    method: "put",
    url: `t/auditions/update/${audition_id}`,
    data: {
      ...data,
    },
  });
};

export const getAuditionAppointments = async (audition_id) => {
  return await dispatchRequest({
    method: "get",
    url: `appointments/auditions/${audition_id}`,
  });
};

export const getWalkAppointments = async (audition_id) => {
  return await dispatchRequest({
    method: "get",
    url: `appointments/show/${audition_id}/walk`,
  });
};

export const getNoWalkAppointments = async (audition_id) => {
  return await dispatchRequest({
    method: "get",
    url: `appointments/show/${audition_id}/notwalk`,
  });
};

export const getCheckinWalkData = async (email) => {
  return await dispatchRequest({
    method: "get",
    url: "t/appointments/auditions",
    params: {
      email,
    },
  });
};

export const getCheckinData = async (user, role_id, appointment_id) => {
  return await dispatchRequest({
    method: "get",
    url: "t/appointments/auditions",
    params: {
      user,
      role_id,
      appointment_id,
    },
  });
};

export const completeCheckin = async (data = {}) => {
  return await dispatchRequest({
    method: "post",
    url: "appointments/auditions",
    data: {
      ...data,
    },
  });
};

export const getPerformanceProfile = async (user_id, appointment_id) => {
  return await dispatchRequest({
    method: "get",
    url: `t/auditions/profile/user/${user_id}/appointment/${appointment_id}`,
  });
};

// export const getPerformanceProfile = async user_id => {
//   return await dispatchRequest({
//     method: "get",
//     url: `t/auditions/profile/user/${user_id}`
//   });
// };
export const getAuditionNotifications = async (audition_id) => {
  return await dispatchRequest({
    method: "get",
    url: `monitor/show/${audition_id}`,
  });
};

export const getAuditionPreNotifications = async (audition_id) => {
  return await dispatchRequest({
    method: "get",
    url: `monitor/show/${audition_id}/pre`,
  });
};

export const createAuditionUpdate = async (data = {}) => {
  return await dispatchRequest({
    method: "post",
    url: "t/monitor/updates",
    data: {
      ...data,
    },
  });
};

export const getAuditionVideos = async (audition_id) => {
  return await dispatchRequest({
    method: "get",
    url: `t/auditions/video/list/${audition_id}`,
  });
};

export const getTeamFeedbacks = async (params) => {
  return await dispatchRequest({
    method: "get",
    url: "t/feedbacks/list",
    params: {
      ...params,
    },
  });
};

export const createFeedback = async (data = {}) => {
  return await dispatchRequest({
    method: "post",
    url: "t/feedbacks/add",
    data: {
      ...data,
    },
  });
};

export const getPerformanceCalendar = async (user_id) => {
  return await dispatchRequest({
    method: "get",
    url: `t/user/${user_id}/calendar`,
  });
};

export const createAuditionVideo = async (data = {}) => {
  return await dispatchRequest({
    method: "post",
    url: "t/auditions/video/save",
    data: {
      ...data,
    },
  });
};

export const deleteMedia = async (id) => {
  return await dispatchRequest({
    method: "delete",
    url: `media/manager/${id}`,
  });
};

export const deleteAuditionVideo = async (id, audition_id) => {
  return await dispatchRequest({
    method: "delete",
    url: `t/auditions/video/delete/${id}/${audition_id}`,
  });
};

export const searchMarketplace = async (value) => {
  return dispatchRequest(
    {
      method: "get",
      url: `t/marketplaces/search?value=${value}`,
    },
    false
  );
};

export const addTagfeedback = async (data = {}) => {
  return await dispatchRequest({
    method: "post",
    url: `t/auditions/feedbacks/tags`,
    data,
  });
};

export const addRecommendationfeedback = async (data = {}) => {
  return await dispatchRequest({
    method: "post",
    url: `t/auditions/feeback/recommendations-marketplaces`,
    data,
  });
};

export const addPersonToRol = async (data = {}) => {
  //	console.log("AddPerson===>" + JSON.stringify(data))
  return await dispatchRequest({
    method: "post",
    url: `t/finalcast`,
    data,
  });
};

export const getFinalList = async (data) => {
  return await dispatchRequest({
    method: "get",
    url: `t/finalcast/${data}/audition`,
  });
};

export const removePersonToRol = async (data) => {
  return await dispatchRequest({
    method: "delete",
    url: `t/finalcast/${data}`,
  });
};

export const getUserInstantFeedback = async (data = {}) => {
  return await dispatchRequest({
    method: "get",
    url: `t/auditions/${data.audition_id}/feedbacks/details?user_id=${data.user_id}`,
  });
};

export const getUserTags = async (data = {}) => {
  return await dispatchRequest({
    method: "get",
    url: `t/auditions/${data.audition_id}/user/tags?user_id=${data.user_id}`,
  });
};

export const getUserMarket = async (data = {}) => {
  return await dispatchRequest({
    method: "get",
    url: `t/auditions/${data.audition_id}/feeback/recommendations-marketplaces-by-user?user_id=${data.user_id}`,
  });
};

export const updateInstantFeedback = async (audition, data = {}) => {
  return await dispatchRequest({
    method: "put",
    url: `t/auditions/${audition}/feedbacks/update`,
    data,
  });
};

export const updateTags = async (audition, data = {}) => {
  return await dispatchRequest({
    method: "put",
    url: `t/auditions/${audition}/feedbacks/user/tags`,
    data: { tags: data },
  });
};

export const updateMarkets = async (audition, data = {}) => {
  return await dispatchRequest({
    method: "put",
    url: `t/auditions/${audition}/feeback/recommendations-marketplaces/update`,
    data: { marketplaces: data },
  });
};

export const removePastTag = async (data) => {
  return await dispatchRequest({
    method: "delete",
    url: `t/auditions/feedbacks/tags/${data}/delete`,
  });
};

export const removePastTagMarket = async (data) => {
  return await dispatchRequest({
    method: "delete",
    url: `t/auditions/feeback/recommendations-marketplaces/${data}/delete`,
  });
};

export const reorderAppointments = async (audition, data) => {
  return await dispatchRequest({
    method: "put",
    url: `t/auditions/appointments/${audition}/slots`,
    data: { slots: data },
  });
};

export const getRoundSlots = async (round = 0) => {
  return await dispatchRequest({
    method: "get",
    url: `/t/appointments/${round}/slots`,
  });
};

export const getOnlineDocs = async (data = {}) => {
  return await dispatchRequest({
    method: "get",
    url: `/media/online`,
    params: data,
  });
};

export const getVideoList = async (audition_id, round_id) => {
  return await dispatchRequest({
    method: "get",
    url: `t/audition/${audition_id}/round/${round_id}/videos`,
  });
};

export const deleteVideo = async (video_id, audition_id) => {
  return await dispatchRequest({
    method: "delete",
    url: `t/auditions/video/delete/${video_id}/${audition_id}`,
  });
};

export const assignNumber = async (params) => {
  return await dispatchRequest({
    method: "post",
    url: `assignNumber`,
    params: {
      ...params,
    },
  });
};

export const instantFeedbacks = async (params) => {
  return await dispatchRequest({
    method: "post",
    url: `t/instantfeedbacks/add`,
    params: {
      ...params,
    },
  });
};

export const getfeedback = async (user_id) => {
  return await dispatchRequest({
    method: "get",
    url: `t/instantfeedbacks/defaultFeedback/${user_id}`,
  });
};
