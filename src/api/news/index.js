//custom
import { dispatchRequest } from '../../api';


export const getNews = async () => dispatchRequest({
    method: 'get',
    url: 't/blog/posts'
})

export const getNewsFilter = async (data = {}) => dispatchRequest({
    method: 'get',
    url: '/t/blog/posts/order_by',
    params: data
})

