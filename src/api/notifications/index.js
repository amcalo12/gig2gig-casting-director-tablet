//custom
import { dispatchRequest } from '../../api';


export const getNotifications = async () => dispatchRequest({
    method: 'get',
    url: 't/notification-history'
})

export const deleteNotifications = async (data) => dispatchRequest({
    method: 'delete',
    url: `t/notification-history/delete/${data}`
})

export const hadleNotification = async (data = {}) => dispatchRequest({
    method: 'get',
    url: `t/auditions/invite-accept/${data.custom_data}?status=${data.status}&notification_id=${data.id}`
})

export const markNotificationsRead = async () => dispatchRequest({
    method: 'get',
    url: 't/notification-read'
})
