//custom
import { dispatchRequest } from "../../api";

export const getTalents = async () =>
  dispatchRequest({
    method: "get",
    url: "t/performers/list"
  });

export const getMediaByType = async (type, user_id) =>
  dispatchRequest({
    method: "get",
    url: `t/media/user/list/${type}?performer_id=${user_id}`
  });

export const filterTalent = async (data = {}) =>
  dispatchRequest(
    {
      method: "post",
      url: "t/performers/filter",
      data
    },
    false
  );

export const createContract = async (data = {}) =>
  dispatchRequest({
    method: "post",
    url: "t/auditions/contract/save",
    data
  });

export const getContract = async (data = {}) =>
  dispatchRequest({
    method: "get",
    url: `t/auditions/contract/${data.personId}/${data.audition_id}`
  });

export const deleteContract = async data =>
  dispatchRequest({
    method: "delete",
    url: `t/auditions/contract/delete/${data}`
  });

export const getCommetsTalents = async (data = {}) =>
  dispatchRequest({
    method: "get",
    url: `t/performers/comments`,
    params: data
  });

export const getTagsTalents = async (data = {}) =>
  dispatchRequest({
    method: "get",
    url: `t/performers/tags`,
    params: data
  });

export const getContractUser = async (data = {}) =>
  dispatchRequest({
    method: "get",
    url: `t/performers/contracts`,
    params: data
  });

export const sharePerformance = async (data = {}) =>
  dispatchRequest({
    method: "post",
    url: `t/performers/code`,
    data
  });

export const validateCode = async (data = {}) =>
  dispatchRequest({
    method: "post",
    url: `t/performers/add`,
    data
  });

export const getAuditionVideoListByPerformer = async data =>
  dispatchRequest({
    method: "get",
    url: `t/auditions/list/${data}`
  });

export const getVideoListByAudition = async (audition_id, performer_id) =>
  dispatchRequest({
    method: "get",
    url: `t/auditions/video/list/${audition_id}/performer/${performer_id}`
  });
