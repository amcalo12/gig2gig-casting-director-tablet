import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Root } from 'native-base';
import { store, persistor } from './store';

// customs
import Splash from './src/screens/splash';
import SplashView from './src/components/commons/splash';
import "./src/utils/networking"

console.disableYellowBox = true;

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate
          loading={<SplashView />}
          persistor={persistor}
        >
          <Root>
            <Splash />
          </Root>
        </PersistGate>
      </Provider>
    );
  }
}
