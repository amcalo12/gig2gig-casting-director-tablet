## G2G Castings

### Dev enviroment

* Use Node version `11.10.0`
* If you use `nvm`, use nvm version `6.7.0`
* For install `node_modules` use `yarn` no `npm`
* install `node_modules` with command `yarn install`
* install `pods` with command `pod install` on `ios` dir
* All changes are documented in the CHANGELOG.md file with the following structure:
```
  kindOfChange = ['ADD', 'FIX', 'REMOVE', 'UPDATE', 'REDESING'] //for example
  ## [kind_of_change] year-month-day hour:minutes

  Example:
  ## [ADD, FIX] 2019-02-26 10:41
  * Add login
  * Fix login
```
* Name of the commitments with the following structure.
* After doing yarn install, you have to move the library 'react-native-picker-select' located on the folder "libs" to the node_modules  folder. 
```
## year-month-day hour:minutes = The reference that was created in the CHANGELOG.md file before the commit

See file CHANGELOG.md ## year-month-day hour:minutes

Example: 
See file CHANGELOG.md ##2019-02-26 10:41
```

* when you add a new library, remember fix the version installed in the `package.json`

* Happy coding :fire::v: