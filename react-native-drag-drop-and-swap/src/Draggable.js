/*
 * @Author: Ranvir Gorai 
 * @Date: 2018-01-30 15:04:14 
 * @Last Modified by: Ranvir Gorai
 * @Last Modified time: 2018-01-30 16:58:05
 */
import React from 'react';
import {
  TouchableOpacity,
  Animated
} from 'react-native';
import PropTypes from 'prop-types';

class Draggable extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Draggable';
    this._initiateDrag = this._initiateDrag.bind(this);
    this.state = {
      pan: new Animated.ValueXY(),
      zIndex: 0,
      position: 'relative'
    }
  }

  static contextTypes = {
    dragContext: PropTypes.any
  }

  static propTypes = {
    dragOn: PropTypes.oneOf(['onLongPress', 'onPressIn'])
  }

  _initiateDrag() {
    if (!this.props.disabled) this.context.dragContext.onDrag(this[`wrapperD${this.props.id}`], this.props.children, this.props.data, this.props.id);
  }

  static defaultProps = {
    dragOn: 'onLongPress'
  }

  componentDidUpdate = (prevProps) => {
    let { position, onAnimationEnds } = this.props;
    if (position !== prevProps.position) {
      Animated.timing(this.state.pan, {
        toValue: position,
        duration: 500
      }).start(() => {
        onAnimationEnds();
      });
    }
  }

  render() {
    let { position, zIndex } = this.state;
    const panStyle = {
      transform: this.state.pan.getTranslateTransform()
    }
    let isDragging = this.context.dragContext.dragging && this.context.dragContext.dragging.ref;

    isDragging = isDragging && isDragging === this[`wrapperD${this.props.id}`];
    return (
      <Animated.View style={[panStyle, this.props.style2]}>
        <TouchableOpacity activeOpacity={this.props.activeOpacity} style={this.props.style} onLongPress={this.props.dragOn === 'onLongPress' ? this._initiateDrag : null} delayLongPress={400} onPress={this.props.onPress} onPressIn={this.props.dragOn === 'onPressIn' ? this._initiateDrag : null} ref={(ref) => this[`wrapperD${this.props.id}`] = ref}>
          {
            React.Children.map(this.props.children, child => {
              return React.cloneElement(child, { ghost: isDragging })
            })
          }
        </TouchableOpacity>
      </Animated.View>
    )
      ;
  }
}

export default Draggable;
