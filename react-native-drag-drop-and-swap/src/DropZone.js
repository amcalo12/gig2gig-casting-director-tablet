/*
 * @Author: Ranvir Gorai 
 * @Date: 2018-01-30 15:04:18 
 * @Last Modified by: Ranvir Gorai 
 * @Last Modified time: 2018-01-30 15:04:18 
 */
import React from 'react';
import {
  View,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';

class DropZone extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'DropZone';
    this.state = {};
    this.reportMeasurements = this.reportMeasurements.bind(this);
    this.onEnter = this.onEnter.bind(this);
    this.onLeave = this.onLeave.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.state = {
      mesuare: null
    }
  }

  reportMeasurements = () => {
    let { mesuare } = this.state;
    let { setDropZoneRef } = this.props;
    if (this.props.dragging) this.context.dragContext.removeZone(this[`wrapper${this.props.id}`]);
    this[`wrapper${this.props.id}`].measure((_, __, width, height, x, y) => {
      if (!mesuare) {
        this.setState({ mesuare: { _, __, width, height, x, y } })
      }
      if (!this.props.dragging) this.context.dragContext.updateZone({ width, height, x, y, ref: this[`wrapper${this.props.id}`], onEnter: this.onEnter, onLeave: this.onLeave, onDrop: this.onDrop });

      if (!this.props.dragging || !mesuare) {
        setDropZoneRef({ _, __, width, height, x, y }, this.props.id);
      }
    })
  }

  static propTypes = {
    onEnter: PropTypes.func,
    onLeave: PropTypes.func,
    onDrop: PropTypes.func,
  }

  componentDidMount() {
    this.reportMeasurements();
    // this._timer = setInterval(this.reportMeasurements, 1000);
  }

  componentWillUnmount() {
    this.context.dragContext.removeZone(this[`wrapper${this.props.id}`]);
    clearInterval(this._timer);
  }

  componentDidUpdate() {
    this.reportMeasurements();
  }

  onEnter({ x, y }) {
    if (this.props.disabled) return;
    if (!this.state.active) {
      if (this.props.onEnter) this.props.onEnter();
      this.setState({
        active: true
      })
    }
  }

  onLeave() {
    if (this.props.disabled) return;
    if (this.state.active) {
      if (this.props.onLeave) this.props.onLeave();
      this.setState({
        active: false
      })
    }
  }

  onDrop(zone, id) {
    let { mesuare } = this.state;
    if (this.props.disabled) return;
    if (this.props.onDrop) this.props.onDrop({ mesuare, zone, id, dropZone: this.props.id });
    this.setState({
      active: false
    });
  }

  static contextTypes = {
    dragContext: PropTypes.any
  }

  render() {
    return <View style={this.props.style} pointerEvents={this.props.pointerEvents} onLayout={this.reportMeasurements} ref={(ref) => this[`wrapper${this.props.id}`] = ref}>
      {
        React.Children.map(this.props.children, child => {
          return React.cloneElement(child, Object.assign({}, this.props, { dragOver: this.state.active }));
        })
      }
    </View>;
  }
}

DropZone.defaultProps = {
  setDropZoneRef: () => { }
}

export default DropZone;
