package com.g2g.tablet.android;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.facebook.react.ReactApplication;
import com.inprogress.reactnativeyoutube.ReactNativeYouTube;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.horcrux.svg.SvgPackage;
import com.imagepicker.ImagePickerPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.rnfs.RNFSPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.wog.videoplayer.VideoPlayerPackage;


import org.reactnative.camera.RNCameraPackage;
import org.wonday.pdf.RCTPdfView;

import java.util.Arrays;
import java.util.List;

import ca.jaysoo.extradimensions.ExtraDimensionsPackage;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.firestore.RNFirebaseFirestorePackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import io.invertase.firebase.storage.RNFirebaseStoragePackage;

import com.shahenlibrary.RNVideoProcessingPackage;

public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
                    new PickerPackage(),
                    new RNDeviceInfo(),
                    new AsyncStoragePackage(),
                    new ImagePickerPackage(),
                    new ReactVideoPackage(),
                    new VideoPlayerPackage(),
                    new RNCameraPackage(),
                    new RNFetchBlobPackage(),
                    new RNGestureHandlerPackage(),
                    new RNFirebasePackage(),
                    new RNFirebaseStoragePackage(),
                    new LinearGradientPackage(),
                    new ExtraDimensionsPackage(),
                    new DocumentPickerPackage(),
                    new MapsPackage(),
                    new RNFirebaseMessagingPackage(),
                    new RNFirebaseNotificationsPackage(),
                    new RNCWebViewPackage(),
                    new SvgPackage(),
                    new RNFSPackage(),
                    new RNVideoProcessingPackage(),
                    new RNFirebaseFirestorePackage(),
                    new RCTPdfView(),
                    new ReactNativeYouTube(),
                    new RNFileViewerPackage()

            );
        }


        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    public boolean canOverrideExistingModule() {
        return true;
    }

    ;

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
