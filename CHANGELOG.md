## [ADD] 2019-02-26 11:00

- Initialize App
- Add README.md file
- Add CHANGELOG.md file

## [CHANGE] 2019-02-26 14:26

- Rename project
- set bundle identifier

## [ADD] 2019-02-26 16:00

- add firebase configuration in android
- add key for release apk in android folder
- add firebase configuration in ios

## [ADD] 2019-02-26 16:15

- add folder struct
- add fonts in assets folder
- add example of folder struct

## [ADD] 2019-02-26 16:48

- compile android

## [ADD] 2019-02-27 8:26

- Add google service file in ios

## [CHANGE] 2019-02-27 8:30

- Remove tvOS targets from xcode
- Add reference Google-Service-info.plist

## [CHANGE] 2019-02-27 8:55

- Remvove iOS Test target
- Rename iOS scheme

## [CHANGE] 2019-02-27 9:03

- change relative to group path in Xcode

## [ADD] 2019-02-27 9:28

- Add react-navigation
- Add react-native-gesture-handler
- Configurate react-native-gesture-handler

## [ADD] 2019-02-27 11:50

- Add nativa base
- linking fonts
- configure redux, react-redux and react-persist
- reducer of login
- validate splash

## [CHANGE, ADD] 2019-02-27 13:11

- fix library versions
- Add nexa fonts
- Add new rule in README.md

## [ADD] 2019-02-27 17:00

- add assest & splash xcode

## [ADD, FIX] 2019-02-27 23:15

- fix error with react native gesture
- add gradient button component
- add background in view login

## [ADD] 2019-02-28 9:43

- add input radius component
- complete login view

## [ADD] 2019-02-28 09:50

- add splash for react

## [CHANGE] 2019-02-28 11:00

- change reference react native linear gradient frameworks and libraries xcode

## [CHANGE] 2019-02-28 11:19

- stylesheet guide for splash

## [CHANGE] 2019-02-28 11:35

- change url of background and splash component

## [ADD] 2019-02-28 11:38

- Add splash screen android

## [CHANGE] 2019-02-28 13:51

- Change text and imagen in splash screen android

## [CHANGE] 2019-02-28 14:00

- Change position of imagen and text splash screen android

## [ADD] 2019-02-28 14:04

- Add forgot password and create acount links in login screen

## [ADD] 2019-02-28 16:55

- Add forgot password
- Add navigation bar

## [FIX] 2019-02-28 17:00

- FIX improve iconback props in forget password

## [ADD, CHANGE] 2019-03-01 08:41

- Add create account component
- change default props for commons components

## [FIX] 2019-03-01 10:40

- Fix improve header component
- Change way to inject components in headers

## [FIX] 2019-03-01 11:15

- Fix identation components
- Change back set onPress only if has reviced props
- Fix Header component should not receive props

## [CHANGE] 2019-03-01 14:30

- Refact forgot password view with flex box

## [ADD, CHANGE] 2019-03-04 13:12

- Add datepicker select component
- Add picker select component
- change width and height in input's and gradient button
- sign up screen completed

## [ADD] 2019-03-04 13:30

- Add picker image

## [ADD] 2019-03-4 14:00

- Add card audition component

## [FIX] 2019-03-04 14:15

- Fix default style for audition card
- Add onPress handlers to buttons

## [ADD] 2019-03-04 15:30

- Show image in create account

## [ADD] 2019-03-04 15:59

- Add header button component
- Add section header component

## [ADD] 2019-03-04 17:30

- Add Home auditions sections

## [ADD] 2019-03-05 11:13

- Add sidebar menu
- configure combine reducers
- create actions folder for redux

## [ADD] 2019-03-06 8:00

- Add settings navigation

## [ADD, CHANGE] 2019-03-06 15:20

- Add settings screen
- Change, passing a function in component with touchable opacity

## [ADD] 2019-03-07 13:10

- Add my info screen in settings
- Add input disable, select disable and datepicker disable components

## [ADD, CHANGE] 2019-03-07 15:00

- Add app help screen
- Add contact us screen
- Add help screen
- Add privacy policy screen
- Add Terms of use screen
- Change, dynamic width and height in components of settings

## [ADD, CHANGE] 2019-03-08 13:24

- Add input search in header home
- Change, dynamic width and height in audition card

## [ADD] 2019-03-11 13:46

- Add contribution screen
- add autocapitalize=false in input type text's

## [ADD, CHANGE] 2019-03-12 10:51

- Add manage view
- Change names of components in auditions components folder

## [ADD, CHANGE] 2019-03-12 15:06

- Add, Open audition and close auditions in manage screen
- Change, dynamic width and height in manage screen
- Add icons

## [CHANGE] 2019-03-13 10:36

- Change orientation to landscape

## [ADD, CHANGE] 2019-03-15 16:45

- Add partaker detail screen
- Add icons for partaker detail screen
- Add extra-dimension-library in android
- Add function resize in styles for dynamic height and width
- Change react native splash to landscape
- Change all screens and components to landscape
- Chnage dynamic width and height in all screens and components

## [ADD, CHANGE] 2019-03-19 8:40

- Add "Super Admin" Partaker Detail screen
- Add VSC configuration
- Add icons for "Super Admin" Partaker Detail screen
- Change scopes of roles in auditions

## [CHANGE] 2019-03-19 8:51

- Refact show search in audition detail(manager and contributor)

## [ADD] 2019-03-19 10:49

- Add file menu component
- Add pin keyboard component
- Add alert iOS component
- Add ref to file component in father component
- Add implementation for all new component in audition manage screen

##[ADD] 2019-03-19 10:59

- Add changelog script

##[CHANGE] 2019-03-19 11:18

- Change git ignore for pods
- Change README pod install instruction

## [Add, CHANGE] 2019-03-19 18:00

- Add add role modal
- Add edit role modal
- Add props multi line in input Radius component
- Add partaker modal
- Add appointment managment modal

## [ADD] 2019-03-19 18:06

- Add create audition screen
- Add tags components
- Add icons for create audition view

## [Add] 2019-03-20 9:14

- Add appointment managment modal
- Add row appointment component

## [CHANGE] 2019-03-20 9:24

- Change app navigation stack in routes
- Refact card audition component

## [ADD] 2019-03-20 11:10

- Add bordered select component

## [ADD, CHANGE] 2019-03-20 15:08

- Add edit audition screen
- change margin for padding in all scrollview's

## [CHANGE] 2019-03-20 15:57

- Refact components when he have one prop

## [ADD] 2019-03-20 16:40

- Add pickers to appointments modal

## [ADD] 2019-03-21 10:14

- Add pickers to appointments modal implementation

## [ADD, REMOVE] 2019-03-21 11:17

- Add enter monitor mode screen
- Remove tags when audtions has passed

## [ADD] 2019-03-21 17:43

- Add pickers to appointments modal implementation
- Add appointments modal create
- Add invitation modal create

## [ADD, CHANGE] 2019-03-21 17:44

- Add chekin mode screen for manager
- Change routes struct

## [ADD] 2019-03-22 8:21

- Add edit audition routes

## [ADD] 2019-03-22 8:30

- Add navigation back in edit audition

## [ADD] 2019-03-24 18:17

- Add checkin mode screens

## [ADD] 2019-03-25 13:40

- Configure firebase storage in android and ios
- utils folder
- uploadImage and getImage functions
- pod files

## [ADD, CHANGE] 2019-03-26 17:05

- Add locale file for translates
- Add api method for dispatch requets
- Implement logic in login, create account and forgot password
- Change gradient buttton props

## [ADD, CHANGE] 2019-03-26 17:08

- implementation of all modals in all screens
- Add all functions of modals

## [CHANGE] 2019-03-27 10:22

- Change function uploadImage to uploadAsset

## [ADD] 2019-03-27 13:05

- Add file picker in android and ios

## [CHANGE] 2019-03-27 14:06

- Change method of get file

## [ADD] 2019-03-27 15:18

- Add api logic in create account
- Add list of states of USA

## [FIX] 2019-03-27 15:38

- Fix problem with react native document picker library

## [ADD] 2019-03-28 10:03

- Configure react native maps in ios

## [ADD] 2019-03-28 10:21

- Configure react native maps in android

## [ADD] 2019-03-28 13:34

- Add location modal
- Implement location modal in create audition

## [ADD] 2019-03-28 14:16

- Add calendar component

## [CHANGE, ADD] 2019-03-28 16:28

- Implement info partaker modal
- Add pending icon in invitation modal

## [ADD] 2019-03-28 17:30

- Add split animation

## [FIX,ADD] 2019-03-29 10:34

- Fix calendar day height
- Add animation in detail

## [FIX] 2019-03-29 11:46

- Fix modals height

## [ADD] 2019-04-02 15:40

- Add api logic to create a new audition
- Add api locic to get auditions in home screen

## [ADD] 2019-04-03 16:04

- Add reset action to home when audition was created

## [ADD] 2019-04-04 15:48

- Add logic api in show audition, (detail, manager and contributor)
- Add name in files and images
- Add scrollview horizontal in show tags in auditions details (detail, manager and contributor)

## [ADD, CHANGE] 2019-04-04 16:35

- Add locale in all screen(todo: except audition module)
- Change way of show error message with parse error function

## [ADD] 2019-04-05 16:43

- Add filter in home
- Add ios catalog icons
- Upload build to app store connect

## [ADD] 2019-04-08 8:17

- Add configuration for generate singned apk

## [ADD, CHANGE] 2019-04-10 8:16

- Change ios catalog icons
- Add logic API for open and close audititon

## [FIX, REMOVE] 2019-04-10 8:39

- Remove console log's
- Fix indent

## [ADD, REMOVE] 2019-04-12 15:08

- Add android catalog icons
- Add edit audition logic api
- Remove mapStateToProps in screen where its doesn't use.
- Add locale in edit audition

## [REMOVE] 2019-04-12 16:24

- Remove console.log

## [CHANGE] 2019-04-12 17:24

- Change ios catalog icons

## [ADD] 2019-04-19 20:19

- Configurate react native camera
- Configurate react native permissions

## [ADD] 2019-04-19 21:11

- Configurate react native scan qr

## [ADD] 2019-04-22 13:06

- Add set checkin pin and monitor updates pin in manage audition screen

## [ADD] 2019-04-22 14:40

- Add webview in setting's screens

## [FIX] 2019-04-24 11:06

- Fix first QA

## [REMOVE] 2019-04-24 11:35

- Remove location in users

## [ADD] 2019-04-24 15:08

- Implement im walk checkin(70%)
- Add locale to walk checkin
- redux logic for walk checkin

## [CHANGE] 2019-04-24 17:11

- Change splash in ios

## [CHANGE] 2019-04-25 13:06

- Change splash in android

## [ADD] 2019-04-26 10:09

- Completed checkin guest api logic

## [ADD] 2019-04-26 11:58

- Add performance list in detail of audition

## [ADD] 2019-04-26 17:19

- Add performance modal detail

## [ADD] 2019-04-26 17:41

- IOS bundle

## [ADD] 2019-04-27 17:46

- IOS bundle

## [ADD] 2019-04-27 19:50

- Complete checkin with appointment

## [ADD, CHANGE] 2019-04-29 16:18

- change in checkin mode
- Add role in checkin mode
- Add monitor updates

## [ADD] 2019-04-30 17:30

- Add feedback in contributor
- Add feedback in manager
- Fetch audition videos

## [ADD] 2019-05-02 13:23

- Audition videos
- Partaker detail calendar

## [ADD, FIX] 2019-05-02 16:12

- Add count of audition in sidebar
- Fix error in create audition
- Fix second file of QA

## [FIX] 2019-05-02 17:30

- Fix edit audition

## [ADD, FIX] 2019-05-03 11:00

- Add favorite icon when perfomance is favorite in audition user list
- Fix boolean value in partaker component

## [CHANGE] 2019-05-03 13:40

- Ios bundle
- Target version

## [ADD] 2019-05-03 16:57

- Google maps autocomplete 50%

## [ADD] 2019-05-03 17:02

- Add slot id for create audition video

## [ADD] 2019-05-03 17:07

- Show error message when video exist

## [ADD] 2019-05-06 10:40

- Google maps autocomplete finished

## [CHANGE] 2019-05-06 13:15

- ios bundle 9

## [CHANGE] 2019-05-06 17:21

- ios bundle 10
- Fix QA file 3

## [CHANGE] 2019-05-07 10:27

- ios bundle 11
- Fix QA file 4

## [ADD] 2019-05-08 09:02

- Add network debugger

## [ADD, CHANGE] 2019-05-09 10:23

- Add block loading when audition is created
- Add upload document in edit audition
- Documents options in create audition
- Role image optional

## [CHANGE, FIX] 2019-05-09 11:27

- Change feedback emojis in performance's detail
- Fix in availability calendar when the date is equal to last day of week

## [FIX] 2019-05-09 12:03

- Fix icon blur in files

## [FIX] 2019-05-09 14:20

- Fix letter g with font

## [ADD] 2019-05-09 16:11

- Add send invitations in edit audition

## [CHANGE] 2019-05-09 16:40

- ios bundle 12

## [ADD, CHANGE] 2019-05-16 16:01

- Add label to show pin in manage audition
- Change, Hide contributor tag in home when the current user is a manage
- Add (\*) in required fields in create a new audition
- Add mailto to in settings contact us

## [CHANGE] 2019-05-09 16:36

- ios bundle 13

## [CHANGE] 2019-06-04 12:42

- Change Help screen for FAQ screen
- Change check icon in manage audition
- ios bundle 14

## [FIX] 2019-06-05 15:10

- fix when app is launched
- Fix margin and padding of input text

## [ADD] 2019-06-05 14:37

- add export bundle in android

## [FIX] 2019-06-06 15:53

- Fix padding and margin in select inputs
- Fix gesture handler drawer to be able to use it in android

## [CHANGE] 2019-06-06 14:12

- Ios build 15

## [CHANGE] 2019-06-06 14:17

- Android build 2

## [CHANGE] 2019-06-14 17:27

- Check-in: I have an appointment flow
- Ios build 16
- Android version 3

## [CHANGE, FIX] 2019-08-08 16:39

- Sort auditions check in
- Change null text when user doesnt have lastname

## [CHANGE] 2019-08-08 16:39
  * Change Biuld numbers

## [ADD] 2019-08-13 08:45
  * Add casting branch

## [ADD] 2019-08-14 10:31
  * Add notification libs support
  * Get user token for notification
  
## [REDESIGN] 2019-08-14 9:48
  * Add the Audition Title & start time on the Top of the Check-In Screen
  * Add a “Camera Rotate” rotate button on the Check-in screen

## [REDESIGN] 2019-08-14 13:00
  * Add a “Camera Rotate” rotate button on the Check-in screen
  * Added modal on check-in screen

## [ADD] 2019-08-14 16:02
  * Set user notification to API
  * Add mark document has shareable (only UI)

## [REDESIGN] 2019-08-14 16:28
  * Insert fields for Phone #, Email, and Other under "Contact Information"
  * Insert fields for Personnel Information and Additional Information under "Performance Information"

## [ADD] 2019-08-14 16:02
  * Set user notification to API
  * Add mark document has shareable (only UI)

## [ADD] 2019-08-15 11:05
  * Add design to screen "Create New Audition"

## [ADD] 2019-08-15 11:13
  * Added instructions for libs on README.md

## [FIX] 2019-08-15 11:10
  * Fix mark document has shareable

## [ADD] 2019-08-16 09:18
  * Added flip camera with functionality on QA screen.

## [ADD] 2019-08-16 11:30
  * Tab bar menu functionality.

## [ADD] 2019-08-19 16:15
  * Added talent database screen.

## [ADD] 2019-08-16 11:33
  * The ability for casting directors to recommend a marketplace vendor to a performer in “Instant Feedback”
  * The ability to add tags to a performer that will be filed in the performers information
  * Redising admin panel for performance information

## [ADD] 2019-08-19 14:37
  * Add G2G blog desing
  * Add G2G blog details desing

## [ADD] 2019-08-21 09:40
  * Added integration of phone, email, etc fields with api

## [ADD] 2019-08-21 09:45
  * Add document shareable options integration with API

## [FIX] 2019-08-21 13:50

- Validate dates in new audition.
- RC-FIX iCloud permission.
- Fix order of dates in create.

## [ADD] 2019-08-21 23:10
  * Add notification screen

## [ADD, FIX] 2019-08-22 11:35
  * Add api integration for tags and marketplace for audition Feedback
  * Fix some layout problems

## [ADD, FIX] 2019-08-22 12:15
  * Add notification screens 90%
  * Fix some layout problems

## [FIX] 2019-08-22 13:48
  * Fix keyboard dissmiss after type on marketplace search

## [ADD, FIXED] 2019-08-23 12:05
  * Add notification screens 90%
  * Fixed create new audition and edit audition
  * Fixed design of notification screen.

## [FIX] 2019-08-23 11:54
  * fix create audition files
  * Revision of week 1 and 2

- Change Biuld numbers

## [FIX] 2019-08-23 10:40

- Fix camera permissions

## [FIX] 2019-08-26 17:44

- Fix add apointment
- Fix dates min

## [ADD] 2019-08-30 16:04
  * Drag and drop functionality UI

## [ADD, CHANGE] 2019-09-04 14:34
  * Finished Drag an drop funcionality
  * Add API integrations for news & updates
  * Add API integrations for Notifications
  * Change some layouts

## [CHANGE] 2019-09-04 17:18
  * Change WebView lib for IOS and Android 
  * Use jetify for android X support

## [ADD] 2019-09-06 16:31
  * Add talen Database and details UI
  * Add API integratios for talent datadate
  * Add sort news API

## [ADD, CHANGE] 2019-09-10 12:01
  * Add options menu for QR code and notifications
  * Add functionality to get number of appoitmens
  * Change UI components
  * Add informations for Talent Database details
  